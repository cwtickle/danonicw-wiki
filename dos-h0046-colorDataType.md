**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-h0046-colorDataType) | Japanese** 

[Home](./) &gt; [譜面ヘッダー仕様](dos_header) &gt; [設定時の初期設定](dos_header#-設定時の初期設定)

| [<- gaugeX](dos-h0022-gaugeX) | **colorDataType** | [colorCdPaddingUse ->](dos-h0047-colorCdPaddingUse) |

## colorDataType
- 色変化の過去互換設定  
※ ver36.1.0以降、この設定は使用できません。

### 使い方
```
|colorDataType=v6|
```
### 説明
色変化の過去バージョンの互換設定を有効にするかを設定します。  
デフォルトは指定なし(空)。`v6`で過去のバージョンを有効にします。  

|値|既定|内容|
|----|----|----|
|(未指定)|*|フリーズアローヒット時の個別色変化のタイミングをステップゾーン到達時にする|
|v6||フリーズアローヒット時の個別色変化のタイミングをフリーズアロー通常時と同じにする(過去バージョン互換)|

### 関連項目
- [色変化 (acolor_data, color_data)](dos-e0002-colorData) [:pencil:](dos-e0002-colorData/_edit) 

### 更新履歴

|Version|変更内容|
|----|----|
|[v36.1.0](https://github.com/cwtickle/danoniplus/releases/tag/v36.1.0)|・機能廃止|
|[v7.3.0](https://github.com/cwtickle/danoniplus/releases/tag/v7.3.0)|・初回実装|

| [<- gaugeX](dos-h0022-gaugeX) | **colorDataType** | [colorCdPaddingUse ->](dos-h0047-colorCdPaddingUse) |