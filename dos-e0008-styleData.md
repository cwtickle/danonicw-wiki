**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-e0008-styleData) | Japanese** 

[↑ 譜面エフェクト仕様に戻る](./dos_effect)

| [< 背景・マスクモーション](./dos-e0004-animationData) | **スキン変更** | [矢印・フリーズアローモーション >](./dos-e0005-motionData) |

## スキン変更 (style_data)
### 概要
- 画面内のスキンに関する部分の色やスタイルを変更します。

<img src="https://github.com/cwtickle/danoniplus/assets/44026291/21473686-748a-4455-a1ce-2305eefdfb36" width="50%"><img src="https://github.com/cwtickle/danoniplus/assets/44026291/5fc00333-bfd3-4b3e-8e82-0c54d0b9b757" width="50%">

### 使い方／記述仕様
- 3つで1セット。改行区切りです。

#### 1. 通常
```
|style_data=
0,--background,-45deg:#993399:#000000:#993333
|
|styletitle_data=
100,--title-star,#9999ff
300,--background,#999999
500,--title-star,#ff9999
|
|styleresult_data=
0,--background,135deg:#339933:#000000:#999933
|
|stylefailedS_data=
0,--background,45deg:#339999:#000000:#999933
|
```

|番号|論理名|設定例|内容|
|----|----|----|----|
|1|Frame|200|変更するタイミングのフレーム数を指定します。|
|2|CSS-CustomProperty|--background|CSSのカスタムプロパティ名を指定します。|
|3|Style|-45deg:#993399:#000000:#993333|適用するスタイルを指定します。<br>グラデーション指定も使えます。|

#### 2. 他のカスタムプロパティから設定を引用
```
|style_data=
0,--background,-45deg:#993399:#000000:#993333
700,--main-stepDefault,--background
|
```

|番号|論理名|設定例|内容|
|----|----|----|----|
|1|Frame|200|変更するタイミングのフレーム数を指定します。|
|2|CSS-CustomProperty|--background|設定するCSSのカスタムプロパティ名を指定します。|
|3|CSS-CustomProperty(Reference)|--common-ii|引用元のCSSカスタムプロパティ名を指定します。|


#### 3. コメント文
```
|styletitle_data=
0,＜コメントを入力＞
|
```
|番号|論理名|設定例|内容|
|----|----|----|----|
|1|Frame|200|フレーム数指定。指定は任意です。|
|2|Comment|＜演出開始＞|コメント文を自由に入力できます。|

#### 4. 他設定の流用 (タイトル以外)
```
|styleCross_data=styleRev_data|
```

|番号|論理名|設定例|内容|
|----|----|----|----|
|1|Data Name|styleRev_data|コピー元のスキン変更のデータ名を指定します。|

### データ名の種類
- [背景・マスクモーション](./dos-e0004-animationData)と同じような指定が可能です。
- タイトル・メイン・リザルトで設定が分かれています。言語別も可能です。

#### 通常用

|画面|スキン変更<br>(言語未指定)|スキン変更<br>(日本語)|スキン変更<br>(英語)|
|----|----|----|----|
|タイトル|styletitle_data|styletitle**Ja**_data|styletitle**En**_data|
|メイン(通常)|style_data, <br>style2_data, <br>...|style**Ja**_data, <br>style**Ja**2_data, <br>...|style**En**_data, <br>style**En**2_data, <br>...|
|メイン(リバース)|styleRev_data, <br>styleRev2_data, <br>...|styleRev**Ja**_data, <br>styleRev**Ja**2_data, <br>...|styleRev**En**_data, <br>styleRev**En**2_data, <br>...|
|メイン(Cross, Split 他)|styleAlt_data, <br>styleAlt2_data, <br>...<br>styleCross_data, <br>styleFlat_data, <br>...|styleAlt**Ja**_data, <br>styleAlt**Ja**2_data, <br>...<br>styleCross**Ja**_data, <br>styleFlat**Ja**_data, <br>...|styleAlt**En**_data, <br>styleAlt**En**2_data, <br>...<br>styleCross**En**_data, <br>styleFlat**En**_data, <br>...|
|リザルト(クリア時)|styleresult_data<br>styleresult2_data<br>...|styleresult**Ja**_data<br>styleresult**Ja**2_data<br>...|styleresult**En**_data<br>styleresult**En**2_data<br>...|
|リザルト(途中終了時)|stylefailedS_data<br>stylefailedS2_data<br>...|stylefailedS**Ja**_data<br>stylefailedS**Ja**2_data<br>...|stylefailedS**En**_data<br>stylefailedS**En**2_data<br>...|
|リザルト(ノルマ失敗時)|stylefailedB_data<br>stylefailedB2_data<br>...|stylefailedB**Ja**_data<br>stylefailedB**Ja**2_data<br>...|stylefailedB**En**_data<br>stylefailedB**En**2_data<br>...|

#### 別キーモード用（未設定時は通常用を適用）

|画面|スキン変更<br>(言語未指定)|スキン変更<br>(日本語)|スキン変更<br>(英語)|
|----|----|----|----|
|メイン(通常)|style**A**_data, <br>style**A**2_data, <br>...|style**A**Ja_data, <br>style**A**Ja2_data, <br>...|style**A**En_data, <br>style**A**En2_data, <br>...|
|メイン(リバース)|styleRev**A**_data, <br>styleRev**A**2_data, <br>...|styleRev**A**Ja_data, <br>styleRev**A**Ja2_data, <br>...|styleRev**A**En_data, <br>styleRev**A**En2_data, <br>...|
|メイン(Cross, Split 他)|styleAlt**A**_data, <br>styleAlt**A**2_data, <br>...<br>styleCross**A**_data, <br>styleFlat**A**_data, <br>...|styleAlt**A**Ja_data, <br>styleAlt**A**Ja2_data, <br>...<br>styleCross**A**Ja_data, <br>styleFlat**A**Ja_data, <br>...|styleAlt**A**En_data, <br>styleAlt**A**En2_data, <br>...<br>styleCross**A**En_data, <br>styleFlat**A**En_data, <br>...|

- タイトル以外のスキン変更については指定が無い場合、  
例えば 2譜面目のリザルト(ノルマ失敗時) については以下の順で適用されます。

|適用順|適用モーション|意味|
|----|----|----|
|1|stylefailedBJa2_data<br>stylefailedBEn2_data|リザルト(ノルマ失敗時)の２譜面目 ※言語指定あり|
|2|stylefailedBJa_data<br>stylefailedBEn_data|リザルト(ノルマ失敗時)の１譜面目 ※言語指定あり|
|3|stylefailedB2_data|リザルト(ノルマ失敗時)の２譜面目|
|4|stylefailedB_data|リザルト(ノルマ失敗時)の１譜面目|
|5|styleresultJa2_data<br>styleresultEn2_data|リザルト(クリア時)の２譜面目 ※言語指定あり|
|6|styleresultJa_data<br>styleresultEn_data|リザルト(クリア時)の１譜面目 ※言語指定あり|
|7|styleresult2_data|リザルト(クリア時)の２譜面目|
|8|styleresult_data|リザルト(クリア時)の１譜面目|

#### 補足
- ver34.0.0より、キー別・パターン別の指定が可能になりました。
```
|transKey9A=$$9B$9C|

|style_data=...|         // 全体共通
|style2_data=...|        // 2譜面目共通
|styleEn2_data=...|      // 2譜面目、英語モードのみ適用
|styleAEn2_data=...|     // 2譜面目、英語モード、別キーモード(9B, 9C)のみ適用
|style<9C>En2_data=...|  // 2譜面目、英語モード、別キーモード(9C)のみ適用
|style<9A>En2_data=...|  // 2譜面目、英語モード、別キーモード外のみ適用
|style[2]En2_data=...|   // 2譜面目、英語モード、キーパターン:2のみ適用
```

### 設定できるCSSカスタムプロパティの一覧とデフォルト値
- コメントアウトしている変数も利用可能です。
- 初期値は別の設定値で上書きされるよう、コメントアウトしています。
- 各変数（CSSカスタムプロパティ）の意味については[スキンファイル仕様](./AboutSkin)をご覧ください。
- 末尾が`-x`のものはグラデーション指定ができません。
```css
:root {
	/* 背景 */
	--background: linear-gradient(#000000, #222222);
	/* --back-chartDetail: var(--background); */
	/* --back-difListL: var(--background); */
	/* --back-difListR: var(--back-difListL, var(--background)); */

	/* タイトル共通色 */
	--title-base: #cccccc;

	/* 画面タイトル色 */
	--title-dancing: #6666ff;
	--title-onigiri: #ff6666;
	--title-star: #ffff66;
	--title-display: #ffff66;

	/* タイトルサイズ */
	--title-siz-x: 40px;

	/* 設定別ヘッダー */
	--settings-difficulty-x: #ff9999;
	--settings-speed-x: #ffff99;
	--settings-motion-x: #eeff99;
	--settings-scroll-x: #ddff99;
	--settings-shuffle-x: #99ff99;
	--settings-autoPlay-x: #99ffbb;
	--settings-gauge-x: #99ffdd;
	--settings-adjustment-x: #99ffff;
	--settings-fadein-x: #99eeff;
	--settings-volume-x: #99ddff;
	--settings-appearance-x: #cc99ff;
	--settings-opacity-x: #ee99ff;
	--settings-hitPosition-x: #ff99ff;

	/* 譜面選択部分背景 */
	--settings-difSelector: #111111;

	/* 設定無効化時 */
	--settings-disabled-x: #666666;

	/* フェードインバー */
	--settings-fadeinBar-x: #ffffff;

	/* ゲージ表示 (可変) */
	--settings-lifeVal-x: #ff9966;

	/* ゲージ設定枠 */
	--settings-bc-gaugeTable: #666666;
	--settings-slice-gaugeTable-x: 1;
	--settings-border-gaugeTable-x: 1px;

	/* キーコンフィグ関連 */
	--keyconfig-warning-x: #ffff99;
	--keyconfig-imgType-x: #99ddff;
	--keyconfig-colorType-x: #ffdd99;
	--keyconfig-changekey-x: #ffff00;
	--keyconfig-defaultkey-x: #99ccff;

	/* 判定 */
	--common-ii: #66ffff;
	--common-shakin: #99ff99;
	--common-matari: #ff9966;
	--common-shobon: #ccccff;
	--common-uwan: #ff9999;
	--common-kita: #ffff99;
	--common-iknai: #99ff66;
	--common-combo: #ffffff;
	--common-score-x: #ffffff;
	/* --common-comboJ: var(--common-kita); */
	/* --common-comboFJ: var(--common-ii); */
	/* --common-diffFast: var(--common-matari); */
	/* --common-diffSlow: var(--common-shobon); */
	/* --common-estAdj: var(--common-shakin); */
	/* --common-excessive: var(--common-kita); */

	/* ステップゾーン */
	--main-stepKeyDown: #66ffff;
	--main-stepDefault: #999999;
	--main-stepDummy: #777777;
	--main-stepIi: var(--common-ii);
	--main-stepShakin: var(--common-shakin);
	--main-stepMatari: var(--common-matari);
	--main-stepShobon: var(--common-shobon);
	--main-stepExcessive: #ffff99;
	--main-objStepShadow: #000000;
	--main-objFrzShadow: #000000;
	--main-frzHitTop: #ffffff;

	/* ライフゲージ */
	--life-max: #444400;
	--life-cleared: #004444;
	--life-failed: #444444;

	/* ライフゲージ本体 */
	--life-background: #222222;
	--life-bg-border: #555555;
	--life-color-border-x: #cccccc;

	/* 結果画面 */
	--result-lbl-x: #999999;
	--result-style-x: #cccccc;

	--result-bc-playwindow: #666666;
	--result-slice-playwindow-x: 1;
	--result-border-playwindow-x: 1px;

	--result-score-x: #ffffff;
	--result-scoreHiBracket-x: #999999;
	--result-scoreHi-x: #cccccc;
	--result-scoreHiPlus-x: #ffff99;
	--result-noRecord-x: #999999;

	--result-allPerfect: #ffffff;
	--result-perfect: #ffffcc;
	--result-fullCombo: #66ffff;
	--result-cleared: #ffff66;
	--result-failed: #ff6666;

	/* 結果画面のCleared/Failedを表示している文字のサイズ */
	--result-siz-window-x: 80px;

	/* ボタン */
	/* Click Here */
	--button-bg-start: #333333;
	--button-color-start-x: #ffffff;
	--button-bgHover-start: #666666;
	--button-colorHover-start-x: #ffffff;

	/* 汎用 */
	--button-bg-default: #111111;
	--button-color-default-x: #ffffff;
	--button-bgHover-default: #666666;
	--button-colorHover-default-x: #ffffff;

	/* 汎用(背景が透明) */
	--button-bg-defaultNo: #00000000;
	--button-bgHover-defaultNo: #666666;

	/* 設定用ミニボタン */
	--button-bg-mini: #333333;
	--button-color-mini-x: #ffffff;
	--button-bgHover-mini: #999900;
	--button-colorHover-mini-x: #ffffff;

	/* 戻るボタン */
	--button-bg-back: #111133;
	--button-color-back-x: #cccccc;
	--button-bgHover-back: #000099;
	--button-colorHover-back-x: #cccccc;

	/* 設定ボタン */
	--button-bg-setting: #333311;
	--button-color-setting-x: #ffffff;
	--button-bgHover-setting: #999900;
	--button-colorHover-setting-x: #ffffff;

	/* 開始ボタン */
	--button-bg-next: #331111;
	--button-color-next-x: #ffffff;
	--button-bgHover-next: #990000;
	--button-colorHover-next-x: #ffffff;

	/* リセットボタン */
	--button-bg-reset: #113311;
	--button-color-reset-x: #ffffff;
	--button-bgHover-reset: #009900;
	--button-colorHover-reset-x: #ffffff;

	/* 外部SNSボタン */
	--button-bg-sns: #113333;
	--button-color-sns-x: #ffffff;
	--button-bgHover-sns: #009999;
	--button-colorHover-sns-x: #ffffff;

	/* ON/OFFボタン */
	--button-color-off-x: #666666;
	--button-border-off-x: #000000 #333333;
	--button-color-on-x: #ffffff;
	--button-border-on-x: #000000 #cccccc;

	/* ON/OFFボタン Reverse用 */
	--button-color-offRev-x: #cccccc;
	--button-border-offRev-x: #000000 #999999;
	--button-color-onRev-x: #ffffff;
	--button-border-onRev-x: #000000 #ddff99;

	/* ON/OFFボタン 無効時 */
	--button-bg-offDisabled: #333333;
	--button-color-offDisabled-x: #999999;
	--button-bg-onDisabled: #009999;
	--button-color-onDisabled-x: #ffffff;
}
```

### 補足
- デフォルトスキンで背景色を変更するには、下記のように`canvas`タグが含まれない形にする必要があります。
- ver10以降のhtmlテンプレートでは`canvas`タグが含まれない形になっているため問題は起こりませんが、  
ver9以前のテンプレートを流用している場合はご注意ください。
```html
<div id="canvas-frame">
   <!-- <canvas id="layer0" width="600" height="500"></canvas> コメントアウト -->
   <!-- <canvas id="layer1" width="600" height="500"></canvas> コメントアウト -->
</div>
```
- 共通化の影響で外せない場合、ver33.3.0以降の場合は [bgCanvasUse](./dos-h0094-bgCanvasUse) を `false`に設定すれば対処可能です。
- それ以前のバージョンの場合、デフォルトスキンを変更することで対処します。
`/skin`フォルダにある「danoni_skin_default.css」をコピーして「danoni_skin_default_add.css」を作成してください。  
その上で、作品ページ内の譜面の中に`|skinType=default_add|`を指定すれば反映されるようになります。  


### 関連項目
- [スキンファイル仕様](./AboutSkin)
- [グラデーション仕様](./dos-c0001-gradation)

### 更新履歴

|Version|変更内容|
|----|----|
|[v34.0.0](https://github.com/cwtickle/danoniplus/releases/tag/v34.0.0)|・他譜面のデータを参照しつつ、個別のスキン設定を組み合わせる機能を追加<br>・キー別、キーパターン別のスキン設定を実装|
|[v33.0.0](https://github.com/cwtickle/danoniplus/releases/tag/v33.0.0)|・初回実装|

| [< 背景・マスクモーション](./dos-e0004-animationData) | **スキン変更** | [矢印・フリーズアローモーション >](./dos-e0005-motionData) |
