[← オブジェクトリファレンスに戻る](ObjectReferenceIndex)  

## g_loadObj

### 概要
- 外部ファイルの読み込み状態を表すオブジェクト。  
譜面ファイル名をプロパティに持ち、  
`true`であればプロパティで指定した名前のファイルが読み込めていることを表す。  
[loadScript](fnc-c0010-loadScript), [importCssFile](fnc-c0020-importCssFile), [preloadFile](fnc-c0019-preloadFile)関数で使用。
```javascript
g_loadObj[`score.txt`] = true;
```

### 生成タイミング
- `loadScript ()`呼び出し時（ファイルの読み込み）

### プロパティと生成値
- main (boolean): true
