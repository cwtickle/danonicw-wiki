**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-h0038-frzAttempt) | Japanese** 

[Home](./) &gt; [譜面ヘッダー仕様](dos_header) &gt; [プレイ時の初期設定](dos_header#-プレイ時の初期設定) &gt; [ライフゲージ・判定関連の設定](dos_header#ライフゲージ判定関連の設定)

| [<- frzStartjdgUse](dos-h0037-frzStartjdgUse) | **frzAttempt** | [excessiveJdgUse ->](dos-h0093-excessiveJdgUse) |

## frzAttempt
- フリーズアローヒット時の許容フレーム数の設定

### 使い方
```
|frzAttempt=7|
```
### 説明
フリーズアローヒット時に指を離したとき、  
ミス(イクナイ)にならない許容フレーム数を指定します。  
デフォルトは5フレーム。  

### 関連項目
- [customGauge](dos-h0053-customGauge) [:pencil:](dos-h0053-customGauge/_edit) カスタムゲージ設定
- [**gaugeX**](dos-h0022-gaugeX) [:pencil:](dos-h0022-gaugeX/_edit) ゲージ設定の詳細
- [frzStartjdgUse](dos-h0037-frzStartjdgUse) [:pencil:](dos-h0037-frzStartjdgUse/_edit) フリーズアロー開始判定の設定有無

### 更新履歴

|Version|変更内容|
|----|----|
|[v5.10.0](https://github.com/cwtickle/danoniplus/releases/tag/v5.10.0)|・初回実装|

| [<- frzStartjdgUse](dos-h0037-frzStartjdgUse) | **frzAttempt** | [excessiveJdgUse ->](dos-h0093-excessiveJdgUse) |