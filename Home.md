| [< Ruffleの使い方](./Ruffle) || **トップページ** || [用途別アクセスマップ >](./AccessMap) |

# Dancing☆Onigiri (CW Edition)  
-> English Version is [**HERE**](https://github.com/cwtickle/danoniplus-docs/wiki). * Some pages may have broken links.

## Dancing☆Onigiriとは？ / What's Dancing☆Onigiri?

![howtoplay2](https://github.com/cwtickle/danoniplus/wiki/wiki/howtoplay2.png)

Dancing☆Onigiri(通称ダンおに)とは、ブラウザで動作する**キーボードを使ったリズムゲーム**です。  
リズムや音に合わせて画面上を流れてくる矢印が、画面上に固定して表示されている矢印（ステップゾーン）と重なった瞬間に、
その矢印に対応するキーを押します。

2003年にO-ToroさんがFlashソースとして公開して以来、多くの作品が公開されています。  

ここで公開しているソースは、従来Cross Walkerで公開していた  
ParaFla!ソースの概念をベースとした**HTML5 (HTML Living Standard)版**です。  
オリジナルや従来のParaFla!ソースに比べ、さまざまな機能強化を行っています。詳細は[こちら](DifferenceFromFlashVer)。

*Dancing Onigiri*, commonly called *Dan Oni*, is **a rhythm game**   
using a keyboard with working a browser.  
Many works have been published since O-Toro published as Flash source in 2003.  

The source published here is its **HTML5 (HTML Living Standard)** version.  
Compared to the previous version, we have made various enhancements. Click [here](DifferenceFromFlashVer) for details.

## プレイ方法, 譜面作成方法 / How to Play, How to Make
詳細については右メニュー、[用途別アクセスマップ](AccessMap)もご覧ください。  
See also the sidebars for details.

- [ゲーム画面の説明](AboutGameSystem) / [About game system](https://github.com/cwtickle/danoniplus-docs/wiki/AboutGameSystem)
- [ゲーム内ショートカット](Shortcut) / [Shortcuts](https://github.com/cwtickle/danoniplus-docs/wiki/Shortcut)
- [譜面の作成概要](HowtoMake) / [How to make chart overview](https://github.com/cwtickle/danoniplus-docs/wiki/HowtoMake)
- [作品データのWeb公開方法](HowToFileUpload) / [How to publish your work data on the Web](https://github.com/cwtickle/danoniplus-docs/wiki/HowToFileUpload)

## Dancing☆Onigiriとその派生作品について / Dancing Onigiri Works

Dancing☆Onigiriでは5key/7keyを中心に、さまざまなスタイルのキー種（多鍵）があります。  
キー種によりステップゾーンに対応するキーが異なり、多様な遊び方ができることが特徴です。

There are various styles of key types (multi-key) centering on 5keys and 7keys in *Dancing Onigiri*.  
The key corresponding to the step zone differs depending on the key type, you can play in various ways.


|種類|対応キー数|
|----|----|
|[運指型](AboutKeys-fingering)|[5key](AboutKeys-fingering#5key) / [12key](AboutKeys-fingering#12key) / [14key](AboutKeys-fingering#14key) / [11ikey](AboutKeys-fingering#11ikey) / [17key](AboutKeys-fingering#17key) / [23key](AboutKeys-fingering#23key)|
|[バランス型](AboutKeys-balance)|[7key](AboutKeys-balance#7key) / [9Akey[ダブルプレイ]](AboutKeys-balance#9akey-ダブルプレイ) / [9Bkey](AboutKeys-balance#9bkey) / [7ikey](AboutKeys-balance#7ikey) / [9ikey](AboutKeys-balance#9ikey)|
|[指移動型](AboutKeys-finger-movement)|[11key](AboutKeys-finger-movement#11key) / [11Lkey](AboutKeys-finger-movement#11lkey) / [13key[トリプルプレイ]](AboutKeys-finger-movement#13key-トリプルプレイ) / [15Akey](AboutKeys-finger-movement#15akey) / [15Bkey](AboutKeys-finger-movement#15Bkey) / [14ikey](AboutKeys-finger-movement#14ikey) / [16ikey](AboutKeys-finger-movement#16ikey)|
|[スクラッチ型](AboutKeys-scratch)|[8key](AboutKeys-scratch#8key) / [11Wkey](AboutKeys-scratch#11wkey)|
|[オリジナル](AboutKeys-customKeys)|(Various Keys)|
|ダンおに以外|[Punching◇Panels (パンパネ)](https://github.com/cwtickle/punching-panels) / キリズマ|


## Dancing☆Onigiri History
例年、作品を集めたイベントが開催されています。詳細は下記のサイト一覧などのページをご覧ください。  
In previous years, events featuring the works have been held. For details, please see the following [*Dancing-onigiri-sites-list*](https://cw7.sakura.ne.jp/danonidb/).

<img src="https://user-images.githubusercontent.com/44026291/190275608-d9e32a3d-baa2-40d6-a5f2-1f9c03193bad.png" width="100%">

<img src="https://user-images.githubusercontent.com/44026291/190275665-bf6025b5-4075-4001-8332-798cdb2bac45.png" width="100%">

## まとめサイト  
- [Dancing☆Onigiri サイト一覧](https://cw7.sakura.ne.jp/danonidb/)  
- [Dancing☆Onigiri 作品一覧](https://cw7.sakura.ne.jp/lst/)  
- [Dancing☆Onigiri 難易度表 for.js](http://dodl4.g3.xrea.com/) 
- [多鍵データベース](https://apoi108.sakura.ne.jp/danoni/ta/)

| [< Ruffleの使い方](./Ruffle) || **トップページ** || [用途別アクセスマップ >](./AccessMap) |