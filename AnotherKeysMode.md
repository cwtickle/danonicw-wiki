**[English](https://github.com/cwtickle/danoniplus-docs/wiki/AnotherKeysMode) | Japanese** 

# 別キーモード / Another Key Mode
- Dancing☆Onigiri (CW Edition) では、対象のキーとは別のキーにコンバートして遊ぶことができるモードがあります。 
- 別キーモード中はキーコンフィグやColorType等を保存することはできません。  

## 対応キーの一覧 / Another Keys Mode List

|キー数<br>Keys|対応している別キー<br>Another Keys|
|----|----|
|7key|7ikey, 12key(下段)|
|8key|12key(下段)|
|9Akey|9Bkey, 11ikey|
|9Bkey|9Akey|
|11key|11Lkey, 12key|
|11Lkey|11key, 12key|
|11Wkey|12key|

## 別キーモード中の保存項目
- 本来のキーの設定を上書きしないよう、一部の設定に制限が掛かります。

|保存項目|本来のキー|別キーモード|
|----|----|----|
|Reverse|:heavy_check_mark:||
|Adjustment|:heavy_check_mark:|:heavy_check_mark:|
|Volume|:heavy_check_mark:|:heavy_check_mark:|
|Appearance|:heavy_check_mark:|:heavy_check_mark:|
|Opacity|:heavy_check_mark:|:heavy_check_mark:|
|HitPosition|:heavy_check_mark:|:heavy_check_mark:|
|ハイスコア|:heavy_check_mark:|:heavy_check_mark:|
|キーコンフィグ配列|:heavy_check_mark:||
|矢印・フリーズアロー色配列|:heavy_check_mark:||
|シャッフルグループ配列|:heavy_check_mark:||
|ColorType|:heavy_check_mark:||
|ShuffleGr.|:heavy_check_mark:||
|ColorGr.|:heavy_check_mark:||
|ShapeGr.|:heavy_check_mark:||

## 設定方法 / How to set
- キーコンフィグ画面にて KeyPattern を変更すると設定できます。  

<img src="https://user-images.githubusercontent.com/44026291/226273587-7804ca76-c1a5-490b-9f29-d8f370217d08.png" width="45%"><img src="https://user-images.githubusercontent.com/44026291/226273658-7afdc349-4e0c-4ae8-9d70-2f30eb80ab5f.png" width="45%">

### 関連項目 / Related Pages
- [キーの仕様について](./keys)
- [ゲーム画面の説明 (About Game System)](./AboutGameSystem)
- [ローカルストレージ仕様](./LocalStorage)

### 更新履歴 / History

|Version|変更内容|
|----|----|
|[v31.5.0](https://github.com/cwtickle/danoniplus/releases/tag/v31.5.0)|・別キーモードにおいてハイスコア及び一部の設定保存に対応|
|[v23.1.0](https://github.com/cwtickle/danoniplus/releases/tag/v23.1.0)|・7keyにおいて7ikeyを別キーとして遊べるよう変更|
|[v19.4.1](https://github.com/cwtickle/danoniplus/releases/tag/v19.4.1)|・8keyにおいて12keyを別キーとして遊べるよう変更|
|[v18.5.0](https://github.com/cwtickle/danoniplus/releases/tag/v18.5.0)|・9A/9Bkeyの別キーモード時の配色を一部変更|
|[v15.6.0](https://github.com/cwtickle/danoniplus/releases/tag/v15.6.0)|・7, 11, 11L, 11Wkeyにおいて12keyを別キーとして遊べるよう変更|
|[v4.8.0](https://github.com/cwtickle/danoniplus/releases/tag/v4.8.0)|・初期実装 (9A, 9B, 11, 11Lkey)|
