**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-h0083-customCreditWidth) | Japanese** 

[Home](./) &gt; [譜面ヘッダー仕様](dos_header) &gt; [プレイ時の初期設定](dos_header#-プレイ時の初期設定) &gt; [プレイ画面位置の設定](dos_header#プレイ画面位置の設定)

| [<- playingWidth / playingHeight](dos-h0071-playingWidth) | **customCreditWidth** | [scArea ->](dos-h0096-scArea) |

## customCreditWidth (customViewWidth)
- カスタムクレジットエリアの横幅設定

### 使い方
```
|customCreditWidth=150|
```
### 説明
プレイ画面のクレジット表示のうち、スコアや速度変化表示などカスタムを行っている理由で、クレジット表示を**被せたくない幅**をpx単位で指定できます。  
デフォルトは0pxです。ver32.2.2以前は設定ミスのため「customViewWidth」として設定してください。

<img src="./wiki/dos-h0083-01.png" width="80%">

### 関連項目
- [playingX](dos-h0070-playingX) [:pencil:](dos-h0070-playingX/_edit) ゲーム表示エリアのX座標
- [playingWidth](dos-h0071-playingWidth) [:pencil:](dos-h0071-playingWidth/_edit) ゲーム表示エリアの横幅

### 更新履歴

|Version|変更内容|
|----|----|
|[v23.4.0](https://github.com/cwtickle/danoniplus/releases/tag/v23.4.0)|・初回実装|

| [<- playingWidth / playingHeight](dos-h0071-playingWidth) | **customCreditWidth** | [scArea ->](dos-h0096-scArea) |