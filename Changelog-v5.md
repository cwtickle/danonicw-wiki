⭐ New Features / 🛠️ Improvements / 🐞 Bug Fixes

[**<- v6**](Changelog-v6) | **v5** | [**v4 ->**](Changelog-v4)  
(**🔖 [37 Releases](https://github.com/cwtickle/danoniplus/releases?q=tag%3Av5)** )

## [Warning] 更新終了後の不具合情報
- [更新を終了したバージョンの不具合情報#v5](DeprecatedVersionBugs#v5) を参照

## v5.12.17 ([2019-12-14](https://github.com/cwtickle/danoniplus/releases/tag/v5.12.17))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v5.12.17/total)](https://github.com/cwtickle/danoniplus/archive/v5.12.17.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v5.12.17/support/v5?style=social)](https://github.com/cwtickle/danoniplus/compare/v5.12.17...support/v5#files_bucket) 
- 🛠️ Localhost時にフレーム数を表示するように変更 ( PR [#561](https://github.com/cwtickle/danoniplus/pull/561) )
- 🛠️ ラベルのID重複を解消 ( PR [#545](https://github.com/cwtickle/danoniplus/pull/545), [#546](https://github.com/cwtickle/danoniplus/pull/546), [#562](https://github.com/cwtickle/danoniplus/pull/562) )
- 🛠️ fileでも起動可能なようにcrossorigin属性の付加条件を変更 ( PR [#564](https://github.com/cwtickle/danoniplus/pull/564) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v5.12.17)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v5.12.16...v5.12.17#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v5.12.17)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v5.12.17.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v5.12.17.tar.gz) )
/ 🎣 [**v11.0.0**](./Changelog-v11#v1100-2019-12-14)

## v5.12.16 ([2019-11-11](https://github.com/cwtickle/danoniplus/releases/tag/v5.12.16))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v5.12.16/total)](https://github.com/cwtickle/danoniplus/archive/v5.12.16.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v5.12.16/support/v5?style=social)](https://github.com/cwtickle/danoniplus/compare/v5.12.16...support/v5#files_bucket) 
- 🐞 MacOS, iOS (Safari)においてWeb Audio APIが動作しない問題を修正 ( PR [#523](https://github.com/cwtickle/danoniplus/pull/523), [#525](https://github.com/cwtickle/danoniplus/pull/525), [#527](https://github.com/cwtickle/danoniplus/pull/527) ) <- :boom: **initial**

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v5.12.16)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v5.12.14...v5.12.16#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v5.12.16)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v5.12.16.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v5.12.16.tar.gz) )
/ 🎣 [**v10.1.1**](./Changelog-v10#v1011-2019-11-11)

## v5.12.14 ([2019-10-12](https://github.com/cwtickle/danoniplus/releases/tag/v5.12.14))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v5.12.14/total)](https://github.com/cwtickle/danoniplus/archive/v5.12.14.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v5.12.14/support/v5?style=social)](https://github.com/cwtickle/danoniplus/compare/v5.12.14...support/v5#files_bucket) 
- 🐞 低速時かつフリーズアロー開始判定時にPF判定が早まる問題を修正 ( PR [#481](https://github.com/cwtickle/danoniplus/pull/481) ) <- :boom: [**v5.7.0**](Changelog-v5#v570-2019-06-01)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v5.12.14)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v5.12.13...v5.12.14#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v5.12.14)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v5.12.14.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v5.12.14.tar.gz) )
/ 🎣 [**v9.0.1**](./Changelog-v9#v901-2019-10-12)

## v5.12.13 ([2019-10-08](https://github.com/cwtickle/danoniplus/releases/tag/v5.12.13))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v5.12.13/total)](https://github.com/cwtickle/danoniplus/archive/v5.12.13.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v5.12.13/support/v5?style=social)](https://github.com/cwtickle/danoniplus/compare/v5.12.13...support/v5#files_bucket) 
- 🐞 速度変化が同一フレームにあると正常に動作しなくなる不具合を修正 ( PR [#477](https://github.com/cwtickle/danoniplus/pull/477) ) <- :boom: **initial**

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v5.12.13)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v5.12.12...v5.12.13#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v5.12.13)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v5.12.13.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v5.12.13.tar.gz) )
/ 🎣 [**v8.7.3**](./Changelog-v8#v873-2019-10-08)

## v5.12.12 ([2019-10-06](https://github.com/cwtickle/danoniplus/releases/tag/v5.12.12))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v5.12.12/total)](https://github.com/cwtickle/danoniplus/archive/v5.12.12.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v5.12.12/support/v5?style=social)](https://github.com/cwtickle/danoniplus/compare/v5.12.12...support/v5#files_bucket) 
- 🐞 エスケープ文字の適用順序誤りを修正 ( PR [#472](https://github.com/cwtickle/danoniplus/pull/472), [#473](https://github.com/cwtickle/danoniplus/pull/473) ) <- :boom: [**v0.66.x**](Changelog-v0#v066x-2018-11-16)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v5.12.12)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v5.12.11...v5.12.12#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v5.12.12)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v5.12.12.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v5.12.12.tar.gz) )
/ 🎣 [**v8.7.2**](./Changelog-v8#v872-2019-10-06)

## v5.12.11 ([2019-09-30](https://github.com/cwtickle/danoniplus/releases/tag/v5.12.11))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v5.12.11/total)](https://github.com/cwtickle/danoniplus/archive/v5.12.11.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v5.12.11/support/v5?style=social)](https://github.com/cwtickle/danoniplus/compare/v5.12.11...support/v5#files_bucket) 
- 🐞 エラーウィンドウが表示されない問題を修正 ( PR [#459](https://github.com/cwtickle/danoniplus/pull/459) ) <- :boom: [**v2.6.0**](Changelog-v2#v260-2019-02-10)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v5.12.11)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v5.12.10...v5.12.11#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v5.12.11)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v5.12.11.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v5.12.11.tar.gz) )
/ 🎣 [**v8.6.1**](./Changelog-v8#v861-2019-09-30)

## v5.12.10 ([2019-09-23](https://github.com/cwtickle/danoniplus/releases/tag/v5.12.10))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v5.12.10/total)](https://github.com/cwtickle/danoniplus/archive/v5.12.10.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v5.12.10/support/v5?style=social)](https://github.com/cwtickle/danoniplus/compare/v5.12.10...support/v5#files_bucket) 
- 🐞 ロード画面でEnterを押すと複数回ロードが発生する問題を修正 ( PR [#432](https://github.com/cwtickle/danoniplus/pull/432) ) <- :boom: **initial**
- 🐞 メイン画面で曲中リトライキーを連打した場合に譜面がずれることがある問題を修正 ( PR [#433](https://github.com/cwtickle/danoniplus/pull/433) ) <- :boom: **initial**

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v5.12.10)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v5.12.9...v5.12.10#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v5.12.10)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v5.12.10.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v5.12.10.tar.gz) )
/ 🎣 [**v8.0.4**](./Changelog-v8#v804-2019-09-23)

## v5.12.9 ([2019-09-16](https://github.com/cwtickle/danoniplus/releases/tag/v5.12.9))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v5.12.9/total)](https://github.com/cwtickle/danoniplus/archive/v5.12.9.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v5.12.9/support/v5?style=social)](https://github.com/cwtickle/danoniplus/compare/v5.12.9...support/v5#files_bucket) 
- 🐞 ゲームオーバー時の200ミリ秒遅延により、ハイスコア表示がおかしくなることがある問題を修正 ( PR [#428](https://github.com/cwtickle/danoniplus/pull/428) ) <- :boom: **initial**

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v5.12.9)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v5.12.8...v5.12.9#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v5.12.9)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v5.12.9.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v5.12.9.tar.gz) )
/ 🎣 [**v8.0.2**](./Changelog-v8#v802-2019-09-16)

## v5.12.8 ([2019-09-15](https://github.com/cwtickle/danoniplus/releases/tag/v5.12.8))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v5.12.8/total)](https://github.com/cwtickle/danoniplus/archive/v5.12.8.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v5.12.8/support/v5?style=social)](https://github.com/cwtickle/danoniplus/compare/v5.12.8...support/v5#files_bucket) 
- 🐞 速度変化が補正込みで負のフレームにあるときの不具合を修正 ( PR [#426](https://github.com/cwtickle/danoniplus/pull/426) ) <- :boom: **initial**

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v5.12.8)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v5.12.7...v5.12.8#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v5.12.8)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v5.12.8.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v5.12.8.tar.gz) )
/ 🎣 [**v8.0.1**](./Changelog-v8#v801-2019-09-15)

## v5.12.7 ([2019-08-03](https://github.com/cwtickle/danoniplus/releases/tag/v5.12.7))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v5.12.7/total)](https://github.com/cwtickle/danoniplus/archive/v5.12.7.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v5.12.7/support/v5?style=social)](https://github.com/cwtickle/danoniplus/compare/v5.12.7...support/v5#files_bucket) 
- 🐞 back_data, mask_dataで0フレームが指定された場合に動作しないことがある問題を修正 ( PR [#388](https://github.com/cwtickle/danoniplus/pull/388) ) <- :boom: [**v4.2.0**](Changelog-v4#v420-2019-04-30), [**v0.65.x**](Changelog-v0#v065x-2018-11-16)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v5.12.7)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v5.12.6...v5.12.7#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v5.12.7)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v5.12.7.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v5.12.7.tar.gz) )
/ 🎣 [**v7.5.1**](./Changelog-v7#v751-2019-08-03)

## v5.12.6 ([2019-07-21](https://github.com/cwtickle/danoniplus/releases/tag/v5.12.6))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v5.12.6/total)](https://github.com/cwtickle/danoniplus/archive/v5.12.6.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v5.12.6/support/v5?style=social)](https://github.com/cwtickle/danoniplus/compare/v5.12.6...support/v5#files_bucket) 
- 🐞 リザルト画面で、フェードアウト中にTitleBack/Retryするとフェードアウト状態が引き継がれてしまう問題を修正 ( PR [#384](https://github.com/cwtickle/danoniplus/pull/384) ) <- :boom: [**v0.68.x**](Changelog-v0#v068x-2018-11-17)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v5.12.6)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v5.12.5...v5.12.6#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v5.12.6)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v5.12.6.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v5.12.6.tar.gz) )
/ 🎣 [**v7.4.0**](./Changelog-v7#v740-2019-07-21)

## v5.12.5 ([2019-07-08](https://github.com/cwtickle/danoniplus/releases/tag/v5.12.5))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v5.12.5/total)](https://github.com/cwtickle/danoniplus/archive/v5.12.5.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v5.12.5/support/v5?style=social)](https://github.com/cwtickle/danoniplus/compare/v5.12.5...support/v5#files_bucket) 
- 🐞 変更したキーコンフィグ内容が反映されないことがある問題を修正 ( PR [#365](https://github.com/cwtickle/danoniplus/pull/365) ) <- :boom: [**v4.6.0**](Changelog-v4#v460-2019-05-03)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v5.12.5)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v5.12.4...v5.12.5#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v5.12.5)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v5.12.5.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v5.12.5.tar.gz) )
/ 🎣 [**v6.6.0**](./Changelog-v6#v660-2019-07-08)

## v5.12.4 ([2019-07-06](https://github.com/cwtickle/danoniplus/releases/tag/v5.12.4))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v5.12.4/total)](https://github.com/cwtickle/danoniplus/archive/v5.12.4.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v5.12.4/support/v5?style=social)](https://github.com/cwtickle/danoniplus/compare/v5.12.4...support/v5#files_bucket) 
- 🐞 フェードイン時に個別加速が掛からない問題を修正 ( PR [#363](https://github.com/cwtickle/danoniplus/pull/363) ) <- :boom: **initial**

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v5.12.4)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v5.12.3...v5.12.4#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v5.12.4)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v5.12.4.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v5.12.4.tar.gz) )
/ 🎣 [**v6.5.1**](./Changelog-v6#v651-2019-07-06)

## v5.12.3 ([2019-06-26](https://github.com/cwtickle/danoniplus/releases/tag/v5.12.3))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v5.12.3/total)](https://github.com/cwtickle/danoniplus/archive/v5.12.3.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v5.12.3/support/v5?style=social)](https://github.com/cwtickle/danoniplus/compare/v5.12.3...support/v5#files_bucket) 
- 🐞 通常キー（キーコンフィグ保存済み）が1譜面目、独自キーが2譜面目の場合に譜面の読み込みやキーコンフィグの設定ができない問題を修正 ( PR [#355](https://github.com/cwtickle/danoniplus/pull/355) ) <- :boom: [**v4.6.0**](Changelog-v4#v460-2019-05-03)
- 🐞 `danoni_setting.js` の `g_presetGaugeCustom` において0が指定できない問題を修正 ( PR [#355](https://github.com/cwtickle/danoniplus/pull/355) ) <- :boom: [**v4.6.0**](Changelog-v4#v460-2019-05-03)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v5.12.3)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v5.12.2...v5.12.3#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v5.12.3)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v5.12.3.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v5.12.3.tar.gz) )
/ 🎣 [**v6.2.1**](./Changelog-v6#v621-2019-06-26)

----

## v5.12.2 ([2019-06-18](https://github.com/cwtickle/danoniplus/releases/tag/v5.12.2))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v5.12.2/total)](https://github.com/cwtickle/danoniplus/archive/v5.12.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v5.12.2/support/v5?style=social)](https://github.com/cwtickle/danoniplus/compare/v5.12.2...support/v5#files_bucket) 
- 🐞 フリーズアローのみの譜面が再生できない問題を修正 ( PR [#343](https://github.com/cwtickle/danoniplus/pull/343) ) <- :boom: **initial**

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v5.12.2)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v5.12.1...v5.12.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v5.12.2)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v5.12.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v5.12.2.tar.gz) )

## v5.12.1 ([2019-06-17](https://github.com/cwtickle/danoniplus/releases/tag/v5.12.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v5.12.1/total)](https://github.com/cwtickle/danoniplus/archive/v5.12.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v5.12.1/support/v5?style=social)](https://github.com/cwtickle/danoniplus/compare/v5.12.1...support/v5#files_bucket) 
- 🛠️ フリーズアローの枠外判定の修正・見直し（条件により止まってしまう問題の修正）( PR [#341](https://github.com/cwtickle/danoniplus/pull/341) )
- 🐞 矢印の内側を塗りつぶす機能 ( PR [#340](https://github.com/cwtickle/danoniplus/pull/340) ) で、全体色変化時に矢印内側を含む全体が塗りつぶされる問題を修正 ( PR [#341](https://github.com/cwtickle/danoniplus/pull/341) ) <- :boom: [**v5.12.0**](Changelog-v5#v5120-2019-06-14)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v5.12.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v5.12.0...v5.12.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v5.12.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v5.12.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v5.12.1.tar.gz) )<br>❤️ ぷろろーぐ (@prlg25), izkdic, すずめ (@suzme)

## v5.12.0 ([2019-06-14](https://github.com/cwtickle/danoniplus/releases/tag/v5.12.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v5.12.0/total)](https://github.com/cwtickle/danoniplus/archive/v5.12.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v5.12.0/support/v5?style=social)](https://github.com/cwtickle/danoniplus/compare/v5.12.0...support/v5#files_bucket) 
- ⭐️ 通常矢印の内側を塗りつぶす機能を追加 ( PR [#339](https://github.com/cwtickle/danoniplus/pull/339), [#340](https://github.com/cwtickle/danoniplus/pull/340) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v5.12.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v5.11.0...v5.12.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v5.12.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v5.12.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v5.12.0.tar.gz) )<br>❤️ MFV2 (@MFV2)

## v5.11.0 ([2019-06-10](https://github.com/cwtickle/danoniplus/releases/tag/v5.11.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v5.11.0/total)](https://github.com/cwtickle/danoniplus/archive/v5.11.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v5.11.0/support/v5?style=social)](https://github.com/cwtickle/danoniplus/compare/v5.11.0...support/v5#files_bucket) 
- ⭐️ PageUp/PageDown/End/Homeをキーブロック対象に追加 ( PR [#336](https://github.com/cwtickle/danoniplus/pull/336) )
- ⭐️ キー毎にタイトルバック、リトライ用ショートカットキーの指定を行えるように変更 ( PR [#337](https://github.com/cwtickle/danoniplus/pull/337) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v5.11.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v5.10.0...v5.11.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v5.11.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v5.11.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v5.11.0.tar.gz) )<br>❤️ apoi, すずめ (@suzme)

## v5.10.0 ([2019-06-09](https://github.com/cwtickle/danoniplus/releases/tag/v5.10.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v5.10.0/total)](https://github.com/cwtickle/danoniplus/archive/v5.10.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v5.10.0/support/v5?style=social)](https://github.com/cwtickle/danoniplus/compare/v5.10.0...support/v5#files_bucket) 
- ⭐️ フリーズアローの許容フレームを譜面ヘッダーから指定できるように変更 ( PR [#333](https://github.com/cwtickle/danoniplus/pull/333) )
- ⭐️ プレイ中のショートカットキーを設定できるように変更 ( PR [#334](https://github.com/cwtickle/danoniplus/pull/334) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v5.10.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v5.9.0...v5.10.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v5.10.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v5.10.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v5.10.0.tar.gz) )<br>❤️ izkdic, MFV2 (@MFV2)

## v5.9.0 ([2019-06-09](https://github.com/cwtickle/danoniplus/releases/tag/v5.9.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v5.9.0/total)](https://github.com/cwtickle/danoniplus/archive/v5.9.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v5.9.0/support/v5?style=social)](https://github.com/cwtickle/danoniplus/compare/v5.9.0...support/v5#files_bucket) 
- ⭐️ 特殊キーの折り返し位置（下段終了位置）を指定できるように変更 ( PR [#331](https://github.com/cwtickle/danoniplus/pull/331) )
- 🛠️ キーコンフィグ周りのコードを整理 ( PR [#331](https://github.com/cwtickle/danoniplus/pull/331) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v5.9.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v5.8.1...v5.9.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v5.9.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v5.9.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v5.9.0.tar.gz) )

## v5.8.1 ([2019-06-08](https://github.com/cwtickle/danoniplus/releases/tag/v5.8.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v5.8.1/total)](https://github.com/cwtickle/danoniplus/archive/v5.8.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v5.8.1/support/v5?style=social)](https://github.com/cwtickle/danoniplus/compare/v5.8.1...support/v5#files_bucket) 
- 🐞 ステップゾーン位置がずれている問題を修正 ( PR [#330](https://github.com/cwtickle/danoniplus/pull/330) ) <- :boom: **initial**

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v5.8.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v5.8.0...v5.8.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v5.8.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v5.8.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v5.8.1.tar.gz) )<br>❤️ izkdic

## v5.8.0 ([2019-06-01](https://github.com/cwtickle/danoniplus/releases/tag/v5.8.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v5.8.0/total)](https://github.com/cwtickle/danoniplus/archive/v5.8.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v5.8.0/support/v5?style=social)](https://github.com/cwtickle/danoniplus/compare/v5.8.0...support/v5#files_bucket) 
- ⭐️ 譜面データのURIデコードに対応 ( PR [#328](https://github.com/cwtickle/danoniplus/pull/328) )
- ⭐️ 譜面データの&区切り可否の切替機能を追加 ( PR [#328](https://github.com/cwtickle/danoniplus/pull/328) )
- 🛠️ setVal関数のboolean対応 ( PR [#328](https://github.com/cwtickle/danoniplus/pull/328) )
- 🛠️ 外部dos読み込み処理中の&を|に置き換えるreplaceを削除 ( PR [#328](https://github.com/cwtickle/danoniplus/pull/328) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v5.8.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v5.7.0...v5.8.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v5.8.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v5.8.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v5.8.0.tar.gz) )<br>❤️ すずめ (@suzme), izkdic

## v5.7.0 ([2019-06-01](https://github.com/cwtickle/danoniplus/releases/tag/v5.7.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v5.7.0/total)](https://github.com/cwtickle/danoniplus/archive/v5.7.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v5.7.0/support/v5?style=social)](https://github.com/cwtickle/danoniplus/compare/v5.7.0...support/v5#files_bucket) 
- ⭐️ フリーズアロー開始判定の実装 ( PR [#324](https://github.com/cwtickle/danoniplus/pull/324) )
- ⭐️ 見かけフレーム数(g_scoreObj.baseFrame)の実装 ( PR [#324](https://github.com/cwtickle/danoniplus/pull/324) )
- 🛠️ デフォルト背景画像描画のスクリプトを集約 ( PR [#325](https://github.com/cwtickle/danoniplus/pull/325) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v5.7.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v5.6.6...v5.7.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v5.7.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v5.7.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v5.7.0.tar.gz) )<br>❤️ MFV2 (@MFV2), izkdic

## v5.6.6 ([2019-05-31](https://github.com/cwtickle/danoniplus/releases/tag/v5.6.6))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v5.6.6/total)](https://github.com/cwtickle/danoniplus/archive/v5.6.6.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v5.6.6/support/v5?style=social)](https://github.com/cwtickle/danoniplus/compare/v5.6.6...support/v5#files_bucket) 
- 🐞 歌詞表示のフェードイン・アウトのモーションが想定より早くなる問題を修正 ( PR [#321](https://github.com/cwtickle/danoniplus/pull/321) ) <- :boom: **initial**
- 🐞 判定基準位置が1フレーム手前になっている問題を修正 ( PR [#318](https://github.com/cwtickle/danoniplus/pull/318), [#323](https://github.com/cwtickle/danoniplus/pull/323) ) <- :boom: **initial**

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v5.6.6)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v5.6.1...v5.6.6#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v5.6.6)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v5.6.6.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v5.6.6.tar.gz) )<br>❤️ izkdic

## v5.6.1 ([2019-05-27](https://github.com/cwtickle/danoniplus/releases/tag/v5.6.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v5.6.1/total)](https://github.com/cwtickle/danoniplus/archive/v5.6.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v5.6.1/support/v5?style=social)](https://github.com/cwtickle/danoniplus/compare/v5.6.1...support/v5#files_bucket) 
- 🐞 譜面変更ボタン(setDifficulty)のカスタム関数で条件により通過できない不具合を修正 ( PR [#317](https://github.com/cwtickle/danoniplus/pull/317) ) <- :boom: [**v5.6.0**](Changelog-v5#v560-2019-05-26)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v5.6.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v5.6.0...v5.6.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v5.6.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v5.6.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v5.6.1.tar.gz) )

## v5.6.0 ([2019-05-26](https://github.com/cwtickle/danoniplus/releases/tag/v5.6.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v5.6.0/total)](https://github.com/cwtickle/danoniplus/archive/v5.6.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v5.6.0/support/v5?style=social)](https://github.com/cwtickle/danoniplus/compare/v5.6.0...support/v5#files_bucket) 
- ⭐️ 歌詞フェードイン・アウトの時間の可変対応 ( PR [#315](https://github.com/cwtickle/danoniplus/pull/315) )
- ⭐️ 歌詞データのフェードイン・アウトのデフォルト時間を30→60Frameに変更 ( PR [#315](https://github.com/cwtickle/danoniplus/pull/315) )
- ⭐️ 譜面変更ボタン(setDifficulty)に対してカスタム関数を追加 ( PR [#316](https://github.com/cwtickle/danoniplus/pull/316) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v5.6.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v5.4.0...v5.6.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v5.6.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v5.6.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v5.6.0.tar.gz) )<br>❤️ MFV2 (@MFV2), izkdic, 蒼宮野あいすかぁびぃ４号

## v5.4.0 ([2019-05-26](https://github.com/cwtickle/danoniplus/releases/tag/v5.4.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v5.4.0/total)](https://github.com/cwtickle/danoniplus/archive/v5.4.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v5.4.0/support/v5?style=social)](https://github.com/cwtickle/danoniplus/compare/v5.4.0...support/v5#files_bucket) 
- ⭐️ 歌詞表示(word_data)の複数階層化に対応 ( PR [#311](https://github.com/cwtickle/danoniplus/pull/311), Issue [#307](https://github.com/cwtickle/danoniplus/pull/307) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v5.4.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v5.3.0...v5.4.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v5.4.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v5.4.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v5.4.0.tar.gz) )<br>❤️ izkdic

## v5.3.0 ([2019-05-26](https://github.com/cwtickle/danoniplus/releases/tag/v5.3.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v5.3.0/total)](https://github.com/cwtickle/danoniplus/archive/v5.3.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v5.3.0/support/v5?style=social)](https://github.com/cwtickle/danoniplus/compare/v5.3.0...support/v5#files_bucket) 
- 🛠️ 曲名タイトルのline-heightの指定値が小さい場合に文字欠けする問題を修正 ( PR [#309](https://github.com/cwtickle/danoniplus/pull/309) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v5.3.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v5.2.2...v5.3.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v5.3.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v5.3.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v5.3.0.tar.gz) )

## v5.2.2 ([2019-05-26](https://github.com/cwtickle/danoniplus/releases/tag/v5.2.2))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v5.2.2/total)](https://github.com/cwtickle/danoniplus/archive/v5.2.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v5.2.2/support/v5?style=social)](https://github.com/cwtickle/danoniplus/compare/v5.2.2...support/v5#files_bucket) 
- 🐞 譜面ヘッダー：titlesizeを64以上に指定すると文字欠けが起こる問題を修正 ( PR [#308](https://github.com/cwtickle/danoniplus/pull/308) ) <- :boom: [**v3.5.0**](Changelog-v3#v350-2019-03-23)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v5.2.2)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v5.2.1...v5.2.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v5.2.2)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v5.2.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v5.2.2.tar.gz) )<br>❤️ izkdic

## v5.2.1 ([2019-05-21](https://github.com/cwtickle/danoniplus/releases/tag/v5.2.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v5.2.1/total)](https://github.com/cwtickle/danoniplus/archive/v5.2.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v5.2.1/support/v5?style=social)](https://github.com/cwtickle/danoniplus/compare/v5.2.1...support/v5#files_bucket) 
- 🐞 back_data, mask_dataのフレーム数が小さすぎる場合に画面が止まる問題を修正 ( PR [#305](https://github.com/cwtickle/danoniplus/pull/305) ) <- :boom: [**v2.4.0**](Changelog-v2#v240-2019-02-08) 
- 🐞 既存キーの譜面から特殊キーへ移るときにリバースの設定がリセットされない問題を修正 ( PR [#305](https://github.com/cwtickle/danoniplus/pull/305) ) <- :boom: [**v4.6.0**](Changelog-v4#v460-2019-05-03)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v5.2.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v5.2.0...v5.2.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v5.2.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v5.2.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v5.2.1.tar.gz) )

## v5.2.0 ([2019-05-20](https://github.com/cwtickle/danoniplus/releases/tag/v5.2.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v5.2.0/total)](https://github.com/cwtickle/danoniplus/archive/v5.2.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v5.2.0/support/v5?style=social)](https://github.com/cwtickle/danoniplus/compare/v5.2.0...support/v5#files_bucket) 
- ⭐️ 縦方向のスケーリングに対応 ( PR [#301](https://github.com/cwtickle/danoniplus/pull/301) )  
- ⭐️ 最低・最高速度を譜面ヘッダーから設定できるように変更 ( PR [#302](https://github.com/cwtickle/danoniplus/pull/302), issue [#300](https://github.com/cwtickle/danoniplus/pull/300) ) 
- 🐞 初期倍速が速度配列に存在しない場合に速度がundefinedとなる問題を修正 ( PR [#302](https://github.com/cwtickle/danoniplus/pull/302) ) <- :boom: [**v5.0.0**](Changelog-v5#v500-2019-05-16)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v5.2.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v5.1.1...v5.2.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v5.2.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v5.2.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v5.2.0.tar.gz) )<br>❤️ izkdic

## v5.1.1 ([2019-05-19](https://github.com/cwtickle/danoniplus/releases/tag/v5.1.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v5.1.1/total)](https://github.com/cwtickle/danoniplus/archive/v5.1.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v5.1.1/support/v5?style=social)](https://github.com/cwtickle/danoniplus/compare/v5.1.1...support/v5#files_bucket) 
- 🐞 LocalStorageが設定されていないキーのときにReverseが初期化されない問題を修正 ( PR [#299](https://github.com/cwtickle/danoniplus/pull/299) ) <- :boom: [**v4.6.0**](Changelog-v4#v460-2019-05-03)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v5.1.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v5.1.0...v5.1.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v5.1.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v5.1.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v5.1.1.tar.gz) )

## v5.1.0 ([2019-05-18](https://github.com/cwtickle/danoniplus/releases/tag/v5.1.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v5.1.0/total)](https://github.com/cwtickle/danoniplus/archive/v5.1.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v5.1.0/support/v5?style=social)](https://github.com/cwtickle/danoniplus/compare/v5.1.0...support/v5#files_bucket) 
- ⭐️ タイトル（デフォルト）の行別グラデーションに対応 ( PR [#297](https://github.com/cwtickle/danoniplus/pull/297) ) 
- ⭐️ 譜面ヘッダー「titlegrd」について、他の譜面ヘッダーと同様に「0x」「#」始まり両方のカラーコードに対応 ( PR [#297](https://github.com/cwtickle/danoniplus/pull/297) ) 

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v5.1.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v5.0.3...v5.1.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v5.1.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v5.1.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v5.1.0.tar.gz) )<br>❤️ izkdic

## v5.0.3 ([2019-05-18](https://github.com/cwtickle/danoniplus/releases/tag/v5.0.3))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v5.0.3/total)](https://github.com/cwtickle/danoniplus/archive/v5.0.3.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v5.0.3/support/v5?style=social)](https://github.com/cwtickle/danoniplus/compare/v5.0.3...support/v5#files_bucket) 
- 🐞 LocalStorageのリバース設定が元に戻ってしまう問題を修正 ( PR [#296](https://github.com/cwtickle/danoniplus/pull/296) ) <- :boom: [**v5.0.0**](Changelog-v5#v500-2019-05-16)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v5.0.3)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v5.0.2...v5.0.3#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v5.0.3)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v5.0.3.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v5.0.3.tar.gz) )

## v5.0.2 ([2019-05-17](https://github.com/cwtickle/danoniplus/releases/tag/v5.0.2))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v5.0.2/total)](https://github.com/cwtickle/danoniplus/archive/v5.0.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v5.0.2/support/v5?style=social)](https://github.com/cwtickle/danoniplus/compare/v5.0.2...support/v5#files_bucket) 
- 🐞 特殊キーが既存キーと同一の場合に、設定画面で止まる問題を修正 ( PR [#295](https://github.com/cwtickle/danoniplus/pull/295) ) <- :boom: [**v4.6.0**](Changelog-v4#v460-2019-05-03) 

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v5.0.2)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v5.0.1...v5.0.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v5.0.2)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v5.0.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v5.0.2.tar.gz) )

## v5.0.1 ([2019-05-17](https://github.com/cwtickle/danoniplus/releases/tag/v5.0.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v5.0.1/total)](https://github.com/cwtickle/danoniplus/archive/v5.0.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v5.0.1/support/v5?style=social)](https://github.com/cwtickle/danoniplus/compare/v5.0.1...support/v5#files_bucket) 
- 🛠️ 音量設定の 0.25% を廃止
- 🐞 設定画面のゲージ設定において、カーソルを動かすと詳細表示が1つずつずれる問題を修正 ( PR [#294](https://github.com/cwtickle/danoniplus/pull/294) , Issue [#293](https://github.com/cwtickle/danoniplus/pull/293) ) <- :boom: [**v5.0.0**](Changelog-v5#v500-2019-05-16) 
- 🐞 音量設定のカーソル方向が逆になっていた問題を修正 ( PR [#294](https://github.com/cwtickle/danoniplus/pull/294) ) <- :boom: [**v5.0.0**](Changelog-v5#v500-2019-05-16) 

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v5.0.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v5.0.0...v5.0.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v5.0.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v5.0.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v5.0.1.tar.gz) )

## v5.0.0 ([2019-05-16](https://github.com/cwtickle/danoniplus/releases/tag/v5.0.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v5.0.0/total)](https://github.com/cwtickle/danoniplus/archive/v5.0.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v5.0.0/support/v5?style=social)](https://github.com/cwtickle/danoniplus/compare/v5.0.0...support/v5#files_bucket) 
- 🛠️ 設定画面のスクリプトを整理 ( PR [#291](https://github.com/cwtickle/danoniplus/pull/291) ) 

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v5.0.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v4.10.1...v5.0.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v5.0.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v5.0.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v5.0.0.tar.gz) )

[**<- v6**](Changelog-v6) | **v5** | [**v4 ->**](Changelog-v4)