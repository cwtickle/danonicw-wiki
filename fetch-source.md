**Japanese only** 

| [< Gitクライアントを使ったFork、Pull Requestの方法](./fork-pull-request) | **Pull Requestに上がっているソースの取得方法** || [Tips一覧 >](./tips-index) |

# Pull Requestに上がっているソースの取得方法
- GitHubのサイトでPull Requestに上がっているソースをダウンロードしたい場合、  
[GitHub Desktop](https://desktop.github.com/)を使うのがお手軽です。Fork不要。
- GitHubアカウントの作成が必要です。詳細は[ヘルプ](https://docs.github.com/ja/get-started/signing-up-for-github)などをご覧ください。

## 1. GitHub Desktopのダウンロード、インストール
- 下記サイトよりダウンロード、インストールします。  
https://desktop.github.com/

## 2. GitHub Desktopを開く
- GitHubにログインしたら、下記の画面が表示されます。
<img src="https://github.com/cwtickle/danoniplus/assets/44026291/4205911c-1388-426e-87f3-163b5302beb1" width="80%">

## 3. リポジトリをクローン（初回のみ）
- 上の画像のように「Clone a repository from the Internet...」を選択します。
- 「Clone a repository」の子画面が表示されるので、「URL」タブを押してレポジトリ名に「cwtickle/danoniplus」と入れます。
- 「Local path」の場所が、実際のソース格納場所です。必要に応じて見直してください。  
問題なければ、「Clone」を押して開始します。
<img src="https://github.com/cwtickle/danoniplus/assets/44026291/62d4cb60-7c67-4696-8bed-c58edd16540b" width="80%">

<img src="https://github.com/cwtickle/danoniplus/assets/44026291/fe412acd-4090-4dd6-9763-fea72c10e7c1" width="80%">

## 4. 参照したいブランチ・Pull Requestを選択してソースを差し替え
- Cloneが完了すると、下記の画面になります。（次回起動時以降もこの画面）
<img src="https://github.com/cwtickle/danoniplus/assets/44026291/d230207d-53dc-4012-a485-975ed136c70a" width="80%">

- Current branchを押すと、ブランチの一覧が表示されます。
- Pull Requestがあれば、Branchesタブの横に「Pull Request (1)」のように表示されます。
<img src="https://github.com/cwtickle/danoniplus/assets/44026291/ba098408-637b-40c5-8b2c-3dd6472c8b62" width="80%">

- Pull Requestタブへ移動し、参照したい対象を選択します。
<img src="https://github.com/cwtickle/danoniplus/assets/44026291/aaec3e49-2495-485c-a442-f9ce88de3779" width="80%">

- 自動でFetchが始まります。下記のように、Current branchがPull Request番号になれば切替完了です。
<img src="https://github.com/cwtickle/danoniplus/assets/44026291/cfd9cf3b-afd9-4a1f-9eea-55560d7443b3" width="80%">

- 3.で指定したフォルダに移動すると、フォルダ一式があることが確認できます。
<img src="https://github.com/cwtickle/danoniplus/assets/44026291/fd0e444f-dac1-46cf-8bab-a11763b10879" width="60%">

## 補足: Pull Requestに変化があった場合
- 変化があった場合、下記のように青いボタン（Pull ....）が出ます。このボタンを押せば、最新化されます。  
<img src="https://github.com/cwtickle/danoniplus/assets/44026291/30e1f00b-f017-4403-96d6-16ec4b7ac9f3" width="60%">

<img src="https://github.com/cwtickle/danoniplus/assets/44026291/ee3e911f-904f-4ffe-80bb-84e731609395" width="60%">

| [< Gitクライアントを使ったFork、Pull Requestの方法](./fork-pull-request) | **Pull Requestに上がっているソースの取得方法** || [Tips一覧 >](./tips-index) |
