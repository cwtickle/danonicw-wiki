**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-e0004-animationData) | Japanese** 

[↑ 譜面エフェクト仕様に戻る](dos_effect)

| [< 歌詞表示](dos-e0003-wordData) | **背景・マスクモーション** | [スキン変更 >](dos-e0008-styleData) |

## 背景・マスクモーション (back_data, mask_data)
### 概要
- 画面の背景・マスク部分に表示する画像やテキスト、アニメーションを定義します。

<img src="https://user-images.githubusercontent.com/44026291/216012450-d4f35d03-4ee5-4e52-a83c-5d8e97ea0ebf.png" width="70%">

### 使い方／記述仕様
- 基本的には`back_data`が背景層、`mask_data`が画面上部を覆うマスク層として定義しています。  
背景層・マスク層それぞれが多層構造を持つことでさまざまな表現が可能です。
- 記述方法は`back_data`、`mask_data`共に1行1セット。改行区切りです。  
それぞれはカンマ区切りで、最初の2つ以外は省略が可能です。  
- 設定により、`<img>`タグ版と`<span>`タグ版、色付きオブジェクトなど6種類に分けられます。
- メイン画面以外のモーションとして、タイトル画面用・結果画面用の`backtitle_data`や`backresult_data`などがあります。これらはメイン画面の機能に加えて、フレーム移動やループ操作ができるようになっています。

#### 1. `<img>`タグ版（画像）
- 静止画像の場合
```
|mask_data=
400,0,../img/frzbar.png,,0,250,500,250,1
700,0
|
```
- アニメーション画像の場合
```
|back_data=
200,0,../img/c_600.png,,20,20,200,200,0.5,leftToRightFade,120,forwards
200,1,../img/iyo_600.png,,220,220,200,200,1,spinY,120,forwards
300,0
|
```
|番号|論理名|設定例|内容|
|----|----|----|----|
|1|Frame|200|変更するタイミングのフレーム数を指定します。|
|2|Depth|0|背景の深度を数字で指定します。0以上の値を指定。<br>深度は大きくすることもできますが、その分背景スプライトの数が増えるので重くなる可能性があります。|
|3|**Img Source**|../img/c_600.png|表示する画像のパスを指定します。相対パス可。<br>画像はpng/jpg/gif/bmpのいずれかを拡張子付きで指定します。|
|4|**Img Class**|imgMotion|imgタグにつけるCSSのClassを設定します。以下で設定が足りない場合などに使用します。|
|5|Left(X)|25|画像を表示するX座標を指定します。|
|6|Top(Y)|30|画像を表示するY座標を指定します。|
|7|**Width(X)**|200|画像の横サイズをpxで指定します。デフォルトは画像自体の横サイズです。|
|8|**Height(Y)**|200|画像の縦サイズをpxで指定します。デフォルトは画像自体の縦サイズです。|
|9|Opacity|0.5|画像の透明度を0～1の間で指定します。デフォルトは1(透明度なし)。<br>CSSアニメーション後、表示を消したいときは0を指定するとアニメーション後に画像が消えます。|
|10|Animation-Name|leftToRightFade|CSSアニメーション名を指定します。内容はCSSで定義します。<br>デフォルトはnone(なし)です。|
|11|Animation-Duration|120|CSSアニメーションを動かす間隔をフレーム数で指定します。|
|12|Animation-Fill-Mode|forwards|CSSアニメーション終了時にどのように表示するかを設定します。<br>　`forwards`: アニメーション終了時に最後の@keyframesの「100%(to)」のスタイルを適用<br>　`backwards`: アニメーション完了後、最初の@keyframes「0%(from)」のスタイルを適用<br>デフォルトは未指定です。|

#### 2. `<span>`タグ版（テキスト）
```
|back_data=
400,2,文字のフローテスト,,100,140,36,0,0,fromBig,100,forwards
500,2,文字のフローテスト,,100,140,36,0,0,upToDown,100,forwards
600,2,文字のフローテスト,,100,140,36,0,1,spinX,100,forwards
700,2,文字のフローテスト,,100,140,36,0,1,spinY,100,forwards
800,2,文字のフローテスト,,100,140,36,0,1,spinZ,100,forwards
900,2,文字のフローテスト,,100,140,36,0,1
950,2
|
```

|番号|論理名|設定例|内容|
|----|----|----|----|
|1|Frame|200|変更するタイミングのフレーム数を指定します。|
|2|Depth|0|背景の深度を数字で指定します。0以上の値を指定。<br>深度は大きくすることもできますが、その分背景スプライトの数が増えるので重くなる可能性があります。|
|3|**Display**|フローテスト|画像のパスではなく、文字を入れると&lt;span&gt;タグとして扱います。HTMLタグが使用できます。|
|4|**Span Class**|spanMotion|spanタグにつけるCSSのClassを設定します。以下で設定が足りない場合などに使用します。|
|5|Left(X)|25|文字を表示するX座標を指定します。|
|6|Top(Y)|30|文字を表示するY座標を指定します。|
|7|**FontSize**|36|画像の横サイズをpxで指定します。デフォルトは画面の横サイズです。<br>(画像自体の幅ではありません)|
|8|**FontColor**|#ffff00|文字の色をカラーコードで指定します。|
|9|Opacity|0.5|画像の透明度を0～1の間で指定します。デフォルトは1(透明度なし)。<br>CSSアニメーション後、表示を消したいときは0を指定するとアニメーション後に画像が消えます。|
|10|Animation-Name|leftToRightFade|CSSアニメーション名を指定します。内容はCSSで定義します。<br>デフォルトはnone(なし)です。|
|11|Animation-Duration|120|CSSアニメーションを動かす間隔をフレーム数で指定します。|
|12|Animation-Fill-Mode|forwards|CSSアニメーション終了時にどのように表示するかを設定します。<br>　`forwards`: アニメーション終了時に最後の@keyframesの「100%(to)」のスタイルを適用<br>　`backwards`: アニメーション完了後、最初の@keyframes「0%(from)」のスタイルを適用<br>デフォルトは未指定です。|

#### 3. 色付きオブジェクト
- 色付きオブジェクトの概要については[こちらのページ](./AboutColorObject)をご覧ください。
```
|backtitle_data=
330,3,[c]giko/#ff9999:yellow,,5,200,200,200,0,spinY,120,forwards
480,3
|
```
|番号|論理名|設定例|内容|
|----|----|----|----|
|1|Frame|200|変更するタイミングのフレーム数を指定します。|
|2|Depth|0|背景の深度を数字で指定します。0以上の値を指定。<br>深度は大きくすることもできますが、その分背景スプライトの数が増えるので重くなる可能性があります。|
|3|**ColorObject/Color**|[c]giko/#ff9999|`[c]色付きオブジェクト名:回転量/カラーコード` の形式で指定します。<br>色付きオブジェクト名は`g_imgObj`のプロパティ名を指定 (arrow, giko, c など)。|
|4|**ColorObject Class**|imgMotion|色付きオブジェクトに付加するClassを設定します。以下で設定が足りない場合などに使用します。|
|5|Left(X)|25|色付きオブジェクトを表示するX座標を指定します。|
|6|Top(Y)|30|色付きオブジェクトを表示するY座標を指定します。|
|7|**Width(X)**|200|色付きオブジェクトの横サイズをpxで指定します。|
|8|**Height(Y)**|200|色付きオブジェクトの縦サイズをpxで指定します。|
|9|Opacity|0.5|色付きオブジェクトの透明度を0～1の間で指定します。デフォルトは1(透明度なし)。<br>CSSアニメーションでopacity設定がある場合は無効。|
|10|Animation-Name|leftToRightFade|CSSアニメーション名を指定します。内容はCSSで定義します。<br>デフォルトはnone(なし)です。|
|11|Animation-Duration|120|CSSアニメーションを動かす間隔をフレーム数で指定します。|
|12|Animation-Fill-Mode|forwards|CSSアニメーション終了時にどのように表示するかを設定します。<br>　`forwards`: アニメーション終了時に最後の@keyframesの「100%(to)」のスタイルを適用<br>　`backwards`: アニメーション完了後、最初の@keyframes「0%(from)」のスタイルを適用<br>デフォルトは未指定です。|

#### 4. 表示の消去
```
|back_data=
200,0,../img/c_600.png,,20,20,200,200,0.5,leftToRightFade,120,forwards
200,1,../img/iyo_600.png,,220,220,200,200,1,spinY,120,forwards
400,2,文字のフローテスト,,100,140,36,0,0,fromBig,100,forwards
500,ALL
|
```
|番号|論理名|設定例|内容|
|----|----|----|----|
|1|Frame|200|変更するタイミングのフレーム数を指定します。|
|2|Depth|0|背景の深度を数字で指定します。0以上の値を指定。<br>ALLを指定した場合、全ての深度で設定されている表示を消去します。|
|3|Display||空にすると、画像・文字を消去できます。以降の指定は不要です。|

#### 5. コメント文
```
|back_data=
200,-,＜ここにコメントを入力＞
|
```
|番号|論理名|設定例|内容|
|----|----|----|----|
|1|Frame|200|フレーム数指定。指定は任意です。|
|2|Depth|-|ハイフン固定でコメント扱いとなり、行ごと読込対象外となります。|
|3|Comment|＜演出開始＞|コメント文を自由に入力できます。|

#### 6. ループ・フレームジャンプ (タイトル/リザルトモーションのみ)
```
|backtitle_data=
200,0,../img/c_600.png,,20,20,200,200,0.5,leftToRightFade,120,forwards
200,1,../img/iyo_600.png,,220,220,200,200,1,spinY,120,forwards
300,0
320,1,../img/iyo_600.png,,220,220,200,200,0,leftToRightFade,120,forwards
400,2,文字のフローテスト,,100,140,36,0,0,fromBig,100,forwards
500,2,文字のフローテスト,,100,140,36,#ff9999,0,upToDown,100,forwards
600,2,文字のフローテスト,,100,140,36,#ffff99,1,spinX,100,forwards
700,2,文字のフローテスト,,100,140,36,#99ff99,1,spinY,100,forwards
800,2,文字のフローテスト,,100,140,36,#99ffff,1,spinZ,100,forwards
900,2,文字のフローテスト,,100,140,36,0,1
1000,0,[jump],1100,3
1050,0,[loop],200:700:700
1200,0,../img/giko_600.png,,220,220,200,200,1,spinX,120
1300,0
|
```
|番号|論理名|設定例|内容|
|----|----|----|----|
|1|Frame|200|変更するタイミングのフレーム数を指定します。|
|2|ー|0|指定なし|
|3|Keyword|[jump]|キーワード指定。<br>[loop]指定フレームへ戻ります。<br>[jump]ループ条件付きで指定フレームへ移動します。|
|4|gotoAndPlay|1200|移動先のフレームを指定します。<br>コロン（：）指定で、どのフレームに飛ぶかを確率で分岐できます。<br>例）300:1500 → 1/2の確率で300フレーム、1/2の確率で1500フレーム<br>例）300:300:1200 → 2/3の確率で300フレーム、1/3の確率で1200フレーム|
|5|LoopCondition|2|[jump]のみ指定。<br>ここで指定した回数分のループが終わっていれば、4で指定したフレームへ移動します。|

#### 7. 他設定の流用 (タイトルモーション以外)
```
|backCross_data=backRev_data|
```

|番号|論理名|設定例|内容|
|----|----|----|----|
|1|Data Name|backRev_data|コピー元の背景・マスクモーションのデータ名を指定します。|


### データ名の種類
- 設定を行う画面により、データ名が異なります。  
back_data, mask_dataはメイン画面用のため、譜面別にデータを持たせることができます。  
メイン(リバース)は未指定の場合、メイン(通常)のものが採用されます。  
メイン(Cross, Split 他)についても同様です。  
- 日本語・英語で言語を切り替えた際に表示を変えることができます。

#### 通常用

|画面|背景モーション<br>(言語未指定)|マスクモーション<br>(言語未指定)|マスクモーション<br>(英語)|
|----|----|----|----|
|タイトル|backtitle_data|masktitle_data|masktitle**En**_data|
|メイン(通常)|back_data, <br>back2_data, <br>...|mask_data, <br>mask2_data, <br>...|mask**En**_data, <br>mask**En**2_data, <br>...|
|メイン(リバース)|backRev_data, <br>backRev2_data, <br>...|maskRev_data, <br>maskRev2_data, <br>...|maskRev**En**_data, <br>maskRev**En**2_data, <br>...|
|メイン(Cross, Split 他)|backAlt_data, <br>backAlt2_data, <br>...<br>backCross_data, <br>backFlat_data, <br>...|maskAlt_data, <br>maskAlt2_data, <br>...<br>maskCross_data, <br>maskFlat_data, <br>...|maskAlt**En**_data, <br>maskAlt**En**2_data, <br>...<br>maskCross**En**_data, <br>maskFlat**En**_data, <br>...|
|リザルト(クリア時)|backresult_data<br>backresult2_data<br>...|maskresult_data<br>maskresult2_data<br>...|maskresult**En**_data<br>maskresult**En**2_data<br>...|
|リザルト(途中終了時)|backfailedS_data<br>backfailedS2_data<br>...|maskfailedS_data<br>maskfailedS2_data<br>...|maskfailedS**En**_data<br>maskfailedS**En**2_data<br>...|
|リザルト(ノルマ失敗時)|backfailedB_data<br>backfailedB2_data<br>...|maskfailedB_data<br>maskfailedB2_data<br>...|maskfailedB**En**_data<br>maskfailedB**En**2_data<br>...|

#### 別キーモード用（未設定時は通常用を適用）

|画面|背景モーション<br>(言語未指定)|マスクモーション<br>(言語未指定)|マスクモーション<br>(英語)|
|----|----|----|----|
|メイン(通常)|back**A**_data, <br>back**A**2_data, <br>...|mask**A**_data, <br>mask**A**2_data, <br>...|mask**A**En_data, <br>mask**A**En2_data, <br>...|
|メイン(リバース)|backRev**A**_data, <br>backRev**A**2_data, <br>...|maskRev**A**_data, <br>maskRev**A**2_data, <br>...|maskRev**A**En_data, <br>maskRev**A**En2_data, <br>...|
|メイン(Cross, Split 他)|backAlt**A**_data, <br>backAlt**A**2_data, <br>...<br>backCross**A**_data, <br>backFlat**A**_data, <br>...|maskAlt**A**_data, <br>maskAlt**A**2_data, <br>...<br>maskCross**A**_data, <br>maskFlat**A**_data, <br>...|maskAlt**A**En_data, <br>maskAlt**A**En2_data, <br>...<br>maskCross**A**En_data, <br>maskFlat**A**En_data, <br>...|

- タイトルモーション以外のモーションについては指定が無い場合、  
例えば 2譜面目のリザルト(ノルマ失敗時) については以下の順で適用されます。

|適用順|適用モーション|意味|
|----|----|----|
|1|backfailedBJa2_data<br>backfailedBEn2_data|リザルト(ノルマ失敗時)の２譜面目 ※言語指定あり|
|2|backfailedBJa_data<br>backfailedBEn_data|リザルト(ノルマ失敗時)の１譜面目 ※言語指定あり|
|3|backfailedB2_data|リザルト(ノルマ失敗時)の２譜面目|
|4|backfailedB_data|リザルト(ノルマ失敗時)の１譜面目|
|5|backresultJa2_data<br>backresultEn2_data|リザルト(クリア時)の２譜面目 ※言語指定あり|
|6|backresultJa_data<br>backresultEn_data|リザルト(クリア時)の１譜面目 ※言語指定あり|
|7|backresult2_data|リザルト(クリア時)の２譜面目|
|8|backresult_data|リザルト(クリア時)の１譜面目|

#### 補足
- ver34.0.0より、キー別・パターン別の指定が可能になりました。
```
|transKey9A=$$9B$9C|

|back_data=...|         // 全体共通
|back2_data=...|        // 2譜面目共通
|backEn2_data=...|      // 2譜面目、英語モードのみ適用
|backAEn2_data=...|     // 2譜面目、英語モード、別キーモード(9B, 9C)のみ適用
|back<9C>En2_data=...|  // 2譜面目、英語モード、別キーモード(9C)のみ適用
|back<9A>En2_data=...|  // 2譜面目、英語モード、別キーモード外のみ適用
|back[2]En2_data=...|   // 2譜面目、英語モード、キーパターン:2のみ適用
```

### 変数埋め込み仕様
- 背景・マスクモーションの座標について変数の埋め込みが可能です。  
`{変数}`の形式で指定します。`{}`の中であれば数式も挿入可能です。
```
|mask_data=
330,3,[c]arrowSpecial/red:#ffff6666,,{g_workObj.stepX[3]},200,50,50,0,leftToRightFade,120
700,0
|
```

### 関連項目
- [displayUse](dos-h0057-displayUse) [:pencil:](dos-h0035-displayUse/_edit) Display項目の利用有無
- [masktitleButton](dos-h0043-masktitleButton) [:pencil:](dos-h0043-masktitleButton/_edit) タイトル画面上のボタン群の有効/無効設定
- [maskresultButton](dos-h0044-maskresultButton) [:pencil:](dos-h0044-maskresultButton/_edit) リザルト画面上のボタン群の有効/無効設定
- [resultMotionSet](dos-h0048-resultMotionSet) [:pencil:](dos-h0048-resultMotionSet/_edit) リザルトモーションのON/OFF設定
- [**preloadImages**](dos-h0021-preloadImages) [:pencil:](dos-h0021-preloadImages/_edit) 画像ファイルの事前読み込み設定
- [**autoPreload**](dos-h0055-autoPreload) [:pencil:](dos-h0055-autoPreload/_edit) 画像ファイルの自動読み込み設定
- [歌詞表示 (word_data)](dos-e0003-wordData) [:pencil:](dos-e0003-wordData/_edit) 
- [色付きオブジェクト仕様](AboutColorObject) [:pencil:](AboutColorObject/_edit) 

### 更新履歴

|Version|変更内容|
|----|----|
|[v36.0.0](https://github.com/cwtickle/danoniplus/releases/tag/v36.0.0)|・タブとカンマ前後にある半角スペースを自動除去するよう変更|
|[v34.0.0](https://github.com/cwtickle/danoniplus/releases/tag/v34.0.0)|・他譜面のデータを参照しつつ、個別の背景・マスクモーションを組み合わせる機能を追加<br>・キー別、キーパターン別の背景・マスクモーション設定を実装|
|[v31.2.0](https://github.com/cwtickle/danoniplus/releases/tag/v31.2.0)|・他の背景、マスクモーションのデータ名を指定することでそのデータ名の設定を参照する機能を追加|
|[v30.2.0](https://github.com/cwtickle/danoniplus/releases/tag/v30.2.0)|・スクロール別の背景、マスクモーションに対応 (backCross_data, backSplit_data など)|
|[v30.1.1](https://github.com/cwtickle/danoniplus/releases/tag/v30.1.1)|・背景、マスクモーションに色付きオブジェクトを実装<br>・背景、マスクモーションの座標に対して変数埋め込みに対応<br>・背景、マスクモーションに対して"Animation-Fill-Mode"項目を追加|
|[v30.0.1](https://github.com/cwtickle/danoniplus/releases/tag/v30.0.1)|・背景、マスクモーションの別キーモード用記述対応|
|[v24.2.1](https://github.com/cwtickle/danoniplus/releases/tag/v24.2.1)|・背景、マスクモーションの多言語対応|
|[v18.4.0](https://github.com/cwtickle/danoniplus/releases/tag/v18.4.0)|・&lt;span&gt;タグ版のDisplayにおいてHTMLタグが使えるように変更|
|[v15.1.0](https://github.com/cwtickle/danoniplus/releases/tag/v15.1.0)|・フレーム数、深度において数式記法に対応|
|[v10.2.1](https://github.com/cwtickle/danoniplus/releases/tag/v10.2.1)|・スクロール拡張設定用背景・マスクモーション<br>(backAlt_data, maskAlt_data)の実装|
|[v9.1.0](https://github.com/cwtickle/danoniplus/releases/tag/v9.1.0)|・背景／マスクモーションの適用順序設定を追加<br>（リザルトモーションと同様の設定）|
|[v8.0.0](https://github.com/cwtickle/danoniplus/releases/tag/v8.0.0)|・リザルトモーションの譜面別設定を実装|
|[v7.9.0](https://github.com/cwtickle/danoniplus/releases/tag/v7.9.0)|・クリア失敗時のリザルトモーション実装<br>・コメント行を実装|
|[v7.7.0](https://github.com/cwtickle/danoniplus/releases/tag/v7.7.0)|・同一フレームの複数同時描画に対応<br>・表示の消去において深度「ALL」（全消去）を実装|
|[v7.6.0](https://github.com/cwtickle/danoniplus/releases/tag/v7.6.0)|・タイトル／リザルトモーションのループ・フレームジャンプを拡張|
|[v6.3.0](https://github.com/cwtickle/danoniplus/releases/tag/v6.3.0)|・タイトルモーション(masktitle_data), <br>　リザルトモーション(backresult_data, maskresult_data)を実装<br>・リバース用背景・マスクモーション(backRev_data, maskRev_data)の実装|
|[v4.7.0](https://github.com/cwtickle/danoniplus/releases/tag/v4.7.0)|・タイトルモーション(backtitle_data)に対して<br>　ループ・フレームジャンプを実装|
|[v4.4.0](https://github.com/cwtickle/danoniplus/releases/tag/v4.4.0)|・タイトルモーション(backtitle_data)を実装|
|[v4.2.0](https://github.com/cwtickle/danoniplus/releases/tag/v4.2.0)|・マスク表示(mask_data)を実装|
|[v1.0.0<br>(v0.67.0)](https://github.com/cwtickle/danoniplus/releases/tag/v1.0.1)|・背景表示(back_data) 初回実装|

| [< 歌詞表示](dos-e0003-wordData) | **背景・マスクモーション** | [スキン変更 >](dos-e0008-styleData) |
