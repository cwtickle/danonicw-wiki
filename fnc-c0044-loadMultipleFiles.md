[← オブジェクトリファレンスに戻る](ObjectReferenceIndex)  

# loadMultipleFiles
### 概要
- ファイル名、ディレクトリ（絶対パス）のデータを1セットとした配列を使って  
ファイルの読み込みを再帰的に行う関数。
- _loadTypeが`js`の場合で、連携されたファイル名の拡張子が「.css」の場合、cssファイルとして読み込む。
- _loadTypeが`css`の場合は、連携されたファイル名が「.js」ファイルであっても、「.css」に置き換えて読み込む。
- **loadMultipleFiles**関数の後継として、ver26.3.1より**loadMultipleFiles2**関数に切り替えている。  
コールバック関数が不要で、async/awaitを使って呼び出す。

### _fileDataのフォーマット例
```javascript
const _fileData = [[`danoni_custom.js`, `C:/danoni/danoniplus/js/`], [`danoni_special.css`, `C:/danoni/danoniplus/css/`]];
```

## loadMultipleFiles2
### 引数（括弧内はデフォルト値）

|引数|型|必須|指定内容|
|----|----|----|----|
|_fileData|array2|*|ファイル名, ディレクトリ名を1セットとした二次元配列。<br>通常、**getFilePath**関数で取得した値を使う。|
|_loadType|string|*|読み込むファイルの拡張子を指定。現状はjs, cssに対応。|

### 返却値
- Promise&lt;any&gt;

### 使用例
```javascript
// 呼び出す関数に'async'をつける
async function load() {
    await loadMultipleFiles2(g_headerObj.cssData, `css`);  // 呼び出す前に'await'を付加
    await loadMultipleFiles2(g_headerObj.jsData, `js`);    // 呼び出す前に'await'を付加
    // 後続処理
}
```

### フローチャート
```mermaid
flowchart TD
    S([start])-->loadMultipleFiles2-->E([end])

subgraph loadMultipleFiles2
    A["ファイルの拡張子を取得"]-->B{ファイルの種類}-->|"jsファイル"| loadScript2;
    B-->|"cssファイル"| importCssFile2;
end
```

## loadMultipleFiles
- ver26.2.0以前の関数です。現在は非推奨になっています。

<details>
<summary>（参考）loadMultipleFiles の引数、使い方</summary>

### 引数（括弧内はデフォルト値）

|引数|型|必須|指定内容|
|----|----|----|----|
|_j|number|*|_fileDataの取得位置(0以上を指定)|
|_fileData|array2|*|ファイル名, ディレクトリ名を1セットとした二次元配列。<br>通常、**getFilePath**関数で取得した値を使う。|
|_loadType|string|*|読み込むファイルの拡張子を指定。現状はjs, cssに対応。|
|_afterFunc|function||読込完了後に実行する関数|

### 返却値
- なし

### 使用例
```javascript
loadMultipleFiles(0, g_headerObj.jsData, `js`, _ => {
	loadLegacyCustomFunc();
	loadDos(_ => getScoreDetailData(0), 0, true);
});
```

</details>

### 関連項目
- [**getFilePath**](fnc-c0037-getFilePath)
- [**loadDos**](fnc-c0011-loadDos)
- [**loadScript**](fnc-c0010-loadScript)
- [importCssFile](fnc-c0020-importCssFile)

### 更新履歴

|Version|変更内容|
|----|----|
|[v26.3.1](https://github.com/cwtickle/danoniplus/releases/tag/v26.3.1)|・loadMultipleFiles2関数として作り直し|
|[v25.3.0](https://github.com/cwtickle/danoniplus/releases/tag/v25.3.0)|・初回実装|