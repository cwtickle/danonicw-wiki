⭐ New Features / 🛠️ Improvements / 🐞 Bug Fixes  
( Files changed: ⚪ danoni_main.js / 🔵 danoni_constants.js / 🔴 css files )

[**<- v11**](Changelog-v11) | **v10** | [**v9 ->**](Changelog-v9)  
(**🔖 [15 Releases](https://github.com/cwtickle/danoniplus/releases?q=tag%3Av10)** )

## [Warning] 更新終了後の不具合情報
- [更新を終了したバージョンの不具合情報#v10](DeprecatedVersionBugs#v10) を参照

## v10.5.5 ([2020-01-04](https://github.com/cwtickle/danoniplus/releases/tag/v10.5.5))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v10.5.5/total)](https://github.com/cwtickle/danoniplus/archive/v10.5.5.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v10.5.5/support/v10?style=social)](https://github.com/cwtickle/danoniplus/compare/v10.5.5...support/v10#files_bucket) 
- 🐞 ロード画面でブロックすべきキーコントロールがブロックされない問題を修正 ( PR [#581](https://github.com/cwtickle/danoniplus/pull/581) ) <- :boom: [**v8.0.4**](Changelog-v8#v804-2019-09-23)
- 🐞 譜面名未指定のときにリザルトコピー時のクレジットがundefinedになる問題を修正 ( PR [#583](https://github.com/cwtickle/danoniplus/pull/583) ) <- :boom: [**v8.2.0**](Changelog-v8#v820-2019-09-24)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v10.5.5)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v10.5.3...v10.5.5#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v10.5.5)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v10.5.5.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v10.5.5.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v10.5.5/js/danoni_main.js)
/ 🎣 [**v11.2.0**](./Changelog-v11#v1120-2020-01-04)

## v10.5.3 ([2019-12-27](https://github.com/cwtickle/danoniplus/releases/tag/v10.5.3))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v10.5.3/total)](https://github.com/cwtickle/danoniplus/archive/v10.5.3.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v10.5.3/support/v10?style=social)](https://github.com/cwtickle/danoniplus/compare/v10.5.3...support/v10#files_bucket) 
- 🐞 ライフが0のとき、ライフ値が反映されない問題を修正 ( PR [#573](https://github.com/cwtickle/danoniplus/pull/573), [#576](https://github.com/cwtickle/danoniplus/pull/576) ) <- :boom: [**v10.5.0**](Changelog-v10#v1050-2019-12-13)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v10.5.3)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v10.5.2...v10.5.3#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v10.5.3)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v10.5.3.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v10.5.3.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v10.5.3/js/danoni_main.js)
/ 🎣 [**v11.1.2**](./Changelog-v11#v1112-2019-12-28)

## v10.5.2 ([2019-12-22](https://github.com/cwtickle/danoniplus/releases/tag/v10.5.2))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v10.5.2/total)](https://github.com/cwtickle/danoniplus/archive/v10.5.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v10.5.2/support/v10?style=social)](https://github.com/cwtickle/danoniplus/compare/v10.5.2...support/v10#files_bucket) 
- 🐞 結果画面（曲名）のid重複を修正 ( PR [#567](https://github.com/cwtickle/danoniplus/pull/567) ) <- :boom: [**v3.5.0**](Changelog-v3#v350-2019-03-23)
- 🐞 実装途中の未定義関数により、低速時エラーで止まる問題を修正 ( PR [#570](https://github.com/cwtickle/danoniplus/pull/570) ) <- :boom: [**v10.3.0**](Changelog-v10#v1030-2019-12-01)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v10.5.2)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v10.5.1...v10.5.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v10.5.2)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v10.5.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v10.5.2.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v10.5.2/js/danoni_main.js)
/ 🎣 [**v11.0.2**](./Changelog-v11#v1102-2019-12-22)

## v10.5.1 ([2019-12-14](https://github.com/cwtickle/danoniplus/releases/tag/v10.5.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v10.5.1/total)](https://github.com/cwtickle/danoniplus/archive/v10.5.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v10.5.1/support/v10?style=social)](https://github.com/cwtickle/danoniplus/compare/v10.5.1...support/v10#files_bucket) 
- 🛠️ fileでも起動可能なようにcrossorigin属性の付加条件を変更 ( PR [#564](https://github.com/cwtickle/danoniplus/pull/564) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v10.5.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v10.5.0...v10.5.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v10.5.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v10.5.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v10.5.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v10.5.1/js/danoni_main.js)
/ 🎣 [**v11.0.0**](./Changelog-v11#v1100-2019-12-14)

----

## v10.5.0 ([2019-12-13](https://github.com/cwtickle/danoniplus/releases/tag/v10.5.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v10.5.0/total)](https://github.com/cwtickle/danoniplus/archive/v10.5.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v10.5.0/support/v10?style=social)](https://github.com/cwtickle/danoniplus/compare/v10.5.0...support/v10#files_bucket) 
- 🛠️ ファイル読込関係のコードを整理 ( PR [#560](https://github.com/cwtickle/danoniplus/pull/560) )
- 🛠️ Localhostアクセス時にフレーム数を表示するように対応 ( PR [#561](https://github.com/cwtickle/danoniplus/pull/561) )
- 🛠️ リザルト画面のカスタム関数の位置見直し ( PR [#557](https://github.com/cwtickle/danoniplus/pull/557) )
- 🐞 ラベル重複を修正 ( PR [#557](https://github.com/cwtickle/danoniplus/pull/557) ) <- :boom: [**v7.5.0**](Changelog-v7#v750-2019-07-29)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v10.5.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v10.4.1...v10.5.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v10.5.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v10.5.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v10.5.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v10.5.0/js/danoni_main.js)

## v10.4.1 ([2019-12-07](https://github.com/cwtickle/danoniplus/releases/tag/v10.4.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v10.4.1/total)](https://github.com/cwtickle/danoniplus/archive/v10.4.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v10.4.1/support/v10?style=social)](https://github.com/cwtickle/danoniplus/compare/v10.4.1...support/v10#files_bucket) 
- 🛠️ 重複記述部分のコードを整理 ( PR [#552](https://github.com/cwtickle/danoniplus/pull/552) )
- 🐞 リザルト用ラベルの指定間違いを修正 ( PR [#553](https://github.com/cwtickle/danoniplus/pull/553) ) <- :boom: [**v10.4.0**](Changelog-v10#v1040-2019-12-07)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v10.4.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v10.4.0...v10.4.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v10.4.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v10.4.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v10.4.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v10.4.1/js/danoni_main.js)

## v10.4.0 ([2019-12-07](https://github.com/cwtickle/danoniplus/releases/tag/v10.4.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v10.4.0/total)](https://github.com/cwtickle/danoniplus/archive/v10.4.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v10.4.0/support/v10?style=social)](https://github.com/cwtickle/danoniplus/compare/v10.4.0...support/v10#files_bucket) 
- ⭐️ 矢印のヒット部分を押したタイミングに応じて変わるように変更 ( PR [#547](https://github.com/cwtickle/danoniplus/pull/547) )
- ⭐️ 矢印名で指定できる種類を拡張
（ある程度任意の名前を指定しても、フリーズアローが自動で対応するように変更）( PR [#550](https://github.com/cwtickle/danoniplus/pull/550) )
- 🛠️ ハイスコア差分、スコア表示のid重複を解消 ( PR [#545](https://github.com/cwtickle/danoniplus/pull/545), [#546](https://github.com/cwtickle/danoniplus/pull/546) )
- 🛠️ 重複記述部分のコードを整理 ( PR [#548](https://github.com/cwtickle/danoniplus/pull/548), [#549](https://github.com/cwtickle/danoniplus/pull/549), [#550](https://github.com/cwtickle/danoniplus/pull/550) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v10.4.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v10.3.0...v10.4.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v10.4.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v10.4.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v10.4.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v10.4.0/js/danoni_main.js)<br>❤️ ショウタ

## v10.3.0 ([2019-12-01](https://github.com/cwtickle/danoniplus/releases/tag/v10.3.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v10.3.0/total)](https://github.com/cwtickle/danoniplus/archive/v10.3.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v10.3.0/support/v10?style=social)](https://github.com/cwtickle/danoniplus/compare/v10.3.0...support/v10#files_bucket) 
- 🛠️ 重複記述部分のコードを整理 ( PR [#541](https://github.com/cwtickle/danoniplus/pull/541), [#542](https://github.com/cwtickle/danoniplus/pull/542), [#543](https://github.com/cwtickle/danoniplus/pull/543) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v10.3.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v10.2.4...v10.3.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v10.3.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v10.3.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v10.3.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v10.3.0/js/danoni_main.js)

## v10.2.4 ([2019-11-24](https://github.com/cwtickle/danoniplus/releases/tag/v10.2.4))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v10.2.4/total)](https://github.com/cwtickle/danoniplus/archive/v10.2.4.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v10.2.4/support/v10?style=social)](https://github.com/cwtickle/danoniplus/compare/v10.2.4...support/v10#files_bucket) 
- 🐞 譜面選択リストの長さ間違いを修正 ( PR [#539](https://github.com/cwtickle/danoniplus/pull/539) ) <- :boom: [**v10.2.1**](Changelog-v10#v1021-2019-11-20)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v10.2.4)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v10.2.3...v10.2.4#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v10.2.4)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v10.2.4.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v10.2.4.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v10.2.4/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v10.2.4/js/lib/danoni_constants.js)

## v10.2.3 ([2019-11-24](https://github.com/cwtickle/danoniplus/releases/tag/v10.2.3))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v10.2.3/total)](https://github.com/cwtickle/danoniplus/archive/v10.2.3.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v10.2.3/support/v10?style=social)](https://github.com/cwtickle/danoniplus/compare/v10.2.3...support/v10#files_bucket) 
- 🐞 ぃょぅがギコの画像になっている問題を修正 ( PR [#537](https://github.com/cwtickle/danoniplus/pull/537) ) <- :boom: [**v10.0.0**](Changelog-v10#v1000-2019-11-04)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v10.2.3)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v10.2.2...v10.2.3#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v10.2.3)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v10.2.3.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v10.2.3.tar.gz) ) [🔵](https://github.com/cwtickle/danoniplus/blob/v10.2.3/js/lib/danoni_constants.js)<br>❤️ ★ぞろり★

## v10.2.2 ([2019-11-23](https://github.com/cwtickle/danoniplus/releases/tag/v10.2.2))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v10.2.2/total)](https://github.com/cwtickle/danoniplus/archive/v10.2.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v10.2.2/support/v10?style=social)](https://github.com/cwtickle/danoniplus/compare/v10.2.2...support/v10#files_bucket) 
- 🐞 15A, 15Bkey用のスクロール拡張設定誤りを修正 ( PR [#535](https://github.com/cwtickle/danoniplus/pull/535) ) <- :boom: [**v10.2.1**](Changelog-v10#v1021-2019-11-20)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v10.2.2)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v10.2.1...v10.2.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v10.2.2)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v10.2.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v10.2.2.tar.gz) ) [🔵](https://github.com/cwtickle/danoniplus/blob/v10.2.2/js/lib/danoni_constants.js)

## v10.2.1 ([2019-11-20](https://github.com/cwtickle/danoniplus/releases/tag/v10.2.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v10.2.1/total)](https://github.com/cwtickle/danoniplus/archive/v10.2.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v10.2.1/support/v10?style=social)](https://github.com/cwtickle/danoniplus/compare/v10.2.1...support/v10#files_bucket) 
- ⭐️ スクロール拡張（Cross / Split / Flat 他）に対応 ( PR [#530](https://github.com/cwtickle/danoniplus/pull/530) )
- ⭐️ スクロール拡張に対応した歌詞表示・背景／マスクモーションを追加 ( PR [#530](https://github.com/cwtickle/danoniplus/pull/530) )
- 🛠️ 共通定数・旧バージョン互換関数を別ファイルに分離 ( PR [#529](https://github.com/cwtickle/danoniplus/pull/529) )
- 🛠️ Displayオプションで「StepZone」を選択時、任意のキーを押したときに
ステップゾーンが光る仕様に変更 ( PR [#530](https://github.com/cwtickle/danoniplus/pull/530) )
- 🛠️ ステップゾーン関係のオブジェクトをグループ化 ( PR [#532](https://github.com/cwtickle/danoniplus/pull/532) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v10.2.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v10.1.1...v10.2.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v10.2.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v10.2.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v10.2.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v10.2.1/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v10.2.1/js/lib/danoni_constants.js)🔴<br>❤️ izkdic, ★ぞろり★

## v10.1.1 ([2019-11-11](https://github.com/cwtickle/danoniplus/releases/tag/v10.1.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v10.1.1/total)](https://github.com/cwtickle/danoniplus/archive/v10.1.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v10.1.1/support/v10?style=social)](https://github.com/cwtickle/danoniplus/compare/v10.1.1...support/v10#files_bucket) 
- 🐞 MacOS, iOS (Safari)においてWeb Audio APIが動作しない問題を修正 ( PR [#527](https://github.com/cwtickle/danoniplus/pull/527) ) <- :boom: **initial**

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v10.1.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v10.1.0...v10.1.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v10.1.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v10.1.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v10.1.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v10.1.1/js/danoni_main.js)

## v10.1.0 ([2019-11-10](https://github.com/cwtickle/danoniplus/releases/tag/v10.1.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v10.1.0/total)](https://github.com/cwtickle/danoniplus/archive/v10.1.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v10.1.0/support/v10?style=social)](https://github.com/cwtickle/danoniplus/compare/v10.1.0...support/v10#files_bucket) 
- 🛠️ skinTypeがdefault以外の場合、背景オブジェクトを作成するよう変更 ( PR [#524](https://github.com/cwtickle/danoniplus/pull/524) )
- 🐞 iOS利用時にロードで止まる問題を一部修正 ( PR [#523](https://github.com/cwtickle/danoniplus/pull/523), [#525](https://github.com/cwtickle/danoniplus/pull/525) ) <- :boom: **initial**
- 🐞 iOS利用時にAppearanceが動作しない問題を修正 ( PR [#523](https://github.com/cwtickle/danoniplus/pull/523) ) <- :boom: **initial**

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v10.1.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v10.0.0...v10.1.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v10.1.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v10.1.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v10.1.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v10.1.0/js/danoni_main.js)🔴

## v10.0.0 ([2019-11-04](https://github.com/cwtickle/danoniplus/releases/tag/v10.0.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v10.0.0/total)](https://github.com/cwtickle/danoniplus/archive/v10.0.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v10.0.0/support/v10?style=social)](https://github.com/cwtickle/danoniplus/compare/v10.0.0...support/v10#files_bucket) 
- ⭐️ スキン機能の実装 ( Issue [#185](https://github.com/cwtickle/danoniplus/pull/185), PR [#514](https://github.com/cwtickle/danoniplus/pull/514), [#515](https://github.com/cwtickle/danoniplus/pull/515), [#516](https://github.com/cwtickle/danoniplus/pull/516), [#520](https://github.com/cwtickle/danoniplus/pull/520) )
- ⭐️ 背景・マスクに対象拡張子のデータがあれば、自動でpreloadする機能を実装 ( PR [#513](https://github.com/cwtickle/danoniplus/pull/513) )
- ⭐️ 矢印、アスキーアート画像のファイル名を変更し、mask-imageをjs側へ移動 ( Issue [#507](https://github.com/cwtickle/danoniplus/pull/507), PR [#511](https://github.com/cwtickle/danoniplus/pull/511) )
- ⭐️ danoni_setting.js をグループごとに分けられるように変更 ( PR [#515](https://github.com/cwtickle/danoniplus/pull/515) )
- ⭐️ js, css, skinフォルダ配下のファイル指定に対してカレントディレクトリ指定に対応
(skinType, settingType, musicUrl, customjs でファイルの先頭に(..) を付加することで可能) ( PR [#515](https://github.com/cwtickle/danoniplus/pull/515) )
- ⭐️ 矢印等の回転数を管理するオブジェクトをステップゾーン・ヒットゾーン・矢印の3つに分離 ( Issue [#235](https://github.com/cwtickle/danoniplus/pull/235), PR [#518](https://github.com/cwtickle/danoniplus/pull/518) )
- 🛠️ danoni_custom.jsやdanoni_setting.jsが無くても動作するように、loadScript関数の仕様を変更 ( PR [#515](https://github.com/cwtickle/danoniplus/pull/515) )
- 🛠️ Canvasタグを使用しなくても動作するように変更 ( PR [#514](https://github.com/cwtickle/danoniplus/pull/514) )
- 🛠️ ボタンを作成する処理、矢印生成処理の関数変更 ( Issue [#152](https://github.com/cwtickle/danoniplus/pull/152), PR [#514](https://github.com/cwtickle/danoniplus/pull/514), [#518](https://github.com/cwtickle/danoniplus/pull/518) )
- 🛠️ 設定画面で、設定項目ごとにdiv要素をまとめるように変更 ( PR [#519](https://github.com/cwtickle/danoniplus/pull/519) )
- 🛠️ 設定画面の項目を囲うオブジェクト(optionsprite)の位置を中央揃えになるように調整 ( PR [#519](https://github.com/cwtickle/danoniplus/pull/519) )
- 🛠️ ParaFla版の矢印画像を追加 ( PR [#521](https://github.com/cwtickle/danoniplus/pull/521) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v10.0.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v9.4.3...v10.0.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v10.0.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v10.0.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v10.0.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v10.0.0/js/danoni_main.js)🔴+(img)

[**<- v11**](Changelog-v11) | **v10** | [**v9 ->**](Changelog-v9)
