⭐ New Features / 🛠️ Improvements / 🐞 Bug Fixes

[**<- v5**](Changelog-v5) | **v4** | [**v3 ->**](Changelog-v3)  
(**🔖 [39 Releases](https://github.com/cwtickle/danoniplus/releases?q=tag%3Av4)** )

## [Warning] 更新終了後の不具合情報
- [更新を終了したバージョンの不具合情報#v4](DeprecatedVersionBugs#v4) を参照

## v4.10.22 ([2019-10-08](https://github.com/cwtickle/danoniplus/releases/tag/v4.10.22))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v4.10.22/total)](https://github.com/cwtickle/danoniplus/archive/v4.10.22.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v4.10.22/support/v4?style=social)](https://github.com/cwtickle/danoniplus/compare/v4.10.22...support/v4#files_bucket)
- 🐞 速度変化が同一フレームにあると正常に動作しなくなる不具合を修正 ( PR [#477](https://github.com/cwtickle/danoniplus/pull/477) ) <- :boom: **initial**

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v4.10.22)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v4.10.21...v4.10.22#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v4.10.22)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v4.10.22.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v4.10.22.tar.gz) )
/ 🎣 [**v8.7.3**](./Changelog-v8#v873-2019-10-08)

## v4.10.21 ([2019-10-06](https://github.com/cwtickle/danoniplus/releases/tag/v4.10.21))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v4.10.21/total)](https://github.com/cwtickle/danoniplus/archive/v4.10.21.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v4.10.21/support/v4?style=social)](https://github.com/cwtickle/danoniplus/compare/v4.10.21...support/v4#files_bucket)
- 🐞 エスケープ文字の適用順序誤りを修正 ( PR [#472](https://github.com/cwtickle/danoniplus/pull/472), [#473](https://github.com/cwtickle/danoniplus/pull/473) ) <- :boom: [**v0.66.x**](Changelog-v0#v066x-2018-11-16)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v4.10.21)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v4.10.20...v4.10.21#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v4.10.21)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v4.10.21.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v4.10.21.tar.gz) )
/ 🎣 [**v8.7.2**](./Changelog-v8#v872-2019-10-06)

## v4.10.20 ([2019-09-30](https://github.com/cwtickle/danoniplus/releases/tag/v4.10.20))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v4.10.20/total)](https://github.com/cwtickle/danoniplus/archive/v4.10.20.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v4.10.20/support/v4?style=social)](https://github.com/cwtickle/danoniplus/compare/v4.10.20...support/v4#files_bucket) 
- 🐞 エラーウィンドウが表示されない問題を修正 ( PR [#459](https://github.com/cwtickle/danoniplus/pull/459) ) <- :boom: [**v2.6.0**](Changelog-v2#v260-2019-02-10)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v4.10.20)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v4.10.19...v4.10.20#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v4.10.20)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v4.10.20.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v4.10.20.tar.gz) )
/ 🎣 [**v8.6.1**](./Changelog-v8#v861-2019-09-30)

## v4.10.19 ([2019-09-23](https://github.com/cwtickle/danoniplus/releases/tag/v4.10.19))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v4.10.19/total)](https://github.com/cwtickle/danoniplus/archive/v4.10.19.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v4.10.19/support/v4?style=social)](https://github.com/cwtickle/danoniplus/compare/v4.10.19...support/v4#files_bucket) 
- 🐞 ロード画面でEnterを押すと複数回ロードが発生する問題を修正 ( PR [#432](https://github.com/cwtickle/danoniplus/pull/432) ) <- :boom: **initial**
- 🐞 メイン画面で曲中リトライキーを連打した場合に譜面がずれることがある問題を修正 ( PR [#433](https://github.com/cwtickle/danoniplus/pull/433) ) <- :boom: **initial**

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v4.10.19)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v4.10.18...v4.10.19#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v4.10.19)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v4.10.19.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v4.10.19.tar.gz) )
/ 🎣 [**v8.0.4**](./Changelog-v8#v804-2019-09-23)

## v4.10.18 ([2019-09-16](https://github.com/cwtickle/danoniplus/releases/tag/v4.10.18))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v4.10.18/total)](https://github.com/cwtickle/danoniplus/archive/v4.10.18.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v4.10.18/support/v4?style=social)](https://github.com/cwtickle/danoniplus/compare/v4.10.18...support/v4#files_bucket) 
- 🐞 ゲームオーバー時の200ミリ秒遅延により、ハイスコア表示がおかしくなることがある問題を修正 ( PR [#428](https://github.com/cwtickle/danoniplus/pull/428) ) <- :boom: **initial**

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v4.10.18)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v4.10.17...v4.10.18#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v4.10.18)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v4.10.18.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v4.10.18.tar.gz) )
/ 🎣 [**v8.0.2**](./Changelog-v8#v802-2019-09-16)

## v4.10.17 ([2019-09-15](https://github.com/cwtickle/danoniplus/releases/tag/v4.10.17))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v4.10.17/total)](https://github.com/cwtickle/danoniplus/archive/v4.10.17.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v4.10.17/support/v4?style=social)](https://github.com/cwtickle/danoniplus/compare/v4.10.17...support/v4#files_bucket) 
- 🐞 速度変化が補正込みで負のフレームにあるときの不具合を修正 ( PR [#426](https://github.com/cwtickle/danoniplus/pull/426) ) <- :boom: **initial**

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v4.10.17)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v4.10.16...v4.10.17#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v4.10.17)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v4.10.17.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v4.10.17.tar.gz) )
/ 🎣 [**v8.0.1**](./Changelog-v8#v801-2019-09-15)

## v4.10.16 ([2019-08-03](https://github.com/cwtickle/danoniplus/releases/tag/v4.10.16))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v4.10.16/total)](https://github.com/cwtickle/danoniplus/archive/v4.10.16.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v4.10.16/support/v4?style=social)](https://github.com/cwtickle/danoniplus/compare/v4.10.16...support/v4#files_bucket) 
- 🐞 back_data, mask_dataで0フレームが指定された場合に動作しないことがある問題を修正 ( PR [#388](https://github.com/cwtickle/danoniplus/pull/388) ) <- :boom: [**v4.2.0**](Changelog-v4#v420-2019-04-30), [**v0.65.x**](Changelog-v0#v065x-2018-11-16)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v4.10.16)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v4.10.15...v4.10.16#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v4.10.16)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v4.10.16.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v4.10.16.tar.gz) )
/ 🎣 [**v7.5.1**](./Changelog-v7#v751-2019-08-03)

## v4.10.15 ([2019-07-21](https://github.com/cwtickle/danoniplus/releases/tag/v4.10.15))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v4.10.15/total)](https://github.com/cwtickle/danoniplus/archive/v4.10.15.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v4.10.15/support/v4?style=social)](https://github.com/cwtickle/danoniplus/compare/v4.10.15...support/v4#files_bucket) 
- 🐞 リザルト画面で、フェードアウト中にTitleBack/Retryするとフェードアウト状態が引き継がれてしまう問題を修正 ( PR [#384](https://github.com/cwtickle/danoniplus/pull/384) ) <- :boom: [**v0.68.x**](Changelog-v0#v068x-2018-11-17)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v4.10.15)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v4.10.14...v4.10.15#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v4.10.15)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v4.10.15.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v4.10.15.tar.gz) )
/ 🎣 [**v7.4.0**](./Changelog-v7#v740-2019-07-21)

## v4.10.14 ([2019-07-08](https://github.com/cwtickle/danoniplus/releases/tag/v4.10.14))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v4.10.14/total)](https://github.com/cwtickle/danoniplus/archive/v4.10.14.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v4.10.14/support/v4?style=social)](https://github.com/cwtickle/danoniplus/compare/v4.10.14...support/v4#files_bucket) 
- 🐞 変更したキーコンフィグ内容が反映されないことがある問題を修正 ( PR [#365](https://github.com/cwtickle/danoniplus/pull/365) ) <- :boom: [**v4.6.0**](Changelog-v4#v460-2019-05-03)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v4.10.14)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v4.10.13...v4.10.14#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v4.10.14)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v4.10.14.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v4.10.14.tar.gz) )
/ 🎣 [**v6.6.0**](./Changelog-v6#v660-2019-07-08)

## v4.10.13 ([2019-07-06](https://github.com/cwtickle/danoniplus/releases/tag/v4.10.13))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v4.10.13/total)](https://github.com/cwtickle/danoniplus/archive/v4.10.13.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v4.10.13/support/v4?style=social)](https://github.com/cwtickle/danoniplus/compare/v4.10.13...support/v4#files_bucket) 
- 🐞 フェードイン時に個別加速が掛からない問題を修正 ( PR [#363](https://github.com/cwtickle/danoniplus/pull/363) ) <- :boom: **initial**

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v4.10.13)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v4.10.12...v4.10.13#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v4.10.13)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v4.10.13.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v4.10.13.tar.gz) )
/ 🎣 [**v6.5.1**](./Changelog-v6#v651-2019-07-06)

## v4.10.12 ([2019-06-27](https://github.com/cwtickle/danoniplus/releases/tag/v4.10.12))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v4.10.12/total)](https://github.com/cwtickle/danoniplus/archive/v4.10.12.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v4.10.12/support/v4?style=social)](https://github.com/cwtickle/danoniplus/compare/v4.10.12...support/v4#files_bucket) 
- 🐞 [#355](https://github.com/cwtickle/danoniplus/pull/355) にて直す必要のない条件文を再修正 <- :boom: [**v4.10.11**](Changelog-v4#v41011-2019-06-26)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v4.10.12)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v4.10.11...v4.10.12#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v4.10.12)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v4.10.12.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v4.10.12.tar.gz) )
/ 🎣 [**v6.2.1**](./Changelog-v6#v621-2019-06-26)

## v4.10.11 ([2019-06-26](https://github.com/cwtickle/danoniplus/releases/tag/v4.10.11))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v4.10.11/total)](https://github.com/cwtickle/danoniplus/archive/v4.10.11.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v4.10.11/support/v4?style=social)](https://github.com/cwtickle/danoniplus/compare/v4.10.11...support/v4#files_bucket) 
- 🐞 通常キー（キーコンフィグ保存済み）が1譜面目、独自キーが2譜面目の場合に譜面の読み込みやキーコンフィグの設定ができない問題を修正 ( PR [#355](https://github.com/cwtickle/danoniplus/pull/355) ) <- :boom: [**v4.6.0**](Changelog-v4#v460-2019-05-03)
- 🐞 `danoni_setting.js` の `g_presetGaugeCustom` において0が指定できない問題を修正 ( PR [#355](https://github.com/cwtickle/danoniplus/pull/355) ) <- :boom: [**v4.6.0**](Changelog-v4#v460-2019-05-03)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v4.10.11)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v4.10.10...v4.10.11#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v4.10.11)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v4.10.11.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v4.10.11.tar.gz) )
/ 🎣 [**v6.2.1**](./Changelog-v6#v621-2019-06-26)

## v4.10.10 ([2019-06-18](https://github.com/cwtickle/danoniplus/releases/tag/v4.10.10))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v4.10.10/total)](https://github.com/cwtickle/danoniplus/archive/v4.10.10.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v4.10.10/support/v4?style=social)](https://github.com/cwtickle/danoniplus/compare/v4.10.10...support/v4#files_bucket) 
- 🛠️ フリーズアローの枠外判定の修正・見直し（条件により止まってしまう問題の修正）( PR [#341](https://github.com/cwtickle/danoniplus/pull/341) )
- 🐞 フリーズアローのみの譜面が再生できない問題を修正 ( PR [#343](https://github.com/cwtickle/danoniplus/pull/343) ) <- :boom: **initial**

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v4.10.10)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v4.10.9...v4.10.10#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v4.10.10)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v4.10.10.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v4.10.10.tar.gz) )
/ 🎣 [**v5.12.2**](./Changelog-v5#v5122-2019-06-18)

## v4.10.9 ([2019-05-31](https://github.com/cwtickle/danoniplus/releases/tag/v4.10.9))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v4.10.9/total)](https://github.com/cwtickle/danoniplus/archive/v4.10.9.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v4.10.9/support/v4?style=social)](https://github.com/cwtickle/danoniplus/compare/v4.10.9...support/v4#files_bucket) 
- 🐞 判定基準位置が1フレーム手前になっている問題を修正 ( PR [#318](https://github.com/cwtickle/danoniplus/pull/318), [#323](https://github.com/cwtickle/danoniplus/pull/323) ) <- :boom: **initial**

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v4.10.9)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v4.10.5...v4.10.9#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v4.10.9)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v4.10.9.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v4.10.9.tar.gz) )
/ 🎣 [**v5.6.6**](./Changelog-v5#v566-2019-05-31)

## v4.10.5 ([2019-05-26](https://github.com/cwtickle/danoniplus/releases/tag/v4.10.5))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v4.10.5/total)](https://github.com/cwtickle/danoniplus/archive/v4.10.5.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v4.10.5/support/v4?style=social)](https://github.com/cwtickle/danoniplus/compare/v4.10.5...support/v4#files_bucket) 
- 🐞 譜面ヘッダー：titlesizeを64以上に指定すると文字欠けが起こる問題を修正 ( PR [#308](https://github.com/cwtickle/danoniplus/pull/308) ) <- :boom: [**v3.5.0**](Changelog-v3#v350-2019-03-23)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v4.10.5)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v4.10.4...v4.10.5#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v4.10.5)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v4.10.5.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v4.10.5.tar.gz) )
/ 🎣 [**v5.2.2**](./Changelog-v5#v522-2019-05-26)

## v4.10.4 ([2019-05-21](https://github.com/cwtickle/danoniplus/releases/tag/v4.10.4))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v4.10.4/total)](https://github.com/cwtickle/danoniplus/archive/v4.10.4.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v4.10.4/support/v4?style=social)](https://github.com/cwtickle/danoniplus/compare/v4.10.4...support/v4#files_bucket) 
- 🐞 back_data, mask_dataのフレーム数が小さすぎる場合に画面が止まる問題を修正 ( PR [#305](https://github.com/cwtickle/danoniplus/pull/305) ) <- :boom: [**v2.4.0**](Changelog-v2#v240-2019-02-08) 
- 🐞 既存キーの譜面から特殊キーへ移るときにリバースの設定がリセットされない問題を修正 ( PR [#305](https://github.com/cwtickle/danoniplus/pull/305) ) <- :boom: [**v4.6.0**](Changelog-v4#v460-2019-05-03)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v4.10.4)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v4.10.3...v4.10.4#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v4.10.4)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v4.10.4.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v4.10.4.tar.gz) )
/ 🎣 [**v5.2.1**](./Changelog-v5#v521-2019-05-21)

## v4.10.3 ([2019-05-19](https://github.com/cwtickle/danoniplus/releases/tag/v4.10.3))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v4.10.3/total)](https://github.com/cwtickle/danoniplus/archive/v4.10.3.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v4.10.3/support/v4?style=social)](https://github.com/cwtickle/danoniplus/compare/v4.10.3...support/v4#files_bucket) 
- 🐞 LocalStorageが設定されていないキーのときにReverseが初期化されない問題を修正 ( PR [#299](https://github.com/cwtickle/danoniplus/pull/299) ) <- :boom: [**v4.6.0**](Changelog-v4#v460-2019-05-03)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v4.10.3)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v4.10.2...v4.10.3#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v4.10.3)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v4.10.3.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v4.10.3.tar.gz) )
/ 🎣 [**v5.1.1**](./Changelog-v5#v511-2019-05-19)

## v4.10.2 ([2019-05-17](https://github.com/cwtickle/danoniplus/releases/tag/v4.10.2))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v4.10.2/total)](https://github.com/cwtickle/danoniplus/archive/v4.10.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v4.10.2/support/v4?style=social)](https://github.com/cwtickle/danoniplus/compare/v4.10.2...support/v4#files_bucket) 
- 🐞 特殊キーが既存キーと同一の場合に、設定画面で止まる問題を修正 ( PR [#295](https://github.com/cwtickle/danoniplus/pull/295) ) <- :boom: [**v4.6.0**](Changelog-v4#v460-2019-05-03) 

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v4.10.2)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v4.10.1...v4.10.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v4.10.2)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v4.10.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v4.10.2.tar.gz) )
/ 🎣 [**v5.0.2**](./Changelog-v5#v502-2019-05-17)

----

## v4.10.1 ([2019-05-16](https://github.com/cwtickle/danoniplus/releases/tag/v4.10.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v4.10.1/total)](https://github.com/cwtickle/danoniplus/archive/v4.10.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v4.10.1/support/v4?style=social)](https://github.com/cwtickle/danoniplus/compare/v4.10.1...support/v4#files_bucket) 
- 🐞 fadeFrame指定あり・endFrameが無指定のときにendFrameが優先されない場合がある問題を修正 ( PR [#290](https://github.com/cwtickle/danoniplus/pull/290) ) <- :boom: [**v4.10.0**](Changelog-v4#v4100-2019-05-15) 

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v4.10.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v4.10.0...v4.10.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v4.10.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v4.10.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v4.10.1.tar.gz) )

## v4.10.0 ([2019-05-15](https://github.com/cwtickle/danoniplus/releases/tag/v4.10.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v4.10.0/total)](https://github.com/cwtickle/danoniplus/archive/v4.10.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v4.10.0/support/v4?style=social)](https://github.com/cwtickle/danoniplus/compare/v4.10.0...support/v4#files_bucket) 
- ⭐️ 曲中リトライ以外のタイミングで曲の再読み込みを行う仕様に変更 ( PR [#288](https://github.com/cwtickle/danoniplus/pull/288) )  
- ⭐️ fadeFrameとendFrameが同時指定された場合、endFrameの時間を優先するように変更 ( PR [#289](https://github.com/cwtickle/danoniplus/pull/289) ) 
- 🛠️ ローディングバーについて、Canvas描画からdiv要素へ変更 ( PR [#288](https://github.com/cwtickle/danoniplus/pull/288) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v4.10.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v4.9.1...v4.10.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v4.10.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v4.10.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v4.10.0.tar.gz) )<br>❤️ izkdic

## v4.9.1 ([2019-05-12](https://github.com/cwtickle/danoniplus/releases/tag/v4.9.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v4.9.1/total)](https://github.com/cwtickle/danoniplus/archive/v4.9.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v4.9.1/support/v4?style=social)](https://github.com/cwtickle/danoniplus/compare/v4.9.1...support/v4#files_bucket) 
- 🐞 Display画面からEnterキーで開始すると画面が止まる問題を修正 ( PR [#287](https://github.com/cwtickle/danoniplus/pull/287) ) <- :boom: [**v4.0.0**](Changelog-v4#v400-2019-04-25)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v4.9.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v4.9.0...v4.9.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v4.9.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v4.9.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v4.9.1.tar.gz) )

## v4.9.0 ([2019-05-11](https://github.com/cwtickle/danoniplus/releases/tag/v4.9.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v4.9.0/total)](https://github.com/cwtickle/danoniplus/archive/v4.9.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v4.9.0/support/v4?style=social)](https://github.com/cwtickle/danoniplus/compare/v4.9.0...support/v4#files_bucket) 
- 🛠️ タイトル画面において、Maker, Artistが空のときに空ウィンドウを表示しないように修正 ( PR [#285](https://github.com/cwtickle/danoniplus/pull/285) ) 
- 🛠️ キーコンフィグ画面において、別キーモード時にデータ保存しない旨のメッセージを追加  ( PR [#285](https://github.com/cwtickle/danoniplus/pull/285) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v4.9.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v4.8.1...v4.9.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v4.9.0) 
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v4.9.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v4.9.0.tar.gz) )<br>❤️ izkdic

## v4.8.1 ([2019-05-11](https://github.com/cwtickle/danoniplus/releases/tag/v4.8.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v4.8.1/total)](https://github.com/cwtickle/danoniplus/archive/v4.8.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v4.8.1/support/v4?style=social)](https://github.com/cwtickle/danoniplus/compare/v4.8.1...support/v4#files_bucket) 
- 🐞 ライフ回復・ダメージ量計算でフリーズアローのカウントが間違っていたのを修正 ( PR [#284](https://github.com/cwtickle/danoniplus/pull/284) ) <- :boom: [**v0.72.x**](Changelog-v0#v072x-2018-11-20)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v4.8.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v4.8.0...v4.8.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v4.8.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v4.8.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v4.8.1.tar.gz) )<br>❤️ izkdic

## v4.8.0 ([2019-05-10](https://github.com/cwtickle/danoniplus/releases/tag/v4.8.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v4.8.0/total)](https://github.com/cwtickle/danoniplus/archive/v4.8.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v4.8.0/support/v4?style=social)](https://github.com/cwtickle/danoniplus/compare/v4.8.0...support/v4#files_bucket) 
- ⭐️ 11key譜面を11Lkeyとしてプレイできるモードを追加 ( PR [#282](https://github.com/cwtickle/danoniplus/pull/282) , Issue [#281](https://github.com/cwtickle/danoniplus/pull/281) 関連 )  
- ⭐️ 別キープレイを許可するかどうかのフラグ（譜面ヘッダー）を追加 ( PR [#282](https://github.com/cwtickle/danoniplus/pull/282) ) 
- ⭐️ 11key, 11Lkeyのキーコンパターンの見直し（おにぎりが上段（5key側）に来るパターンを削除）( PR [#282](https://github.com/cwtickle/danoniplus/pull/282) )
- 🐞 独自キー(keyExtraList)を指定した作品で、複数譜面時に譜面選択できない問題を修正 <- :boom: [**v4.7.0**](Changelog-v4#v470-2019-05-05)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v4.8.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v4.7.0...v4.8.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v4.8.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v4.8.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v4.8.0.tar.gz) )<br>❤️ apoi
 
## v4.7.0 ([2019-05-05](https://github.com/cwtickle/danoniplus/releases/tag/v4.7.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v4.7.0/total)](https://github.com/cwtickle/danoniplus/archive/v4.7.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v4.7.0/support/v4?style=social)](https://github.com/cwtickle/danoniplus/compare/v4.7.0...support/v4#files_bucket) 
- ⭐️ タイトルモーション(backtitle_data)において、キーワード指定によりループやフレームジャンプが行えるように変更 ( PR [#279](https://github.com/cwtickle/danoniplus/pull/279) ) 

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v4.7.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v4.6.2...v4.7.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v4.7.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v4.7.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v4.7.0.tar.gz) )

## v4.6.2 ([2019-05-04](https://github.com/cwtickle/danoniplus/releases/tag/v4.6.2))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v4.6.2/total)](https://github.com/cwtickle/danoniplus/archive/v4.6.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v4.6.2/support/v4?style=social)](https://github.com/cwtickle/danoniplus/compare/v4.6.2...support/v4#files_bucket) 
- 🐞 タイトルバック時に速度、ゲージ、リバース設定が初期化されることがある問題を修正 ( PR [#278](https://github.com/cwtickle/danoniplus/pull/278) ) <- :boom: [**v4.6.0**](Changelog-v4#v460-2019-05-03) 

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v4.6.2)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v4.6.1...v4.6.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v4.6.2)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v4.6.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v4.6.2.tar.gz) )

## v4.6.1 ([2019-05-03](https://github.com/cwtickle/danoniplus/releases/tag/v4.6.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v4.6.1/total)](https://github.com/cwtickle/danoniplus/archive/v4.6.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v4.6.1/support/v4?style=social)](https://github.com/cwtickle/danoniplus/compare/v4.6.1...support/v4#files_bucket) 
- 🐞 キーコンフィグ(ローカル保存中データ)を使用する際、Shuffle配列がコピーされない可能性がある問題を修正 ( PR [#277](https://github.com/cwtickle/danoniplus/pull/277) ) <- :boom: [**v4.6.0**](Changelog-v4#v460-2019-05-03) 

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v4.6.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v4.6.0...v4.6.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v4.6.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v4.6.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v4.6.1.tar.gz) )

## v4.6.0 ([2019-05-03](https://github.com/cwtickle/danoniplus/releases/tag/v4.6.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v4.6.0/total)](https://github.com/cwtickle/danoniplus/archive/v4.6.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v4.6.0/support/v4?style=social)](https://github.com/cwtickle/danoniplus/compare/v4.6.0...support/v4#files_bucket) 
- ⭐️ (ドメイン別)キー毎にリバース、キーコンフィグを自動保存する機能を実装 ( PR [#272](https://github.com/cwtickle/danoniplus/pull/272), [#274](https://github.com/cwtickle/danoniplus/pull/274), [#275](https://github.com/cwtickle/danoniplus/pull/275) ) 
- ⭐️ キーコンフィグ画面において、変更・自動保存色を設定 ( PR [#272](https://github.com/cwtickle/danoniplus/pull/272) ) 
- ⭐️ ローカルストレージ保存有無ボタンの追加 ( PR [#274](https://github.com/cwtickle/danoniplus/pull/274) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v4.6.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v4.4.1...v4.6.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v4.6.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v4.6.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v4.6.0.tar.gz) )<br>❤️ MFV2 (@MFV2)

## v4.4.1 ([2019-05-02](https://github.com/cwtickle/danoniplus/releases/tag/v4.4.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v4.4.1/total)](https://github.com/cwtickle/danoniplus/archive/v4.4.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v4.4.1/support/v4?style=social)](https://github.com/cwtickle/danoniplus/compare/v4.4.1...support/v4#files_bucket) 
- 🐞 Volumeのカーソル位置をLocalStorageから読み込んだ値に合わせるように修正 ( PR [#270](https://github.com/cwtickle/danoniplus/pull/270) ) <- :boom: [**v4.1.0**](Changelog-v4#v410-2019-04-29)
- 🐞 startFrameがundefined以外でかつ、未指定の場合にエラーとなる問題を修正 ( PR [#271](https://github.com/cwtickle/danoniplus/pull/271) ) <- :boom: [**v3.13.1**](Changelog-v3#v3131-2019-04-21)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v4.4.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v4.4.0...v4.4.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v4.4.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v4.4.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v4.4.1.tar.gz) )

## v4.4.0 ([2019-05-01](https://github.com/cwtickle/danoniplus/releases/tag/v4.4.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v4.4.0/total)](https://github.com/cwtickle/danoniplus/archive/v4.4.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v4.4.0/support/v4?style=social)](https://github.com/cwtickle/danoniplus/compare/v4.4.0...support/v4#files_bucket) 
- ⭐️ タイトル画面に対して背景表示をつける機能 (backtitle_data) を追加 ( PR [#267](https://github.com/cwtickle/danoniplus/pull/267) ) 
- ⭐️ ローカルストレージを削除(初期化)するボタンを追加 ( PR [#268](https://github.com/cwtickle/danoniplus/pull/268) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v4.4.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v4.3.2...v4.4.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v4.4.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v4.4.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v4.4.0.tar.gz) )<br>❤️ ショウタ

## v4.3.2 ([2019-05-01](https://github.com/cwtickle/danoniplus/releases/tag/v4.3.2))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v4.3.2/total)](https://github.com/cwtickle/danoniplus/archive/v4.3.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v4.3.2/support/v4?style=social)](https://github.com/cwtickle/danoniplus/compare/v4.3.2...support/v4#files_bucket) 
- 🐞 スコア算出方法を独自に決めている場合、ハイスコアの差分が正しく反映されない問題を修正 ( PR [#265](https://github.com/cwtickle/danoniplus/pull/265) ) <- :boom: [**v4.3.0**](Changelog-v4#v430-2019-04-30)
- 🐞 ライフの初期値が設定の方法により異なることがある問題を修正 ( PR [#265](https://github.com/cwtickle/danoniplus/pull/265) ) <- :boom: [**v4.3.0**](Changelog-v4#v430-2019-04-30)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v4.3.2)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v4.3.1...v4.3.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v4.3.2)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v4.3.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v4.3.2.tar.gz) )

## v4.3.1 ([2019-05-01](https://github.com/cwtickle/danoniplus/releases/tag/v4.3.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v4.3.1/total)](https://github.com/cwtickle/danoniplus/archive/v4.3.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v4.3.1/support/v4?style=social)](https://github.com/cwtickle/danoniplus/compare/v4.3.1...support/v4#files_bucket) 
- 🐞 ハイスコアの初期化忘れを修正 ( PR [#263](https://github.com/cwtickle/danoniplus/pull/263) ) <- :boom: [**v4.3.0**](Changelog-v4#v430-2019-04-30)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v4.3.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v4.3.0...v4.3.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v4.3.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v4.3.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v4.3.1.tar.gz) )

## v4.3.0 ([2019-04-30](https://github.com/cwtickle/danoniplus/releases/tag/v4.3.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v4.3.0/total)](https://github.com/cwtickle/danoniplus/archive/v4.3.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v4.3.0/support/v4?style=social)](https://github.com/cwtickle/danoniplus/compare/v4.3.0...support/v4#files_bucket) 
- ⭐️ ハイスコア機能の実装 ( PR [#261](https://github.com/cwtickle/danoniplus/pull/261), Issue [#205](https://github.com/cwtickle/danoniplus/pull/205) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v4.3.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v4.2.1...v4.3.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v4.3.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v4.3.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v4.3.0.tar.gz) )<br>❤️ MFV2 (@MFV2), izkdic

## v4.2.1 ([2019-04-30](https://github.com/cwtickle/danoniplus/releases/tag/v4.2.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v4.2.1/total)](https://github.com/cwtickle/danoniplus/archive/v4.2.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v4.2.1/support/v4?style=social)](https://github.com/cwtickle/danoniplus/compare/v4.2.1...support/v4#files_bucket) 
- 🐞 マスク表示(mask_data)で、背景表示(back_data)より深度が大きいものを指定した場合、プレイ時に止まる問題を修正 ( PR [#260](https://github.com/cwtickle/danoniplus/pull/260) ) <- :boom: [**v4.2.0**](Changelog-v4#v420-2019-04-30)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v4.2.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v4.2.0...v4.2.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v4.2.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v4.2.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v4.2.1.tar.gz) )

## v4.2.0 ([2019-04-30](https://github.com/cwtickle/danoniplus/releases/tag/v4.2.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v4.2.0/total)](https://github.com/cwtickle/danoniplus/archive/v4.2.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v4.2.0/support/v4?style=social)](https://github.com/cwtickle/danoniplus/compare/v4.2.0...support/v4#files_bucket) 
- ⭐️ 画面全体にマスクを設定するためのデータ(mask_data)を実装 ( PR [#258](https://github.com/cwtickle/danoniplus/pull/258) ) 

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v4.2.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v4.1.1...v4.2.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v4.2.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v4.2.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v4.2.0.tar.gz) )<br>❤️ ショウタ

## v4.1.1 ([2019-04-29](https://github.com/cwtickle/danoniplus/releases/tag/v4.1.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v4.1.1/total)](https://github.com/cwtickle/danoniplus/archive/v4.1.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v4.1.1/support/v4?style=social)](https://github.com/cwtickle/danoniplus/compare/v4.1.1...support/v4#files_bucket) 
- 🐞 Display画面からプレイ開始できない問題を修正 ( PR [#257](https://github.com/cwtickle/danoniplus/pull/257) ) <- :boom: [**v4.0.0**](Changelog-v4#v400-2019-04-25) 

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v4.1.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v4.1.0...v4.1.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v4.1.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v4.1.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v4.1.1.tar.gz) )

## v4.1.0 ([2019-04-29](https://github.com/cwtickle/danoniplus/releases/tag/v4.1.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v4.1.0/total)](https://github.com/cwtickle/danoniplus/archive/v4.1.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v4.1.0/support/v4?style=social)](https://github.com/cwtickle/danoniplus/compare/v4.1.0...support/v4#files_bucket) 
- ⭐️ ローカルストレージ(作品別)に「Adjustment」「Volume」を追加 ( PR [#255](https://github.com/cwtickle/danoniplus/pull/255), Issue [#205](https://github.com/cwtickle/danoniplus/pull/205) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v4.1.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v4.0.1...v4.1.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v4.1.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v4.1.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v4.1.0.tar.gz) )<br>❤️ izkdic

## v4.0.1 ([2019-04-27](https://github.com/cwtickle/danoniplus/releases/tag/v4.0.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v4.0.1/total)](https://github.com/cwtickle/danoniplus/archive/v4.0.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v4.0.1/support/v4?style=social)](https://github.com/cwtickle/danoniplus/compare/v4.0.1...support/v4#files_bucket) 
- 🛠️ back_dataでX/Y座標の整数値制限を緩和（小数が使えるように）( PR [#254](https://github.com/cwtickle/danoniplus/pull/254) ) 
- 🐞 back_dataでOpacityが機能していない問題を修正 ( PR [#254](https://github.com/cwtickle/danoniplus/pull/254) ) <- :boom: [**v0.65.x**](Changelog-v0#v065x-2018-11-16)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v4.0.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v4.0.0...v4.0.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v4.0.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v4.0.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v4.0.1.tar.gz) )<br>❤️ ショウタ, izkdic

## v4.0.0 ([2019-04-25](https://github.com/cwtickle/danoniplus/releases/tag/v4.0.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v4.0.0/total)](https://github.com/cwtickle/danoniplus/archive/v4.0.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v4.0.0/support/v4?style=social)](https://github.com/cwtickle/danoniplus/compare/v4.0.0...support/v4#files_bucket) 
- ⭐️ 譜面別の複数曲読込に対応 ( PR [#252](https://github.com/cwtickle/danoniplus/pull/252) )  
- ⭐️ 譜面別に曲名を複数表示できるように変更 ( PR [#252](https://github.com/cwtickle/danoniplus/pull/252) ) 

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v4.0.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v3.13.1...v4.0.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v4.0.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v4.0.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v4.0.0.tar.gz) )<br>❤️ izkdic

[**<- v5**](Changelog-v5) | **v4** | [**v3 ->**](Changelog-v3)
