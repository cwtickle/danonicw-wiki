**[English](https://github.com/cwtickle/danoniplus-docs/wiki/keys) | Japanese** 

| [< 譜面データ中の特殊文字の取り扱い](./SpecialCharacters) | **キーの仕様について** | [KeyCtrl属性で使用するキーコード >](./KeyCtrlCodeList) |

# キー数仕様 / Key type's Specification
<img src="https://github.com/cwtickle/danoniplus/assets/44026291/3f82a63a-3c02-4784-afd2-a6a0d0c0fea8" width="50%"><img src="https://github.com/cwtickle/danoniplus/assets/44026291/2c3f2f1a-9241-4161-a7d9-4da665612d38" width="50%">

- Dancing☆Onigiri (CW Edition)における各キーは、キーとパターンの組み合わせから成っています。  
以下であれば、11keyの1番目（0番目）のパターンであることを示します。
<pre>
pos<b>11</b>_<b>0</b>
</pre>

- 同じキーには複数のパターンが紐づきます。各パターンはそれぞれが独立したキーのように個別設定できます。
<img src="https://github.com/cwtickle/danoniplus/assets/44026291/8ee66d83-1783-4a55-972d-f1ee5b79d875" width="70%">

|要素|意味|詳細|
|----|----|----|
|keyNameX|キー数の名前|キー数の名前。未指定時はXになる。|
|minWidthX|最小横幅|該当キーが確保する最小の横幅をピクセル単位で指定。デフォルトは600px。|
|keyCtrlX_Y|キーコンフィグ|各ステップに対応するキー名を指定。多次元配列で表現する。同じステップに対して複数のキーを割り当てることも可能。|
|charaX_Y|読込変数の接頭辞|left_data, down_dataなどの _dataより前の文字列を表す。|
|colorX_Y|矢印色の割り当て|譜面データ内「setColor」で記載のカラーコードを割り振る。<br>通常0～4が利用可能で、それぞれ「setColor」を配列にした場合の番号に対応した色が初期色として割り当てられる。|
|stepRtnX_Y|角度/AAの種類|左向き矢印を基準に、何度回転した矢印を配置するかを表す。<br>もしくは、対象のアスキーアート(AA)を指定する。<br>指定できるAAは、「onigiri」「giko」「iyo」「c」「morara」「monar」の6種類。<br>[共通設定ファイル#追加指定する画像のリスト](./dos_setting#%E8%BF%BD%E5%8A%A0%E6%8C%87%E5%AE%9A%E3%81%99%E3%82%8B%E7%94%BB%E5%83%8F%E3%81%AE%E3%83%AA%E3%82%B9%E3%83%88-g_presetcustomimagelist)に追加していれば、その名前も使用できる。|
|posX_Y|ステップゾーン位置|ステップゾーンを等間隔で置いた場合に、どこに置くかを示した数字。**左から右、上から下**になるように数字を割り当てる。上記画像参照。<br>等間隔にしたくない場合は、上の例のように番号をスキップすることでステップゾーンを置かないことができる。|
|divX_Y|上段折り返し位置|ステップゾーンの上段を折り返す位置を指定する。<br>指定対象は、posX_Yの値を使用。初めて下段となる数字を指定する。<br>上段/下段の括りがない場合は、最後の数字+1を指定する。|
|divMaxX_Y|下段折り返し位置|ステップゾーンの下段の最終位置を指定する。<br>divX_Y同様、posX_Yの値に準拠。<br>未指定の場合は最後の数字+1を指定する。|
|blankX_Y|ステップゾーン間隔(X座標)|ステップゾーンの間隔を指定。指定がない場合は、通常55pxとなる。|
|scaleX_Y|矢印の描画エリアの拡大・縮小|矢印の描画エリアの拡大率を指定。指定がない場合は、1(1倍)となる。|
|shuffleX_Y|シャッフルグループ|Mirror, Random, S-Random使用時、同じグループ同士で入れ替える。同じ数字が同じグループになる。|
|scrollDirX_Y|スクロール拡張の方向設定|矢印毎の方向設定。「1」は下から上へ、「-1」は上から下へ流れる。|
|assistPosX_Y|アシストするキーの設定|矢印毎のアシスト設定。「1」はオートプレイ対象、「0」はオートプレイ対象外。|
|keyGroupX_Y|部分キーの割当先指定|矢印毎に部分キー（キーグループ）をどのように割り当てるかを指定。デフォルトはすべて0(同一グループ)。|
|keyGroupOrderX_Y|部分キーグループの表示順制御|keyGroupXで定義したキーグループをキーコンフィグ側でどう表示するかを指定。デフォルトはすべて表示。|
|transKeyX_Y|別キーモードの設定|指定されたキーパターンが別キーのものである場合にそのキーの名前を指定。<u>同一キーの場合は指定しない。</u><br>指定した場合、結果画面にてここで指定したキーが補足情報として表示される。<br>また、別キーモードでプレイ中はハイスコアやキーコンフィグ等の保存対象外となる。|
|flatModeX_Y|フラットモード|`true`にするとステップゾーンを常時Flat化します。<br>デフォルトは`false`(Flat化しない)。|
|keyRetryX_Y|リトライキー|プレイ中ショートカットで、リトライが行えるキーのキーコードを指定。<br>デフォルトは8(BackSpace)。|
|keyTitleBackX_Y|タイトルバックキー|プレイ中ショートカットで、タイトルバックが行えるキーのキーコードを指定。<br>デフォルトは46(Delete)。|
|layerGroupX_Y|階層グループ|レーン別に割り当てる階層番号を指定。デフォルトは0。<br>階層は通常スクロールとリバーススクロールの2層あるが、2層分を1セットとして数える。|
|layerTransX_Y|階層別のCSS Transition設定|階層別のCSS Transitionを指定。デフォルトは空。<br>**階層グループではなく、通常スクロールとリバーススクロールの2層分個々**で指定する（階層グループが3層あれば、6層分指定するということ）。|

## 少しだけ具体例 (11keyの場合)  

- 文字で書いてもピンと来ないかもしれないので、具体的に11keyのパターン0(1番目)の例を見ていきましょう。  
- 設定値はこんな感じになっています。並び順は左から右、上から下の順です。

|要素|実際の値|
|----|----|
|keyCtrl11_0|[ [\"Left\"], [\"Down\"], [\"Up\"], [\"Right\"], [\"S\"], [\"D\"], [\"F\"], [\"Space\"], [\"J\"], [\"K\"], [\"L\"] ]|

- 対応するキー名を指定します。ここでの名前は[KeyCtrl属性で使用するキーコード](./KeyCtrlCodeList)に従った値が入ります。二次元配列になっているのは1つのキーに対して複数キーを割り当て可能にするためです。  
- 現在のバージョンでは特に指定が無くても、1つのキーに対して最低2つは割り当てできるようになっています。

|要素|実際の値|
|----|----|
|chara11_0|["sleft", "sdown", "sup", "sright", "left", "leftdia", "down", "space", "up", "rightdia", "right"]|

- レーンごとの譜面データの名前です。この設定はエディターを使うときにも利用します。
- この場合、下記のような譜面形式で読み込みができます。(数字は省略)  
```
|sleft_data=...|sdown_data=...|...|right_data=...|
```

|要素|実際の値|
|----|----|
|color11_0|[3, 3, 3, 3, 0, 1, 0, 2, 0, 1, 0]|
|setColor|#cc99ff, #ffccff, #ffffff, #ffff99, #ff9966|

- 一番上の画像において、上段は3(=#ffff99, 黄色)、下段の奇数番目は0(=#cc99ff, 紫色)、偶数番目は1(=#ffccff, ピンク)、おにぎりは2(=#ffffff, 白色)となっています。  

|要素|実際の値|
|----|----|
|stepRtn11_0|[0, -90, 90, 180, 0, -45, -90, "onigiri", 90, 135, 180]|

- 一番上の画像において、左矢印を基準にすると-90°は下矢印、90°は上矢印、180°は右矢印となります。"onigiri"の箇所はおにぎりになっていますね。  

|要素|実際の値|
|----|----|
|pos11_0|[2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]|
|div11_0|6|

- 一番上の画像の黄色文字で数字を入れました。数字が6と書いてあるところから下段に折り返しています。0と1は指定が無いので、矢印が表示されていません。  

## カスタムキーの場合  

- 自分で作ったキーを公開する場合、譜面データに記述が必要です。  
- 基本的な考え方は上述と全く同じですが、記述方法が少し異なります。  
- [カスタムキーテンプレート](./tips-0004-extrakeys)も参考にしてください。

### keyExtraList
- 追加するキーのリストです。ver31.3.1以降はキー割当設定(**keyCtrlX**)があれば自動でキー追加されるため、基本不要になりました。
<pre>
|keyExtraList=6|
</pre>

### keyNameX
- キー名とキー単位名の順に指定します。間はカンマで区切ります。(ver37.6.0以降)
- キー単位名は省略も可能です。

### minWidthX
- 区切り文字無し。具体的な値をそのまま指定します。

### appendX
- 既存キーに対して、キーパターンを追記する記法を使用する場合、`true`を指定します。  
- カスタムキーの場合には通常使用しません。
- 下記の「既存キーのパターン追加設定について」もご覧ください。

### colorX, charaX, stepRtnX, keyCtrlX, blankX, shuffleX, keyGroupX, keyGroupOrderX, flatModeX, layerGroupX
- それぞれ、colorX_Y, charaX_Y, stepRtnX_Y, keyCtrlX_Y, blankX_Y, shuffleX_Y, keyGroupX_Y, keyGroupOrderX_Y, flatModeX_Y, layerGroupX_Yに相当します。キーパターンが複数ある場合は"$"で区切ります。  
- keyCtrlの場合、代替キー指定はスラッシュ(/)で区切ります。  
- keyGroupの場合、複数の部分キーに属させたい場合はスラッシュ(/)で区切ります。  
- keyGroupで指定した部分キー（キーグループ）で使用した名前はキー変化時のキーグループ名として使用することができます。
```
|keyGroupTr=5,5,5,5,5,7i,7i,7i,7i,7i,7i,7i,11,11,11,11,11L,11L,11L,11L,11W,11W,11W,11W,12,12,12,12,11/11L/11W,11/11L/11W,11/11L/11W,11/11L/11W,11/11L/11W,11/11L/11W,11/11L/11W,12,12,12,12,12,12,12,12|
|keyGroupOrderTr=5,7i,11,11L,11W,12|
```  

#### layerTransX
- layerTransX_Yに相当します。使い方はcolorX等とほぼ同じですが、Transition名ではカンマを使用する関係で、  
階層ごとの区切り文字は**スラッシュ(/)**となります。
- 下記の例では、最初の2層は空、次の2層は「rotate(30deg)」「skew(10deg, -10deg) rotate(10deg)」が入ります。
```
|layerTransTr=//rotate(30deg)/skew(10deg, -10deg) rotate(10deg)|
```

#### colorX, shuffleX, StepRtnX の複数パターン指定
- スラッシュ(/)区切りで同一キーパターンで複数のパターンを作成できます。
```
|shuffle8i=1,1,1,2,0,0,0,0/1,1,1,1,0,0,0,0|  <- スラッシュで複数化
|shuffle8i=1,1,1,2,0,0,0,0/1,1,1,1,0,0,0,0$1,1,1,2,0,0,0,0/1,1,1,1,0,0,0,0| <- 複数化＆キーパターン毎に指定($区切り)
|shuffle8i=1,1,1,2,0,0,0,0/1,1,1,1,0,0,0,0$8i_0| <- 複数化＆キーパターン毎に指定($区切り、略記指定)
|shuffle10p=$$11i_0$11i_1| <- 1, 2番目は全て同一シャッフルグループの指定、3, 4番目は11ikeyの設定を継承
```

- 以下のような部分略記指定も可能です。
```
|shuffle8i=8_0/0,0,0,2,0,0,0,3|
// シャッフルグループ1:  8keyのグループ1から参照
// シャッフルグループ2:  指定パターン(0,0,0,2,0,0,0,3)を適用

|color11j=2,0,0,0,0,2,3,3,3,3,2/11_0/11W_0$11j_0/11L_0|
// キーパターン1:
//      カラーグループ1: 指定パターン(2,0,0,0,0,2,3,3,3,3,2)を適用
//      カラーグループ2: 11keyのグループ1から参照
//      カラーグループ3: 11Wkeyのグループ1から参照
// キーパターン2:
//      カラーグループ1～3: 先ほど定義した11jkeyのグループ1から参照
//      カラーグループ4: 11Lkeyのグループ1から参照
```

### divX
- divX_Y, divMaxX_Yを1セットで指定する形式です。カンマ区切りでdivX_Y, divMaxX_Yの順に指定します。  
- 省略した場合はposXの最大値が適用されます。キーパターンが複数ある場合は"$"で区切ります。
- パターン別に部分的な省略指定が可能です。
```
|div11f=$$6$|  // 1, 2, 4番目はposXの最大値+1を適用、3番目のみ6とする
```

### scrollX, assistX
- コロンを2つ繋げてコロンの前が名称、後が矢印ごとの設定になるよう記述します。 
- 設定を増やす場合はスラッシュ(/)で区切ります。キーパターンが複数ある場合は"$"で区切ります。 
- これらのみ、通常キーの変数の定義方法とは異なります。

|追加キーでの指定名|通常キーでの変数名|
|----|----|
|scrollX|scrollDirX_Y|
|assistX|assistPosX_Y|

### 使用例（通常カスタムキー）

|要素|指定の仕方|
|----|----|
|keyNameX|keyName6=6j|
|minWidthX|minWidth6=500|
|keyCtrlX|keyCtrl6=K,O,L,P,Semicolon,Space$K,O,L,P,Semicolon,Space|
|charaX|chara6=arrowA,arrowB,arrowC,arrowD,arrowE,arrowF$arrowA,arrowB,arrowC,arrowD,arrowE,arrowF|
|colorX|color6=0,1,0,1,0,2$0,1,0,1,0,2|
|divX|div6=6$3|
|stepRtnX|stepRtn6=0,45,-90,135,180,onigiri$0,45,-90,135,180,onigiri|
|blankX|blank6=60$60|
|scaleX|scale6=1$0.8|
|shuffleX|shuffle6=0,0,0,0,0,1$0,0,0,0,0,1|
|scrollX|scroll6=Cross::1,1,-1,-1,1,1/Split::1,1,1,-1,-1,-1/Alternate::1,-1,1,-1,1,-1$Cross::1,1,-1,-1,1,1/Split::1,1,1,-1,-1,-1/Alternate::1,-1,1,-1,1,-1|
|assistX|assist6=Onigiri::0,0,0,0,0,1/AA::0,0,0,1,1,1$...|
|transKeyX|transKey6=$6i|
|keyRetryX|keyRetry6=9|
|keyTitleBackX|keyTitleBack6=46|

### 使用例（トランスキー）
- ここでは見やすくなるように改行区切りを使っています。改行区切りは`$`区切りの代わりとして使えます。
- 同じキーグループ内では**左から右、上から下**の順に記述が必要です。
- posX（ここでは`posTr`）の記述は複数の部分キーを重ね合わせるため、同じ数字が入ることがあります。  
charaXとposX、keyGroupXをセットで比較するとどの位置にどのキーグループが対応するかがわかります。

<details>
<summary>トランスキーでの記述例</summary>

```
|minWidthTr=800|

|charaTr=
aleft,adown,aup,aright,aspace,bleft,bleftdia,bdown,bspace,bup,brightdia,bright,cleft,cdown,cup,cright,dleft,ddown,dup,dright,eleft,edown,eup,eright,fleft,fdown,fup,fright,gleft,gleftdia,gdown,gspace,gup,grightdia,gright,oni,hleft,hleftdia,hdown,hspace,hup,hrightdia,hright
aspace,aleft,adown,aup,aright,bleft,bleftdia,bdown,bspace,bup,brightdia,bright,cleft,cdown,cup,cright,dleft,ddown,dup,dright,eleft,edown,eup,eright,fleft,fdown,fup,fright,gleft,gleftdia,gdown,gspace,gup,grightdia,gright,oni,hleft,hleftdia,hdown,hspace,hup,hrightdia,hright
aleft,adown,aspace,aup,aright,bleft,bleftdia,bdown,bspace,bup,brightdia,bright,cleft,cdown,cup,cright,dleft,ddown,dup,dright,eleft,edown,eup,eright,fleft,fdown,fup,fright,gleft,gleftdia,gdown,gspace,gup,grightdia,gright,oni,hleft,hleftdia,hdown,hspace,hup,hrightdia,hright
aleft,adown,aup,aright,aspace,blefta,bleft,bleftdia,bdown,bleftb,bspace,bup,brightdia,bright,cleft,cdown,cup,cright,dleft,ddown,dup,dright,eleft,edown,eup,eright,fleft,fdown,fup,fright,oni,hleft,hleftdia,hdown,hspace,hup,hrightdia,hright,gleft,gleftdia,gdown,gspace,gup,grightdia,gright
|
|colorTr=
4,4,4,4,2,2,2,2,3,3,3,3,3,3,3,3,4,4,4,4,2,3,3,2,3,3,3,3,0,1,0,2,0,1,0,2,0,1,0,1,0,1,0
2,4,4,4,4,2,2,2,3,3,3,3,3,3,3,3,4,4,4,4,2,3,3,2,3,3,3,3,0,1,0,2,0,1,0,2,0,1,0,1,0,1,0
4,4,2,4,4,2,2,2,3,3,3,3,3,3,3,3,4,4,4,4,2,3,3,2,3,3,3,3,0,1,0,2,0,1,0,2,0,1,0,1,0,1,0
3,3,3,3,2,0,1,0,1,2,1,0,1,0,4,4,4,4,3,3,3,3,2,3,3,2,1,0,1,0,3,3,3,3,0,1,0,1,0,1,0,2,0,1,0
|
|stepRtnTr=
0,-90,90,180,onigiri,giko,onigiri,iyo,0,-90,90,180,0,-90,90,180,0,-90,90,180,giko,135,45,iyo,0,-90,90,180,0,-45,-90,onigiri,90,135,180,onigiri,0,30,60,90,120,150,180
onigiri,0,-90,90,180,giko,onigiri,iyo,0,-90,90,180,0,-90,90,180,0,-90,90,180,giko,135,45,iyo,0,-90,90,180,0,-45,-90,onigiri,90,135,180,onigiri,0,30,60,90,120,150,180
0,-90,onigiri,90,180,giko,onigiri,iyo,0,-90,90,180,0,-90,90,180,0,-90,90,180,giko,135,45,iyo,0,-90,90,180,0,-45,-90,onigiri,90,135,180,onigiri,0,30,60,90,120,150,180
0,45,135,180,giko,90,120,150,180,onigiri,0,30,60,90,0,-90,90,180,0,-90,90,180,giko,135,45,iyo,45,0,-45,-90,giko,onigiri,iyo,c,90,135,180,225,0,-45,-90,onigiri,90,135,180
|
|posTr=
1,2,3,4,5,0,1,2,3,4,5,6,2.5,3.5,4.5,5.5,0.5,1.5,2.5,3.5,0.5,2.5,3.5,5.5,2,3,4,5,7.5,8.5,9.5,10.5,11.5,12.5,13.5,7,8,9,10,11,12,13,14
1,2,3,4,5,0,1,2,3,4,5,6,2.5,3.5,4.5,5.5,0.5,1.5,2.5,3.5,0.5,2.5,3.5,5.5,2,3,4,5,7.5,8.5,9.5,10.5,11.5,12.5,13.5,7,8,9,10,11,12,13,14
1,2,3,4,5,0,1,2,3,4,5,6,2.5,3.5,4.5,5.5,0.5,1.5,2.5,3.5,0.5,2.5,3.5,5.5,2,3,4,5,7.5,8.5,9.5,10.5,11.5,12.5,13.5,7,8,9,10,11,12,13,14
3.5,4.5,5.5,6.5,7.5,1.5,2.5,3.5,4.5,5.5,6.5,7.5,8.5,9.5,3,4,5,6,5,6,7,8,3,5,6,8,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18
|
|keyGroupTr=
5,5,5,5,5,7i,7i,7i,7i,7i,7i,7i,11,11,11,11,11L,11L,11L,11L,11W,11W,11W,11W,12,12,12,12,11/11L/11W,11/11L/11W,11/11L/11W,11/11L/11W,11/11L/11W,11/11L/11W,11/11L/11W,12,12,12,12,12,12,12,12
5,5,5,5,5,7i,7i,7i,7i,7i,7i,7i,11,11,11,11,11L,11L,11L,11L,11W,11W,11W,11W,12,12,12,12,11/11L/11W,11/11L/11W,11/11L/11W,11/11L/11W,11/11L/11W,11/11L/11W,11/11L/11W,12,12,12,12,12,12,12,12
5,5,5,5,5,7i,7i,7i,7i,7i,7i,7i,11,11,11,11,11L,11L,11L,11L,11W,11W,11W,11W,12,12,12,12,11/11L/11W,11/11L/11W,11/11L/11W,11/11L/11W,11/11L/11W,11/11L/11W,11/11L/11W,12,12,12,12,12,12,12,12
5,5,5,5,5,7i,7i,7i,7i,7i,7i,7i,7i,7i,11,11,11,11,11L,11L,11L,11L,11W,11W,11W,11W,12,12,12,12,12,12,12,12,12,12,12,12,11/11L/11W,11/11L/11W,11/11L/11W,11/11L/11W,11/11L/11W,11/11L/11W,11/11L/11W
|
|divTr=7$7$7$12,19|
|keyCtrlTr=
Left,Down,Up,Right,Space,Z,X,C,Left,Down,Up,Right,Left,Down,Up,Right,W,E,D3/D4,R,D1/D2,T,Y,D0/Minus,U,I,D8/D9/0,O,S,D,F,Space,J,K,L,Space,N,J,M,K,Comma,L,Period
Space,Left,Down,Up,Right,Z,X,C,Left,Down,Up,Right,Left,Down,Up,Right,W,E,D3/D4,R,D1/D2,T,Y,D0/Minus,U,I,D8/D9/0,O,S,D,F,Space,J,K,L,Space,N,J,M,K,Comma,L,Period
D,F,Space,J,K,Z,X,C,Left,Down,Up,Right,Left,Down,Up,Right,W,E,D3/D4,R,D1/D2,T,Y,D0/Minus,U,I,D8/D9/0,O,S,D,F,Space,J,K,L,Space,N,J,M,K,Comma,L,Period
F1,F2,F3,F4,Enter/ShiftRight,D4,R,F,V,Space,N,J,I,D9,W,E,D3/D4,R,Left,Down,Up,Right,D1/D2,T,Y,D0/Minus,F1,F2,F3,F4,F5,F6,F7,F8,F9,F10,F11,F12,S,D,F,Space,J,K,L
|
|shuffleTr=
0,0,0,0,1,2,2,2,3,3,3,3,4,4,4,4,5,5,5,5,6,6,6,6,7,7,7,7,8,8,8,9,8,8,8,10,11,11,11,11,11,11,11/0,0,0,0,0,2,2,2,2,2,2,2,4,4,4,4,5,5,5,5,6,6,6,6,7,7,7,7,8,8,8,8,8,8,8,7,7,7,7,7,7,7,7
1,0,0,0,0,2,2,2,3,3,3,3,4,4,4,4,5,5,5,5,6,6,6,6,7,7,7,7,8,8,8,9,8,8,8,10,11,11,11,11,11,11,11/0,0,0,0,0,2,2,2,2,2,2,2,4,4,4,4,5,5,5,5,6,6,6,6,7,7,7,7,8,8,8,8,8,8,8,7,7,7,7,7,7,7,7
0,0,1,0,0,2,2,2,3,3,3,3,4,4,4,4,5,5,5,5,6,6,6,6,7,7,7,7,8,8,8,9,8,8,8,10,11,11,11,11,11,11,11/0,0,0,0,0,2,2,2,2,2,2,2,4,4,4,4,5,5,5,5,6,6,6,6,7,7,7,7,8,8,8,8,8,8,8,7,7,7,7,7,7,7,7
0,0,0,0,1,2,2,2,2,3,2,2,2,2,4,4,4,4,5,5,5,5,6,6,6,6,7,7,7,7,8,8,8,8,9,9,9,9,10,10,10,11,10,10,10/0,0,0,0,0,2,2,2,2,2,2,2,2,2,4,4,4,4,5,5,5,5,6,6,6,6,7,7,7,7,7,7,7,7,7,7,7,7,8,8,8,8,8,8,8
|
|scrollTr=
Reverse::-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1/Cross::1,-1,-1,1,1,1,1,-1,-1,-1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1/Flat::1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1/R-Flat::-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
Reverse::-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1/Cross::1,1,-1,-1,1,1,1,-1,-1,-1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1/Flat::1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1/R-Flat::-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
Reverse::-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1/Cross::1,-1,-1,-1,1,1,1,-1,-1,-1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1/Flat::1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1/R-Flat::-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1
---::1,1,1,1,1,1,1,-1,-1,-1,-1,-1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1/Reverse::-1,-1,-1,-1,-1,1,1,-1,-1,-1,-1,-1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,1,1,1,1,1,1,1/Cross::1,-1,-1,1,1,-1,-1,1,1,1,1,1,-1,-1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,-1,-1,-1,-1,1,1,1,1,1,1,1,1,1,1,1/Flat::1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,-1,-1,-1,-1,-1,-1,-1/R-Flat::-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,1,1,1,1,1,1,1
|
|transKeyTr=$$$Tr2|
```

</details>

### カスタムキーにおける略記指定について
- 設定が全く同じである場合に限り、別パターンの指定を丸ごと持ってくることができます。  
`(対象キー数)_(パターン番号 - 1)`の形式で指定します。  
- ただし、**blank**, **transKey** については略記に対応していません。従来通りの指定が必要です。
- 下記の例では、`6_0`が6keyのパターン1と同じ設定を持ってくることを表します。
```
|keyCtrl6=K,O,L,P,Semicolon,Space$6_0|
|chara6=arrowA,arrowB,arrowC,arrowD,arrowE,arrowF$6_0|
|color6=0,1,0,1,0,2$6_0|
|div6=6$3|
|stepRtn6=0,45,-90,135,180,onigiri$6_0|
```
- 略さずに書いた場合は下記と同等となります。
```
|keyCtrl6=K,O,L,P,Semicolon,Space$K,O,L,P,Semicolon,Space|
|chara6=arrowA,arrowB,arrowC,arrowD,arrowE,arrowF$arrowA,arrowB,arrowC,arrowD,arrowE,arrowF|
|color6=0,1,0,1,0,2$0,1,0,1,0,2|
|div6=6$3|
|stepRtn6=0,45,-90,135,180,onigiri$0,45,-90,135,180,onigiri|
```

### 既存キーのパターン追加設定について
- 既存キーの設定を維持したまま、キーパターンを追加することができます。  
|appendX=true| (Xは既存キー)を指定することで、既存キーのキーパターン記述を省略できます。
- 略記指定については、既存パターンからコピーする場合は`12_0`, `12_1`のように指定し、追加したパターンからコピーする場合は`12_(0)`, `12_(1)`のように括弧付きで指定します。  
詳細は[既存キーのキーパターン上書き](./tips-0006-keypattern-update)をご覧ください。

### その他の略記指定について (ver35.4.0以降)
#### `(開始数字)...(終了数字)`
- `(開始数字)...(終了数字)`の記法で開始から終了までカウントアップした文字列を自動生成します。
開始数字には小数を使うこともできます。
また、`(開始数字)...(+加算数)`の記法で開始から加算数を加えた分までカウントアップした文字列を自動生成します。
```
|pos9j=0...4,6...9| <-> |pos9j=0,1,2,3,4,6,7,8,9| 

|pos20=0...3,5...+3,10...+11$|
<->
|pos20=0...3,5...8,10...21$|
<->
|pos20=0,1,2,3,5,6,7,8,10,11,12,13,14,15,16,17,18,19,20,21$|
<->
|pos20=0,1,2,3,5,6,7,8,10,11,12,13,14,15,16,17,18,19,20,21$0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19|
```

#### `(対象)@:(繰り返し数)`
- `(対象)@:(繰り返し数)`の記法で対象文字を繰り返し表示することができます。
また、`(対象1!対象2!対象3)@:(繰り返し数)`で対象1～3を繰り返すこともできます。
```
|color20=4@:4,3@:4,4@:4,8_2_0$20_0|
-> |color20=4,4,4,4,3,3,3,3,4,4,4,4,8_2_0$20_0|
-> |color20=4,4,4,4,3,3,3,3,4,4,4,4,2,0,1,0,1,0,1,0$20_0|
-> |color20=4,4,4,4,3,3,3,3,4,4,4,4,2,0,1,0,1,0,1,0$4,4,4,4,3,3,3,3,4,4,4,4,2,0,1,0,1,0,1,0|

|stepRtnXX=onigiri@3|
-> |stepRtnXX=onigiri,onigiri,onigiri|

|stepRtnXX=0!-90!90!180@:3|
-> |stepRtnXX=0,-90,90,180,0,-90,90,180,0,-90,90,180|
```

#### 部分キー定義
- 通常のキー定義とは別に、カスタムキー定義用の部分キー定義があります。  
これを使うことで、簡潔に記載が可能です。
```javascript
g_keyObj = {

    // 頻度の高い譜面データ名パターン
    // 後で chara4A, chara4A_a, chara4A_b, ... に変換する
    ptchara4A: [`left`, `down`, `up`, `right`],
    ptchara3S: [`left`, `leftdia`, `down`],
    ptchara3J: [`up`, `rightdia`, `right`],
    ptchara7: [`left`, `leftdia`, `down`, `space`, `up`, `rightdia`, `right`],

    // 頻度の高い部分ShapeGroupの定義
    stepRtn4A: [0, -90, 90, 180],
    stepRtn3S: [0, -45, -90],
    stepRtn3J: [90, 135, 180],
    stepRtn3Z: [`giko`, `onigiri`, `iyo`],

    // 頻度の高い部分キーコンフィグの定義
    keyCtrl4A: [[`Left`], [`Down`], [`Up`], [`Right`]],
    keyCtrl4S: [[`S`], [`D`], [`E`, `R`], [`F`]],
    keyCtrl4J: [[`J`], [`K`], [`I`, `O`], [`L`]],
    keyCtrl4W: [[`W`], [`E`], [`D3`, `D4`], [`R`]],
    keyCtrl4U: [[`U`], [`I`], [`D8`, `D9`], [`O`]],
    keyCtrl3S: [[`S`], [`D`], [`F`]],
    keyCtrl3J: [[`J`], [`K`], [`L`]],
    keyCtrl3W: [[`W`], [`E`], [`R`]],
    keyCtrl3Z: [[`Z`], [`X`], [`C`]],
};
```
- 利用例
```
|keyCtrl9A=4S,Space,4J|     // S, D, E/R, F, Space, J, K, I/O, L と同じ
|chara9A=4A,space,s>4A|     // left, down, up, right, space, sleft, sdown, sup, sright と同じ
|stepRtn9A=4A,onigiri,4A|   // 0, -90, 90, 180, onigiri, 0, -90, 90, 180 と同じ

|keyCtrl18=
4W,Space,4A,3S,3Z,3J
4W,Space,4A,3S,V,B,N,3J
4W,Space,4U,3S,3Z,3J
4W,Space,4U,3S,V,B,N,3J
|
|chara18=s>4A,space,a>4A,3S,gor,oni,iyo,3J$18_0$18_0$18_0|
|stepRtn18=9A_0,3S,3Z,3J$18_0$18_0$18_0|

->
|keyCtrl18=
W,E,D3/D4,R,Space,Left,Down,Up,Right,S,D,F,Z,X,C,J,K,L
W,E,D3/D4,R,Space,Left,Down,Up,Right,S,D,F,V,B,N,J,K,L
W,E,D3/D4,R,Space,U,I,D8/D9,O,S,D,F,Z,X,C,J,K,L
W,E,D3/D4,R,Space,U,I,D8/D9,O,S,D,F,V,B,N,J,K,L|
|chara18=sleft,sdown,sup,sright,space,aleft,adown,aup,aright,left,leftdia,down,gor,oni,iyo,up,rightdia,right$18_0$18_0$18_0|
|stepRtn18=9A_0,0,-45,-90,giko,onigiri,iyo,90,135,180$18_0$18_0$18_0|
```

- `sleft, sleftdia, sdown, sspace, sup, srightdia, sright` を指定したいとき
⇒ `|charaX=s>7_0|` と指定
- `tleft, tleftdia, tdown, tup, trightdia, tright` を指定したいとき
⇒ `|charaX=t>3S,t>3J|`と指定


### カスタムキー定義における禁則文字について
- 略記指定を実装している関係で、一部定義できない設定が存在します。  
これらを名称として利用すると、別の意味となってしまうため使用しないでください。

#### charaX, stepRtnX
- 4A, 3S, 3J, 7, 4A_x, 3S_x, 3J_x, 7_x  
※ _x は_a, _b, ... _z を表す
- (キー数)_(パターン番号)  
※ 例: 5_0, 5_1, 7_0, 7i_0, ..., 23_0

## 関連項目
- [KeyCtrl属性で使用するキーコード](./KeyCtrlCodeList)
- [カスタムキーテンプレート](./tips-0004-extrakeys)
- [カスタムキーの作成方法](./tips-0015-make-custom-key-types)
- [既存キーのキーパターン上書き](./tips-0006-keypattern-update)
- [キー変化作品の実装例](./tips-0009-key-types-change)
- [カスタムキーの省略形記法](./tips-0016-custom-key-abbreviation)

## 更新履歴

|Version|変更内容|
|----|----|
|[v39.1.0](https://github.com/cwtickle/danoniplus/releases/tag/v39.1.0)|・レーンごとの階層グループ指定(layerGroupX), 階層別のCSS Transition設定(layerTransX)を追加|
|[v37.6.0](https://github.com/cwtickle/danoniplus/releases/tag/v37.6.0)|・keyNameXでキー単位名を指定できる機能を追加|
|[v35.4.0](https://github.com/cwtickle/danoniplus/releases/tag/v35.4.0)|・ステップゾーンを強制Flat化する設定(flatModeX)を追加<br>・charaXの略記拡張設定仕様を一部変更|
|[v35.3.0](https://github.com/cwtickle/danoniplus/releases/tag/v35.3.0)|・posX, colorX, shuffleX, stepRtnXの略記指定を他の設定にも拡張|
|[v35.2.1](https://github.com/cwtickle/danoniplus/releases/tag/v35.2.1)|・部分キーの定義を追加<br>・posX, colorX, shuffleX, stepRtnXの略記指定を追加|
|[v35.1.0](https://github.com/cwtickle/danoniplus/releases/tag/v35.1.0)|・部分キーの表示順制御(keyGroupOrderX)を追加|
|[v33.4.0](https://github.com/cwtickle/danoniplus/releases/tag/v33.4.0)|・折り返し位置設定(divX), 表示位置設定(posX)について下段の相対位置表記での指定に対応|
|[v32.0.0](https://github.com/cwtickle/danoniplus/releases/tag/v32.0.0)|・キー割当設定(keyCtrlX)で定義できるcode略名を拡張<br>・同一パターン内の部分略記に対応<br>・拡張スクロール設定(scrollX)におけるリバースの略記指定(`-1`の代わりに`-`を使う)に対応<br>・blankX, scaleX, keyRetryX, keyTitleBackX, transKeyXの略記指定に対応|
|[v31.4.0](https://github.com/cwtickle/danoniplus/releases/tag/v31.4.0)|・キー割当設定(keyCtrlX)について[KeyboardEvent.code](https://developer.mozilla.org/ja/docs/Web/API/KeyboardEvent/code)及び略記での指定に対応|
|[v31.3.1](https://github.com/cwtickle/danoniplus/releases/tag/v31.3.1)|・カスタムキーリスト(keyExtraList)の指定を原則不要化<br>・読込変数の接頭辞(charaX)を任意化|
|[v31.0.0](https://github.com/cwtickle/danoniplus/releases/tag/v31.0.0)|・カスタムキー定義(stepRtnX)について同一キーパターンの複数化に対応|
|[v30.5.0](https://github.com/cwtickle/danoniplus/releases/tag/v30.5.0)|・既存キーへのキーパターン追記時に相対パターン番号を略記で指定できるよう変更<br>・カスタムキー定義(scrollX,assistX)について部分省略指定に対応|
|[v30.4.0](https://github.com/cwtickle/danoniplus/releases/tag/v30.4.0)|・カスタムキー定義(colorX,shuffleX)について部分省略指定に対応|
|[v30.3.0](https://github.com/cwtickle/danoniplus/releases/tag/v30.3.0)|・カスタムキー定義(divX,shuffleX)について空指定に対応|
|[v30.0.1](https://github.com/cwtickle/danoniplus/releases/tag/v30.0.1)|・矢印毎の部分キー割当設定(keyGroupX)を追加|
|[v28.2.0](https://github.com/cwtickle/danoniplus/releases/tag/v28.2.0)|・カスタムキー定義(colorX,shuffleX)について同一キーパターンの複数化に対応|
|[v27.6.0](https://github.com/cwtickle/danoniplus/releases/tag/v27.6.0)|・キーパターンの追記設定(appendX)を追加|
|[v27.5.0](https://github.com/cwtickle/danoniplus/releases/tag/v27.5.0)|・カスタムキー定義の一部(colorX, charaX, stepRtnX, keyCtrlX, posX, shuffleX, scrollX, assistX)について$区切りの代替として改行区切りに対応|
|[v27.1.0](https://github.com/cwtickle/danoniplus/releases/tag/v27.1.0)|・キー別の最小横幅(minWidthX)を追加|
|[v26.5.0](https://github.com/cwtickle/danoniplus/releases/tag/v26.5.0)|・カスタムキー定義のステップゾーン位置(posX)について小数点に対応|
|[v26.2.0](https://github.com/cwtickle/danoniplus/releases/tag/v26.2.0)|・作品別のカスタムキー定義に限り、keyExtraList項目を任意化|
|[v24.3.0](https://github.com/cwtickle/danoniplus/releases/tag/v24.3.0)|・スクロール拡張、キーアシストの名称設定(scrollNameX, assistNameX)を廃止<br>・カスタムキー設定において別パターンの略記指定を追加|
|[v23.4.0](https://github.com/cwtickle/danoniplus/releases/tag/v23.4.0)|・キー数の名前設定(keyNameX)を追加|
|[v15.0.0](https://github.com/cwtickle/danoniplus/releases/tag/v15.0.0)|・キーアシスト設定(assistNameX, assistPosX_Y)を追加|
|[v12.1.0](https://github.com/cwtickle/danoniplus/releases/tag/v12.1.0)|・矢印描画エリアを縮小する設定(scaleX_Y)を追加|
|[v10.2.1](https://github.com/cwtickle/danoniplus/releases/tag/v10.2.1)|・スクロール拡張設定(scrollNameX, scrollDirX_Y)を追加|
|[v5.11.0](https://github.com/cwtickle/danoniplus/releases/tag/v5.11.0)|・タイトルバック、リトライ用ショートカットキー設定(keyRetryX_Y, keyTtitleBackX_Y)を追加|
|[v4.8.0](https://github.com/cwtickle/danoniplus/releases/tag/v4.8.0)|・別キーとしてプレイできるモードの設定(transKeyX_Y)を追加|
|[v3.1.0](https://github.com/cwtickle/danoniplus/releases/tag/v3.1.0)|・ミラー／ランダムの設定(shuffleX_Y)を追加|
|[v1.0.0](https://github.com/cwtickle/danoniplus/releases/tag/v1.0.1)|・初期実装|

| [< 譜面データ中の特殊文字の取り扱い](./SpecialCharacters) | **キーの仕様について** | [KeyCtrl属性で使用するキーコード >](./KeyCtrlCodeList) |
