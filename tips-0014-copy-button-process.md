
[^ Tips Indexに戻る](./tips-index)

| < [ボタン処理の拡張・上書き](./tips-0005-button-expansion) | **ボタン処理のコピー** | [イベントリスナーの追加・削除](./tips-0012-event-listener) > |

# ボタン処理のコピー
- ver32.1.0より、ボタン処理を別のdiv要素にコピーすることができるようになりました。

## 使い方
- `lnkDifficulty`というidのボタン処理は下記3種類で定義されています。
```javascript
g_btnFunc.base.lnkDifficulty;   // lnkDifficultyのボタン処理
g_btnFunc.reset.lnkDifficulty;  // lnkDifficultyのボタン処理（画面遷移系）
g_btnFunc.cxt.lnkDifficulty;    // lnkDifficultyの右クリック時処理
```

- このとき、新たに作成したボタンに上記のボタン処理を設定することで処理のコピーが可能です。
```javascript
const newId = createCss2Button(`newId`, `newName`, g_btnFunc.base.lnkDifficulty, {
    x: 10, y: 10, w: 100, h: 20, siz: 16,
    resetFunc: g_btnFunc.reset.lnkDifficulty,
    cxtFunc: g_btnFunc.cxt.lnkDifficulty,
});
divRoot.appendChild(newId);  // divRootに対してdivボタンを追加する場合
```
- このnewIdに処理をさらに追加したい場合は[ボタン処理の拡張・上書き](./tips-0005-button-expansion)にて追加が可能です。
- または、次のように書くこともできます。
```javascript
const newId2 = createCss2Button(`newId2`, `newName`, 
    evt => { 
        g_btnFunc.base.lnkDifficulty(evt);
        // 追加する処理を記述
    }, 
    {
        x: 10, y: 10, w: 100, h: 20, siz: 16,
        resetFunc: evt => {
            g_btnFunc.reset.lnkDifficulty(evt);
            // 追加する処理を記述
        },
        cxtFunc: evt => {
            g_btnFunc.cxt.lnkDifficulty(evt);
            // 追加する処理を記述
        },
    });
```
- この方法では、他でこの`newId2`の処理をそのままコピーしたい場合に便利です。  
`createCss2Button`で定義したボタン処理は `g_btnFunc.base[_id]`, `g_btnFunc.reset[_id]`, `g_btnFunc.cxt[_id]`に格納されるので、
他のボタン転用時に、以下をセットすればそのまま処理が流用できます。
```javascript
g_btnFunc.base.newId2;   // newId2のボタン処理
g_btnFunc.reset.newId2;  // newId2のボタン処理（画面遷移系）
g_btnFunc.cxt.newId2;    // newId2の右クリック時処理
```

### 注意点
- コピー元の処理で使用している変数が、内部変数やその画面でのみ使用するdiv要素の場合にはうまく動かないことがあります。
- うまく行かない場合には、コピーしようとしている処理中で使用している変数やdiv要素がコピー先にあるか、確認してみてください。

## 動作確認バージョン
- [v32.1.0](https://github.com/cwtickle/danoniplus/releases/tag/v32.1.0)以降で利用可能です。

## ページ作成者
- ティックル

## 関連項目
- [ボタン処理の拡張・上書き](./tips-0005-button-expansion)
- [createCss2Button](./fnc-c0004-createCss2Button)

[^ Tips Indexに戻る](./tips-index)

| < [ボタン処理の拡張・上書き](./tips-0005-button-expansion) | **ボタン処理のコピー** | [イベントリスナーの追加・削除](./tips-0012-event-listener) > |
