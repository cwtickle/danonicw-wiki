**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-h0067-commentAutoBr) | Japanese** 

[Home](./) &gt; [譜面ヘッダー仕様](dos_header) &gt; [タイトル・結果画面の初期設定](dos_header#%EF%B8%8F-タイトル結果画面の初期設定)

| [<- commentVal](dos-h0066-commentVal) | **commentAutoBr** | [commentExternal ->](dos-h0068-commentExternal) |

## commentAutoBr
- コメント表示時に改行タグを自動挿入する設定

### 使い方
```
|commentAutoBr=false|
|commentVal=
<p>
ページ整形の関係で、改行タグを個別にしたい場合は<br>
commentAutoBrをfalseに設定してください。<br>
</p>
|
```
### 説明
[commentVal](dos-h0066-commentVal)ではデフォルトで自動で改行タグを挿入しますが、  
諸事情で挿入したくない場合に、制御するための項目です。

|値|既定|内容|
|----|----|----|
|false||commentValで改行しても改行タグを挿入しない|
|true|*|commentValで改行した場合、自動で改行タグを挿入する|


### 関連項目
- [commentVal](dos-h0066-commentVal) [:pencil:](dos-h0066-commentVal/_edit) コメント表示
- [commentExternal](dos-h0068-commentExternal) [:pencil:](dos-h0068-commentExternal/_edit) コメントを本体の外部に置く設定

### 更新履歴

|Version|変更内容|
|----|----|
|[v15.2.0](https://github.com/cwtickle/danoniplus/releases/tag/v15.2.0)|・初回実装|

| [<- commentVal](dos-h0066-commentVal) | **commentAutoBr** | [commentExternal ->](dos-h0068-commentExternal) |