**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-h0040-keyTitleBack) | Japanese** 

[Home](./) &gt; [譜面ヘッダー仕様](dos_header) &gt; [プレイ時の初期設定](dos_header#-プレイ時の初期設定) &gt; [画面表示・キーコントロール](dos_header#画面表示キーコントロール)

| [<- keyRetry](dos-h0039-keyRetry) | **keyTitleBack** || [commentVal ->](dos-h0066-commentVal) |

## keyTitleBack
- タイトルバックを行うショートカットキーの設定

### 使い方
```
|keyTitleBack=46|
|keyTitleBack=Delete|
```
### 説明
プレイ中ショートカットで、タイトルバックに割り当てるキーのキーコードもしくはキー名を指定します。  
デフォルトは46(Delete)。  
キー毎の設定である、[keyTitleBackX_Y](./keys)よりも優先されます。  

### 関連項目
- [keyRetry](./dos-h0039-keyRetry) [:pencil:](./dos-h0039-keyRetry/_edit) リトライを行うショートカットキーの設定
- [キー数仕様](./keys)
- [KeyCtrl属性で使用するキーコード](./KeyCtrlCodeList)

### 更新履歴

|Version|変更内容|
|----|----|
|[v31.4.0](https://github.com/cwtickle/danoniplus/releases/tag/v31.4.0)|・[KeyboardEvent.code](https://developer.mozilla.org/ja/docs/Web/API/KeyboardEvent/code)及び略記での指定に対応|
|[v5.10.0](https://github.com/cwtickle/danoniplus/releases/tag/v5.10.0)|・初回実装|

| [<- keyRetry](dos-h0039-keyRetry) | **keyTitleBack** || [commentVal ->](dos-h0066-commentVal) |