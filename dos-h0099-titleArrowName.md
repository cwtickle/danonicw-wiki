**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-h0099-titleArrowName) | Japanese** 

[Home](./) &gt; [譜面ヘッダー仕様](dos_header) &gt; [タイトル曲名文字(デフォルトデザイン)のエフェクト](dos_header#-タイトル曲名文字デフォルトデザインのエフェクト)

| [<- titleAnimationClass](dos-h0079-titleanimationclass) | **titleArrowName** || [autoSpread ->](dos-h0089-autoSpread) |

## titleArrowName
- タイトルの背景矢印の矢印種類及び回転量の設定

### 使い方
```
|titleArrowName=classic|
|titleArrowName=classic-thin:-90|
```

### 説明
- タイトルの背景矢印の矢印種類(ImgType)と矢印の回転量を設定します。
- コロン(`:`)区切りで矢印種類(ImgType)、回転量の順に指定します。
- ローカルファイル時はImgType:「Original」として動作します。

<img src="https://github.com/cwtickle/danoniplus/assets/44026291/f9cacacb-bdd8-45f8-a3df-569403b5e408" width="50%">

### 関連項目
- [titleArrowgrd](dos-h0032-titlegrd) [:pencil:](dos-h0032-titlegrd/_edit) グラデーション
- [共通設定ファイル仕様](./dos_setting) &gt; [デフォルトデザイン・画像設定](./dos-s0005-defaultDesign)

### 更新履歴

|Version|変更内容|
|----|----|
|[v34.7.0](https://github.com/cwtickle/danoniplus/releases/tag/v34.7.0)|・初回実装|

| [<- titleAnimationClass](dos-h0079-titleanimationclass) | **titleArrowName** || [autoSpread ->](dos-h0089-autoSpread) |
