**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-h0066-commentVal) | Japanese** 

[Home](./) &gt; [譜面ヘッダー仕様](dos_header) &gt; [タイトル・結果画面の初期設定](dos_header#%EF%B8%8F-タイトル結果画面の初期設定)

| [<- keyTitleBack](dos-h0040-keyTitleBack) || **commentVal** | [commentAutoBr ->](dos-h0067-commentAutoBr) |

## commentVal
- タイトル画面で表示するコメントの設定

### 使い方
```
|commentVal=
このような形で
コメントを入れます。
<img src="image.png" alt="画像タグや"><a href="index.html">リンクも可能です。</a>

ver26.1.0からは{g_detailObj.toolDif[0].tool}のように定義済み変数も扱えます。
|
```
### 説明
タイトル画面、もしくは本体外部にコメントを表示します。  
詳細の設定は関連譜面ヘッダーにて指定します。  
htmlタグも使えますが、シングルクォートなどの特殊文字は[譜面データ中の特殊文字の取り扱い](SpecialCharacters)に従って置き換えてください。  
なお、区切り文字の関係でダブルクォートは必須ではありません。

ver26.1.0からは定義済み変数が使えるようになりました。中括弧内の文字列が定義済み変数に置き換えられます。

#### 言語別設定 (ver24.2.1以降)

|通常設定|言語別設定(Ja)|言語別設定(En)|
|----|----|----|
|commentVal|commentVal**Ja**|commentVal**En**|

### 関連項目
- [commentAutoBr](dos-h0067-commentAutoBr) [:pencil:](dos-h0067-commentAutoBr/_edit) コメント表示時に改行タグを自動挿入する設定
- [commentExternal](dos-h0068-commentExternal) [:pencil:](dos-h0068-commentExternal/_edit) コメントを本体の外部に置く設定
- [譜面データ中の特殊文字の取り扱い](SpecialCharacters)

### 更新履歴

|Version|変更内容|
|----|----|
|[v26.1.0](https://github.com/cwtickle/danoniplus/releases/tag/v26.1.0)|・定義済み変数を埋め込みできるように変更|
|[v24.2.1](https://github.com/cwtickle/danoniplus/releases/tag/v24.2.1)|・言語別の設定に対応|
|[v15.2.0](https://github.com/cwtickle/danoniplus/releases/tag/v15.2.0)|・初回実装|

| [<- keyTitleBack](dos-h0040-keyTitleBack) || **commentVal** | [commentAutoBr ->](dos-h0067-commentAutoBr) |