**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-h0008-fadeFrame) | Japanese** 


[Home](./) &gt; [譜面ヘッダー仕様](dos_header) &gt; [プレイ時間制御・譜面位置調整](dos_header#-プレイ時間制御譜面位置調整)

| [<- endFrame](dos-h0007-endFrame) | **fadeFrame** | [adjustment ->](dos-h0009-adjustment) |

## fadeFrame
- フェードアウト開始フレーム数及びフェードアウト長（譜面別）の設定

### 使い方
```
|fadeFrame=3600,600$7200|
|fadeFrame=2:30| <- 疑似タイマー表記 (ver13.6.0以降対応)
```
### 説明
フェードアウトを開始するタイミングのフレーム数です。  
譜面ごとに設定が可能で、"$"で区切って使います。  
「endFrame」との併用が可能で、  
フェードアウトに合わせて結果画面に移行させることもできます。  

ver13.6.0以降は疑似タイマー表記(`分:秒.差分フレーム数`)に対応しています。

|番号|設定例|内容|
|----|----|----|
|1|3600|フェードアウト開始フレーム|
|2|1000|フェードアウト長(フレーム)。省略した場合は420フレーム(7秒)。|

### fadeFrameとendFrame、リザルト画面突入時間の関係
![dos-h0008-01.png](./wiki/dos-h0008-01.png)

### 関連項目
- [**difData**](dos-h0002-difData) [:pencil:](dos-h0002-difData/_edit) 譜面情報 
- [**endFrame**](dos-h0007-endFrame) [:pencil:](dos-h0007-endFrame/_edit) プレイ終了フレーム数

### 更新履歴

|Version|変更内容|
|----|----|
|[v13.6.0](https://github.com/cwtickle/danoniplus/releases/tag/v13.6.0)|・フェードアウト開始フレームの疑似タイマー表記に対応|
|[v8.0.0](https://github.com/cwtickle/danoniplus/releases/tag/v8.0.0)|・楽曲のフェードアウト長を指定できるように変更|
|[v1.0.0](https://github.com/cwtickle/danoniplus/releases/tag/v1.0.1)|・初回実装|

| [<- endFrame](dos-h0007-endFrame) | **fadeFrame** | [adjustment ->](dos-h0009-adjustment) |