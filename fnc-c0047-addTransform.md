[← オブジェクトリファレンスに戻る](ObjectReferenceIndex)  

# addTransform
### 概要

- id名、識別子別のtransformを追加・変更する関数。処理後はid名のtransform属性を再結合して反映します。
- 異なる識別子間のtransformはそれぞれ維持されます。
- 過去に使用済みの識別子を使った場合、その識別子の値のみが上書きされます。

#### 補足：g_transforms

- 下記の例の場合、次の意味になります。  
⇒ mainSprite.style.transform = `rotate(15deg) skew(10deg, 10deg) rotate(45deg)`;

```javascript
g_transforms['mainSprite'] = new Map([
    ['playWindow', `rotate(15deg) skew(10deg, 10deg)`],
    ['frzReturn', `rotate(45deg)`],
]);
```

### 引数（括弧内はデフォルト値）

|引数|型|必須|指定内容|
|----|----|----|----|
|_id|string|*|オブジェクトラベルに設定されているid名|
|_transformId|string|*|識別子。id別のtransformは、この単位で管理する。|
|_transform|string|*|CSSのtransform形式で指定。複数ある場合は半角スペースを入れる。|

### 返却値
- なし (指定された_idに合致するオブジェクトのtransform属性を更新)

### 使用例

```javascript

addTransform(`mainSprite`, `playWindow`, `rotate(15deg)`);
// -> transform: rotate(15deg)

addTransform(`mainSprite`, `frzReturn`, `rotate(5deg)`);
// -> transform: rotate(15deg) rotate(5deg)

addTransform(`mainSprite`, `frzReturn`, `rotate(10deg)`);
// -> transform: rotate(15deg) rotate(10deg)

addTransform(`mainSprite`, `frzReturn`, `rotate(15deg)`);
// -> transform: rotate(15deg) rotate(15deg)

```

# addTempTransform
### 概要

- id名に対して、一時的に別のtransformを付加する場合に使用する関数。
- ここで指定したtransformは他では転用されません。

### 引数（括弧内はデフォルト値）

|引数|型|必須|指定内容|
|----|----|----|----|
|_id|string|*|オブジェクトラベルに設定されているid名|
|_transform|string|*|CSSのtransform形式で指定。複数ある場合は半角スペースを入れる。|

## 更新履歴

|Version|変更内容|
|----|----|
|[v39.1.0](https://github.com/cwtickle/danoniplus/releases/tag/v39.1.0)|・初期実装|
