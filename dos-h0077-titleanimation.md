**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-h0077-titleanimation) | Japanese** 

[Home](./) &gt; [譜面ヘッダー仕様](dos_header) &gt; [タイトル曲名文字(デフォルトデザイン)のエフェクト](dos_header#-タイトル曲名文字デフォルトデザインのエフェクト)

| [<- titleLineHeight](dos-h0034-titlelineheight) | **titleAnimation** | [titleAnimationClass ->](dos-h0079-titleanimationclass) |

## titleAnimation (titleanimation)
- タイトル文字のアニメーション設定

### 使い方
```
|titleAnimation=upToDown,120,0,linear$spinY,120,0|
```
### 説明
タイトル文字のアニメーション方法を設定します。$区切りで2行目の設定を行えます。  
なお、このタイトル文字のアニメーションは`lblmusicTitle`の子div要素に適用します。  
このため、`lblmusicTitle`に対して直接CSSアニメーションをつけた場合は、両方の設定が反映されます。  
ご注意ください。

|番号|設定例|内容|
|----|----|----|
|1|upToDown|アニメーション名|
|2|120|アニメーションフレーム数|
|3|0|アニメーション開始遅延フレーム数（既定：0フレーム）|
|4|linear|アニメーション方法（既定：ease）|

#### 言語別設定 (ver32.7.0以降)

|通常設定|言語別設定(Ja)|言語別設定(En)|
|----|----|----|
|titleAnimation|titleAnimation**Ja**|titleAnimation**En**|
|titleanimation|titleanimation**Ja**|titleanimation**En**|

### 関連項目
- [customTitleAnimationUse](dos-h0078-customTitleAnimationUse) [:pencil:](dos-h0078-customTitleAnimationUse/_edit) タイトル文字のアニメーション有無設定
- [titlegrd / titleArrowgrd](dos-h0032-titlegrd) [:pencil:](dos-h0032-titlegrd/_edit) グラデーション
- [titleAnimationClass](dos-h0079-titleanimationclass) [:pencil:](dos-h0079-titleanimationclass/_edit) タイトル文字のアニメーションクラス設定


### 更新履歴

|Version|変更内容|
|----|----|
|[v32.7.0](https://github.com/cwtickle/danoniplus/releases/tag/v32.7.0)|・キャメル表記での指定に対応<br>・言語別の設定に対応|
|[v18.9.0](https://github.com/cwtickle/danoniplus/releases/tag/v18.9.0)|・4番目の要素（アニメーション方法）を追加|
|[v18.5.0](https://github.com/cwtickle/danoniplus/releases/tag/v18.5.0)|・初回実装|

| [<- titleLineHeight](dos-h0034-titlelineheight) | **titleAnimation** | [titleAnimationClass ->](dos-h0079-titleanimationclass) |