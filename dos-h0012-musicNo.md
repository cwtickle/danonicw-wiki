**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-h0012-musicNo) | Japanese** 

[Home](./) &gt; [譜面ヘッダー仕様](dos_header) &gt; [楽曲・譜面情報](dos_header#-楽曲譜面情報)

| [<- musicUrl](dos-h0011-musicUrl) | **musicNo** | [musicFolder ->](dos-h0013-musicFolder) |

## musicNo
- 楽曲ファイルと譜面の対応付けの設定

### 使い方
```
|musicNo=0$0$1|        // 1，2譜面目: 1曲目(a.mp3), 3譜面目: 2曲目(b.mp3) 

|musicUrl=a.mp3$b.mp3| // 1曲目: a.mp3, 2曲目: b.mp3
```
### 説明
譜面毎にどの曲を対象にするか指定します。"$"区切り。  
musicTitle, musicUrlと連動しており、1曲目＝0, 2曲目＝1, 3曲目＝2 のように指定します。

### 関連項目
- [**musicTitle**](dos-h0001-musicTitle) [:pencil:](dos-h0001-musicTitle/_edit) 楽曲／楽曲クレジット
- [**difData**](dos-h0002-difData) [:pencil:](dos-h0002-difData/_edit) 譜面情報 
- [**musicUrl**](dos-h0011-musicUrl) [:pencil:](dos-h0011-musicUrl/_edit) 楽曲ファイル名

### 更新履歴

|Version|変更内容|
|----|----|
|[v4.0.0](https://github.com/cwtickle/danoniplus/releases/tag/v4.0.0)|・初回実装|

| [<- musicUrl](dos-h0011-musicUrl) | **musicNo** | [musicFolder ->](dos-h0013-musicFolder) |