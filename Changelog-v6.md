⭐ New Features / 🛠️ Improvements / 🐞 Bug Fixes

[**<- v7**](Changelog-v7) | **v6** | [**v5 ->**](Changelog-v5)  
(**🔖 [21 Releases](https://github.com/cwtickle/danoniplus/releases?q=tag%3Av6)** )

## [Warning] 更新終了後の不具合情報
- [更新を終了したバージョンの不具合情報#v6](DeprecatedVersionBugs#v6) を参照

## v6.6.13 ([2019-10-13](https://github.com/cwtickle/danoniplus/releases/tag/v6.6.13))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v6.6.13/total)](https://github.com/cwtickle/danoniplus/archive/v6.6.13.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v6.6.13/support/v6?style=social)](https://github.com/cwtickle/danoniplus/compare/v6.6.13...support/v6#files_bucket) 
- 🐞 ダミーIDの譜面ヘッダー誤りを修正 ( PR [#483](https://github.com/cwtickle/danoniplus/pull/483) ) <- :boom: [**v6.0.0**](Changelog-v6#v600-2019-06-22)
- 🐞 ダミー矢印/フリーズアロー用カスタム関数が定義できない問題を修正 ( PR [#485](https://github.com/cwtickle/danoniplus/pull/485) ) <- :boom: [**v6.1.0**](Changelog-v6#v610-2019-06-22)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v6.6.13)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v6.6.11...v6.6.13#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v6.6.13)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v6.6.13.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v6.6.13.tar.gz) )
/ 🎣 [**v9.0.3**](./Changelog-v9#v903-2019-10-15)

## v6.6.11 ([2019-10-12](https://github.com/cwtickle/danoniplus/releases/tag/v6.6.11))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v6.6.11/total)](https://github.com/cwtickle/danoniplus/archive/v6.6.11.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v6.6.11/support/v6?style=social)](https://github.com/cwtickle/danoniplus/compare/v6.6.11...support/v6#files_bucket) 
- 🐞 低速時かつフリーズアロー開始判定時にPF判定が早まる問題を修正 ( PR [#481](https://github.com/cwtickle/danoniplus/pull/481) ) <- :boom: [**v5.7.0**](Changelog-v5#v570-2019-06-01)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v6.6.11)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v6.6.10...v6.6.11#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v6.6.11)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v6.6.11.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v6.6.11.tar.gz) )
/ 🎣 [**v9.0.1**](./Changelog-v9#v901-2019-10-12)

## v6.6.10 ([2019-10-08](https://github.com/cwtickle/danoniplus/releases/tag/v6.6.10))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v6.6.10/total)](https://github.com/cwtickle/danoniplus/archive/v6.6.10.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v6.6.10/support/v6?style=social)](https://github.com/cwtickle/danoniplus/compare/v6.6.10...support/v6#files_bucket) 
- 🐞 速度変化が同一フレームにあると正常に動作しなくなる不具合を修正 ( PR [#477](https://github.com/cwtickle/danoniplus/pull/477) ) <- :boom: **initial**

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v6.6.10)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v6.6.9...v6.6.10#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v6.6.10)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v6.6.10.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v6.6.10.tar.gz) )
/ 🎣 [**v8.7.3**](./Changelog-v8#v873-2019-10-08)

## v6.6.9 ([2019-10-06](https://github.com/cwtickle/danoniplus/releases/tag/v6.6.9))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v6.6.9/total)](https://github.com/cwtickle/danoniplus/archive/v6.6.9.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v6.6.9/support/v6?style=social)](https://github.com/cwtickle/danoniplus/compare/v6.6.9...support/v6#files_bucket) 
- 🐞 エスケープ文字の適用順序誤りを修正 ( PR [#472](https://github.com/cwtickle/danoniplus/pull/472), [#473](https://github.com/cwtickle/danoniplus/pull/473) ) <- :boom: [**v0.66.x**](Changelog-v0#v066x-2018-11-16)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v6.6.9)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v6.6.8...v6.6.9#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v6.6.9)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v6.6.9.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v6.6.9.tar.gz) )
/ 🎣 [**v8.7.2**](./Changelog-v8#v872-2019-10-06)

## v6.6.8 ([2019-09-30](https://github.com/cwtickle/danoniplus/releases/tag/v6.6.8))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v6.6.8/total)](https://github.com/cwtickle/danoniplus/archive/v6.6.8.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v6.6.8/support/v6?style=social)](https://github.com/cwtickle/danoniplus/compare/v6.6.8...support/v6#files_bucket) 
- 🐞 エラーウィンドウが表示されない問題を修正 ( PR [#459](https://github.com/cwtickle/danoniplus/pull/459) ) <- :boom: [**v2.6.0**](Changelog-v2#v260-2019-02-10)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v6.6.8)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v6.6.7...v6.6.8#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v6.6.8)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v6.6.8.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v6.6.8.tar.gz) )
/ 🎣 [**v8.6.1**](./Changelog-v8#v861-2019-09-30)

## v6.6.7 ([2019-09-23](https://github.com/cwtickle/danoniplus/releases/tag/v6.6.7))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v6.6.7/total)](https://github.com/cwtickle/danoniplus/archive/v6.6.7.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v6.6.7/support/v6?style=social)](https://github.com/cwtickle/danoniplus/compare/v6.6.7...support/v6#files_bucket) 
- 🐞 ロード画面でEnterを押すと複数回ロードが発生する問題を修正 ( PR [#432](https://github.com/cwtickle/danoniplus/pull/432) ) <- :boom: **initial**
- 🐞 メイン画面で曲中リトライキーを連打した場合に譜面がずれることがある問題を修正 ( PR [#433](https://github.com/cwtickle/danoniplus/pull/433) ) <- :boom: **initial**

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v6.6.7)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v6.6.6...v6.6.7#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v6.6.7)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v6.6.7.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v6.6.7.tar.gz) )
/ 🎣 [**v8.0.4**](./Changelog-v8#v804-2019-09-23)

## v6.6.6 ([2019-09-16](https://github.com/cwtickle/danoniplus/releases/tag/v6.6.6))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v6.6.6/total)](https://github.com/cwtickle/danoniplus/archive/v6.6.6.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v6.6.6/support/v6?style=social)](https://github.com/cwtickle/danoniplus/compare/v6.6.6...support/v6#files_bucket) 
- 🐞 ゲームオーバー時の200ミリ秒遅延により、ハイスコア表示がおかしくなることがある問題を修正 ( PR [#428](https://github.com/cwtickle/danoniplus/pull/428) ) <- :boom: **initial**

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v6.6.6)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v6.6.5...v6.6.6#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v6.6.6)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v6.6.6.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v6.6.6.tar.gz) )
/ 🎣 [**v8.0.2**](./Changelog-v8#v801-2019-09-16)

## v6.6.5 ([2019-09-15](https://github.com/cwtickle/danoniplus/releases/tag/v6.6.5))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v6.6.5/total)](https://github.com/cwtickle/danoniplus/archive/v6.6.5.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v6.6.5/support/v6?style=social)](https://github.com/cwtickle/danoniplus/compare/v6.6.5...support/v6#files_bucket) 
- 🐞 速度変化が補正込みで負のフレームにあるときの不具合を修正 ( PR [#426](https://github.com/cwtickle/danoniplus/pull/426) ) <- :boom: **initial**

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v6.6.5)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v6.6.4...v6.6.5#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v6.6.5)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v6.6.5.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v6.6.5.tar.gz) )
/ 🎣 [**v8.0.1**](./Changelog-v8#v801-2019-09-15)

## v6.6.4 ([2019-08-18](https://github.com/cwtickle/danoniplus/releases/tag/v6.6.4))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v6.6.4/total)](https://github.com/cwtickle/danoniplus/archive/v6.6.4.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v6.6.4/support/v6?style=social)](https://github.com/cwtickle/danoniplus/compare/v6.6.4...support/v6#files_bucket) 
- 🐞 タイトル・リザルトモーションでループカウンターが機能していない問題を修正 ( PR [#393](https://github.com/cwtickle/danoniplus/pull/393) ) <- :boom: [**v6.3.0**](Changelog-v6#v630-2019-06-27)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v6.6.4)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v6.6.3...v6.6.4#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v6.6.4)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v6.6.4.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v6.6.4.tar.gz) )
/ 🎣 [**v7.6.0**](./Changelog-v7#v760-2019-08-18)

## v6.6.3 ([2019-08-03](https://github.com/cwtickle/danoniplus/releases/tag/v6.6.3))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v6.6.3/total)](https://github.com/cwtickle/danoniplus/archive/v6.6.3.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v6.6.3/support/v6?style=social)](https://github.com/cwtickle/danoniplus/compare/v6.6.3...support/v6#files_bucket) 
- 🛠️ back_data, mask_dataで0フレームが指定された場合、ステップゾーンやライフゲージの表示より早く表示されるように変更 ( PR [#388](https://github.com/cwtickle/danoniplus/pull/388) )
- 🐞 back_data, mask_dataで0フレームが指定された場合に動作しないことがある問題を修正 ( PR [#388](https://github.com/cwtickle/danoniplus/pull/388) ) <- :boom: [**v4.2.0**](Changelog-v4#v420-2019-04-30), [**v0.65.x**](Changelog-v0#v065x-2018-11-16)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v6.6.3)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v6.6.2...v6.6.3#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v6.6.3)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v6.6.3.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v6.6.3.tar.gz) )
/ 🎣 [**v7.5.1**](./Changelog-v7#v751-2019-08-03)

## v6.6.2 ([2019-07-21](https://github.com/cwtickle/danoniplus/releases/tag/v6.6.2))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v6.6.2/total)](https://github.com/cwtickle/danoniplus/archive/v6.6.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v6.6.2/support/v6?style=social)](https://github.com/cwtickle/danoniplus/compare/v6.6.2...support/v6#files_bucket) 
- 🐞 リザルト画面で、フェードアウト中にTitleBack/Retryするとフェードアウト状態が引き継がれてしまう問題を修正 ( PR [#384](https://github.com/cwtickle/danoniplus/pull/384) ) <- :boom: [**v0.68.x**](Changelog-v0#v068x-2018-11-17)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v6.6.2)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v6.6.1...v6.6.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v6.6.2)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v6.6.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v6.6.2.tar.gz) )
/ 🎣 [**v7.4.0**](./Changelog-v7#v740-2019-07-21)

## v6.6.1 ([2019-07-18](https://github.com/cwtickle/danoniplus/releases/tag/v6.6.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v6.6.1/total)](https://github.com/cwtickle/danoniplus/archive/v6.6.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v6.6.1/support/v6?style=social)](https://github.com/cwtickle/danoniplus/compare/v6.6.1...support/v6#files_bucket) 
- 🐞 特殊キーを複数譜＆複数キー指定していた場合で、片方のキーのみキーコンフィグ設定を保存していた場合に、もう片方のキーのキーコンフィグが表示できないことがある問題を修正 ( PR [#377](https://github.com/cwtickle/danoniplus/pull/377) ) <- :boom: [**v6.6.0**](Changelog-v6#v660-2019-07-08)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v6.6.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v6.6.0...v6.6.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v6.6.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v6.6.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v6.6.1.tar.gz) )
/ 🎣 [**v7.3.0**](./Changelog-v7#v730-2019-07-20)

----

## v6.6.0 ([2019-07-08](https://github.com/cwtickle/danoniplus/releases/tag/v6.6.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v6.6.0/total)](https://github.com/cwtickle/danoniplus/archive/v6.6.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v6.6.0/support/v6?style=social)](https://github.com/cwtickle/danoniplus/compare/v6.6.0...support/v6#files_bucket) 
- ⭐️ 特殊キー(keyExtraList指定キー)に対してリバース、キーコンフィグ保存に対応 ( PR [#365](https://github.com/cwtickle/danoniplus/pull/365) )
- 🐞 変更したキーコンフィグ内容が反映されないことがある問題を修正 ( PR [#365](https://github.com/cwtickle/danoniplus/pull/365) ) <- :boom: [**v4.6.0**](Changelog-v4#v460-2019-05-03)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v6.6.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v6.5.1...v6.6.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v6.6.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v6.6.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v6.6.0.tar.gz) )

## v6.5.1 ([2019-07-06](https://github.com/cwtickle/danoniplus/releases/tag/v6.5.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v6.5.1/total)](https://github.com/cwtickle/danoniplus/archive/v6.5.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v6.5.1/support/v6?style=social)](https://github.com/cwtickle/danoniplus/compare/v6.5.1...support/v6#files_bucket) 
- 🐞 フェードイン時に個別加速が掛からない問題を修正 ( PR [#363](https://github.com/cwtickle/danoniplus/pull/363) ) <- :boom: **initial**
- 🐞 譜面ヘッダー(frzAttempt)が機能せず、フリーズアローを離したときの許容フレーム数が実質ゼロだった問題を修正 ( PR [#363](https://github.com/cwtickle/danoniplus/pull/363) ) <- :boom: [**v6.0.0**](Changelog-v6#v600-2019-06-22)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v6.5.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v6.5.0...v6.5.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v6.5.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v6.5.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v7.9.13.tar.gz) )<br>❤️ izkdic

## v6.5.0 ([2019-06-30](https://github.com/cwtickle/danoniplus/releases/tag/v6.5.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v6.5.0/total)](https://github.com/cwtickle/danoniplus/archive/v6.5.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v6.5.0/support/v6?style=social)](https://github.com/cwtickle/danoniplus/compare/v6.5.0...support/v6#files_bucket) 
- ⭐️ ライフ上限値の設定を追加 ( PR [#361](https://github.com/cwtickle/danoniplus/pull/361), Issue [#150](https://github.com/cwtickle/danoniplus/pull/150) )
- ⭐️ ライフ制ゲージ（Original, Light, NoRecovery）の設定を譜面ヘッダーから設定できるように変更 ( PR [#361](https://github.com/cwtickle/danoniplus/pull/361) )
- 🛠️ Gaugeオプションの「No Recovery」を「NoRecovery」に表記変更 ( PR [#361](https://github.com/cwtickle/danoniplus/pull/361) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v6.5.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v6.4.0...v6.5.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v6.5.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v6.5.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v6.5.0.tar.gz) )

## v6.4.0 ([2019-06-28](https://github.com/cwtickle/danoniplus/releases/tag/v6.4.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v6.4.0/total)](https://github.com/cwtickle/danoniplus/archive/v6.4.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v6.4.0/support/v6?style=social)](https://github.com/cwtickle/danoniplus/compare/v6.4.0...support/v6#files_bucket) 
- ⭐️ タイトルに対してマスクモーション(masktitle_data)を実装 ( PR [#359](https://github.com/cwtickle/danoniplus/pull/359) )
- ⭐️ タイトル・リザルトのマスクモーションについて、既存のボタンを有効にするかどうかの設定を追加 ( PR [#359](https://github.com/cwtickle/danoniplus/pull/359) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v6.4.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v6.3.0...v6.4.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v6.4.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v6.4.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v6.4.0.tar.gz) )

## v6.3.0 ([2019-06-27](https://github.com/cwtickle/danoniplus/releases/tag/v6.3.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v6.3.0/total)](https://github.com/cwtickle/danoniplus/archive/v6.3.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v6.3.0/support/v6?style=social)](https://github.com/cwtickle/danoniplus/compare/v6.3.0...support/v6#files_bucket) 
- ⭐️ リバース用背景・マスクモーション(backRev_data, maskRev_data)の実装 ( PR [#357](https://github.com/cwtickle/danoniplus/pull/357) )
- ⭐️ リザルトモーション(backresult_data, maskresult_data)の実装 ( PR [#357](https://github.com/cwtickle/danoniplus/pull/357) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v6.3.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v6.2.1...v6.3.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v6.3.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v6.3.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v6.3.0.tar.gz) )<br>❤️ izkdic

## v6.2.1 ([2019-06-26](https://github.com/cwtickle/danoniplus/releases/tag/v6.2.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v6.2.1/total)](https://github.com/cwtickle/danoniplus/archive/v6.2.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v6.2.1/support/v6?style=social)](https://github.com/cwtickle/danoniplus/compare/v6.2.1...support/v6#files_bucket) 
- 🐞 通常キー（キーコンフィグ保存済み）が1譜面目、独自キーが2譜面目の場合に譜面の読み込みやキーコンフィグの設定ができない問題を修正 ( PR [#355](https://github.com/cwtickle/danoniplus/pull/355) ) <- :boom: [**v4.6.0**](Changelog-v4#v460-2019-05-03)
- 🐞 `danoni_setting.js` の `g_presetGaugeCustom` において0が指定できない問題を修正 ( PR [#355](https://github.com/cwtickle/danoniplus/pull/355) ) <- :boom: [**v4.6.0**](Changelog-v4#v460-2019-05-03)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v6.2.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v6.2.0...v6.2.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v6.2.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v6.2.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v6.2.1.tar.gz) )<br>❤️ izkdic

## v6.2.0 ([2019-06-23](https://github.com/cwtickle/danoniplus/releases/tag/v6.2.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v6.2.0/total)](https://github.com/cwtickle/danoniplus/archive/v6.2.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v6.2.0/support/v6?style=social)](https://github.com/cwtickle/danoniplus/compare/v6.2.0...support/v6#files_bucket) 
- ⭐️ 配点と最大スコアを変数化し、danoni_custom/setting.jsから変更できるように対応 ( PR [#350](https://github.com/cwtickle/danoniplus/pull/350) )
- 🐞 ダミーフリーズアロー用関数の一部引数間違いを修正 ( PR [#353](https://github.com/cwtickle/danoniplus/pull/353) ) <- :boom: [**v6.1.0**](Changelog-v6#v610-2019-06-22)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v6.2.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v6.1.0...v6.2.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v6.2.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v6.2.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v6.2.0.tar.gz) )<br>❤️ すずめ (@suzme)

## v6.1.0 ([2019-06-22](https://github.com/cwtickle/danoniplus/releases/tag/v6.1.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v6.1.0/total)](https://github.com/cwtickle/danoniplus/archive/v6.1.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v6.1.0/support/v6?style=social)](https://github.com/cwtickle/danoniplus/compare/v6.1.0...support/v6#files_bucket) 
- ⭐️ ダミー矢印・フリーズアローに対してS-Random/S-Random+を実装 ( PR [#348](https://github.com/cwtickle/danoniplus/pull/348) )
- ⭐️ ダミーフリーズアローに対してカスタム判定用関数を追加 ( PR [#348](https://github.com/cwtickle/danoniplus/pull/348) )
- 🛠️ 自動判定処理周りのコードを整理 ( PR [#348](https://github.com/cwtickle/danoniplus/pull/348) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v6.1.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v6.0.0...v6.1.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v6.1.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v6.1.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v6.1.0.tar.gz) )

## v6.0.0 ([2019-06-22](https://github.com/cwtickle/danoniplus/releases/tag/v6.0.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v6.0.0/total)](https://github.com/cwtickle/danoniplus/archive/v6.0.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v6.0.0/support/v6?style=social)](https://github.com/cwtickle/danoniplus/compare/v6.0.0...support/v6#files_bucket) 
- ⭐️ ダミー矢印・フリーズアロー機能の実装 ( PR [#346](https://github.com/cwtickle/danoniplus/pull/346) )
- 🛠️ メイン処理を中心にコードを整理・見直し ( PR [#346](https://github.com/cwtickle/danoniplus/pull/346) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v6.0.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v5.12.2...v6.0.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v6.0.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v6.0.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v6.0.0.tar.gz) )

[**<- v7**](Changelog-v7) | **v6** | [**v5 ->**](Changelog-v5)
