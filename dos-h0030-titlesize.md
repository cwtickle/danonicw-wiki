**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-h0030-titlesize) | Japanese** 

[Home](./) &gt; [譜面ヘッダー仕様](dos_header) &gt; [タイトル曲名文字(デフォルトデザイン)のエフェクト](dos_header#-タイトル曲名文字デフォルトデザインのエフェクト)

| [<- customReadyUse](dos-h0029-customReadyUse) || **titleSize** | [titleFont ->](dos-h0031-titlefont) |

## titleSize (titlesize)
- タイトルの文字サイズ

### 使い方
```
|titleSize=40|
|titleSize=40,20|
|titleSize=40$20|
```
### 説明
デフォルトの曲名表示を使用した場合に、曲名のフォントサイズを指定します。  
指定しない場合は、曲名の文字数に合わせて自動でサイズが決定されます。  
ver3.5.0より曲名表示が最大2行となり、カンマ区切り(もしくは$区切り)で2行目が指定できるようになりました。  

#### 言語別設定 (ver29.3.0以降)

|通常設定|言語別設定(Ja)|言語別設定(En)|
|----|----|----|
|titleSize|titleSize**Ja**|titleSize**En**|
|titlesize|titlesize**Ja**|titlesize**En**|

### タイトル曲名文字(デフォルトデザイン)の設定箇所について
![dos-h0030-01.png](./wiki/dos-h0030-01.png)

### 関連項目
- [**musicTitle**](dos-h0001-musicTitle) [:pencil:](dos-h0001-musicTitle/_edit) 楽曲／楽曲クレジット
- [**customTitleUse**](dos-h0025-customTitleUse) [:pencil:](dos-h0025-customTitleUse/_edit) タイトルの曲名文字
- [customTitleArrowUse](dos-h0026-customTitleArrowUse) [:pencil:](dos-h0026-customTitleArrowUse/_edit) タイトルの背景矢印
- [**titleFont**](dos-h0031-titlefont) [:pencil:](dos-h0031-titlefont/_edit) フォント
- [titlegrd / titleArrowgrd](dos-h0032-titlegrd) [:pencil:](dos-h0032-titlegrd/_edit) グラデーション
- [titlePos](dos-h0033-titlepos) [:pencil:](dos-h0033-titlepos/_edit) X, Y座標位置
- [**titleLineHeight**](dos-h0034-titlelineheight) [:pencil:](dos-h0034-titlelineheight/_edit) 複数行の際の行間

### 更新履歴

|Version|変更内容|
|----|----|
|[v32.7.0](https://github.com/cwtickle/danoniplus/releases/tag/v32.7.0)|・キャメル表記での指定に対応|
|[v29.3.0](https://github.com/cwtickle/danoniplus/releases/tag/v29.3.0)|・言語別の設定に対応|
|[v17.3.0](https://github.com/cwtickle/danoniplus/releases/tag/v17.3.0)|・曲名表示の2行対応で$区切りにも対応|
|[v3.5.0](https://github.com/cwtickle/danoniplus/releases/tag/v3.5.0)|・曲名表示の2行対応。<br>　カンマ区切りでそれぞれの行サイズを指定できるように変更|
|[v2.1.0](https://github.com/cwtickle/danoniplus/releases/tag/v2.1.0)|・初回実装|

| [<- customReadyUse](dos-h0029-customReadyUse) || **titleSize** | [titleFont ->](dos-h0031-titlefont) |