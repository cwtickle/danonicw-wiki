⭐ New Features / 🛠️ Improvements / 🐞 Bug Fixes  
( Files changed: ⚪ danoni_main.js / 🔵 danoni_constants.js / 🔴 css files )

[**<- v16**](Changelog-v16) | **v15** | [**v14 ->**](Changelog-v14)  
(**🔖 [18 Releases](https://github.com/cwtickle/danoniplus/releases?q=tag%3Av15)** )

## [Warning] 更新終了後の不具合情報
- [更新を終了したバージョンの不具合情報#v15](DeprecatedVersionBugs#v15) を参照

## v15.7.5 ([2020-10-16](https://github.com/cwtickle/danoniplus/releases/tag/v15.7.5))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v15.7.5/total)](https://github.com/cwtickle/danoniplus/archive/v15.7.5.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v15.7.5/support/v15?style=social)](https://github.com/cwtickle/danoniplus/compare/v15.7.5...support/v15#files_bucket)
- 🐞 矢印塗りつぶし有、ステップゾーンをOFFにした場合に矢印塗りつぶし部分が残る問題を修正 ( PR [#866](https://github.com/cwtickle/danoniplus/pull/866) ) <- :boom: [**v5.12.0**](Changelog-v5#v5120-2019-06-14)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v15.7.5)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v15.7.4...v15.7.5#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v15.7.5)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v15.7.5.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v15.7.5.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v15.7.5/js/danoni_main.js)
/ 🎣 [**v17.4.3**](./Changelog-v17#v1743-2020-10-12)

## v15.7.4 ([2020-10-09](https://github.com/cwtickle/danoniplus/releases/tag/v15.7.4))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v15.7.4/total)](https://github.com/cwtickle/danoniplus/archive/v15.7.4.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v15.7.4/support/v15?style=social)](https://github.com/cwtickle/danoniplus/compare/v15.7.4...support/v15#files_bucket) 
- 🐞 tuningのみ指定があった場合に制作者リンクが出ない問題を修正 ( PR [#850](https://github.com/cwtickle/danoniplus/pull/850) ) <- :boom: [**v3.0.0**](Changelog-v3#v300-2019-02-25)
- 🐞 ツール値出力時、時間表示がおかしくなることがある問題を修正 ( PR [#858](https://github.com/cwtickle/danoniplus/pull/858) ) <- :boom: [**v12.0.0**](Changelog-v12#v1200-2020-02-09) <- [**v2.0.0**](Changelog-v2#v200-2019-01-18)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v15.7.4)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v15.7.2...v15.7.4#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v15.7.4)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v15.7.4.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v15.7.4.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v15.7.4/js/danoni_main.js)
/ 🎣 [**v17.4.0**](./Changelog-v17#v1740-2020-10-09)

## v15.7.2 ([2020-08-24](https://github.com/cwtickle/danoniplus/releases/tag/v15.7.2))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v15.7.2/total)](https://github.com/cwtickle/danoniplus/archive/v15.7.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v15.7.2/support/v15?style=social)](https://github.com/cwtickle/danoniplus/compare/v15.7.2...support/v15#files_bucket) 
- 🐞 リトライ時に稀に早回しになる不具合を修正、キーリピートを無効化 ( PR [#810](https://github.com/cwtickle/danoniplus/pull/810) ) <- :boom: [**v4.0.0**](Changelog-v4#v400-2019-04-25)
- 🐞 キー変換処理が抜けていた部分を修正 ( PR [#811](https://github.com/cwtickle/danoniplus/pull/811) ) <- :boom: [**v11.2.0**](Changelog-v11#v1120-2020-01-04)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v15.7.2)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v15.7.1...v15.7.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v15.7.2)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v15.7.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v15.7.2.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v15.7.2/js/danoni_main.js)
/ 🎣 [**v16.2.1**](./Changelog-v16#v1621-2020-08-25)

## v15.7.1 ([2020-08-22](https://github.com/cwtickle/danoniplus/releases/tag/v15.7.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v15.7.1/total)](https://github.com/cwtickle/danoniplus/archive/v15.7.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v15.7.1/support/v15?style=social)](https://github.com/cwtickle/danoniplus/compare/v15.7.1...support/v15#files_bucket) 
- 🛠️ Applicationキーがキーコンフィグ画面、メイン画面で反応しないよう変更 ( PR [#806](https://github.com/cwtickle/danoniplus/pull/806) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v15.7.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v15.7.0...v15.7.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v15.7.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v15.7.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v15.7.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v15.7.1/js/danoni_main.js)
/ 🎣 [**v16.1.0**](./Changelog-v16#v1610-2020-08-22)

----

## v15.7.0 ([2020-08-01](https://github.com/cwtickle/danoniplus/releases/tag/v15.7.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v15.7.0/total)](https://github.com/cwtickle/danoniplus/archive/v15.7.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v15.7.0/support/v15?style=social)](https://github.com/cwtickle/danoniplus/compare/v15.7.0...support/v15#files_bucket) 
- 🛠️ Winキー+Shiftキーでキーコンが反応しないように処理を見直し ( Issue [#776](https://github.com/cwtickle/danoniplus/pull/776), PR [#777](https://github.com/cwtickle/danoniplus/pull/777) )
- 🛠️ ゲージ詳細表示部分にIDを割り当て ( Issue [#778](https://github.com/cwtickle/danoniplus/pull/778), PR [#780](https://github.com/cwtickle/danoniplus/pull/780) )
- 🛠️ 外部リンクにrel属性を追加 ( Issue [#781](https://github.com/cwtickle/danoniplus/pull/781), PR [#782](https://github.com/cwtickle/danoniplus/pull/782) )
- 🛠️ タイトル画面にオンマウス説明文を追加 ( Issue [#765](https://github.com/cwtickle/danoniplus/pull/765), PR [#784](https://github.com/cwtickle/danoniplus/pull/784) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v15.7.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v15.6.0...v15.7.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v15.7.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v15.7.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v15.7.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v15.7.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v15.7.0/js/lib/danoni_constants.js)

## v15.6.0 ([2020-07-11](https://github.com/cwtickle/danoniplus/releases/tag/v15.6.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v15.6.0/total)](https://github.com/cwtickle/danoniplus/archive/v15.6.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v15.6.0/support/v15?style=social)](https://github.com/cwtickle/danoniplus/compare/v15.6.0...support/v15#files_bucket) 
- ⭐ 7, 11, 11L, 11Wkeyにおいて12keyモードを実装 ( Issue [#769](https://github.com/cwtickle/danoniplus/pull/769), PR [#770](https://github.com/cwtickle/danoniplus/pull/770) )
- 🛠️ キーコンフィグのConfigTypeの挙動を改善 ( Issue [#767](https://github.com/cwtickle/danoniplus/pull/767), PR [#771](https://github.com/cwtickle/danoniplus/pull/771) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v15.6.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v15.5.0...v15.6.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v15.6.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v15.6.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v15.6.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v15.6.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v15.6.0/js/lib/danoni_constants.js)

<img src="./wiki/changelog/v15_0001.png" alt="v15_0001" width="60%">

## v15.5.0 ([2020-07-02](https://github.com/cwtickle/danoniplus/releases/tag/v15.5.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v15.5.0/total)](https://github.com/cwtickle/danoniplus/archive/v15.5.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v15.5.0/support/v15?style=social)](https://github.com/cwtickle/danoniplus/compare/v15.5.0...support/v15#files_bucket) 
- ⭐ 歌詞表示でフォントサイズが変更できる構文を追加 ( PR [#757](https://github.com/cwtickle/danoniplus/pull/757) )
- 🛠️ バージョン比較用のリンクをSecurity Policyに変更 ( Issue [#759](https://github.com/cwtickle/danoniplus/pull/759), PR [#760](https://github.com/cwtickle/danoniplus/pull/760) )
- 🛠️ コードの整理 ( Issue [#758](https://github.com/cwtickle/danoniplus/pull/758), PR [#761](https://github.com/cwtickle/danoniplus/pull/761) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v15.5.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v15.4.0...v15.5.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v15.5.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v15.5.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v15.5.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v15.5.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v15.5.0/js/lib/danoni_constants.js)<br>❤️ goe (@goe0)

## v15.4.0 ([2020-06-28](https://github.com/cwtickle/danoniplus/releases/tag/v15.4.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v15.4.0/total)](https://github.com/cwtickle/danoniplus/archive/v15.4.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v15.4.0/support/v15?style=social)](https://github.com/cwtickle/danoniplus/compare/v15.4.0...support/v15#files_bucket) 
- ⭐ Reverse時の歌詞の自動反転制御設定を追加 ( Issue [#754](https://github.com/cwtickle/danoniplus/pull/754), PR [#755](https://github.com/cwtickle/danoniplus/pull/755) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v15.4.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v15.3.0...v15.4.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v15.4.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v15.4.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v15.4.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v15.4.0/js/danoni_main.js)<br>❤️ izkdic

## v15.3.0 ([2020-06-27](https://github.com/cwtickle/danoniplus/releases/tag/v15.3.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v15.3.0/total)](https://github.com/cwtickle/danoniplus/archive/v15.3.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v15.3.0/support/v15?style=social)](https://github.com/cwtickle/danoniplus/compare/v15.3.0...support/v15#files_bucket) 
- ⭐ Reverse時に条件付きで歌詞を上下逆の位置に表示するよう変更 ( Issue [#747](https://github.com/cwtickle/danoniplus/pull/747), PR [#752](https://github.com/cwtickle/danoniplus/pull/752) )
- 🐞 カスタムゲージ使用時にクリア失敗時のリザルトモーションが使用できない問題を修正 ( Issue [#749](https://github.com/cwtickle/danoniplus/pull/749), PR [#751](https://github.com/cwtickle/danoniplus/pull/751) ) <- :boom: [**v9.4.0**](Changelog-v9#v940-2019-10-20)
- 🐞 主要画像をpngに設定したときの参照誤りを修正 ( Issue [#746](https://github.com/cwtickle/danoniplus/pull/746), PR [#750](https://github.com/cwtickle/danoniplus/pull/750) ) <- :boom: [**v15.1.0**](Changelog-v15#v1510-2020-05-21)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v15.3.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v15.2.2...v15.3.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v15.3.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v15.3.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v15.3.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v15.3.0/js/danoni_main.js)<br>❤️ さつき (@satsukizzz), goe (@goe0)

<img src="./wiki/changelog/v15_0004.png" alt="v15_0004" width="60%">

## v15.2.2 ([2020-06-21](https://github.com/cwtickle/danoniplus/releases/tag/v15.2.2))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v15.2.2/total)](https://github.com/cwtickle/danoniplus/archive/v15.2.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v15.2.2/support/v15?style=social)](https://github.com/cwtickle/danoniplus/compare/v15.2.2...support/v15#files_bucket) 
- 🛠️ プレイ中に他のキーイベントが呼ばれないようにするよう変更 ( PR [#741](https://github.com/cwtickle/danoniplus/pull/741) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v15.2.2)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v15.2.1...v15.2.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v15.2.2)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v15.2.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v15.2.2.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v15.2.2/js/danoni_main.js)<br>❤️ SKB (@superkuppabros)

## v15.2.1 ([2020-06-18](https://github.com/cwtickle/danoniplus/releases/tag/v15.2.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v15.2.1/total)](https://github.com/cwtickle/danoniplus/archive/v15.2.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v15.2.1/support/v15?style=social)](https://github.com/cwtickle/danoniplus/compare/v15.2.1...support/v15#files_bucket) 
- 🐞 通常キーであらかじめデフォルトセットされているキーコンフィグパターンと異なるキーを割り当てた際、初回保存時のみそのキーがリセットされる問題を修正 ( PR [#738](https://github.com/cwtickle/danoniplus/pull/738) ) <- :boom: [**v6.6.0**](Changelog-v6#v660-2019-07-08)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v15.2.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v15.2.0...v15.2.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v15.2.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v15.2.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v15.2.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v15.2.1/js/danoni_main.js)<br>❤️ izkdic

## v15.2.0 ([2020-06-13](https://github.com/cwtickle/danoniplus/releases/tag/v15.2.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v15.2.0/total)](https://github.com/cwtickle/danoniplus/archive/v15.2.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v15.2.0/support/v15?style=social)](https://github.com/cwtickle/danoniplus/compare/v15.2.0...support/v15#files_bucket) 
- ⭐ 作品コメント文をタイトル画面に表示する機能を実装 ( PR [#734](https://github.com/cwtickle/danoniplus/pull/734) )
- ⭐ 作品を動的に切り替えられる機能を実装 ( PR [#735](https://github.com/cwtickle/danoniplus/pull/735) )
- ⭐ Background:OFF時にjdgYを初期化する設定を追加 ( Issue [#714](https://github.com/cwtickle/danoniplus/pull/714), PR [#733](https://github.com/cwtickle/danoniplus/pull/733) )
- ⭐ 譜面分割、譜面番号固定をタグ指定だけでなくクエリでも指定できるように変更 ( PR [#735](https://github.com/cwtickle/danoniplus/pull/735) )
- ⭐ 曲名・アーティスト名表示の動的設定機能を実装 ( PR [#735](https://github.com/cwtickle/danoniplus/pull/735) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v15.2.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v15.1.3...v15.2.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v15.2.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v15.2.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v15.2.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v15.2.0/js/danoni_main.js)

<img src="./wiki/changelog/v15_0003.png" alt="v15_0003" width="60%">

## v15.1.3 ([2020-05-26](https://github.com/cwtickle/danoniplus/releases/tag/v15.1.3))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v15.1.3/total)](https://github.com/cwtickle/danoniplus/archive/v15.1.3.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v15.1.3/support/v15?style=social)](https://github.com/cwtickle/danoniplus/compare/v15.1.3...support/v15#files_bucket) 
- 🐞 フリーズアローヒット時の矢印色が変化しない問題を修正 ( PR [#731](https://github.com/cwtickle/danoniplus/pull/731) ) <- :boom: [**v15.1.0**](Changelog-v15#v1510-2020-05-21)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v15.1.3)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v15.1.2...v15.1.3#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v15.1.3)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v15.1.3.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v15.1.3.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v15.1.3/js/danoni_main.js)<br>❤️ ショウタ

## v15.1.2 ([2020-05-24](https://github.com/cwtickle/danoniplus/releases/tag/v15.1.2))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v15.1.2/total)](https://github.com/cwtickle/danoniplus/archive/v15.1.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v15.1.2/support/v15?style=social)](https://github.com/cwtickle/danoniplus/compare/v15.1.2...support/v15#files_bucket) 
- 🐞 特殊キーの後に通常キーの譜面があると譜面選択できない問題を修正 ( PR [#729](https://github.com/cwtickle/danoniplus/pull/729) ) <- :boom: [**v4.6.2**](Changelog-v4#v462-2019-05-04)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v15.1.2)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v15.1.1...v15.1.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v15.1.2)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v15.1.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v15.1.2.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v15.1.2/js/danoni_main.js)

## v15.1.1 ([2020-05-23](https://github.com/cwtickle/danoniplus/releases/tag/v15.1.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v15.1.1/total)](https://github.com/cwtickle/danoniplus/archive/v15.1.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v15.1.1/support/v15?style=social)](https://github.com/cwtickle/danoniplus/compare/v15.1.1...support/v15#files_bucket) 
- 🐞 MaxComboが初期化されない問題を修正 ( PR [#727](https://github.com/cwtickle/danoniplus/pull/727) ) <- :boom: [**v15.1.0**](Changelog-v15#v1510-2020-05-21)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v15.1.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v15.1.0...v15.1.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v15.1.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v15.1.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v15.1.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v15.1.1/js/danoni_main.js)

## v15.1.0 ([2020-05-21](https://github.com/cwtickle/danoniplus/releases/tag/v15.1.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v15.1.0/total)](https://github.com/cwtickle/danoniplus/archive/v15.1.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v15.1.0/support/v15?style=social)](https://github.com/cwtickle/danoniplus/compare/v15.1.0...support/v15#files_bucket) 
- ⭐ 譜面効果データでフレーム・深度等に数式が使えるよう変更 ( PR [#718](https://github.com/cwtickle/danoniplus/pull/718), [#720](https://github.com/cwtickle/danoniplus/pull/720) )
- ⭐ setVal関数に数式に変換するオプションを追加 ( PR [#718](https://github.com/cwtickle/danoniplus/pull/718) )
- 🛠️ 色変化、ゲージ、メイン部分のコード整理 ( PR [#715](https://github.com/cwtickle/danoniplus/pull/715) )
- 🛠️ 主要画像をpng形式からsvg形式へ変更 ( PR [#723](https://github.com/cwtickle/danoniplus/pull/723) )
- 🛠️ タイトル文字のサイズを見直し ( PR [#725](https://github.com/cwtickle/danoniplus/pull/725) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v15.1.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v15.0.1...v15.1.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v15.1.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v15.1.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v15.1.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v15.1.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v15.1.0/js/lib/danoni_constants.js)+(img)<br>❤️ izkdic, MFV2 (@MFV2)

## v15.0.1 ([2020-05-17](https://github.com/cwtickle/danoniplus/releases/tag/v15.0.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v15.0.1/total)](https://github.com/cwtickle/danoniplus/archive/v15.0.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v15.0.1/support/v15?style=social)](https://github.com/cwtickle/danoniplus/compare/v15.0.1...support/v15#files_bucket) 
- 🐞 フリーズアロー始点判定有効時の矢印分がライフ増減値に反映されない問題を修正 ( PR [#716](https://github.com/cwtickle/danoniplus/pull/716) ) <- :boom: [**v15.0.0**](Changelog-v15#v1500-2020-05-13)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v15.0.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v15.0.0...v15.0.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v15.0.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v15.0.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v15.0.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v15.0.1/js/danoni_main.js)<br>❤️ izkdic

## v15.0.0 ([2020-05-13](https://github.com/cwtickle/danoniplus/releases/tag/v15.0.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v15.0.0/total)](https://github.com/cwtickle/danoniplus/archive/v15.0.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v15.0.0/support/v15?style=social)](https://github.com/cwtickle/danoniplus/compare/v15.0.0...support/v15#files_bucket) 
- ⭐ AutoPlay設定に対して一部キーをアシストする設定を追加 ( Issue [#662](https://github.com/cwtickle/danoniplus/pull/662), PR [#705](https://github.com/cwtickle/danoniplus/pull/705) )
- 🛠️ 外部リンクに対してオンマウスでURLを表示 ( PR [#708](https://github.com/cwtickle/danoniplus/pull/708) )
- 🛠️ ロード画面の譜面読込前にカスタム関数を追加 ( PR [#709](https://github.com/cwtickle/danoniplus/pull/709) )
- 🛠️ AutoPlay「ON」を「ALL」に名称変更 ( PR [#705](https://github.com/cwtickle/danoniplus/pull/705) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v15.0.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v14.5.2...v15.0.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v15.0.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v15.0.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v15.0.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v15.0.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v15.0.0/js/lib/danoni_constants.js)

<img src="./wiki/changelog/v15_0002.png" alt="v15_0002" width="70%">

[**<- v16**](Changelog-v16) | **v15** | [**v14 ->**](Changelog-v14)
