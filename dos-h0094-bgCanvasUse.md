**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-h0094-bgCanvasUse) | Japanese** 

[Home](./) &gt; [譜面ヘッダー仕様](dos_header) &gt; [カスタムデータの取込](dos_header#-カスタムデータの取込)

| [<- skinType](dos-h0054-skinType) | **bgCanvasUse** | [settingType ->](dos-h0056-settingType) |

## bgCanvasUse
- デフォルトスキン時、背景としてCanvasデータを使用するかどうかの設定（ver9以前互換）
- 共通設定 ⇒ [g_presetObj.bgCanvasUse](dos-s0002-customFile#デフォルトスキン時のcanvas有効化設定-g_presetobjbgcanvasuse)

### 使い方
```
|bgCanvasUse=false|
```
### 説明
デフォルトスキン時、背景としてCanvasデータを使用するかどうかを設定します。  
htmlファイルを作成時、下記のように**Canvasタグを挟んでいるときにのみ**使える設定です。
```html
<div id="canvas-frame" style="width:600px">
    <canvas id="layer0" width="600" height="500"> <!--ここにCanvasタグがある -->
    <canvas id="layer1" width="600" height="500">
</div>
```
なおver10以降はスキンが導入されているため、サンプル用のhtmlでは下記のようにCanvasタグを含まない形になっています。
この場合は、この設定を行う・行わないによらず一律`false`と同じ扱いです。
```html
<div id="canvas-frame"></div>
```

|値|既定|内容|
|----|----|----|
|false||背景としてCanvasを使用しない|
|true|*|デフォルトスキン時、背景としてCanvasを使用する|

### 関連項目
- [**skinType**](dos-h0054-skinType) [:pencil:](dos-h0056-settingType/_edit) スキン設定

### 更新履歴

|Version|変更内容|
|----|----|
|[v33.3.0](https://github.com/cwtickle/danoniplus/releases/tag/v33.3.0)|・初回実装|

| [<- skinType](dos-h0054-skinType) | **bgCanvasUse** | [settingType ->](dos-h0056-settingType) |