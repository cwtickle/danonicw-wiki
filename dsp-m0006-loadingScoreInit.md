[← オブジェクトリファレンスに戻る](ObjectReferenceIndex)  

# プレイ開始前設定
### 概要
- 楽曲データの読込、譜面データの分解などプレイ開始前の設定を行う。
- プレイ開始前に必ず実行される。

### 処理の流れ
1. 楽曲データのファイル名・ディレクトリ名を取得、Now Loading表示を行う ( **loadMusic** 関数 )
2. 楽曲の読込処理を行う ( **setAudio**, **initWebAudioAPI** 関数 )  
※**initWebAudioAPI**はhtmlファイル直接起動時は使用しない。詳細は[オーディオ仕様](Audio)を参照。
3. iOSのみ、楽曲読込終了後に「PLAY!」ボタンを表示し押した後に処理を再開させる ( **readyToStart** 関数 )
4. 譜面データの分解、速度変化・色変化などのデータ分解、ライフ設定などを行う ( **loadScoreInit** 関数 )
5. Firefoxのみ、画像ファイルが全て読込済みであることを確認してからプレイを開始 ( **loadScoreInit** 関数 )

以上の処理が完了後、プレイ画面描画を行う ( Maininit 関数 )

### フローチャート
- 点線はローカル時もしくはエラー時の遷移を表す。
- カスタムJs/スキンJsによる差し込み処理は水色で表記。
- 関連ページがあるボックスは青枠で表記。scoreConvert関数(赤枠)の詳細についてはページ下記を参照。
```mermaid
flowchart TD
    load-->setAudio;
    setAudio-->readyToStart-->|"エンコード済み音源ファイルがある"| initWebAudioAPIfromBase64-->musicAfterLoaded;
    setAudio.->|"音源取得失敗時"| musicAfterLoaded;
    musicAfterLoaded-->loadingScoreInit;
    readyToStart-->loadMp3;
    loadMp3-->|"音源ファイルが<br>サーバー上にある"| initWebAudioAPIfromURL-->musicAfterLoaded;
    loadMp3.->|"音源ファイルが<br>ローカルファイルにある"|musicAfterLoaded;
    loadingScoreInit-->mainInit;

subgraph loadMusic
    progress-->load;
    progress.->timeout;
    progress.->error;
end;

subgraph progress
    a1["ローダー読込状況更新"]-->b1["ユーザカスタムイベント<br>(読込中)<br>g_customObj.progress"]
end;

subgraph loadingScoreInit
    A([start])-->B["譜面ファイル読込<br>(選択中譜面を含むファイルのみ)<br>loadChartFile"]-->B2["ユーザカスタムイベント<br>(譜面ロード開始前)<br>g_customObj.preloading<br>g_skinObj.preloading"]-->C["譜面データ分割<br>(preblankFrame調整前)<br>scoreConvert"]-->D["最終フレーム数取得<br>getLastFrame"]-->E["最初の矢印データがあるフレーム数を取得<br>getFirstArrowFrame"]-->F["開始フレーム数の取得(フェードイン加味)<br>getStartFrame"]-->G["フレームごとの速度を取得<br>setSpeedOnFrame"]-->H["Motionオプション適用時の矢印別の速度を取得<br>setMotionOnFrame"]-->I["最初のフレームで出現する矢印が、<br>ステップゾーンに到達するまでのフレーム数を取得<br>getFirstArrivalFrame"]
    I-->J["キーパターン(デフォルト)に対応する矢印番号を格納<br>convertReplaceNums"]
    J-->R{先頭フレームが基点位置より前}
    R-->|Yes| S["譜面データ分割<br>(preblankFrame調整後)<br>scoreConvert"]-->T[setSpeedOnFrame]-->U[getFirstArrivalFrame]-->V[シャッフル/アシスト設定]
    R-->|No| V
    V-->W["ライフ回復・ダメージ量の算出<br>calcLifeVals"]-->X
    X-->Y["メインに入る前の最終初期化処理／<br>ローカルストレージ保存処理<br>getArrowSettings"]-->L["ユーザカスタムイベント<br>(プレイ開始直前)<br>g_customObj.loading"]
-->Z([end])
end;

subgraph X["矢印・フリーズアローデータの格納<br>pushArrows"]
    x3([start])-->a3["矢印・フリーズアローの出現位置計算<br>calcNotes"]-->b3-->c3-->d3-->e3-->f3["歌詞表示、背景/マスク表示のフェードイン時調整処理<br>calcAnimationData"]-->g3["フレーム毎速度配列の作成"]-->y3([end]);
end;

subgraph a3["矢印・フリーズアローの出現位置計算 / calcNotes"]
    a4["全体速度変化の最後尾データ取得<br>getSpeedPos"]-->b4[/Loop start\]-->c4["ステップゾーンから逆算して<br>初期位置と出現フレーム数を算出<br>getArrowStartFrame"]-->d4["出現フレーム毎の矢印データを格納<br>setNotes"]-->e4[\Loop end/];
end;

subgraph b3["個別加速データの出現位置計算"]
    a5["ステップゾーンから逆算して<br>初期位置と出現フレーム数を算出<br>getArrowStartFrame"]-->b5["個別加速データの格納"];
end;

subgraph c3["個別・全体色変化のタイミング更新・データ格納 / calcDataTiming"]
    a6["ステップゾーンから逆算して<br>初期位置と出現フレーム数を算出<br>getArrowStartFrame"]-->b6["色変化データ格納<br>pushColors"];
end;

subgraph d3["モーションデータのタイミング更新・データ格納 / calcDataTiming"]
    a7["ステップゾーンから逆算して<br>初期位置と出現フレーム数を算出<br>getArrowStartFrame"]-->b7["矢印・フリーズアローモーションデータ格納<br>pushCssMotions"];
end;

subgraph e3["スクロール反転データのタイミング更新・データ格納 / calcDataTiming"]
    a8["ステップゾーンから逆算して<br>初期位置と出現フレーム数を算出<br>getArrowStartFrame"]-->b8["スクロール反転データ格納<br>pushScrollchs"];
end;

style b1 fill:#00ffff;
style B2 fill:#00ffff;
style L fill:#00ffff;
click B "https://github.com/cwtickle/danoniplus/wiki/fnc-c0011-loadChartFile" _blank;
style B stroke:#0000ff,stroke-width:4px;
style C stroke:#ff0000,stroke-width:4px;
style S stroke:#ff0000,stroke-width:4px;

```

#### scoreConvert関数部分
- データ分解時、Adjustmentの加算処理とソート処理を合わせて行っている。
```mermaid
flowchart TD
    x([start])-->scoreConvert-->y([end]);
subgraph scoreConvert
    x1([start])-->a1["Adjustmentの取得(整数・小数の分離)"]-->|"矢印・フリーズアロー"| b1["矢印データの分解"];
    b1-->c1["矢印名からフリーズアロー名の変換<br>g_escapeStr.frzName"]-->d1["フリーズアローデータの分解"];
    d1-->|"速度変化"| e1["全体速度変化名の末尾設定<br>(speed_data, speed_change)"];
    e1-->f1["速度変化データの分解<br>setSpeedData"];
    f1-->|"色変化"| g1["色変化データの分解<br>setColorData"]-->h1{"譜面詳細データ取得用？"};
    h1-->|Yes| y1([end]);
    h1-->|No| i1["全体/個別色変化のマージ処理<br>mergeColorData"];
    i1-->|"矢印・フリーズアローモーション"| j1["矢印・フリーズアローモーションデータの分解<br>setCssMotionData"];
    j1-->|"スクロール反転データ"| j2["スクロール反転データの分解<br>setScrollchData"];
    j2-->|"歌詞データ"| k1["歌詞データの分解<br>makeWordData"];
    k1-->|"背景/マスクデータ"| l1["背景/マスクデータ(プレイ画面用)の分解<br>makeBackgroundData"];
    l1-->m1["背景/マスクデータ(結果画面用)の分解<br>makeBackgroundResultData"]-->y1;
end;

style scoreConvert stroke:#ff0000,stroke-width:4px;
```

### 関連項目
- [速度変化仕様](SpeedChange)
- [オーディオ仕様](Audio)
- [ローカルストレージ仕様](LocalStorage)
- [カスタムJs(スキンJs)による処理割り込み](AboutCustomJs)
