**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-h0009-adjustment) | Japanese** 


[Home](./) &gt; [譜面ヘッダー仕様](dos_header) &gt; [プレイ時間制御・譜面位置調整](dos_header#-プレイ時間制御譜面位置調整)

| [<- fadeFrame](dos-h0008-fadeFrame) | **adjustment** | [playbackRate ->](dos-h0010-playbackRate) |

## adjustment
- 譜面開始位置の初期補正フレーム（譜面別）の設定

### 使い方
```
|adjustment=3$0$0|
```
### 説明
この値を指定することで、譜面の流れるタイミングを指定フレーム数遅く(早く)設定できます。  
$区切りで譜面単位に設定でき、単一指定で全譜面に対して適用できます。  
デフォルトは0（タイミング調整なし）です。  

### 楽曲開始位置、blankFrame、adjustmentの関係
![dos-h0006-01.png](./wiki/dos-h0006-01.png)

- v23以後は音源再生位置を調整することで、小数フレームも扱えるようになっています。  

<img src="./wiki/dos-h0008-02.png" width="90%">

### 関連項目
- [**difData**](dos-h0002-difData) [:pencil:](dos-h0002-difData/_edit) 譜面情報 
- [startFrame](dos-h0005-startFrame) [:pencil:](dos-h0005-startFrame/_edit) プレイ開始フレーム数
- [blankFrame](dos-h0006-blankFrame) [:pencil:](dos-h0006-blankFrame/_edit) 曲開始までの空白フレーム数
- [**playbackRate**](dos-h0010-playbackRate) [:pencil:](dos-h0010-playbackRate/_edit) 楽曲再生速度（主にテストプレイ用）

### 更新履歴

|Version|変更内容|
|----|----|
|[v23.0.0](https://github.com/cwtickle/danoniplus/releases/tag/v23.0.0)|・小数表記に対応|
|[v2.2.0](https://github.com/cwtickle/danoniplus/releases/tag/v2.2.0)|・初回実装|

| [<- fadeFrame](dos-h0008-fadeFrame) | **adjustment** | [playbackRate ->](dos-h0010-playbackRate) |