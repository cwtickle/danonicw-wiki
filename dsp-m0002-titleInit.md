[← オブジェクトリファレンスに戻る](ObjectReferenceIndex)  

# タイトル表示
### 概要
- タイトル画面の各項目を表示する。

### フローチャート
- カスタムJs/スキンJsによる差し込み処理は水色で表記。
- 関連ページがあるボックスは青枠で表記。
```mermaid
flowchart TD
    A([start])-->titleInit-->Z([end])

subgraph titleInit
    B["画面クリア・背景再描画<br>clearWindow"]-->C["背景矢印の表示"]-->D["backtitle_data(背景)用オブジェクトの作成"];
    D-->E-->F["警告メッセージの初期表示<br>makeWarningWindow"];
    F-->G["ユーザカスタムイベント<br>g_customObj.title"]-->H["バージョン情報取得"]-->I["ボタン作成・配置"]-->J["コメントエリア作成"]-->K["masktitle_data(マスク)用オブジェクトの作成"]-->L-->M["キー操作イベントの作成<br>setShortcutEvent"]-->N["ユーザスキン設定<br>g_skinJsObj.title"];
    B.->|"デフォルトデザイン<br>未使用時はスキップ"| D;
    D.->|"デフォルトデザイン<br>未使用時はスキップ"| F;
end;

subgraph E["タイトル・曲名文字の描画"]
    a1["曲名文字のグラデーション取得<br>makeColorGradation"]-->b1["曲名文字のフォントサイズ取得<br>getFontSize"]-->c1["曲名文字のアニメーション設定"];
end;

subgraph L["タイトル画面でのフレーム毎処理<br>flowTitleTimeline"]
    a2([start])-->b2["ユーザカスタムイベント<br>(フレーム毎処理)<br>g_customObj.titleEnterFrame"]-->c2["背景・マスクモーションの実行<br>drawTitleResultMotion"]-->d2["フレームカウント処理"]-->e2([end]).->|"フレーム毎に実行"| a2
end;

style G fill:#00ffff;
style N fill:#00ffff;
style b2 fill:#00ffff;
click B "https://github.com/cwtickle/danoniplus/wiki/fnc-c0021-clearWindow" _blank;
style B stroke:#0000ff;
style B stroke-width:4px;
click F "https://github.com/cwtickle/danoniplus/wiki/fnc-c0013-makeWarningWindow" _blank;
style F stroke:#0000ff;
style F stroke-width:4px;
click M "https://github.com/cwtickle/danoniplus/wiki/fnc-c0035-setShortcutEvent" _blank;
style M stroke:#0000ff;
style M stroke-width:4px;
click a1 "https://github.com/cwtickle/danoniplus/wiki/fnc-c0036-makeColorGradation" _blank;
style a1 stroke:#0000ff;
style a1 stroke-width:4px;
click b1 "https://github.com/cwtickle/danoniplus/wiki/fnc-c0009-getFontSize" _blank;
style b1 stroke:#0000ff;
style b1 stroke-width:4px;

```

### 関連項目
- [疑似フレーム処理仕様](AboutFrameProcessing)
- [ローカルストレージ仕様](LocalStorage)
- [カスタムJs(スキンJs)による処理割り込み](AboutCustomJs)