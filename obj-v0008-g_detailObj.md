[← オブジェクトリファレンスに戻る](ObjectReferenceIndex)  

## g_detailObj

### 概要
- 譜面詳細データを管理するためのオブジェクト。   
譜面毎の矢印数、譜面密度、難易度レベルなどをタイトル画面前に取得し、格納する。    
```javascript
const g_detailObj = {
	arrowCnt: [],
	frzCnt: [],
	maxDensity: [],
	densityData: [],
	startFrame: [],
	playingFrame: [],
	playingFrameWithBlank: [],
	speedData: [],
	boostData: [],
	toolDif: [],
};
```

### 生成タイミング
- `storeBaseData ()`呼び出し時（譜面読込時）

### プロパティ
※設定されるプロパティはすべて譜面毎の配列になっています。
- arrowCnt - 矢印数
- frzCnt - フリーズアロー数
- maxDensity - 最大密度
- densityData - 密度グラフデータ(16分割)
- startFrame - 開始フレーム数
- playingFrame - プレイフレーム数
- playingFrameWithBlank - プレイフレーム数(blankFrame込み)
- speedData - 全体速度変化数
- boostData - 個別速度変化数
- toolDif - レベル計算ツール算出値