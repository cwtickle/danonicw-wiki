**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-h0025-customTitleUse) | Japanese** 

[Home](./) &gt; [譜面ヘッダー仕様](dos_header) &gt; [デフォルトデザインの利用有無
](dos_header#-デフォルトデザインの利用有無)

| [<- syncBackPath](dos-h0087-syncBackPath) || **customTitleUse** | [customTitleArrowUse ->](dos-h0026-customTitleArrowUse) |

## customTitleUse
- タイトルの曲名文字の利用有無設定
- 共通設定 ⇒ [g_presetObj.customDesignUse](dos-s0005-defaultDesign#デフォルトデザインの使用有無設定-g_presetobjcustomdesignuse)

### 使い方
```
|customTitleUse=false|
```
### 説明
タイトル画面の曲名文字の形式を個別に設定するか、デフォルトのものを使用するかを指定します。   

|値|既定|内容|
|----|----|----|
|false|*|デフォルトの形式を採用|
|true||個別設定|

### customTitleUse の適用範囲
![dos-h0025-01.png](./wiki/dos-h0025-01.png)

### 関連項目
- [customTitleArrowUse](dos-h0026-customTitleArrowUse) [:pencil:](dos-h0026-customTitleArrowUse/_edit) タイトルの背景矢印
- [customBackUse](dos-h0027-customBackUse) [:pencil:](dos-h0027-customBackUse/_edit) 背景(プレイ画面以外)
- [背景・マスクモーション (back_data, mask_data)](dos-e0004-animationData) [:pencil:](dos-e0004-animationData/_edit) 

### 更新履歴

|Version|変更内容|
|----|----|
|[v3.6.0](https://github.com/cwtickle/danoniplus/releases/tag/v3.6.0)|・デフォルト値変更 (true -> false)|
|[v2.1.0](https://github.com/cwtickle/danoniplus/releases/tag/v2.1.0)|・初回実装|

| [<- syncBackPath](dos-h0087-syncBackPath) || **customTitleUse** | [customTitleArrowUse ->](dos-h0026-customTitleArrowUse) |