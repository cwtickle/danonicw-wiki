**[English](https://github.com/cwtickle/danoniplus-docs/wiki/Changelog-v35) | Japanese**

[^ Back to All Version List (Security Policy)](https://github.com/cwtickle/danoniplus/security/policy)  
[^ Back to Update Info](./UpdateInfo#-v35-changelog)

[**<- v36**](./Changelog-v36) | **v35** | [**v34 ->**](./Changelog-v34)  
(**🔖 [16 Releases](https://github.com/cwtickle/danoniplus/releases?q=tag%3Av35)** )

## 🔃 Files changed (v35)

| Directory | FileName            |                                                                                            | Last Updated                                |
| --------- | ------------------- | ------------------------------------------------------------------------------------------ | ------------------------------------------- |
| /js       | danoni_main.js      | [📥](https://github.com/cwtickle/danoniplus/releases/download/v35.5.7/danoni_main.js)      | **v35.5.7**                                 |
| /js/lib   | danoni_constants.js | [📥](https://github.com/cwtickle/danoniplus/releases/download/v35.5.7/danoni_constants.js) | **v35.5.7**                                 |

<details>
<summary>Changed file list before v34</summary>

| Directory | FileName                                                                                                                                               |                                                                                              | Last Updated                                |
| --------- | ------------------------------------------------------------------------------------------------------------------------------------------------------ | -------------------------------------------------------------------------------------------- | ------------------------------------------- |
| /js/lib   | danoni_localbinary.js                                                                                                                                  | [📥](https://github.com/cwtickle/danoniplus/releases/download/v15.1.0/danoni_localbinary.js) | [v15.1.0](./Changelog-v15#v1510-2020-05-21) |
| /img      | aaShadow.svg<br>arrow.svg<br>arrowShadow.svg<br>borderline.svg<br>c.svg<br>cursor.svg<br>giko.svg<br>iyo.svg<br>monar.svg<br>morara.svg<br>onigiri.svg | [📥](https://github.com/cwtickle/danoniplus/releases/download/v15.1.0/img.zip)               | [v15.1.0](./Changelog-v15#v1510-2020-05-21) |
| /css      | danoni_main.css                                                                                                                                        | [📥](https://github.com/cwtickle/danoniplus/releases/download/v34.5.1/danoni_main.css)       | [v34.5.1](./Changelog-v35#v3451-2023-11-05) |
| /skin     | danoni_skin_default.css<br>danoni_skin_light.css<br>danoni_skin_skyblue.css                                                                            | [📥](https://github.com/cwtickle/danoniplus/releases/download/v33.0.0/skin_css.zip)          | [v33.0.0](./Changelog-v33#v3300-2023-07-29) |

</details>

⭐ New Features / 🛠️ Improvements / 🐞 Bug Fixes  
( Files changed: ⚪ danoni_main.js / 🔵 danoni_constants.js / 🔴 css files )

## [Warning] 更新終了後の不具合情報
- [更新を終了したバージョンの不具合情報#v35](./DeprecatedVersionBugs#v35) を参照

## v35.5.7 ([2024-11-04](https://github.com/cwtickle/danoniplus/releases/tag/v35.5.7))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v35.5.7/total)](https://github.com/cwtickle/danoniplus/archive/v35.5.7.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v35.5.7/support/v35?style=social)](https://github.com/cwtickle/danoniplus/compare/v35.5.7...support/v35#files_bucket)

- 🛠️ ハッシュタグの前後に半角スペースを追加 ( PR [#1718](https://github.com/cwtickle/danoniplus/pull/1718) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v35.5.7)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v35.5.6...v35.5.7#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v35.5.7)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v35.5.7.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v35.5.7.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v35.5.7/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v35.5.7/js/lib/danoni_constants.js)
> / 🎣 [**v38.0,0**](./Changelog-v38#v3800-2024-11-04)

## v35.5.6 ([2024-09-25](https://github.com/cwtickle/danoniplus/releases/tag/v35.5.6))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v35.5.6/total)](https://github.com/cwtickle/danoniplus/archive/v35.5.6.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v35.5.6/support/v35?style=social)](https://github.com/cwtickle/danoniplus/compare/v35.5.6...support/v35#files_bucket)

- 🐞 **C)** ImgType で拡張子の指定がないときに、強制的に他の拡張子に変更する機能の問題を修正 ( PR [#1708](https://github.com/cwtickle/danoniplus/pull/1708) ) <- :boom: [**v23.1.0**](./Changelog-v23#v2310-2021-09-11)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v35.5.6)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v35.5.5...v35.5.6#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v35.5.6)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v35.5.6.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v35.5.6.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v35.5.6/js/danoni_main.js)
> / 🎣 [**v37.6.2**](./Changelog-v37#v3762-2024-09-25)

## v35.5.5 ([2024-08-14](https://github.com/cwtickle/danoniplus/releases/tag/v35.5.5))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v35.5.5/total)](https://github.com/cwtickle/danoniplus/archive/v35.5.5.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v35.5.5/support/v35?style=social)](https://github.com/cwtickle/danoniplus/compare/v35.5.5...support/v35#files_bucket)

- 🐞 **C)** 一部文字列の置換処理がおかしくなる問題を修正 ( PR [#1697](https://github.com/cwtickle/danoniplus/pull/1697) ) <- :boom: [**v27.0.0**](./Changelog-v27#v2700-2022-03-18)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v35.5.5)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v35.5.4...v35.5.5#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v35.5.5)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v35.5.5.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v35.5.5.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v35.5.5/js/danoni_main.js)
> / 🎣 [**v37.4.0**](./Changelog-v37#v3740-2024-08-14)

## v35.5.4 ([2024-06-25](https://github.com/cwtickle/danoniplus/releases/tag/v35.5.4))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v35.5.4/total)](https://github.com/cwtickle/danoniplus/archive/v35.5.4.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v35.5.4/support/v35?style=social)](https://github.com/cwtickle/danoniplus/compare/v35.5.4...support/v35#files_bucket)

- 🐞 **C)** 速度変化平均（AvgO, AvgB）の表示が想定より高く出ることがある問題を修正 ( PR [#1685](https://github.com/cwtickle/danoniplus/pull/1685) ) <- :boom: [**v32.2.0**](./Changelog-v32#v3220-2023-05-21)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v35.5.4)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v35.5.3...v35.5.4#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v35.5.4)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v35.5.4.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v35.5.4.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v35.5.4/js/danoni_main.js)
> / 🎣 [**v37.1.0**](./Changelog-v37#v3710-2024-06-25)

## v35.5.3 ([2024-06-20](https://github.com/cwtickle/danoniplus/releases/tag/v35.5.3))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v35.5.3/total)](https://github.com/cwtickle/danoniplus/archive/v35.5.3.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v35.5.3/support/v35?style=social)](https://github.com/cwtickle/danoniplus/compare/v35.5.3...support/v35#files_bucket)

- 🐞 **C)** Reverse のときに歌詞表示の自動反転が効かない問題を修正 ( PR [#1679](https://github.com/cwtickle/danoniplus/pull/1679), [#1681](https://github.com/cwtickle/danoniplus/pull/1681) ) <- :boom: [**v30.0.1**](./Changelog-v30#v3001-2023-02-11)
- 🐞 **C)** ダミーフリーズアローヒット時に、開始矢印の塗りつぶし色が消えてしまう問題を修正 ( PR [#1680](https://github.com/cwtickle/danoniplus/pull/1680) ) <- :boom: [**v31.0.0**](./Changelog-v31#v3100-2023-03-20)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v35.5.3)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v35.5.2...v35.5.3#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v35.5.3)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v35.5.3.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v35.5.3.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v35.5.3/js/danoni_main.js)
> / 🎣 [**v37.0.1**](./Changelog-v37#v3701-2024-06-20)

## v35.5.2 ([2024-06-08](https://github.com/cwtickle/danoniplus/releases/tag/v35.5.2))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v35.5.2/total)](https://github.com/cwtickle/danoniplus/archive/v35.5.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v35.5.2/support/v35?style=social)](https://github.com/cwtickle/danoniplus/compare/v35.5.2...support/v35#files_bucket)

- 🛠️ X のポスト用 URL の変更 ( PR [#1671](https://github.com/cwtickle/danoniplus/pull/1671) )
- 🐞 **C)** Hudden+/Sudden+で HitPosition 変更時、フィルター用のバーと実際に隠れる位置がずれる問題を修正 ( PR [#1673](https://github.com/cwtickle/danoniplus/pull/1673) ) <- :boom: [**v31.0.0**](./Changelog-v31#v3100-2023-03-20)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v35.5.2)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v35.5.1...v35.5.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v35.5.2)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v35.5.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v35.5.2.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v35.5.2/js/danoni_main.js)
> / 🎣 [**v36.6.0**](./Changelog-v36#v3660-2024-06-08)

## v35.5.1 ([2024-05-18](https://github.com/cwtickle/danoniplus/releases/tag/v35.5.1))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v35.5.1/total)](https://github.com/cwtickle/danoniplus/archive/v35.5.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v35.5.1/support/v35?style=social)](https://github.com/cwtickle/danoniplus/compare/v35.5.1...support/v35#files_bucket)

- 🛠️ 結果画面の旧 Twitter、Gitter のリンクを X, Discord に変更 ( PR [#1661](https://github.com/cwtickle/danoniplus/pull/1661) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v35.5.1)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v35.5.0...v35.5.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v35.5.1)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v35.5.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v35.5.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v35.5.1/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v35.5.1/js/lib/danoni_constants.js)
> / 🎣 [**v36.4.1**](./Changelog-v36#v3641-2024-05-18)

---

## v35.5.0 ([2024-04-03](https://github.com/cwtickle/danoniplus/releases/tag/v35.5.0))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v35.5.0/total)](https://github.com/cwtickle/danoniplus/archive/v35.5.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v35.5.0/support/v35?style=social)](https://github.com/cwtickle/danoniplus/compare/v35.5.0...support/v35#files_bucket)

- ⭐ 横幅が 900px を超えるときに、ボタンの伸縮を固定するよう変更 ( PR [#1634](https://github.com/cwtickle/danoniplus/pull/1634) )
- 🛠️ キーコンフィグ画面で横スクロールが必要な場合に、表示するよう変更 ( PR [#1634](https://github.com/cwtickle/danoniplus/pull/1634) )
- 🐞 **C)** キーコンフィグ画面でオブジェクトが画面外になるとき、キーを押しても画面が見える位置に移動しない問題を修正 ( PR [#1634](https://github.com/cwtickle/danoniplus/pull/1634) ) <- :boom: [**v30.0.1**](./Changelog-v30#v3001-2023-02-11)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v35.5.0)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v35.4.5...v35.5.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v35.5.0)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v35.5.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v35.5.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v35.5.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v35.5.0/js/lib/danoni_constants.js)

## v35.4.5 ([2024-03-28](https://github.com/cwtickle/danoniplus/releases/tag/v35.4.5))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v35.4.5/total)](https://github.com/cwtickle/danoniplus/archive/v35.4.5.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v35.4.5/support/v35?style=social)](https://github.com/cwtickle/danoniplus/compare/v35.4.5...support/v35#files_bucket)

- 🐞 **C)** カスタムキー定義の keyGroupOrderX, flatModeX のローカル保存後、その情報が取り出せない問題を修正 ( PR [#1630](https://github.com/cwtickle/danoniplus/pull/1630), [#1632](https://github.com/cwtickle/danoniplus/pull/1632) ) <- :boom: [**v35.1.0**](./Changelog-v35#v3510-2024-02-03) / [**v35.4.1**](./Changelog-v35#v3541-2024-02-23)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v35.4.5)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v35.4.3...v35.4.5#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v35.4.5)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v35.4.5.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v35.4.5.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v35.4.5/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v35.4.5/js/lib/danoni_constants.js)

## v35.4.3 ([2024-03-17](https://github.com/cwtickle/danoniplus/releases/tag/v35.4.3))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v35.4.3/total)](https://github.com/cwtickle/danoniplus/archive/v35.4.3.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v35.4.3/support/v35?style=social)](https://github.com/cwtickle/danoniplus/compare/v35.4.3...support/v35#files_bucket)

- 🐞 **C)** フリーズアローの終点と始点が近いとき、キーを押しても次のフリーズアローが反応しないことがある問題を修正 ( PR [#1627](https://github.com/cwtickle/danoniplus/pull/1627), [#1628](https://github.com/cwtickle/danoniplus/pull/1628) ) <- :boom: [**v29.0.1**](./Changelog-v29#v2901-2022-11-05)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v35.4.3)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v35.4.2...v35.4.3#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v35.4.3)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v35.4.3.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v35.4.3.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v35.4.3/js/danoni_main.js)<br>❤️ goe (@goe0), apoi

## v35.4.2 ([2024-02-29](https://github.com/cwtickle/danoniplus/releases/tag/v35.4.2))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v35.4.2/total)](https://github.com/cwtickle/danoniplus/archive/v35.4.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v35.4.2/support/v35?style=social)](https://github.com/cwtickle/danoniplus/compare/v35.4.2...support/v35#files_bucket)

- 🐞 **C)** 環境によりタイトルの背景矢印が表示されない問題を修正 ( PR [#1625](https://github.com/cwtickle/danoniplus/pull/1625) ) <- :boom: [**v34.7.0**](./Changelog-v34#v3470-2024-01-12)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v35.4.2)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v35.4.1...v35.4.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v35.4.2)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v35.4.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v35.4.2.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v35.4.2/js/danoni_main.js)

## v35.4.1 ([2024-02-23](https://github.com/cwtickle/danoniplus/releases/tag/v35.4.1))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v35.4.1/total)](https://github.com/cwtickle/danoniplus/archive/v35.4.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v35.4.1/support/v35?style=social)](https://github.com/cwtickle/danoniplus/compare/v35.4.1...support/v35#files_bucket)

- ⭐ カスタムキー定義に強制的にステップゾーンを Flat 化する設定を追加 ( PR [#1620](https://github.com/cwtickle/danoniplus/pull/1620) )
- ⭐ Display オプションの「StepZone」に「FlatBar」設定を追加 ( PR [#1620](https://github.com/cwtickle/danoniplus/pull/1620), [#1623](https://github.com/cwtickle/danoniplus/pull/1623) )
- ⭐ 上下両方にステップゾーンがある場合、両方に「Flat」の線が出るように変更 ( PR [#1620](https://github.com/cwtickle/danoniplus/pull/1620) )
- ⭐ 拡張スクロール名の末尾が「Flat」なら、自動でステップゾーンを Flat 化するよう変更 ( PR [#1620](https://github.com/cwtickle/danoniplus/pull/1620) )
- ⭐ カスタムキー定義の**charaX**における略記指定の仕様変更 ( PR [#1621](https://github.com/cwtickle/danoniplus/pull/1621) )
- 🛠️ Display オプションについて、3 つ以上の選択肢が持てるよう機能拡張 ( PR [#1620](https://github.com/cwtickle/danoniplus/pull/1620) )
- 🛠️ Display オプションにおける「Speed」を「Velocity」に変更 ( PR [#1620](https://github.com/cwtickle/danoniplus/pull/1620) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v35.4.1)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v35.3.0...v35.4.1#files_bucket)
> / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v35.4.1)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v35.4.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v35.4.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v35.4.1/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v35.4.1/js/lib/danoni_constants.js)

## v35.3.0 ([2024-02-16](https://github.com/cwtickle/danoniplus/releases/tag/v35.3.0))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v35.3.0/total)](https://github.com/cwtickle/danoniplus/archive/v35.3.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v35.3.0/support/v35?style=social)](https://github.com/cwtickle/danoniplus/compare/v35.3.0...support/v35#files_bucket)

- ⭐ カスタムキーの略記指定の組み合わせに対応 ( PR [#1618](https://github.com/cwtickle/danoniplus/pull/1618) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v35.3.0)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v35.2.1...v35.3.0#files_bucket)
> / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v35.3.0)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v35.3.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v35.3.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v35.3.0/js/danoni_main.js)

## v35.2.1 ([2024-02-08](https://github.com/cwtickle/danoniplus/releases/tag/v35.2.1))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v35.2.1/total)](https://github.com/cwtickle/danoniplus/archive/v35.2.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v35.2.1/support/v35?style=social)](https://github.com/cwtickle/danoniplus/compare/v35.2.1...support/v35#files_bucket)

- ⭐ カスタムキー用に一部の部分キー定義を標準実装 ( PR [#1614](https://github.com/cwtickle/danoniplus/pull/1614) )
- ⭐ カスタムキーの略記指定パターンを追加 ( PR [#1614](https://github.com/cwtickle/danoniplus/pull/1614), [#1616](https://github.com/cwtickle/danoniplus/pull/1616) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v35.2.1)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v35.1.0...v35.2.1#files_bucket)
> / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v35.2.1)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v35.2.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v35.2.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v35.2.1/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v35.2.1/js/lib/danoni_constants.js)

## v35.1.0 ([2024-02-03](https://github.com/cwtickle/danoniplus/releases/tag/v35.1.0))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v35.1.0/total)](https://github.com/cwtickle/danoniplus/archive/v35.1.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v35.1.0/support/v35?style=social)](https://github.com/cwtickle/danoniplus/compare/v35.1.0...support/v35#files_bucket)

- ⭐ keyGroupOrder をカスタムキー定義の 1 項目として追加 ( PR [#1612](https://github.com/cwtickle/danoniplus/pull/1612) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v35.1.0)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v35.0.0...v35.1.0#files_bucket)
> / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v35.1.0)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v35.1.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v35.1.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v35.1.0/js/danoni_main.js)

## v35.0.0 ([2024-01-28](https://github.com/cwtickle/danoniplus/releases/tag/v35.0.0))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v35.0.0/total)](https://github.com/cwtickle/danoniplus/archive/v35.0.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v35.0.0/support/v35?style=social)](https://github.com/cwtickle/danoniplus/compare/v35.0.0...support/v35#files_bucket)

- ⭐ ウィンドウサイズ（高さ）とステップゾーン位置により基準速度を変更 ( PR [#1608](https://github.com/cwtickle/danoniplus/pull/1608) )
- ⭐ Appearance 設定で可視範囲を明記するよう変更 ( PR [#1608](https://github.com/cwtickle/danoniplus/pull/1608) )
- ⭐ ウィンドウサイズ（高さ）を外部パラメータから変更できる機能を追加 ( PR [#1608](https://github.com/cwtickle/danoniplus/pull/1608) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v35.0.0)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v34.7.1...v35.0.0#files_bucket)
> / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v35.0.0)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v35.0.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v35.0.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v35.0.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v35.0.0/js/lib/danoni_constants.js)

[**<- v36**](./Changelog-v36) | **v35** | [**v34 ->**](./Changelog-v34)

| [< 更新を終了したバージョンの不具合情報](./DeprecatedVersionBugs) | **Changelog** || [譜面ヘッダー仕様 >](./dos_header) |
