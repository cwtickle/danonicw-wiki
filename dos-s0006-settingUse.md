**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-s0006-settingUse) | Japanese** 

[↑ 共通設定ファイル仕様に戻る](dos_setting)

[ <- デフォルトデザイン・画像設定 ](dos-s0005-defaultDesign) | **オプション有効化** | [プレイ画面制御 -> ](dos-s0007-viewControl)

## オプション有効化
### オプション有効化設定 (g_presetObj.settingUse)
⇒ 指定があった場合に優先される譜面ヘッダー：[settingUse](dos-h0035-settingUse), [displayUse](dos-h0057-displayUse)
- オプションをデフォルト無効化する設定を行います。  
オプション名を指定し、`false`で無効化します。  
Displayオプションの場合は、`(無効化の設定),(初期値)`の順に指定します。  
使い方は譜面ヘッダー：[settingUse](dos-h0035-settingUse)や[displayUse](dos-h0057-displayUse)をご確認ください。
```javascript
g_presetObj.settingUse = {
	motion: `true`,
	scroll: `true`,
	reverse: `true`,
	shuffle: `true`,
	autoPlay: `true`,
	gauge: `true`,
	excessive: `true`,
	appearance: `true`,

	stepZone: `true`,
	judgment: `true`,
	fastSlow: `true`,
	lifeGauge: `true,OFF`, // 複数指定の場合はこのようにカンマを入れて繋げる
	score: `true`,
	musicInfo: `false,ON`, // 複数指定の場合はこのようにカンマを入れて繋げる
	filterLine: `true`,
	speed: `true`,
	color: `true`,
	lyrics: `true`,
	background: `true`,
	arrowEffect: `true`,
	special: `true`,

	playWindow: `true`,
	stepArea: `true`,
	frzReturn : `true`,
	shaking : `true`,
	effect : `true`,
	camoufrage : `true`,
	swapping : `true`,
	judgRange : `true`,
	autoRetry : `true`,
};
```

## 更新履歴

|Version|変更内容|
|----|----|
|[v39.0.0](https://github.com/cwtickle/danoniplus/releases/tag/v39.0.0)|・オプション有効化設定 (g_presetObj.settingUse)について<br>　PlayWindow, StepArea, FrzReturn, Shaking, Effect, Camoufrage, Swapping, JudgRange, AutoRetryの設定有無を追加|
|[v37.5.0](https://github.com/cwtickle/danoniplus/releases/tag/v37.5.0)|・オプション有効化設定 (g_presetObj.settingUse)について<br>　Reverseの設定有無を追加|
|[v32.5.0](https://github.com/cwtickle/danoniplus/releases/tag/v32.5.0)|・オプション有効化設定 (g_presetObj.settingUse)について<br>　Excessiveの設定有無を追加|
|[v26.3.1](https://github.com/cwtickle/danoniplus/releases/tag/v26.3.1)|・変数名変更<br>　・g_presetSettingUse -> g_presetObj.settingUse|
|[v14.0.2](https://github.com/cwtickle/danoniplus/releases/tag/v14.0.2)|・オプション有効化設定 (g_presetSettingUse)のDisplayオプションについて、<br>　デフォルトOFF設定及び有効/無効化設定を実装|
|[v13.4.0](https://github.com/cwtickle/danoniplus/releases/tag/v13.4.0)|・オプション有効化設定 (g_presetSettingUse)について<br>　Displayオプションも指定できるように変更|
|[v9.0.0](https://github.com/cwtickle/danoniplus/releases/tag/v9.0.0)|・オプション有効化設定 (g_presetSettingUse)についてAppearanceの設定有無を追加|
|[v3.1.1](https://github.com/cwtickle/danoniplus/releases/tag/v3.1.1)|・オプション有効化設定 (g_presetSettingUse)についてGaugeの設定有無を追加|
|[v3.1.0](https://github.com/cwtickle/danoniplus/releases/tag/v3.1.0)|・オプション有効化設定 (g_presetSettingUse)を実装|

[ <- デフォルトデザイン・画像設定 ](dos-s0005-defaultDesign) | **オプション有効化** | [プレイ画面制御 -> ](dos-s0007-viewControl)