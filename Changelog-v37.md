**[English](https://github.com/cwtickle/danoniplus-docs/wiki/Changelog-v37) | Japanese**

[^ Back to All Version List (Security Policy)](https://github.com/cwtickle/danoniplus/security/policy)  
[^ Back to Update Info](./UpdateInfo#-v37-changelog)

[**<- v38**](./Changelog-v38) | **v37** | [**v36 ->**](./Changelog-v36)  
(**🔖 [24 Releases](https://github.com/cwtickle/danoniplus/releases?q=tag%3Av37)** )

## 🔃 Files changed (v37)

| Directory | FileName            |                                                                                            | Last Updated                                |
| --------- | ------------------- | ------------------------------------------------------------------------------------------ | ------------------------------------------- |
| /js       | danoni_main.js      | [📥](https://github.com/cwtickle/danoniplus/releases/download/v37.8.9/danoni_main.js)      | **v37.8.9**                                 |
| /js/lib   | danoni_constants.js | [📥](https://github.com/cwtickle/danoniplus/releases/download/v37.8.1/danoni_constants.js) | [v37.8.1](./Changelog-v37#v3781-2024-11-04) |
| /css      | danoni_main.css     | [📥](https://github.com/cwtickle/danoniplus/releases/download/v37.2.1/danoni_main.css)     | [v37.2.1](./Changelog-v37#v3721-2024-06-30) |

<details>
<summary>Changed file list before v36</summary>

| Directory | FileName                                                                                                                                               |                                                                                              | Last Updated                                |
| --------- | ------------------------------------------------------------------------------------------------------------------------------------------------------ | -------------------------------------------------------------------------------------------- | ------------------------------------------- |
| /js/lib   | danoni_localbinary.js                                                                                                                                  | [📥](https://github.com/cwtickle/danoniplus/releases/download/v15.1.0/danoni_localbinary.js) | [v15.1.0](./Changelog-v15#v1510-2020-05-21) |
| /skin     | danoni_skin_default.css<br>danoni_skin_light.css<br>danoni_skin_skyblue.css                                                                            | [📥](https://github.com/cwtickle/danoniplus/releases/download/v36.4.1/skin_css.zip)          | [v36.4.1](./Changelog-v37#v3641-2024-05-18) |
| /img      | aaShadow.svg<br>arrow.svg<br>arrowShadow.svg<br>borderline.svg<br>c.svg<br>cursor.svg<br>giko.svg<br>iyo.svg<br>monar.svg<br>morara.svg<br>onigiri.svg | [📥](https://github.com/cwtickle/danoniplus/releases/download/v15.1.0/img.zip)               | [v15.1.0](./Changelog-v15#v1510-2020-05-21) |

</details>

⭐ New Features / 🛠️ Improvements / 🐞 Bug Fixes  
( Files changed: ⚪ danoni_main.js / 🔵 danoni_constants.js / 🔴 css files )

## [Warning] 更新終了後の不具合情報
- [更新を終了したバージョンの不具合情報#v37](./DeprecatedVersionBugs#v37) を参照

## v37.8.9 ([2025-02-24](https://github.com/cwtickle/danoniplus/releases/tag/v37.8.9))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v37.8.9/total)](https://github.com/cwtickle/danoniplus/archive/v37.8.9.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v37.8.9/support/v37?style=social)](https://github.com/cwtickle/danoniplus/compare/v37.8.9...support/v37#files_bucket)

- 🐞 **C)** キーパターン違いで対象のキーアシストがいない場合、キーパターン変更時に止まることがある問題を修正 ( PR [#1786](https://github.com/cwtickle/danoniplus/pull/1786) ) <- :boom: [**v15.0.0**](./Changelog-v15#v1500-2020-05-13)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v37.8.9)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v37.8.8...v37.8.9#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v37.8.9)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v37.8.9.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v37.8.9.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v37.8.9/js/danoni_main.js)
> / 🎣 [**v39.8.2**](./Changelog-v39#v3982-2025-02-24)

## v37.8.8 ([2025-02-17](https://github.com/cwtickle/danoniplus/releases/tag/v37.8.8))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v37.8.8/total)](https://github.com/cwtickle/danoniplus/archive/v37.8.8.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v37.8.8/support/v37?style=social)](https://github.com/cwtickle/danoniplus/compare/v37.8.8...support/v37#files_bucket)

- 🐞 **C)** キーパターンが別キーモードで、デフォルトの曲中ショートカットが異なる場合に曲中ショートカットキーが変わらない問題を修正 ( PR [#1773](https://github.com/cwtickle/danoniplus/pull/1773) ) <- :boom: [**v4.8.0**](./Changelog-v4#v480-2019-05-10)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v37.8.8)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v37.8.7...v37.8.8#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v37.8.8)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v37.8.8.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v37.8.8.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v37.8.8/js/danoni_main.js)
> / 🎣 [**v39.6.1**](./Changelog-v39#v3961-2025-02-17)

## v37.8.7 ([2025-01-28](https://github.com/cwtickle/danoniplus/releases/tag/v37.8.7))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v37.8.7/total)](https://github.com/cwtickle/danoniplus/archive/v37.8.7.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v37.8.7/support/v37?style=social)](https://github.com/cwtickle/danoniplus/compare/v37.8.7...support/v37#files_bucket)

- 🐞 **C)** HitPosition有効時にHidden+/Sudden+のフィルター位置がずれる問題を修正 ( PR [#1746](https://github.com/cwtickle/danoniplus/pull/1746) ) <- :boom: [**v31.0.0**](./Changelog-v31#v3100-2023-03-20)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v37.8.7)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v37.8.6...v37.8.7#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v37.8.7)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v37.8.7.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v37.8.7.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v37.8.7/js/danoni_main.js)
> / 🎣 [**v39.0.0**](./Changelog-v39#v3900-2025-02-01)

## v37.8.6 ([2025-01-28](https://github.com/cwtickle/danoniplus/releases/tag/v37.8.6))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v37.8.6/total)](https://github.com/cwtickle/danoniplus/archive/v37.8.6.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v37.8.6/support/v37?style=social)](https://github.com/cwtickle/danoniplus/compare/v37.8.6...support/v37#files_bucket)

- 🐞 **B)** フリーズアロー移動中のフリーズアロー長の計算誤りを修正 ( PR [#1744](https://github.com/cwtickle/danoniplus/pull/1744) ) <- :boom: **initial**
- 🐞 **B)** フリーズアローの末尾付近に矢印がいる場合の判定不具合を修正 ( PR [#1744](https://github.com/cwtickle/danoniplus/pull/1744) ) <- :boom: **initial**

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v37.8.6)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v37.8.5...v37.8.6#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v37.8.6)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v37.8.6.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v37.8.6.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v37.8.6/js/danoni_main.js)
> / 🎣 [**v38.3.1**](./Changelog-v38#v3831-2025-01-28)

## v37.8.5 ([2024-12-19](https://github.com/cwtickle/danoniplus/releases/tag/v37.8.5))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v37.8.5/total)](https://github.com/cwtickle/danoniplus/archive/v37.8.5.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v37.8.5/support/v37?style=social)](https://github.com/cwtickle/danoniplus/compare/v37.8.5...support/v37#files_bucket)

- 🐞 **C)** 色変化(ncolor_data)でキーパターンを変えると適用する矢印グループが想定と異なる問題を修正 ( PR [#1738](https://github.com/cwtickle/danoniplus/pull/1738) ) <- :boom: [**v36.0.0**](./Changelog-v36#v3600-2024-04-15)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v37.8.5)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v37.8.4...v37.8.5#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v37.8.5)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v37.8.5.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v37.8.5.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v37.8.5/js/danoni_main.js)
> / 🎣 [**v38.2.2**](./Changelog-v38#v3822-2024-12-19)

## v37.8.4 ([2024-12-07](https://github.com/cwtickle/danoniplus/releases/tag/v37.8.4))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v37.8.4/total)](https://github.com/cwtickle/danoniplus/archive/v37.8.4.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v37.8.4/support/v37?style=social)](https://github.com/cwtickle/danoniplus/compare/v37.8.4...support/v37#files_bucket)

- 🐞 **B)** 加算するモーションフレーム数の計算方法の誤りを修正 ( PR [#1734](https://github.com/cwtickle/danoniplus/pull/1734) ) <- :boom: [**v28.4.0**](./Changelog-v28#v2840-2022-10-20)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v37.8.4)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v37.8.3...v37.8.4#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v37.8.4)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v37.8.4.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v37.8.4.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v37.8.4/js/danoni_main.js)
> / 🎣 [**v38.1.2**](./Changelog-v38#v3812-2024-12-07)

## v37.8.3 ([2024-11-27](https://github.com/cwtickle/danoniplus/releases/tag/v37.8.3))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v37.8.3/total)](https://github.com/cwtickle/danoniplus/archive/v37.8.3.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v37.8.3/support/v37?style=social)](https://github.com/cwtickle/danoniplus/compare/v37.8.3...support/v37#files_bucket)

- 🛠️ リモート接続用サンプルHTMLをnetlifyに変更 ( PR [#1728](https://github.com/cwtickle/danoniplus/pull/1728) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v37.8.3)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v37.8.2...v37.8.3#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v37.8.3)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v37.8.3.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v37.8.3.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v37.8.3/js/danoni_main.js)

## v37.8.2 ([2024-11-07](https://github.com/cwtickle/danoniplus/releases/tag/v37.8.2))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v37.8.2/total)](https://github.com/cwtickle/danoniplus/archive/v37.8.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v37.8.2/support/v37?style=social)](https://github.com/cwtickle/danoniplus/compare/v37.8.2...support/v37#files_bucket)

- 🐞 **C)** 旧関数のボタン作成処理の引数間違いを修正 ( PR [#1724](https://github.com/cwtickle/danoniplus/pull/1724) ) <- :boom: [**v37.8.0**](./Changelog-v37#v3780-2024-11-01)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v37.8.2)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v37.8.1...v37.8.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v37.8.2)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v37.8.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v37.8.2.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v37.8.2/js/danoni_main.js)
> / 🎣 [**v38.0.2**](./Changelog-v38#v3802-2024-11-07)

## v37.8.1 ([2024-11-04](https://github.com/cwtickle/danoniplus/releases/tag/v37.8.1))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v37.8.1/total)](https://github.com/cwtickle/danoniplus/archive/v37.8.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v37.8.1/support/v37?style=social)](https://github.com/cwtickle/danoniplus/compare/v37.8.1...support/v37#files_bucket)

- 🛠️ ハッシュタグの前後に半角スペースを追加 ( PR [#1718](https://github.com/cwtickle/danoniplus/pull/1718) )
- 🐞 **C)** フェードインで矢印が足りない場合でもクリアランプの点灯条件を通過してしまう問題を修正 ( PR [#1719](https://github.com/cwtickle/danoniplus/pull/1719) ) <- :boom: [**v36.2.0**](./Changelog-v36#v3620-2024-05-01)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v37.8.1)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v37.8.0...v37.8.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v37.8.1)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v37.8.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v37.8.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v37.8.1/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v37.8.1/js/lib/danoni_constants.js)
> / 🎣 [**v38.0.0**](./Changelog-v38#v3800-2024-11-04)

---

## v37.8.0 ([2024-11-01](https://github.com/cwtickle/danoniplus/releases/tag/v37.8.0))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v37.8.0/total)](https://github.com/cwtickle/danoniplus/archive/v37.8.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v37.8.0/support/v37?style=social)](https://github.com/cwtickle/danoniplus/compare/v37.8.0...support/v37#files_bucket)

- 🛠️ 古い関数・定数を条件付きで復活 ( PR [#1716](https://github.com/cwtickle/danoniplus/pull/1716) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v37.8.0)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v37.7.1...v37.8.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v37.8.0)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v37.8.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v37.8.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v37.8.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v37.8.0/js/lib/danoni_constants.js)

## v37.7.1 ([2024-10-28](https://github.com/cwtickle/danoniplus/releases/tag/v37.7.1))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v37.7.1/total)](https://github.com/cwtickle/danoniplus/archive/v37.7.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v37.7.1/support/v37?style=social)](https://github.com/cwtickle/danoniplus/compare/v37.7.1...support/v37#files_bucket)

- 🐞 **C)** 色変化（ncolor_data）で矢印種類を省略したときに色変化対象が正しく反映されない問題を修正 ( PR [#1714](https://github.com/cwtickle/danoniplus/pull/1714) ) <- :boom: [**v37.4.0**](./Changelog-v37#v3740-2024-08-14)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v37.7.1)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v37.7.0...v37.7.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v37.7.1)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v37.7.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v37.7.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v37.7.1/js/danoni_main.js)<br>❤️ ショウタ

## v37.7.0 ([2024-10-06](https://github.com/cwtickle/danoniplus/releases/tag/v37.7.0))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v37.7.0/total)](https://github.com/cwtickle/danoniplus/archive/v37.7.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v37.7.0/support/v37?style=social)](https://github.com/cwtickle/danoniplus/compare/v37.7.0...support/v37#files_bucket)

- 🛠️ コードの整理 ( PR [#1712](https://github.com/cwtickle/danoniplus/pull/1712) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v37.7.0)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v37.6.2...v37.7.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v37.7.0)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v37.7.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v37.7.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v37.7.0/js/danoni_main.js)

## v37.6.2 ([2024-09-25](https://github.com/cwtickle/danoniplus/releases/tag/v37.6.2))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v37.6.2/total)](https://github.com/cwtickle/danoniplus/archive/v37.6.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v37.6.2/support/v37?style=social)](https://github.com/cwtickle/danoniplus/compare/v37.6.2...support/v37#files_bucket)

- 🐞 **C)** ImgType で拡張子の指定がないときに、強制的に他の拡張子に変更する機能の問題を修正 ( PR [#1708](https://github.com/cwtickle/danoniplus/pull/1708) ) <- :boom: [**v23.1.0**](./Changelog-v23#v2310-2021-09-11)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v37.6.2)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v37.6.1...v37.6.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v37.6.2)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v37.6.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v37.6.2.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v37.6.2/js/danoni_main.js)

## v37.6.1 ([2024-09-25](https://github.com/cwtickle/danoniplus/releases/tag/v37.6.1))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v37.6.1/total)](https://github.com/cwtickle/danoniplus/archive/v37.6.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v37.6.1/support/v37?style=social)](https://github.com/cwtickle/danoniplus/compare/v37.6.1...support/v37#files_bucket)

- 🛠️ 古い関数を新しい関数に代替する部分のコード整理 ( PR [#1706](https://github.com/cwtickle/danoniplus/pull/1706) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v37.6.1)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v37.6.0...v37.6.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v37.6.1)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v37.6.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v37.6.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v37.6.1/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v37.6.1/js/lib/danoni_constants.js)

## v37.6.0 ([2024-09-11](https://github.com/cwtickle/danoniplus/releases/tag/v37.6.0))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v37.6.0/total)](https://github.com/cwtickle/danoniplus/archive/v37.6.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v37.6.0/support/v37?style=social)](https://github.com/cwtickle/danoniplus/compare/v37.6.0...support/v37#files_bucket)

- ⭐ カスタムキーの単位名を変更できる設定を追加 ( PR [#1702](https://github.com/cwtickle/danoniplus/pull/1702) )
- ⭐ keyNameX のエスケープ文字対応 ( PR [#1702](https://github.com/cwtickle/danoniplus/pull/1702) )
- 🛠️ 譜面選択リストのキーフィルタ名のキー名やキー単位名が keyNameX を考慮した名前になるように変更 ( PR [#1702](https://github.com/cwtickle/danoniplus/pull/1702) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v37.6.0)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v37.5.0...v37.6.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v37.6.0)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v37.6.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v37.6.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v37.6.0/js/danoni_main.js)

## v37.5.0 ([2024-09-04](https://github.com/cwtickle/danoniplus/releases/tag/v37.5.0))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v37.5.0/total)](https://github.com/cwtickle/danoniplus/archive/v37.5.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v37.5.0/support/v37?style=social)](https://github.com/cwtickle/danoniplus/compare/v37.5.0...support/v37#files_bucket)

- ⭐ リバース設定を制限する機能を実装 ( PR [#1700](https://github.com/cwtickle/danoniplus/pull/1700) )
- ⭐ 13key にカラーグループを追加 ( PR [#1699](https://github.com/cwtickle/danoniplus/pull/1699) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v37.5.0)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v37.4.0...v37.5.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v37.5.0)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v37.5.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v37.5.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v37.5.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v37.5.0/js/lib/danoni_constants.js)

## v37.4.0 ([2024-08-14](https://github.com/cwtickle/danoniplus/releases/tag/v37.4.0))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v37.4.0/total)](https://github.com/cwtickle/danoniplus/archive/v37.4.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v37.4.0/support/v37?style=social)](https://github.com/cwtickle/danoniplus/compare/v37.4.0...support/v37#files_bucket)

- ⭐ 9Akey にカラーグループを追加 ( PR [#1695](https://github.com/cwtickle/danoniplus/pull/1695) )
- 🛠️ 曲名や制作者名で実体参照記述をしているときに、CopyResult で元の文字になるよう変更 ( PR [#1696](https://github.com/cwtickle/danoniplus/pull/1696) )
- 🐞 **C)** 一部文字列の置換処理がおかしくなる問題を修正 ( PR [#1697](https://github.com/cwtickle/danoniplus/pull/1697) ) <- :boom: [**v27.0.0**](./Changelog-v27#v2700-2022-03-18)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v37.4.0)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v37.3.1...v37.4.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v37.4.0)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v37.4.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v37.4.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v37.4.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v37.4.0/js/lib/danoni_constants.js)

## v37.3.1 ([2024-07-07](https://github.com/cwtickle/danoniplus/releases/tag/v37.3.1))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v37.3.1/total)](https://github.com/cwtickle/danoniplus/archive/v37.3.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v37.3.1/support/v37?style=social)](https://github.com/cwtickle/danoniplus/compare/v37.3.1...support/v37#files_bucket)

- 🛠️ フリーズアローの終端がいない場合に末尾のフリーズアロー始端を自動除去する仕様に変更 ( PR [#1693](https://github.com/cwtickle/danoniplus/pull/1693) )
- 🛠️ 難易度表示（DifLevel）に表示しているフリーズアロー数について無効化分を含めないよう対応 ( PR [#1693](https://github.com/cwtickle/danoniplus/pull/1693) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v37.3.1)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v37.3.0...v37.3.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v37.3.1)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v37.3.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v37.3.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v37.3.1/js/danoni_main.js)

## v37.3.0 ([2024-07-06](https://github.com/cwtickle/danoniplus/releases/tag/v37.3.0))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v37.3.0/total)](https://github.com/cwtickle/danoniplus/archive/v37.3.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v37.3.0/support/v37?style=social)](https://github.com/cwtickle/danoniplus/compare/v37.3.0...support/v37#files_bucket)

- ⭐ 生成タイミングが同じで Y 座標違いの場合でも矢印・フリーズアローを区別して生成できるよう対応 ( PR [#1691](https://github.com/cwtickle/danoniplus/pull/1691) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v37.3.0)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v37.2.1...v37.3.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v37.3.0)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v37.3.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v37.3.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v37.3.0/js/danoni_main.js)<br>❤️ goe ( @goe0 )

## v37.2.1 ([2024-06-30](https://github.com/cwtickle/danoniplus/releases/tag/v37.2.1))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v37.2.1/total)](https://github.com/cwtickle/danoniplus/archive/v37.2.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v37.2.1/support/v37?style=social)](https://github.com/cwtickle/danoniplus/compare/v37.2.1...support/v37#files_bucket)

- 🛠️ mask, background-clip についてベンダープレフィックスの互換設定を元に戻す ( PR [#1689](https://github.com/cwtickle/danoniplus/pull/1689) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v37.2.1)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v37.2.0...v37.2.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v37.2.1)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v37.2.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v37.2.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v37.2.1/js/danoni_main.js)[🔴](https://github.com/cwtickle/danoniplus/blob/v37.2.1/css/danoni_main.css)

## v37.2.0 ([2024-06-29](https://github.com/cwtickle/danoniplus/releases/tag/v37.2.0))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v37.2.0/total)](https://github.com/cwtickle/danoniplus/archive/v37.2.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v37.2.0/support/v37?style=social)](https://github.com/cwtickle/danoniplus/compare/v37.2.0...support/v37#files_bucket)

- ⭐ playbackRate の値に合わせて Adjustment の設定幅を伸縮するよう変更 ( PR [#1687](https://github.com/cwtickle/danoniplus/pull/1687) )
- 🛠️ playbackRate の値を 1 以外にした場合、プレイ・結果画面にその内容を表示するよう変更 ( PR [#1687](https://github.com/cwtickle/danoniplus/pull/1687) )
- 🛠️ playbackRate の値を 1 以外にした場合、ハイスコアを保存しないよう変更 ( PR [#1687](https://github.com/cwtickle/danoniplus/pull/1687) )
- 🛠️ 設定ボタン関数の引数拡張 ( PR [#1687](https://github.com/cwtickle/danoniplus/pull/1687) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v37.2.0)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v37.1.0...v37.2.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v37.2.0)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v37.2.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v37.2.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v37.2.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v37.2.0/js/lib/danoni_constants.js)

<img src="https://github.com/cwtickle/danoniplus/assets/44026291/ff2b480f-c68e-4861-b8f5-1c3a52b7985f" width="50%">

## v37.1.0 ([2024-06-25](https://github.com/cwtickle/danoniplus/releases/tag/v37.1.0))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v37.1.0/total)](https://github.com/cwtickle/danoniplus/archive/v37.1.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v37.1.0/support/v37?style=social)](https://github.com/cwtickle/danoniplus/compare/v37.1.0...support/v37#files_bucket)

- ⭐ Appearance 設定の Lock ボタンにショートカットキーを割り当て ( PR [#1684](https://github.com/cwtickle/danoniplus/pull/1684) )
- ⭐ 速度変化グラフにおいて最低/最高変化量を表記するように変更 ( PR [#1684](https://github.com/cwtickle/danoniplus/pull/1685) )
- 🛠️ 譜面選択ウィンドウの位置調整、速度変化グラフの表示幅を調整 ( PR [#1685](https://github.com/cwtickle/danoniplus/pull/1685) )
- 🐞 **C)** 速度変化平均（AvgO, AvgB）の表示が想定より高く出ることがある問題を修正 ( PR [#1685](https://github.com/cwtickle/danoniplus/pull/1685) ) <- :boom: [**v32.2.0**](./Changelog-v32#v3220-2023-05-21)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v37.1.0)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v37.0.1...v37.1.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v37.1.0)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v37.1.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v37.1.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v37.1.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v37.1.0/js/lib/danoni_constants.js)<br>❤️ apoi, MFV2 ( @MFV2 )

<img src="https://github.com/cwtickle/danoniplus/assets/44026291/4201ea71-ae66-48e1-ac94-dd1a5dd1a4bf" width="50%">

## v37.0.1 ([2024-06-20](https://github.com/cwtickle/danoniplus/releases/tag/v37.0.1))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v37.0.1/total)](https://github.com/cwtickle/danoniplus/archive/v37.0.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v37.0.1/support/v37?style=social)](https://github.com/cwtickle/danoniplus/compare/v37.0.1...support/v37#files_bucket)

- 🐞 **C)** Reverse のときに歌詞表示の自動反転が効かない問題を修正 ( PR [#1679](https://github.com/cwtickle/danoniplus/pull/1679), [#1681](https://github.com/cwtickle/danoniplus/pull/1681) ) <- :boom: [**v30.0.1**](./Changelog-v30#v3001-2023-02-11)
- 🐞 **B)** ダミーフリーズアロー（部分オートノーツ）生成時に画面が止まってしまう問題を修正 ( PR [#1680](https://github.com/cwtickle/danoniplus/pull/1680) ) <- :boom: [**v36.0.0**](./Changelog-v36#v3600-2024-04-15)
- 🐞 **C)** ダミーフリーズアローヒット時に、開始矢印の塗りつぶし色が消えてしまう問題を修正 ( PR [#1680](https://github.com/cwtickle/danoniplus/pull/1680) ) <- :boom: [**v31.0.0**](./Changelog-v31#v3100-2023-03-20)
- 🐞 **C)** スキンがデフォルトのとき、ダミーフリーズアローの矢印の塗りつぶし色が矢印の枠色と同じになってしまう問題を修正 ( PR [#1680](https://github.com/cwtickle/danoniplus/pull/1680) ) <- :boom: [**v36.0.0**](./Changelog-v36#v3600-2024-04-15)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v37.0.1)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v37.0.0...v37.0.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v37.0.1)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v37.0.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v37.0.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v37.0.1/js/danoni_main.js)<br>❤️ すずめ ( @suzme )

## v37.0.0 ([2024-06-15](https://github.com/cwtickle/danoniplus/releases/tag/v37.0.0))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v37.0.0/total)](https://github.com/cwtickle/danoniplus/archive/v37.0.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v37.0.0/support/v37?style=social)](https://github.com/cwtickle/danoniplus/compare/v37.0.0...support/v37#files_bucket)

- ⭐ キー数変化 (keych_data) で同時に複数のキーを共存させる機能を実装 ( PR [#1676](https://github.com/cwtickle/danoniplus/pull/1676) )
- 🛠️ キー数変化の部分キー指定箇所においてタブ、半角空白をカットする機能を追加 ( PR [#1676](https://github.com/cwtickle/danoniplus/pull/1676) )
- 🛠️ 未使用定数 及び danoni_legacy_function.js の読込を停止 ( PR [#1677](https://github.com/cwtickle/danoniplus/pull/1677) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v37.0.0)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v36.6.0...v37.0.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v37.0.0)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v37.0.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v37.0.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v37.0.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v37.0.0/js/lib/danoni_constants.js)

<img src="https://github.com/cwtickle/danoniplus/assets/44026291/91fc2d75-9317-411d-92ac-361873d2a58d" width="60%">

[**<- v38**](./Changelog-v38) | **v37** | [**v36 ->**](./Changelog-v36)

| [< 更新を終了したバージョンの不具合情報](./DeprecatedVersionBugs) | **Changelog** || [譜面ヘッダー仕様 >](./dos_header) |
