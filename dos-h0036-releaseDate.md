**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-h0036-releaseDate) | Japanese** 

[Home](./) &gt; [譜面ヘッダー仕様](dos_header) &gt; [クレジット関連、共通設定](dos_header#-クレジット関連共通設定)

| [<- hashTag](dos-h0018-hashTag) | **releaseDate** || [musicTitle ->](dos-h0001-musicTitle) |

## releaseDate
- 作品公開日の設定

### 使い方
```
|releaseDate=2019-01-24|
```
### 説明
指定すると、バージョン名の右に表示。ページ更新日などの用途に利用できます。    

### 関連項目
- [**tuning**](dos-h0017-tuning) [:pencil:](dos-h0017-tuning/_edit) 製作者クレジット

### 更新履歴

|Version|変更内容|
|----|----|
|[v2.8.0](https://github.com/cwtickle/danoniplus/releases/tag/v2.8.0)|・初回実装|

| [<- hashTag](dos-h0018-hashTag) | **releaseDate** || [musicTitle ->](dos-h0001-musicTitle) |