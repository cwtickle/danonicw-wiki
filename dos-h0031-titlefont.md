**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-h0031-titlefont) | Japanese** 

[Home](./) &gt; [譜面ヘッダー仕様](dos_header) &gt; [タイトル曲名文字(デフォルトデザイン)のエフェクト](dos_header#-タイトル曲名文字デフォルトデザインのエフェクト)

| [<- titleSize](dos-h0030-titlesize) | **titleFont** | [titlegrd / titleArrowgrd ->](dos-h0032-titlegrd) |

## titleFont (titlefont)
- タイトル文字のフォント設定

### 使い方
```
|titleFont=Century,Meiryo UI|
|titleFont=Century,Meiryo UI$Century Gothic,Meiryo UI|
```
### 説明
デフォルトの曲名表示を使用した場合に、曲名のフォントを指定します。  
カンマ区切りで複数のフォントを指定できます。前に設定したものが優先です。  
指定しない場合は、"メイリオ", sans-serifが自動指定されます。  
なおWebフォントを指定した場合、読込のラグで初回表示のみ、  
Webフォント以外のフォントが表示されます。  
（→ Ver3.2.0以降より、この事象は解消されました。）  

ver17.3.0より、$区切りで2行目限定のフォントが指定できるようになりました。

#### 言語別設定 (ver29.3.0以降)

|通常設定|言語別設定(Ja)|言語別設定(En)|
|----|----|----|
|titleFont|titleFont**Ja**|titleFont**En**|
|titlefont|titlefont**Ja**|titlefont**En**|

### タイトル曲名文字(デフォルトデザイン)の設定箇所について
![dos-h0030-01.png](./wiki/dos-h0030-01.png)

### 関連項目
- [**customFont**](dos-h0020-customFont) [:pencil:](dos-h0020-customFont/_edit) 画面全般のフォント
- [**titlesize**](dos-h0030-titlesize) [:pencil:](dos-h0030-titlesize/_edit) 文字サイズ
- [titlegrd](dos-h0032-titlegrd) [:pencil:](dos-h0032-titlegrd/_edit) グラデーション
- [titlePos](dos-h0033-titlepos) [:pencil:](dos-h0033-titlepos/_edit) X, Y座標位置
- [**titleLineHeight**](dos-h0034-titlelineheight) [:pencil:](dos-h0034-titlelineheight/_edit) 複数行の際の行間

### 更新履歴

|Version|変更内容|
|----|----|
|[v32.7.0](https://github.com/cwtickle/danoniplus/releases/tag/v32.7.0)|・キャメル表記での指定に対応|
|[v29.3.0](https://github.com/cwtickle/danoniplus/releases/tag/v29.3.0)|・言語別の設定に対応|
|[v17.3.0](https://github.com/cwtickle/danoniplus/releases/tag/v17.3.0)|・2行目のフォント設定を追加|
|[v2.1.0](https://github.com/cwtickle/danoniplus/releases/tag/v2.1.0)|・初回実装|

| [<- titleSize](dos-h0030-titlesize) | **titleFont** | [titlegrd / titleArrowgrd ->](dos-h0032-titlegrd) |