**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-s0002-customFile) | Japanese** 

[↑ 共通設定ファイル仕様に戻る](dos_setting)

[ <- 制作者クレジット・基本設定 ](dos-s0001-makerInfo) | **カスタムファイル設定** | [ゲージ設定 -> ](dos-s0003-initialGauge)

## カスタムファイル設定
### デフォルトスキン (g_presetObj.skinType)
⇒ 指定があった場合に優先される譜面ヘッダー：[skinType](dos-h0054-skinType)
- デフォルトスキンを**1つ**設定できます。譜面ヘッダーが指定された場合は上書きされます。  
カレントディレクトリ指定などの方法は譜面ヘッダーと同じです。
```javascript
g_presetObj.skinType = `light`;
```

### デフォルトスキン時のCanvas有効化設定 (g_presetObj.bgCanvasUse)
⇒ 指定があった場合に優先される譜面ヘッダー：[bgCanvasUse](dos-h0094-bgCanvasUse)
- デフォルトスキン時、ダンおにのメインdiv(canvas-frame)に対してCanvasタグがあるとき、そのCanvasデータを背景として使用するかどうかを設定します。詳細は譜面ヘッダーの同名のページをご覧ください。
```javascript
g_presetObj.bgCanvasUse = false;
```

### デフォルトカスタムJs (g_presetObj.customJs)
⇒ 指定があった場合に優先される譜面ヘッダー：[customJs](dos-h0019-customjs)
- デフォルトのカスタムJsをカンマ区切りで複数設定できます。譜面ヘッダーが指定された場合は上書きされます。  
カレントディレクトリ指定などの方法は譜面ヘッダーと同じです。
```javascript
g_presetObj.customJs = `mainCustom.js,groupCustom.js`;
```

### デフォルトカスタムCss (g_presetObj.customCss)
⇒ 指定があった場合に優先される譜面ヘッダー：[customCss](dos-h0086-customcss)
- デフォルトのカスタムCssをカンマ区切りで複数設定できます。譜面ヘッダーが指定された場合は上書きされます。  
カレントディレクトリ指定などの方法は譜面ヘッダーと同じです。
```javascript
g_presetObj.customCss = `danoni_custom.css`;
```

### 背景・マスクモーションで使う画像パスの基準フォルダ設定 (g_presetObj.syncBackPath)
⇒ 指定があった場合に優先される譜面ヘッダー：[syncBackPath](dos-h0087-syncBackPath)
- 背景／マスクモーションで使用する画像パスの指定方法を  
customJsやskinTypeのようなカレントディレクトリ指定に合わせる設定です。`true`で有効化します。

```javascript
g_presetObj.syncBackPath = true;
```

## 更新履歴

|Version|変更内容|
|----|----|
|[v33.3.0](https://github.com/cwtickle/danoniplus/releases/tag/v33.3.0)|・デフォルトスキン時のCanvas有効化設定(g_presetObj.bgCanvasUse)を追加|
|[v26.3.1](https://github.com/cwtickle/danoniplus/releases/tag/v26.3.1)|・変数名変更<br>　・g_presetSkinType -> g_presetObj.skinType<br>　・g_presetCustomJs -> g_presetObj.customJs<br>　・g_presetCustomCss -> g_presetObj.customCss|
|[v25.4.0](https://github.com/cwtickle/danoniplus/releases/tag/v25.4.0)|・背景・マスクモーションで使う画像パスの基準フォルダ設定(g_presetSyncBackPath)を追加|
|[v25.3.0](https://github.com/cwtickle/danoniplus/releases/tag/v25.3.0)|・カスタムCss (g_presetCustomCss)を実装|
|[v19.3.0](https://github.com/cwtickle/danoniplus/releases/tag/v19.3.0)|・デフォルトスキン／カスタムJs (g_presetSkinType, g_presetCustomJs)を実装|

[ <- 制作者クレジット・基本設定 ](dos-s0001-makerInfo) | **カスタムファイル設定** | [ゲージ設定 -> ](dos-s0003-initialGauge)