**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-h0086-customcss) | Japanese** 

[Home](./) &gt; [譜面ヘッダー仕様](dos_header) &gt; [カスタムデータの取込](dos_header#-カスタムデータの取込)

| [<- customJs](dos-h0019-customjs) | **customCss** | [preloadImages ->](dos-h0021-preloadImages) |

## customCss (customcss)
- カスタムcssファイルの指定
- 共通設定 ⇒ [g_presetObj.customCss](dos-s0002-customFile#デフォルトカスタムcss-g_presetobjcustomcss)

### 使い方
```
|customcss=danoni_custom.css,(..)special.css| // 2つ目は作品ページと同じフォルダを参照
|customcss=*,special.css|  // 1つ目は g_presetObj.customCss で指定したファイルを適用
```
### 説明
既存のcssファイルとは別に読み込むcssファイルを指定します。  
指定の仕方は「customJs」と同じです。  
デフォルトは「css」フォルダの中のcssファイルを参照します。  
他の譜面ヘッダーと同様、相対パスや同一パス指定(..)に対応しています。

ver27.1.0以降、「*」を指定することで [g_presetObj.customCss](dos-s0002-customFile) で指定した内容を適用できるようにしました。  
下記が指定されているとき、
```javascript
g_presetObj.customCss = `init.css`;
```
譜面ヘッダーで下記を指定した場合、
```
|customcss=*,special.css|
```
次の定義と同じ意味になります。
```
|customcss=init.css,special.css|
```

#### 言語別設定 (ver32.7.0以降)

|通常設定|言語別設定(Ja)|言語別設定(En)|
|----|----|----|
|customCss|customCss**Ja**|customCss**En**|
|customcss|customcss**Ja**|customcss**En**|

### 関連項目
- [**customJs**](dos-h0019-customjs) [:pencil:](dos-h0019-customjs/_edit) カスタムjsファイルの指定
- [共通設定ファイル](dos_setting) &gt; [カスタムファイル設定](dos-s0002-customFile)

### 更新履歴

|Version|変更内容|
|----|----|
|[v32.7.0](https://github.com/cwtickle/danoniplus/releases/tag/v32.7.0)|・キャメル表記での指定に対応<br>・言語別の設定に対応|
|[v27.1.0](https://github.com/cwtickle/danoniplus/releases/tag/v27.1.0)|・共通設定ファイルで指定した内容を略記指定できる機能を実装|
|[v25.3.0](https://github.com/cwtickle/danoniplus/releases/tag/v25.3.0)|・初回実装|

| [<- customJs](dos-h0019-customjs) | **customCss** | [preloadImages ->](dos-h0021-preloadImages) |