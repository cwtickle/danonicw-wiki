
[^ Tips Indexに戻る](./tips-index)

| < [画面の位置調整方法](./tips-0003-position-adjustment) | **ステップゾーンや矢印全体を一時的に隠す** | [スコアドラムロールの作成](./tips-0023-score-drum-roll) > |


# ステップゾーンや矢印全体を一時的に隠す
- 演出上、瞬間的にステップゾーンや矢印全体を隠したい場合があります。
- ちょっとした設定やcustomJsで実現することができます。

## 使い方
### 1. 矢印・フリーズアロー
- 矢印・フリーズアローは`arrowSprite0`, `arrowSprite1`の配下にあるオブジェクトです。  
このため、特定のフレーム数で表示・非表示を切り替えたい場合は、これらのオブジェクトを表示・非表示すれば実現できます。
- 色変化を使う方法もありますが、ColorTypeの変更に追随できないのでこちらの方がお手軽です。
```javascript
const hideArrows = _ => {
    $id(`arrowSprite0`).display = `none`;
    $id(`arrowSprite1`).display = `none`;
};
const appearArrows = _ => {
    $id(`arrowSprite0`).display = `inherit`;
    $id(`arrowSprite1`).display = `inherit`;
};

/**
 * メイン画面(フレーム毎表示) [Scene: Main / Banana]
 */
function customMainEnterFrame2() {

    // 1000フレームで矢印・フリーズアローを隠し、1050フレームで再表示する例
    if (g_scoreObj.baseFrame === 1000) {
        hideArrows();
    } else if (g_scoreObj.baseFrame === 1050) {
        appearArrows();
    }
}
```

### 2. ステップゾーン
#### 2-1. キー数変化機能を利用
- v30で実装したキー数変化の機能を使います。  
元々、キーグループが指定されていない場合は一律「0」という名前のキーグループに割り当てられる仕様になっています。
- 一時的に「0」以外（例えば`X`）といったキーグループに変えることでステップゾーンを隠すことができます。
```
|keych_data=1000,X,1050,0|  // 1000フレームでステップゾーンを隠し、1050フレームで再表示する例
```

#### 2-2. customJsを利用
- 2-1.の方法の場合、フリーズアローヒット時や空押ししたときのモーションも消えてしまいます。
- これを避けたい場合は、customJsで指定する必要があります。
```javascript
const hideStepZone = _j => $id(`step${_j}`).display = `none`;
const appearStepZone = _j => {
    // Display:StepZoneが`OFF`のときは再表示されないようにする
    if (g_stateObj.d_stepzone === C_FLG_ON) {
        $id(`step${_j}`).display = `inherit`;
    }
};

/**
 * メイン画面(フレーム毎表示) [Scene: Main / Banana]
 */
function customMainEnterFrame2() {
    const keyCtrlPtn = `${g_keyObj.currentKey}_${g_keyObj.currentPtn}`;
    const keyNum = g_keyObj[`chara${keyCtrlPtn}`].length;

    // 1000フレームでステップゾーンを隠し、1050フレームで再表示する例
    if (g_scoreObj.baseFrame === 1000) {
        for (let j = 0; j < keyNum; j++) {
            hideStepZone(j);
        }
    } else if (g_scoreObj.baseFrame === 1050) {
        for (let j = 0; j < keyNum; j++) {
            appearStepZone(j);
        }
    }
}
```

## ページ作成者
- ティックル

[^ Tips Indexに戻る](./tips-index)

| < [画面の位置調整方法](./tips-0003-position-adjustment) | **ステップゾーンや矢印全体を一時的に隠す** | [スコアドラムロールの作成](./tips-0023-score-drum-roll) > |
