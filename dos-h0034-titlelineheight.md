**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-h0034-titlelineheight) | Japanese** 

[Home](./) &gt; [譜面ヘッダー仕様](dos_header) &gt; [タイトル曲名文字(デフォルトデザイン)のエフェクト](dos_header#-タイトル曲名文字デフォルトデザインのエフェクト)

| [<- titlePos](dos-h0033-titlepos) | **titleLineHeight** | [titleAnimation ->](dos-h0077-titleanimation) |

## titleLineHeight (titlelineheight)
- タイトル文字の複数行の際の行間サイズ

### 使い方
```
|titleLineHeight=50|
```
### 説明
デフォルトの曲名表示を使用した場合で曲名が2行になる場合、行間を指定します。  
デフォルトはタイトル1行目のフォントサイズ+10pxです。  
customTitleUseがfalseに設定されているとき、この値は無視されます。

#### 言語別設定 (ver29.3.0以降)

|通常設定|言語別設定(Ja)|言語別設定(En)|
|----|----|----|
|titleLineHeight|titleLineHeight**Ja**|titleLineHeight**En**|
|titlelineheight|titlelineheight**Ja**|titlelineheight**En**|

### タイトル曲名文字(デフォルトデザイン)の設定箇所について
![dos-h0030-01.png](./wiki/dos-h0030-01.png)

### 関連項目
- [**titleSize**](dos-h0030-titlesize) [:pencil:](dos-h0030-titlesize/_edit) 文字サイズ
- [**titleFont**](dos-h0031-titlefont) [:pencil:](dos-h0031-titlefont/_edit) フォント
- [titlePos](dos-h0033-titlepos) [:pencil:](dos-h0033-titlepos/_edit) X, Y座標位置

### 更新履歴

|Version|変更内容|
|----|----|
|[v32.7.0](https://github.com/cwtickle/danoniplus/releases/tag/v32.7.0)|・キャメル表記での指定に対応|
|[v29.3.0](https://github.com/cwtickle/danoniplus/releases/tag/v29.3.0)|・言語別の設定に対応|
|[v3.2.0](https://github.com/cwtickle/danoniplus/releases/tag/v3.2.0)|・初回実装|

| [<- titlePos](dos-h0033-titlepos) | **titleLineHeight** | [titleAnimation ->](dos-h0077-titleanimation) |