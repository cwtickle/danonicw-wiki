**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-h0089-autoSpread) | Japanese** 

[Home](./) &gt; [譜面ヘッダー仕様](dos_header) &gt; [クレジット関連、共通設定](dos_header#-クレジット関連共通設定)

| [<- titleArrowName](dos-h0099-titleArrowName) || **autoSpread** | [windowWidth ->](dos-h0090-windowWidth) |

## autoSpread
- 自動横幅拡張設定
- 共通設定 ⇒ [g_presetObj.autoSpread](dos-s0001-makerInfo#自動横幅拡張設定-g_presetobjautospread)

### 使い方
```
|autoSpread=false|
```
### 説明
キー別の最小横幅を考慮した横幅になるよう調整するかどうかを設定します。  
デフォルトは`true`(有効)です。  

|値|既定|内容|
|----|----|----|
|false||キー別の最小横幅を考慮せず、作品別の指定幅にする|
|true|*|キー別の最小横幅を考慮した横幅になるよう自動調整|

### 関連項目
- [**windowWidth**](dos-h0090-windowWidth) [:pencil:](dos-h0090-windowWidth/_edit) 画面ウィンドウの横幅
- [**windowAlign**](dos-h0091-windowAlign) [:pencil:](dos-h0091-windowAlign/_edit) 画面ウィンドウの位置
- [共通設定ファイル仕様](dos_setting) &gt; [制作者クレジット・共通設定](dos-s0001-makerInfo)

### 更新履歴

|Version|変更内容|
|----|----|
|[v27.1.0](https://github.com/cwtickle/danoniplus/releases/tag/v27.1.0)|・初回実装|

| [<- titleArrowName](dos-h0099-titleArrowName) || **autoSpread** | [windowWidth ->](dos-h0090-windowWidth) |