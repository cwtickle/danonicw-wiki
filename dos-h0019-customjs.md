**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-h0019-customjs) | Japanese** 

[Home](./) &gt; [譜面ヘッダー仕様](dos_header) &gt; [カスタムデータの取込](dos_header#-カスタムデータの取込)

| [<- resultValsView](dos-h0095-resultValsView) || **customJs** | [customCss ->](dos-h0086-customcss) |

## customJs (customjs)
- カスタムjsファイルの指定
- 共通設定 ⇒ [g_presetObj.customJs](dos-s0002-customFile#デフォルトカスタムjs-g_presetobjcustomjs)

### 使い方
```
|customJs=danoni_custom.js,danoni_custom-0190.js|
|customJs=danoni_custom.js,(..)special.js|  // 2つ目は作品ページと同じフォルダを参照
|customJs=*,special.js|  // 1つ目は g_presetObj.customJs で指定したファイルを適用
```
### 説明
作品別の割り込み処理が行えるcustomのjsファイルを指定します。  
デフォルトは「danoni_custom.js」を読み込みます。  
最大2ファイルをカンマ区切りで指定できます。  
ver25.3.0以降では2ファイルの制限が無くなり、3つ以上指定することもできるようになりました。

ver27.1.0以降、「*」を指定することで [g_presetObj.customJs](dos-s0002-customFile) で指定した内容を適用できるようにしました。  
下記が指定されているとき、
```javascript
g_presetObj.customJs = `init.js,initX.js`;
```
譜面ヘッダーで下記を指定した場合、
```
|customJs=*,special.js|
```
次の定義と同じ意味になります。
```
|customJs=init.js,initX.js,special.js|
```

#### 言語別設定 (ver32.7.0以降)

|通常設定|言語別設定(Ja)|言語別設定(En)|
|----|----|----|
|customJs|customJs**Ja**|customJs**En**|
|customjs|customjs**Ja**|customjs**En**|

### 関連項目
- [masktitleButton](dos-h0043-masktitleButton) [:pencil:](dos-h0043-masktitleButton/_edit) タイトル画面上のボタン群の有効/無効設定
- [maskresultButton](dos-h0044-maskresultButton) [:pencil:](dos-h0044-maskresultButton/_edit) リザルト画面上のボタン群の有効/無効設定
- [**skinType**](dos-h0054-skinType) [:pencil:](dos-h0054-skinType/_edit) スキン設定
- [**settingType**](dos-h0056-settingType) [:pencil:](dos-h0056-settingType/_edit) 共通設定名
- [customCss](dos-h0086-customcss) [:pencil:](dos-h0086-customcss/_edit) カスタムcssファイルの指定
- [共通設定ファイル](dos_setting) &gt; [カスタムファイル設定](dos-s0002-customFile)

### 更新履歴

|Version|変更内容|
|----|----|
|[v32.7.0](https://github.com/cwtickle/danoniplus/releases/tag/v32.7.0)|・キャメル表記での指定に対応<br>・言語別の設定に対応|
|[v27.1.0](https://github.com/cwtickle/danoniplus/releases/tag/v27.1.0)|・共通設定ファイルで指定した内容を略記指定できる機能を実装|
|[v25.3.0](https://github.com/cwtickle/danoniplus/releases/tag/v25.3.0)|・最大2ファイルの制限を撤廃|
|[v19.3.0](https://github.com/cwtickle/danoniplus/releases/tag/v19.3.0)|・カレント＋サブディレクトリ指定に対応|
|[v10.0.0](https://github.com/cwtickle/danoniplus/releases/tag/v10.0.0)|・`(..)`を先頭に指定することで作品ページと同じフォルダを参照するように変更|
|[v1.0.0<br>(v0.40.0)](https://github.com/cwtickle/danoniplus/releases/tag/v1.0.1)|・初回実装|

| [<- resultValsView](dos-h0095-resultValsView) || **customJs** | [customCss ->](dos-h0086-customcss) |