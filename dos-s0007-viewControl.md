**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-s0007-viewControl) | Japanese** 

[↑ 共通設定ファイル仕様に戻る](dos_setting)

[ <- オプション有効化 ](dos-s0006-settingUse) | **プレイ画面制御** | [リザルトデータ -> ](dos-s0008-resultVals)

## プレイ画面制御
### Reverse時の歌詞の自動反転制御設定 (g_presetObj.wordAutoReverse)
⇒ 指定があった場合に優先される譜面ヘッダー：[wordAutoReverse](dos-h0069-wordAutoReverse)
- 通常は以下の条件でReverseが指定された場合、歌詞表示を反転します。  
この設定をどのように制御するか設定します。
  - 上下スクロールを挟まないキーに限定（5key, 7key, 7ikey, 9A/9Bkeyなど）
  - リバース・スクロール拡張用の歌詞表示（wordRev_data / wordAlt_data）が設定されていない作品
  - SETTINGS 画面で Reverse：ON、Scroll：--- (指定なし) を指定してプレイ開始した場合
  - 歌詞表示がすべて1段表示の場合

- 設定可能な値は以下の通りです。  
（譜面ヘッダー：[wordAutoReverse](dos-h0069-wordAutoReverse)と同じ）

|値|既定|内容|
|----|----|----|
|auto|*|Reverse時に歌詞表示を条件付きで反転する|
|OFF||Reverse時に歌詞表示を反転しない|
|ON||Reverse時に条件を満たさなくても歌詞を反転する　※|

※スクロール拡張（Cross, Splitなど）を設定している場合や、  
　wordRev_dataが含まれている場合は反転しません。  
　11keyなど本来適用しないキーや、歌詞が2段になっているケースが反転可能です。 

```javascript
g_presetObj.wordAutoReverse = `auto`;
```
### フェードイン前のデータを保持しない種別の設定 (g_presetObj.unStockCategories)
⇒ 指定があった場合に優先される譜面ヘッダー：[unStockCategory](dos-h0084-unStockCategory)
- ver25.0.0より、フェードイン時にフェードイン前のデータを原則保持し、  
プレイ開始時にそのデータを同時に表示する形式に変わりました。
ただ、作品によっては不都合が出ることが想定されるため、この設定を使って、従来通り保持しない設定をまとめて行うことができます。

```javascript
g_presetObj.unStockCategories = [`back`, `mask`];
```
※譜面ヘッダーと同時に指定した場合、両方の設定が反映されます。

### フェードイン前のデータを保持しないパターンの設定 (g_presetObj.stockForceDelList)
⇒ 指定があった場合に優先される譜面ヘッダー：[word/back/maskStockForceDel](dos-h0085-stockForceDel)
- ver25.0.0より、フェードイン時にフェードイン前のデータを原則保持し、  
プレイ開始時にそのデータを同時に表示する形式に変わりました。
g_presetObj.unStockCategories と同様、作品によっては不都合が出ることを回避するための設定ですが、この設定では種別ごとにどのパターンを除外するかを指定することが可能です。

|種別|無効にするパターンの対象|
|----|----|
|word (歌詞表示)|歌詞表示本体|
|back, mask (背景・マスク)|アニメーション名|

```javascript
g_presetObj.stockForceDelList = {
	word: [],
	back: [`fade`, `out`], // アニメーション名に「fade」「Fade」「out」が含まれる場合はフェードイン前のデータは読み込まない
	mask: [],
};
```
※英字は小文字で指定する必要があります。  
※譜面ヘッダーと同時に指定した場合、両方の設定が反映されます。

### ショートカットキーエリアの横幅拡張設定 (g_presetObj.scAreaWidth)
⇒ 指定があった場合に優先される譜面ヘッダー：[scArea](dos-h0096-scArea)
- プレイ画面でショートカットキー表示を行うために拡張する幅をpx単位で指定できます。  
デフォルトは0pxです。ショートカットキー表示は既定（リトライ: BackSpace, タイトルバック: Delete）と異なる場合に表示します。

```javascript
g_presetObj.scAreaWidth = 80;
```

### プレイ画面の表示レイアウト (g_presetObj.playingLayout)
⇒ 指定があった場合に優先される譜面ヘッダー：[scArea](dos-h0096-scArea)
- プレイ画面でショートカットキー表示を行う際、拡張先を左右に拡げるか右側だけ拡げるかを設定します。

```javascript
g_presetObj.playingLayout = `left`;
```

|設定例|既定|内容|
|----|----|----|
|left||左寄せ (右側のみ拡張)|
|center|*|中央寄せ (左右両方に拡張)|

### ゲーム表示エリアのX, Y座標 (g_presetObj.playingX, g_presetObj.playingY)
⇒ 指定があった場合に優先される譜面ヘッダー：[playingX, playingY](dos-h0070-playingX)
- プレイ画面のうち、ゲーム表示エリアのX座標(playingX), Y座標(playingY)を設定します。  
デフォルトはどちらも0pxです。
```javascript
g_presetObj.playingX = 50;
g_presetObj.playingY = 20;
```

### ゲーム表示エリアの横幅、高さ (g_presetObj.playingWidth, g_presetObj.playingHeight)
⇒ 指定があった場合に優先される譜面ヘッダー：[playingWidth, playingHeight](dos-h0071-playingWidth)
- プレイ画面のうち、ゲーム表示エリアの横幅、高さを指定します。  
デフォルトはプレイ画面の横幅／高さです。
```javascript
g_presetObj.playingWidth = 600;
g_presetObj.playingHeight = 570;
```

<img src="https://user-images.githubusercontent.com/44026291/199850377-f4516af7-d8c4-452e-8561-4f6f4eade429.png" width="100%">


## 更新履歴

|Version|変更内容|
|----|----|
|[v33.7.0](https://github.com/cwtickle/danoniplus/releases/tag/v33.7.0)|・ゲーム表示エリアのX, Y座標位置設定(g_presetObj.playingX, g_presetObj.playingY), 横幅、高さ設定(g_presetObj.playingWidth, g_presetObj.playingHeight)を追加|
|[v33.4.0](https://github.com/cwtickle/danoniplus/releases/tag/v33.4.0)|・ショートカットキーエリアの横幅拡張設定(g_presetObj.scAreaWidth), プレイ画面の表示レイアウト設定(g_presetObj.playingLayout)を追加|
|[v26.3.1](https://github.com/cwtickle/danoniplus/releases/tag/v26.3.1)|・変数名変更<br>　・g_presetWordAutoReverse -> g_presetObj.wordAutoReverse<br>　・g_presetUnStockCategories -> g_presetObj.unStockCategories<br>　・g_presetStockForceDelList -> g_presetObj.stockForceDelList|
|[v25.0.0](https://github.com/cwtickle/danoniplus/releases/tag/v25.0.0)|・フェードイン前のデータを保持しない設定(g_presetUnStockCategories, g_presetStockForceDelList)を追加|
|[v15.4.0](https://github.com/cwtickle/danoniplus/releases/tag/v15.4.0)|・Reverse時の歌詞の自動反転制御設定 (g_presetWordAutoReverse)を実装|

[ <- オプション有効化 ](dos-s0006-settingUse) | **プレイ画面制御** | [リザルトデータ -> ](dos-s0008-resultVals)