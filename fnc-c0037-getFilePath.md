[← オブジェクトリファレンスに戻る](ObjectReferenceIndex)  

# getFilePath
### 概要
- 譜面ヘッダーで指定されたファイルパスを、キーワードとディレクトリに分割する関数。

### 引数（括弧内はデフォルト値）

|引数|型|必須|指定内容|
|----|----|----|----|
|_fileName|string|*|ファイルパス|
|_directory|string||デフォルトディレクトリ。既定は`(空)`。|

### 返却値
- [ファイルキーワード, 参照するルートディレクトリ]

### 使用例
```javascript
// カスタムjsの場合
console.log(getFilePath(`file.txt`, `../js/`)); // [`file.txt`, `../js/`] => ../js/file.txt
console.log(getFilePath(`(..)file.txt`, `../js/`)); // [`file.txt`, ``] => file.txt

// スキンの場合
console.log(getFilePath(`(..)light`, `../skin/`)); // [`light`, ``] => danoni_skin_light.css
console.log(getFilePath(`sub/blue`, `../skin/`)); // [`blue`, `../skin/sub/`] => ../skin/sub/danoni_skin_blue.css
```

### 関連項目
- [**customJs**](dos-h0019-customjs) [:pencil:](dos-h0019-customjs/_edit) カスタムjsファイルの指定
- [**skinType**](dos-h0054-skinType) [:pencil:](dos-h0054-skinType/_edit) スキン設定
- [**settingType**](dos-h0056-settingType) [:pencil:](dos-h0056-settingType/_edit) 共通設定名
