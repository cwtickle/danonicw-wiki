**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-h0053-customGauge) | Japanese** 

[Home](./) &gt; [譜面ヘッダー仕様](dos_header) &gt; [設定時の初期設定](dos_header#-設定時の初期設定)

| [<- customFont](dos-h0020-customFont) | **customGauge** | [gaugeX ->](dos-h0022-gaugeX) |

## customGauge
- カスタムゲージリストの設定
- 共通設定 ⇒ [g_presetObj.gaugeList](dos-s0003-initialGauge#カスタムゲージリスト-g_presetobjgaugelist)

### 使い方
```
|customGauge=Original::F,Normal::V,Escape::V|
|customGauge2=Original::F,Normal::V,Escape::V,SuddenDeath::F|
|customGauge3=customDefault| // 共通設定ファイルより g_presetObj.gaugeList に設定されたリストを継承
|customGauge4=survival|      // ライフ制ゲージの設定を継承
|customGauge5=border|        // ノルマ制ゲージの設定を継承

|gaugeOriginal=x,4,60,25$x,3,20,25|
|gaugeNormal=50,70,1,25$x,3,25,25|
|gaugeEscape=0,1,50,100$x,5,30,25|
```

### 説明
- [gaugeX](dos-h0022-gaugeX)と合わせて使用します。
- カンマ区切りで、ゲージ設定毎の設定を行います。  
「ゲージ名::回復/ダメージ量設定」の組み合わせで設定します。  
回復/ダメージ量設定は、「F」なら矢印数によらず固定、  
「V」なら矢印数により変動します。  
- ライフ制/ノルマ制の区別は各個別のゲージ設定([gaugeX](dos-h0022-gaugeX))にて行います。  
このため、ノルマ制ながら回復/ダメージ量は固定にするといった設定が可能になります。  

#### 特殊な指定方法
- 既存のゲージ設定リストを継承する設定が可能です。

|設定名|継承先のリスト|
|----|----|
|survival|ライフ制ゲージ<br>[`Original`, `Heavy`, `NoRecovery`, `SuddenDeath`, `Practice`, `Light`]|
|border|ノルマ制ゲージ<br>[`Normal`, `Hard`, `SuddenDeath`, `Easy`]|
|customDefault|共通設定ファイルの g_presetObj.gaugeList で定義されたリスト|

### 補足
- カスタムゲージを指定した場合は [gaugeX](dos-h0022-gaugeX) の指定 もしくは  
`danoni_setting.js`の`g_presetObj.gaugeCustom`の設定が必須です。  
また、OriginalとNormalがデフォルトゲージで無い場合についても、  
設定が必須となります。ご注意ください。  
- 原則、ライフ設定は [difData](dos-h0002-difData) ではなく [gaugeX](dos-h0022-gaugeX) で行い、  
自動設定されているゲージ設定についても、個別に設定してください。
- カスタムゲージを設定した場合、その作品全体でゲージ設定が共有されます。  
譜面個別にゲージ設定を設定する場合は、customGauge2, customGauge3, ...で指定します。
- `danoni_setting.js`の`g_presetObj.gaugeCustom`に追加するゲージ設定を記載することで  
どの作品に対してもカスタムゲージを適用できるようになります。

```javascript
// ゲージ設定（デフォルト以外）
g_presetObj.gaugeCustom = {
	// (省略)
	Escape: {
		Border: 70,
		Recovery: 1,
		Damage: 50,
		Init: 70,
	},
};
```

### 関連項目
- [**difData**](dos-h0002-difData) [:pencil:](dos-h0002-difData/_edit) 譜面情報 
- [**gaugeX**](dos-h0022-gaugeX) [:pencil:](dos-h0022-gaugeX/_edit) ゲージ設定の詳細
- [maxLifeVal](dos-h0045-maxLifeVal) [:pencil:](dos-h0045-maxLifeVal/_edit) ライフの上限値
- [frzStartjdgUse](dos-h0037-frzStartjdgUse) [:pencil:](dos-h0037-frzStartjdgUse/_edit) フリーズアロー開始判定の設定有無
- [frzAttempt](dos-h0038-frzAttempt) [:pencil:](dos-h0038-frzAttempt/_edit) フリーズアローヒット時の許容フレーム数
- [共通設定ファイル仕様](dos_setting) &gt; [ゲージ設定](dos-s0003-initialGauge)

### 更新履歴

|Version|変更内容|
|----|----|
|[v21.1.0](https://github.com/cwtickle/danoniplus/releases/tag/v21.1.0)|・2譜面目以降の個別設定、既存リスト継承設定に対応|
|[v9.4.0](https://github.com/cwtickle/danoniplus/releases/tag/v9.4.0)|・初回実装|

| [<- customFont](dos-h0020-customFont) | **customGauge** | [gaugeX ->](dos-h0022-gaugeX) |