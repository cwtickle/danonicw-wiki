**[English](https://github.com/cwtickle/danoniplus-docs/wiki/Changelog-v25) | Japanese** 

[^ Back to All Version List (Security Policy)](https://github.com/cwtickle/danoniplus/security/policy)  
[^ Back to Update Info](./UpdateInfo#-v25-changelog)

[**<- v26**](Changelog-v26) | **v25** | [**v24 ->**](Changelog-v24)  
(**🔖 [18 Releases](https://github.com/cwtickle/danoniplus/releases?q=tag%3Av25)** )

## 🔃 Files changed (v25)

|Directory|FileName||Last Updated|
|----|----|----|----|
|/js|danoni_main.js|[📥](https://github.com/cwtickle/danoniplus/releases/download/v25.5.10/danoni_main.js)|**v25.5.10**|
|/js/lib|danoni_constants.js|[📥](https://github.com/cwtickle/danoniplus/releases/download/v25.5.8/danoni_constants.js)|[v25.5.8](Changelog-v25#v2558-2022-04-16)|

<details>
<summary>Changed file list before v24</summary>

|Directory|FileName||Last Updated|
|----|----|----|----|
|/js/lib|danoni_localbinary.js|[📥](https://github.com/cwtickle/danoniplus/releases/download/v15.1.0/danoni_localbinary.js)|[v15.1.0](./Changelog-v15#v1510-2020-05-21)|
|/css|danoni_main.css|[📥](https://github.com/cwtickle/danoniplus/releases/download/v21.4.2/danoni_main.css)|[v21.4.2](Changelog-v21#v2142-2021-04-07)|
|/img|aaShadow.svg<br>arrow.svg<br>arrowShadow.svg<br>borderline.svg<br>c.svg<br>cursor.svg<br>giko.svg<br>iyo.svg<br>monar.svg<br>morara.svg<br>onigiri.svg|[📥](https://github.com/cwtickle/danoniplus/releases/download/v15.1.0/img.zip)|[v15.1.0](./Changelog-v15#v1510-2020-05-21)|
|/skin|danoni_skin_default.css<br>danoni_skin_light.css<br>danoni_skin_skyblue.css|[📥](https://github.com/cwtickle/danoniplus/releases/download/v21.4.2/danoni_skin_default.css)<br>[📥](https://github.com/cwtickle/danoniplus/releases/download/v21.4.2/danoni_skin_light.css)<br>[📥](https://github.com/cwtickle/danoniplus/releases/download/v21.4.2/danoni_skin_skyblue.css)|[v21.4.2](Changelog-v21#v2142-2021-04-07)|

</details>

⭐ New Features / 🛠️ Improvements / 🐞 Bug Fixes  
( Files changed: ⚪ danoni_main.js / 🔵 danoni_constants.js / 🔴 css files )

## [Warning] 更新終了後の不具合情報
- [更新を終了したバージョンの不具合情報#v25](DeprecatedVersionBugs#v25) を参照

## v25.5.10 ([2022-08-03](https://github.com/cwtickle/danoniplus/releases/tag/v25.5.10))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v25.5.10/total)](https://github.com/cwtickle/danoniplus/archive/v25.5.10.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v25.5.10/support/v25?style=social)](https://github.com/cwtickle/danoniplus/compare/v25.5.10...support/v25#files_bucket) 
- 🐞 **C)** 重複するID名の解消 ( PR [#1300](https://github.com/cwtickle/danoniplus/pull/1300) ) <- :boom: [**v11.4.0**](Changelog-v11#v1140-2020-02-05)
- 🐞 **C)** 譜面明細画面でID名に英数以外が入りうる問題の修正 ( PR [#1300](https://github.com/cwtickle/danoniplus/pull/1300) ) <- :boom: [**v19.3.0**](Changelog-v19#v1930-2021-01-30)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v25.5.10)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v25.5.9...v25.5.10#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v25.5.10)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v25.5.10.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v25.5.10.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v25.5.10/js/danoni_main.js)
/ 🎣 [**v27.8.1**](./Changelog-v27#v2781-2022-08-01)

## v25.5.9 ([2022-05-22](https://github.com/cwtickle/danoniplus/releases/tag/v25.5.9))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v25.5.9/total)](https://github.com/cwtickle/danoniplus/archive/v25.5.9.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v25.5.9/support/v25?style=social)](https://github.com/cwtickle/danoniplus/compare/v25.5.9...support/v25#files_bucket) 
- 🐞 **B)** フォルダ名に半角スペースや全角文字が含まれている場合に起動できないことがある問題を修正 ( PR [#1289](https://github.com/cwtickle/danoniplus/pull/1289) ) <- :boom: [**v25.5.7**](Changelog-v25#v2557-2022-04-10)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v25.5.9)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v25.5.8...v25.5.9#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v25.5.9)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v25.5.9.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v25.5.9.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v25.5.9/js/danoni_main.js)
/ 🎣 [**v27.5.1**](./Changelog-v27#v2751-2022-05-22)

## v25.5.8 ([2022-04-16](https://github.com/cwtickle/danoniplus/releases/tag/v25.5.8))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v25.5.8/total)](https://github.com/cwtickle/danoniplus/archive/v25.5.8.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v25.5.8/support/v25?style=social)](https://github.com/cwtickle/danoniplus/compare/v25.5.8...support/v25#files_bucket) 
- 🛠️ 11ikeyの別パターンのアシスト設定に対応 ( PR [#1282](https://github.com/cwtickle/danoniplus/pull/1282) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v25.5.8)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v25.5.7...v25.5.8#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v25.5.8)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v25.5.8.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v25.5.8.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v25.5.8/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v25.5.8/js/lib/danoni_constants.js)
/ 🎣 [**v27.4.0**](./Changelog-v27#v2740-2022-04-16)

## v25.5.7 ([2022-04-10](https://github.com/cwtickle/danoniplus/releases/tag/v25.5.7))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v25.5.7/total)](https://github.com/cwtickle/danoniplus/archive/v25.5.7.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v25.5.7/support/v25?style=social)](https://github.com/cwtickle/danoniplus/compare/v25.5.7...support/v25#files_bucket) 
- 🐞 **C)** カレントパス、譜面ファイル取得部分をエスケープするよう修正 ( PR [#1277](https://github.com/cwtickle/danoniplus/pull/1277) ) <- :boom: [**v19.4.1**](Changelog-v19#v1941-2021-02-06)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v25.5.7)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v25.5.6...v25.5.7#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v25.5.7)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v25.5.7.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v25.5.7.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v25.5.7/js/danoni_main.js)
/ 🎣 [**v27.3.1**](./Changelog-v27#v2731-2022-04-10)

## v25.5.6 ([2022-02-20](https://github.com/cwtickle/danoniplus/releases/tag/v25.5.6))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v25.5.6/total)](https://github.com/cwtickle/danoniplus/archive/v25.5.6.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v25.5.6/support/v25?style=social)](https://github.com/cwtickle/danoniplus/compare/v25.5.6...support/v25#files_bucket) 
- 🐞**C)** g_lblRenames未定義時に各種名称の上書きができない問題を修正 ( PR [#1230](https://github.com/cwtickle/danoniplus/pull/1230) ) <- :boom: [**v20.3.1**](Changelog-v20#v2031-2021-02-24)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v25.5.6)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v25.5.5...v25.5.6#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v25.5.6)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v25.5.6.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v25.5.6.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v25.5.6/js/danoni_main.js)
/ 🎣 [**v26.2.0**](./Changelog-v26#v2620-2022-02-20)

## v25.5.5 ([2022-02-16](https://github.com/cwtickle/danoniplus/releases/tag/v25.5.5))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v25.5.5/total)](https://github.com/cwtickle/danoniplus/archive/v25.5.5.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v25.5.5/support/v25?style=social)](https://github.com/cwtickle/danoniplus/compare/v25.5.5...support/v25#files_bucket) 
- 🛠️ npm環境用にディレクトリ構成を見直し ( PR [#1228](https://github.com/cwtickle/danoniplus/pull/1228) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v25.5.5)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v25.5.4...v25.5.5#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v25.5.5)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v25.5.5.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v25.5.5.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v25.5.5/js/danoni_main.js)
/ 🎣 [**v26.1.2**](./Changelog-v26#v2612-2022-02-16)

## v25.5.4 ([2022-02-05](https://github.com/cwtickle/danoniplus/releases/tag/v25.5.4))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v25.5.4/total)](https://github.com/cwtickle/danoniplus/archive/v25.5.4.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v25.5.4/support/v25?style=social)](https://github.com/cwtickle/danoniplus/compare/v25.5.4...support/v25#files_bucket) 
- 🐞**C)** メイン画面のスキン拡張が利いていない問題を修正 ( PR [#1220](https://github.com/cwtickle/danoniplus/pull/1220) ) <- :boom: [**v25.3.0**](Changelog-v25#v2530-2022-01-16)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v25.5.4)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v25.5.3...v25.5.4#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v25.5.4)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v25.5.4.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v25.5.4.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v25.5.4/js/danoni_main.js)
/ 🎣 [**v26.1.0**](./Changelog-v26#v2610-2022-02-05)

## v25.5.3 ([2022-01-30](https://github.com/cwtickle/danoniplus/releases/tag/v25.5.3))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v25.5.3/total)](https://github.com/cwtickle/danoniplus/archive/v25.5.3.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v25.5.3/support/v25?style=social)](https://github.com/cwtickle/danoniplus/compare/v25.5.3...support/v25#files_bucket) 
- 🐞**B)** 全体色変化かつフリーズヒット時、AAの色が変わらない問題を修正 ( PR [#1218](https://github.com/cwtickle/danoniplus/pull/1218) ) <- :boom: [**v25.1.0**](Changelog-v25#v2510-2022-01-07)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v25.5.3)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v25.5.2...v25.5.3#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v25.5.3)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v25.5.3.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v25.5.3.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v25.5.3/js/danoni_main.js)
/ 🎣 [**v26.0.0**](./Changelog-v26#v2600-2022-01-30)

----

## v25.5.2 ([2022-01-29](https://github.com/cwtickle/danoniplus/releases/tag/v25.5.2))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v25.5.2/total)](https://github.com/cwtickle/danoniplus/archive/v25.5.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v25.5.2/support/v25?style=social)](https://github.com/cwtickle/danoniplus/compare/v25.5.2...support/v25#files_bucket) 
- 🐞**B)** シャッフル・カラーグループを変更した際にその内容が反映されない問題を修正 ( PR [#1215](https://github.com/cwtickle/danoniplus/pull/1215), Gitter [2022-01-29](https://gitter.im/danonicw/community?at=61f42696bfe2f54b2e3f0422) ) <- :boom: [**v24.5.1**](Changelog-v24#v2451-2021-12-13)
- 🐞**C)** 結果画面のNo Recordの文字が推定Adjと重なる問題を修正 ( PR [#1216](https://github.com/cwtickle/danoniplus/pull/1216), Gitter [2022-01-29](https://gitter.im/danonicw/community?at=61f4147fd41a5853f9728967) ) <- :boom: [**v25.5.1**](Changelog-v25#v2551-2022-01-26)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v25.5.2)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v25.5.1...v25.5.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v25.5.2)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v25.5.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v25.5.2.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v25.5.2/js/danoni_main.js)<br>❤️ すずめ (@suzme), izkdic

## v25.5.1 ([2022-01-26](https://github.com/cwtickle/danoniplus/releases/tag/v25.5.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v25.5.1/total)](https://github.com/cwtickle/danoniplus/archive/v25.5.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v25.5.1/support/v25?style=social)](https://github.com/cwtickle/danoniplus/compare/v25.5.1...support/v25#files_bucket) 
- ⭐ 結果画面に推定Adjustment値を表示する機能を追加 ( PR [#1210](https://github.com/cwtickle/danoniplus/pull/1210), Gitter [2022-01-24, 25](https://gitter.im/danonicw/community?at=61ee6e44742c3d4b21b22154) )
- ⭐ 矢印色変化に対するフリーズアローの追従設定を分離 ( PR [#1211](https://github.com/cwtickle/danoniplus/pull/1211), Gitter [2022-01-26](https://gitter.im/danonicw/community?at=61f0389c9a335454063e2e1d) )
- 🛠️ 二重定義箇所、定義コメントの誤りを修正 ( PR [#1209](https://github.com/cwtickle/danoniplus/pull/1209) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v25.5.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v25.4.1...v25.5.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v25.5.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v25.5.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v25.5.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v25.5.1/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v25.5.1/js/lib/danoni_constants.js)<br>❤️ izkdic, ぷろろーぐ (@prlg25), aconite


## v25.4.1 ([2022-01-22](https://github.com/cwtickle/danoniplus/releases/tag/v25.4.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v25.4.1/total)](https://github.com/cwtickle/danoniplus/archive/v25.4.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v25.4.1/support/v25?style=social)](https://github.com/cwtickle/danoniplus/compare/v25.4.1...support/v25#files_bucket) 
- 🐞**A)** http環境でクリップボードコピーに失敗する問題を修正 ( PR [#1207](https://github.com/cwtickle/danoniplus/pull/1207) ) <- 💥 [**v25.4.0**](Changelog-v25#v2540-2022-01-22)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v25.4.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v25.4.0...v25.4.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v25.4.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v25.4.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v25.4.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v25.4.1/js/danoni_main.js)

## v25.4.0 ([2022-01-22](https://github.com/cwtickle/danoniplus/releases/tag/v25.4.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v25.4.0/total)](https://github.com/cwtickle/danoniplus/archive/v25.4.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v25.4.0/support/v25?style=social)](https://github.com/cwtickle/danoniplus/compare/v25.4.0...support/v25#files_bucket) 
- ⭐ 背景・マスクモーションで使用する画像パスのカレントディレクトリ指定に対応 ( PR [#1204](https://github.com/cwtickle/danoniplus/pull/1204) )
- ⭐ キー名部分について名称を変更できるよう対応 ( PR [#1205](https://github.com/cwtickle/danoniplus/pull/1205) )
- 🛠️ クリップボードコピーの方法について Clipboard API を使うよう変更 ( PR [#1203](https://github.com/cwtickle/danoniplus/pull/1203) )
- 🛠️ スライダー処理を一部共通化 ( PR [#1205](https://github.com/cwtickle/danoniplus/pull/1205) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v25.4.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v25.3.3...v25.4.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v25.4.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v25.4.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v25.4.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v25.4.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v25.4.0/js/lib/danoni_constants.js) 

## v25.3.3 ([2022-01-19](https://github.com/cwtickle/danoniplus/releases/tag/v25.3.3))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v25.3.3/total)](https://github.com/cwtickle/danoniplus/archive/v25.3.3.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v25.3.3/support/v25?style=social)](https://github.com/cwtickle/danoniplus/compare/v25.3.3...support/v25#files_bucket) 
- 🐞**B)** skinTypeがDefaultのとき、背景が正しく表示されないことがある問題を修正 ( PR [#1201](https://github.com/cwtickle/danoniplus/pull/1201) ) <- 💥 [**v25.3.0**](Changelog-v25#v2530-2022-01-16)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v25.3.3)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v25.3.2...v25.3.3#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v25.3.3)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v25.3.3.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v25.3.3.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v25.3.3/js/danoni_main.js)

## v25.3.2 ([2022-01-18](https://github.com/cwtickle/danoniplus/releases/tag/v25.3.2))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v25.3.2/total)](https://github.com/cwtickle/danoniplus/archive/v25.3.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v25.3.2/support/v25?style=social)](https://github.com/cwtickle/danoniplus/compare/v25.3.2...support/v25#files_bucket) 
- 🛠️ 0フレーム指定の色変化、モーションデータに対応 ( PR [#1199](https://github.com/cwtickle/danoniplus/pull/1199) )
- 🐞**B)** g_scoreObj.baseFrame の初期化誤りを修正 ( Issue [#1196](https://github.com/cwtickle/danoniplus/pull/1196), PR [#1197](https://github.com/cwtickle/danoniplus/pull/1197) ) <- 💥 [**v25.3.0**](Changelog-v25#v2530-2022-01-16)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v25.3.2)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v25.3.0...v25.3.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v25.3.2)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v25.3.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v25.3.2.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v25.3.2/js/danoni_main.js)

## v25.3.0 ([2022-01-16](https://github.com/cwtickle/danoniplus/releases/tag/v25.3.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v25.3.0/total)](https://github.com/cwtickle/danoniplus/archive/v25.3.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v25.3.0/support/v25?style=social)](https://github.com/cwtickle/danoniplus/compare/v25.3.0...support/v25#files_bucket) 
- ⭐ customjs, skinTypeの2ファイル制限を撤廃 ( PR [#1191](https://github.com/cwtickle/danoniplus/pull/1191) )
- ⭐ カスタム関数 （XXXInit, XXXInit2 など）について、XXXInitが無くてもXXXInit2の直接呼び出しが可能になるよう変更 ( PR [#1191](https://github.com/cwtickle/danoniplus/pull/1191) )
- ⭐ 独自のcssファイルを読み込む設定を実装 ( PR [#1193](https://github.com/cwtickle/danoniplus/pull/1193) )
- 🛠️ 14i, 16ikeyのAA部分の代替キーを設定 ( PR [#1192](https://github.com/cwtickle/danoniplus/pull/1192) )

<img src="https://user-images.githubusercontent.com/44026291/149642999-a7ff5c38-ad49-4faa-89b6-f19809701885.png" width="45%"><img src="https://user-images.githubusercontent.com/44026291/149643087-4d7f6966-2166-4eb0-9eff-97de9f615ec5.png" width="45%">

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v25.3.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v25.2.0...v25.3.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v25.3.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v25.3.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v25.3.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v25.3.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v25.3.0/js/lib/danoni_constants.js)<br>❤️ izkdic

## v25.2.0 ([2022-01-15](https://github.com/cwtickle/danoniplus/releases/tag/v25.2.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v25.2.0/total)](https://github.com/cwtickle/danoniplus/archive/v25.2.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v25.2.0/support/v25?style=social)](https://github.com/cwtickle/danoniplus/compare/v25.2.0...support/v25#files_bucket) 
- ⭐ キーコンフィグ画面でオブジェクトの一部が隠れる場合に横スクロールできるよう対応 ( PR [#1189](https://github.com/cwtickle/danoniplus/pull/1189) )
- 🛠️ 設定画面間のページ移動を関数化 ( PR [#1189](https://github.com/cwtickle/danoniplus/pull/1189) )

<img src="https://user-images.githubusercontent.com/44026291/149605159-65555f25-0138-4868-911c-518db43a5709.png" width="45%"><img src="https://user-images.githubusercontent.com/44026291/149605174-9f804029-48a9-4b07-8eff-436b6e411e19.png" width="45%">

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v25.2.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v25.1.0...v25.2.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v25.2.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v25.2.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v25.2.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v25.2.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v25.2.0/js/lib/danoni_constants.js) 


## v25.1.0 ([2022-01-07](https://github.com/cwtickle/danoniplus/releases/tag/v25.1.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v25.1.0/total)](https://github.com/cwtickle/danoniplus/archive/v25.1.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v25.1.0/support/v25?style=social)](https://github.com/cwtickle/danoniplus/compare/v25.1.0...support/v25#files_bucket) 
- ⭐ 条件付きで矢印の色変化に追随してフリーズアローにも同じ色変化を適用する機能を実装 ( Issue [#869](https://github.com/cwtickle/danoniplus/pull/869), PR [#1187](https://github.com/cwtickle/danoniplus/pull/1187) )
- ⭐ フェードイン時に個別色変化と全体色変化が混在している場合でも色変化が反映されるよう変更 ( Issue [#1186](https://github.com/cwtickle/danoniplus/pull/1186), PR [#1187](https://github.com/cwtickle/danoniplus/pull/1187) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v25.1.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v25.0.0...v25.1.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v25.1.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v25.1.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v25.1.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v25.1.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v25.1.0/js/lib/danoni_constants.js)<br>❤️ ぷろろーぐ (@prlg25)

## v25.0.0 ([2022-01-04](https://github.com/cwtickle/danoniplus/releases/tag/v25.0.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v25.0.0/total)](https://github.com/cwtickle/danoniplus/archive/v25.0.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v25.0.0/support/v25?style=social)](https://github.com/cwtickle/danoniplus/compare/v25.0.0...support/v25#files_bucket) 
- ⭐ フェードイン時にそれまでの歌詞表示、背景・マスク表示データを保持するように変更 ( PR [#1184](https://github.com/cwtickle/danoniplus/pull/1184) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v25.0.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v24.6.0...v25.0.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v25.0.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v25.0.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v25.0.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v25.0.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v25.0.0/js/lib/danoni_constants.js)<br>❤️ MFV2 (@MFV2)

[**<- v26**](Changelog-v26) | **v25** | [**v24 ->**](Changelog-v24)  
