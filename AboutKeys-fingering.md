**[English](https://github.com/cwtickle/danoniplus-docs/wiki/AboutKeys-fingering) | Japanese** 

| [< ゲーム内ショートカット](./Shortcut) | **多鍵の種類について** | [ローカルでのプレイ方法 >](./HowToLocalPlay) |

[<- Home](./) || [運指型](AboutKeys-fingering) | [バランス型](AboutKeys-balance) | [指移動型](AboutKeys-finger-movement) | [スクラッチ型](AboutKeys-scratch) | [オリジナル](AboutKeys-customKeys)

# 多鍵の種類（運指型）
- 状況によって対応キーの打ち方を変えるパターン。  
自由度が高く、その分そのキーに特化したいろいろな打ち方ができるキーと言えるでしょう。  
その特性ゆえか、癖のあるキーが多いのも特徴の一つです。  

## 5key
[ 難易度表 : [片手通常](http://dodl4.g3.xrea.com/normal5s/) / [両手通常](http://dodl4.g3.xrea.com/normal5d/) / [片手発狂](http://dodl4.g3.xrea.com/insane5s/) / [両手発狂](http://dodl4.g3.xrea.com/insane5d/) ]

<img src="./wiki/keyimg/key5_02.png" width="40%">
<img src="./wiki/keyimg/key5_03.png" width="40%">

<img src="./wiki/keyimg/key5_01.jpg" width="40%">

### 矢印キーを使った配列
<img src="./wiki/keys/key5_1.png" width="80%">

### 両手プレイ時の配列
<img src="./wiki/keys/key5_2.png" width="80%">

- Dancing★Onigiriの代名詞とも言えるおにぎりが、矢印キーの横についたものが5keyです。  
矢印キーとスペースでプレイする、片手5keyの打ち方が一般的です。

- 7keyに近い「D F J K スペース」「D F J K L」といった明確に両手で5keyをするスタイルと、
同じ矢印キーとスペースでも、状況により両手に持ち替えてプレイする両手打ちがあります。

- 両手5keyを行うメリットとしては、同時押しが多重に連続する場合に両手で打つことで負荷を軽減できるメリットがあります。
一方で、矢印キーに集中するためおにぎりの多い譜面には不向きと言われています。

- シンプルながらいろんな打ち方・譜面幅のある5keyは、  
見方によってはバランス型・スクラッチ型にも取れる部分があり、ある意味で万能だと言えます。

## 12key 
[ 多鍵DB : [12key](http://apoi108.sakura.ne.jp/danoni/ta/?keys=12) / [8key(12key下段)](http://apoi108.sakura.ne.jp/danoni/ta/?keys=8%2812%29) ]

<img src="./wiki/keyimg/key12_01.png" width="40%">
<img src="./wiki/keyimg/key12_02.png" width="40%">

<img src="./wiki/keys/key12.png" width="80%">

- Dancing★Onigiriに存在するキーの中で、特に癖の強いキーがこの12keyです。  
キーボードを縦に使用し、左右の指で矢印を分担しながら処理します。

- 上段(U I 8 O)、中段(J K L)、下段(N M < >)と矢印のやってくる場所を押さえながら捌く必要があり、  
どう押せばいいかは状況によって変わってきます。

- ある程度基本となる型を自分の中で持っておいて、必要に応じ崩していくのがこのキーの特徴です。  
キー種が複雑なため、下段のみ、一部キーのみに絞った譜面も見られます。

## 14key
[ 多鍵DB : [14key](http://apoi108.sakura.ne.jp/danoni/ta/?keys=14) ]

<img src="./wiki/keyimg/key14_01.png" width="60%">  
<img src="./wiki/keys/key14.png" width="80%">

- 癖の強いキー12keyに対し、さらに左右の斜め矢印がついたキーが14keyです。  
上段が増えたことでさらに幅が広がり、より柔軟な捌き方が求められます。  
12keyの強化版の印象が強いものの、代替キーも多いためまた違った特徴を秘めたキーともいえます。  

## 11ikey
[ 多鍵DB : [11ikey](http://apoi108.sakura.ne.jp/danoni/ta/?keys=11i) ]

<img src="./wiki/keyimg/key11i_01.png" width="50%">

<img src="./wiki/keys/key11i.png" width="80%">

- D(ギコ)とK(イヨウ)を中心に4keyが並び、おにぎりが真ん中にある変則11keyです。  
キー配置が独特で、必要に応じて手を傾けたりする動作が入ることからこの分類としました。  

## 17key
[ 多鍵DB : [17key](http://apoi108.sakura.ne.jp/danoni/ta/?keys=17) ]

<img src="./wiki/keyimg/key17_01.png" width="60%">

<img src="./wiki/keys/key17.png" width="80%">

- 左右に多くの矢印が並び、真ん中におにぎりがある17key。  
- スクロールをAlternateにすることでキーを9key+9keyに分離することが可能で、  
キーの視認性も相まって、注目されているキーの一つになっています。

## 23key
[ 多鍵DB : [23key](http://apoi108.sakura.ne.jp/danoni/ta/?keys=23) / [15key(23key下段)](https://apoi108.sakura.ne.jp/danoni/ta/?keys=15%2823%29) ]

<img src="./wiki/keyimg/key23_01.png" width="40%">
<img src="./wiki/keyimg/key23_02.png" width="40%">

<img src="./wiki/keys/key23.png" width="80%">

- 多鍵祭'20に登場した、12keyのダブルプレイ。  
縦横に広くプレイできることが特徴で、自由度が非常に高いです。  

----

### Special Thanks
- しょーでん、decresc.

----

[<- Home](./) || [運指型](AboutKeys-fingering) | [バランス型](AboutKeys-balance) | [指移動型](AboutKeys-finger-movement) | [スクラッチ型](AboutKeys-scratch) | [オリジナル](AboutKeys-customKeys)

|種類|対応キー数|
|----|----|
|[運指型](AboutKeys-fingering)|[5key](AboutKeys-fingering#5key) / [12key](AboutKeys-fingering#12key) / [14key](AboutKeys-fingering#14key) / [11ikey](AboutKeys-fingering#11ikey) / [17key](AboutKeys-fingering#17key) / [23key](AboutKeys-fingering#23key)|
|[バランス型](AboutKeys-balance)|[7key](AboutKeys-balance#7key) / [9Akey[ダブルプレイ]](AboutKeys-balance#9akey-ダブルプレイ) / [9Bkey](AboutKeys-balance#9bkey) / [7ikey](AboutKeys-balance#7ikey) / [9ikey](AboutKeys-balance#9ikey)|
|[指移動型](AboutKeys-finger-movement)|[11key](AboutKeys-finger-movement#11key) / [11Lkey](AboutKeys-finger-movement#11lkey) / [13key[トリプルプレイ]](AboutKeys-finger-movement#13key-トリプルプレイ) / [15Akey](AboutKeys-finger-movement#15akey) / [15Bkey](AboutKeys-finger-movement#15Bkey) / [14ikey](AboutKeys-finger-movement#14ikey) / [16ikey](AboutKeys-finger-movement#16ikey)|
|[スクラッチ型](AboutKeys-scratch)|[8key](AboutKeys-scratch#8key) / [11Wkey](AboutKeys-scratch#11wkey)|
|[オリジナル](AboutKeys-customKeys)|(Various Keys)|

| [< ゲーム内ショートカット](./Shortcut) | **多鍵の種類について** | [ローカルでのプレイ方法 >](./HowToLocalPlay) |
