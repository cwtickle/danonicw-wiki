**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-h0080-readyHtml) | Japanese** 

[Home](./) &gt; [譜面ヘッダー仕様](dos_header) &gt; [プレイ時の初期設定](dos_header#-プレイ時の初期設定) &gt; [画面表示・キーコントロール](dos_header#画面表示キーコントロール)

| [<- readyColor](dos-h0075-readyColor) | **readyHtml** | [finishView ->](dos-h0023-finishView) |

## readyHtml
- Ready?の文字設定

### 使い方
```
|readyHtml=<span style="color:#ff9999">Start!</span>|
```
### 説明
Ready? 表示のテキスト及びスタイルを上書き変更する設定です。HTMLタグが使用できます。  
この設定を使用すると、Ready?文字に掛かっているテキストのCSSスタイルは削除されます。  
なお、アニメーション設定については別設定です。関連項目をご覧ください。

### 関連項目
- [readyDelayFrame](dos-h0052-readyDelayFrame) [:pencil:](dos-h0052-readyDelayFrame/_edit) Ready?が表示されるまでの遅延フレーム数
- [readyAnimationFrame](dos-h0073-readyAnimationFrame) [:pencil:](dos-h0073-readyAnimationFrame/_edit) Ready?のアニメーションフレーム数
- [readyAnimationName](dos-h0074-readyAnimationName) [:pencil:](dos-h0074-readyAnimationName/_edit) Ready?のアニメーション名
- [readyColor](dos-h0075-readyColor) [:pencil:](dos-h0075-readyColor/_edit) Ready?の先頭色設定

### 更新履歴

|Version|変更内容|
|----|----|
|[v19.3.0](https://github.com/cwtickle/danoniplus/releases/tag/v19.3.0)|・初回実装|

| [<- readyColor](dos-h0075-readyColor) | **readyHtml** | [finishView ->](dos-h0023-finishView) |