[← オブジェクトリファレンスに戻る](ObjectReferenceIndex)  

## g_escapeStr
### 概要
- [譜面データにおける特殊文字の取り扱い](SpecialCharacters)で定義されている特殊文字を  
ブラウザで解釈できるエスケープ文字に変換したり、  
逆にエスケープ文字を元の文字に逆変換する二次元配列を管理するオブジェクト。

### 生成タイミング
- 初回起動時

### 補足
- replaceStr, escapeHtml, escapeHtmlForEnabledTag, unEscapeHtml関数で利用する。

### プロパティ
```javascript
const g_escapeStr = {
    escape: [[`&`, `&amp;`], [`<`, `&lt;`], [`>`, `&gt;`], [`"`, `&quot;`]],
    escapeTag: [
        [`*amp*`, `&amp;`], [`*pipe*`, `|`], [`*dollar*`, `$`], [`*rsquo*`, `&rsquo;`],
        [`*quot*`, `&quot;`], [`*comma*`, `&sbquo;`], [`*squo*`, `&#39;`], [`*bkquo*`, `&#96;`],
        [`*lt*`, `&lt;`], [`*gt*`, `&gt;`],
    ],
    unEscapeTag: [
        [`&amp;`, `&`], [`&rsquo;`, `’`], [`&quot;`, `"`], [`&sbquo;`, `,`],
        [`&lt;`, `<`], [`&gt;`, `>`], [`&#39;`, `'`], [`&#96;`, `\``],
    ],
    escapeCode: [
        [`<script>`, ``], [`</script>`, ``],
    ],
    musicNameSimple: [
        [`<br>`, ` `], [`<nbr>`, ``], [`<dbr>`, `　`],
    ],
    musicNameMultiLine: [
        [`<nbr>`, `<br>`], [`<dbr>`, `<br>`],
    ],
    frzName: [
        [`leftdia`, `frzLdia`], [`rightdia`, `frzRdia`],
        [`left`, `frzLeft`], [`down`, `frzDown`], [`up`, `frzUp`], [`right`, `frzRight`],
        [`space`, `frzSpace`], [`iyo`, `frzIyo`], [`gor`, `frzGor`], [`oni`, `foni`],
    ],
    keyCtrlName: [
        [`ShiftLeft`, `Shift`], [`ControlLeft`, `Control`], [`AltLeft`, `Alt`],
        [`Digit`, `D`], [`Numpad`, `N`], [`Semicolon`, `;`],
        [`Multiply`, `*`], [`Add`, `+`], [`Subtract`, `-`], [`Decimal`, `.`], [`Divide`, `Div`],
        [`Quote`, `Ja-Colon`], [`BracketLeft`, `Ja-@`], [`BracketRight`, `Ja-[`],
        [`Backslash`, `Ja-]`], [`Equal`, `Ja-^`],
    ],
};
```

### 関連項目
- [escapeHtml / escapeHtmlForEnabledTag](fnc-c0032-escapeHtml)
- [unEscapeHtml](fnc-c0033-unEscapeHtml)
