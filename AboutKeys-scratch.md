**[English](https://github.com/cwtickle/danoniplus-docs/wiki/AboutKeys-scratch) | Japanese** 

[<- Home](./) || [運指型](AboutKeys-fingering) | [バランス型](AboutKeys-balance) | [指移動型](AboutKeys-finger-movement) | [スクラッチ型](AboutKeys-scratch) | [オリジナル](AboutKeys-customKeys)

# 多鍵の種類（スクラッチ型）
- Enterなどスクラッチのようなキーがあるパターン。  
Dancing★Onigiriではすでにおにぎりがあるため、さらに＋αのキーがつくことで違った表現を出すことができます。

## 8key
[ 多鍵DB : [8key](https://apoi108.sakura.ne.jp/danoni/ta/?keys=8) ]

<img src="./wiki/keyimg/key8_01.png" width="40%">

<img src="./wiki/keys/key8.png" width="80%">

- 7key＋Enterが特徴の8key。  
Enterは小指で取れる位置にあり、7key右側（J K L）との同時押しのようなパターンも見られます。

## 11Wkey
[ 多鍵DB : [11Wkey](https://apoi108.sakura.ne.jp/danoni/ta/?keys=11W) ]

<img src="./wiki/keyimg/key11W_01.png" width="40%">

<img src="./wiki/keys/key11W.png" width="80%">

- 下段の7keyに対して、上段が大きく離れているのが特徴の11Wkey。  
Enter要素は無いですが、分類的に近い表現ができるのでスクラッチ型に分類しています。  
斜めの要素が加わることで、また一味違った構成が楽しめます。  

----

### Special Thanks
- 蒼宮あいす

----

[<- Home](./) || [運指型](AboutKeys-fingering) | [バランス型](AboutKeys-balance) | [指移動型](AboutKeys-finger-movement) | [スクラッチ型](AboutKeys-scratch) | [オリジナル](AboutKeys-customKeys)

|種類|対応キー数|
|----|----|
|[運指型](AboutKeys-fingering)|[5key](AboutKeys-fingering#5key) / [12key](AboutKeys-fingering#12key) / [14key](AboutKeys-fingering#14key) / [11ikey](AboutKeys-fingering#11ikey) / [17key](AboutKeys-fingering#17key) / [23key](AboutKeys-fingering#23key)|
|[バランス型](AboutKeys-balance)|[7key](AboutKeys-balance#7key) / [9Akey[ダブルプレイ]](AboutKeys-balance#9akey-ダブルプレイ) / [9Bkey](AboutKeys-balance#9bkey) / [7ikey](AboutKeys-balance#7ikey) / [9ikey](AboutKeys-balance#9ikey)|
|[指移動型](AboutKeys-finger-movement)|[11key](AboutKeys-finger-movement#11key) / [11Lkey](AboutKeys-finger-movement#11lkey) / [13key[トリプルプレイ]](AboutKeys-finger-movement#13key-トリプルプレイ) / [15Akey](AboutKeys-finger-movement#15akey) / [15Bkey](AboutKeys-finger-movement#15Bkey) / [14ikey](AboutKeys-finger-movement#14ikey) / [16ikey](AboutKeys-finger-movement#16ikey)|
|[スクラッチ型](AboutKeys-scratch)|[8key](AboutKeys-scratch#8key) / [11Wkey](AboutKeys-scratch#11wkey)|
|[オリジナル](AboutKeys-customKeys)|(Various Keys)|