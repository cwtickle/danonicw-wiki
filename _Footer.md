- Dancing Onigiri (CW Edition) Wikiのテキストは[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.ja)の下で利用可能です。
    - *Dancing Onigiri "CW Edition"* Wiki text available under [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.en).
- [Wiki 更新履歴 / History](_history)

&copy; 2018 [ティックル](https://github.com/cwtickle) &amp; [Contributors](https://github.com/cwtickle/danoniplus/blob/develop/CONTRIBUTORS.md).
