**[English](https://github.com/cwtickle/danoniplus-docs/wiki/AboutGameSystem-ex) | Japanese** 

# ゲーム画面の説明（拡張設定）

- 主要機能については [ゲーム画面の説明](./AboutGameSystem) をご覧ください。
- このページでは、v39から使用できる「Data Management」「Ex-Settings」について説明します。

## Data Management / データ管理

- 保存したデータの管理を行えます。

<img src="https://github.com/user-attachments/assets/bd454892-91ec-48b8-81d0-eda84316e543" width="70%">

|設定名|削除対象|削除項目|
|----|----|----|
|Environment|作品別|Adjustment, Volume, Appearance, Opacity, HitPosition, ColorType|
|Highscores|作品別|全譜面のハイスコア情報（譜面個別はSettings画面より）|
|CustomKey|作品別|作品側で定義しているカスタムキー情報（データ整理用。通常はKeyDataからの個別消去を推奨）|
|Others|作品別|上記以外のカスタム保存情報|
|（KeyData）|キー数別|Reverse, KeyCtrl, KeyCtrlPtn, Color, Shuffle, StepRtn|

- キー別の保存情報は矢印色情報を除き、消去できます。  
※個別の矢印色情報については、上書きが容易なことと、ColorTypeの参照先が無くなることでエラーになる可能性を考慮して消去対象外となっています。
- 消去後すぐの操作に限り、タブを閉じるまでひとつ前の保存内容を復元することができます。
「Restore」ボタンが表示されていれば、戻すことができます。
消去した内容によっては、既存の内容が上書きされる可能性に注意してください。

<img src="https://github.com/user-attachments/assets/25620418-fd7f-4ca0-9d42-31a6142d7bb0" width="90%">

### セーフモードについて

- ローカルストレージ情報が何らかの理由で壊れているかを検証するために、
ローカルストレージ情報を使わないセーフモードがあります。Data Management にて設定が可能です。

#### セーフモード中の挙動について

- ローカルストレージ情報を使わなくなります。作品別／キー別問わず適用されます。
- ローカルストレージ情報の保存が強制的にOFFになります。「Data Save」ボタンも押せません。
- Data Management からローカルストレージ情報の消去ができなくなります。参照は可能です。

<img src="https://github.com/user-attachments/assets/6b52dbe2-6eb4-4b07-8ed6-b7a0c912fa36" width="50%"><img src="https://github.com/user-attachments/assets/a3d70efb-a61d-4d6e-a02c-2ea3cac18d04" width="50%">


## Ex-Settings / 拡張設定

<img src="https://github.com/user-attachments/assets/e400e4e7-9208-4bd0-84f4-27d22be2d54d" width="70%">


### PlayWindow
- ステップゾーン及び矢印の位置を全体的に回転する設定です。

|種類<br>Paramater|内容<br>Description|
|----|----|
|Default|デフォルト。回転等の設定は行いません。|
|Stairs /<br>R-Stairs|ステップゾーン・矢印が階段状になるように表示します。|
|Slope /<br>R-Slope|ステップゾーン・矢印がStairs / R-Stairsより急になるように表示します。<br>※heightVariable（高さ可変設定）が有効の場合のみ指定できます。|
|Distorted /<br>R-Distorted|ステップゾーン・矢印が歪んだ状態で表示します。|
|SideScroll /<br>R-SideScroll|矢印が横スクロールで流れてきます。<br>※heightVariable（高さ可変設定）が有効の場合のみ指定できます。|

<img src="https://github.com/user-attachments/assets/48fd0ede-b0ca-493a-9c4c-9ce6e4d511e5" width="45%"><img src="https://github.com/user-attachments/assets/af340c0b-aa2b-4aee-adce-4e6ff8b62419" width="45%">

### StepArea
- ステップゾーンの位置を変更します。

|種類<br>Paramater|内容<br>Description|
|----|----|
|Default|デフォルト。何もしません。|
|Halfway|ステップゾーンが中央に並び、表示範囲が半分になります。<br>※上下スクロールがあるキーの場合は、<br>　強制的にステップゾーンがFlatに変わります。|
|2Step|スクロール方向が同一になり、片側が段違いで流れてきます。|
|Mismatched /<br>R-Mismatched|スクロールの向きが上下で異なる方向に流れます。|
|X-Flower|中央を境に左右から矢印が降ってきます。|

<img src="https://github.com/user-attachments/assets/6dd10f0d-d797-4549-89ad-7230e5b3cb3e" width="45%"><img src="https://github.com/user-attachments/assets/2acbb2a6-c22d-40fd-863c-78ef4ffc7d06" width="45%">

### FrzReturn
- フリーズアロー到達時もしくは矢印の回復判定が100の倍数に達するごとに、
X/Y/Z軸のいずれかに360度回転します。

|種類<br>Paramater|内容<br>Description|
|----|----|
|OFF|デフォルト。何もしません。|
|X-Axis|条件合致時にX軸回転します。|
|Y-Axis|条件合致時にY軸回転します。|
|Z-Axis|条件合致時にZ軸回転します。|
|Random|条件合致時にX軸/Y軸/Z軸のいずれかにランダムに回転します。|
|XY-Axis|条件合致時にX-Y軸回転します。|
|XZ-Axis|条件合致時にX-Z軸回転します。|
|XY-Axis|条件合致時にY-Z軸回転します。|
|Random+|条件合致時にX軸/Y軸/Z軸/X-Y軸/X-Z軸/Y-Z軸のいずれかにランダムに回転します。|

<img src="https://github.com/user-attachments/assets/5651a637-c88f-40af-a4a4-b6c43b579b86" width="45%"><img src="https://github.com/user-attachments/assets/53a6dc09-b643-48e8-aa69-b735ce417b10" width="45%">

### Shaking
- ステップゾーン及び矢印、画面全体を揺らす設定です。

|種類<br>Paramater|内容<br>Description|
|----|----|
|OFF|デフォルト。何もしません。|
|Horizontal|ステップゾーン及び矢印を左右に揺らします。|
|Vertical|ステップゾーン及び矢印を上下に揺らします。|
|Drunk|画面全体を上下左右にランダムに揺らします。|

<img src="https://github.com/user-attachments/assets/356b4eb6-41ba-43e9-a176-8c729c2c67cc" width="45%">

### Effect
- 矢印・フリーズアローにエフェクトをかけます。  
※矢印・フリーズアローモーションが設定されている場合は、設定してもOFFに戻されます。  
※この設定を有効にした場合、DisplayのArrowEffect設定が強制的にONに変わります。

|種類<br>Paramater|内容<br>Description|
|----|----|
|OFF|デフォルト。何もしません。|
|Dizzy|矢印とフリーズアローの矢印部分がZ軸回転します。|
|Spin|矢印とフリーズアローの矢印部分がY軸回転します。|
|Wave|矢印とフリーズアローが左右に揺れながら移動します。|
|Storm|矢印とフリーズアローが左右に激しく揺れ、回転しながら移動します。|
|Blinking|矢印とフリーズアローが点滅します。|
|Squids|矢印とフリーズアローが伸縮しながら移動します。|

<img src="https://github.com/user-attachments/assets/c19ea05b-8dd6-49fe-afd6-d1ec6302aeec" width="45%"><img src="https://github.com/user-attachments/assets/07773aa6-0938-4bcd-9adc-12956a88c5f0" width="45%">

### Camoufrage
- 矢印・フリーズアローの見た目が他の矢印・フリーズアローの色／形状に擬態します。

|種類<br>Paramater|内容<br>Description|
|----|----|
|OFF|デフォルト。何もしません。|
|Color|矢印とフリーズアローの色が他のものに擬態します。|
|Arrow|矢印とフリーズアローの矢印形状が他のものに擬態します。<br>※ImgTypeが「Note」の場合は自動で「OFF」になります。|
|ALL|矢印とフリーズアローの色・矢印形状の両方が他のものに擬態します。<br>※ImgTypeが「Note」の場合は自動で「Color」になります。|

<img src="https://github.com/user-attachments/assets/63c6092f-9f65-4c5d-9cf0-904256e83f9b" width="45%">

### Swapping
- ステップゾーンの位置をグループ単位で入れ替えます。

|種類<br>Paramater|内容<br>Description|
|----|----|
|OFF|デフォルト。何もしません。|
|Mirror|ステップゾーンの位置がグループ単位で反転して表示されます。|
|X-Mirror|ステップゾーンの位置の中央部のみグループ単位で反転して表示します。<br>5keyの場合、いわゆる「OldStyle」と同じになります。|

<img src="https://github.com/user-attachments/assets/ae701662-193e-4b2e-9728-5a1116502f3a" width="45%">

### JudgRange
- 判定の許容範囲を設定します。

|種類<br>Paramater|内容<br>Description|
|----|----|
|Normal|デフォルトの判定範囲です。|
|Narrow|シャキン、マターリの判定範囲がNormalより狭い判定範囲です。|
|Hard|Normalより全体的に厳しい判定範囲です。|
|ExHard|Hardより全体的に厳しい判定範囲です。|

### AutoRetry
- ミスやマターリを出したときに自動でリトライを行う設定です。

|種類<br>Paramater|内容<br>Description|
|----|----|
|OFF|デフォルト。何もしません。|
|Miss|ダメージ判定（ショボーン、ウワァン、イクナイ）が発生したときに<br>自動でリトライします。|
|Matari<br>(Good)|マターリ、ダメージ判定が発生したときに自動でリトライします。|
|Shakin<br>(Great)|シャキン、マターリ、ダメージ判定が発生したときに自動でリトライします。|
|Fast/Slow|シャキン、マターリ、ダメージ判定に加え、<br>Fast/Slowが発生したときに自動でリトライします。|
