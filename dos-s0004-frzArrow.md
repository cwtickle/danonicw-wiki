**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-s0004-frzArrow) | Japanese** 

[↑ 共通設定ファイル仕様に戻る](dos_setting)

[ <- ゲージ設定 ](dos-s0003-initialGauge) | **フリーズアロー設定** | [デフォルトデザイン・画像設定 -> ](dos-s0005-defaultDesign)

## フリーズアロー設定
### フリーズアローのデフォルト色セットの利用有無 (g_presetObj.frzColors)
⇒ 指定があった場合に優先される譜面ヘッダー：[defaultFrzColorUse](dos-h0063-defaultFrzColorUse)
- フリーズアローの矢印色が未指定の場合、色セットをどこから指定するかを設定します。
- `true`: デフォルト色セットから使用 / `false`: 矢印色(setColor)に合わせて変更 となっており、  
未指定の場合は`true`となります。
```javascript
g_presetObj.frzColors = true;
```

### 矢印色変化に対応してフリーズアロー色を追随する範囲の設定 (g_presetObj.frzScopeFromAC)
⇒ 指定があった場合に優先される譜面ヘッダー：[frzScopeFromAC](dos-h0088-frzScopeFromAC)
- `g_presetObj.frzColors`もしくは[defaultFrzColorUse](dos-h0063-defaultFrzColorUse)が`false`(矢印色優先) のときに使用できる設定です。
- 矢印色変化に対応してフリーズアロー色を追随する範囲を、「Normal（通常時）」「Hit（ヒット時）」で指定できます。  
未指定の場合はいずれにも追随しません。
```javascript
g_presetObj.frzScopeFromAC = [`Normal`]; // 通常時のみ追随
```

### フリーズアローの始点判定設定 (g_presetObj.frzStartjdgUse)
⇒ 指定があった場合に優先される譜面ヘッダー：[frzStartjdgUse](dos-h0037-frzStartjdgUse)
- フリーズアローの始点で通常矢印の判定を行うかを設定します。  
判定させる場合は `true` を指定します。
```javascript
g_presetObj.frzStartjdgUse = `false`;
```

## 更新履歴

|Version|変更内容|
|----|----|
|[v26.3.1](https://github.com/cwtickle/danoniplus/releases/tag/v26.3.1)|・変数名変更<br>　・g_presetFrzColors -> g_presetObj.frzColors<br>　・g_presetFrzScopeFromAC -> g_presetObj.frzScopeFromAC<br>　・g_presetFrzStartjdgUse -> g_presetObj.frzStartjdgUse|
|[v25.5.1](https://github.com/cwtickle/danoniplus/releases/tag/v25.5.1)|・矢印色変化に対応してフリーズアロー色を追随する範囲の設定(g_presetFrzScopeFromAC)を追加<br>・フリーズアローのデフォルト色セットの利用有無 (g_presetFrzColors)がfalse時の色追随設定を解除|
|[v25.1.0](https://github.com/cwtickle/danoniplus/releases/tag/v25.1.0)|・フリーズアローのデフォルト色セットの利用有無 (g_presetFrzColors)がfalse時、矢印色変化に対応してフリーズアロー色を追随するよう変更|
|[v13.3.1](https://github.com/cwtickle/danoniplus/releases/tag/v13.3.1)|・フリーズアローのデフォルト色セットの利用有無 (g_presetFrzColors)を実装|
|[v5.7.0](https://github.com/cwtickle/danoniplus/releases/tag/v5.7.0)|・フリーズアローの始点判定設定 (g_presetFrzStartjdgUse)を実装|

[ <- ゲージ設定 ](dos-s0003-initialGauge) | **フリーズアロー設定** | [デフォルトデザイン・画像設定 -> ](dos-s0005-defaultDesign)