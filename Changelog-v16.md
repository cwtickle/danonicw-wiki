⭐ New Features / 🛠️ Improvements / 🐞 Bug Fixes  
( Files changed: ⚪ danoni_main.js / 🔵 danoni_constants.js / 🔴 css files )

[**<- v17**](Changelog-v17) | **v16** | [**v15 ->**](Changelog-v15)  
(**🔖 [17 Releases](https://github.com/cwtickle/danoniplus/releases?q=tag%3Av16)** )

## [Warning] 更新終了後の不具合情報
- [更新を終了したバージョンの不具合情報#v16](DeprecatedVersionBugs#v16) を参照

## v16.4.10 ([2021-01-05](https://github.com/cwtickle/danoniplus/releases/tag/v16.4.10))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v16.4.10/total)](https://github.com/cwtickle/danoniplus/archive/v16.4.10.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v16.4.10/support/v16?style=social)](https://github.com/cwtickle/danoniplus/compare/v16.4.10...support/v16#files_bucket) 
- 🐞 同一レーン上に通常矢印とフリーズアローが短い間隔で存在するとき、
通常矢印の判定が優先されてしまう事象を修正 ( PR [#929](https://github.com/cwtickle/danoniplus/pull/929), [#930](https://github.com/cwtickle/danoniplus/pull/930), [#932](https://github.com/cwtickle/danoniplus/pull/932) ) <- :boom: **initial**

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v16.4.10)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v16.4.9...v16.4.10#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v16.4.10)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v16.4.10.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v16.4.10.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v16.4.10/js/danoni_main.js)
/ 🎣 [**v18.9.2**](./Changelog-v18#v1892-2021-01-05), [**v18.9.1**](./Changelog-v18#v1891-2021-01-03)

## v16.4.9 ([2020-12-30](https://github.com/cwtickle/danoniplus/releases/tag/v16.4.9))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v16.4.9/total)](https://github.com/cwtickle/danoniplus/archive/v16.4.9.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v16.4.9/support/v16?style=social)](https://github.com/cwtickle/danoniplus/compare/v16.4.9...support/v16#files_bucket) 
- 🐞 一部のキーを押すと全押し状態になる問題を修正 ( PR [#924](https://github.com/cwtickle/danoniplus/pull/924) ) <- :boom: [**v16.0.0**](Changelog-v16#v1600-2020-08-06)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v16.4.9)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v16.4.8...v16.4.9#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v16.4.9)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v16.4.9.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v16.4.9.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v16.4.9/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v16.4.9/js/lib/danoni_constants.js)
/ 🎣 [**v18.8.2**](./Changelog-v18#v1892-2020-12-30)

## v16.4.8 ([2020-12-25](https://github.com/cwtickle/danoniplus/releases/tag/v16.4.8))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v16.4.8/total)](https://github.com/cwtickle/danoniplus/archive/v16.4.8.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v16.4.8/support/v16?style=social)](https://github.com/cwtickle/danoniplus/compare/v16.4.8...support/v16#files_bucket) 
- 🐞 カスタムjs, スキンjsを二重読込している問題を修正 ( PR [#917](https://github.com/cwtickle/danoniplus/pull/917) ) <- :boom: [**v7.7.0**](Changelog-v7#v770-2019-08-25)
- 🐞 楽曲・画像読込チェックの待ち時間誤りを修正 ( PR [#915](https://github.com/cwtickle/danoniplus/pull/915) ) <- :boom: [**v10.1.1**](Changelog-v10#v1011-2019-11-11)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v16.4.8)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v16.4.7...v16.4.8#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v16.4.8)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v16.4.8.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v16.4.8.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v16.4.8/js/danoni_main.js)
/ 🎣 [**v18.7.0**](./Changelog-v18#v1870-2020-12-25)

## v16.4.7 ([2020-12-21](https://github.com/cwtickle/danoniplus/releases/tag/v16.4.7))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v16.4.7/total)](https://github.com/cwtickle/danoniplus/archive/v16.4.7.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v16.4.7/support/v16?style=social)](https://github.com/cwtickle/danoniplus/compare/v16.4.7...support/v16#files_bucket) 
- 🐞 曲終了直後にリトライキーを押すとALL0のリザルトが表示される問題を修正 ( PR [#908](https://github.com/cwtickle/danoniplus/pull/908) ) <- :boom: [**v0.67.x**](Changelog-v0#v067x-2018-11-17)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v16.4.7)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v16.4.6...v16.4.7#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v16.4.7)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v16.4.7.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v16.4.7.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v16.4.7/js/danoni_main.js)
/ 🎣 [**v18.6.0**](./Changelog-v18#v1860-2020-12-21)

## v16.4.6 ([2020-11-23](https://github.com/cwtickle/danoniplus/releases/tag/v16.4.6))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v16.4.6/total)](https://github.com/cwtickle/danoniplus/archive/v16.4.6.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v16.4.6/support/v16?style=social)](https://github.com/cwtickle/danoniplus/compare/v16.4.6...support/v16#files_bucket) 
- 🐞 Display:Colorのボタンが無効化されていた場合にColorTypeの挙動により、Display:Colorの切り替えができてしまう問題を修正 ( PR [#892](https://github.com/cwtickle/danoniplus/pull/892) ) <- :boom: [**v14.0.2**](Changelog-v14#v1402-2020-04-29) 

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v16.4.6)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v16.4.5...v16.4.6#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v16.4.6)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v16.4.6.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v16.4.6.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v16.4.6/js/danoni_main.js)
/ 🎣 [**v18.3.0**](./Changelog-v18#v1830-2020-11-18)

## v16.4.5 ([2020-11-02](https://github.com/cwtickle/danoniplus/releases/tag/v16.4.5))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v16.4.5/total)](https://github.com/cwtickle/danoniplus/archive/v16.4.5.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v16.4.5/support/v16?style=social)](https://github.com/cwtickle/danoniplus/compare/v16.4.5...support/v16#files_bucket) 
- 🐞 速度データ(speed_change)が空の場合、データがあると判断されてしまいspeed_changeが優先されてしまう問題を修正 ( PR [#884](https://github.com/cwtickle/danoniplus/pull/884) ) <- :boom: [**v8.0.0**](Changelog-v8#v800-2019-09-08) 

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v16.4.5)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v16.4.4...v16.4.5#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v16.4.5)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v16.4.5.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v16.4.5.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v16.4.5/js/danoni_main.js)
/ 🎣 [**v18.1.0**](./Changelog-v18#v1810-2020-11-02)

## v16.4.4 ([2020-10-16](https://github.com/cwtickle/danoniplus/releases/tag/v16.4.4))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v16.4.4/total)](https://github.com/cwtickle/danoniplus/archive/v16.4.4.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v16.4.4/support/v16?style=social)](https://github.com/cwtickle/danoniplus/compare/v16.4.4...support/v16#files_bucket) 
- 🐞 矢印塗りつぶし有、ステップゾーンをOFFにした場合に矢印塗りつぶし部分が残る問題を修正 ( PR [#866](https://github.com/cwtickle/danoniplus/pull/866) ) <- :boom: [**v5.12.0**](Changelog-v5#v5120-2019-06-14)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v16.4.4)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v16.4.3...v16.4.4#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v16.4.4)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v16.4.4.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v16.4.4.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v16.4.4/js/danoni_main.js)
/ 🎣 [**v17.4.3**](./Changelog-v17#v1743-2020-10-12)

## v16.4.3 ([2020-10-09](https://github.com/cwtickle/danoniplus/releases/tag/v16.4.3))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v16.4.3/total)](https://github.com/cwtickle/danoniplus/archive/v16.4.3.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v16.4.3/support/v16?style=social)](https://github.com/cwtickle/danoniplus/compare/v16.4.3...support/v16#files_bucket) 
- 🛠️ danoni_main.jsに対してstrictモードを解除
- 🐞 tuningのみ指定があった場合に制作者リンクが出ない問題を修正 ( PR [#850](https://github.com/cwtickle/danoniplus/pull/850) ) <- :boom: [**v3.0.0**](Changelog-v3#v300-2019-02-25)
- 🐞 ツール値出力時、時間表示がおかしくなることがある問題を修正 ( PR [#858](https://github.com/cwtickle/danoniplus/pull/858) ) <- :boom: [**v12.0.0**](Changelog-v12#v1200-2020-02-09) <- [**v2.0.0**](Changelog-v2#v200-2019-01-18)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v16.4.3)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v16.4.1...v16.4.3#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v16.4.3)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v16.4.3.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v16.4.3.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v16.4.3/js/danoni_main.js)
/ 🎣 [**v17.4.0**](./Changelog-v17#v1740-2020-10-09)

## v16.4.1 ([2020-09-27](https://github.com/cwtickle/danoniplus/releases/tag/v16.4.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v16.4.1/total)](https://github.com/cwtickle/danoniplus/archive/v16.4.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v16.4.1/support/v16?style=social)](https://github.com/cwtickle/danoniplus/compare/v16.4.1...support/v16#files_bucket) 
- 🐞 user-selectがSafariブラウザのみ動作しない問題を修正 ( PR [#832](https://github.com/cwtickle/danoniplus/pull/832) ) <- :boom: [**v16.4.0**](Changelog-v16#v1640-2020-09-22)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v16.4.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v16.4.0...v16.4.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v16.4.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v16.4.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v16.4.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v16.4.1/js/danoni_main.js)
/ 🎣 [**v17.0.1**](./Changelog-v17#v1701-2020-09-27)

----

## v16.4.0 ([2020-09-22](https://github.com/cwtickle/danoniplus/releases/tag/v16.4.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v16.4.0/total)](https://github.com/cwtickle/danoniplus/archive/v16.4.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v16.4.0/support/v16?style=social)](https://github.com/cwtickle/danoniplus/compare/v16.4.0...support/v16#files_bucket) 
- ⭐ 結果画面のCLEARED / FAILED表示に対する遅延フレーム設定を実装 ( Issue [#824](https://github.com/cwtickle/danoniplus/pull/824), PR [#828](https://github.com/cwtickle/danoniplus/pull/828) )
- 🛠️ 結果画面周りのコード整理 ( PR [#825](https://github.com/cwtickle/danoniplus/pull/825), [#829](https://github.com/cwtickle/danoniplus/pull/829), [#830](https://github.com/cwtickle/danoniplus/pull/830) )
- 🛠️ 設定画面（フェードイン）のコード整理 ( PR [#826](https://github.com/cwtickle/danoniplus/pull/826) )
- 🛠️ 非推奨関数を削除 ( PR [#827](https://github.com/cwtickle/danoniplus/pull/827), [#829](https://github.com/cwtickle/danoniplus/pull/829) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v16.4.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v16.3.0...v16.4.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v16.4.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v16.4.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v16.4.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v16.4.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v16.4.0/js/lib/danoni_constants.js)<br>❤️ goe (@goe0)

## v16.3.0 ([2020-09-05](https://github.com/cwtickle/danoniplus/releases/tag/v16.3.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v16.3.0/total)](https://github.com/cwtickle/danoniplus/archive/v16.3.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v16.3.0/support/v16?style=social)](https://github.com/cwtickle/danoniplus/compare/v16.3.0...support/v16#files_bucket) 
- ⭐ Ready表示のアニメーション変更、先頭色変更に対応 ( PR [#820](https://github.com/cwtickle/danoniplus/pull/820) )
- 🛠️ Ready表示の遅延フレーム設定について、
プレイ中のフェードイン有効時は無効にするよう変更 ( Issue [#819](https://github.com/cwtickle/danoniplus/pull/819), PR [#820](https://github.com/cwtickle/danoniplus/pull/820) )
- 🛠️ Ready表示部分をカスタム関数の前に生成するよう変更 ( PR [#820](https://github.com/cwtickle/danoniplus/pull/820) )
- 🛠️ リザルトデータについてフリーズアローが無い場合、表記を略すように変更 ( Issue [#817](https://github.com/cwtickle/danoniplus/pull/817), PR [#818](https://github.com/cwtickle/danoniplus/pull/818) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v16.3.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v16.2.1...v16.3.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v16.3.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v16.3.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v16.3.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v16.3.0/js/danoni_main.js)<br>❤️ izkdic

## v16.2.1 ([2020-08-25](https://github.com/cwtickle/danoniplus/releases/tag/v16.2.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v16.2.1/total)](https://github.com/cwtickle/danoniplus/archive/v16.2.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v16.2.1/support/v16?style=social)](https://github.com/cwtickle/danoniplus/compare/v16.2.1...support/v16#files_bucket) 
- ⭐ 20個以上のキーに対する色変化（単発矢印・個別）に対応 ( Issue [#807](https://github.com/cwtickle/danoniplus/pull/807), PR [#812](https://github.com/cwtickle/danoniplus/pull/812) )
- ⭐ リザルトデータのフォーマット機能を実装 ( Issue [#809](https://github.com/cwtickle/danoniplus/pull/809), PR [#813](https://github.com/cwtickle/danoniplus/pull/813) )
- 🐞 リトライ時に稀に早回しになる不具合を修正、キーリピートを無効化 ( PR [#810](https://github.com/cwtickle/danoniplus/pull/810) ) <- :boom: [**v4.0.0**](Changelog-v4#v400-2019-04-25)
- 🐞 キー変換処理が抜けていた部分を修正 ( PR [#811](https://github.com/cwtickle/danoniplus/pull/811) ) <- :boom: [**v11.2.0**](Changelog-v11#v1120-2020-01-04)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v16.2.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v16.1.0...v16.2.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v16.2.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v16.2.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v16.2.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v16.2.1/js/danoni_main.js)<br>❤️ すずめ (@suzme)

## v16.1.0 ([2020-08-22](https://github.com/cwtickle/danoniplus/releases/tag/v16.1.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v16.1.0/total)](https://github.com/cwtickle/danoniplus/archive/v16.1.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v16.1.0/support/v16?style=social)](https://github.com/cwtickle/danoniplus/compare/v16.1.0...support/v16#files_bucket) 
- ⭐ リザルトデータのクリップボードコピー機能を実装 ( Issue [#803](https://github.com/cwtickle/danoniplus/pull/803), PR [#804](https://github.com/cwtickle/danoniplus/pull/804) )
- 🛠️ CapsLockキーの無効化設定及び無効キーのメッセージを追加 ( Gitter [2020-08-17](https://gitter.im/danonicw/community?at=5f3a8e4a60892e0c69794df8), Issue [#805](https://github.com/cwtickle/danoniplus/pull/805), PR [#806](https://github.com/cwtickle/danoniplus/pull/806) )
- 🛠️ Applicationキーがキーコンフィグ画面、メイン画面で反応しないよう変更 ( PR [#806](https://github.com/cwtickle/danoniplus/pull/806) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v16.1.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v16.0.4...v16.1.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v16.1.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v16.1.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v16.1.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v16.1.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v16.1.0/js/lib/danoni_constants.js)<br>❤️ すずめ (@suzme)

<img src="./wiki/changelog/v16_0001.png" alt="v16_0001" width="50%">

<img src="./wiki/changelog/v16_0002.png" alt="v16_0002" width="50%">

## v16.0.4 ([2020-08-14](https://github.com/cwtickle/danoniplus/releases/tag/v16.0.4))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v16.0.4/total)](https://github.com/cwtickle/danoniplus/archive/v16.0.4.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v16.0.4/support/v16?style=social)](https://github.com/cwtickle/danoniplus/compare/v16.0.4...support/v16#files_bucket) 
- 🐞 テンキー一式、atmarkキーが反応しない問題を修正 ( PR [#799](https://github.com/cwtickle/danoniplus/pull/799), [#801](https://github.com/cwtickle/danoniplus/pull/801) ) <- :boom: [**v16.0.0**](Changelog-v16#v1600-2020-08-06)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v16.0.4)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v16.0.2...v16.0.4#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v16.0.4)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v16.0.4.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v16.0.4.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v16.0.4/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v16.0.4/js/lib/danoni_constants.js)<br>❤️ izkdic, goe (@goe0), すずめ (@suzme)

## v16.0.2 ([2020-08-09](https://github.com/cwtickle/danoniplus/releases/tag/v16.0.2))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v16.0.2/total)](https://github.com/cwtickle/danoniplus/archive/v16.0.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v16.0.2/support/v16?style=social)](https://github.com/cwtickle/danoniplus/compare/v16.0.2...support/v16#files_bucket) 
- 🐞 セミコロンキーが反応しない問題を修正 ( Issue [#796](https://github.com/cwtickle/danoniplus/pull/796), PR [#797](https://github.com/cwtickle/danoniplus/pull/797) ) <- :boom: [**v16.0.0**](Changelog-v16#v1600-2020-08-06)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v16.0.2)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v16.0.1...v16.0.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v16.0.2)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v16.0.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v16.0.2.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v16.0.2/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v16.0.2/js/lib/danoni_constants.js)

## v16.0.1 ([2020-08-07](https://github.com/cwtickle/danoniplus/releases/tag/v16.0.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v16.0.1/total)](https://github.com/cwtickle/danoniplus/archive/v16.0.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v16.0.1/support/v16?style=social)](https://github.com/cwtickle/danoniplus/compare/v16.0.1...support/v16#files_bucket) 
- 🐞 Shift+Deleteキーの組み合わせが利かない問題を修正 ( Issue [#792](https://github.com/cwtickle/danoniplus/pull/792), PR [#793](https://github.com/cwtickle/danoniplus/pull/793) ) <- :boom: [**v16.0.0**](Changelog-v16#v1600-2020-08-06)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v16.0.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v16.0.0...v16.0.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v16.0.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v16.0.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v16.0.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v16.0.1/js/danoni_main.js)

## v16.0.0 ([2020-08-06](https://github.com/cwtickle/danoniplus/releases/tag/v16.0.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v16.0.0/total)](https://github.com/cwtickle/danoniplus/archive/v16.0.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v16.0.0/support/v16?style=social)](https://github.com/cwtickle/danoniplus/compare/v16.0.0...support/v16#files_bucket) 
- ⭐ プレイ画面範囲の横幅・位置調整機能を追加 ( Issue [#789](https://github.com/cwtickle/danoniplus/pull/789), PR [#790](https://github.com/cwtickle/danoniplus/pull/790) )
- 🛠️ `KeyboardEvent.keyCode`を`KeyboardEvent.code`へ置き換え ( Issue [#737](https://github.com/cwtickle/danoniplus/pull/737), PR [#783](https://github.com/cwtickle/danoniplus/pull/783) )
- 🛠️ ライフ初期値を小数第2位まで内部的に持つよう変更 ( Issue [#787](https://github.com/cwtickle/danoniplus/pull/787), PR [#788](https://github.com/cwtickle/danoniplus/pull/788) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v16.0.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v15.7.0...v16.0.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v16.0.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v16.0.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v16.0.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v16.0.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v16.0.0/js/lib/danoni_constants.js)

[**<- v17**](Changelog-v17) | **v16** | [**v15 ->**](Changelog-v15)
