**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-s0010-customKeys) | Japanese** 

[↑ 共通設定ファイル仕様に戻る](dos_setting)

[ <- ラベルテキスト・メッセージ ](dos-s0009-labelUpdate) | **カスタムキー定義** || [制作者クレジット・基本設定 -> ](dos-s0001-makerInfo)

## カスタムキー定義
### 共通カスタムキー定義 (g_presetObj.keysData)
⇒ 指定があった場合に優先される譜面ヘッダー：[作品別カスタムキー定義](keys)
- 共通で設定するカスタムキー定義を設定します。

```javascript
g_presetObj.keysData = `

|color6=0,1,0,1,0,2$2,0,1,0,1,0|
|chara6=left,leftdia,down,rightdia,right,space$space,left,leftdia,down,rightdia,right|
|div6=6$6|
|stepRtn6=0,45,-90,135,180,onigiri$onigiri,0,45,-90,135,180|
|keyCtrl6=75/0,79/0,76/0,80/0,187/0,32/0$32/0,75/0,79/0,76/0,80/0,187/0|
|shuffle6=1,1,1,1,1,0$0,1,1,1,1,1|

|chara9v=9B_0$9B_0|
|color9v=1,0,1,0,2,0,1,0,1$9B_0|
|div9v=9$9|
|keyCtrl9v=52/0,82/0,70/0,86/0,32/0,78/0,74/0,73/0,57/0$9B_0|
|pos9v=0,1,2,3,4,5,6,7,8$0,1,2,3,4,5,6,7,8|
|scroll9v=---::1,1,-1,-1,-1,-1,-1,1,1/flat::1,1,1,1,1,1,1,1,1$9B_0|
|shuffle9v=9B_0$9B_0|
|stepRtn9v=90,120,150,180,onigiri,0,30,60,90$9B_0|
|transKey9v=$9B|

`;
```

### ライブラリ用カスタムキー定義 (g_presetObj.keysDataLib)
- ライブラリ用で設定するカスタムキー定義を設定します。  
`g_presetObj.keysDataLib`は配列で定義されているため、  
カスタム定義方法は同じですがカスタム定義自体を`push`する必要があります。
```javascript
const customLibData = `

|keyExtraList=27k|
|chara27k=...|
|color27k=...|

`;
g_presetObj.keysDataLib.push(customLibData);
```

## 更新履歴

|Version|変更内容|
|----|----|
|[v31.3.1](https://github.com/cwtickle/danoniplus/releases/tag/v31.3.1)|・カスタムキーリスト(keyExtraList)の指定を原則不要化|
|[v30.6.0](https://github.com/cwtickle/danoniplus/releases/tag/v30.6.0)|・ライブラリ用カスタムキー定義 (g_presetObj.keysDataLib)を追加|
|[v26.3.1](https://github.com/cwtickle/danoniplus/releases/tag/v26.3.1)|・変数名変更<br>　・g_presetKeysData -> g_presetObj.keysData|
|[v26.2.0](https://github.com/cwtickle/danoniplus/releases/tag/v26.2.0)|・共通カスタムキー定義(g_presetKeysData)を追加|

[ <- ラベルテキスト・メッセージ ](dos-s0009-labelUpdate) | **カスタムキー定義** || [制作者クレジット・基本設定 -> ](dos-s0001-makerInfo)