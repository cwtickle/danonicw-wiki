**[English](https://github.com/cwtickle/danoniplus-docs/wiki/Changelog-v23) | Japanese** 

[^ Back to All Version List (Security Policy)](https://github.com/cwtickle/danoniplus/security/policy)  
[^ Back to Update Info](./UpdateInfo#-v23-changelog)

[**<- v24**](Changelog-v24) | **v23** | [**v22 ->**](Changelog-v22)  
(**🔖 [15 Releases](https://github.com/cwtickle/danoniplus/releases?q=tag%3Av23)** )

## 🔃 Files changed (v23)

|Directory|FileName||Last Updated|
|----|----|----|----|
|/js|danoni_main.js|[📥](https://github.com/cwtickle/danoniplus/releases/download/v23.5.6/danoni_main.js)|**v23.5.6**|
|/js/lib|danoni_constants.js|[📥](https://github.com/cwtickle/danoniplus/releases/download/v23.5.5/danoni_constants.js)|[v23.5.5](Changelog-v23#v2355-2022-01-26)|

<details>
<summary>Changed file list before v22</summary>

|Directory|FileName||Last Updated|
|----|----|----|----|
|/js/lib|danoni_localbinary.js|[📥](https://github.com/cwtickle/danoniplus/releases/download/v15.1.0/danoni_localbinary.js)|[v15.1.0](./Changelog-v15#v1510-2020-05-21)|
|/css|danoni_main.css|[📥](https://github.com/cwtickle/danoniplus/releases/download/v21.4.2/danoni_main.css)|[v21.4.2](Changelog-v21#v2142-2021-04-07)|
|/img|aaShadow.svg<br>arrow.svg<br>arrowShadow.svg<br>borderline.svg<br>c.svg<br>cursor.svg<br>giko.svg<br>iyo.svg<br>monar.svg<br>morara.svg<br>onigiri.svg|[📥](https://github.com/cwtickle/danoniplus/releases/download/v15.1.0/img.zip)|[v15.1.0](./Changelog-v15#v1510-2020-05-21)|
|/skin|danoni_skin_default.css<br>danoni_skin_light.css<br>danoni_skin_skyblue.css|[📥](https://github.com/cwtickle/danoniplus/releases/download/v21.4.2/danoni_skin_default.css)<br>[📥](https://github.com/cwtickle/danoniplus/releases/download/v21.4.2/danoni_skin_light.css)<br>[📥](https://github.com/cwtickle/danoniplus/releases/download/v21.4.2/danoni_skin_skyblue.css)|[v21.4.2](Changelog-v21#v2142-2021-04-07)|

</details>

⭐ New Features / 🛠️ Improvements / 🐞 Bug Fixes  
( Files changed: ⚪ danoni_main.js / 🔵 danoni_constants.js / 🔴 css files )

## [Warning] 更新終了後の不具合情報
- [更新を終了したバージョンの不具合情報#v23](DeprecatedVersionBugs#v23) を参照

## v23.5.6 ([2022-01-29](https://github.com/cwtickle/danoniplus/releases/tag/v23.5.6))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v23.5.6/total)](https://github.com/cwtickle/danoniplus/archive/v23.5.6.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v23.5.6/support/v23?style=social)](https://github.com/cwtickle/danoniplus/compare/v23.5.6...support/v23#files_bucket) 
- 🐞 シャッフル・カラーグループを変更した際にその内容が反映されない問題を修正 ( PR [#1215](https://github.com/cwtickle/danoniplus/pull/1215), Gitter [2022-01-29](https://gitter.im/danonicw/community?at=61f42696bfe2f54b2e3f0422) ) <- :boom: [**v23.5.4**](Changelog-v23#v2354-2021-12-13)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v23.5.6)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v23.5.5...v23.5.6#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v23.5.6)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v23.5.6.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v23.5.6.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v23.5.6/js/danoni_main.js)
/ 🎣 [**v25.5.2**](./Changelog-v25#v2552-2022-01-29)

## v23.5.5 ([2022-01-26](https://github.com/cwtickle/danoniplus/releases/tag/v23.5.5))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v23.5.5/total)](https://github.com/cwtickle/danoniplus/archive/v23.5.5.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v23.5.5/support/v23?style=social)](https://github.com/cwtickle/danoniplus/compare/v23.5.5...support/v23#files_bucket) 
- 🛠️ 二重定義箇所、定義コメントの誤りを修正 ( PR [#1209](https://github.com/cwtickle/danoniplus/pull/1209) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v23.5.5)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v23.5.4...v23.5.5#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v23.5.5)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v23.5.5.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v23.5.5.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v23.5.5/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v23.5.5/js/lib/danoni_constants.js)
/ 🎣 [**v25.5.1**](./Changelog-v25#v2551-2022-01-26)

## v23.5.4 ([2021-12-13](https://github.com/cwtickle/danoniplus/releases/tag/v23.5.4))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v23.5.4/total)](https://github.com/cwtickle/danoniplus/archive/v23.5.4.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v23.5.4/support/v23?style=social)](https://github.com/cwtickle/danoniplus/compare/v23.5.4...support/v23#files_bucket) 
- 🛠️ シャッフルグループ・カラーグループ周りの処理を見直し ( PR [#1176](https://github.com/cwtickle/danoniplus/pull/1176), [#1177](https://github.com/cwtickle/danoniplus/pull/1177), [#1178](https://github.com/cwtickle/danoniplus/pull/1178) )
- 🐞 9Akeyでキーパターン移動中に9Bkeyを通過すると9Akey(Pattern: 1)の配色がおかしくなる問題を修正 ( PR [#1174](https://github.com/cwtickle/danoniplus/pull/1174), [#1175](https://github.com/cwtickle/danoniplus/pull/1175) ), Gitter [2021-12-12](https://gitter.im/danonicw/community?at=61b5b044abdd6644e3d30f14) ) <- :boom: [**v22.4.1**](Changelog-v22#v2241-2021-05-16)
- 🐞 キーコン画面で矢印色を一時的に変更した際、他のKeyPatternにも色変更が反映されることがある問題を修正 ( PR [#1174](https://github.com/cwtickle/danoniplus/pull/1174) ) <- :boom: [**v22.4.1**](Changelog-v22#v2241-2021-05-16)
- 🐞 musicUrlが未指定の場合にタイトル画面で止まることがある問題を修正 ( PR [#1151](https://github.com/cwtickle/danoniplus/pull/1151) ) 
<- :boom: [**v23.0.0**](Changelog-v23#v2300-2021-09-04)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v23.5.4)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v23.5.2...v23.5.4#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v23.5.4)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v23.5.4.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v23.5.4.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v23.5.4/js/danoni_main.js)
/ 🎣 [**v24.5.1**](./Changelog-v24#v2451-2021-12-13)

## v23.5.2 ([2021-10-27](https://github.com/cwtickle/danoniplus/releases/tag/v23.5.2))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v23.5.2/total)](https://github.com/cwtickle/danoniplus/archive/v23.5.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v23.5.2/support/v23?style=social)](https://github.com/cwtickle/danoniplus/compare/v23.5.2...support/v23#files_bucket) 
- 🐞 Reverse時のarrowMotionDataが正しく反映されない問題を修正 ( PR [#1146](https://github.com/cwtickle/danoniplus/pull/1146) ) <- :boom: [**v23.4.1**](Changelog-v23#v2341-2021-10-04)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v23.5.2)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v23.5.1...v23.5.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v23.5.2)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v23.5.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v23.5.2.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v23.5.2/js/danoni_main.js)
/ 🎣 [**v24.0.1**](./Changelog-v24#v2401-2021-10-27)

---

## v23.5.1 ([2021-10-16](https://github.com/cwtickle/danoniplus/releases/tag/v23.5.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v23.5.1/total)](https://github.com/cwtickle/danoniplus/archive/v23.5.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v23.5.1/support/v23?style=social)](https://github.com/cwtickle/danoniplus/compare/v23.5.1...support/v23#files_bucket) 
- 🐞 保存したキーコンフィグが無いときに設定画面で止まることがある問題を修正 ( PR [#1139](https://github.com/cwtickle/danoniplus/pull/1139), Gitter [2021-10-16](https://gitter.im/danonicw/community?at=616a7890fb8ca0572bc5147f) ) <- :boom: [**v23.5.0**](Changelog-v23#v2350-2021-10-14)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v23.5.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v23.5.0...v23.5.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v23.5.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v23.5.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v23.5.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v23.5.1/js/danoni_main.js)<br>❤️ aconite

## v23.5.0 ([2021-10-14](https://github.com/cwtickle/danoniplus/releases/tag/v23.5.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v23.5.0/total)](https://github.com/cwtickle/danoniplus/archive/v23.5.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v23.5.0/support/v23?style=social)](https://github.com/cwtickle/danoniplus/compare/v23.5.0...support/v23#files_bucket) 
- ⭐ 譜面変更時、キーが同一の場合にReverse/Scroll/AutoPlay/キーパターンを引き継ぐよう変更 ( PR [#1136](https://github.com/cwtickle/danoniplus/pull/1136) )
- 🛠️ 曲中リトライキーの猶予フレームを廃止 ( PR [#1137](https://github.com/cwtickle/danoniplus/pull/1137), Gitter [2021-10-14](https://gitter.im/danonicw/community?at=6167f968fb8ca0572bbf48d6) )
- 🐞 Display, キーコンフィグ画面から直接開始した場合、プレイ後キーコンフィグ画面に戻るとキーパターンがデータセーブ有効時、Selfにならない問題を修正 ( PR [#1136](https://github.com/cwtickle/danoniplus/pull/1136) ) <- :boom: [**v4.6.0**](Changelog-v4#v460-2019-05-03)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v23.5.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v23.4.1...v23.5.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v23.5.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v23.5.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v23.5.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v23.5.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v23.5.0/js/lib/danoni_constants.js)<br>❤️ goe (@goe0)

## v23.4.1 ([2021-10-04](https://github.com/cwtickle/danoniplus/releases/tag/v23.4.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v23.4.1/total)](https://github.com/cwtickle/danoniplus/archive/v23.4.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v23.4.1/support/v23?style=social)](https://github.com/cwtickle/danoniplus/compare/v23.4.1...support/v23#files_bucket) 
- 🐞 矢印データが昇順で無いときに一部矢印が表示されないことがある問題を修正 ( PR [#1134](https://github.com/cwtickle/danoniplus/pull/1134), Gitter [2021-10-03](https://gitter.im/danonicw/community?at=61595d062197144e84443743) ) <- :boom: **initial**

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v23.4.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v23.4.0...v23.4.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v23.4.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v23.4.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v23.4.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v23.4.1/js/danoni_main.js)<br>❤️ izkdic

## v23.4.0 ([2021-10-02](https://github.com/cwtickle/danoniplus/releases/tag/v23.4.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v23.4.0/total)](https://github.com/cwtickle/danoniplus/archive/v23.4.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v23.4.0/support/v23?style=social)](https://github.com/cwtickle/danoniplus/compare/v23.4.0...support/v23#files_bucket) 
- ⭐ ランクAPの実装 ( PR [#1123](https://github.com/cwtickle/danoniplus/pull/1123) )
- ⭐ プレイ画面に譜面名を表示、それに合わせレイアウトを見直し ( PR [#1124](https://github.com/cwtickle/danoniplus/pull/1124), [#1126](https://github.com/cwtickle/danoniplus/pull/1126), Gitter [2021-09-24](https://gitter.im/danonicw/community?at=614d9e3363dca81891792046) )
- ⭐ キーコンフィグ画面のプレイボタンを追加、レイアウト修正 ( PR [#1125](https://github.com/cwtickle/danoniplus/pull/1125), Gitter [2021-09-26](https://gitter.im/danonicw/community?at=614fe93163dca818917dfe46) )
- ⭐ カスタムキーの名前を変更できる機能を追加 ( PR [#1128](https://github.com/cwtickle/danoniplus/pull/1128) )
- 🛠️ 曲名が長い場合にプレイ画面のクレジットフォントサイズを自動調整するよう変更 ( PR [#1124](https://github.com/cwtickle/danoniplus/pull/1124) )
- 🛠️ タイトル画面のクレジットフォントサイズを自動調整するよう変更 ( PR [#1124](https://github.com/cwtickle/danoniplus/pull/1124) )
- 🛠️ サンプルhtmlの画像リンク間違いを修正 ( PR [#1129](https://github.com/cwtickle/danoniplus/pull/1129) )
- 🛠️ コントリビューター (CONTRIBUTORS.md) の追加 ( PR [#1130](https://github.com/cwtickle/danoniplus/pull/1130) )
- 🛠️ 歌詞表示 (word_data) の初期色をCSSに任せる設定に変更 ( PR [#1131](https://github.com/cwtickle/danoniplus/pull/1131) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v23.4.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v23.3.1...v23.4.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v23.4.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v23.4.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v23.4.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v23.4.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v23.4.0/js/lib/danoni_constants.js)<br>❤️ goe (@goe0), SKB (@superkuppabros), izkdic, aconite

<img src="./wiki/changelog/v23_0007.png" alt="v23_0007" width="80%">

<img src="./wiki/changelog/v23_0008.png" alt="v23_0008" width="60%">

## v23.3.1 ([2021-09-24](https://github.com/cwtickle/danoniplus/releases/tag/v23.3.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v23.3.1/total)](https://github.com/cwtickle/danoniplus/archive/v23.3.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v23.3.1/support/v23?style=social)](https://github.com/cwtickle/danoniplus/compare/v23.3.1...support/v23#files_bucket) 
- 🛠️ 設定画面のショートカットキーについて、ReverseをRキー、Scrollを上下キーに変更 ( PR [#1120](https://github.com/cwtickle/danoniplus/pull/1120), [#1121](https://github.com/cwtickle/danoniplus/pull/1121) )
- 🐞 Reverseのショートカットキー(Rキー)について、拡張スクロールがある場合でもバックグラウンドで動いてしまう問題を修正 ( PR [#1120](https://github.com/cwtickle/danoniplus/pull/1120) ) <- :boom: [**v19.5.0**](Changelog-v19#v1950-2021-02-09)
- 🐞 Scroll無効時、Reverseの左キーとラベルボタンの一部が押せない問題を修正 ( PR [#1120](https://github.com/cwtickle/danoniplus/pull/1120) ) <- :boom: [**v10.2.1**](Changelog-v10#v1021-2019-11-20)
- 🐞 Scroll有効かつ9ikeyや17keyのような拡張スクロールが無い場合、Reverseの左右キーが押せない問題を修正 ( PR [#1120](https://github.com/cwtickle/danoniplus/pull/1120) ) <- :boom: [**v23.0.0**](Changelog-v23#v2300-2021-09-04)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v23.3.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v23.3.0...v23.3.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v23.3.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v23.3.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v23.3.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v23.3.1/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v23.3.1/js/lib/danoni_constants.js)

## v23.3.0 ([2021-09-23](https://github.com/cwtickle/danoniplus/releases/tag/v23.3.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v23.3.0/total)](https://github.com/cwtickle/danoniplus/archive/v23.3.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v23.3.0/support/v23?style=social)](https://github.com/cwtickle/danoniplus/compare/v23.3.0...support/v23#files_bucket) 
- ⭐ 12, 14keyにキーボードの左側を使ったキーパターンを追加 ( PR [#1117](https://github.com/cwtickle/danoniplus/pull/1117), Gitter [2021-09-23](https://gitter.im/danonicw/community?at=614b507467e789189c115da3) )
- 🛠️ 設定画面のAdjustmentのオンマウス説明文の修正、コード修正 ( PR [#1116](https://github.com/cwtickle/danoniplus/pull/1116) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v23.3.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v23.2.0...v23.3.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v23.3.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v23.3.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v23.3.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v23.3.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v23.3.0/js/lib/danoni_constants.js)<br>❤️ aconite

<img src="./wiki/changelog/v23_0005.png" alt="v23_0005" width="80%">

## v23.2.0 ([2021-09-20](https://github.com/cwtickle/danoniplus/releases/tag/v23.2.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v23.2.0/total)](https://github.com/cwtickle/danoniplus/archive/v23.2.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v23.2.0/support/v23?style=social)](https://github.com/cwtickle/danoniplus/compare/v23.2.0...support/v23#files_bucket) 
- 🛠️ 設定画面のAdjustmentボタン・ショートカットキー仕様変更 ( PR [#1114](https://github.com/cwtickle/danoniplus/pull/1114), Gitter [2021-09-20](https://gitter.im/danonicw/community?at=6147dde6f428f97a9f95ef24) )
- 🛠️ 3つ同時押しのショートカットキーに対応 ( PR [#1114](https://github.com/cwtickle/danoniplus/pull/1114) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v23.2.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v23.1.1...v23.2.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v23.2.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v23.2.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v23.2.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v23.2.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v23.2.0/js/lib/danoni_constants.js)<br>❤️ aconite, izkdic, apoi, FUJI, ぷろろーぐ (@prlg25), すずめ (@suzme)

<img src="./wiki/changelog/v23_0004.png" alt="v23_0004" width="80%">

## v23.1.1 ([2021-09-17](https://github.com/cwtickle/danoniplus/releases/tag/v23.1.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v23.1.1/total)](https://github.com/cwtickle/danoniplus/archive/v23.1.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v23.1.1/support/v23?style=social)](https://github.com/cwtickle/danoniplus/compare/v23.1.1...support/v23#files_bucket) 
- 🛠️ Motion: Boost時の軌道計算方法を見直し ( PR [#1111](https://github.com/cwtickle/danoniplus/pull/1111) )
- 🐞 フリーズアローを押し直したとき終端の位置がずれる問題を修正 ( PR [#1110](https://github.com/cwtickle/danoniplus/pull/1110) ) <- :boom: **initial**
- 🐞 フリーズアローを離したとき全体の位置がずれる問題を修正 ( PR [#1110](https://github.com/cwtickle/danoniplus/pull/1110) ) <- :boom: **initial**
- 🐞 Motion: Boost, Brake時、早めにフリーズアローを押し始めると終端の位置がずれる問題を修正 ( PR [#1110](https://github.com/cwtickle/danoniplus/pull/1110) ) <- :boom: **initial**
- 🐞 遅めにフリーズアローを押し始めると、終端の直後に次のフリーズアローが存在する場合にのみ、最後まで押し切ってもイクナイ判定となってしまう問題を修正 ( PR [#1110](https://github.com/cwtickle/danoniplus/pull/1110) ) <- :boom: [**v5.12.1**](Changelog-v5#v5121-2019-06-17)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v23.1.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v23.1.0...v23.1.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v23.1.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v23.1.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v23.1.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v23.1.1/js/danoni_main.js)<br>❤️ goe (@goe0)

<img src="./wiki/changelog/v23_0006.png" alt="v23_0006" width="70%">

## v23.1.0 ([2021-09-11](https://github.com/cwtickle/danoniplus/releases/tag/v23.1.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v23.1.0/total)](https://github.com/cwtickle/danoniplus/archive/v23.1.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v23.1.0/support/v23?style=social)](https://github.com/cwtickle/danoniplus/compare/v23.1.0...support/v23#files_bucket) 
- ⭐ ImgTypeの追加画像セットとして「note」を追加、各種設定を追加 ( PR [#1108](https://github.com/cwtickle/danoniplus/pull/1108) )
- ⭐ ImgType: Original の位置を自由に変更できるよう対応 ( PR [#1105](https://github.com/cwtickle/danoniplus/pull/1105) )
- ⭐ 7keyの別キーモードとして7ikeyを追加 ( PR [#1106](https://github.com/cwtickle/danoniplus/pull/1106) )
- ⭐ 速度設定をショートカットキー限定で0.05倍速刻みで設定できるように変更 ( PR [#1107](https://github.com/cwtickle/danoniplus/pull/1107) )
- 🛠️ Enterキーに割り当てている操作に対してテンキーにも対応 ( PR [#1104](https://github.com/cwtickle/danoniplus/pull/1104) )
- 🛠️ Adjustment設定について、0.1フレーム刻みの設定をショートカットキーのみに限定 ( PR [#1107](https://github.com/cwtickle/danoniplus/pull/1107) )
- 🛠️ 画像セット「classic」の影矢印(arrowShadow.png)を差し替え ( PR [#1108](https://github.com/cwtickle/danoniplus/pull/1108) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v23.1.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v23.0.1...v23.1.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v23.1.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v23.1.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v23.1.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v23.1.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v23.1.0/js/lib/danoni_constants.js)<br>❤️ goe (@goe0)

<img src="./wiki/changelog/v23_0003.png" alt="v23_0003" width="80%">

## v23.0.1 ([2021-09-06](https://github.com/cwtickle/danoniplus/releases/tag/v23.0.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v23.0.1/total)](https://github.com/cwtickle/danoniplus/archive/v23.0.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v23.0.1/support/v23?style=social)](https://github.com/cwtickle/danoniplus/compare/v23.0.1...support/v23#files_bucket) 
- 🐞 blankFrame, 譜面データが極端に小さいフレーム数のときにAdjustmentが一部利かない問題を修正 ( PR [#1102](https://github.com/cwtickle/danoniplus/pull/1102), Issue [#1101](https://github.com/cwtickle/danoniplus/pull/1101) ) <- :boom: [**v23.0.0**](Changelog-v23#v2300-2021-09-04)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v23.0.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v23.0.0...v23.0.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v23.0.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v23.0.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v23.0.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v23.0.1/js/danoni_main.js)<br>❤️ すずめ (@suzme), aconite

## v23.0.0 ([2021-09-04](https://github.com/cwtickle/danoniplus/releases/tag/v23.0.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v23.0.0/total)](https://github.com/cwtickle/danoniplus/archive/v23.0.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v23.0.0/support/v23?style=social)](https://github.com/cwtickle/danoniplus/compare/v23.0.0...support/v23#files_bucket) 
- ⭐ Adjustmentを0.1フレーム単位で設定できる機能を追加 ( PR [#1096](https://github.com/cwtickle/danoniplus/pull/1096), Gitter [2021-09-01, 04](https://gitter.im/danonicw/community?at=612f7a61fd7409696ee9e933) )
- ⭐ Adjustment (画面)の刻み幅を変更 (内側: 1->0.5f, 外側: 5 -> 3f) ( PR [#1096](https://github.com/cwtickle/danoniplus/pull/1096) )
- ⭐ Adjustment (画面)について、ラベルボタンもしくはAlt＋「+」「-」で0.1f幅の調整ができるよう変更 ( PR [#1096](https://github.com/cwtickle/danoniplus/pull/1096) )
- ⭐ デフォルト画像セット(ImgType)を複数指定できるように変更 ( PR [#1097](https://github.com/cwtickle/danoniplus/pull/1097) )
- 🛠️ Adjustment (画面)の単位を表記するよう変更 ( PR [#1099](https://github.com/cwtickle/danoniplus/pull/1099) )
- 🛠️ ファイル直接起動かつエンコード無しの音源データのとき、警告メッセージを表示するよう変更 ( PR [#1098](https://github.com/cwtickle/danoniplus/pull/1098) )
- 🛠️ Speed, Adjustmentのオンマウス説明文を見直し ( PR [#1099](https://github.com/cwtickle/danoniplus/pull/1099) )
- 🛠️ デフォルト画像セットの変更時の右回し、左回しボタンを追加 ( PR [#1097](https://github.com/cwtickle/danoniplus/pull/1097) )
- 🛠️ サンプルHTMLのデフォルト文字コードをUTF-8に統一 ( PR [#1094](https://github.com/cwtickle/danoniplus/pull/1094) )
- 🐞 classicフォルダにある矢印画像が透過pngでない問題を修正 ( PR [#1097](https://github.com/cwtickle/danoniplus/pull/1097) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v23.0.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v22.5.1...v23.0.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v23.0.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v23.0.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v23.0.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v23.0.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v23.0.0/js/lib/danoni_constants.js)<br>❤️ すずめ (@suzme), aconite, izkdic

<img src="./wiki/changelog/v23_0001.png" alt="v23_0001" width="80%">

<img src="./wiki/changelog/v23_0002.png" alt="v23_0002" width="80%">

[**<- v24**](Changelog-v24) | **v23** | [**v22 ->**](Changelog-v22)