⭐ New Features / 🛠️ Improvements / 🐞 Bug Fixes

[**<- v1**](Changelog-v1) | **v0** |

## [Warning] 開発版における注意点
- v0は開発版のため、機能未実装や仕掛中がある場合があります。

## v0.77.x (2018-11-24)
[![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/db75a7a/7fb6b90?style=social&label=Commits%20since%20v0.77.x)](https://github.com/cwtickle/danoniplus/compare/db75a7a...7fb6b90)
- ⭐️ リザルト画面に曲名・譜面名・設定したオプション表示を追加 ( [#105](https://github.com/cwtickle/danoniplus/pull/105) )
- 🛠️ 事前読込対象にライフボーダー用画像を追加 ( [#106](https://github.com/cwtickle/danoniplus/pull/106) )
- 🐞 結果画面表示のColor:OFFの表示不具合を修正 ( [#107](https://github.com/cwtickle/danoniplus/pull/107) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/f03b2caa8d7a3cb4b439b5b76698ecc5651418cd)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/68b8deb7f149f0720cb913ddcbb27a2f56fc7e14...f03b2caa8d7a3cb4b439b5b76698ecc5651418cd#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v0.77.x)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/f03b2ca.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/f03b2ca.tar.gz) )
/ 📃 [**Main source**](https://github.com/cwtickle/danoniplus/raw/f03b2caa8d7a3cb4b439b5b76698ecc5651418cd/js/danoni_main.js)

## v0.76.x (2018-11-24)
[![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/68b8deb/7fb6b90?style=social&label=Commits%20since%20v0.76.x)](https://github.com/cwtickle/danoniplus/compare/68b8deb...7fb6b90)
- ⭐️ Fadeinオプションにスライドバーを追加 ( [#99](https://github.com/cwtickle/danoniplus/pull/99) )
- ⭐️ ライフゲージバーのデザイン追加、位置調整 ( [#100](https://github.com/cwtickle/danoniplus/pull/100), [#102](https://github.com/cwtickle/danoniplus/pull/102) )
- 🐞 ライフゲージ非表示時の記述ミスを修正 ( [#101](https://github.com/cwtickle/danoniplus/pull/101) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/68b8deb7f149f0720cb913ddcbb27a2f56fc7e14)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/b65ddd94b67f707d59b0d1c3a8c8fcbc3995dec0...68b8deb7f149f0720cb913ddcbb27a2f56fc7e14#files_bucket)
 / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v0.76.x) / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/68b8deb.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/68b8deb.tar.gz) ) / 📃 [**Main source**](https://github.com/cwtickle/danoniplus/raw/68b8deb7f149f0720cb913ddcbb27a2f56fc7e14/js/danoni_main.js)

## v0.75.x (2018-11-24)
[![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/b65ddd9/7fb6b90?style=social&label=Commits%20since%20v0.75.x)](https://github.com/cwtickle/danoniplus/compare/b65ddd9...7fb6b90)
- ⭐️ ライフゲージバー初期実装

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/b65ddd94b67f707d59b0d1c3a8c8fcbc3995dec0) / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/5680a86285c474f1c2352da1e88fefe77cb2366f...b65ddd94b67f707d59b0d1c3a8c8fcbc3995dec0#files_bucket)
 / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v0.75.x) / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/b65ddd9.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/b65ddd9.tar.gz) ) / 📃 [**Main source**](https://github.com/cwtickle/danoniplus/raw/b65ddd94b67f707d59b0d1c3a8c8fcbc3995dec0/js/danoni_main.js)

## v0.74.x (2018-11-23)
[![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/5680a86/7fb6b90?style=social&label=Commits%20since%20v0.74.x)](https://github.com/cwtickle/danoniplus/compare/5680a86...7fb6b90)
- ⭐️ Displayオプションを実装 ( [#91](https://github.com/cwtickle/danoniplus/pull/91) )
- 🛠️ 画面レイアウトの修正

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/5680a86285c474f1c2352da1e88fefe77cb2366f) / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/0d675a855b0c62a78a6171577f4a5bc63eccba2e...5680a86285c474f1c2352da1e88fefe77cb2366f#files_bucket)
 / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v0.74.x) / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/5680a86.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/5680a86.tar.gz) ) / 📃 [**Main source**](https://github.com/cwtickle/danoniplus/raw/5680a86285c474f1c2352da1e88fefe77cb2366f/js/danoni_main.js)

## v0.73.x (2018-11-20)
[![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/2b0e4ae/7fb6b90?style=social&label=Commits%20since%20v0.73.x)](https://github.com/cwtickle/danoniplus/compare/2b0e4ae...7fb6b90)
- 🛠️ varではなくconst/letで変数宣言するように変更 ( [#85](https://github.com/cwtickle/danoniplus/pull/85) )
- 🛠️ 現在時刻の取得方法変更(performance.now()を利用) ( [#87](https://github.com/cwtickle/danoniplus/pull/87) )
- 🛠️ 楽曲時間長を秒切り上げに変更

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/0d675a855b0c62a78a6171577f4a5bc63eccba2e) / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/a5be1d1d915cbb35234a58478eb54cfe4b42d4f7...0d675a855b0c62a78a6171577f4a5bc63eccba2e#files_bucket)
 / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v0.73.x) / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/0d675a8.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/0d675a8.tar.gz) ) / 📃 [**Main source**](https://github.com/cwtickle/danoniplus/raw/0d675a855b0c62a78a6171577f4a5bc63eccba2e/js/danoni_main.js)

## v0.72.x (2018-11-20)
[![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/a5be1d1/7fb6b90?style=social&label=Commits%20since%20v0.72.x)](https://github.com/cwtickle/danoniplus/compare/a5be1d1...7fb6b90)
- ⭐️ ライフゲージ（数字）初期実装

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/a5be1d1d915cbb35234a58478eb54cfe4b42d4f7) / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/a7e0e4e41095c7fda66c61324817388e1c75af77...a5be1d1d915cbb35234a58478eb54cfe4b42d4f7#files_bucket)
 / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v0.72.x) / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/a5be1d1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/a5be1d1.tar.gz) ) / 📃 [**Main source**](https://github.com/cwtickle/danoniplus/raw/a5be1d1d915cbb35234a58478eb54cfe4b42d4f7/js/danoni_main.js)

## v0.71.x (2018-11-17)
[![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/a7e0e4e/7fb6b90?style=social&label=Commits%20since%20v0.71.x)](https://github.com/cwtickle/danoniplus/compare/a7e0e4e...7fb6b90)
- ⭐️ customjsファイルを1作品につき最大2ファイル指定できるように変更

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/a7e0e4e41095c7fda66c61324817388e1c75af77) / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/8a22fdd4803304874fe8965ee3ca16f1a5a36045...a7e0e4e41095c7fda66c61324817388e1c75af77#files_bucket)
 / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v0.71.x) / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/a7e0e4e.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/a7e0e4e.tar.gz) ) / 📃 [**Main source**](https://github.com/cwtickle/danoniplus/raw/a7e0e4e41095c7fda66c61324817388e1c75af77/js/danoni_main.js)

## v0.70.x (2018-11-17)
[![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/8a22fdd/7fb6b90?style=social&label=Commits%20since%20v0.70.x)](https://github.com/cwtickle/danoniplus/compare/8a22fdd...7fb6b90)
- ⭐️ フリーズアローの猶予フレームを実装
- 🐞 オート時にフリーズアローが失敗する不具合を修正
- 🐞 リザルト画面移行時、jsエラーが出力される問題を修正

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/8a22fdd4803304874fe8965ee3ca16f1a5a36045) / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/bb025bcd86e3b21b2ef7c4c28287ba8667a1a77f...8a22fdd4803304874fe8965ee3ca16f1a5a36045#files_bucket)
 / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v0.70.x) / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/8a22fdd.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/8a22fdd.tar.gz) ) / 📃 [**Main source**](https://github.com/cwtickle/danoniplus/raw/8a22fdd4803304874fe8965ee3ca16f1a5a36045/js/danoni_main.js)

## v0.69.x (2018-11-17)
[![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/bb025bc/7fb6b90?style=social&label=Commits%20since%20v0.69.x)](https://github.com/cwtickle/danoniplus/compare/bb025bc...7fb6b90)
- ⭐️ ショボーン判定を実装

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/bb025bcd86e3b21b2ef7c4c28287ba8667a1a77f) / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/b2cdf7bff3c5aa4d963a4c4673ced84dbac995eb...bb025bcd86e3b21b2ef7c4c28287ba8667a1a77f#files_bucket)
 / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v0.69.x) / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/bb025bc.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/bb025bc.tar.gz) ) / 📃 [**Main source**](https://github.com/cwtickle/danoniplus/raw/bb025bcd86e3b21b2ef7c4c28287ba8667a1a77f/js/danoni_main.js)

## v0.68.x (2018-11-17)
[![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/b2cdf7b/7fb6b90?style=social&label=Commits%20since%20v0.68.x)](https://github.com/cwtickle/danoniplus/compare/b2cdf7b...7fb6b90)
- ⭐️ フェードアウト、曲終了時間指定時は曲を結果画面中も垂れ流すように変更

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/b2cdf7bff3c5aa4d963a4c4673ced84dbac995eb) / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/1da91c80cc3235b78598ec4076812a766c32aa80...b2cdf7bff3c5aa4d963a4c4673ced84dbac995eb#files_bucket)
 / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v0.68.x) / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/b2cdf7b.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/b2cdf7b.tar.gz) ) / 📃 [**Main source**](https://github.com/cwtickle/danoniplus/raw/b2cdf7bff3c5aa4d963a4c4673ced84dbac995eb/js/danoni_main.js)

## v0.67.x (2018-11-17)
[![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/493fd15/7fb6b90?style=social&label=Commits%20since%20v0.67.x)](https://github.com/cwtickle/danoniplus/compare/493fd15...7fb6b90)
- ⭐️ back_dataのAnimationDurationの単位を秒数からフレーム数に変更
- ⭐️ 画像のpreloadを実装 (rel:preload形式)
- 🐞 途中終了、曲終了時にデバッグ時エラーが発生していた問題を修正

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/1da91c80cc3235b78598ec4076812a766c32aa80) / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/aeda82523f7a6506489374571579c7e8c2fd62f5...1da91c80cc3235b78598ec4076812a766c32aa80#files_bucket)
 / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v0.67.x) / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/1da91c8.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/1da91c8.tar.gz) ) / 📃 [**Main source**](https://github.com/cwtickle/danoniplus/raw/1da91c80cc3235b78598ec4076812a766c32aa80/js/danoni_main.js)

## v0.66.x (2018-11-16)
[![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/ac133fa/7fb6b90?style=social&label=Commits%20since%20v0.66.x)](https://github.com/cwtickle/danoniplus/compare/ac133fa...7fb6b90)
- ⭐️ 背景モーション (back_data) において、spanタグ適用時に色も指定できるよう変更

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/aeda82523f7a6506489374571579c7e8c2fd62f5) / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/c6fcfc210c8feea71c28b0a670f088698c11e84d...aeda82523f7a6506489374571579c7e8c2fd62f5#files_bucket)
 / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v0.66.x) / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/aeda825.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/aeda825.tar.gz) ) / 📃 [**Main source**](https://github.com/cwtickle/danoniplus/raw/aeda82523f7a6506489374571579c7e8c2fd62f5/js/danoni_main.js)

## v0.65.x (2018-11-16)
[![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/c74a55e/7fb6b90?style=social&label=Commits%20since%20v0.65.x)](https://github.com/cwtickle/danoniplus/compare/c74a55e...7fb6b90)
- ⭐️ 背景モーション (back_data) の実装
- 🛠️ 背景画像について、.png/.gif/.jpg/.bmpでないときは文字を表示するように変更

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/c6fcfc210c8feea71c28b0a670f088698c11e84d) / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/2f6a0c465d262330886e79fac4b777e5e86dc7a9...c6fcfc210c8feea71c28b0a670f088698c11e84d#files_bucket)
 / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v0.65.x) / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/c6fcfc2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/c6fcfc2.tar.gz) ) / 📃 [**Main source**](https://github.com/cwtickle/danoniplus/raw/c6fcfc210c8feea71c28b0a670f088698c11e84d/js/danoni_main.js)

## v0.64.x (2018-11-14)
[![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/2f6a0c4/7fb6b90?style=social&label=Commits%20since%20v0.64.x)](https://github.com/cwtickle/danoniplus/compare/2f6a0c4...7fb6b90)
- ⭐️ Canvasのレイヤーを集約し、1つでも動作するように対応
- 🐞 歌詞データが同一フレーム指定の場合にフレームを自動調整するように修正

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/2f6a0c465d262330886e79fac4b777e5e86dc7a9) / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/a0d375e424ff69c28a963a8cee8c9b18ed6b3e7a...2f6a0c465d262330886e79fac4b777e5e86dc7a9#files_bucket)
 / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v0.64.x) / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/2f6a0c4.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/2f6a0c4.tar.gz) ) / 📃 [**Main source**](https://github.com/cwtickle/danoniplus/raw/2f6a0c465d262330886e79fac4b777e5e86dc7a9/js/danoni_main.js)

## v0.63.x (2018-11-13)
[![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/2a8c40c/7fb6b90?style=social&label=Commits%20since%20v0.63.x)](https://github.com/cwtickle/danoniplus/compare/2a8c40c...7fb6b90)
- ⭐️ Readyモーションの実装
- 🐞 フェードイン時の環境ズレを部分的に解消
- 🐞 色変化＆キーパターンの組み合わせで色変化（矢印）の対象が異なる問題を修正

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/a0d375e424ff69c28a963a8cee8c9b18ed6b3e7a) / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/c49b2c99a8312ba1ea50a0ae82851db89a91160c...a0d375e424ff69c28a963a8cee8c9b18ed6b3e7a#files_bucket)
 / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v0.63.x) / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/a0d375e.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/a0d375e.tar.gz) ) / 📃 [**Main source**](https://github.com/cwtickle/danoniplus/raw/a0d375e424ff69c28a963a8cee8c9b18ed6b3e7a/js/danoni_main.js)

## v0.62.x (2018-11-11)
[![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/a858acf/7fb6b90?style=social&label=Commits%20since%20v0.62.x)](https://github.com/cwtickle/danoniplus/compare/a858acf...7fb6b90)
- ⭐️ 独自キーでキーコンパターンの複数指定に対応

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/c49b2c99a8312ba1ea50a0ae82851db89a91160c) / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/dd2b5887115f4311b124ec32664a5b8ac7364797...c49b2c99a8312ba1ea50a0ae82851db89a91160c#files_bucket)
 / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v0.62.x) / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/c49b2c9.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/c49b2c9.tar.gz) ) / 📃 [**Main source**](https://github.com/cwtickle/danoniplus/raw/c49b2c99a8312ba1ea50a0ae82851db89a91160c/js/danoni_main.js)

## v0.61.x (2018-11-11)
[![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/8940b3e/7fb6b90?style=social&label=Commits%20since%20v0.61.x)](https://github.com/cwtickle/danoniplus/compare/8940b3e...7fb6b90)
- ⭐️ 音楽フォルダを変更可能にするヘッダー変数を追加（musicFolder）
- 🐞 checkArrayVal の判定誤りを修正
- 🐞 キーコンフィグパターン変更時、カーソル位置がおかしくなる不具合を修正

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/dd2b5887115f4311b124ec32664a5b8ac7364797) / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/3202644d4be59931cbf23cbb99728564007fc46c...dd2b5887115f4311b124ec32664a5b8ac7364797#files_bucket)
 / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v0.61.x) / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/dd2b588.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/dd2b588.tar.gz) ) / 📃 [**Main source**](https://github.com/cwtickle/danoniplus/raw/dd2b5887115f4311b124ec32664a5b8ac7364797/js/danoni_main.js)

## v0.60.x (2018-11-10)
[![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/0f5ff64/7fb6b90?style=social&label=Commits%20since%20v0.60.x)](https://github.com/cwtickle/danoniplus/compare/0f5ff64...7fb6b90)
- ⭐️ キーコンフィグの対象範囲変更に対応 (ConfigTypeで設定)
- 🐞 キーコンフィグパターン変更時、カーソル位置がおかしくなる不具合を修正

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/3202644d4be59931cbf23cbb99728564007fc46c) / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/f6a11e62f267d017047a647ecdb615540f0e1089...3202644d4be59931cbf23cbb99728564007fc46c#files_bucket)
 / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v0.60.x) / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/3202644.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/3202644.tar.gz) ) / 📃 [**Main source**](https://github.com/cwtickle/danoniplus/raw/3202644d4be59931cbf23cbb99728564007fc46c/js/danoni_main.js)

## v0.59.x (2018-11-10)
[![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/acedcb2/7fb6b90?style=social&label=Commits%20since%20v0.59.x)](https://github.com/cwtickle/danoniplus/compare/acedcb2...7fb6b90)
- ⭐️ 警告メッセージ枠をタイトルに追加（ブラウザチェックなど）
- ⭐️ 新キー作成時のアラートメッセージを追加

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/f6a11e62f267d017047a647ecdb615540f0e1089) / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/3b19692392cd9b2cd285de2138ef5769d41fb658...f6a11e62f267d017047a647ecdb615540f0e1089#files_bucket)
 / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v0.59.x) / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/f6a11e6.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/f6a11e6.tar.gz) ) / 📃 [**Main source**](https://github.com/cwtickle/danoniplus/raw/f6a11e62f267d017047a647ecdb615540f0e1089/js/danoni_main.js)

## v0.58.x (2018-11-07)
[![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/fd9a5ff/7fb6b90?style=social&label=Commits%20since%20v0.58.x)](https://github.com/cwtickle/danoniplus/compare/fd9a5ff...7fb6b90)
- ⭐️ 11i, 13(TP), 14i, 16ikeyを追加

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/3b19692392cd9b2cd285de2138ef5769d41fb658) / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/c2761b33a3fc41db054025c8d8151dd56b4053e2...3b19692392cd9b2cd285de2138ef5769d41fb658#files_bucket)
 / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v0.58.x) / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/3b19692.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/3b19692.tar.gz) ) / 📃 [**Main source**](https://github.com/cwtickle/danoniplus/raw/3b19692392cd9b2cd285de2138ef5769d41fb658/js/danoni_main.js)

## v0.57.x (2018-11-07)
[![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/c2761b3/7fb6b90?style=social&label=Commits%20since%20v0.57.x)](https://github.com/cwtickle/danoniplus/compare/c2761b3...7fb6b90)
- 🛠️ フォントに考慮した記述に一部変更
- 🛠️ スコアがリザルト画面に反映されていない問題を修正
- 🛠️ 全体的にソースのフォーマッターを実行

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/c2761b33a3fc41db054025c8d8151dd56b4053e2) / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/faec77158dd2028621442037bbe675df232d9e42...c2761b33a3fc41db054025c8d8151dd56b4053e2#files_bucket)
 / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v0.57.x) / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/c2761b3.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/c2761b3.tar.gz) ) / 📃 [**Main source**](https://github.com/cwtickle/danoniplus/raw/c2761b33a3fc41db054025c8d8151dd56b4053e2/js/danoni_main.js)

## v0.56.x (2018-11-06)
[![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/b879be2/7fb6b90?style=social&label=Commits%20since%20v0.56.x)](https://github.com/cwtickle/danoniplus/compare/b879be2...7fb6b90)
- 🛠️ 判定カウンタ、曲名、歌詞表示を別スプライトへ移動
- 🛠️ 判定カウンタ作成関数を外へ出してカスタムでも使えるように修正

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/faec77158dd2028621442037bbe675df232d9e42) / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/2641d21623fd271ad6aecfad0fca0eb98c8ed731...faec77158dd2028621442037bbe675df232d9e42#files_bucket)
 / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v0.56.x) / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/faec771.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/faec771.tar.gz) ) / 📃 [**Main source**](https://github.com/cwtickle/danoniplus/raw/faec77158dd2028621442037bbe675df232d9e42/js/danoni_main.js)

## v0.55.x (2018-11-04)
[![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/31ef228/7fb6b90?style=social&label=Commits%20since%20v0.55.x)](https://github.com/cwtickle/danoniplus/compare/31ef228...7fb6b90)
- 🛠️ サンプルがそのまま動くように無音のmp3を追加
- 🐞 判定系を矢印の手前に表示されるよう修正
- 🐞 コピーライト表記を修正

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/2641d21623fd271ad6aecfad0fca0eb98c8ed731) / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/9899e4e192ee976ec5f2f93d318ae01bd816449c...2641d21623fd271ad6aecfad0fca0eb98c8ed731#files_bucket)
 / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v0.55.x) / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/2641d21.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/2641d21.tar.gz) ) / 📃 [**Main source**](https://github.com/cwtickle/danoniplus/raw/2641d21623fd271ad6aecfad0fca0eb98c8ed731/js/danoni_main.js)

## v0.54.x (2018-11-04)
[![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/ad8b577/7fb6b90?style=social&label=Commits%20since%20v0.54.x)](https://github.com/cwtickle/danoniplus/compare/ad8b577...7fb6b90)
- ⭐️ フェードインの早回しボタンを追加
- 🛠️ jsファイルをそのまま使えるように、ライセンス原文を追加
- 🛠️ タイトルページにGithubのリンクを追加

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/9899e4e192ee976ec5f2f93d318ae01bd816449c) / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/0b222e3e1dff5d70029a8ce7670a05c5c84a17d4...9899e4e192ee976ec5f2f93d318ae01bd816449c#files_bucket)
 / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v0.54.x) / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/9899e4e.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/9899e4e.tar.gz) ) / 📃 [**Main source**](https://github.com/cwtickle/danoniplus/raw/9899e4e192ee976ec5f2f93d318ae01bd816449c/js/danoni_main.js)

## v0.53.x (2018-11-04)
[![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/e241753/7fb6b90?style=social&label=Commits%20since%20v0.53.x)](https://github.com/cwtickle/danoniplus/compare/e241753...7fb6b90)
- ⭐️ 逆回しボタン、右クリック制御を追加
- 🛠️ オプション画面のボタン制御を定数・関数化して整理
- 🐞 速度変化・色変化の記述がない場合にメイン画面が動作しない問題を修正
- 🐞 色変化未使用時、プレイ続行できない問題を修正
- 🐞 最初の矢印を押したタイミングだけ、キーが無効になる問題を修正

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/0b222e3e1dff5d70029a8ce7670a05c5c84a17d4) / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/16d48003600ad4925c17dbedbf65d69da7e22437...0b222e3e1dff5d70029a8ce7670a05c5c84a17d4#files_bucket)
 / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v0.53.x) / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/0b222e3.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/0b222e3.tar.gz) ) / 📃 [**Main source**](https://github.com/cwtickle/danoniplus/raw/0b222e3e1dff5d70029a8ce7670a05c5c84a17d4/js/danoni_main.js)

## v0.52.x (2018-11-04)
[![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/c21a354/7fb6b90?style=social&label=Commits%20since%20v0.52.x)](https://github.com/cwtickle/danoniplus/compare/c21a354...7fb6b90)
- ⭐️ オプション画面の一部操作を除き、Contextmenuを非表示に変更
- 🛠️ オプション画面のFadein:100%を削除
- 🐞 結果画面移行時の内部エラーを修正（実動作には影響しない）

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/16d48003600ad4925c17dbedbf65d69da7e22437) / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/cce3152f90180dd0dbb9db33503b25a3a54d5dfe...16d48003600ad4925c17dbedbf65d69da7e22437#files_bucket)
 / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v0.52.x) / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/16d4800.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/16d4800.tar.gz) ) / 📃 [**Main source**](https://github.com/cwtickle/danoniplus/raw/16d48003600ad4925c17dbedbf65d69da7e22437/js/danoni_main.js)

## v0.51.x (2018-11-03)
[![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/84899ae/7fb6b90?style=social&label=Commits%20since%20v0.51.x)](https://github.com/cwtickle/danoniplus/compare/84899ae...7fb6b90)
- 🛠️ 判定キャラクタ 及び ステップゾーンヒット時のモーションを一定時間後に消去するよう変更
- 🐞 色変化が動作しない問題を修正

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/cce3152f90180dd0dbb9db33503b25a3a54d5dfe) / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/5ae11affae5dd10c3741ca56b2b004968dc7e32b...cce3152f90180dd0dbb9db33503b25a3a54d5dfe#files_bucket)
 / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v0.51.x) / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/cce3152.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/cce3152.tar.gz) ) / 📃 [**Main source**](https://github.com/cwtickle/danoniplus/raw/cce3152f90180dd0dbb9db33503b25a3a54d5dfe/js/danoni_main.js)

## v0.50.x (2018-11-01)
[![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/b197aee/7fb6b90?style=social&label=Commits%20since%20v0.50.x)](https://github.com/cwtickle/danoniplus/compare/b197aee...7fb6b90)
- ⭐️ Fadeinオプションの実装
- ⭐️ Volumeの選択域を変更 (1%単位 / 指定されたボリューム群のみ選択可)
- 🐞 Fadein時に速度変化が入っているとタイミングがずれる問題を修正

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/5ae11affae5dd10c3741ca56b2b004968dc7e32b) / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/5369bf46c218b10ca158bbe8330e8e2da2449ee6...5ae11affae5dd10c3741ca56b2b004968dc7e32b#files_bucket)
 / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v0.50.x) / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/5ae11af.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/5ae11af.tar.gz) ) / 📃 [**Main source**](https://github.com/cwtickle/danoniplus/raw/5ae11affae5dd10c3741ca56b2b004968dc7e32b/js/danoni_main.js)

## v0.49.x (2018-10-31)
[![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/d26ad21/7fb6b90?style=social&label=Commits%20since%20v0.49.x)](https://github.com/cwtickle/danoniplus/compare/d26ad21...7fb6b90)
- ⭐️ 個別、全体色変化実装
- 🐞 danoni_custom.js のMain(EnterFrame)が使用できない問題を修正

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/5369bf46c218b10ca158bbe8330e8e2da2449ee6) / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/a360c0c0df156e68ba7cf13f6465c706044b6231...5369bf46c218b10ca158bbe8330e8e2da2449ee6#files_bucket)
 / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v0.49.x) / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/5369bf4.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/5369bf4.tar.gz) ) / 📃 [**Main source**](https://github.com/cwtickle/danoniplus/raw/5369bf46c218b10ca158bbe8330e8e2da2449ee6/js/danoni_main.js)

## v0.47.x (2018-10-30)
[![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/33afc4b/7fb6b90?style=social&label=Commits%20since%20v0.47.x)](https://github.com/cwtickle/danoniplus/compare/33afc4b...7fb6b90)
- ⭐️ 初期フリーズアロー色の変更に対応 (frzColorの実装)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/a360c0c0df156e68ba7cf13f6465c706044b6231) / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/3b69542e7bfc11bf29bc4145520386bbcc733c7c...a360c0c0df156e68ba7cf13f6465c706044b6231#files_bucket)
 / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v0.47.x) / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/a360c0c.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/a360c0c.tar.gz) ) / 📃 [**Main source**](https://github.com/cwtickle/danoniplus/raw/a360c0c0df156e68ba7cf13f6465c706044b6231/js/danoni_main.js)

## v0.46.x (2018-10-30)
[![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/194d5c1/7fb6b90?style=social&label=Commits%20since%20v0.46.x)](https://github.com/cwtickle/danoniplus/compare/194d5c1...7fb6b90)
- ⭐️ 歌詞表示のフェードイン・アウトに対応

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/3b69542e7bfc11bf29bc4145520386bbcc733c7c) / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/b704e5a699a5c1ee919b354010e5441f0d31eba0...3b69542e7bfc11bf29bc4145520386bbcc733c7c#files_bucket)
 / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v0.46.x) / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/3b69542.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/3b69542.tar.gz) ) / 📃 [**Main source**](https://github.com/cwtickle/danoniplus/raw/3b69542e7bfc11bf29bc4145520386bbcc733c7c/js/danoni_main.js)

## v0.44.x (2018-10-29)
[![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/991bdf3/7fb6b90?style=social&label=Commits%20since%20v0.44.x)](https://github.com/cwtickle/danoniplus/compare/991bdf3...7fb6b90)
- 🐞 譜面データの数字が小さく到達時間に満たない場合、自動で曲開始位置をずらすように修正

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/b704e5a699a5c1ee919b354010e5441f0d31eba0) / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/d75c4fd554255ee046286de9073fc2c8c64bffc3...b704e5a699a5c1ee919b354010e5441f0d31eba0#files_bucket)
 / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v0.44.x) / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/b704e5a.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/b704e5a.tar.gz) ) / 📃 [**Main source**](https://github.com/cwtickle/danoniplus/raw/b704e5a699a5c1ee919b354010e5441f0d31eba0/js/danoni_main.js)

## v0.43.x (2018-10-29)
[![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/d75c4fd/7fb6b90?style=social&label=Commits%20since%20v0.43.x)](https://github.com/cwtickle/danoniplus/compare/d75c4fd...7fb6b90)
- 🛠️ ユーザーカスタムイベントの存在チェックを追加
- 🛠️ 読込画面のカスタム処理を追加
- 🐞 キーコンフィグのカーソルがおかしい問題を修正
- 🛠️ キー押下状態を取得して判定を確認する方法に変更
- 🐞 fadeFrame, endFrameが端数時に、曲時間がおかしくなる問題を修正
- 🛠️ 譜面ヘッダー未入力の場合の処理を追加
- 🛠️ キーコンフィグ画面においてボタンが前面に来るように修正

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/d75c4fd554255ee046286de9073fc2c8c64bffc3) / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/bcf8fd25d2ad770da449c689b0c895759a89c72e...d75c4fd554255ee046286de9073fc2c8c64bffc3#files_bucket)
 / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v0.43.x) / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/d75c4fd.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/d75c4fd.tar.gz) ) / 📃 [**Main source**](https://github.com/cwtickle/danoniplus/raw/d75c4fd554255ee046286de9073fc2c8c64bffc3/js/danoni_main.js)

## v0.42.x (2018-10-28)
[![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/0bf910e/7fb6b90?style=social&label=Commits%20since%20v0.42.x)](https://github.com/cwtickle/danoniplus/compare/0bf910e...7fb6b90)
- ⭐️ startFrameに対するendFrameを設定できるように変更

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/bcf8fd25d2ad770da449c689b0c895759a89c72e) / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/257595df7ab9afe19678afcd6e333c217f93ca80...bcf8fd25d2ad770da449c689b0c895759a89c72e#files_bucket)
 / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v0.42.x) / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/bcf8fd2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/bcf8fd2.tar.gz) ) / 📃 [**Main source**](https://github.com/cwtickle/danoniplus/raw/bcf8fd25d2ad770da449c689b0c895759a89c72e/js/danoni_main.js)

## v0.41.x (2018-10-28)
[![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/83120c1/7fb6b90?style=social&label=Commits%20since%20v0.41.x)](https://github.com/cwtickle/danoniplus/compare/83120c1...7fb6b90)
- 🛠️ 矢印画像を差し替え
- 🐞 速度が初期化できていない問題を修正
- 🛠️ 譜面ヘッダーのTwitURLを廃止（location.href利用）
- 🛠️ リザルトコピーにハッシュタグを付加する譜面ヘッダー（hashTag）追加

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/257595df7ab9afe19678afcd6e333c217f93ca80) / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/abe5b6e52511fe07928ca41a723e973529030a5d...257595df7ab9afe19678afcd6e333c217f93ca80#files_bucket)
 / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v0.41.x) / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/257595d.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/257595d.tar.gz) ) / 📃 [**Main source**](https://github.com/cwtickle/danoniplus/raw/257595df7ab9afe19678afcd6e333c217f93ca80/js/danoni_main.js)

## v0.40.x (2018-10-27)
[![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/abe5b6e/7fb6b90?style=social&label=Commits%20since%20v0.40.x)](https://github.com/cwtickle/danoniplus/compare/abe5b6e...7fb6b90)
- ⭐️ 作品別customjsファイルを新設

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/abe5b6e52511fe07928ca41a723e973529030a5d) / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/7f94775...abe5b6e)  / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v0.40.x) / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/abe5b6e.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/abe5b6e.tar.gz) ) / 📃 [**Main source**](https://github.com/cwtickle/danoniplus/raw/abe5b6e52511fe07928ca41a723e973529030a5d/js/danoni_main.js)

----

### v0.39.x (2018-10-27)
- 速度変化（個別加速）を実装

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/7f94775) / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/69a56a0...7f94775)
 / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v0.39.x) / 📃 [**Main source**](https://github.com/cwtickle/danoniplus/raw/7f94775/js/danoni_main.js)

### v0.38.x (2018-10-27)
- パーフェクト演出（暫定）を追加

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/69a56a0) / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/a47b359...69a56a0)
 / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v0.38.x) / 📃 [**Main source**](https://github.com/cwtickle/danoniplus/raw/69a56a0/js/danoni_main.js)

### v0.37.x (2018-10-27)
- オートプレイ実装、ステップゾーンの挙動を一部見直し、AAのフリーズアローを実装

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/a47b359) / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/f230661...a47b359)
 / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v0.37.x) / 📃 [**Main source**](https://github.com/cwtickle/danoniplus/raw/a47b359/js/danoni_main.js)

### v0.36.x (2018-10-27)
- 判定キャラクタを追加

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/f230661) / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/88e39c8...f230661)
 / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v0.36.x) / 📃 [**Main source**](https://github.com/cwtickle/danoniplus/raw/f230661/js/danoni_main.js)

### v0.35.x (2018-10-26)
- 11W, 15Akeyを追加、Enterキーによる画面移動・Shift+Delによる強制終了実装
- スコア計算式修正、9ikeyのキーコンのデフォルトをオリジナルに戻す

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/88e39c8) / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/f3c91de...88e39c8)
 / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v0.35.x) / 📃 [**Main source**](https://github.com/cwtickle/danoniplus/raw/88e39c8/js/danoni_main.js)

### v0.34.x (2018-10-24)
- 結果スコア実装（暫定）、ランク表示を暫定で追加
- フリーズアロー失敗判定が多重になる不具合を修正

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/f3c91de) / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/9dcce95...f3c91de)
 / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v0.34.x) / 📃 [**Main source**](https://github.com/cwtickle/danoniplus/raw/f3c91de/js/danoni_main.js)

### v0.33.x (2018-10-24)
- 17key、9ikeyのステップゾーン配置に対応

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/9dcce95) / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/fc2aea9...9dcce95)
 / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v0.33.x) / 📃 [**Main source**](https://github.com/cwtickle/danoniplus/raw/9dcce95/js/danoni_main.js)

### v0.32.x (2018-10-24)
- 曲時間表示追加、譜面データからのフェードイン・アウト実装

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/fc2aea9) / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/936f6f1...fc2aea9)
 / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v0.32.x) / 📃 [**Main source**](https://github.com/cwtickle/danoniplus/raw/fc2aea9/js/danoni_main.js)

### v0.31.x (2018-10-22)
- フリーズアローの実装

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/936f6f1) / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/4fd2bc8...936f6f1)
 / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v0.31.x) / 📃 [**Main source**](https://github.com/cwtickle/danoniplus/raw/936f6f1/js/danoni_main.js)

### v0.29.x (2018-10-21)
- Speed/Adjustmentに対して逆回しボタン（右クリック操作）を追加

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/4fd2bc8) / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/50ceba1...4fd2bc8)
 / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v0.29.x) / 📃 [**Main source**](https://github.com/cwtickle/danoniplus/raw/4fd2bc8/js/danoni_main.js)

### v0.28.x (2018-10-21)
- 音楽の再生が不安定な事象を改善、曲中リトライ・タイトルバックに対応

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/50ceba1) / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/936422e...50ceba1)
 / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v0.28.x) / 📃 [**Main source**](https://github.com/cwtickle/danoniplus/raw/50ceba1/js/danoni_main.js)

### v0.27.x (2018-10-21)
- フェードイン（譜面データから指定）に対応

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/936422e) / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/dc8de48...936422e)
 / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v0.27.x) / 📃 [**Main source**](https://github.com/cwtickle/danoniplus/raw/936422e/js/danoni_main.js)

### v0.26.x (2018-10-21)
- 歌詞表示に対応、速度変化の不具合を修正
- Tweetリンク実装、プレイ中の曲名表示実装
- タイトルの曲名文字サイズの調整

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/dc8de48) / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/5c61ff8...dc8de48)
 / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v0.26.x) / 📃 [**Main source**](https://github.com/cwtickle/danoniplus/raw/dc8de48/js/danoni_main.js)

### v0.25.x (2018-10-20)
- 楽曲再生同期の仮実装、Boost/Brake実装
- 矢印判定の実装、判定用表示の追加
- サンプルをhtml5用に置き換え、リザルト画面一部実装

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/5c61ff8) / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/da72aef...5c61ff8) 
 / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v0.25.x) / 📃 [**Main source**](https://github.com/cwtickle/danoniplus/raw/5c61ff8/js/danoni_main.js)

## v0.24.x以前のバージョン
⇒ v0.24.x以前のコミットは[こちら](https://github.com/cwtickle/danoniplus/commits/5c61ff8)

### v0.24.x (2018-10-19)[📃](https://github.com/cwtickle/danoniplus/raw/da72aef/js/danoni_main.js)[↕️](https://github.com/cwtickle/danoniplus/compare/0dee669..da72aef)
- 読込処理仮実装
- ステップゾーン仮実装、矢印処理仮実装

### v0.21.x (2018-10-16)[📃](https://github.com/cwtickle/danoniplus/raw/0dee669/js/danoni_main.js)[↕️](https://github.com/cwtickle/danoniplus/compare/2ff8e49..0dee669)
- キーコンフィグ位置をキーの表示位置に合わせて位置調整
- 誤動作対策 (矢印キーやBackSpaceキー、ファンクションキーのブラウザ標準の動きを止める)
- Firefoxでキーが反応しない問題に対応

### v0.19.x (2018-10-14)[📃](https://github.com/cwtickle/danoniplus/raw/2ff8e49/js/danoni_main.js)[↕️](https://github.com/cwtickle/danoniplus/compare/9ca922b..2ff8e49)
- 譜面データからカスタムキーを追加する機能を実装
- g_keyObjの主要項目についてキー別・パターン別で管理するよう変更

### v0.18.x (2018-10-14)[📃](https://github.com/cwtickle/danoniplus/raw/9ca922b/js/danoni_main.js)[↕️](https://github.com/cwtickle/danoniplus/compare/b4ca511..9ca922b)
- 7ikeyのAAキャラクタの位置間違いを修正

### v0.17.x (2018-10-14)[📃](https://github.com/cwtickle/danoniplus/raw/b4ca511/js/danoni_main.js)[↕️](https://github.com/cwtickle/danoniplus/compare/fbe748a..b4ca511)
- キーコンフィグの位置の二段表示対応 (11key等への対応)

### v0.16.x (2018-10-14)[📃](https://github.com/cwtickle/danoniplus/raw/fbe748a/js/danoni_main.js)[↕️](https://github.com/cwtickle/danoniplus/compare/c0ff4d0..fbe748a)
- グローバル変数名の表記見直し、strictモード適用

### v0.15.x (2018-10-14)[📃](https://github.com/cwtickle/danoniplus/raw/c0ff4d0/js/danoni_main.js)[↕️](https://github.com/cwtickle/danoniplus/compare/cfa2ce7..c0ff4d0)
- AAキャラクタの追加、キーコンフィグパターン切替ボタンの実装

### v0.14.x (2018-10-14)[📃](https://github.com/cwtickle/danoniplus/raw/cfa2ce7/js/danoni_main.js)[↕️](https://github.com/cwtickle/danoniplus/compare/3e0b3cd..cfa2ce7)
- キーコンフィグ画面上のスキップ、無効化キーの実装
- キーコンフィグ画面用のカーソル画像の追加
- 譜面データ（矢印本体）の分解処理追加、フリーズアロー名の自動取得追加

### v0.13.x (2018-10-13)[📃](https://github.com/cwtickle/danoniplus/raw/3e0b3cd/js/danoni_main.js)[↕️](https://github.com/cwtickle/danoniplus/compare/12d2bef..3e0b3cd)
- キーコンフィグ初期実装、おにぎりの画像追加

### v0.12.x (2018-10-13)[📃](https://github.com/cwtickle/danoniplus/raw/12d2bef/js/danoni_main.js)[↕️](https://github.com/cwtickle/danoniplus/compare/8325581..12d2bef)
- 横幅可変対応、グローバル変数の見直し

### v0.11.x (2018-10-13)[📃](https://github.com/cwtickle/danoniplus/raw/8325581/js/danoni_main.js)[↕️](https://github.com/cwtickle/danoniplus/compare/af83906..8325581)
- オプション画面のボタンを実装
- ボタン・ラベルの選択不可設定を追加
- 矢印画像追加及び画像表示の反映
- キー別管理オブジェクトの準備

### v0.10.x (2018-10-12)[📃](https://github.com/cwtickle/danoniplus/raw/af83906/js/danoni_main.js)[↕️](https://github.com/cwtickle/danoniplus/compare/5bf014d..af83906)
- タイトルラベルについてコンテキスト直書きからdiv子要素へ変更
- 譜面データ（矢印本体）の分解処理の一部実装

### v0.9.x (2018-10-11)[📃](https://github.com/cwtickle/danoniplus/raw/5bf014d/js/danoni_main.js)
- GitHubへの初回コミット ※コミット日は2018-10-12  
（各画面の枠表示、最低限のボタン・ラベル表示と譜面分解処理を実装）

### v0.1.x (2018-10-08)
- 作成開始（タイトル表示のみ）


[**<- v1**](Changelog-v1) | **v0** |