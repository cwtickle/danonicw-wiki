[<- Home](./) || [運指型](AboutKeys-fingering) | [バランス型](AboutKeys-balance) | [指移動型](AboutKeys-finger-movement) | [スクラッチ型](AboutKeys-scratch) | [オリジナル](AboutKeys-customKeys)

# 多鍵の種類（オリジナル）
## 1key
[ 作品一覧 : [1key](https://cw7.sakura.ne.jp/lstart/?keys=1) ]

<img src="./wiki/keyimg/key1_01.jpg" width="40%">

<img src="./wiki/keys/key1.png" width="80%">

- Dancing★Onigiriの中で別の意味で存在感があるのがこの1keyです。  
おにぎりしかありませんが作品によっては代替キーが用意されている場合もあります。
- 難易度表や多鍵DBの掲載対象ではないため、探すのが大変ですが一定の人気(？)があります。

## 3ikey
[ 作品一覧 : [3ikey](https://cw7.sakura.ne.jp/lstart/?keys=3i) ]

<img src="./wiki/keyimg/key3i_01.png" width="40%">

<img src="./wiki/keys/key3i.png" width="80%">

- 7ikeyのAA部分のみが独立したキーです。7ikeyとして公開されている場合があります。  
1keyよりはキー数が増えているためその分表現の幅は広がっています(？)  

## 7ekey
[ 多鍵DB : [7ekey](https://apoi108.sakura.ne.jp/danoni/ta/?keys=7e) ]

<img src="./wiki/keyimg/key7e_01.png" width="40%">

<img src="./wiki/keys/key7e.png" width="80%">

## 10key
[ 多鍵DB : [10key](https://apoi108.sakura.ne.jp/danoni/ta/?keys=10) ]

<img src="./wiki/keyimg/key10_01.png" width="40%">

<img src="./wiki/keys/key10.png" width="80%">

- 9Akey＋Enterが特徴の10key。  
8key同様にEnterの要素が加わることで、9Akeyにプラスした要素が楽しめます。  

## 10pkey
[ 多鍵DB : [10pkey](https://apoi108.sakura.ne.jp/danoni/ta/?keys=10p) ]

<img src="./wiki/keyimg/key10p_01.jpg" width="40%">
<img src="./wiki/keyimg/key10p_02.jpg" width="40%">
<img src="./wiki/keyimg/key10p_03.jpg" width="40%">
<img src="./wiki/keyimg/key10p_04.jpg" width="40%">

<img src="./wiki/keys/key10p.png" width="80%">

- X型の配置が両サイドについたキー。  
代替キーが多く、柔軟なキー運びが求められそうなキーです。

## 11jkey
[ 多鍵DB : [11jkey](https://apoi108.sakura.ne.jp/danoni/ta/?keys=11j) ]

<img src="./wiki/keys/key11j.png" width="80%">

- Tab＋9Akey＋Enterが特徴の11jkey。  
左右にEnterの要素が加わり、左手もダイナミックな表現ができるようになっています。

## 12Lkey
[ 多鍵DB : [12Lkey](https://apoi108.sakura.ne.jp/danoni/ta/?keys=12L) ]

<img src="./wiki/keyimg/key12L_01.jpg" width="40%">
<img src="./wiki/keyimg/key12L_02.jpg" width="40%">

<img src="./wiki/keys/key12L.png" width="80%">

- 12keyを左右反転したキー。  
おにぎり位置が反転している点が12keyとは異なります。

## 12ikey
[ 多鍵DB : [12ikey](https://apoi108.sakura.ne.jp/danoni/ta/?keys=12i) ]

<img src="./wiki/keyimg/key12i_01.png" width="40%">

<img src="./wiki/keys/key12i.png" width="80%">

- 突如として登場した、ファンクションキーを使った横一列のキー。  
真ん中の4つのキーを左右で取り合いながらプレイします。

## 13ikey
[ 多鍵DB : [13ikey](https://apoi108.sakura.ne.jp/danoni/ta/?keys=13i) ]

<img src="./wiki/keyimg/key13i_01.png" width="40%">

<img src="./wiki/keys/key13i.png" width="80%">

## 14ekey
[ 多鍵DB : [14ekey](https://apoi108.sakura.ne.jp/danoni/ta/?keys=14e) ]

<img src="./wiki/keyimg/key14e_01.png" width="40%">

<img src="./wiki/keys/key14e.png" width="80%">

## 16ekey
[ 多鍵DB : [16ekey](https://apoi108.sakura.ne.jp/danoni/ta/?keys=16e) ]

<img src="./wiki/keyimg/key16e_01.jpg" width="40%">
<img src="./wiki/keyimg/key16e_02.jpg" width="40%">

<img src="./wiki/keys/key16e.png" width="80%">

- 中央のキーの段違いが特徴のキー。  
スクラッチとは違いますが11Wkeyらしい位置移動があるので分類上はこちらにしました。

## 24key
[ 多鍵DB : [24key](https://apoi108.sakura.ne.jp/danoni/ta/?keys=24) ]

<img src="./wiki/keyimg/key24_01.jpg" width="40%">
<img src="./wiki/keyimg/key24_02.jpg" width="40%">

<img src="./wiki/keys/key24.png" width="80%">

- 23keyと同時期に登場。12keyのダブルプレイに上段のおにぎりがついたもの。  

----

## Special Thanks
- しょーでん、蒼宮あいす

----

[<- Home](./) || [運指型](AboutKeys-fingering) | [バランス型](AboutKeys-balance) | [指移動型](AboutKeys-finger-movement) | [スクラッチ型](AboutKeys-scratch) | [オリジナル](AboutKeys-customKeys)

|種類|対応キー数|
|----|----|
|[運指型](AboutKeys-fingering)|[5key](AboutKeys-fingering#5key) / [12key](AboutKeys-fingering#12key) / [14key](AboutKeys-fingering#14key) / [11ikey](AboutKeys-fingering#11ikey) / [17key](AboutKeys-fingering#17key) / [23key](AboutKeys-fingering#23key)|
|[バランス型](AboutKeys-balance)|[7key](AboutKeys-balance#7key) / [9Akey[ダブルプレイ]](AboutKeys-balance#9akey-ダブルプレイ) / [9Bkey](AboutKeys-balance#9bkey) / [7ikey](AboutKeys-balance#7ikey) / [9ikey](AboutKeys-balance#9ikey)|
|[指移動型](AboutKeys-finger-movement)|[11key](AboutKeys-finger-movement#11key) / [11Lkey](AboutKeys-finger-movement#11lkey) / [13key[トリプルプレイ]](AboutKeys-finger-movement#13key-トリプルプレイ) / [15Akey](AboutKeys-finger-movement#15akey) / [15Bkey](AboutKeys-finger-movement#15Bkey) / [14ikey](AboutKeys-finger-movement#14ikey) / [16ikey](AboutKeys-finger-movement#16ikey)|
|[スクラッチ型](AboutKeys-scratch)|[8key](AboutKeys-scratch#8key) / [11Wkey](AboutKeys-scratch#11wkey)|
|[オリジナル](AboutKeys-customKeys)|(Various Keys)|