**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-h0003-setColor) | Japanese** 


[Home](./) &gt; [譜面ヘッダー仕様](dos_header) &gt; [プレイ時の初期設定](dos_header#-プレイ時の初期設定) &gt; [矢印・フリーズアロー色の設定](dos_header#矢印フリーズアロー色の設定)

| [<- colorCdPaddingUse](dos-h0047-colorCdPaddingUse) || **setColor** | [frzColor ->](dos-h0004-frzColor) |

## setColor
- カラーグループ毎の矢印（枠）の色設定

### 使い方
```
|setColor=#9999ff,#ccffff,#ffffff,#ffff99,#ff9966|
|setColor=#ffcccc:#ffffff@radial-gradient,#ff9999:#ffff99@conic-gradient,#ffffff,#ffff99,#ff9966|
|setColor2=#ff9999,#ccffff,#ffffff,#ffff99,#ff9966| // 2譜面目
|setColor3=#ff9999,#ccffff,#ffffff,#ffff99,#ff9966| // 3譜面目

|setColor4=setColor2|  // 4譜面目は2譜面目のデータをコピー
```
### 説明
矢印色を指定します。指定の仕方は、「#ffffff」もしくは「0xffffff」の形式です。  
カンマ区切りで5つの色を指定すると、キー数によって自動で色を振り分けてくれます。  
(ランダムではなく、キー数によって決まっています)  
また、ver12以降[コロン(:)区切りによるグラデーション](dos-c0001-gradation)に対応しています。

### 補足1：省略時の色補完について
- ver13より下記のように色補完を行います。
```
|setColor=#ff9999|                   (1)のみ     ⇒ すべて(1)で埋める
   -> #ffff99,#ffff99,#ffff99,#ffff99,#ffff99
|setColor=#ff3333,#99ffff|           (1),(2)     ⇒ (1),(2)の繰り返し
   -> #ff3333,#99ffff,#ff3333,#99ffff,#ff3333
|setColor=#ff3333,#99ffff,#ffffff|   (1),(2),(3) ⇒ (1),(2),(3)の繰り返し
   -> #ff3333,#99ffff,#ffffff,#ff3333,#99ffff
```

### 補足2：2譜面目以降の個別設定について
- ver21より2譜面目以降の個別設定が行えるようになりました。  
setColor2, setColor3, ... のように指定します。使い方はsetColorと同じです。  
未指定時はこれまで通り、setColorの値が採用されます。

### 参考：setColorで割り当てられた色について
- 割り当て色は、キーコンフィグ画面で確認できます。

<img src="https://github.com/cwtickle/danoniplus/assets/44026291/c1488d28-7afd-4438-b9ec-2ae19dd97603" width="60%">

### 関連項目
- [colorCdPaddingUse](dos-h0047-colorCdPaddingUse) [:pencil:](dos-h0047-colorCdPaddingUse/_edit) 初期矢印色/フリーズアロー色のゼロパディング有無設定
- [**frzColor**](dos-h0004-frzColor) [:pencil:](dos-h0004-frzColor/_edit) フリーズアロー色
- [setShadowColor](dos-h0041-setShadowColor) [:pencil:](dos-h0041-setShadowColor/_edit) 矢印の内側を塗りつぶす設定および色の設定
- [defaultColorgrd](dos-h0061-defaultColorgrd) [:pencil:](dos-h0061-defaultColorgrd/_edit) 自動グラデーション設定
- [titlegrd / titleArrowgrd](dos-h0032-titlegrd) [:pencil:](dos-h0032-titlegrd/_edit) タイトルグラデーション
- [色変化 (ncolor_data)](dos-e0002-ncolorData) [:pencil:](dos-e0002-ncolorData/_edit) 
- [グラデーション仕様](dos-c0001-gradation) [:pencil:](dos-c0001-gradation/_edit)

### 更新履歴

|Version|変更内容|
|----|----|
|[v36.3.0](https://github.com/cwtickle/danoniplus/releases/tag/v36.3.0)|・最大10個まで指定できるよう拡張|
|[v31.2.0](https://github.com/cwtickle/danoniplus/releases/tag/v31.2.0)|・他譜面の`setColor`名を指定することで他譜面のデータを参照する機能を追加|
|[v21.1.0](https://github.com/cwtickle/danoniplus/releases/tag/v21.1.0)|・2譜面目以降の個別設定に対応（譜面分割＆譜面番号可変時）|
|[v21.0.0](https://github.com/cwtickle/danoniplus/releases/tag/v21.0.0)|・2譜面目以降の個別設定に対応|
|[v13.0.0](https://github.com/cwtickle/danoniplus/releases/tag/v13.0.0)|・省略時の色補完仕様を変更|
|[v12.0.0](https://github.com/cwtickle/danoniplus/releases/tag/v12.0.0)|・グラデーション記述に対応|
|[v1.0.0](https://github.com/cwtickle/danoniplus/releases/tag/v1.0.1)|・初回実装|

| [<- colorCdPaddingUse](dos-h0047-colorCdPaddingUse) || **setColor** | [frzColor ->](dos-h0004-frzColor) |