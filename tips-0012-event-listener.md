
[^ Tips Indexに戻る](./tips-index)

| < [ボタン処理のコピー](./tips-0014-copy-button-process) | **イベントリスナーの追加・削除** || [スキップボタンの作成方法](./tips-0013-titleskip) > |

# イベントリスナーの追加・削除
- イベントリスナーはキーを押す(keyDown)、クリックする(click)といったイベントが発生したときに実行する処理を紐づけます。
- 通常は問題ないのですが、カスタムjsで割込み処理を挿入した場合、  
ページの行き来によってイベントリスナーが二重に発生してしまうことがあります。  
- Dancing☆Onigiri (CW Edition)では、こういったことを避けるため、イベントリスナー用の関数を別に用意しています。
- なお、既存ボタンの割込み処理については [ボタン処理の拡張・上書き](./tips-0005-button-expansion) で実現可能です。イベントリスナーの追加は思わぬ不具合を引き起こすことがあるため、まずは既存の方法で実現できるか考えてから使用することをお勧めします。

## 関数概要

|関数名|引数|用途|
|----|----|----|
|g_handler.addListener|_target<br>_type<br>_listener<br>_capture|イベントリスナー追加用関数。イベント識別キーを返却する。<br>`_target`: リスナーを追加する対象(div要素, windowなど)<br>`_type`, `_listener`, `_capture`: [EventTarget.addEventListener](https://developer.mozilla.org/ja/docs/Web/API/EventTarget/addEventListener)の引数と同じです。|
|g_handler.removeListener|_key|引数に指定したイベント識別キーに対応するイベントリスナーを削除する。|

## 使い方
### div要素
```javascript
const div = document.getElementById(`divTest`);

// リスナー作成時のキーを取得
const lsnrKey = g_handler.addListener(div, `keydown`, _ => executeFunction());

// リスナーキーを参照できるように、divのプロパティとして追加
div.setAttribute(`lsnrKey`, lsnrKey);

//---------------------------------------------------

// リスナーキーの削除
g_handler.removeListener(div.getAttribute(`lsnrKey`));
```

### window関数（リスナー重複起動対策）
```javascript
// 初期化
g_stateObj.currentListenerKey = ``:

//---------------------------------------------------

// すでにリスナー作成済みの場合は、該当のリスナーキーを削除
if (g_stateObj.currentListenerKey !== ``) {
    g_handler.removeListener(g_stateObj.currentListenerKey);
}

// リスナー作成時のキーを変数に格納
g_stateObj.currentListenerKey = g_handler.addListener(div, `keydown`, _ => executeFunction());
```

## ページ作成者
- ティックル

[^ Tips Indexに戻る](./tips-index)

| < [ボタン処理のコピー](./tips-0014-copy-button-process) | **イベントリスナーの追加・削除** || [スキップボタンの作成方法](./tips-0013-titleskip) > |
