**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-h0071-playingWidth) | Japanese** 

[Home](./) &gt; [譜面ヘッダー仕様](dos_header) &gt; [プレイ時の初期設定](dos_header#-プレイ時の初期設定) &gt; [プレイ画面位置の設定](dos_header#プレイ画面位置の設定)

| [<- playingX / playingY](dos-h0070-playingX) | **playingWidth / playingHeight** | [customCreditWidth ->](dos-h0083-customCreditWidth) |

## playingWidth / playingHeight
- ゲーム表示エリアの横幅／高さ設定
- 共通設定 ⇒ [g_presetObj.playingWidth, playingHeight](dos-s0007-viewControl#ゲーム表示エリアの横幅高さ-g_presetobjplayingwidth-g_presetobjplayingheight)

### 使い方
```
|playingWidth=600|
|playingHeight=570|
```
### 説明
プレイ画面のうち、ゲーム表示エリアの横幅、高さを指定します。デフォルトはプレイ画面の横幅／高さです。

<img src="https://user-images.githubusercontent.com/44026291/199850377-f4516af7-d8c4-452e-8561-4f6f4eade429.png" width="100%">

<img src="https://github.com/cwtickle/danoniplus/assets/44026291/bdf3d1a8-f951-485b-9d71-e7ab7c2fe757" width="50%">

### 関連項目
- [playingX / playingY](dos-h0070-playingX) [:pencil:](dos-h0070-playingX/_edit) ゲーム表示エリアのX, Y座標
- [customCreditWidth](dos-h0083-customCreditWidth) [:pencil:](dos-h0071-playingWidth/_edit) カスタムクレジットエリアの横幅

### 更新履歴

|Version|変更内容|
|----|----|
|[v33.6.0](https://github.com/cwtickle/danoniplus/releases/tag/v33.6.0)|・初回実装 (playingHeight)|
|[v16.0.0](https://github.com/cwtickle/danoniplus/releases/tag/v16.0.0)|・初回実装 (playingWidth)|

| [<- playingX / playingY](dos-h0070-playingX) | **playingWidth / playingHeight** | [customCreditWidth ->](dos-h0083-customCreditWidth) |