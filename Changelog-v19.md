**[English](https://github.com/cwtickle/danoniplus-docs/wiki/Changelog-v19) | Japanese** 

[^ Back to All Version List (Security Policy)](https://github.com/cwtickle/danoniplus/security/policy)  
[^ Back to Update Info](./UpdateInfo#-v19-changelog)

[**<- v20**](Changelog-v20) | **v19** | [**v18** ->](Changelog-v18)  
(**🔖 [22 Releases](https://github.com/cwtickle/danoniplus/releases?q=tag%3Av19)** )

## 🔃 Files changed (v19)

|Directory|FileName||Last Updated|
|----|----|----|----|
|/js|danoni_main.js|[📥](https://github.com/cwtickle/danoniplus/releases/download/v19.5.17/danoni_main.js)|**v19.5.17**|
|/js/lib|danoni_constants.js|[📥](https://github.com/cwtickle/danoniplus/releases/download/v19.5.10/danoni_constants.js)|[v19.5.10](Changelog-v19#v19510-2021-09-24)|

<details>
<summary>Changed file list before v18</summary>

|Directory|FileName||Last Updated|
|----|----|----|----|
|/js/lib|danoni_localbinary.js|[📥](https://github.com/cwtickle/danoniplus/releases/download/v15.1.0/danoni_localbinary.js)|[v15.1.0](Changelog-v15#v1510-2020-05-21)|
|/css|danoni_main.css|[📥](https://github.com/cwtickle/danoniplus/releases/download/v18.5.0/danoni_main.css)|[v18.5.0](Changelog-v18#v1850-2020-12-20)|
|/img|aaShadow.svg<br>arrow.svg<br>arrowShadow.svg<br>borderline.svg<br>c.svg<br>cursor.svg<br>giko.svg<br>iyo.svg<br>monar.svg<br>morara.svg<br>onigiri.svg|[📥](https://github.com/cwtickle/danoniplus/releases/download/v15.1.0/img.zip)|[v15.1.0](Changelog-v15#v1510-2020-05-21)|
|/skin|danoni_skin_default.css<br>danoni_skin_light.css<br>danoni_skin_skyblue.css|[📥](https://github.com/cwtickle/danoniplus/releases/download/v14.1.0/danoni_skin_default.css)<br>[📥](https://github.com/cwtickle/danoniplus/releases/download/v14.1.0/danoni_skin_light.css)<br>[📥](https://github.com/cwtickle/danoniplus/releases/download/v14.1.0/danoni_skin_skyblue.css)|[v14.1.0](Changelog-v14#v1410-2020-05-01)|

</details>

⭐ New Features / 🛠️ Improvements / 🐞 Bug Fixes  
( Files changed: ⚪ danoni_main.js / 🔵 danoni_constants.js / 🔴 css files )

## [Warning] 更新終了後の不具合情報
- [更新を終了したバージョンの不具合情報#v19](DeprecatedVersionBugs#v19) を参照

## v19.5.17 ([2022-08-03](https://github.com/cwtickle/danoniplus/releases/tag/v19.5.17))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v19.5.17/total)](https://github.com/cwtickle/danoniplus/archive/v19.5.17.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v19.5.17/support/v19?style=social)](https://github.com/cwtickle/danoniplus/compare/v19.5.17...support/v19#files_bucket) 
- 🐞 **C)** 重複するID名の解消 ( PR [#1300](https://github.com/cwtickle/danoniplus/pull/1300) ) <- :boom: [**v11.4.0**](Changelog-v11#v1140-2020-02-05)
- 🐞 **C)** 譜面明細画面でID名に英数以外が入りうる問題の修正 ( PR [#1300](https://github.com/cwtickle/danoniplus/pull/1300) ) <- :boom: [**v19.3.0**](Changelog-v19#v1930-2021-01-30)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v19.5.17)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v19.5.16...v19.5.17#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v19.5.17)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v19.5.17.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v19.5.17.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v19.5.17/js/danoni_main.js)
/ 🎣 [**v27.8.1**](./Changelog-v27#v2781-2022-08-01)

## v19.5.16 ([2022-05-22](https://github.com/cwtickle/danoniplus/releases/tag/v19.5.16))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v19.5.16/total)](https://github.com/cwtickle/danoniplus/archive/v19.5.16.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v19.5.16/support/v19?style=social)](https://github.com/cwtickle/danoniplus/compare/v19.5.16...support/v19#files_bucket) 
- 🐞 フォルダ名に半角スペースや全角文字が含まれている場合に起動できないことがある問題を修正 ( PR [#1289](https://github.com/cwtickle/danoniplus/pull/1289) ) <- :boom: [**v19.5.15**](Changelog-v19#v19515-2022-04-10)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v19.5.16)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v19.5.15...v19.5.16#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v19.5.16)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v19.5.16.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v19.5.16.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v19.5.16/js/danoni_main.js)
/ 🎣 [**v27.5.1**](./Changelog-v27#v2751-2022-05-22)

## v19.5.15 ([2022-04-10](https://github.com/cwtickle/danoniplus/releases/tag/v19.5.15))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v19.5.15/total)](https://github.com/cwtickle/danoniplus/archive/v19.5.15.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v19.5.15/support/v19?style=social)](https://github.com/cwtickle/danoniplus/compare/v19.5.15...support/v19#files_bucket) 
- 🐞 カレントパス、譜面ファイル取得部分をエスケープするよう修正 ( PR [#1277](https://github.com/cwtickle/danoniplus/pull/1277) ) <- :boom: [**v19.4.1**](Changelog-v19#v1941-2021-02-06)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v19.5.15)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v19.5.14...v19.5.15#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v19.5.15)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v19.5.15.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v19.5.15.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v19.5.15/js/danoni_main.js)
/ 🎣 [**v27.3.1**](./Changelog-v27#v2731-2022-04-10)

## v19.5.14 ([2022-02-16](https://github.com/cwtickle/danoniplus/releases/tag/v19.5.14))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v19.5.14/total)](https://github.com/cwtickle/danoniplus/archive/v19.5.14.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v19.5.14/support/v19?style=social)](https://github.com/cwtickle/danoniplus/compare/v19.5.14...support/v19#files_bucket) 
- 🛠️ npm環境用にディレクトリ構成を見直し ( PR [#1228](https://github.com/cwtickle/danoniplus/pull/1228) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v19.5.14)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v19.5.13...v19.5.14#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v19.5.14)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v19.5.14.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v19.5.14.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v19.5.14/js/danoni_main.js)
/ 🎣 [**v26.1.2**](./Changelog-v26#v2612-2022-02-16)

## v19.5.13 ([2021-10-27](https://github.com/cwtickle/danoniplus/releases/tag/v19.5.13))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v19.5.13/total)](https://github.com/cwtickle/danoniplus/archive/v19.5.13.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v19.5.13/support/v19?style=social)](https://github.com/cwtickle/danoniplus/compare/v19.5.13...support/v19#files_bucket) 
- 🐞 Reverse時のarrowMotionDataが正しく反映されない問題を修正 ( PR [#1146](https://github.com/cwtickle/danoniplus/pull/1146) ) <- :boom: [**v19.5.11**](Changelog-v19#v19511-2021-10-04)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v19.5.13)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v19.5.12...v19.5.13#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v19.5.13)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v19.5.13.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v19.5.13.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v19.5.13/js/danoni_main.js)
/ 🎣 [**v24.0.1**](./Changelog-v24#v2401-2021-10-27)

## v19.5.12 ([2021-10-14](https://github.com/cwtickle/danoniplus/releases/tag/v19.5.12))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v19.5.12/total)](https://github.com/cwtickle/danoniplus/archive/v19.5.12.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v19.5.12/support/v19?style=social)](https://github.com/cwtickle/danoniplus/compare/v19.5.12...support/v19#files_bucket) 
- 🛠️ 曲中リトライキーの猶予フレームを廃止 ( PR [#1137](https://github.com/cwtickle/danoniplus/pull/1137), Gitter [2021-10-14](https://gitter.im/danonicw/community?at=6167f968fb8ca0572bbf48d6) )
- 🐞 Display画面から直接開始した場合、プレイ後キーコンフィグ画面に戻るとキーパターンがデータセーブ有効時、Selfにならない問題を修正 ( PR [#1136](https://github.com/cwtickle/danoniplus/pull/1136) ) <- :boom: [**v4.6.0**](Changelog-v4#v460-2019-05-03)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v19.5.12)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v19.5.11...v19.5.12#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v19.5.12)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v19.5.12.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v19.5.12.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v19.5.12/js/danoni_main.js)
/ 🎣 [**v23.5.0**](./Changelog-v23#v2350-2021-10-14)

## v19.5.11 ([2021-10-04](https://github.com/cwtickle/danoniplus/releases/tag/v19.5.11))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v19.5.11/total)](https://github.com/cwtickle/danoniplus/archive/v19.5.11.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v19.5.11/support/v19?style=social)](https://github.com/cwtickle/danoniplus/compare/v19.5.11...support/v19#files_bucket) 
- 🐞 矢印データが昇順で無いときに一部矢印が表示されないことがある問題を修正 ( PR [#1134](https://github.com/cwtickle/danoniplus/pull/1134), Gitter [2021-10-03](https://gitter.im/danonicw/community?at=61595d062197144e84443743) ) <- :boom: **initial**

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v19.5.11)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v19.5.10...v19.5.11#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v19.5.11)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v19.5.11.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v19.5.11.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v19.5.11/js/danoni_main.js)
/ 🎣 [**v23.4.1**](./Changelog-v23#v2341-2021-10-04)

## v19.5.10 ([2021-09-24](https://github.com/cwtickle/danoniplus/releases/tag/v19.5.10))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v19.5.10/total)](https://github.com/cwtickle/danoniplus/archive/v19.5.10.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v19.5.10/support/v19?style=social)](https://github.com/cwtickle/danoniplus/compare/v19.5.10...support/v19#files_bucket) 
- 🛠️ 設定画面のショートカットキーについて、ReverseをRキー、Scrollを上下キーに変更 ( PR [#1120](https://github.com/cwtickle/danoniplus/pull/1120), [#1121](https://github.com/cwtickle/danoniplus/pull/1121) )
- 🐞 Reverseのショートカットキー(Rキー)について、拡張スクロールがある場合でもバックグラウンドで動いてしまう問題を修正 ( PR [#1120](https://github.com/cwtickle/danoniplus/pull/1120) ) <- :boom: [**v19.5.0**](Changelog-v19#v1950-2021-02-09)
- 🐞 Scroll無効時、Reverseの左キーとラベルボタンの一部が押せない問題を修正 ( PR [#1120](https://github.com/cwtickle/danoniplus/pull/1120) ) <- :boom: [**v10.2.1**](Changelog-v10#v1021-2019-11-20)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v19.5.10)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v19.5.9...v19.5.10#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v19.5.10)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v19.5.10.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v19.5.10.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v19.5.10/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v19.5.10/js/lib/danoni_constants.js)
/ 🎣 [**v23.3.1**](./Changelog-v23#v2331-2021-09-24)

## v19.5.9 ([2021-09-17](https://github.com/cwtickle/danoniplus/releases/tag/v19.5.9))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v19.5.9/total)](https://github.com/cwtickle/danoniplus/archive/v19.5.9.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v19.5.9/support/v19?style=social)](https://github.com/cwtickle/danoniplus/compare/v19.5.9...support/v19#files_bucket) 
- 🛠️ Motion: Boost時の軌道計算方法を見直し ( PR [#1111](https://github.com/cwtickle/danoniplus/pull/1111) )
- 🐞 フリーズアローを押し直したとき終端の位置がずれる問題を修正 ( PR [#1110](https://github.com/cwtickle/danoniplus/pull/1110) ) <- :boom: **initial**
- 🐞 フリーズアローを離したとき全体の位置がずれる問題を修正 ( PR [#1110](https://github.com/cwtickle/danoniplus/pull/1110) ) <- :boom: **initial**
- 🐞 Motion: Boost, Brake時、早めにフリーズアローを押し始めると終端の位置がずれる問題を修正 ( PR [#1110](https://github.com/cwtickle/danoniplus/pull/1110) ) <- :boom: **initial**
- 🐞 遅めにフリーズアローを押し始めると、終端の直後に次のフリーズアローが存在する場合にのみ、最後まで押し切ってもイクナイ判定となってしまう問題を修正 ( PR [#1110](https://github.com/cwtickle/danoniplus/pull/1110) ) <- :boom: [**v5.12.1**](Changelog-v5#v5121-2019-06-17)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v19.5.9)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v19.5.8...v19.5.9#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v19.5.9)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v19.5.9.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v19.5.9.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v19.5.9/js/danoni_main.js)
/ 🎣 [**v23.1.1**](./Changelog-v23#v2311-2021-09-17)

## v19.5.8 ([2021-05-16](https://github.com/cwtickle/danoniplus/releases/tag/v19.5.8))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v19.5.8/total)](https://github.com/cwtickle/danoniplus/archive/v19.5.8.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v19.5.8/support/v19?style=social)](https://github.com/cwtickle/danoniplus/compare/v19.5.8...support/v19#files_bucket) 
- 🐞 17key(パターン2)のシャッフルグループ間違いを修正 ( PR [#1084](https://github.com/cwtickle/danoniplus/pull/1084) ) <- :boom: [**v3.1.0**](Changelog-v3#v310-2019-02-26)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v19.5.8)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v19.5.7...v19.5.8#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v19.5.8)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v19.5.8.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v19.5.8.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v19.5.8/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v19.5.8/js/lib/danoni_constants.js)
/ 🎣 [**v22.4.1**](./Changelog-v22#v2241-2021-05-16)

## v19.5.7 ([2021-04-07](https://github.com/cwtickle/danoniplus/releases/tag/v19.5.7))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v19.5.7/total)](https://github.com/cwtickle/danoniplus/archive/v19.5.7.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v19.5.7/support/v19?style=social)](https://github.com/cwtickle/danoniplus/compare/v19.5.7...support/v19#files_bucket) 
- 🐞 空の矢印データを含む譜面にS-Randomをかけるとフルコン演出が早まる問題を修正
 ( PR [#1044](https://github.com/cwtickle/danoniplus/pull/1044), Gitter [2021-04-06](https://gitter.im/danonicw/community?at=606c808592a3431fd67b1640) ) <- :boom: [**v3.1.0**](Changelog-v3#v310-2019-02-26)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v19.5.7)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v19.5.6...v19.5.7#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v19.5.7)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v19.5.7.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v19.5.7.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v19.5.7/js/danoni_main.js)
/ 🎣 [**v21.4.2**](./Changelog-v21#v2142-2021-04-07)

## v19.5.6 ([2021-03-06](https://github.com/cwtickle/danoniplus/releases/tag/v19.5.6))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v19.5.6/total)](https://github.com/cwtickle/danoniplus/archive/v19.5.6.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v19.5.6/support/v19?style=social)](https://github.com/cwtickle/danoniplus/compare/v19.5.6...support/v19#files_bucket) 
- 🛠️ Base64エンコードした曲データを直接デコードするよう変更 ( PR [#1009](https://github.com/cwtickle/danoniplus/pull/1009) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v19.5.6)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v19.5.5...v19.5.6#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v19.5.6)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v19.5.6.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v19.5.6.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v19.5.6/js/danoni_main.js)
/ 🎣 [**v20.5.1**](./Changelog-v20#v2051-2021-03-06)

## v19.5.5 ([2021-02-20](https://github.com/cwtickle/danoniplus/releases/tag/v19.5.5))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v19.5.5/total)](https://github.com/cwtickle/danoniplus/archive/v19.5.5.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v19.5.5/support/v19?style=social)](https://github.com/cwtickle/danoniplus/compare/v19.5.5...support/v19#files_bucket) 
- 🛠️ KeyConfig画面のTo Settingsボタンの表示部にg_lblNameObjを適用 ( PR [#986](https://github.com/cwtickle/danoniplus/pull/986) )
- 🛠️ imgタグ生成時においてcrossOrigin属性を付与するよう変更 ( PR [#988](https://github.com/cwtickle/danoniplus/pull/988) )
- 🐞 preloadFile関数において画像ファイル以外が指定された場合の問題を修正 ( PR [#989](https://github.com/cwtickle/danoniplus/pull/989) ) <- :boom: [**v18.5.0**](Changelog-v18#v1850-2020-12-20) 

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v19.5.5)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v19.5.4...v19.5.5#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v19.5.5)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v19.5.5.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v19.5.5.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v19.5.5/js/danoni_main.js)
/ 🎣 [**v20.2.1**](./Changelog-v20#v2021-2021-02-20)

## v19.5.4 ([2021-02-16](https://github.com/cwtickle/danoniplus/releases/tag/v19.5.4))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v19.5.4/total)](https://github.com/cwtickle/danoniplus/archive/v19.5.4.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v19.5.4/support/v19?style=social)](https://github.com/cwtickle/danoniplus/compare/v19.5.4...support/v19#files_bucket) 
- 🛠️ キーコンフィグ画面以外の戻るボタンのショートカットキーにShift+Tabを追加割り当て ( PR [#981](https://github.com/cwtickle/danoniplus/pull/981) )
- 🛠️ MacOS/iPadOSにおいてメイン画面、キーコンフィグ画面の割り当てキーを一部変更 ( PR [#981](https://github.com/cwtickle/danoniplus/pull/981) )
- 🐞 MacOS/iPadOSにおいて結果画面のCopyResultのショートカットキーを押した後、
他のキーを押すとCopyResultが動いてしまう問題を修正 ( PR [#981](https://github.com/cwtickle/danoniplus/pull/981) ) <- :boom: [**v19.5.0**](Changelog-v19#v1950-2021-02-09)
- 🐞 対象が無いときにショートカットキーを使うとエラーになることがある問題を修正 ( PR [#983](https://github.com/cwtickle/danoniplus/pull/983) ) <- :boom: [**v19.5.0**](Changelog-v19#v1950-2021-02-09)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v19.5.4)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v19.5.3...v19.5.4#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v19.5.4)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v19.5.4.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v19.5.4.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v19.5.4/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v19.5.4/js/lib/danoni_constants.js)
/ 🎣 [**v20.2.0**](./Changelog-v20#v2020-2021-02-16)

## v19.5.3 ([2021-02-13](https://github.com/cwtickle/danoniplus/releases/tag/v19.5.3))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v19.5.3/total)](https://github.com/cwtickle/danoniplus/archive/v19.5.3.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v19.5.3/support/v19?style=social)](https://github.com/cwtickle/danoniplus/compare/v19.5.3...support/v19#files_bucket) 
- 🛠️ Adjustment設定のショートカットにテンキーの「+」「-」を追加で割り当て ( PR [#973](https://github.com/cwtickle/danoniplus/pull/973) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v19.5.3)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v19.5.2...v19.5.3#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v19.5.3)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v19.5.3.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v19.5.3.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v19.5.3/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v19.5.3/js/lib/danoni_constants.js)
/ 🎣 [**v20.1.2**](./Changelog-v20#v2020-2021-02-14)

----

## v19.5.2 ([2021-02-12](https://github.com/cwtickle/danoniplus/releases/tag/v19.5.2))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v19.5.2/total)](https://github.com/cwtickle/danoniplus/archive/v19.5.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v19.5.2/support/v19?style=social)](https://github.com/cwtickle/danoniplus/compare/v19.5.2...support/v19#files_bucket) 
- 🛠️ ショートカット表示部分などで使用している共通文字を定数化・変数化 ( PR [#968](https://github.com/cwtickle/danoniplus/pull/968) )
- 🛠️ ResultViewで利用している g_resultMsgObj を廃止し、resultViewText関数へ置き換え ( PR [#968](https://github.com/cwtickle/danoniplus/pull/968) )
- 🐞 createCss2Button関数にて右クリック拡張ができない場合がある問題を修正 ( PR [#970](https://github.com/cwtickle/danoniplus/pull/970) ) <- :boom: [**v17.5.0**](Changelog-v17#v1750-2020-10-17)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v19.5.2)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v19.5.0...v19.5.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v19.5.2)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v19.5.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v19.5.2.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v19.5.2/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v19.5.2/js/lib/danoni_constants.js)

## v19.5.0 ([2021-02-09](https://github.com/cwtickle/danoniplus/releases/tag/v19.5.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v19.5.0/total)](https://github.com/cwtickle/danoniplus/archive/v19.5.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v19.5.0/support/v19?style=social)](https://github.com/cwtickle/danoniplus/compare/v19.5.0...support/v19#files_bucket) 
- ⭐ 画面別ショートカットキーを実装 ( PR [#964](https://github.com/cwtickle/danoniplus/pull/964) )
- 🛠️ Ready? の文字幅を画面幅に合わせるよう変更 ( PR [#966](https://github.com/cwtickle/danoniplus/pull/966) ) 
- 🐞 譜面明細画面でデータ出力ボタンを押した後、譜面選択できない問題を修正 ( PR [#965](https://github.com/cwtickle/danoniplus/pull/965) ) <- :boom: [**v19.3.0**](Changelog-v19#v1930-2021-01-30)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v19.5.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v19.4.1...v19.5.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v19.5.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v19.5.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v19.5.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v19.5.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v19.5.0/js/lib/danoni_constants.js)<br>❤️ apoi, aconite

<img src="./wiki/changelog/v19_0001.png" alt="v19_0001" width="90%">

## v19.4.1 ([2021-02-06](https://github.com/cwtickle/danoniplus/releases/tag/v19.4.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v19.4.1/total)](https://github.com/cwtickle/danoniplus/archive/v19.4.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v19.4.1/support/v19?style=social)](https://github.com/cwtickle/danoniplus/compare/v19.4.1...support/v19#files_bucket) 
- ⭐ 作品htmlの配置場所の変更に対応 ( PR [#955](https://github.com/cwtickle/danoniplus/pull/955), [#959](https://github.com/cwtickle/danoniplus/pull/959) )
- ⭐ 8keyにおいてEnterおにぎりを左側にするモードと12keyモードを実装 ( PR [#957](https://github.com/cwtickle/danoniplus/pull/957) )
- 🐞 changeStyle関数のコードミスを修正 ( PR [#958](https://github.com/cwtickle/danoniplus/pull/958) ) <- :boom: [**v17.1.0**](Changelog-v17#v1710-2020-09-28)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v19.4.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v19.3.0...v19.4.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v19.4.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v19.4.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v19.4.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v19.4.1/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v19.4.1/js/lib/danoni_constants.js)<br>❤️ apoi, izkdic

<img src="./wiki/changelog/v19_0002.png" alt="v19_0002" width="80%">

<img src="./wiki/changelog/v19_0003.png" alt="v19_0003" width="80%">

## v19.3.0 ([2021-01-30](https://github.com/cwtickle/danoniplus/releases/tag/v19.3.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v19.3.0/total)](https://github.com/cwtickle/danoniplus/archive/v19.3.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v19.3.0/support/v19?style=social)](https://github.com/cwtickle/danoniplus/compare/v19.3.0...support/v19#files_bucket) 
- ⭐ ラベルテキスト及び説明文を danoni_constants.js へ移動・集約 ( PR [#946](https://github.com/cwtickle/danoniplus/pull/946) )
- ⭐ musicFolderのカレントディレクトリ指定に対応 ( Issue [#935](https://github.com/cwtickle/danoniplus/pull/935), PR [#948](https://github.com/cwtickle/danoniplus/pull/948) )
- ⭐ settingType, skinType, customjsについてカレント＋サブディレクトリ指定に対応 ( Issue [#935](https://github.com/cwtickle/danoniplus/pull/935), PR [#949](https://github.com/cwtickle/danoniplus/pull/949) )
- ⭐ danoni_setting.js 側でデフォルトスキン・デフォルトカスタムJs指定に対応 ( PR [#950](https://github.com/cwtickle/danoniplus/pull/950) )
- ⭐ Ready? 文字を直接書き換える設定を追加 ( PR [#946](https://github.com/cwtickle/danoniplus/pull/946) )
- 🛠️ 各譜面の譜面情報を出力する「データ出力」を押したとき、メッセージが出るように変更 ( PR [#946](https://github.com/cwtickle/danoniplus/pull/946) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v19.3.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v19.2.0...v19.3.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v19.3.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v19.3.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v19.3.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v19.3.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v19.3.0/js/lib/danoni_constants.js)

<img src="./wiki/changelog/v19_0004.png" alt="v19_0004" width="75%">

## v19.2.0 ([2021-01-20](https://github.com/cwtickle/danoniplus/releases/tag/v19.2.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v19.2.0/total)](https://github.com/cwtickle/danoniplus/archive/v19.2.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v19.2.0/support/v19?style=social)](https://github.com/cwtickle/danoniplus/compare/v19.2.0...support/v19#files_bucket) 
- ⭐ タイトルの背景矢印の透明度を指定できるように変更 ( Gitter [2021-01-18](https://gitter.im/danonicw/community?at=60058b1d5562a61e9ab284a6), PR [#942](https://github.com/cwtickle/danoniplus/pull/942) )
- ⭐ グラデーションで色名を指定する場合に、透明度を合わせて指定できるよう変更 ( PR [#943](https://github.com/cwtickle/danoniplus/pull/943) )
- ⭐ グラデーションの角度単位にrad, grad, turnが使えるよう変更 ( PR [#943](https://github.com/cwtickle/danoniplus/pull/943) )
- 🛠️ リストのいずれかに部分一致検索（大小文字問わず）する処理を統一 ( PR [#943](https://github.com/cwtickle/danoniplus/pull/943) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v19.2.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v19.1.0...v19.2.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v19.2.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v19.2.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v19.2.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v19.2.0/js/danoni_main.js)<br>❤️ izkdic

<img src="./wiki/changelog/v19_0005.png" alt="v19_0005" width="60%">

## v19.1.0 ([2021-01-18](https://github.com/cwtickle/danoniplus/releases/tag/v19.1.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v19.1.0/total)](https://github.com/cwtickle/danoniplus/archive/v19.1.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v19.1.0/support/v19?style=social)](https://github.com/cwtickle/danoniplus/compare/v19.1.0...support/v19#files_bucket) 
- ⭐ danoni_setting.jsにカスタムゲージリストを追加 ( PR [#940](https://github.com/cwtickle/danoniplus/pull/940) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v19.1.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v19.0.0...v19.1.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v19.1.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v19.1.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v19.1.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v19.1.0/js/danoni_main.js)<br>❤️ MFV2 (@MFV2)

```javascript
// カスタムゲージ設定(デフォルト)
// 'ゲージ名': `回復・ダメージ量設定`　の形式で指定します。
// (F : 矢印数によらず固定, V: 矢印数により変動)
const g_presetGaugeList = {
	'Original': `F`,
	'Normal': `V`,
	'Hard': `V`,
};
```

## v19.0.0 ([2021-01-17](https://github.com/cwtickle/danoniplus/releases/tag/v19.0.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v19.0.0/total)](https://github.com/cwtickle/danoniplus/archive/v19.0.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v19.0.0/support/v19?style=social)](https://github.com/cwtickle/danoniplus/compare/v19.0.0...support/v19#files_bucket) 
- ⭐ ゲージ設定(Gauge)のゲージ種・譜面別の入力補完に対応 ( PR [#937](https://github.com/cwtickle/danoniplus/pull/937) )
- 🛠️ コード整理、非推奨関数を danoni_legacy_function.js へ移動 ( PR [#926](https://github.com/cwtickle/danoniplus/pull/926), [#934](https://github.com/cwtickle/danoniplus/pull/934), [#938](https://github.com/cwtickle/danoniplus/pull/938) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v19.0.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v18.9.3...v19.0.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v19.0.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v19.0.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v19.0.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v19.0.0/js/danoni_main.js)

```
|gaugeHeavy=x,1,20,100|
-> |gaugeHeavy=x,1,20,100$x,1,20,100$...$x,1,20,100|
|gaugeEasy=70,2,5,40$80,2,7,40|
-> |gaugeEasy=70,2,5,40$80,2,7,40$70,2,5,40$...$70,2,5,40|
```

[**<- v20**](Changelog-v20) | **v19** | [**v18** ->](Changelog-v18)
