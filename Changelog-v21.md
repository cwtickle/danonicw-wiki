**[English](https://github.com/cwtickle/danoniplus-docs/wiki/Changelog-v21) | Japanese** 

[^ Back to All Version List (Security Policy)](https://github.com/cwtickle/danoniplus/security/policy)  
[^ Back to Update Info](./UpdateInfo#-v21-changelog)

[**<- v22**](Changelog-v22) | **v21** | [**v20 ->**](Changelog-v20)  
(**🔖 [12 Releases](https://github.com/cwtickle/danoniplus/releases?q=tag%3Av21)** )

## 🔃 Files changed (v21)

|Directory|FileName||Last Updated|
|----|----|----|----|
|/js|danoni_main.js|[📥](https://github.com/cwtickle/danoniplus/releases/download/v21.5.6/danoni_main.js)|**v21.5.6**|
|/js/lib|danoni_constants.js|[📥](https://github.com/cwtickle/danoniplus/releases/download/v21.5.3/danoni_constants.js)|[v21.5.3](Changelog-v21#v2153-2021-09-24)|
|/css|danoni_main.css|[📥](https://github.com/cwtickle/danoniplus/releases/download/v21.4.2/danoni_main.css)|[v21.4.2](Changelog-v21#v2142-2021-04-07)|
|/skin|danoni_skin_default.css<br>danoni_skin_light.css<br>danoni_skin_skyblue.css|[📥](https://github.com/cwtickle/danoniplus/releases/download/v21.4.2/danoni_skin_default.css)<br>[📥](https://github.com/cwtickle/danoniplus/releases/download/v21.4.2/danoni_skin_light.css)<br>[📥](https://github.com/cwtickle/danoniplus/releases/download/v21.4.2/danoni_skin_skyblue.css)|[v21.4.2](Changelog-v21#v2142-2021-04-07)|

<details>
<summary>Changed file list before v20</summary>

|Directory|FileName||Last Updated|
|----|----|----|----|
|/js/lib|danoni_localbinary.js|[📥](https://github.com/cwtickle/danoniplus/releases/download/v15.1.0/danoni_localbinary.js)|[v15.1.0](./Changelog-v15#v1510-2020-05-21)|
|/img|aaShadow.svg<br>arrow.svg<br>arrowShadow.svg<br>borderline.svg<br>c.svg<br>cursor.svg<br>giko.svg<br>iyo.svg<br>monar.svg<br>morara.svg<br>onigiri.svg|[📥](https://github.com/cwtickle/danoniplus/releases/download/v15.1.0/img.zip)|[v15.1.0](./Changelog-v15#v1510-2020-05-21)|

</details>

⭐ New Features / 🛠️ Improvements / 🐞 Bug Fixes  
( Files changed: ⚪ danoni_main.js / 🔵 danoni_constants.js / 🔴 css files )

## [Warning] 更新終了後の不具合情報
- [更新を終了したバージョンの不具合情報#v21](DeprecatedVersionBugs#v21) を参照

## v21.5.6 ([2021-10-27](https://github.com/cwtickle/danoniplus/releases/tag/v21.5.6))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v21.5.6/total)](https://github.com/cwtickle/danoniplus/archive/v21.5.6.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v21.5.6/support/v21?style=social)](https://github.com/cwtickle/danoniplus/compare/v21.5.6...support/v21#files_bucket) 
- 🐞 Reverse時のarrowMotionDataが正しく反映されない問題を修正 ( PR [#1146](https://github.com/cwtickle/danoniplus/pull/1146) ) <- :boom: [**v21.5.4**](Changelog-v21#v2154-2021-10-04)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v21.5.6)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v21.5.5...v21.5.6#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v21.5.6)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v21.5.6.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v21.5.6.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v21.5.6/js/danoni_main.js)
/ 🎣 [**v24.0.1**](./Changelog-v24#v2401-2021-10-27)

## v21.5.5 ([2021-10-14](https://github.com/cwtickle/danoniplus/releases/tag/v21.5.5))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v21.5.5/total)](https://github.com/cwtickle/danoniplus/archive/v21.5.5.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v21.5.5/support/v21?style=social)](https://github.com/cwtickle/danoniplus/compare/v21.5.5...support/v21#files_bucket) 
- 🛠️ 曲中リトライキーの猶予フレームを廃止 ( PR [#1137](https://github.com/cwtickle/danoniplus/pull/1137), Gitter [2021-10-14](https://gitter.im/danonicw/community?at=6167f968fb8ca0572bbf48d6) )
- 🐞 Display画面から直接開始した場合、プレイ後キーコンフィグ画面に戻るとキーパターンがデータセーブ有効時、Selfにならない問題を修正 ( PR [#1136](https://github.com/cwtickle/danoniplus/pull/1136) ) <- :boom: [**v4.6.0**](Changelog-v4#v460-2019-05-03)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v21.5.5)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v21.5.4...v21.5.5#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v21.5.5)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v21.5.5.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v21.5.5.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v21.5.5/js/danoni_main.js)
/ 🎣 [**v23.5.0**](./Changelog-v23#v2350-2021-10-14)

## v21.5.4 ([2021-10-04](https://github.com/cwtickle/danoniplus/releases/tag/v21.5.4))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v21.5.4/total)](https://github.com/cwtickle/danoniplus/archive/v21.5.4.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v21.5.4/support/v21?style=social)](https://github.com/cwtickle/danoniplus/compare/v21.5.4...support/v21#files_bucket) 
- 🐞 矢印データが昇順で無いときに一部矢印が表示されないことがある問題を修正 ( PR [#1134](https://github.com/cwtickle/danoniplus/pull/1134), Gitter [2021-10-03](https://gitter.im/danonicw/community?at=61595d062197144e84443743) ) <- :boom: **initial**

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v21.5.4)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v21.5.3...v21.5.4#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v21.5.4)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v21.5.4.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v21.5.4.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v21.5.4/js/danoni_main.js)
/ 🎣 [**v23.4.1**](./Changelog-v23#v2341-2021-10-04)

## v21.5.3 ([2021-09-24](https://github.com/cwtickle/danoniplus/releases/tag/v21.5.3))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v21.5.3/total)](https://github.com/cwtickle/danoniplus/archive/v21.5.3.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v21.5.3/support/v21?style=social)](https://github.com/cwtickle/danoniplus/compare/v21.5.3...support/v21#files_bucket) 
- 🛠️ 設定画面のショートカットキーについて、ReverseをRキー、Scrollを上下キーに変更 ( PR [#1120](https://github.com/cwtickle/danoniplus/pull/1120), [#1121](https://github.com/cwtickle/danoniplus/pull/1121) )
- 🐞 Reverseのショートカットキー(Rキー)について、拡張スクロールがある場合でもバックグラウンドで動いてしまう問題を修正 ( PR [#1120](https://github.com/cwtickle/danoniplus/pull/1120) ) <- :boom: [**v19.5.0**](Changelog-v19#v1950-2021-02-09)
- 🐞 Scroll無効時、Reverseの左キーとラベルボタンの一部が押せない問題を修正 ( PR [#1120](https://github.com/cwtickle/danoniplus/pull/1120) ) <- :boom: [**v10.2.1**](Changelog-v10#v1021-2019-11-20)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v21.5.3)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v21.5.2...v21.5.3#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v21.5.3)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v21.5.3.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v21.5.3.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v21.5.3/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v21.5.3/js/lib/danoni_constants.js)
/ 🎣 [**v23.3.1**](./Changelog-v23#v2331-2021-09-24)

## v21.5.2 ([2021-09-17](https://github.com/cwtickle/danoniplus/releases/tag/v21.5.2))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v21.5.2/total)](https://github.com/cwtickle/danoniplus/archive/v21.5.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v21.5.2/support/v21?style=social)](https://github.com/cwtickle/danoniplus/compare/v21.5.2...support/v21#files_bucket) 
- 🛠️ Motion: Boost時の軌道計算方法を見直し ( PR [#1111](https://github.com/cwtickle/danoniplus/pull/1111) )
- 🐞 フリーズアローを押し直したとき終端の位置がずれる問題を修正 ( PR [#1110](https://github.com/cwtickle/danoniplus/pull/1110) ) <- :boom: **initial**
- 🐞 フリーズアローを離したとき全体の位置がずれる問題を修正 ( PR [#1110](https://github.com/cwtickle/danoniplus/pull/1110) ) <- :boom: **initial**
- 🐞 Motion: Boost, Brake時、早めにフリーズアローを押し始めると終端の位置がずれる問題を修正 ( PR [#1110](https://github.com/cwtickle/danoniplus/pull/1110) ) <- :boom: **initial**
- 🐞 遅めにフリーズアローを押し始めると、終端の直後に次のフリーズアローが存在する場合にのみ、最後まで押し切ってもイクナイ判定となってしまう問題を修正 ( PR [#1110](https://github.com/cwtickle/danoniplus/pull/1110) ) <- :boom: [**v5.12.1**](Changelog-v5#v5121-2019-06-17)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v21.5.2)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v21.5.1...v21.5.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v21.5.2)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v21.5.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v21.5.2.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v21.5.2/js/danoni_main.js)
/ 🎣 [**v23.1.1**](./Changelog-v23#v2311-2021-09-17)

## v21.5.1 ([2021-05-16](https://github.com/cwtickle/danoniplus/releases/tag/v21.5.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v21.5.1/total)](https://github.com/cwtickle/danoniplus/archive/v21.5.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v21.5.1/support/v21?style=social)](https://github.com/cwtickle/danoniplus/compare/v21.5.1...support/v21#files_bucket) 
- 🐞 17key(パターン2)のシャッフルグループ間違いを修正 ( PR [#1084](https://github.com/cwtickle/danoniplus/pull/1084) ) <- :boom: [**v3.1.0**](Changelog-v3#v310-2019-02-26)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v21.5.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v21.5.0...v21.5.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v21.5.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v21.5.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v21.5.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v21.5.1/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v21.5.1/js/lib/danoni_constants.js)
/ 🎣 [**v22.4.1**](./Changelog-v22#v2241-2021-05-16)

----

## v21.5.0 ([2021-04-22](https://github.com/cwtickle/danoniplus/releases/tag/v21.5.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v21.5.0/total)](https://github.com/cwtickle/danoniplus/archive/v21.5.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v21.5.0/support/v21?style=social)](https://github.com/cwtickle/danoniplus/compare/v21.5.0...support/v21#files_bucket) 
- ⭐ 譜面明細表示の逆回し、Save切替/Display切替ボタンの右クリック対応 ( PR [#1052](https://github.com/cwtickle/danoniplus/pull/1052), [#1056](https://github.com/cwtickle/danoniplus/pull/1056) )
- 🛠️ タイトル画面、結果画面で「Ctrl+C」(コピー)が機能するよう改善 ( PR [#1053](https://github.com/cwtickle/danoniplus/pull/1053) )
- 🛠️ 共通系処理のコード見直し ( PR [#1054](https://github.com/cwtickle/danoniplus/pull/1054), [#1055](https://github.com/cwtickle/danoniplus/pull/1055) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v21.5.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v21.4.2...v21.5.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v21.5.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v21.5.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v21.5.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v21.5.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v21.5.0/js/lib/danoni_constants.js)

<img src="./wiki/changelog/v21_0001.png" alt="v21_0001" width="70%">

## v21.4.2 ([2021-04-07](https://github.com/cwtickle/danoniplus/releases/tag/v21.4.2))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v21.4.2/total)](https://github.com/cwtickle/danoniplus/archive/v21.4.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v21.4.2/support/v21?style=social)](https://github.com/cwtickle/danoniplus/compare/v21.4.2...support/v21#files_bucket) 
- ⭐ キーコンフィグの対応キー箇所をボタンに変更 ( PR [#1040](https://github.com/cwtickle/danoniplus/pull/1040), [#1048](https://github.com/cwtickle/danoniplus/pull/1048) )
- 🛠️ キー数、共通処理系のコード整理 ( PR [#1041](https://github.com/cwtickle/danoniplus/pull/1041), [#1042](https://github.com/cwtickle/danoniplus/pull/1042), [#1043](https://github.com/cwtickle/danoniplus/pull/1043), [#1045](https://github.com/cwtickle/danoniplus/pull/1045), [#1046](https://github.com/cwtickle/danoniplus/pull/1046), [#1050](https://github.com/cwtickle/danoniplus/pull/1050) )
- 🛠️ 警告ウィンドウ内の上下レイアウト崩れを改善 ( Issue [#823](https://github.com/cwtickle/danoniplus/pull/823), PR [#1046](https://github.com/cwtickle/danoniplus/pull/1046) )
- 🐞 空の矢印データを含む譜面にS-Randomをかけるとフルコン演出が早まる問題を修正
 ( PR [#1044](https://github.com/cwtickle/danoniplus/pull/1044), Gitter [2021-04-06](https://gitter.im/danonicw/community?at=606c808592a3431fd67b1640) ) <- :boom: [**v3.1.0**](Changelog-v3#v310-2019-02-26)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v21.4.2)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v21.3.0...v21.4.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v21.4.2)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v21.4.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v21.4.2.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v21.4.2/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v21.4.2/js/lib/danoni_constants.js)🔴<br>❤️ izkdic, すずめ (@suzme)

<img src="./wiki/changelog/v21_0002.png" alt="v21_0002" width="60%">

## v21.3.0 ([2021-04-03](https://github.com/cwtickle/danoniplus/releases/tag/v21.3.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v21.3.0/total)](https://github.com/cwtickle/danoniplus/archive/v21.3.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v21.3.0/support/v21?style=social)](https://github.com/cwtickle/danoniplus/compare/v21.3.0...support/v21#files_bucket) 
- 🛠️ 同じ警告メッセージが二重に表示される可能性がある問題を改善 ( PR [#1037](https://github.com/cwtickle/danoniplus/pull/1037)  )
- 🛠️ g_loadObjのプロパティ値について、読込完了時に `true` に変えるよう統一 ( PR [#1036](https://github.com/cwtickle/danoniplus/pull/1036) )
- 🛠️ 空スプライト作成処理を`createEmptySprite`関数へ移行 ( PR [#1038](https://github.com/cwtickle/danoniplus/pull/1038) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v21.3.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v21.2.0...v21.3.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v21.3.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v21.3.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v21.3.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v21.3.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v21.3.0/js/lib/danoni_constants.js)

## v21.2.0 ([2021-03-28](https://github.com/cwtickle/danoniplus/releases/tag/v21.2.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v21.2.0/total)](https://github.com/cwtickle/danoniplus/archive/v21.2.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v21.2.0/support/v21?style=social)](https://github.com/cwtickle/danoniplus/compare/v21.2.0...support/v21#files_bucket) 
- ⭐ 背景状況により明暗用のカラーセットを使用するよう変更 ( PR [#1033](https://github.com/cwtickle/danoniplus/pull/1033) )
- ⭐ ColorType：Type0について、未指定かつ明背景の場合、自動で中間色を黒にするよう変更 ( PR [#1033](https://github.com/cwtickle/danoniplus/pull/1033) )
- 🛠️ データセーブフラグ切替ボタンをDisplay画面にも表示 ( PR [#1032](https://github.com/cwtickle/danoniplus/pull/1032) )
- 🛠️ danoni3.html のカスタムjsサンプルを更新 ( PR [#1034](https://github.com/cwtickle/danoniplus/pull/1034) )
- 🛠️ メッセージ表示周りのコード整理 ( PR [#1033](https://github.com/cwtickle/danoniplus/pull/1033) )
- 🛠️ CSSファイルの動的読み込みについて読込完了待ちをするよう変更 ( PR [#1033](https://github.com/cwtickle/danoniplus/pull/1033) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v21.2.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v21.1.0...v21.2.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v21.2.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v21.2.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v21.2.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v21.2.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v21.2.0/js/lib/danoni_constants.js)

<img src="./wiki/changelog/v21_0003.png" alt="v21_0003" width="80%">

## v21.1.0 ([2021-03-19](https://github.com/cwtickle/danoniplus/releases/tag/v21.1.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v21.1.0/total)](https://github.com/cwtickle/danoniplus/archive/v21.1.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v21.1.0/support/v21?style=social)](https://github.com/cwtickle/danoniplus/compare/v21.1.0...support/v21#files_bucket) 
- ⭐ 譜面毎にカスタムゲージリストが作成できるように変更 ( PR [#1024](https://github.com/cwtickle/danoniplus/pull/1024) )
- ⭐ ゲージ個別設定（譜面ヘッダー）について、譜面毎の分割記法に対応 ( PR [#1024](https://github.com/cwtickle/danoniplus/pull/1024) )
- ⭐ 譜面分割＆譜面番号可変時、初期矢印・フリーズアロー色（setColor2, frzColor2, ...）を譜面番号固定時と同様、個別の譜面ファイルへ記載できるように変更 ( PR [#1024](https://github.com/cwtickle/danoniplus/pull/1024) )
- ⭐ カスタムゲージリストについて、ライフ制ゲージ/ノルマ制ゲージ/共通設定ファイルで指定のリストを設定できるよう変更 ( Issue [#1026](https://github.com/cwtickle/danoniplus/pull/1026), PR [#1027](https://github.com/cwtickle/danoniplus/pull/1027) )
- 🛠️ 文字列関連のコード整理 ( PR [#1024](https://github.com/cwtickle/danoniplus/pull/1024), [#1025](https://github.com/cwtickle/danoniplus/pull/1025) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v21.1.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v21.0.0...v21.1.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v21.1.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v21.1.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v21.1.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v21.1.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v21.1.0/js/lib/danoni_constants.js)<br>❤️ MFV2 (@MFV2), すずめ (@suzme)

## v21.0.0 ([2021-03-12](https://github.com/cwtickle/danoniplus/releases/tag/v21.0.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v21.0.0/total)](https://github.com/cwtickle/danoniplus/archive/v21.0.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v21.0.0/support/v21?style=social)](https://github.com/cwtickle/danoniplus/compare/v21.0.0...support/v21#files_bucket) 
- ⭐ 初期矢印・フリーズアロー色(setColor/frzColor)の譜面別設定実装 ( Issue [#390](https://github.com/cwtickle/danoniplus/pull/390), PR [#1019](https://github.com/cwtickle/danoniplus/pull/1019) )
- ⭐ 譜面分割＆譜面番号固定時、個別の譜面ファイル内に
setColor/frzColor/setShadowColor/frzShadowColorの記述があれば、その値を採用するよう変更 ( PR [#1019](https://github.com/cwtickle/danoniplus/pull/1019) )
- ⭐ 譜面セレクター表示時のショートカットキー実装 ( PR [#1013](https://github.com/cwtickle/danoniplus/pull/1013) )
- ⭐ 矢印・フリーズアローモーションの20以上のキー対応 ( PR [#1020](https://github.com/cwtickle/danoniplus/pull/1020) )
- 🛠️ 非推奨関数を danoni_legacy_function.js へ移動 ( PR [#1017](https://github.com/cwtickle/danoniplus/pull/1017) )
- 🛠️ 譜面セレクター表示時、Difficulty設定、各画面移動以外の隠れているボタンに対する
ショートカットキーを無効化 ( PR [#1013](https://github.com/cwtickle/danoniplus/pull/1013) )
- 🛠️ キーコンフィグ画面の影矢印部分がColorTypeの変化に対応するよう変更 ( PR [#1013](https://github.com/cwtickle/danoniplus/pull/1013) )
- 🛠️ キーコンフィグ画面、レベル計算ツール周りのコード整理 ( PR [#1016](https://github.com/cwtickle/danoniplus/pull/1016), [#1018](https://github.com/cwtickle/danoniplus/pull/1018), [#1021](https://github.com/cwtickle/danoniplus/pull/1021) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v21.0.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v20.5.2...v21.0.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v21.0.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v21.0.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v21.0.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v21.0.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v21.0.0/js/lib/danoni_constants.js)<br>❤️ ショウタ, MFV2 (@MFV2)

<img src="./wiki/changelog/v21_0004.png" alt="v21_0004" width="80%">

[**<- v22**](Changelog-v22) | **v21** | [**v20 ->**](Changelog-v20)
