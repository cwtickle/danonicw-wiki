**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-h0048-resultMotionSet) | Japanese** 

[Home](./) &gt; [譜面ヘッダー仕様](dos_header) &gt; [タイトル・結果画面の初期設定](dos_header#%EF%B8%8F-タイトル結果画面の初期設定)

| [<- maskresultButton](dos-h0044-maskresultButton) | **resultMotionSet** | [resultFormat ->](dos-h0072-resultFormat) |

## resultMotionSet
- リザルトモーションのON/OFF設定

### 使い方
```
|resultMotionSet=false|
```
### 説明
リザルトモーションのON/OFFを、DisplayオプションのBackgroundのON/OFFと連動させるかを設定します。    

デフォルトは`true`(連動する)です。

|値|既定|内容|
|----|----|----|
|false||設定と連動しない|
|true|*|設定と連動する|

### 関連項目
- [maskresultButton](dos-h0044-maskresultButton) [:pencil:](dos-h0044-maskresultButton/_edit) リザルト画面上のボタン群の有効/無効設定
- [背景・マスクモーション (back_data, mask_data)](dos-e0004-animationData) [:pencil:](dos-e0004-animationData/_edit) 

### 更新履歴

|Version|変更内容|
|----|----|
|[v7.8.0](https://github.com/cwtickle/danoniplus/releases/tag/v7.8.0)|・初回実装|

| [<- maskresultButton](dos-h0044-maskresultButton) | **resultMotionSet** | [resultFormat ->](dos-h0072-resultFormat) |