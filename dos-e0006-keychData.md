**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-e0006-keychData) | Japanese** 

[↑ 譜面エフェクト仕様に戻る](dos_effect)

| [< 矢印・フリーズアローモーション](./dos-e0005-motionData) | **キー数変化** | [スクロール反転・階層移動 >](./dos-e0007-scrollchData) |

## キー数変化 ( keych_data )
### 概要
- カスタムキー専用で、表示する部分キー(のステップゾーン)をフレーム指定で瞬時に切り替えます。

### 使い方
```
|keych_data=0,11L,1951,5,1955,11L,2810,5,2814,11L,3264,5,3275,11L|

|keyExtraList=Tr|
|keyGroupTr=5,5,5,5,5,11L,11L,11L,11L,11L,11L,11L,11L,11L,11L,11L|
 ^ あらかじめカスタムキーのkeyGroupXの指定が必要。ここで定義した「5」「11L」がkeych_dataのキーグループとして使用できる

|keych2_data=
   0,5:0.3/11L
1951,11L
|
|keych3_data=keych2_data|  // 3譜面目は2譜面目のデータをコピー
```
- あらかじめ、部分表示したいキー全体をカバーする[カスタムキー](./keys)を作成し、  
**keyGroupX**(部分キーの割り当て)を済ませている必要があります。
- 奇数番目で変更タイミングを指定し、偶数番目に**keyGroupX**で指定したキーグループを指定すると、部分キーの切り替えを行うことができます。
- 偶数番目のキーグループは、スラッシュ区切りで複数指定することができ、さらにコロン(`:`)の後に透明度が指定できます。

### 記述仕様

|番号|論理名|設定例|内容|
|----|----|----|----|
|1|Frame|1436|変更するタイミングのフレーム数を指定します。|
|2|keyGroup|11L|[カスタムキー](./keys)定義の**keyGroupX**(部分キーの割り当て)で指定したキーグループを指定。キーグループ以外(`X`など)を記載して何も表示しない設定にすることも可能。<br>スラッシュ区切りで複数のキーグループの指定も可能(ver37以降)。|

### 関連項目
- [キー数仕様](./keys)
- [キー変化作品の実装例](./tips-0009-key-types-change)
- [keyGroupOrder](dos-h0092-keyGroupOrder) (◇) [:pencil:](dos-h0092-keyGroupOrder/_edit) キーコンフィグで設定可能な部分キーグループの設定

### 更新履歴

|Version|変更内容|
|----|----|
|[v37.0.0](https://github.com/cwtickle/danoniplus/releases/tag/v37.0.0)|・複数の部分キー指定、透明度指定に対応|
|[v31.2.0](https://github.com/cwtickle/danoniplus/releases/tag/v31.2.0)|・他譜面の`keych_data`名を指定することで他譜面のデータを参照する機能を追加|
|[v30.2.0](https://github.com/cwtickle/danoniplus/releases/tag/v30.2.0)|・改行区切りに対応|
|[v30.0.1](https://github.com/cwtickle/danoniplus/releases/tag/v30.0.1)|・初回実装|

| [< 矢印・フリーズアローモーション](./dos-e0005-motionData) | **キー数変化** | [スクロール反転・階層移動 >](./dos-e0007-scrollchData) |
