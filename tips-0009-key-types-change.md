**[English](https://github.com/cwtickle/danoniplus-docs/wiki/tips-0009-key-types-change) | Japanese** 

[^ Tips Indexに戻る](./tips-index)

| < [既存キーのキーパターン上書き](./tips-0006-keypattern-update) | **キー変化作品の実装例** | [カスタムキーで独自のReverseを設定](./tips-0011-original-reverse) > |

# キー変化作品の実装例
- ver30より、キー変化作品がcustomJsを使わなくても作成できるようになりました。  
ただ、実際に作る場合にカスタムキー定義などの前提知識が必要となるため、実際の例を元にtipsとしてまとめました。
- ver35.1.0以前については[こちら](./tips-0009-key-types-change-v34)をご覧ください。

## カスタムキー定義
- 部分キーを重ねるように定義します。  
重ねる位置は**posX**、部分キー定義は**keyGroupX**で記述します。  
**posX**の値がわかりにくいですが、下記のイメージで入れていきます。
- **divX**には下段の開始位置、終了位置+1を入れていきます。  
この例の場合は`9`と`16`です。

<img src="https://user-images.githubusercontent.com/44026291/219953589-c8dab174-7d84-4696-a312-70f9af9efdb8.png" width="100%">

```
|minWidth11D=650| // このキー数で要求する最小横幅(px単位)
|pos11D=1.5...4.5,3.5...6.5,9...15,0...8|
|keyGroup11D=11L,11L,11L,11L,11,11,11,11,11/11L,11/11L,11/11L,11/11L,11/11L,11/11L,11/11L,9C,9C,9C,9C,9C,9C,9C,9C,9C|
|div11D=9,16|
```
- 上記で指定した対応付けに沿って、色番号(colorX)と割り当てる変数名(charaX)、  
矢印の表示回転量(stepRtnX)、キー割当(keyCtrlX)を割り当てていきます。
```
// 表示順の都合で、11Lkey(上段)→11key(上段)→7key(下段)→9Ckey(上段)の順に指定しています
|color11D=3,3,3,3,4,4,4,4,0,1,0,2,0,1,0,3,3,3,3,2,4,4,4,4|
|chara11D=4A_s,4A_t,7,4A_a,aspace,4A_b|
|stepRtn11D=4A,4A,0,-45,-90,onigiri,90,135,180,4A,onigiri,4A|
|keyCtrl11D=4W,4A,S,D,F,Space,J,K,L,4W,Space,4A|
```
- |charaXX=4A_s| -> sleft, sdown, sup, sright / |charaXX=4A_t| -> tleft, tdown, tup, tright
- |charaXX=7| -> left, leftdia, down, space, up, rightdia, right
- |stepRtnXX=4A| -> 0, -90, 90, 180
- |keyCtrlXX=4A| -> Left, Down, Up, Right / |keyCtrlXX=4W| -> W, E, 3/4, R

## キー変化データの入れ込み
- キー変化するタイミングを譜面から読み取り、設定していきます。
- キーの入れ替わりタイミングはアニメーションを予定しているため、その部分のキー数を`X`で定義しています。
```
|keych_data=0,11,7520,X,7540,11L,12879,X,12899,11F,13754,X,13925,9C|
//    0フレーム: 11keyモード
// 7520フレーム: (非表示)
// 7540フレーム: 11Lkeyモード
//12879フレーム: (非表示)
//12899フレーム: 11Fkeyモード　※11と11Lの上段を半分ずつ使用
//13754フレーム: (非表示)
//13925フレーム: 9Ckeyモード　 ※11と11Lの上段＋おにぎり
```
- 実際には7520フレームと12879フレームの箇所については下段の7key（、上段の11Lkeyの一部）を動かさないため、キー変化データを次のように見直しました。
```
|keych_data=0,11,7520,7,7540,11L,12879,11LL,12899,11F,13754,X,13925,9C|
//    0フレーム: 11keyモード
// 7520フレーム: (下段7keyを固定表示 ⇒キーグループ:7)
// 7540フレーム: 11Lkeyモード
//12879フレーム: (下段7key + 11Lkeyの上段左2つを固定表示 ⇒キーグループ:11LL)
//12899フレーム: 11Fkeyモード　※11と11Lの上段を半分ずつ使用 ⇒キーグループ:11F
//13754フレーム: (非表示)
//13925フレーム: 9Ckeyモード　 ※11と11Lの上段＋おにぎり
```
- この結果に合わせて、カスタムキー定義の**keyGroupX**を次のように修正しています。
- 新たに作成したキーグループ: 7, 11LL, 11Fを追加しています。
```
|keyGroup11D=11L/11LL/11F,11L/11LL/11F,11L,11L,11,11,11/11F,11/11F,7/11/11L/11LL/11F,7/11/11L/11LL/11F,7/11/11L/11LL/11F,7/11/11L/11LL/11F,7/11/11L/11LL/11F,7/11/11L/11LL/11F,7/11/11L/11LL/11F,9C,9C,9C,9C,9C,9C,9C,9C,9C|
```
<img src="https://user-images.githubusercontent.com/44026291/219977493-da70a936-17cb-4ee4-9089-9cff68a8dcc4.png" width="100%">


- ただ、新たに追加したキーグループ: 7, 11LL, 11Fはキーコンフィグ上は重複するだけなので、キーコンフィグのリストから外します。この設定は、譜面側の **keyGroupOrder**を使用します。
- ver35.1.0からはカスタムキー定義側で**keyGroupOrderX**が使えるようになったので、そちらが便利です。
```
|keyGroupOrder11D=11,11L,9C| // 11key, 11Lkey, 9Ckey(11key, 11Lkeyの上段部分+おにぎり)モードのみ表示
```

## アニメーションデータの追加
- ベースとなる設定が終わったので、キー変化を繋ぐ部分を`mask_data`を使って作成します。
- 最初の11keyから11Lkeyの変化では、次のような横移動と回転があります。

<img src="https://user-images.githubusercontent.com/44026291/219978128-26abfd52-a8b7-42df-875d-a30d607de4a8.png" width="50%">

- cssアニメーションで、これらの4パターンのアニメーションを作成します。  
矢印のオブジェクトはそれ自体に回転が掛かっているため、`translateX`で移動した後`rotate`で回転させます。
- ここでは最初と最後で半周分の回転が掛かるため、移動した後回転が掛かるように設定しています。
```css
@keyframes slide1-0 {
	0% {
		transform: translateX(0) rotate(0deg);
	}
	
	100% {
		transform: translateX(55px) rotate(180deg); /* 55px右へ移動し180度回転 */
	}
}
@keyframes slide1-1 {
	0% {
		transform: translateX(0) rotate(-90deg);
	}

	100% {
		transform: translateX(-55px) rotate(90deg); /* 55px左へ移動し180度回転 */
	}
}
@keyframes slide1-2 {
	0% {
		transform: translateX(0) rotate(90deg);
	}
	
	100% {
		transform: translateX(-165px) rotate(270deg); /* 165px左へ移動し180度回転 */
	}
}
@keyframes slide1-3 {
	0% {
		transform: translateX(0) rotate(180deg);
	}
	
	100% {
		transform: translateX(-275px) rotate(360deg); /* 275px左へ移動し180度回転 */
	}
}
```
- 次に、このアニメーションを`mask_data`へ追加します。  
矢印そのものを利用するため、色付きオブジェクトを使用します。
- 座標部分は次のように数式にします。横幅の変更、スクロール方向の変更に強くほぼそのまま利用できるのが利点です。

|種別|計算式（ステップゾーン位置）|
|----|----|
|X座標|`{g_workObj.stepX[j]}`|
|Y座標|`{g_posObj.stepY + g_posObj.reverseStepY * g_workObj.dividePos[j]}`|

- `g_workObj.stepX[j]`がステップゾーンのX座標、`g_posObj.stepY`がステップゾーン(上段)のY座標、  
`g_posObj.reverseStepY`がステップゾーン(上段/下段)間の距離を表しています。  
`g_workObj.dividePos[j]`は通常時は`0`、リバース時は`1`が入ります。  
- `mask_data`の書式に沿って、上で定義したcssアニメーション「slide1-0」～「slide1-3」を反映すると次のように書けます。  
ここでは20フレームで11Lkeyの上段にステップゾーンが来るように調整しました。  
※`[j]`の部分は0番目を開始位置として、**charaX** に対応した位置が入ります。今回、11key上段は 4～7番目に入れたので`[4]～[7]`を割り当てています。
- 7520フレームでアニメーションを出し、7540フレームで(11Lkeyのステップゾーンを表示するため)アニメーションを全て消去しています。
```
|mask_data=
7520,-,■11⇒(7⇒)11L
7520,-,□11key上段(4～7)を11Lkey上段(0～3)へ移動
7520,0,[c]arrow/#ccccff,,{g_workObj.stepX[4]},{g_posObj.stepY + g_posObj.reverseStepY * g_workObj.dividePos[4]},50,50,1,slide1-0,20,forwards
7520,1,[c]arrow:-90/#ccccff,,{g_workObj.stepX[5]},{g_posObj.stepY + g_posObj.reverseStepY * g_workObj.dividePos[5]},50,50,1,slide1-1,20,forwards
7520,2,[c]arrow:90/#ccccff,,{g_workObj.stepX[6]},{g_posObj.stepY + g_posObj.reverseStepY * g_workObj.dividePos[6]},50,50,1,slide1-2,20,forwards
7520,3,[c]arrow:180/#ccccff,,{g_workObj.stepX[7]},{g_posObj.stepY + g_posObj.reverseStepY * g_workObj.dividePos[7]},50,50,1,slide1-3,20,forwards
7540,ALL
|
```
- 後は同じようにアニメーションを作っていけばアニメーション部分が作成可能です。

## ページ作成者
- ティックル

## 関連項目
- [キー数仕様](./keys)
- [背景・マスクモーション (back_data, mask_data)](./dos-e0004-animationData)
- [キー数変化](./dos-e0006-keychData)
- [keyGroupOrder](./dos-h0092-keyGroupOrder) (◇) - キーコンフィグで設定可能な部分キーグループの設定

[^ Tips Indexに戻る](./tips-index)

| < [既存キーのキーパターン上書き](./tips-0006-keypattern-update) | **キー変化作品の実装例** | [カスタムキーで独自のReverseを設定](./tips-0011-original-reverse) > |
