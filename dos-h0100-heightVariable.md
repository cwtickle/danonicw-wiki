**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-h0100-heightVariable) | Japanese** 

[Home](./) &gt; [譜面ヘッダー仕様](dos_header) &gt; [クレジット関連、共通設定](dos_header#-クレジット関連共通設定)

| [<- windowHeight](dos-h0097-windowHeight) | **heightVariable** | [windowAlign ->](dos-h0091-windowAlign) |

## heightVariable
- 画面ウィンドウの高さ可変設定
- 共通設定 ⇒ [g_presetObj.heightVariable](dos-s0001-makerInfo#高さ可変設定-g_presetobjheightvariable)

### 使い方
```
|heightVariable=true|
```
### 説明
この作品の画面ウィンドウの高さをパラメータ`h`にて制御できるようにするかを設定します。デフォルトは`false`(制御しない)です。   
パラメータ`h`の制御範囲は400px～1000pxです。  
500px以下を指定した場合は高さは500pxとなり、プレイ画面のサイズが指定された値に縮小されます。

|値|既定|内容|
|----|----|----|
|false|*|パラメータ`h`を無効化する|
|true||パラメータ`h`を有効化する<br>（パラメータで指定した値に画面が引き伸ばしされます）|

### 関連項目
- [**windowHeight**](dos-h0097-windowHeight) [:pencil:](dos-h0097-windowWidth/_edit) 画面ウィンドウの高さ
- [playingWidth / playingHeight](dos-h0071-playingWidth) [:pencil:](dos-h0071-playingWidth/_edit) ゲーム表示エリアの横幅・高さ

### 更新履歴

|Version|変更内容|
|----|----|
|[v35.0.0](https://github.com/cwtickle/danoniplus/releases/tag/v35.0.0)|・初回実装|

| [<- windowHeight](dos-h0097-windowHeight) | **heightVariable** | [windowAlign ->](dos-h0091-windowAlign) |