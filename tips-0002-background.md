**[English](https://github.com/cwtickle/danoniplus-docs/wiki/tips-0002-background) | Japanese** 

[^ Tips Indexに戻る](./tips-index)

| < [スキップボタンの作成方法](./tips-0013-titleskip) | **背景の表示方法** | [画面の位置調整方法](./tips-0003-position-adjustment) > |

# 背景の表示方法
- 背景の表示方法はいくつかあります。

## 方法
### 1. スキン変更機能を利用した背景表示方法 (ver33以降)
- [スキンファイル仕様](./AboutSkin)の方法を利用します。  
方法は簡単で、以下を譜面ヘッダーに入れるだけです。
```
|--background=#000000:#222222|  // 背景色を指定する場合
|--background=url("(..)back123.png")| // 同じフォルダにある"back123.png"を背景に適用
```
- プレイ画面のみ背景を変えたい場合は、[スキン変更機能](./dos-e0008-styleData)を使用します。
```
|style_data=
0,--background,#000000:#222222
|
```

#### 補足1: layer0のcanvasタグがhtmlページ内にある場合
- 2.にもあるように、`<div id="canvas-frame">`の配下に`<canvas id="layer0" ...>`がある場合は動作しません。
- 共通化の影響で外せない場合、ver33.3.0以降の場合は [bgCanvasUse](./dos-h0094-bgCanvasUse) を `false`に設定すれば対処可能です。
- それ以前のバージョンの場合、デフォルトスキンを変更することで対処します。
`/skin`フォルダにある「danoni_skin_default.css」をコピーして「danoni_skin_default_add.css」を作成してください。  
その上で、作品ページ内の譜面の中に`|skinType=default_add|`を指定すれば反映されるようになります。  

#### 補足2: 補足1で、スキンを変更することで対処できる理由
- デフォルトスキン時のみ、canvas背景を優先する設定になっているためです。  
`default`という文字を含まない別の名前のスキンを作成することで、canvas背景を参照せず、スキン優先にすることができます。

### 2. 背景スキンを利用した背景表示方法
- ここでは、スキン名を「**backgroundTest**」とします。  
この場合、「skin」フォルダに「danoni_skin_**backgroundTest**.css」  
というファイルを作る必要があります。  

- 手軽に実装するため、今回は「skin」フォルダにある  
「danoni_skin_background.css」を利用します。  
「danoni_skin_background.css」をコピーして、  
「danoni_skin_**backgroundTest**.css」を作成してください。

- 「danoni_skin_**backgroundTest**.css」を開いて、background-imageの行のコメントを外します。  
背景ファイルのあるパスを指定しておきます。  
下記は、「background」フォルダを新規に作成し、  
その中に「back123.png」という名前の背景画像を配置する例です。
```css
@charset "UTF-8";
/* 背景 */
#divBack {
	background: linear-gradient(#000000, #222222); /* 背景が使用できない場合の代替 */
	background-image: url("../background/back123.png");
}
```

- 次に、譜面ファイル側でこの背景スキンを利用するための設定を行います。
譜面データ側で、譜面ヘッダー「skinType」の2要素目に、今回作成した背景スキンを指定してください。
```
|skinType=default,backgroundTest|
```
- またこのとき、canvas要素を入れないでください。
canvas要素を入れないことで、背景用div要素(divBack)が適用されるようになります。
```html
<div id="canvas-frame" style="width:600px;">
   <!-- <canvas id="layer0" width="600" height="500"></canvas> コメントアウト -->
   <!-- <canvas id="layer1" width="600" height="500"></canvas> コメントアウト -->
</div>
```

### 3. 譜面データ（back_data）を利用した背景表示方法
- プレイ画面のみ背景を表示したい場合に使用する方法です。  
「background」フォルダを新規に作成し、その中に「back123.png」という名前の背景画像を配置する例です。
```
|back_data=
0,0,../background/back123.png,,0,0,600,500
|
```
- この場合、背景サイズをゲームの縦横サイズ（この場合は600x500）  
に合わせる必要があります。  
（画像の拡大・縮小が行われるため）

## 動作確認バージョン
- 1.はv33以降、2, 3.はv11以降で使用できます。

## ページ作成者
- ティックル

## 関連項目
- [譜面の作成概要](HowtoMake)
- [**skinType**](dos-h0054-skinType) [:pencil:](dos-h0054-skinType/_edit) スキン設定
- [背景・マスクモーション (back_data, mask_data)](dos-e0004-animationData)

[^ Tips Indexに戻る](./tips-index)

| < [スキップボタンの作成方法](./tips-0013-titleskip) | **背景の表示方法** | [画面の位置調整方法](./tips-0003-position-adjustment) > |
