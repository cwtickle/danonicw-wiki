**[English](https://github.com/cwtickle/danoniplus-docs/wiki/KeyCtrlCodeList) | Japanese** 

| [< キーの仕様について](./keys) | **KeyCtrl属性で使用するキーコード** | [実行時エラー一覧 >](./ErrorList) |

# KeyCtrl属性で使用するキーコード
- [キー数仕様](./keys)のカスタムキー定義の **keyCtrlX** で使用する  
キーコード(KeyCode)と[KeyboardEvent.code](https://developer.mozilla.org/ja/docs/Web/API/KeyboardEvent/code)及び略記、JIS/USキーボードでの対応キーの対応表です。
- キーコンフィグ画面上のキー表示はタイトルの言語設定で変わります。(Ja⇒JIS, En⇒US)
- キーコードは基本的に`KeyboardEvent.keyCode` (Windows, JIS) の値に準拠していますが、一部拡張しています。

|KeyCode|Code略名|[KeyboardEvent.code](https://developer.mozilla.org/ja/docs/Web/API/KeyboardEvent/code)|キー名(JIS)|キー名(US)|
|----|----|----|----|----|
|0|-|-|(キー無効)|(キー無効)|
|1|-|-|Unknown<br>※割り当て不可|Unknown<br>※割り当て不可|
|8|Backspace|Backspace|`Backspace`<br>(Macでは`Delete`)|`Backspace`<br>(Macでは`Delete`)|
|9|Tab|Tab|`Tab`|`Tab`|
|12|Clear|Clear|`Clear`|`Clear`|
|13|Enter|Enter|`Enter`|`Enter`|
|16|Shift<br>ShiftLeft|ShiftLeft|左`Shift`|左`Shift`|
|17|Control<br>ControlLeft|ControlLeft|左`Ctrl`|左`Ctrl`|
|18|Alt<br>AltLeft|AltLeft|左`Alt`|左`Alt`|
|19|Pause|Pause|`Pause`|`Pause`|
|27|Escape|Escape|`Esc`|`Esc`|
|28|Convert|Convert|`変換`|ー|
|29|NonConvert|NonConvert|`無変換`|ー|
|32|Space|Space|`スペース`|`スペース`|
|33|PageUp|PageUp|`PageUp`|`PageUp`|
|34|PageDown|PageDown|`PageDown`|`PageDown`|
|35|End|End|`End`|`End`|
|36|Home|Home|`Home`|`Home`|
|37|Left<br>ArrowLeft|ArrowLeft|`←`|`←`|
|38|Up<br>ArrowUp|ArrowUp|`↑`|`↑`|
|39|Right<br>ArrowRight|ArrowRight|`→`|`→`|
|40|Down<br>ArrowDown|ArrowDown|`↓`|`↓`|
|44|PrintScreen|PrintScreen|`PrintScreen`|`PrintScreen`|
|45|Insert|Insert|`Insert`|`Insert`|
|46|Delete|Delete|`Delete`<br>(Macでは未使用)|`Delete`<br>(Macでは未使用)|
|47|Help|Help|`Help`|`Help`|
|48|D0<br>Digit0|Digit0|`0`|`0 )`|
|49|D1<br>Digit1|Digit1|`1 !`|`1 !`|
|50|D2<br>Digit2|Digit2|`2 "`|`2 @`|
|51|D3<br>Digit3|Digit3|`3 #`|`3 #`|
|52|D4<br>Digit4|Digit4|`4 $`|`4 $`|
|53|D5<br>Digit5|Digit5|`5 %`|`5 %`|
|54|D6<br>Digit6|Digit6|`6 &`|`6 ^`|
|55|D7<br>Digit7|Digit7|`7 '`|`7 &`|
|56|D8<br>Digit8|Digit8|`8 (`|`8 *`|
|57|D9<br>Digit9|Digit9|`9 )`|`9 (`|
|65|A<br>KeyA|KeyA|`A`|`A`|
|66|B<br>KeyB|KeyB|`B`|`B`|
|67|C<br>KeyC|KeyC|`C`|`C`|
|68|D<br>KeyD|KeyD|`D`|`D`|
|69|E<br>KeyE|KeyE|`E`|`E`|
|70|F<br>KeyF|KeyF|`F`|`F`|
|71|G<br>KeyG|KeyG|`G`|`G`|
|72|H<br>KeyH|KeyH|`H`|`H`|
|73|I<br>KeyI|KeyI|`I`|`I`|
|74|J<br>KeyJ|KeyJ|`J`|`J`|
|75|K<br>KeyK|KeyK|`K`|`K`|
|76|L<br>KeyL|KeyL|`L`|`L`|
|77|M<br>KeyM|KeyM|`M`|`M`|
|78|N<br>KeyN|KeyN|`N`|`N`|
|79|O<br>KeyO|KeyO|`O`|`O`|
|80|P<br>KeyP|KeyP|`P`|`P`|
|81|Q<br>KeyQ|KeyQ|`Q`|`Q`|
|82|R<br>KeyR|KeyR|`R`|`R`|
|83|S<br>KeyS|KeyS|`S`|`S`|
|84|T<br>KeyT|KeyT|`T`|`T`|
|85|U<br>KeyU|KeyU|`U`|`U`|
|86|V<br>KeyV|KeyV|`V`|`V`|
|87|W<br>KeyW|KeyW|`W`|`W`|
|88|X<br>KeyX|KeyX|`X`|`X`|
|89|Y<br>KeyY|KeyY|`Y`|`Y`|
|90|Z<br>KeyZ|KeyZ|`Z`|`Z`|
|91|-|(MataLeft)|`Windows`<br>※割り当て不可|`Windows`<br>※割り当て不可|
|93|ContextMenu|ContextMenu|`Application`|`Application`|
|96|N0<br>Numpad0|Numpad0|テンキー`0`|テンキー`0`|
|97|N1<br>Numpad1|Numpad1|テンキー`1`|テンキー`1`|
|98|N2<br>Numpad2|Numpad2|テンキー`2`|テンキー`2`|
|99|N3<br>Numpad3|Numpad3|テンキー`3`|テンキー`3`|
|100|N4<br>Numpad4|Numpad4|テンキー`4`|テンキー`4`|
|101|N5<br>Numpad5|Numpad5|テンキー`5`|テンキー`5`|
|102|N6<br>Numpad6|Numpad6|テンキー`6`|テンキー`6`|
|103|N7<br>Numpad7|Numpad7|テンキー`7`|テンキー`7`|
|104|N8<br>Numpad8|Numpad8|テンキー`8`|テンキー`8`|
|105|N9<br>Numpad9|Numpad9|テンキー`9`|テンキー`9`|
|106|N*<br>NumpadMultiply|NumpadMultiply|テンキー`*`|テンキー`*`|
|107|N+<br>NumpadAdd|NumpadAdd|テンキー`+`|テンキー`+`|
|108|NEnter<br>NumpadEnter|NumpadEnter|テンキー`Enter`|テンキー`Enter`|
|109|N-<br>NumpadSubtract|NumpadSubtract|テンキー`-`|テンキー`-`|
|110|N.<br>NumpadDecimal|NumpadDecimal|テンキー`.`|テンキー`.`|
|111|NDiv<br>NumpadDivide|NumpadDivide|テンキー`/`|テンキー`/`|
|112|F1|F1|`F1`|`F1`|
|113|F2|F2|`F2`|`F2`|
|114|F3|F3|`F3`|`F3`|
|115|F4|F4|`F4`|`F4`|
|116|F5|F5|`F5`|`F5`|
|117|F6|F6|`F6`|`F6`|
|118|F7|F7|`F7`|`F7`|
|119|F8|F8|`F8`|`F8`|
|120|F9|F9|`F9`|`F9`|
|121|F10|F10|`F10`|`F10`|
|122|F11|F11|`F11`|`F11`|
|123|F12|F12|`F12`|`F12`|
|124|F13|F13|`F13`|`F13`|
|125|F14|F14|`F14`|`F14`|
|126|F15|F15|`F15`|`F15`|
|134|FN|FN|`FN`|`FN`|
|144|NumLock|NumLock|`NumLock`|`NumLock`|
|145|ScrollLock|ScrollLock|`ScrollLock`|`ScrollLock`|
|186|Quote<br>Ja-Colon|Quote|`: *`|`' "`|
|187|;<br>Semicolon|Semicolon|`; +`|`; :`|
|188|Comma|Comma|`, <`|`, <`|
|189|Minus|Minus|`- =`|`- _`|
|190|Period|Period|`. >`|`. >`|
|191|Slash|Slash|`/ ?`|`/ ?`|
|192|BracketLeft<br>Ja-@|BracketLeft|`` @ ` ``|`[ {`|
|219|BracketRight<br>Ja-[|BracketRight|`[ {`|`] }`|
|220|IntlYen|IntlYen|`\ \|`|ー|
|221|Backslash<br>Ja-]|Backslash|`] }`|`\ \|`|
|222|Equal<br>Ja-^|Equal|`^ ~`|`= +`|
|226|IntlRo|IntlRo|`\ _`|ー|
|229|-|Backquote|`IME`<br>※割り当て不可|`` ` ~``|
|240|-|(CapsLock)|`CapsLock`<br>※割り当て不可|`CapsLock`<br>※割り当て不可|
|256|ShiftRight|ShiftRight|右`Shift`|右`Shift`|
|257|ControlRight|ControlRight|右`Ctrl`|右`Ctrl`|
|258|AltRight|AltRight|右`Alt`|右`Alt`|

### 関連項目
- [キー数仕様](./keys)

### 更新履歴

|Version|変更内容|
|----|----|
|[v34.5.1](https://github.com/cwtickle/danoniplus/releases/tag/v34.5.1)|・キーコード: 260の利用を取り止め(キーコード: 256へ統合)|
|[v34.4.0](https://github.com/cwtickle/danoniplus/releases/tag/v34.4.0)|・Chrome系ブラウザの右シフト解釈変更対応（キーコード: 260を追加）|
|[v34.2.0](https://github.com/cwtickle/danoniplus/releases/tag/v34.2.0)|・タイトルの言語設定の切り替えにより、キーコンフィグ画面のキー表示をJIS/USキーボードに切り替える機能を追加|
|[v32.0.0](https://github.com/cwtickle/danoniplus/releases/tag/v32.0.0)|・Shift, Ctrl, Altキーの左右キーの割り当てを分離<br>・日本語キーボードと対応キーが異なるキー(Quote, BracketLeft, BracketRight, Backslash, Equal)について別名を追加|
|[v31.4.0](https://github.com/cwtickle/danoniplus/releases/tag/v31.4.0)|・キー割当設定について KeyboardEvent.code の値及び略名が使えるよう変更|
|[v16.0.0](https://github.com/cwtickle/danoniplus/releases/tag/v16.0.0)|・KeyBoardEvent.keyCode から KeyBoardEvent.code へ移行<br>(この時点で指定可能な値は従来のキーコードのみ)|

| [< キーの仕様について](./keys) | **KeyCtrl属性で使用するキーコード** | [実行時エラー一覧 >](./ErrorList) |
