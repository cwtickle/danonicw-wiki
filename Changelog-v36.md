**[English](https://github.com/cwtickle/danoniplus-docs/wiki/Changelog-v36) | Japanese**

[^ Back to All Version List (Security Policy)](https://github.com/cwtickle/danoniplus/security/policy)  
[^ Back to Update Info](./UpdateInfo#-v36-changelog)

[**<- v37**](./Changelog-v37) | **v36** | [**v35 ->**](./Changelog-v35)  
(**🔖 [23 Releases](https://github.com/cwtickle/danoniplus/releases?q=tag%3Av36)** )

## 🔃 Files changed (v36)

| Directory | FileName                                                                    |                                                                                            | Last Updated                                |
| --------- | --------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------ | ------------------------------------------- |
| /js       | danoni_main.js                                                              | [📥](https://github.com/cwtickle/danoniplus/releases/download/v36.6.11/danoni_main.js)      | **v36.6.11**                                 |
| /js/lib   | danoni_constants.js                                                         | [📥](https://github.com/cwtickle/danoniplus/releases/download/v36.6.6/danoni_constants.js) | [v36.6.6](./Changelog-v36#v3666-2024-11-04) |
| /css      | danoni_main.css                                                             | [📥](https://github.com/cwtickle/danoniplus/releases/download/v36.6.3/danoni_main.css)     | [v36.6.3](./Changelog-v36#v3663-2024-06-30) |
| /skin     | danoni_skin_default.css<br>danoni_skin_light.css<br>danoni_skin_skyblue.css | [📥](https://github.com/cwtickle/danoniplus/releases/download/v36.4.1/skin_css.zip)        | [v36.4.1](./Changelog-v36#v3641-2024-05-18) |

<details>
<summary>Changed file list before v35</summary>

| Directory | FileName                                                                                                                                               |                                                                                              | Last Updated                                |
| --------- | ------------------------------------------------------------------------------------------------------------------------------------------------------ | -------------------------------------------------------------------------------------------- | ------------------------------------------- |
| /js/lib   | danoni_localbinary.js                                                                                                                                  | [📥](https://github.com/cwtickle/danoniplus/releases/download/v15.1.0/danoni_localbinary.js) | [v15.1.0](./Changelog-v15#v1510-2020-05-21) |
| /img      | aaShadow.svg<br>arrow.svg<br>arrowShadow.svg<br>borderline.svg<br>c.svg<br>cursor.svg<br>giko.svg<br>iyo.svg<br>monar.svg<br>morara.svg<br>onigiri.svg | [📥](https://github.com/cwtickle/danoniplus/releases/download/v15.1.0/img.zip)               | [v15.1.0](./Changelog-v15#v1510-2020-05-21) |

</details>

⭐ New Features / 🛠️ Improvements / 🐞 Bug Fixes  
( Files changed: ⚪ danoni_main.js / 🔵 danoni_constants.js / 🔴 css files )

## [Warning] 更新終了後の不具合情報
- [更新を終了したバージョンの不具合情報#v36](./DeprecatedVersionBugs#v36) を参照

## v36.6.11 ([2025-02-01](https://github.com/cwtickle/danoniplus/releases/tag/v36.6.11))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v36.6.11/total)](https://github.com/cwtickle/danoniplus/archive/v36.6.11.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v36.6.11/support/v36?style=social)](https://github.com/cwtickle/danoniplus/compare/v36.6.11...support/v36#files_bucket)

- 🐞 **C)** HitPosition有効時にHidden+/Sudden+のフィルター位置がずれる問題を修正 ( PR [#1746](https://github.com/cwtickle/danoniplus/pull/1746) ) <- :boom: [**v31.0.0**](./Changelog-v31#v3100-2023-03-20)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v36.6.11)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v36.6.10...v36.6.11#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v36.6.11)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v36.6.11.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v36.6.11.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v36.6.11/js/danoni_main.js)
> / 🎣 [**v39.0.0**](./Changelog-v39#v3900-2025-02-01)

## v36.6.10 ([2025-01-28](https://github.com/cwtickle/danoniplus/releases/tag/v36.6.10))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v36.6.10/total)](https://github.com/cwtickle/danoniplus/archive/v36.6.10.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v36.6.10/support/v36?style=social)](https://github.com/cwtickle/danoniplus/compare/v36.6.10...support/v36#files_bucket)

- 🐞 **B)** フリーズアロー移動中のフリーズアロー長の計算誤りを修正 ( PR [#1744](https://github.com/cwtickle/danoniplus/pull/1744) ) <- :boom: **initial**
- 🐞 **B)** フリーズアローの末尾付近に矢印がいる場合の判定不具合を修正 ( PR [#1744](https://github.com/cwtickle/danoniplus/pull/1744) ) <- :boom: **initial**

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v36.6.10)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v36.6.9...v36.6.10#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v36.6.10)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v36.6.10.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v36.6.10.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v36.6.10/js/danoni_main.js)
> / 🎣 [**v38.3.1**](./Changelog-v38#v3831-2025-01-28)

## v36.6.9 ([2024-12-19](https://github.com/cwtickle/danoniplus/releases/tag/v36.6.9))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v36.6.9/total)](https://github.com/cwtickle/danoniplus/archive/v36.6.9.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v36.6.9/support/v36?style=social)](https://github.com/cwtickle/danoniplus/compare/v36.6.9...support/v36#files_bucket)

- 🐞 **C)** 色変化(ncolor_data)でキーパターンを変えると適用する矢印グループが想定と異なる問題を修正 ( PR [#1738](https://github.com/cwtickle/danoniplus/pull/1738) ) <- :boom: [**v36.0.0**](./Changelog-v36#v3600-2024-04-15)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v36.6.9)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v36.6.8...v36.6.9#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v36.6.9)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v36.6.9.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v36.6.9.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v36.6.9/js/danoni_main.js)
> / 🎣 [**v38.2.2**](./Changelog-v38#v3822-2024-12-19)

## v36.6.8 ([2024-12-07](https://github.com/cwtickle/danoniplus/releases/tag/v36.6.8))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v36.6.8/total)](https://github.com/cwtickle/danoniplus/archive/v36.6.8.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v36.6.8/support/v36?style=social)](https://github.com/cwtickle/danoniplus/compare/v36.6.8...support/v36#files_bucket)

- 🐞 **B)** 加算するモーションフレーム数の計算方法の誤りを修正 ( PR [#1734](https://github.com/cwtickle/danoniplus/pull/1734) ) <- :boom: [**v28.4.0**](./Changelog-v28#v2840-2022-10-20)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v36.6.8)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v36.6.7...v36.6.8#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v36.6.8)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v36.6.8.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v36.6.8.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v36.6.8/js/danoni_main.js)
> / 🎣 [**v38.1.2**](./Changelog-v38#v3812-2024-12-07)

## v36.6.7 ([2024-11-27](https://github.com/cwtickle/danoniplus/releases/tag/v36.6.7))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v36.6.7/total)](https://github.com/cwtickle/danoniplus/archive/v36.6.7.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v36.6.7/support/v36?style=social)](https://github.com/cwtickle/danoniplus/compare/v36.6.7...support/v36#files_bucket)

- 🛠️ リモート接続用サンプルHTMLをnetlifyに変更 ( PR [#1728](https://github.com/cwtickle/danoniplus/pull/1728) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v36.6.7)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v36.6.6...v36.6.7#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v36.6.7)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v36.6.7.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v36.6.7.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v36.6.7/js/danoni_main.js)
> / 🎣 [**v37.8.3**](./Changelog-v37#v3783-2024-11-27)

## v36.6.6 ([2024-11-04](https://github.com/cwtickle/danoniplus/releases/tag/v36.6.6))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v36.6.6/total)](https://github.com/cwtickle/danoniplus/archive/v36.6.6.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v36.6.6/support/v36?style=social)](https://github.com/cwtickle/danoniplus/compare/v36.6.6...support/v36#files_bucket)

- 🛠️ ハッシュタグの前後に半角スペースを追加 ( PR [#1718](https://github.com/cwtickle/danoniplus/pull/1718) )
- 🐞 **C)** フェードインで矢印が足りない場合でもクリアランプの点灯条件を通過してしまう問題を修正 ( PR [#1719](https://github.com/cwtickle/danoniplus/pull/1719) ) <- :boom: [**v36.2.0**](./Changelog-v36#v3620-2024-05-01)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v36.6.6)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v36.6.5...v36.6.6#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v36.6.6)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v36.6.6.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v36.6.6.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v36.6.6/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v36.6.6/js/lib/danoni_constants.js)
> / 🎣 [**v38.0.0**](./Changelog-v38#v3800-2024-11-04)

## v36.6.5 ([2024-09-25](https://github.com/cwtickle/danoniplus/releases/tag/v36.6.5))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v36.6.5/total)](https://github.com/cwtickle/danoniplus/archive/v36.6.5.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v36.6.5/support/v36?style=social)](https://github.com/cwtickle/danoniplus/compare/v36.6.5...support/v36#files_bucket)

- 🐞 **C)** ImgType で拡張子の指定がないときに、強制的に他の拡張子に変更する機能の問題を修正 ( PR [#1708](https://github.com/cwtickle/danoniplus/pull/1708) ) <- :boom: [**v23.1.0**](./Changelog-v23#v2310-2021-09-11)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v36.6.5)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v36.6.4...v36.6.5#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v36.6.5)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v36.6.5.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v36.6.5.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v36.6.5/js/danoni_main.js)
> / 🎣 [**v37.6.2**](./Changelog-v37#v3762-2024-09-25)

## v36.6.4 ([2024-08-14](https://github.com/cwtickle/danoniplus/releases/tag/v36.6.4))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v36.6.4/total)](https://github.com/cwtickle/danoniplus/archive/v36.6.4.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v36.6.4/support/v36?style=social)](https://github.com/cwtickle/danoniplus/compare/v36.6.4...support/v36#files_bucket)

- 🐞 **C)** 一部文字列の置換処理がおかしくなる問題を修正 ( PR [#1697](https://github.com/cwtickle/danoniplus/pull/1697) ) <- :boom: [**v27.0.0**](./Changelog-v27#v2700-2022-03-18)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v36.6.4)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v36.6.3...v36.6.4#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v36.6.4)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v36.6.4.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v36.6.4.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v36.6.4/js/danoni_main.js)
> / 🎣 [**v37.4.0**](./Changelog-v37#v3740-2024-08-14)

## v36.6.3 ([2024-06-30](https://github.com/cwtickle/danoniplus/releases/tag/v36.6.3))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v36.6.3/total)](https://github.com/cwtickle/danoniplus/archive/v36.6.3.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v36.6.3/support/v36?style=social)](https://github.com/cwtickle/danoniplus/compare/v36.6.3...support/v36#files_bucket)

- 🛠️ mask, background-clip についてベンダープレフィックスの互換設定を元に戻す ( PR [#1689](https://github.com/cwtickle/danoniplus/pull/1689) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v36.6.3)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v36.6.2...v36.6.3#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v36.6.3)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v36.6.3.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v36.6.3.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v36.6.3/js/danoni_main.js)[🔴](https://github.com/cwtickle/danoniplus/blob/v36.6.3/css/danoni_main.css)
> / 🎣 [**v37.2.1**](./Changelog-v37#v3721-2024-06-30)

## v36.6.2 ([2024-06-25](https://github.com/cwtickle/danoniplus/releases/tag/v36.6.2))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v36.6.2/total)](https://github.com/cwtickle/danoniplus/archive/v36.6.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v36.6.2/support/v36?style=social)](https://github.com/cwtickle/danoniplus/compare/v36.6.2...support/v36#files_bucket)

- 🐞 **C)** 速度変化平均（AvgO, AvgB）の表示が想定より高く出ることがある問題を修正 ( PR [#1685](https://github.com/cwtickle/danoniplus/pull/1685) ) <- :boom: [**v32.2.0**](./Changelog-v32#v3220-2023-05-21)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v36.6.2)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v36.6.1...v36.6.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v36.6.2)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v36.6.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v36.6.2.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v36.6.2/js/danoni_main.js)
> / 🎣 [**v37.1.0**](./Changelog-v37#v3710-2024-06-25)

## v36.6.1 ([2024-06-20](https://github.com/cwtickle/danoniplus/releases/tag/v36.6.1))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v36.6.1/total)](https://github.com/cwtickle/danoniplus/archive/v36.6.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v36.6.1/support/v36?style=social)](https://github.com/cwtickle/danoniplus/compare/v36.6.1...support/v36#files_bucket)

- 🐞 **C)** Reverse のときに歌詞表示の自動反転が効かない問題を修正 ( PR [#1679](https://github.com/cwtickle/danoniplus/pull/1679), [#1681](https://github.com/cwtickle/danoniplus/pull/1681) ) <- :boom: [**v30.0.1**](./Changelog-v30#v3001-2023-02-11)
- 🐞 **B)** ダミーフリーズアロー（部分オートノーツ）生成時に画面が止まってしまう問題を修正 ( PR [#1680](https://github.com/cwtickle/danoniplus/pull/1680) ) <- :boom: [**v36.0.0**](./Changelog-v36#v3600-2024-04-15)
- 🐞 **C)** ダミーフリーズアローヒット時に、開始矢印の塗りつぶし色が消えてしまう問題を修正 ( PR [#1680](https://github.com/cwtickle/danoniplus/pull/1680) ) <- :boom: [**v31.0.0**](./Changelog-v31#v3100-2023-03-20)
- 🐞 **C)** スキンがデフォルトのとき、ダミーフリーズアローの矢印の塗りつぶし色が矢印の枠色と同じになってしまう問題を修正 ( PR [#1680](https://github.com/cwtickle/danoniplus/pull/1680) ) <- :boom: [**v36.0.0**](./Changelog-v36#v3600-2024-04-15)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v36.6.1)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v36.6.0...v36.6.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v36.6.1)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v36.6.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v36.6.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v36.6.1/js/danoni_main.js)
> / 🎣 [**v37.0.1**](./Changelog-v37#v3701-2024-06-20)

---

## v36.6.0 ([2024-06-08](https://github.com/cwtickle/danoniplus/releases/tag/v36.6.0))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v36.6.0/total)](https://github.com/cwtickle/danoniplus/archive/v36.6.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v36.6.0/support/v36?style=social)](https://github.com/cwtickle/danoniplus/compare/v36.6.0...support/v36#files_bucket)

- ⭐ シャッフルグループ数の上限をキー毎に変えるよう変更 ( PR [#1672](https://github.com/cwtickle/danoniplus/pull/1672) )
- 🛠️ Mirror/X-Mirror のハイスコア保存条件の変更、関連メッセージの追加 ( PR [#1672](https://github.com/cwtickle/danoniplus/pull/1672) )
- 🛠️ シャッフル利用時、通常と異なるシャッフルグループを指定した場合の譜面名の表記を変更 ( PR [#1672](https://github.com/cwtickle/danoniplus/pull/1672) )
- 🛠️ X のポスト用 URL の変更 ( PR [#1671](https://github.com/cwtickle/danoniplus/pull/1671) )
- 🐞 **C)** Hudden+/Sudden+で HitPosition 変更時、フィルター用のバーと実際に隠れる位置がずれる問題を修正 ( PR [#1673](https://github.com/cwtickle/danoniplus/pull/1673) ) <- :boom: [**v31.0.0**](./Changelog-v31#v3100-2023-03-20)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v36.6.0)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v36.5.0...v36.6.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v36.6.0)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v36.6.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v36.6.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v36.6.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v36.6.0/js/lib/danoni_constants.js)

## v36.5.0 ([2024-06-05](https://github.com/cwtickle/danoniplus/releases/tag/v36.5.0))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v36.5.0/total)](https://github.com/cwtickle/danoniplus/archive/v36.5.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v36.5.0/support/v36?style=social)](https://github.com/cwtickle/danoniplus/compare/v36.5.0...support/v36#files_bucket)

- ⭐ シャッフル設定に矢印・フリーズアローが散らばる S-Random の設定（Scatter/Scatter+）を追加 ( PR [#1668](https://github.com/cwtickle/danoniplus/pull/1668) )
- 🛠️ 全押し中にフリーズアローがいた場合、S-Random ロード中にエラーで止まらないよう処理を改善 ( PR [#1668](https://github.com/cwtickle/danoniplus/pull/1668) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v36.5.0)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v36.4.2...v36.5.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v36.5.0)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v36.5.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v36.5.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v36.5.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v36.5.0/js/lib/danoni_constants.js)

## v36.4.2 ([2024-06-01](https://github.com/cwtickle/danoniplus/releases/tag/v36.4.2))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v36.4.2/total)](https://github.com/cwtickle/danoniplus/archive/v36.4.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v36.4.2/support/v36?style=social)](https://github.com/cwtickle/danoniplus/compare/v36.4.2...support/v36#files_bucket)

- 🐞 **B)** 色変化、歌詞表示、背景・マスクモーションにおいて全角空白や改行など想定外な文字まで自動除去してしまう問題を修正 ( PR [#1665](https://github.com/cwtickle/danoniplus/pull/1665) ) <- :boom: [**v36.0.0**](./Changelog-v36#v3600-2024-04-15)
- 🐞 **B)** 背景・マスクモーションで深度を空で指定したときに`[loop][jump]`コマンドがスキップされる問題を修正 ( PR [#1666](https://github.com/cwtickle/danoniplus/pull/1666) ) <- :boom: [**v36.3.0**](./Changelog-v36#v3630-2024-05-08)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v36.4.2)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v36.4.1...v36.4.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v36.4.2)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v36.4.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v36.4.2.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v36.4.2/js/danoni_main.js)

## v36.4.1 ([2024-05-18](https://github.com/cwtickle/danoniplus/releases/tag/v36.4.1))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v36.4.1/total)](https://github.com/cwtickle/danoniplus/archive/v36.4.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v36.4.1/support/v36?style=social)](https://github.com/cwtickle/danoniplus/compare/v36.4.1...support/v36#files_bucket)

- 🛠️ 結果画面の旧 Twitter、Gitter のリンクを X, Discord に変更 ( PR [#1661](https://github.com/cwtickle/danoniplus/pull/1661) )
- 🛠️ danoni_legacy_function.js の付属を取り止め ( PR [#1663](https://github.com/cwtickle/danoniplus/pull/1663) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v36.4.1)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v36.4.0...v36.4.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v36.4.1)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v36.4.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v36.4.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v36.4.1/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v36.4.1/js/lib/danoni_constants.js)[🔴](https://github.com/cwtickle/danoniplus/blob/v36.4.1/css/danoni_main.css)

## v36.4.0 ([2024-05-12](https://github.com/cwtickle/danoniplus/releases/tag/v36.4.0))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v36.4.0/total)](https://github.com/cwtickle/danoniplus/archive/v36.4.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v36.4.0/support/v36?style=social)](https://github.com/cwtickle/danoniplus/compare/v36.4.0...support/v36#files_bucket)

- 🛠️ 空押し設定（Excessive）が変更不可かつ有効のときに設定画面にその旨を表示するよう変更 ( PR [#1657](https://github.com/cwtickle/danoniplus/pull/1657) )
- 🛠️ 設定画面の各項目のオンマウスで下線が出るよう変更 ( PR [#1658](https://github.com/cwtickle/danoniplus/pull/1658) )
- 🛠️ 譜面明細画面の各ボタンのオンマウス説明文を追加 ( PR [#1658](https://github.com/cwtickle/danoniplus/pull/1658) )
- 🛠️ 譜面明細画面と譜面選択リストの組み合わせにより、ウィンドウの端が見えてしまう問題を見直し ( PR [#1658](https://github.com/cwtickle/danoniplus/pull/1658) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v36.4.0)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v36.3.2...v36.4.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v36.4.0)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v36.4.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v36.4.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v36.4.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v36.4.0/js/lib/danoni_constants.js)[🔴](https://github.com/cwtickle/danoniplus/blob/v36.4.0/css/danoni_main.css)

## v36.3.2 ([2024-05-11](https://github.com/cwtickle/danoniplus/releases/tag/v36.3.2))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v36.3.2/total)](https://github.com/cwtickle/danoniplus/archive/v36.3.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v36.3.2/support/v36?style=social)](https://github.com/cwtickle/danoniplus/compare/v36.3.2...support/v36#files_bucket)

- 🐞 **C)** キーコンフィグ画面のスクロールバーが出ないことがある問題を修正 ( PR [#1655](https://github.com/cwtickle/danoniplus/pull/1655) ) <- :boom: [**v36.3.0**](./Changelog-v36#v3630-2024-05-08)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v36.3.2)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v36.3.1...v36.3.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v36.3.2)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v36.3.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v36.3.2.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v36.3.2/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v36.3.2/js/lib/danoni_constants.js)

## v36.3.1 ([2024-05-09](https://github.com/cwtickle/danoniplus/releases/tag/v36.3.1))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v36.3.1/total)](https://github.com/cwtickle/danoniplus/archive/v36.3.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v36.3.1/support/v36?style=social)](https://github.com/cwtickle/danoniplus/compare/v36.3.1...support/v36#files_bucket)

- 🛠️ ncolor_data の対象「all」についてグループ指定を最大 50 個まで拡張 ( PR [#1653](https://github.com/cwtickle/danoniplus/pull/1653) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v36.3.1)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v36.3.0...v36.3.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v36.3.1)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v36.3.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v36.3.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v36.3.1/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v36.3.1/js/lib/danoni_constants.js)

## v36.3.0 ([2024-05-08](https://github.com/cwtickle/danoniplus/releases/tag/v36.3.0))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v36.3.0/total)](https://github.com/cwtickle/danoniplus/archive/v36.3.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v36.3.0/support/v36?style=social)](https://github.com/cwtickle/danoniplus/compare/v36.3.0...support/v36#files_bucket)

- ⭐ カラーグループ数を 5 から 10 に拡張、カラーピッカーで設定した色をリセットする機能を実装 ( PR [#1650](https://github.com/cwtickle/danoniplus/pull/1650) )
- ⭐ 一時的に変更できるシャッフルグループ数を 10 から 20 に変更 ( PR [#1650](https://github.com/cwtickle/danoniplus/pull/1650) )
- 🛠️ コードの整理 ( PR [#1650](https://github.com/cwtickle/danoniplus/pull/1650), [#1651](https://github.com/cwtickle/danoniplus/pull/1651) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v36.3.0)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v36.2.1...v36.3.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v36.3.0)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v36.3.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v36.3.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v36.3.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v36.3.0/js/lib/danoni_constants.js)

<img src="https://github.com/cwtickle/danoniplus/assets/44026291/6f673706-38ab-44c8-bd60-cfedc793ac27" width="50%"><img src="https://github.com/cwtickle/danoniplus/assets/44026291/45168347-0dac-48a8-87e9-ad903ffc4c78" width="50%">

## v36.2.1 ([2024-05-02](https://github.com/cwtickle/danoniplus/releases/tag/v36.2.1))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v36.2.1/total)](https://github.com/cwtickle/danoniplus/archive/v36.2.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v36.2.1/support/v36?style=social)](https://github.com/cwtickle/danoniplus/compare/v36.2.1...support/v36#files_bucket)

- 🐞 **B)** Accuracy が計算外のときに結果画面でエラーになることがある問題を修正 ( PR [#1648](https://github.com/cwtickle/danoniplus/pull/1648) ) <- :boom: [**v36.2.0**](./Changelog-v36#v3620-2024-05-01)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v36.2.1)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v36.2.0...v36.2.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v36.2.1)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v36.2.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v36.2.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v36.2.1/js/danoni_main.js)

## v36.2.0 ([2024-05-01](https://github.com/cwtickle/danoniplus/releases/tag/v36.2.0))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v36.2.0/total)](https://github.com/cwtickle/danoniplus/archive/v36.2.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v36.2.0/support/v36?style=social)](https://github.com/cwtickle/danoniplus/compare/v36.2.0...support/v36#files_bucket)

- ⭐ 譜面明細画面にハイスコア表示を実装 ( PR [#1644](https://github.com/cwtickle/danoniplus/pull/1644), [#1646](https://github.com/cwtickle/danoniplus/pull/1646) )
- ⭐ ハイスコアの Fast/Slow、ランク等の情報をローカルストレージデータに追加 ( PR [#1644](https://github.com/cwtickle/danoniplus/pull/1644) )
- 🛠️ キーコンフィグエラー部分の文言を見直し ( PR [#1645](https://github.com/cwtickle/danoniplus/pull/1645) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v36.2.0)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v36.1.0...v36.2.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v36.2.0)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v36.2.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v36.2.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v36.2.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v36.2.0/js/lib/danoni_constants.js)

<img src="https://github.com/cwtickle/danoniplus/assets/44026291/24ed5e06-891f-42a7-bd61-1be9836c831e" width="50%"><img src="https://github.com/cwtickle/danoniplus/assets/44026291/3ef6d42e-626e-488b-a840-f96b89c41add" width="50%">

## v36.1.0 ([2024-04-18](https://github.com/cwtickle/danoniplus/releases/tag/v36.1.0))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v36.1.0/total)](https://github.com/cwtickle/danoniplus/archive/v36.1.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v36.1.0/support/v36?style=social)](https://github.com/cwtickle/danoniplus/compare/v36.1.0...support/v36#files_bucket)

- ⭐ フリーズアローのヒット時における個別色変化の動作仕様を変更 ( PR [#1642](https://github.com/cwtickle/danoniplus/pull/1642) )
- 🛠️ ベンダープレフィックスの削除、画像プリロード方式の統一 ( PR [#1641](https://github.com/cwtickle/danoniplus/pull/1641), Issue [#1636](https://github.com/cwtickle/danoniplus/pull/1636) )
- 🛠️ 譜面ヘッダー「colorDataType」の廃止 ( PR [#1642](https://github.com/cwtickle/danoniplus/pull/1642) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v36.1.0)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v36.0.0...v36.1.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v36.1.0)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v36.1.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v36.1.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v36.1.0/js/danoni_main.js)[🔴](https://github.com/cwtickle/danoniplus/blob/v36.1.0/css/danoni_main.css)

## v36.0.0 ([2024-04-15](https://github.com/cwtickle/danoniplus/releases/tag/v36.0.0))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v36.0.0/total)](https://github.com/cwtickle/danoniplus/archive/v36.0.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v36.0.0/support/v36?style=social)](https://github.com/cwtickle/danoniplus/compare/v36.0.0...support/v36#files_bucket)

- ⭐ 色変化の新記述形式を実装 ( PR [#1638](https://github.com/cwtickle/danoniplus/pull/1638), Issue [#1637](https://github.com/cwtickle/danoniplus/pull/1637) )
- ⭐ 色変化(新仕様)において矢印・フリーズアローの塗りつぶし部分の変化に対応 ( PR [#1638](https://github.com/cwtickle/danoniplus/pull/1638) )
- ⭐ 色変化(新仕様)においてフリーズアローのレーン個別の色変化に対応 ( PR [#1638](https://github.com/cwtickle/danoniplus/pull/1638) )
- 🛠️ 色変化(新仕様)、歌詞表示、背景・マスクモーションについてタブ・前後の半角スペースを自動除去するよう変更 ( PR [#1638](https://github.com/cwtickle/danoniplus/pull/1638) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v36.0.0)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v35.5.0...v36.0.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v36.0.0)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v36.0.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v36.0.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v36.0.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v36.0.0/js/lib/danoni_constants.js)

[**<- v37**](./Changelog-v37) | **v36** | [**v35 ->**](./Changelog-v35)
