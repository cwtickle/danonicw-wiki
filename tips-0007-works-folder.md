**[English](https://github.com/cwtickle/danoniplus-docs/wiki/tips-0007-works-folder) | Japanese**

[^ Tips Indexに戻る](./tips-index)

| < [カスタムキーの省略形記法](./tips-0016-custom-key-abbreviation) || **作品ページを任意の場所に配置** | [WordPressを使って作品を公開](./tips-0008-wordpress) > |

# 作品ページを任意の場所に配置
- v19.4.1より、作品ページを任意のフォルダに配置できるようになりました。
- またv27.1.0より、cssファイルのリンク指定が不要になっています。
- `<head>`タグ内にある`<script>`タグの相対パス部分を修正すれば、他のフォルダでも起動できます。

## 使い方

### 使用例
- danoniplus/danoni/ に作品用htmlがある場合 (jsファイルは danoniplus/js/ にある)
```html
<script src="../js/danoni_main.js" charset="UTF-8">
```

- danoniplus/danoni/sub/ に作品用htmlがある場合 (jsファイルは danoniplus/js/ にある)
```html
<script src="../../js/danoni_main.js" charset="UTF-8">
```

- GitHub Pagesに配置しているjs/css/svgファイルを利用する場合
   - この方法を利用する場合、jsファイル、cssファイルなどの個別ファイルのアップロードが不要です。
   - 個別のjs/cssファイルを使用する場合には不向きです。(カレントディレクトリ指定を行えば一応可能)
```html
<script src="https://cwtickle.github.io/danoniplus/js/danoni_main.js" charset="UTF-8">
```

## 動作確認バージョン
- [v27.1.0](https://github.com/cwtickle/danoniplus/releases/tag/v27.1.0)以降で利用可能です。

## ページ作成者
- ティックル

[^ Tips Indexに戻る](./tips-index)

| < [カスタムキーの省略形記法](./tips-0016-custom-key-abbreviation) || **作品ページを任意の場所に配置** | [WordPressを使って作品を公開](./tips-0008-wordpress) > |
