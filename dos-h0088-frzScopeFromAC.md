**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-h0088-frzScopeFromAC) | Japanese** 

[Home](./) &gt; [譜面ヘッダー仕様](dos_header) &gt; [プレイ時の初期設定](dos_header#-プレイ時の初期設定) &gt; [矢印・フリーズアロー色の設定](dos_header#矢印フリーズアロー色の設定)

| [<- defaultFrzColorUse](dos-h0063-defaultFrzColorUse) | **frzScopeFromAC** || [playingX / playingY ->](dos-h0070-playingX) |

## frzScopeFromAC
- 矢印色変化をフリーズアロー色変化に自動適用する範囲の設定
- 共通設定 ⇒ [g_presetObj.frzScopeFromAC](dos-s0004-frzArrow#矢印色変化に対応してフリーズアロー色を追随する範囲の設定-g_presetobjfrzscopefromac)

### 使い方
```
|frzScopeFromAC=Normal|     // フリーズ通常時のみ追随する
|frzScopeFromAC=Normal,Hit| // フリーズ通常時、ヒット時双方追随する
|frzScopeFromAC=None|       // 追随しない（danoni_setting.jsの設定を上書きしたい場合にのみ指定）
```
### 説明
- `g_presetObj.frzColors`もしくは[defaultFrzColorUse](dos-h0063-defaultFrzColorUse)がfalse(矢印色優先) のときに使用できる設定です。
- 矢印色変化に対応してフリーズアロー色を追随する範囲を、「Normal（通常時）」「Hit（ヒット時）」で指定できます。  
未指定の場合はいずれにも追随しません。
- 両方指定する場合、カンマで区切って指定します。

|指定可能値|内容|
|----|----|
|Normal|フリーズアロー（通常時）の矢印・帯に対して適用する|
|Hit|フリーズアロー（ヒット時）の矢印・帯に対して適用する|
|None|何もしない。通常指定する必要は無いが、`g_presetObj.frzScopeFromAC`が設定されている状態でこの作品には適用したくない場合、入れておくことで無効にできる|

### 補足
- この設定は `danoni_setting.js`にも全体の設定として対応する項目があります。  
どちらも設定があった場合は、譜面ヘッダーの値(frzScopeFromAC)が優先されます。
```javascript
g_presetObj.frzScopeFromAC = [`Normal`]; // 通常時のみ追随
```

### 関連項目
- [**frzColor**](dos-h0004-frzColor) [:pencil:](dos-h0004-frzColor/_edit) フリーズアロー色
- [defaultFrzColorUse](dos-h0063-defaultFrzColorUse) [:pencil:](dos-h0063-defaultFrzColorUse/_edit) フリーズアロー初期色(frzColor)が未指定時の適用方法
- [共通設定ファイル仕様](dos_setting) &gt; [フリーズアロー設定](dos-s0004-frzArrow)

### 更新履歴

|Version|変更内容|
|----|----|
|[v25.5.1](https://github.com/cwtickle/danoniplus/releases/tag/v25.5.1)|・初回実装|

| [<- defaultFrzColorUse](dos-h0063-defaultFrzColorUse) | **frzScopeFromAC** || [playingX / playingY ->](dos-h0070-playingX) |