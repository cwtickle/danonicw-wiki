**[English](https://github.com/cwtickle/danoniplus-docs/wiki/AboutKeys-balance) | Japanese** 

[<- Home](./) || [運指型](AboutKeys-fingering) | [バランス型](AboutKeys-balance) | [指移動型](AboutKeys-finger-movement) | [スクラッチ型](AboutKeys-scratch) | [オリジナル](AboutKeys-customKeys)

# 多鍵の種類（バランス型）
- 対応キーに対し、指の位置がほぼ決まっているパターン。  
いわゆる崩しがないため、演奏感のある譜面が作りやすい傾向にあります。  
大階段が映えるのもこれらのキーの特徴です。  

## 7key
[ 難易度表 : [通常](http://dodl4.g3.xrea.com/normal7/) / [発狂](http://dodl4.g3.xrea.com/insane7/) ]

<img src="./wiki/keyimg/key7_01.png" width="40%">

<img src="./wiki/keys/key7_1.png" width="80%">

- おにぎりを中央に、左右に3つずつ並ぶキーが7keyです。  
おにぎりは階段の一部にも、5keyのような裏打ち、強調にも使用できるためいろんな使い方ができます。  
3つ押し以上の同時押しがきれいに嵌りやすく、使い方によって譜面を軽くも重くもできる幅広さがあります。  

## 9Akey [ダブルプレイ]
[ 難易度表 : [通常](http://dodl4.g3.xrea.com/normal9/) ]

<img src="./wiki/keyimg/key9A_01.png" width="50%">

<img src="./wiki/keys/key9A.png" width="80%">

- 7keyを拡張し、両方に上キーがくっついた形が9Akeyです。  
5keyを両手で使うキーにも取れるのでダブルプレイとも呼ばれます。  
デュエット曲など左右を意識した曲で使われることが多い傾向にあります。  

## 9Bkey
[ 難易度表 : [通常](http://dodl4.g3.xrea.com/normal9/) ]

<img src="./wiki/keys/key9B.png" width="80%">

7keyを横に拡張し、「A, +」キーがくっついた形が9Bkeyです。  
7keyよりもダイナミックな表現が可能で、ピアノ曲でよく使われる傾向にあります。  

## 7ikey
[ 難易度表 : [通常](http://dodl4.g3.xrea.com/normal7i/) ]

<img src="./wiki/keyimg/key7i_02.png" width="40%">
<img src="./wiki/keyimg/key7i_03.png" width="40%">

<img src="./wiki/keyimg/key7i_01.png" width="40%">

<img src="./wiki/keys/key7i.png" width="80%">

- 矢印キーに、ギコ・おにぎり・イヨウのキャラクタが「Z X C」に対応する変則7keyです。  
スペースキーを使わないために軽快な配置が可能で、横の広さから演奏感も出せるのが特徴です。  
ところによって5key、7keyのような使い分けができるのはこのキーの強みと言えるでしょう。  

## 9ikey
[ 多鍵DB : [9ikey](https://apoi108.sakura.ne.jp/danoni/ta/?keys=9i) ]

<img src="./wiki/keyimg/key9i_01.png" width="50%">

<img src="./wiki/keys/key9i.png" width="80%">

- 7iのキャラクタの部分が5つになったキーです。  
キャラクタと矢印のスクロールが逆になっており、左右を分離するような構成が映えそうなキー構成になっています。  

----

### Special Thanks
- 蒼宮あいす、decresc.

----

[<- Home](./) || [運指型](AboutKeys-fingering) | [バランス型](AboutKeys-balance) | [指移動型](AboutKeys-finger-movement) | [スクラッチ型](AboutKeys-scratch) | [オリジナル](AboutKeys-customKeys)

|種類|対応キー数|
|----|----|
|[運指型](AboutKeys-fingering)|[5key](AboutKeys-fingering#5key) / [12key](AboutKeys-fingering#12key) / [14key](AboutKeys-fingering#14key) / [11ikey](AboutKeys-fingering#11ikey) / [17key](AboutKeys-fingering#17key) / [23key](AboutKeys-fingering#23key)|
|[バランス型](AboutKeys-balance)|[7key](AboutKeys-balance#7key) / [9Akey[ダブルプレイ]](AboutKeys-balance#9akey-ダブルプレイ) / [9Bkey](AboutKeys-balance#9bkey) / [7ikey](AboutKeys-balance#7ikey) / [9ikey](AboutKeys-balance#9ikey)|
|[指移動型](AboutKeys-finger-movement)|[11key](AboutKeys-finger-movement#11key) / [11Lkey](AboutKeys-finger-movement#11lkey) / [13key[トリプルプレイ]](AboutKeys-finger-movement#13key-トリプルプレイ) / [15Akey](AboutKeys-finger-movement#15akey) / [15Bkey](AboutKeys-finger-movement#15Bkey) / [14ikey](AboutKeys-finger-movement#14ikey) / [16ikey](AboutKeys-finger-movement#16ikey)|
|[スクラッチ型](AboutKeys-scratch)|[8key](AboutKeys-scratch#8key) / [11Wkey](AboutKeys-scratch#11wkey)|
|[オリジナル](AboutKeys-customKeys)|(Various Keys)|