**[English](https://github.com/cwtickle/danoniplus-docs/wiki/Changelog-latest) | Japanese**

| [< 更新を終了したバージョンの不具合情報](./DeprecatedVersionBugs) | **Changelog** || [譜面ヘッダー仕様 >](./dos_header) |

[^ Back to All Version List (Security Policy)](https://github.com/cwtickle/danoniplus/security/policy)  
[^ Back to Update Info](./UpdateInfo#-v40-changelog)

| **v40** | [**v39 ->**](./Changelog-v39)  
(**🔖 [6 Releases](https://github.com/cwtickle/danoniplus/releases?q=tag%3Av40)** )

## 🔃 Files changed (v40)

| Directory | FileName            |                                                                                            | Last Updated |
| --------- | ------------------- | ------------------------------------------------------------------------------------------ | ------------ |
| /js       | danoni_main.js      | [📥](https://github.com/cwtickle/danoniplus/releases/download/v40.3.1/danoni_main.js)      | **v40.3.1**  |
| /js/lib   | danoni_constants.js | [📥](https://github.com/cwtickle/danoniplus/releases/download/v40.3.1/danoni_constants.js) | **v40.3.1**  |

<details>
<summary>Changed file list before v39</summary>

| Directory | FileName                                                                                                                                               |                                                                                              | Last Updated                                   |
| --------- | ------------------------------------------------------------------------------------------------------------------------------------------------------ | -------------------------------------------------------------------------------------------- | ---------------------------------------------- |
| /js/lib   | danoni_localbinary.js                                                                                                                                  | [📥](https://github.com/cwtickle/danoniplus/releases/download/v15.1.0/danoni_localbinary.js) | [v15.1.0](./Changelog-v15#v1510-2020-05-21)    |
| /skin     | danoni_skin_default.css<br>danoni_skin_light.css<br>danoni_skin_skyblue.css                                                                            | [📥](https://github.com/cwtickle/danoniplus/releases/download/v39.0.0/skin_css.zip)          | [v39.0.0](./Changelog-latest#v3900-2025-02-01) |
| /css      | danoni_main.css                                                                                                                                        | [📥](https://github.com/cwtickle/danoniplus/releases/download/v39.0.0/danoni_main.css)       | [v39.0.0](./Changelog-latest#v3900-2025-02-01) |
| /img      | aaShadow.svg<br>arrow.svg<br>arrowShadow.svg<br>borderline.svg<br>c.svg<br>cursor.svg<br>giko.svg<br>iyo.svg<br>monar.svg<br>morara.svg<br>onigiri.svg | [📥](https://github.com/cwtickle/danoniplus/releases/download/v15.1.0/img.zip)               | [v15.1.0](./Changelog-v15#v1510-2020-05-21)    |

</details>

⭐ New Features / 🛠️ Improvements / 🐞 Bug Fixes  
( Files changed: ⚪ danoni_main.js / 🔵 danoni_constants.js / 🔴 css files )

## v40.3.1 ([2025-03-10](https://github.com/cwtickle/danoniplus/releases/tag/v40.3.1))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v40.3.1/total)](https://github.com/cwtickle/danoniplus/archive/v40.3.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v40.3.1/master?style=social)](https://github.com/cwtickle/danoniplus/compare/v40.3.1...master#files_bucket)

- 🛠️ formatObject関数の処理を整理、速度変化・色変化の装飾追加 ( PR [#1811](https://github.com/cwtickle/danoniplus/pull/1811) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v40.3.1)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v40.3.0...v40.3.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v40.3.1)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v40.3.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v40.3.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v40.3.1/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v40.3.1/js/lib/danoni_constants.js)

## v40.3.0 ([2025-03-08](https://github.com/cwtickle/danoniplus/releases/tag/v40.3.0))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v40.3.0/total)](https://github.com/cwtickle/danoniplus/archive/v40.3.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v40.3.0/master?style=social)](https://github.com/cwtickle/danoniplus/compare/v40.3.0...master#files_bucket)

- ⭐ エディターのカスタムキー定義を自動生成する機能を追加 ( PR [#1809](https://github.com/cwtickle/danoniplus/pull/1809) )
- ⭐ 前提条件画面のオブジェクトに対してクリップボードへコピーする機能を追加 ( PR [#1809](https://github.com/cwtickle/danoniplus/pull/1809) )
- 🐞 **C)** キー名が英小文字から始まる場合にData Management画面で止まる問題を修正 ( PR [#1808](https://github.com/cwtickle/danoniplus/pull/1808) ) <- :boom: [**v39.5.0**](./Changelog-v39#v3950-2025-02-15)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v40.3.0)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v40.2.0...v40.3.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v40.3.0)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v40.3.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v40.3.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v40.3.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v40.3.0/js/lib/danoni_constants.js)

## v40.2.0 ([2025-03-07](https://github.com/cwtickle/danoniplus/releases/tag/v40.2.0))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v40.2.0/total)](https://github.com/cwtickle/danoniplus/archive/v40.2.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v40.2.0/master?style=social)](https://github.com/cwtickle/danoniplus/compare/v40.2.0...master#files_bucket)

- 🛠️ 前提条件画面からDataManagementへのリンクを追加 ( PR [#1804](https://github.com/cwtickle/danoniplus/pull/1804) )
- 🛠️ boolean型, scrollDirXの値についてformatObjectで色やスクロール方向を付けるよう変更 ( PR [#1805](https://github.com/cwtickle/danoniplus/pull/1805) )
- 🛠️ formatObject関数のループ対策用変数を削除 ( PR [#1805](https://github.com/cwtickle/danoniplus/pull/1805), [#1806](https://github.com/cwtickle/danoniplus/pull/1806) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v40.2.0)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v40.1.0...v40.2.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v40.2.0)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v40.2.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v40.2.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v40.2.0/js/danoni_main.js)

## v40.1.0 ([2025-03-02](https://github.com/cwtickle/danoniplus/releases/tag/v40.1.0))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v40.1.0/total)](https://github.com/cwtickle/danoniplus/archive/v40.1.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v40.1.0/master?style=social)](https://github.com/cwtickle/danoniplus/compare/v40.1.0...master#files_bucket)

- 🛠️ ローカル・特定ドメインにのみ常時デバッグモードにするよう変更 ( PR [#1799](https://github.com/cwtickle/danoniplus/pull/1799), [#1800](https://github.com/cwtickle/danoniplus/pull/1800) )
- 🛠️ 新規画面追加時に、g_btnWaitFrameやg_btnPatternsを必須にしないよう変更 ( PR [#1801](https://github.com/cwtickle/danoniplus/pull/1801) )
- 🛠️ キーコンフィグ保存時にコピーする元の配列が未定義の場合、それがコピーされないよう見直し ( PR [#1802](https://github.com/cwtickle/danoniplus/pull/1802) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v40.1.0)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v40.0.1...v40.1.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v40.1.0)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v40.1.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v40.1.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v40.1.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v40.1.0/js/lib/danoni_constants.js)

## v40.0.1 ([2025-03-01](https://github.com/cwtickle/danoniplus/releases/tag/v40.0.1))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v40.0.1/total)](https://github.com/cwtickle/danoniplus/archive/v40.0.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v40.0.1/master?style=social)](https://github.com/cwtickle/danoniplus/compare/v40.0.1...master#files_bucket)

- 🐞 **B)** g_keyObjの自動削除処理でdivMax, keyGroupOrderが消えてしまう問題を修正 ( PR [#1797](https://github.com/cwtickle/danoniplus/pull/1797) ) <- :boom: [**v40.0.0**](./Changelog-latest#v4000-2025-03-01)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v40.0.1)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v40.0.0...v40.0.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v40.0.1)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v40.0.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v40.0.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v40.0.1/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v40.0.1/js/lib/danoni_constants.js)

## v40.0.0 ([2025-03-01](https://github.com/cwtickle/danoniplus/releases/tag/v40.0.0))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v40.0.0/total)](https://github.com/cwtickle/danoniplus/archive/v40.0.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v40.0.0/master?style=social)](https://github.com/cwtickle/danoniplus/compare/v40.0.0...master#files_bucket)

- ⭐ デバッグ用に主要オブジェクトの中身を表示する画面を実装 ( PR [#1790](https://github.com/cwtickle/danoniplus/pull/1790), [#1794](https://github.com/cwtickle/danoniplus/pull/1794), [#1795](https://github.com/cwtickle/danoniplus/pull/1795) )
- ⭐ カスタムキー定義を複数定義できるよう変更 ( PR [#1788](https://github.com/cwtickle/danoniplus/pull/1788) )
- ⭐ 未使用の`g_keyObj`プロパティをタイトル表示後に削除するよう変更 ( PR [#1792](https://github.com/cwtickle/danoniplus/pull/1792), [#1795](https://github.com/cwtickle/danoniplus/pull/1795) )
- 🛠️ Data Management画面で表示するローカルストレージ情報をキー名順にソートするよう変更 ( PR [#1789](https://github.com/cwtickle/danoniplus/pull/1789) )
- 🛠️ Data Management画面で、キー別ストレージ表示と削除対象ボタンを分離 ( PR [#1795](https://github.com/cwtickle/danoniplus/pull/1795) )
- 🛠️ Motion別のフレーム毎速度の配列長を必要以上に取らないよう見直し ( PR [#1791](https://github.com/cwtickle/danoniplus/pull/1791) )
- 🛠️ ロード時関数でMotion別のフレーム毎速度の配列を引数から削除 ( PR [#1791](https://github.com/cwtickle/danoniplus/pull/1791) )
- 🛠️ formatObjectのフォーマットを変更し、色データのときにダブルクォートで囲わないよう変更 ( PR [#1790](https://github.com/cwtickle/danoniplus/pull/1790) )
- 🐞 **C)** formatObject関数で意図しない[Circular]が表示される問題を修正 ( PR [#1792](https://github.com/cwtickle/danoniplus/pull/1792) ) <- :boom: [**v39.6.0**](./Changelog-v39#v3960-2025-02-16)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v40.0.0)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v39.8.2...v40.0.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v40.0.0)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v40.0.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v40.0.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v40.0.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v40.0.0/js/lib/danoni_constants.js)

| **v40** | [**v39 ->**](./Changelog-v39)

| [< 更新を終了したバージョンの不具合情報](./DeprecatedVersionBugs) | **Changelog** || [譜面ヘッダー仕様 >](./dos_header) |
