
[^ Tips Indexに戻る](./tips-index)

| < [作品ページを任意の場所に配置](./tips-0007-works-folder) | **WordPressを使って作品を公開** | [複数バージョンのファイル切り替え](./tips-0018-multiple-versions) > |

# WordPressを使って作品を公開
- WordPress上のページに公開したい場合は、適用中のテーマにjsファイルの読み込み処理を追加します。
- 適用中のテーマに設定を追加するため、テーマを変更した場合は再設定が必要です。
- 作品ページ以外はアップロードが終わっている前提です。

## 使い方

### 1. 全体の設定（初回のみ、テーマ変更時は再設定が必要）
- WordPressにログインし、「外観」→「テーマファイルエディター」を選択します。
<img src="https://user-images.githubusercontent.com/44026291/179139626-ff21d22e-cd6e-4925-afd9-55edfd1a9bbe.png" width="90%">


- テーマのための関数 (functions.php) を選択します。
<img src="https://user-images.githubusercontent.com/44026291/179140310-3bb7586a-14eb-43ca-87f0-e6fa183b83dd.png" width="90%">


- 以下のように danoni_main.js を読み込む文を追加して、「ファイルの更新」を押します（xxxの場所は自身のサイトを指定）。
```php
wp_enqueue_script('Main-Script','https://xxxx/danoni/js/danoni_main.js');
```
<img src="https://user-images.githubusercontent.com/44026291/179141381-eb9eefe9-0d14-4aaa-b8fc-64d9a86aabcf.png" width="90%">

### 2. 個別ページ作成
- 投稿 -> 新規追加からページを作成します。
<img src="https://user-images.githubusercontent.com/44026291/179142352-e2c8ed1a-103f-4080-a326-b19b784e6f68.png" width="90%">

- 「+」ボタンを押して、「カスタムHTML」を選択してください。
<img src="https://user-images.githubusercontent.com/44026291/179142870-0f543926-1d85-426a-bc5b-bd06f99a023e.png" width="50%">

- 編集画面上に下記をコピー＆ペーストします。
```html
<!--more-->

<figure class="wp-block-table"><table><tbody><tr><td>

<!--作品挿入箇所-->

</td></tr></tbody></table></figure>
```

- 譜面本体を、`<!--作品挿入箇所-->`の部分に貼り付けます。
<img src="https://user-images.githubusercontent.com/44026291/179143608-c6dcab5a-778f-44bc-9fe7-5a6dac419834.png" width="80%">

- プレビューもしくは公開するとページが表示されます。
<img src="https://user-images.githubusercontent.com/44026291/179143879-688a9dd0-cac7-4bee-9736-fe4bb3ed48d0.png" width="80%">

### 補足
- WordPressの設定によっては折り返しが利いてしまい画面が崩れることがあります。  
その場合は、`<div id="canvas-frame"></div>`の部分を下記に置き換えてみてください。
- ver28.2.1以降はcss側に同様の記述があるため、`style="white-space: nowrap;"`の記載は不要です。
```html
<div id="canvas-frame" style="white-space: nowrap;"></div>
```

## 動作確認バージョン
- [v27.1.0](https://github.com/cwtickle/danoniplus/releases/tag/v27.1.0)以降で利用可能です。

## ページ作成者
- ティックル

## 関連項目
- [作品データのWeb公開方法](HowToFileUpload)

[^ Tips Indexに戻る](./tips-index)

| < [作品ページを任意の場所に配置](./tips-0007-works-folder) | **WordPressを使って作品を公開** | [複数バージョンのファイル切り替え](./tips-0018-multiple-versions) > |
