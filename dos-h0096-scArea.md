**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-h0096-scArea) | Japanese** 

[Home](./) &gt; [譜面ヘッダー仕様](dos_header) &gt; [プレイ時の初期設定](dos_header#-プレイ時の初期設定) &gt; [プレイ画面位置の設定](dos_header#プレイ画面位置の設定)

| [<- customCreditWidth](dos-h0083-customCreditWidth) | **scArea** | [stepY ->](dos-h0014-stepY) |

## scArea
- ショートカットキー表示の横幅拡張設定
- 共通設定 ⇒ [g_presetObj.scAreaWidth](./dos-s0007-viewControl#ショートカットキーエリアの横幅拡張設定-g_presetobjscareawidth), [g_presetObj.playingLayout](./dos-s0007-viewControl#プレイ画面の表示レイアウト-g_presetobjplayinglayout)

### 使い方
```
|scArea=80|             // プレイ画面中のみ左右に80px拡張（＝160px拡張）
|scArea=80,center|      // 上記と同じ設定（正式）
|scArea=80,left|        // プレイ画面中のみ右に80px拡張（＝80px拡張、左寄せレイアウト用）
```
### 説明
プレイ画面でショートカットキー表示を行うために**拡張する幅**をpx単位で指定できます。  
デフォルトは0pxです。ショートカットキー表示は既定（リトライ: BackSpace, タイトルバック: Delete）と異なる場合に表示します。  
2つ目の設定を行うことで、拡張幅を左右に拡張するか、右側だけを拡張するかを設定できます。

|番号|設定例<br>(既定値)|内容|
|----|----|----|
|1|80<br>(0)|ショートカットキー表示用の拡張幅(px単位)|
|2|left<br>(center)|プレイ画面レイアウト<br>　left: 左寄せ (右側のみ拡張)<br>　center: 中央寄せ (左右両方に拡張)|

#### 中央表示での拡張レイアウト（左右に拡張）
```
|scArea=80|
```
<img src="https://github.com/cwtickle/danoniplus/assets/44026291/81dff85c-1c74-488b-b670-6ccc26c429b1" width="70%">

#### 左寄せ時の拡張レイアウト（右のみ拡張）
```
|scArea=80,left|
```
<img src="https://github.com/cwtickle/danoniplus/assets/44026291/c24c3c77-20a2-4069-b59e-2806a3ad5fb4" width="60%">

#### 左右拡張時の歌詞表示サンプル
- この例では、`|scArea=40|`を指定して左右に40px拡張させています。
- なお、拡張するのはプレイ画面のみで、他の画面には影響を与えません。
- プレイ画面についても、ライフゲージや歌詞表示・背景・マスクなどの位置は判定数の位置と背景色設定(`--background`)を除き、拡張前の横幅から変わりません。

<img src="https://github.com/cwtickle/danoniplus/assets/44026291/8bf2e98d-854f-4052-b51e-0882ba817e8e" width="50%">

### 関連項目
- [playingX](dos-h0070-playingX) [:pencil:](dos-h0070-playingX/_edit) ゲーム表示エリアのX座標
- [playingWidth](dos-h0071-playingWidth) [:pencil:](dos-h0071-playingWidth/_edit) ゲーム表示エリアの横幅

### 更新履歴

|Version|変更内容|
|----|----|
|[v33.4.0](https://github.com/cwtickle/danoniplus/releases/tag/v33.4.0)|・初回実装|

| [<- customCreditWidth](dos-h0083-customCreditWidth) | **scArea** | [stepY ->](dos-h0014-stepY) |