**[English](https://github.com/cwtickle/danoniplus-docs/wiki/AboutKeys-finger-movement) | Japanese** 

[<- Home](./) || [運指型](AboutKeys-fingering) | [バランス型](AboutKeys-balance) | [指移動型](AboutKeys-finger-movement) | [スクラッチ型](AboutKeys-scratch) | [オリジナル](AboutKeys-customKeys)

# 多鍵の種類（指移動型）
- 手をダイナミックに動かす、指移動が存在するパターン。  
複数のキーを合成した形になっており、楽曲の流れやポイントでこの指移動を使うと強い印象を与えることができます。  
上下から矢印が降ってくるのも、このパターンの特徴。  
11keyを始めとして、派生キーが多く存在しています。   

## Movレベルについて
- 難易度表など一部のサイトでは、難易度とは別にA～Gの記号がついています。  
これはMovレベルと呼ばれ、指移動型キー(17keyを含む)の持つ手の移動／Alt成分を難易度とは別に表記したものです。  
譜面の難しさには依存せず、指移動型キーのテクニックが必要かどうかが重視されます。  

- 詳細については次のページもご覧ください。⇒ [Movレベル基準について](https://cw7.sakura.ne.jp/howto/11key_mov.html)

## 11key
[ 難易度表 : [通常](http://dodl4.g3.xrea.com/normal11/) / [発狂](http://dodl4.g3.xrea.com/insane11/) / [Overjoy](https://suzme.github.io/11key-overjoy/) ]

<img src="./wiki/keyimg/key11_01.jpg" width="40%">
<img src="./wiki/keyimg/key11_02.png" width="40%">

<img src="./wiki/keys/key11.png" width="80%">

- 5keyと7keyを組み合わせたような形を持つのが11keyです。  
双方の表現がどちらもできるため、場面に応じて5keyや7keyを切り替えるといった表現もできます。  
5key部（矢印キー）と7keyの右側（J K L）を右手で処理する「右手移動」、  
5key部と7keyの右側の両方を取る「左手移動」や「クロス移動」によってダイナミックな表現が可能なのが大きな特徴です。  

## 11Lkey
[ 難易度表 : [通常](http://dodl4.g3.xrea.com/normal11/) / [発狂](http://dodl4.g3.xrea.com/insane11/) ]

<img src="./wiki/keyimg/key11L_01.png" width="40%">
<img src="./wiki/keys/key11L.png" width="80%">

- 11keyの左手版の11Lkey。左手が担当するキーは7～8個と、癖の強いキー構成です。  
左手側のキー配置はいずれも隣接しており、ちょっとした移動であれば手が届いてしまう距離にあります。  
これを利用した上下を跨ぐ同時押し（二次元配置）は強い印象を与えるのに効果的で、このキーの特徴と言えます。  

- 11keyと比較して指移動しやすいため、場面に応じた変化というよりは11個のキー全体をフルに使う譜面が多い印象があります。

## 13key [トリプルプレイ]
[ 多鍵DB : [13key](https://apoi108.sakura.ne.jp/danoni/ta/?keys=13) ]

<img src="./wiki/keys/key13.png" width="80%">

- 5keyと9Akeyを組み合わせたような形を持つのが13keyです。  
矢印キーの組が3つあることからトリプルプレイとも呼ばれます。  
下段の9Akeyが持つ両方の↑キーを使うことで、11keyとは違った表現ができるキーです。  

## 15Akey
[ 多鍵DB : [15Akey](https://apoi108.sakura.ne.jp/danoni/ta/?keys=15A) ]

<img src="./wiki/keyimg/key15A_01.png" width="40%">
<img src="./wiki/keys/key15A.png" width="80%">

- 11keyと11Lkeyを組み合わせた形を持つのが15Akeyです。  
双方の特色を持っており、11keyのダイナミックな移動に加えて11Lkeyの二次元配置もできるという、欲張りなキー構成になっています。  

## 15Bkey
[ 多鍵DB : [15Bkey](https://apoi108.sakura.ne.jp/danoni/ta/?keys=15B) ]

<img src="./wiki/keys/key15B.png" width="80%">

- 7keyの上に9Akeyが乗っかったような構成になっているのが15Bkeyです。  
移動がしやすい11Lkeyの特性を活かし、上下に駆け回る移動ができるのがこのキーの特徴です。

## 14ikey
[ 多鍵DB : [14ikey](https://apoi108.sakura.ne.jp/danoni/ta/?keys=14i) ]

<img src="./wiki/keys/key14i.png" width="80%">

- 7ikeyと7keyを組み合わせた形の14ikey。  
元々はキー変化作品の一部だったものが独立したキーになりました。  

- 11keyの移動に加えて、SDF/ZXC間の11Lらしい移動もできるキーです。  
キー配置や11Lの影響で、リバースにしたり「ZXC」を「WER」にキーコンするプレイも見られます。

## 16ikey
[ 多鍵DB : [16ikey](https://apoi108.sakura.ne.jp/danoni/ta/?keys=16i) ]

<img src="./wiki/keys/key16i.png" width="80%">

- 7ikeyと9Bkeyを組み合わせた形の16ikey。  
元々はキー変化作品の一部だったものが独立したキーになりました。  

- 11keyの移動、14ikeyの二次元配置に加えて、9Bkeyのダイナミックな配置ができるという贅沢なキーです。さまざまな表現性を秘めています。

----

### Special Thanks
- しょーでん、decresc.

----

[<- Home](./) || [運指型](AboutKeys-fingering) | [バランス型](AboutKeys-balance) | [指移動型](AboutKeys-finger-movement) | [スクラッチ型](AboutKeys-scratch) | [オリジナル](AboutKeys-customKeys)

|種類|対応キー数|
|----|----|
|[運指型](AboutKeys-fingering)|[5key](AboutKeys-fingering#5key) / [12key](AboutKeys-fingering#12key) / [14key](AboutKeys-fingering#14key) / [11ikey](AboutKeys-fingering#11ikey) / [17key](AboutKeys-fingering#17key) / [23key](AboutKeys-fingering#23key)|
|[バランス型](AboutKeys-balance)|[7key](AboutKeys-balance#7key) / [9Akey[ダブルプレイ]](AboutKeys-balance#9akey-ダブルプレイ) / [9Bkey](AboutKeys-balance#9bkey) / [7ikey](AboutKeys-balance#7ikey) / [9ikey](AboutKeys-balance#9ikey)|
|[指移動型](AboutKeys-finger-movement)|[11key](AboutKeys-finger-movement#11key) / [11Lkey](AboutKeys-finger-movement#11lkey) / [13key[トリプルプレイ]](AboutKeys-finger-movement#13key-トリプルプレイ) / [15Akey](AboutKeys-finger-movement#15akey) / [15Bkey](AboutKeys-finger-movement#15Bkey) / [14ikey](AboutKeys-finger-movement#14ikey) / [16ikey](AboutKeys-finger-movement#16ikey)|
|[スクラッチ型](AboutKeys-scratch)|[8key](AboutKeys-scratch#8key) / [11Wkey](AboutKeys-scratch#11wkey)|
|[オリジナル](AboutKeys-customKeys)|(Various Keys)|
