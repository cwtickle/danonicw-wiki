**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-h0023-finishView) | Japanese** 

[Home](./) &gt; [譜面ヘッダー仕様](dos_header) &gt; [プレイ時の初期設定](dos_header#-プレイ時の初期設定) &gt; [画面表示・キーコントロール](dos_header#画面表示キーコントロール)

| [<- readyHtml](dos-h0080-readyHtml) | **finishView** | [keyRetry ->](dos-h0039-keyRetry) |

## finishView
- フルコンボ演出の有無設定

### 使い方
```
|finishView=none|
```
### 説明
フルコンボ演出の有無を設定します。  
"none"を指定することでフルコンボ演出を表示せず、通常のコンボ表示を行います。  
コンボ表示も消す場合は、danoni_custom.js等で設定してください。   


|値|既定|内容|
|----|----|----|
|(未指定)|*|フルコンボ演出あり|
|none||フルコンボ演出なし|

### 関連項目
- [**customJs**](dos-h0019-customjs) [:pencil:](dos-h0019-customjs/_edit) カスタムjsファイルの指定

### 更新履歴

|Version|変更内容|
|----|----|
|[v1.15.0](https://github.com/cwtickle/danoniplus/releases/tag/v1.15.0)|・初回実装|

| [<- readyHtml](dos-h0080-readyHtml) | **finishView** | [keyRetry ->](dos-h0039-keyRetry) |