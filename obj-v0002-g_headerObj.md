[← オブジェクトリファレンスに戻る](ObjectReferenceIndex)  

## g_headerObj

### 概要
- 初期化時に作成されるオブジェクト。`headerConvert ()`関数にて作成される。  
譜面データのうち、譜面ヘッダーの情報を取り出して(厳密にはg_rootObjから)、必要な変換、補完処理を掛けて格納している。  
```javascript
g_headerObj.keyX = value;
```

### 生成タイミング
- タイトル画面呼び出し前 (`headerConvert ()`呼び出し時)

### 補足
- ほとんどの項目は譜面ヘッダー項目とリンクしている。  
- 基本的に１度定義されれば変わることはないが、一部例外がある。(下記に示す)

## プロパティ
(* :初期定義中に値が決まるプロパティ、@ :設定により置き換わる可能性のあるプロパティ)

### ファイル読込関係
- jsData (array2)
    - 関連項目: [loadMultipleFiles](./fnc-c0044-loadMultipleFiles)
```javascript
// カスタムjs, スキンjs, カスタムcssファイルのファイル名とディレクトリ名を管理
g_headerObj.jsData = [[`danoni_custom.js`, `C:/danoniplus/js/`], [`danoni_skin_default.js`, `C:/danoniplus/skin/`]];
```
- defaultSkinFlg (boolean)
- syncBackPath (boolean)

### 初期フォント
- customFont (string) *

### オブジェクトの種類
- imgType (array)
    - 関連項目: [imgType](./dos-h0082-imgType)
```javascript
// オブジェクトの種類(imgType)を定義。g_presetImageSetsと使い方は同じ
g_headerObj.imgType[1] = {
    name: `classic`,        // セット対象のフォルダ名：(img/classicフォルダ直下に画像一式を置く)
    extension: `png`,       // 拡張子：`png`形式
    rotateEnabled: true,    // 画像回転有無：true(回転有り)
    flatStepHeight: 50,     // Flat時ステップ間隔：50(px) ※矢印サイズ
};
```

### タイトルの背景矢印の設定
- titleArrowNo (number)
- titleArrowRotate (number)

### 横幅自動拡張設定
- autoSpread (boolean)
- heightVariable (boolean)

### 楽曲情報
- musicTitles (array)
- musicTitlesForView (array)
- musicNos (array)
    - 関連項目: [musicTitle](./dos-h0001-musicTitle), [musicNo](./dos-h0012-musicNo)
```javascript
// musicTitlesは1行用の曲名表示、musicTitlesForViewは複数行管理
g_headerObj.musicTitles = [`曲名1-Special-`, `曲名2`];
g_headerObj.musicTitlesForView = [[`曲名1`,`-Special-`], [`曲名2`]];

// この場合、1・2譜面目は「曲名1-Special-」、3譜面目は「曲名2」
g_headerObj.musicNos = [0, 0, 1];
```
- artistNames (array)
- artistName (string)
- artistUrl (string)

### 速度上限、下限
- minSpeed (float)
- maxSpeed (float)

### リトライ、タイトルバックのキーコード
- keyRetry (number)
- keyRetryDef (number)
- keyTitleBack (number)
- keyTitleBackDef (number)

### フリーズアローの許容フレーム数
- frzAttempt (number)

### 制作者クレジット
- tuning (string) @
- creatorUrl (string)
- tuningInit (string)

### 対応する譜面ファイル名、譜面番号
- dosNos (array)
- scoreNos (array)

### 譜面情報
- keyLabels (array)
- difLabels (array)
- initSpeeds (array)
    - 関連項目: [difData](./dos-h0002-difData)
```javascript
g_headerObj.keyLabels = [`7`, `11L`, `12`, `12`]; // 譜面毎のキー数
g_headerObj.difLabels = [`Normal`, `Hard`, `Extreme`, `Extra`]; // 譜面毎の名前
g_headerObj.initSpeeds = [2, 3, 4, 4.5]; // 譜面毎の初期速度。2x, 3x, 4x, 4.5x
```
- lifeBorders (array)
- lifeRecoverys (array)
- lifeDamages (array)
- lifeInits (array)
- creatorNames (array)
- keyLists (array)
- undefinedKeyLists (array)
- keyExtraList (array)
- difSelectorUse (boolean)

### グラデーション設定、矢印色設定
- baseBrightFlg (boolean)
- defaultColorgrd (array)
    - 関連項目: [defaultColorgrd](./dos-h0061-defaultColorgrd)
```javascript
// 自動グラデーション設定。1つ目は初期有効化の有無、2つ目はグラデーション有効時の中間色
g_headerObj.defaultColorgrd = [false, `#111111`];
```
- colorCdPaddingUse (boolean)
- defaultFrzColorUse (boolean)
- frzScopeFromArrowColors (array)
- setColorInit (array)
- setColor (array) @
- setColorType0 (array)
- setColorType1 (array)
- setColorType2 (array)
- setColorStr (array)
- setColorStrType0 (array)
- setColorOrg (array)
- setColorOrgType0 (array)
    - 関連項目: [グラデーション仕様](./dos-c0001-gradation)
```javascript
// グラデーション適用後の矢印色設定
g_headerObj.setColor = [`linear-gradient(to right, #ffff99, #9999ff)`, `#ffff0080`, ...];
// 譜面ヘッダーで指定した矢印色設定
g_headerObj.setColorStr = [`#ffff99:#9999ff`, `yellow;128`, ...];
// 譜面ヘッダーで指定した矢印色設定から先頭のカラーコードを抽出したリスト
g_headerObj.setColorOrg = [`#ffff99`, `#ffff0080`, ...];
```
- setShadowColorInit (array)
- setShadowColor (array)
- setShadowColorStr (array)
- setShadowColorOrg (array)
- frzColorInit (array)
- frzColor (array2) @
- frzColorType0 (array2)
- frzColorType1 (array2)
- frzColorType2 (array2)
- frzColorStr (array2)
- frzColorStrType0 (array2)
- frzColorOrg (array2)
- frzColorOrgType0 (array2)
- frzColorDefault (array2)
- frzShadowColorInit (array)
- frzShadowColor (array2)
- setDummyColor (array)
- dfColorgrdSet (object)
```javascript
/* デフォルトのグラデーション設定: 1つ目:グラデ可否, 2つ目:グラデ時の中間色 */
obj.defaultColorgrd = [false, `#eeeeee`];
obj.dfColorgrdSet = {
	''     : [false, `#eeeeee`],
	'Type0': [true , `#eeeeee`],
};
```

### ライフゲージ上限
- maxLifeVal (float)

### ダミーノート割り当て用No
- dummyScoreNos (array)

### 音楽再生位置、開始・終了位置の設定
- blankFrame (number) @
- blankFrameDef (number)
- startFrame (array)
- fadeFrame (array2)
- endFrame (array)
- adjustment (array)
- playbackRate (float)

### Reverse時の歌詞表示をステップゾーン位置に追随する設定
- bottomWordSetFlg (boolean)

### ウィンドウサイズ(縦)拡張時の基準速度
- baseSpeed (number)

### 楽曲ファイル設定
- musicFolder (string)
- musicUrls (array)

### ハッシュタグ設定
- hashTag (string)

### 画像などの読込設定
- autoPreload (boolean) *
- preloadImages (array)

### 初期表示する部分キーの設定
- keyGroupOrder (array2)

### 最終演出設定
- finishView (string)

### 作品更新日
- releaseDate (string)

### タイトル文字などのカスタム可否設定
- customTitleUse (boolean)
- customTitleArrowUse (boolean)
- customBackUse (boolean)
- customBackMainUse (boolean)
- customReadyUse (boolean)

### Ready設定及びアニメーション、遅延設定
- readyDelayFrame (number)
- resultDelayFrame (number)
- readyAnimationFrame (number)
- readyAnimationName (string)
- readyColor (string)
- readyHtml (string)

### デフォルトタイトルの詳細設定
- titlesize (number)
- titlefonts (array)
- titlegrds (array)
- titlearrowgrds (array)
- titlegrd (string)
- titlearrowgrd (string)
- titlepos (string)
- titlelineheight (number)
- titleAnimationName (array)
- titleAnimationDuration (array)
- titleAnimationDelay (array)
- titleAnimationTimingFunction (array)
- titleAnimationClass (array)

### フリーズアローの始点判定、空押し判定
- frzStartjdgUse (boolean)
- excessiveJdgUse (boolean)

### 譜面名に制作者を付加する設定
- makerView (boolean)

### 各種設定の禁止可否
- motionUse (boolean)
- scrollUse (boolean)
- shuffleUse (boolean)
- autoPlayUse (boolean)
- gaugeUse (boolean)
- appearanceUse (boolean) 
- stepzoneUse (boolean)
- stepzoneSet (switch)
- stepzoneChainOFF (array)
    - 関連項目: [displayUse](./dos-h0057-displayUse), [displayChainOFF](./dos-h0064-displayChainOFF)
```javascript
g_headerObj.stepzoneUse = true;  // ボタンの有効・無効（この場合は有効）
g_headerObj.stepzoneSet = `OFF`; // ボタンのデフォルト値（この場合はデフォルトOFF）

// StepZoneのOFFに合わせて「Judgment」「FastSlow」も連動してOFFにする
g_headerObj.stepzoneChainOFF = [`judgment`, `fastSlow`]; 
```
- judgmentUse (boolean) 
- judgmentSet (switch) 
- judgmentChainOFF (array)
- fastSlowUse (boolean) 
- fastSlowSet (switch) 
- fastSlowChainOFF (array)
- lifegaugeUse (boolean) 
- lifegaugeSet (switch) 
- lifegaugeChainOFF (array)
- scoreUse (boolean) 
- scoreSet (switch) 
- scoreChainOFF (array)
- musicInfoUse (boolean) 
- musicInfoSet (switch) 
- musicInfoChainOFF (array)
- filterLineUse (boolean) 
- filterLineSet (switch) 
- filterLineChainOFF (array)
- speedUse (boolean) 
- speedSet (switch) 
- speedChainOFF (array)
- colorUse (boolean) 
- colorSet (switch) 
- colorChainOFF (array)
- lyricsUse (boolean) 
- lyricsSet (switch) 
- lyricsChainOFF (array)
- backgroundUse (boolean) 
- backgroundSet (switch) 
- backgroundChainOFF (array)
- arrowEffectUse (boolean) 
- arrowEffectSet (switch) 
- arrowEffectChainOFF (array)
- specialUse (boolean) 
- specialSet (switch) 
- specialChainOFF (array)
- transKeyUse (boolean) 

### 背景・マスク設定（タイトル・リザルト）
- backTitleData (array2)
- backTitleMaxDepth (number)
- maskTitleData (array2)
- maskTitleMaxDepth (number)
    - 関連項目: [背景・マスクモーション](./dos-e0004-animationData)
```javascript
// フレーム、深度毎の二次元配列でタイトルモーションを管理
g_headerObj.backTitleData[frame][depth] = {
    depth: 2,                // 背景・マスクの深度
    command: `[jump]`,       // 指定キーワード(タイトル・リザルトモーションのみ)
    jumpFrame: `1000:2000`,  // [loop][jump]のとき、移動先のフレーム数（この場合は1000フレームか2000フレームのどちらかへランダム移動）
    maxLoop: 4,              // [jump]のとき、ループ終了回数。
    animationName: ``,       // アニメーション名。フェードイン時のアニメーション除外条件で使用。
    htmlText: ``,            // 背景・マスクに表示する本体。タグなどもここに入る。
}
// タイトルモーションで使用する最大深度（この数字を元にタイトルモーション用の枠を作成）
g_headerObj.backTitleDepth = 6;
```
- maskTitleButton (boolean) 
- maskResultButton (boolean) 

### 背景・マスク設定（リザルト）
※headerConvertではなくscoreConvertで定義
- backResultData (array2)
- backResultMaxDepth (number)
- maskResultData (array2)
- maskResultMaxDepth (number)
- backFailedData (array2)
- backFailedMaxDepth (number)
- maskFailedData (array2)
- maskFailedMaxDepth (number)

### カラーデータの過去データ互換設定
- colorDataType (string)

### リザルトモーションの表示可否設定
- resultMotionSet (boolean)

### 演出OFF時に判定位置をリセットする設定
- jdgPosReset (boolean)

### コメント表示関連
- commentVal (string)
- commentExternal (boolean)

### Reverse時の歌詞の自動反転制御
- wordAutoReverse (string)

### プレイ位置調整
- playingWidth (number)
- playingHeight (number)
- playingX (number)
- playingY (number)
- customViewWidth (number)
- scAreaWidth (number)
- playingLayout (boolean)
- justFrames (number)

### リザルトデータのカスタマイズ
- resultFormat (string)

### フェードイン時にそれ以前のデータを蓄積しない種別(word, back, mask)を指定
- unStockCategories (array)
