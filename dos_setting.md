**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos_setting) | Japanese** 

| [< グラデーション仕様](./dos-c0001-gradation) | **共通設定ファイル仕様** | [スキンファイル仕様 >](./AboutSkin) |

# 共通設定ファイル仕様
- 作品の共通設定については`danoni_setting.js`で設定することができます。
- グループごとに共通設定を分けることが可能です。詳細は譜面ヘッダー：[settingType](dos-h0056-settingType)をご覧ください。
- 対応する譜面ヘッダーがある場合は、譜面ヘッダーの値が優先されます。
- ver26.3.1以降、ここで設定する項目は全て g_presetObj のプロパティとして定義しています。

## 設定可能項目

|カテゴリー|設定項目|
|----|----|
|[制作者クレジット・基本設定](dos-s0001-makerInfo) [:pencil:](dos-s0001-makerInfo/_edit)|**tuning** (譜面製作者名)<br>**tuningUrl** (譜面製作者URL)<br>**autoSpread** (自動横幅拡張設定)<br>**autoMinWidth** (最小横幅設定)<br>**autoMinHeight** (最小高さ設定)<br>**heightVariable** (高さ可変設定)<br>**windowAlign** (ウィンドウ位置)|
|[カスタムファイル設定](dos-s0002-customFile) [:pencil:](dos-s0002-customFile/_edit)|**skinType** (デフォルトスキン)<br>**bgCanvasUse** (デフォルトスキン時のCanvas有効化設定)<br>**customJs** (カスタムJS)<br>**customCss** (カスタムCss)<br>**syncBackPath** (背景・マスクモーションで使う画像パスの基準フォルダ設定)|
|[ゲージ設定](dos-s0003-initialGauge) [:pencil:](dos-s0003-initialGauge/_edit)|**gauge** (デフォルトのゲージ設定)<br>**gaugeCustom** (デフォルト以外のゲージ値の初期設定)<br>**gaugeList** (カスタムゲージリスト)<br>**excessiveJdgUse** (空押し判定のデフォルト設定)|
|[フリーズアロー設定](dos-s0004-frzArrow) [:pencil:](dos-s0004-frzArrow/_edit)|**frzColors** (フリーズアローのデフォルト色セットの利用有無)<br>**frzScopeFromAC** (矢印色変化に対応してフリーズアロー色を追随する範囲の設定)<br>**frzStartjdgUse** (フリーズアローの始点判定設定)|
|[デフォルトデザイン・画像設定](dos-s0005-defaultDesign) [:pencil:](dos-s0005-defaultDesign/_edit)|**customDesignUse** (デフォルトデザインの使用有無設定)<br>**imageSets** (デフォルト画像セットの設定)<br>**overrideExtension** (デフォルト画像の拡張子設定)<br>**customImageList** (追加指定する画像のリスト)<br>**animationFillMode** (背景・マスクモーションで利用する「animationFillMode」のデフォルト値)|
|[オプション有効化](dos-s0006-settingUse) [:pencil:](dos-s0006-settingUse/_edit)|**settingUse** (オプション有効化設定)|
|[プレイ画面制御](dos-s0007-viewControl) [:pencil:](dos-s0007-viewControl/_edit)|**wordAutoReverse** (Reverse時の歌詞の自動反転制御設定)<br>**unStockCategories** (フェードイン前のデータを保持しない種別の設定)<br>**stockForceDelList** (フェードイン前のデータを保持しないパターンの設定)<br>**scAreaWidth** (ショートカットキーエリアの横幅拡張設定)<br>**playingLayout** (プレイ画面の表示レイアウト)<br>**playingX / playingY** (ゲーム表示エリアのX, Y座標)<br>**playingWidth / playingHeight** (ゲーム表示エリアの横幅、高さ)|
|[リザルトデータ](dos-s0008-resultVals) [:pencil:](dos-s0008-resultVals/_edit)|**resultFormat** (リザルトデータのフォーマット設定)<br>**resultVals** (リザルトデータ用のカスタム変数群)<br>**resultValsView** (リザルトデータ用のカスタム変数の画像表示設定)|
|[ラベルテキスト・メッセージ](dos-s0009-labelUpdate) [:pencil:](dos-s0009-labelUpdate/_edit)|**lblName** (ラベルテキスト設定)<br>**msg** (オンマウステキスト、確認メッセージ定義設定)<br> **lblRenames** (設定名の上書き可否設定)|
|[カスタムキー定義](dos-s0010-customKeys) [:pencil:](dos-s0010-customKeys/_edit)|**keysData** (共通カスタムキー定義)<br>**keysDataLib** (ライブラリ用カスタムキー定義)|


## 更新履歴（共通）

|Version|変更内容|
|----|----|
|[v26.3.1](https://github.com/cwtickle/danoniplus/releases/tag/v26.3.1)|・共通設定ファイルで使用する変数名を g_presetObj へ統合|
|[v10.0.0](https://github.com/cwtickle/danoniplus/releases/tag/v10.0.0)|・共通設定ファイルの変更、分割設定（-> [settingType](dos-h0056-settingType)）の実装|
|[v3.0.0](https://github.com/cwtickle/danoniplus/releases/tag/v3.0.0)|・初期実装|

| [< グラデーション仕様](./dos-c0001-gradation) | **共通設定ファイル仕様** | [スキンファイル仕様 >](./AboutSkin) |
