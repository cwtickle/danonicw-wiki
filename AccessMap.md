**[English](https://github.com/cwtickle/danoniplus-docs/wiki/AccessMap) | Japanese** 

| [< トップページ](./) || **用途別アクセスマップ** | [ゲーム画面の説明 >](./AboutGameSystem) |

# 用途別アクセスマップ

## Dancing☆Onigiriを遊ぶ
### 遊び方を知りたい
- [ゲーム画面の説明](AboutGameSystem) ／ [ゲーム内ショートカット](Shortcut)

### Dancing☆Onigiriのキーについて知りたい
|種類|対応キー数|
|----|----|
|[運指型](AboutKeys-fingering)|[5key](AboutKeys-fingering#5key) / [12key](AboutKeys-fingering#12key) / [14key](AboutKeys-fingering#14key) / [11ikey](AboutKeys-fingering#11ikey) / [17key](AboutKeys-fingering#17key) / [23key](AboutKeys-fingering#23key)|
|[バランス型](AboutKeys-balance)|[7key](AboutKeys-balance#7key) / [9Akey[ダブルプレイ]](AboutKeys-balance#9akey-ダブルプレイ) / [9Bkey](AboutKeys-balance#9bkey) / [7ikey](AboutKeys-balance#7ikey) / [9ikey](AboutKeys-balance#9ikey)|
|[指移動型](AboutKeys-finger-movement)|[11key](AboutKeys-finger-movement#11key) / [11Lkey](AboutKeys-finger-movement#11lkey) / [13key[トリプルプレイ]](AboutKeys-finger-movement#13key-トリプルプレイ) / [15Akey](AboutKeys-finger-movement#15akey) / [15Bkey](AboutKeys-finger-movement#15Bkey) / [14ikey](AboutKeys-finger-movement#14ikey) / [16ikey](AboutKeys-finger-movement#16ikey)|
|[スクラッチ型](AboutKeys-scratch)|[8key](AboutKeys-scratch#8key) / [11Wkey](AboutKeys-scratch#11wkey)|
|[オリジナル](AboutKeys-customKeys)|(Various Keys)|
|ダンおに以外|[Punching◇Panels (パンパネ)](https://github.com/cwtickle/punching-panels) / キリズマ|
|その他|[別キーモード](AnotherKeysMode)|

### 実際の作品を遊びたい（外部サイト）
- [Dancing☆Onigiri サイト一覧](https://cw7.sakura.ne.jp/danonidb/) ／ [Dancing☆Onigiri 作品一覧](https://cw7.sakura.ne.jp/lst/)  
- [Dancing☆Onigiri 難易度表 for.js](http://dodl4.g3.xrea.com/) ／ [多鍵データベース](https://apoi108.sakura.ne.jp/danoni/ta/)

## 最新機能を知る／更新する
### 最近の本体更新状況を知りたい
- [Releases (最新)](https://github.com/cwtickle/danoniplus/releases/latest) ／ [Security Policy](https://github.com/cwtickle/danoniplus/security/policy)

### バージョンアップ方法・留意点を知りたい
- [本体のバージョンアップ方法](HowToUpdate) ／ [アップグレードガイド](MigrationGuide)

### 過去バージョンの不具合情報を知りたい
- [更新を終了したバージョンの不具合情報](DeprecatedVersionBugs)

## Dancing☆Onigiriを作る
### 作り方を知りたい、公開したい
- [譜面の作成概要](HowtoMake) ／ [ソースの構成](AboutSource)
- [エディターを使った譜面作成の流れ](https://github.com/superkuppabros/danoni-editor/wiki/%E8%AD%9C%E9%9D%A2%E4%BD%9C%E6%88%90%E3%81%AE%E6%B5%81%E3%82%8C) ／ [エディター操作早見表](https://github.com/superkuppabros/danoni-editor/wiki/%E6%93%8D%E4%BD%9C%E6%97%A9%E8%A6%8B%E8%A1%A8)
- [作品データのWeb公開方法](HowToFileUpload) ／ [作品URLのクエリパラメーター仕様](QueryParameter)

### Flash作品を移植したい、違いを知りたい
- [ParaFla!ユーザ向け移行概要](forParaFlaUser) ／ [Flash版との機能差異](DifferenceFromFlashVer)

### 音源の再生遅延を解消したい
- [オーディオ仕様](Audio) ／ [ダンおに曲データjs化ツール](https://github.com/suzme/danoni-base64)

### 譜面にエフェクトや歌詞、背景をつけたい
- [譜面エフェクト仕様](dos_effect)
    - [速度変化](dos-e0001-speedData) ／ [色変化](dos-e0002-ncolorData) ／ [歌詞表示](dos-e0003-wordData)
    - [背景・マスクの設定](dos-e0004-animationData) ／ [矢印・フリーズアローモーション](dos-e0005-motionData)
- [背景の表示方法](tips-0002-background)

### 使用禁止文字について知りたい
- [譜面データにおける特殊文字の取り扱い](SpecialCharacters)

### 作品内の各種設定をカスタマイズしたい
- [譜面ヘッダー仕様 (初期設定)](dos_header) ／ [共通ファイル設定](dos_setting) ／ [スキンファイル設定](AboutSkin)

|用途|主な設定項目|
|----|----|
|[楽曲・譜面情報](dos_header#-楽曲譜面情報)|[musicTitle](dos-h0001-musicTitle) (楽曲情報) / [difData](dos-h0002-difData) (譜面情報) / <br>[musicUrl](dos-h0011-musicUrl) (楽曲ファイル)|
|[プレイ時間制御・<br>譜面位置調整](dos_header#-プレイ時間制御譜面位置調整)|[endFrame](dos-h0007-endFrame) (終了フレーム数) / [fadeFrame](dos-h0008-fadeFrame) (フェードアウト設定) / <br>[adjustment](dos-h0009-adjustment) (譜面位置の調整) / [playbackRate](dos-h0010-playbackRate) (楽曲再生速度)|
|[設定画面の<br>カスタマイズ](dos_header#-設定時の初期設定)|[settingUse](dos-h0035-settingUse) / [displayUse](dos-h0057-displayUse) (一部オプションの禁止) /<br>[customGauge](dos-h0053-customGauge) (ゲージ設定の独自リスト) / [gaugeX](dos-h0022-gaugeX) (ゲージ設定の詳細)|
|[プレイ画面の<br>カスタマイズ](dos_header#-プレイ時の初期設定)<br>(オブジェクト色)|[setColor](dos-h0003-setColor) (矢印色) / [frzColor](dos-h0004-frzColor) (フリーズアロー色) /<br>[defaultColorgrd](dos-h0061-defaultColorgrd) (自動グラデーション設定) / [グラデーション仕様](dos-c0001-gradation)<br>[defaultFrzColorUse](dos-h0063-defaultFrzColorUse) (矢印色に合わせてフリーズアロー色を設定)|
|(プレイ画面位置)|[stepY](dos-h0014-stepY) / [stepYR](dos-h0049-stepYR) (ステップゾーン位置) /<br>[arrowJdgY / frzJdgY](dos-h0058-jdgY) (判定キャラクタ位置) /<br> [jdgPosReset](dos-h0065-jdgPosReset) (判定キャラクタ位置のリセット) /<br>[bottomWordSet](dos-h0059-bottomWordSet) (ステップゾーン位置に合わせて歌詞表示を移動)|
|(判定関連)|[frzStartjdgUse](dos-h0037-frzStartjdgUse) (フリーズアロー開始判定ON) /<br>[frzAttempt](dos-h0038-frzAttempt) (フリーズアローを離したときの許容フレーム数)|
|(画面表示)|[readyDelayFrame](dos-h0052-readyDelayFrame) / [readyAnimationFrame](dos-h0073-readyAnimationFrame) / [readyAnimationName](dos-h0074-readyAnimationName) / <br>[readyColor](dos-h0075-readyColor) / [readyHtml](dos-h0080-readyHtml) (プレイ開始時文字の各種設定) / <br>[finishView](dos-h0023-finishView) (フルコンボ演出OFF)|
|[タイトル・<br>結果画面の設定](dos_header#%EF%B8%8F-タイトル結果画面の初期設定)|[commentVal / commentValJa / commentValEn](dos-h0066-commentVal) (制作者コメント) /<br>[commentExternal](dos-h0068-commentExternal) (コメントを作品本体の外部に配置) /<br>[resultDelayFrame](dos-h0076-resultDelayFrame) (CLEARED/FAILEDが表示されるまでの遅延フレーム数) / <br>[resultFormat](dos-h0072-resultFormat) (リザルトテキストのフォーマット)|
|[タイトル文字の<br>設定](dos_header#-タイトル曲名文字デフォルトデザインのエフェクト)|[titleSize](dos-h0030-titlesize) (文字サイズ) / [titleFont](dos-h0031-titlefont) (フォント) /<br> [titlegrd / titleArrowgrd](dos-h0032-titlegrd) (グラデーション) / [titlePos](dos-h0033-titlepos) (文字位置調整) / <br>[titleAnimation](dos-h0077-titleanimation) (アニメーション設定)|
|[クレジット](dos_header#-クレジット関連)|[tuning](dos-h0017-tuning) (制作者情報) / [makerView](dos-h0050-makerView) (譜面別の制作者表示) / <br>[hashTag](dos-h0018-hashTag) (ハッシュタグ) / [releaseDate](dos-h0036-releaseDate) (更新日)|

### 自分でカスタムキーを作成したい
- [キー数仕様](keys) ／ [カスタムキーテンプレート](tips-0004-extrakeys) ／ [色付きオブジェクト仕様](AboutColorObject)
- [キー数変化](./dos-e0006-keychData) ／ [キー変化作品の実装例](./tips-0009-key-types-change)

### 通常の設定でできないような仕掛けを作りたい (上級者向け、考え方のみ)
- [カスタムjs(スキンjs)による処理割込み](AboutCustomJs) ／ [カスタム関数の定義](AboutCustomFunction)
- [オブジェクト構成・ボタン](DOM) ／ [疑似フレーム処理仕様](AboutFrameProcessing) ／ [判定仕様](AboutJudgment)
- [ローカルストレージ仕様](LocalStorage) ／ [速度変化仕様](SpeedChange)
- [ID一覧](IdReferenceIndex) ／ [オブジェクト一覧](ObjectReferenceIndex)

### 警告が出た場合の対処方法を知りたい
- [実行時エラー一覧](ErrorList)

| [< トップページ](./) || **用途別アクセスマップ** | [ゲーム画面の説明 >](./AboutGameSystem) |
