**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-h0081-baseBright) | Japanese** 

[Home](./) &gt; [譜面ヘッダー仕様](dos_header) &gt; [カスタムデータの取込](dos_header#-カスタムデータの取込)

| [<- imgType](dos-h0082-imgType) | **baseBright** | [syncBackPath ->](dos-h0087-syncBackPath) |

## baseBright
- 背景の明暗状態の設定

### 使い方
```
|baseBright=true|  // 背景を明色扱いにする
```
### 説明
背景を強制的に明色扱い／暗色扱いにするかどうかを設定します。  
実際に背景色を変えるのではなく、それに付属するColorTypeを明色用、暗色用に切り替えるために使用します。  
デフォルトは`false`ですが、CSS内の`.title_base`クラスで指定されているcolorが明色かどうかで初期値が変わります。

|値|既定|内容|
|----|----|----|
|false|(*)|背景を暗色扱いにする|
|true||背景を明色扱いにする|

### 関連項目
- [**defaultColorgrd**](dos-h0061-defaultColorgrd) [:pencil:](dos-h0061-defaultColorgrd/_edit) 自動グラデーション設定
- [**skinType**](dos-h0054-skinType) [:pencil:](dos-h0054-skinType/_edit) スキン設定

### 更新履歴

|Version|変更内容|
|----|----|
|[v21.2.0](https://github.com/cwtickle/danoniplus/releases/tag/v21.2.0)|・初回実装|

| [<- imgType](dos-h0082-imgType) | **baseBright** | [syncBackPath ->](dos-h0087-syncBackPath) |