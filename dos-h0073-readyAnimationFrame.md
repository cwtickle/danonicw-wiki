**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-h0073-readyAnimationFrame) | Japanese** 

[Home](./) &gt; [譜面ヘッダー仕様](dos_header) &gt; [プレイ時の初期設定](dos_header#-プレイ時の初期設定) &gt; [画面表示・キーコントロール](dos_header#画面表示キーコントロール)

| [<- readyDelayFrame](dos-h0052-readyDelayFrame) | **readyAnimationFrame** | [readyAnimationName ->](dos-h0074-readyAnimationName) |

## readyAnimationFrame
- Ready?のアニメーションフレーム数の設定

### 使い方
```
|readyAnimationFrame=150|
```
### 説明
Ready表示のアニメーションフレーム数を指定します。デフォルトは150フレームです。    

### 関連項目
- [readyDelayFrame](dos-h0052-readyDelayFrame) [:pencil:](dos-h0052-readyDelayFrame/_edit) Ready?が表示されるまでの遅延フレーム数
- [readyAnimationName](dos-h0074-readyAnimationName) [:pencil:](dos-h0074-readyAnimationName/_edit) Ready?のアニメーション名
- [readyColor](dos-h0075-readyColor) [:pencil:](dos-h0075-readyColor/_edit) Ready?の先頭色設定
- [readyHtml](dos-h0080-readyHtml) [:pencil:](dos-h0080-readyHtml/_edit) Ready?の文字設定

### 更新履歴

|Version|変更内容|
|----|----|
|[v16.3.0](https://github.com/cwtickle/danoniplus/releases/tag/v16.3.0)|・初回実装|

| [<- readyDelayFrame](dos-h0052-readyDelayFrame) | **readyAnimationFrame** | [readyAnimationName ->](dos-h0074-readyAnimationName) |