[← オブジェクトリファレンスに戻る](ObjectReferenceIndex)  

## g_shortcutObj

### 概要
- ショートカットキーを管理するオブジェクト。
- ショートカットキーの割り当ては画面により異なるため、画面別にプロパティを持つ。
- 画面別プロパティは割り当てキー毎に、以下の項目を持っている。
  - プロパティ名 : `KeyBoardEvent.code`に従ったコードを指定。  
複数キーを組み合わせる場合は`_`で区切る。
  - id : 実行するボタンのID名を指定。
  - reset (任意) : 実行前にキーを押した状態を解除する場合は`true`を指定。
他のサイトへ移動する場合、onkeyupイベントが動かないためこの項目を設定している。
- ショートカットキーの検索は画面プロパティ別に上から順番に行う。  
このため、複数キーの組み合わせは単一キーより前に持ってくる必要がある。  
    - 「A」→「Shift+A」の順では、実際に「Shift+A」が押されても「A」が合致してしまい、  
「Shift+A」に対応するボタンが反応しない。  
この場合は、「Shift+A」→「A」の順にすることで想定通りの動きになる。

### 生成タイミング
- 初回起動時

### プロパティ
```javascript
const g_shortcutObj = {
    title: {
        Enter: { id: `btnStart` },
        NumpadEnter: { id: `btnStart` },
        Slash: { id: `btnHelp`, reset: true },
        F1: { id: `btnHelp`, reset: true },
        ControlLeft_KeyC: { id: `` },
        KeyC: { id: `btnComment` },
    },
    option: {
        ShiftLeft_KeyD: { id: `lnkDifficultyL` },
        ShiftRight_KeyD: { id: `lnkDifficultyL` },
        KeyD: { id: `lnkDifficultyR` },

        ShiftLeft_ArrowRight: { id: `lnkSpeedR` },
        ShiftRight_ArrowRight: { id: `lnkSpeedR` },
        AltLeft_ArrowRight: { id: `lnkSpeedHR` },
        AltRight_ArrowRight: { id: `lnkSpeedHR` },
        ArrowRight: { id: `lnkSpeedRR` },
        ShiftLeft_ArrowLeft: { id: `lnkSpeedL` },
        ShiftRight_ArrowLeft: { id: `lnkSpeedL` },
        AltLeft_ArrowLeft: { id: `lnkSpeedHL` },
        AltRight_ArrowLeft: { id: `lnkSpeedHL` },
        ArrowLeft: { id: `lnkSpeedLL` },

        KeyL: { id: `lnkDifficulty` },

        ShiftLeft_KeyM: { id: `lnkMotionL` },
        ShiftRight_KeyM: { id: `lnkMotionL` },
        KeyM: { id: `lnkMotionR` },
        ArrowUp: { id: `lnkScrollL` },
        ArrowDown: { id: `lnkScrollR` },
        KeyR: { id: `lnkReverseR`, dfId: `lnkReverseR`, exId: `btnReverse` },

        ShiftLeft_KeyS: { id: `lnkShuffleL` },
        ShiftRight_KeyS: { id: `lnkShuffleL` },
        KeyS: { id: `lnkShuffleR` },
        ShiftLeft_KeyA: { id: `lnkAutoPlayL` },
        ShiftRight_KeyA: { id: `lnkAutoPlayL` },
        KeyA: { id: `lnkAutoPlayR` },
        ShiftLeft_KeyG: { id: `lnkGaugeL` },
        ShiftRight_KeyG: { id: `lnkGaugeL` },
        KeyG: { id: `lnkGaugeR` },
        KeyE: { id: `lnkExcessive` },

        AltLeft_ShiftLeft_Semicolon: { id: `lnkAdjustmentHR` },
        AltLeft_ShiftRight_Semicolon: { id: `lnkAdjustmentHR` },
        AltRight_ShiftLeft_Semicolon: { id: `lnkAdjustmentHR` },
        AltRight_ShiftRight_Semicolon: { id: `lnkAdjustmentHR` },
        ShiftLeft_Semicolon: { id: `lnkAdjustmentR` },
        ShiftRight_Semicolon: { id: `lnkAdjustmentR` },
        AltLeft_Semicolon: { id: `lnkAdjustmentRRR` },
        AltRight_Semicolon: { id: `lnkAdjustmentRRR` },
        Semicolon: { id: `lnkAdjustmentRR` },
        AltLeft_ShiftLeft_Minus: { id: `lnkAdjustmentHL` },
        AltLeft_ShiftRight_Minus: { id: `lnkAdjustmentHL` },
        AltRight_ShiftLeft_Minus: { id: `lnkAdjustmentHL` },
        AltRight_ShiftRight_Minus: { id: `lnkAdjustmentHL` },
        ShiftLeft_Minus: { id: `lnkAdjustmentL` },
        ShiftRight_Minus: { id: `lnkAdjustmentL` },
        AltLeft_Minus: { id: `lnkAdjustmentLLL` },
        AltRight_Minus: { id: `lnkAdjustmentLLL` },
        Minus: { id: `lnkAdjustmentLL` },

        AltLeft_ShiftLeft_NumpadAdd: { id: `lnkAdjustmentHR` },
        AltLeft_ShiftRight_NumpadAdd: { id: `lnkAdjustmentHR` },
        AltRight_ShiftLeft_NumpadAdd: { id: `lnkAdjustmentHR` },
        AltRight_ShiftRight_NumpadAdd: { id: `lnkAdjustmentHR` },
        ShiftLeft_NumpadAdd: { id: `lnkAdjustmentR` },
        ShiftRight_NumpadAdd: { id: `lnkAdjustmentR` },
        AltLeft_NumpadAdd: { id: `lnkAdjustmentRRR` },
        AltRight_NumpadAdd: { id: `lnkAdjustmentRRR` },
        NumpadAdd: { id: `lnkAdjustmentRR` },
        AltLeft_ShiftLeft_NumpadSubtract: { id: `lnkAdjustmentHL` },
        AltLeft_ShiftRight_NumpadSubtract: { id: `lnkAdjustmentHL` },
        AltRight_ShiftLeft_NumpadSubtract: { id: `lnkAdjustmentHL` },
        AltRight_ShiftRight_NumpadSubtract: { id: `lnkAdjustmentHL` },
        ShiftLeft_NumpadSubtract: { id: `lnkAdjustmentL` },
        ShiftRight_NumpadSubtract: { id: `lnkAdjustmentL` },
        AltLeft_NumpadSubtract: { id: `lnkAdjustmentLLL` },
        ShiftRight_NumpadSubtract: { id: `lnkAdjustmentL` },
        NumpadSubtract: { id: `lnkAdjustmentLL` },

        ShiftLeft_KeyV: { id: `lnkVolumeL` },
        ShiftRight_KeyV: { id: `lnkVolumeL` },
        KeyV: { id: `lnkVolumeR` },

        KeyI: { id: `btnGraph` },
        Digit1: { id: `lnkDensityG` },
        Digit2: { id: `lnkSpeedG` },
        Digit3: { id: `lnkToolDifG` },
        Numpad1: { id: `lnkDensityG` },
        Numpad2: { id: `lnkSpeedG` },
        Numpad3: { id: `lnkToolDifG` },
        KeyQ: { id: `lnkDensityG` },
        KeyP: { id: `lnkDifInfo` },
        KeyZ: { id: `btnSave` },

        Escape: { id: `btnBack` },
        Space: { id: `btnKeyConfig` },
        Enter: { id: `btnPlay` },
        NumpadEnter: { id: `btnPlay` },
        ShiftLeft_Tab: { id: `btnBack` },
        ShiftRight_Tab: { id: `btnBack` },
        Tab: { id: `btnDisplay` },
    },
    difSelector: {
        ShiftLeft_KeyD: { id: `lnkDifficultyL` },
        ShiftRight_KeyD: { id: `lnkDifficultyL` },
        KeyD: { id: `lnkDifficultyR` },
        KeyR: { id: `difRandom` },
        KeyL: { id: `lnkDifficulty` },
        ArrowDown: { id: `btnDifD` },
        ArrowUp: { id: `btnDifU` },

        KeyI: { id: `btnGraph` },
        Digit1: { id: `lnkDensityG` },
        Digit2: { id: `lnkSpeedG` },
        Digit3: { id: `lnkToolDifG` },
        Numpad1: { id: `lnkDensityG` },
        Numpad2: { id: `lnkSpeedG` },
        Numpad3: { id: `lnkToolDifG` },
        KeyQ: { id: `lnkDensityG` },
        KeyP: { id: `lnkDifInfo` },

        Escape: { id: `btnBack` },
        Space: { id: `btnKeyConfig` },
        Enter: { id: `lnkDifficulty` },
        NumpadEnter: { id: `lnkDifficulty` },
        ShiftLeft_Tab: { id: `btnBack` },
        ShiftRight_Tab: { id: `btnBack` },
        Tab: { id: `btnDisplay` },
    },
    settingsDisplay: {
        ShiftLeft_KeyA: { id: `lnkAppearanceL` },
        ShiftRight_KeyA: { id: `lnkAppearanceL` },
        KeyA: { id: `lnkAppearanceR` },
        ShiftLeft_KeyO: { id: `lnkOpacityL` },
        ShiftRight_KeyO: { id: `lnkOpacityL` },
        KeyO: { id: `lnkOpacityR` },

        ShiftLeft_KeyB: { id: `lnkHitPositionR` },
        ShiftRight_KeyB: { id: `lnkHitPositionR` },
        KeyB: { id: `lnkHitPositionRR` },
        ShiftLeft_KeyT: { id: `lnkHitPositionL` },
        ShiftRight_KeyT: { id: `lnkHitPositionL` },
        KeyT: { id: `lnkHitPositionLL` },

        Digit1: { id: `lnkstepZone` },
        Digit2: { id: `lnkjudgment` },
        Digit3: { id: `lnkfastSlow` },
        Digit4: { id: `lnklifeGauge` },
        Digit5: { id: `lnkscore` },
        Digit6: { id: `lnkmusicInfo` },
        Digit7: { id: `lnkfilterLine` },
        Digit8: { id: `lnkspeed` },
        Digit9: { id: `lnkcolor` },
        Digit0: { id: `lnklyrics` },
        Semicolon: { id: `lnkbackground` },
        Minus: { id: `lnkarrowEffect` },
        Slash: { id: `lnkspecial` },

        Numpad1: { id: `lnkstepZone` },
        Numpad2: { id: `lnkjudgment` },
        Numpad3: { id: `lnkfastSlow` },
        Numpad4: { id: `lnklifeGauge` },
        Numpad5: { id: `lnkscore` },
        Numpad6: { id: `lnkmusicInfo` },
        Numpad7: { id: `lnkfilterLine` },
        Numpad8: { id: `lnkspeed` },
        Numpad9: { id: `lnkcolor` },
        Numpad0: { id: `lnklyrics` },
        NumpadAdd: { id: `lnkbackground` },
        NumpadSubtract: { id: `lnkarrowEffect` },
        NumpadDivide: { id: `lnkspecial` },

        KeyZ: { id: `btnSave` },
        Escape: { id: `btnBack` },
        Space: { id: `btnKeyConfig` },
        Enter: { id: `btnPlay` },
        NumpadEnter: { id: `btnPlay` },
        ShiftLeft_Tab: { id: `btnBack` },
        ShiftRight_Tab: { id: `btnBack` },
        Tab: { id: `btnSettings` },
    },
    keyConfig: {
        Escape: { id: `btnBack` },
        Backspace_Enter: { id: `btnPlay` },
        Backspace_NumpadEnter: { id: `btnPlay` },
    },
    loadingIos: {
        Enter: { id: `btnPlay` },
        NumpadEnter: { id: `btnPlay` },
    },
    result: {
        Escape: { id: `btnBack` },
        ShiftLeft_Tab: { id: `btnBack` },
        ShiftRight_Tab: { id: `btnBack` },
        ControlLeft_KeyC: { id: `` },
        ControlRight_KeyC: { id: `` },
        KeyC: { id: `btnCopy`, reset: true },
        KeyT: { id: `btnTweet`, reset: true },
        KeyG: { id: `btnGitter`, reset: true },
        KeyP: { id: `btnCopyImage` },
        Backspace: { id: `btnRetry` },
    },
};
```

### 関連項目
- [ゲーム内で使用できるショートカット](Shortcut)
- [ID一覧](idReferenceIndex)

### 更新履歴

|Version|変更内容|
|----|----|
|[v33.5.0](https://github.com/cwtickle/danoniplus/releases/tag/v33.5.0)|・SNS投稿用の画像を表示する機能を実装|
|[v33.1.0](https://github.com/cwtickle/danoniplus/releases/tag/v33.1.0)|・結果をSNS投稿用の画像として作成しクリップボードへコピーする機能を実装<br>(クリップボードコピーに対応しない場合はSNS投稿用の画像を表示)|
|[v32.6.1](https://github.com/cwtickle/danoniplus/releases/tag/v32.6.1)|・Excessive追加に伴い、ショートカットキーを割り当て|
|[v32.5.0](https://github.com/cwtickle/danoniplus/releases/tag/v32.5.0)|・譜面明細表示の順番変更に伴い、ショートカットキーを入れ替え|
|[v31.0.0](https://github.com/cwtickle/danoniplus/releases/tag/v31.0.0)|・HitPosition追加に伴い、ショートカットキーを割り当て|
|[v28.0.1](https://github.com/cwtickle/danoniplus/releases/tag/v28.0.1)|・譜面明細画面の速度変化／譜面密度グラフ、譜面レベル表示を切替するショートカットキーを復活（画面の非表示を兼ねる）|
|[v27.8.0](https://github.com/cwtickle/danoniplus/releases/tag/v27.8.0)|・譜面明細画面の速度変化グラフ、譜面密度グラフ、譜面レベル表示に移動するショートカットキーを割り当て<br>・譜面明細画面のページ切替に関するショートカットを廃止|
|[v24.4.0](https://github.com/cwtickle/danoniplus/releases/tag/v24.4.0)|・キーコンフィグ画面のPlayボタンにショートカットキーを割り当て|
|[v23.3.1](https://github.com/cwtickle/danoniplus/releases/tag/v23.3.1)|・ReverseをRキー、Scrollを上下キーに変更|
|[v23.2.0](https://github.com/cwtickle/danoniplus/releases/tag/v23.2.0)|・Adjustmentのショートカット割り当て仕様を変更<br>( <kbd>Shift</kbd>+<kbd>+ / -</kbd> ±5frame / <kbd>+ / -</kbd> ±1frame / <kbd>Alt</kbd>+<kbd>+ / -</kbd> ±0.5frame / <kbd>Alt</kbd>+<kbd>Shift</kbd>+<kbd>+ / -</kbd> ±0.1frame)|
|[v23.1.0](https://github.com/cwtickle/danoniplus/releases/tag/v23.1.0)|・Speedについて±0.05倍速刻みのショートカットキーを割り当て<br>・Enterキーに割り当てている操作に対してテンキーにも対応|
|[v23.0.0](https://github.com/cwtickle/danoniplus/releases/tag/v23.0.0)|・Adjustmentのショートカット割り当て仕様を変更<br>( <kbd>Shift</kbd>+<kbd>+ / -</kbd> ±3frame / <kbd>+ / -</kbd> ±0.5frame / <kbd>Alt</kbd>+<kbd>+ / -</kbd> ±0.1frame)|
|[v21.5.0](https://github.com/cwtickle/danoniplus/releases/tag/v21.5.0)|・譜面明細表示の逆回しに対するショートカットキーを割り当て<br>・タイトル画面、結果画面において「Ctrl+C」(コピー)が機能するように空のショートカットキーを割り当て|
|[v21.0.0](https://github.com/cwtickle/danoniplus/releases/tag/v21.0.0)|・譜面セレクター（子画面）のショートカットキーを割り当て|
|[v20.4.0](https://github.com/cwtickle/danoniplus/releases/tag/v20.4.0)|・タイトル画面のヘルプ、コメント画面開閉ボタンに対応するキーを割り当て<br>・loadingIos項目を追加(iOS系専用開始待ち画面)|
|[v20.2.0](https://github.com/cwtickle/danoniplus/releases/tag/v20.2.0)|・キーコンフィグ画面以外の戻るボタンにShift+Tabを追加で割り当て|
|[v20.1.2](https://github.com/cwtickle/danoniplus/releases/tag/v20.1.2)|・Adjustment設定にテンキーの「+」「-」を追加で割り当て|
|[v19.5.0](https://github.com/cwtickle/danoniplus/releases/tag/v19.5.0)|・初回実装|