**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-h0070-playingX) | Japanese** 

[Home](./) &gt; [譜面ヘッダー仕様](dos_header) &gt; [プレイ時の初期設定](dos_header#-プレイ時の初期設定) &gt; [プレイ画面位置の設定](dos_header#プレイ画面位置の設定)

| [<- frzScopeFromAC](dos-h0088-frzScopeFromAC) || **playingX / playingY** | [playingWidth / playingHeight ->](dos-h0071-playingWidth) |

## playingX / playingY
- ゲーム表示エリアのX座標, Y座標の設定
- 共通設定 ⇒ [g_presetObj.playingX, playingY](dos-s0007-viewControl#ゲーム表示エリアのx-y座標-g_presetobjplayingx-g_presetobjplayingy)

### 使い方
```
|playingX=50|
|playingY=20|
```
### 説明
プレイ画面のうち、ゲーム表示エリアのX座標(playingX), Y座標(playingY)を設定します。  
デフォルトはどちらも0pxです。

#### playingXを変更した場合
<img src="https://user-images.githubusercontent.com/44026291/199850377-f4516af7-d8c4-452e-8561-4f6f4eade429.png" width="100%">

#### playingYを変更した場合
<img src="https://github.com/cwtickle/danoniplus/assets/44026291/bdf3d1a8-f951-485b-9d71-e7ab7c2fe757" width="50%">

### 関連項目
- [playingWidth / playingHeight](dos-h0071-playingWidth) [:pencil:](dos-h0071-playingWidth/_edit) ゲーム表示エリアの横幅・高さ
- [customCreditWidth](dos-h0083-customCreditWidth) [:pencil:](dos-h0071-playingWidth/_edit) カスタムクレジットエリアの横幅

### 更新履歴

|Version|変更内容|
|----|----|
|[v33.6.0](https://github.com/cwtickle/danoniplus/releases/tag/v33.6.0)|・初回実装 (playingY)|
|[v16.0.0](https://github.com/cwtickle/danoniplus/releases/tag/v16.0.0)|・初回実装 (playingX)|

| [<- frzScopeFromAC](dos-h0088-frzScopeFromAC) || **playingX / playingY** | [playingWidth / playingHeight ->](dos-h0071-playingWidth) |