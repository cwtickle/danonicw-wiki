**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-h0062-frzShadowColor) | Japanese** 

[Home](./) &gt; [譜面ヘッダー仕様](dos_header) &gt; [プレイ時の初期設定](dos_header#-プレイ時の初期設定) &gt; [矢印・フリーズアロー色の設定](dos_header#矢印フリーズアロー色の設定)

| [<- setShadowColor](dos-h0041-setShadowColor) | **frzShadowColor** | [defaultColorgrd ->](dos-h0061-defaultColorgrd) |

## frzShadowColor
- フリーズアローの矢印内側を塗りつぶす設定および色の設定

### 使い方
```
|frzShadowColor=Default|
|frzShadowColor=#ffcccc:#ffffff@radial-gradient,#9999ff$...|
|frzShadowColor2=#666666$#333333$#000000$#333333$#333333|

|frzShadowColor3=frzShadowColor2|  // 3譜面目は2譜面目のデータをコピー
```
### 説明
フリーズアローの矢印内側の塗りつぶし色に関する設定です。  
未指定の場合は強制的に「黒色」になります。  
2つ1セット(カンマ区切り)で、フリーズアローの「通常時」「ヒット時」を表します。  
さらに、"$"を続けて4つずつ記載することで、「setColor(setShadowColor)」に  
対応した矢印レーンごとにフリーズアローの塗りつぶし色を変えることができます。  

## 補足：省略時の色補完について
基本的には`frzColor`の色補完方法と同じです。
- 未指定の場合：  
`setShadowColor`で指定されている色を「通常時」「ヒット時」に割り当てます。

### 関連項目
- [**setColor**](dos-h0003-setColor) [:pencil:](dos-h0003-setColor/_edit) 矢印色
- [**frzColor**](dos-h0004-frzColor) [:pencil:](dos-h0004-frzColor/_edit) フリーズアロー色
- [setShadowColor](dos-h0041-setShadowColor) [:pencil:](dos-h0041-setShadowColor/_edit) 矢印の内側を塗りつぶす設定および色の設定
- [defaultFrzColorUse](dos-h0063-defaultFrzColorUse) [:pencil:](dos-h0063-defaultFrzColorUse/_edit) フリーズアロー初期色(frzColor)が未指定時の適用方法
- [グラデーション仕様](dos-c0001-gradation) [:pencil:](dos-c0001-gradation/_edit)

### 更新履歴

|Version|変更内容|
|----|----|
|[v31.2.0](https://github.com/cwtickle/danoniplus/releases/tag/v31.2.0)|・他譜面の`frzShadowColor`名を指定することで他譜面のデータを参照する機能を追加|
|[v21.1.0](https://github.com/cwtickle/danoniplus/releases/tag/v21.1.0)|・2譜面目以降の個別設定に対応（譜面分割＆譜面番号可変時）|
|[v21.0.0](https://github.com/cwtickle/danoniplus/releases/tag/v21.0.0)|・2譜面目以降の個別設定に対応|
|[v13.1.1](https://github.com/cwtickle/danoniplus/releases/tag/v13.1.1)|・矢印レーンの一部記述省略に対応 (defaultFrzColorUse実装に伴い)|
|[v13.0.0](https://github.com/cwtickle/danoniplus/releases/tag/v13.0.0)|・初回実装|

| [<- setShadowColor](dos-h0041-setShadowColor) | **frzShadowColor** | [defaultColorgrd ->](dos-h0061-defaultColorgrd) |