**[English](https://github.com/cwtickle/danoniplus-docs/wiki/IdReferenceIndex) | Japanese** 

| [< 実行時エラー一覧](./ErrorList) || **ID一覧** | [オブジェクト一覧 >](./ObjectReferenceIndex) |

# IDリファレンス
- Base Version: [**v40.0.1**](./Changelog-latest#4001-2025-03-01)  

## タイトル画面
<img src="./wiki/id01_title.png" width="100%">

- divBack
- lblArrow
- backTitleSprite - 背景モーション(タイトル)
  - backTitleSprite*N*
- lblTitle
- lblmusicTitle [[musicTitle](dos-h0001-musicTitle)] - 曲名
  - lblmusicTitle1 - 曲名(1行目)
  - lblmusicTitle2 - 曲名(2行目)
- lblWarning - 警告メッセージエリア
- btnStart [[lbl.clickHere](obj-v0015-g_lblNameObj#ページタイトル)] - スタートボタン
  - scStart
- btnReset - ローカルストレージのデータ管理遷移ボタン
- btnReload - リロードボタン
- btnHelp - ゲーム画面説明へのリンクボタン　※ver18.8.0より
- lnkMaker [[tuning](dos-h0017-tuning)] - 制作者リンク
- lnkArtist [[musicTitle](dos-h0001-musicTitle)]- アーティストリンク
- lnkVersion [[releaseDate](dos-h0036-releaseDate)]- バージョン番号、GitHubリンク
- lnkComparison - セキュリティポリシーリンク
- lblComment [[commentVal](dos-h0066-commentVal)] - コメントエリア
- btnComment [[lbl.comment](obj-v0015-g_lblNameObj#ページタイトル)] - コメントエリアの表示有無ボタン
- maskTitleSprite - マスクモーション(タイトル)
  - maskTitleSprite*N*

## データ管理画面
<img src="https://github.com/user-attachments/assets/40eb2cc8-70b1-4674-8343-e81be09c3fdf" width="65%">

- divBack
- lblTitle
- dataDelMsg
- optionsprite
  - lblWorkData
  - btnEnvironment
    - scEnvironment
  - btnHighscores
    - scHighscores
  - btnCustomKey
    - scCustomKey
  - btnOthers
    - scOthers
  - lblKeyData
  - lblTargetKey
  - keyListSprite
    - btn*KeyName*
    - btnView*KeyName*
- lblWorkDataView
- lblKeyDataView
- btnWorkStorage
- btnKeyStorage
- btnBack
  - scBack
- btnPrecond
- btnSafeMode
- btnReset
- btnUndo

## 前提条件画面
<img src="https://github.com/user-attachments/assets/85be72bb-8e29-4963-9aa3-d52f0e922aeb" width="65%">

- divBack
- lblTitle
- lblPrecondView
- btnPrecond*N*
- btnPrecondNext
- btnBack
  - scBack

## 設定画面
<img src="https://github.com/cwtickle/danoniplus/assets/44026291/0a1ba4d0-a729-4630-90d3-53e2e5c308df" width="100%">  
<img src="https://github.com/user-attachments/assets/fee34b35-3e89-4e1c-94a7-7b39128a9c9b" width="100%">  
<img src="https://github.com/cwtickle/danoniplus/assets/44026291/df9a60ff-c38d-407d-885f-3a04ca071b67" width="65%">

- divBack
- lblTitle
- optionsprite - 設定描画エリア
  - difficultySprite
    - lblDifficulty [[lbl.Difficulty](obj-v0015-g_lblNameObj#設定画面ラベル)]
    - lnkDifficulty
    - lnkDifficultyR
    - lnkDifficultyL
    - scDifficulty
  - speedSprite
    - lblSpeed [[lbl.Speed](obj-v0015-g_lblNameObj#設定画面ラベル)]
    - lnkSpeed
    - lnkSpeedR
    - lnkSpeedL
    - lnkSpeedRR
    - lnkSpeedLL
    - lnkSpeedHR - ショートカットキー専用（非表示）
    - lnkSpeedHL - ショートカットキー専用（非表示）
    - scSpeed
    - btnGraph - 譜面詳細子画面の開閉ボタン
      - scGraph
    - scDifficulty
  - motionSprite
    - lblMotion [[lbl.Motion](obj-v0015-g_lblNameObj#設定画面ラベル)]
    - lnkMotion
    - lnkMotionR
    - lnkMotionL
    - scMotion
  - reverseSprite - Reverse設定エリア：拡張スクロール設定(Scroll)が無効時にscrollSpriteの代わりに使用
    - lblReverse [[lbl.Reverse](obj-v0015-g_lblNameObj#設定画面ラベル)]
    - lnkReverse
    - lnkReverseR
    - lnkReverseL
    - scReverse
  - scrollSprite - Scroll設定エリア
    - lblScroll [[lbl.Scroll](obj-v0015-g_lblNameObj#設定画面ラベル)]
    - lnkScroll
    - lnkScrollR
    - lnkScrollL
    - btnReverse
    - scScroll
  - shuffleSprite
    - lblShuffle [[lbl.Shuffle](obj-v0015-g_lblNameObj#設定画面ラベル)]
    - lnkShuffle
    - lnkShuffleR
    - lnkShuffleL
    - scShuffle
  - autoPlaySprite
    - lblAutoPlay [[lbl.AutoPlay](obj-v0015-g_lblNameObj#設定画面ラベル)]
    - lnkAutoPlay
    - lnkAutoPlayR
    - lnkAutoPlayL
    - scAutoPlay
  - gaugeSprite
    - lblGauge [[lbl.Gauge](obj-v0015-g_lblNameObj#設定画面ラベル)]
    - lblGauge2
       - gaugeDivCover
         - lblGaugeDivTable - ゲージ詳細画面ヘッダー
           - lblGaugeStart [[lbl.g_start](obj-v0015-g_lblNameObj#設定画面ゲージ詳細表示)]
           - lblGaugeBorder [[lbl.g_border](obj-v0015-g_lblNameObj#設定画面ゲージ詳細表示)]
           - lblGaugeRecovery [[lbl.g_recovery](obj-v0015-g_lblNameObj#設定画面ゲージ詳細表示)]
           - lblGaugeDamage [[lbl.g_damage](obj-v0015-g_lblNameObj#設定画面ゲージ詳細表示)]
           - lblGaugeRate [[lbl.g_rate](obj-v0015-g_lblNameObj#設定画面ゲージ詳細表示)]
         - dataGaugeDivTable - ゲージ詳細画面数値部分
           - dataGaugeStart
           - dataGaugeBorder
           - dataGaugeRecovery
           - dataGaugeDamage
           - dataGaugeRate
    - lnkGauge
    - lnkGaugeR
    - lnkGaugeL
    - scGauge
    - lnkExcessive - 空押し判定の有効/無効設定
  - adjustmentSprite
    - lblAdjustment [[lbl.Adjustment](obj-v0015-g_lblNameObj#設定画面ラベル)]
    - lnkAdjustment
    - lnkAdjustmentR
    - lnkAdjustmentL
    - lnkAdjustmentRR
    - lnkAdjustmentLL
    - lnkAdjustmentRRR - 最内側のボタン
    - lnkAdjustmentLLL - 最内側のボタン
    - lnkAdjustmentHR - ショートカットキー専用（非表示）
    - lnkAdjustmentHL - ショートカットキー専用（非表示）
    - scAdjustment
  - fadeinSprite
    - lblFadein [[lbl.Fadein](obj-v0015-g_lblNameObj#設定画面ラベル)]
    - lnkFadein
    - lnkFadeinR
    - lnkFadeinL
    - lblFadeinBar
    - fadeinSlider
  - volumeSprite
    - lblVolume [[lbl.Volume](obj-v0015-g_lblNameObj#設定画面ラベル)]
    - lnkVolume
    - lnkVolumeR
    - lnkVolumeL
    - scVolume
  - scoreDetail - 譜面詳細エリア
    - lnkSpeedG - 速度変化グラフ切替ボタン
      - scSpeedG
    - lnkDensityG - 譜面密度グラフ切替ボタン
      - scDensityG
    - lnkToolDifG - 譜面レベル表示切替ボタン
      - scToolDifG
    - lnkHighScoreG - ハイスコア表示切替ボタン
      - scHighScoreG
    - detailSpeed - 速度変化グラフ描画エリア
      - graphSpeed
      - lblspeedS [[lbl.s_speed](obj-v0015-g_lblNameObj#設定画面速度密度グラフツール値表示)]
      - dataspeedS
      - lblboostS [[lbl.s_boost](obj-v0015-g_lblNameObj#設定画面速度密度グラフツール値表示)]
      - databoostS
      - lblavgS [[lbl.s_avg](obj-v0015-g_lblNameObj#設定画面速度密度グラフツール値表示)]
      - dataavgS
      - lblavgDspeed [[lbl.s_avgDspeed](obj-v0015-g_lblNameObj#設定画面速度密度グラフツール値表示)]
      - dataavgDspeed
      - lblavgDboost [[lbl.s_avgDboost](obj-v0015-g_lblNameObj#設定画面速度密度グラフツール値表示)]
      - dataavgDboost
    - detailDensity - 譜面密度グラフ描画エリア
      - graphDensity
      - lblAPM [[lbl.s_apm](obj-v0015-g_lblNameObj#設定画面速度密度グラフツール値表示)]
      - dataAPM
      - lblTime [[lbl.s_time](obj-v0015-g_lblNameObj#設定画面速度密度グラフツール値表示)]
      - dataTime
      - lblArrow [[lbl.s_arrow](obj-v0015-g_lblNameObj#設定画面速度密度グラフツール値表示)]
      - dataArrow
      - lblFrz [[lbl.s_frz](obj-v0015-g_lblNameObj#設定画面速度密度グラフツール値表示)]
      - dataFrz
    - detailToolDif - レベル計算ツール値描画エリア
      - lblToolDif [[lbl.s_level](obj-v0015-g_lblNameObj#設定画面速度密度グラフツール値表示)]
      - dataToolDif
      - lblDouji [[lbl.s_douji](obj-v0015-g_lblNameObj#設定画面速度密度グラフツール値表示)]
      - lblTate [[lbl.s_tate](obj-v0015-g_lblNameObj#設定画面速度密度グラフツール値表示)]
      - dataDouji
      - dataTate
      - lblArrowInfo [[lbl.s_cnts](obj-v0015-g_lblNameObj#設定画面速度密度グラフツール値表示)]
      - dataArrowInfo
      - lblArrowInfo2 [[lbl.s_linecnts](obj-v0015-g_lblNameObj#設定画面速度密度グラフツール値表示)]
      - dataArrowInfo2
      - lnkDifInfo [[lbl.s_print](obj-v0015-g_lblNameObj#設定画面速度密度グラフツール値表示)]
        - scDifInfo
    - detailHighScore - ハイスコア表示描画エリア
      - lblHIi
      - lblHIiS
      - lblHShakin
      - lblHShakinS
      - lblHMatari
      - lblHMatariS
      - lblHShobon
      - lblHShobonS
      - lblHUwan
      - lblHUwanS
      - lblHKita
      - lblHKitaS
      - lblHIknai
      - lblHIknaiS
      - lblHMaxCombo
      - lblHMaxComboS
      - lblHFmaxCombo
      - lblHFmaxComboS
      - lblHScore
      - lblHScoreS
      - lblHFast
      - lblHFastS
      - lblHSlow
      - lblHSlowS
      - lblHAdj
      - lblHAdjS
      - lblHAdjF
      - lblHExcessive
      - lblHExcessiveS
      - lblHRank
      - lblHDateTime
      - lblHMarks
      - lblHClearLamps
      - lblHShuffle
      - lblHAssist
      - lblHAnother
      - lnkResetHighScore - Resetボタン [[lbl.s_resetResult](./obj-v0015-g_lblNameObj)]
      - lnkHighScore - CopyResultボタン [[lbl.s_result](./obj-v0015-g_lblNameObj)]
  - difList - 譜面選択用セレクター(右側)
    - dif*N*
  - difCover - 譜面選択用セレクター(左側)
    - difFilter
      - keyFilter*N*
    - lblDifCnt
    - difRandom
    - keyFilter
  - btnDifU
  - btnDifD
- lblBaseSpd
- btnBack [[lbl.b_back](obj-v0015-g_lblNameObj#画面移動系ボタン)]
  - scBack
- btnKeyConfig [[lbl.b_keyConfig](obj-v0015-g_lblNameObj#画面移動系ボタン)]
  - scKeyConfig
- btnPlay [[lbl.b_play](obj-v0015-g_lblNameObj#画面移動系ボタン)]
  - scPlay
- btnDisplay
  - scDisplay
- btnSave [[lbl.dataSave](obj-v0015-g_lblNameObj#固定ボタン)]
  - scSave
- btnReset - ローカルストレージのデータ管理遷移ボタン
- btnPrecond

## Display画面
<img src="https://github.com/cwtickle/danoniplus/assets/44026291/f98c6277-65c0-459e-979d-730a53e67953" width="100%">

- divBack
- lblTitle
- optionsprite - 設定描画エリア
  - appearanceSprite
    - lblAppearance [[lbl.Appearance](obj-v0015-g_lblNameObj#設定画面display画面)]
    - lnkAppearance
    - lnkAppearanceR
    - lnkAppearanceL
    - scAppearance
    - lblAppearancePos
    - lblAppearanceBar
    - lnkLockBtn
  - opacitySprite
    - lblOpacity [[lbl.Opacity](obj-v0015-g_lblNameObj#設定画面display画面)]
    - lnkOpacity
    - lnkOpacityR
    - lnkOpacityL
    - scOpacity
  - hitPositionSprite
    - lblHitPosition [[lbl.Opacity](obj-v0015-g_lblNameObj#設定画面display画面)]
    - lnkHitPosition
    - lnkHitPositionR
    - lnkHitPositionL
    - lnkHitPositionRR
    - lnkHitPositionLL
    - scHitPosition
  - displaySprite
    - lnkstepZone [[lbl.d_StepZone](obj-v0015-g_lblNameObj#設定画面display画面)]
      - scstepZone
    - lnkstepZoneR
    - lnkjudgment [[lbl.d_Judgment](obj-v0015-g_lblNameObj#設定画面display画面)]
      - scjudgment
    - lnkfastSlow [[lbl.d_FastSlow](obj-v0015-g_lblNameObj#設定画面display画面)]
      - scfastSlow
    - lnklifeGauge [[lbl.d_LifeGauge](obj-v0015-g_lblNameObj#設定画面display画面)]
      - sclifeGauge
    - lnkscore [[lbl.d_Score](obj-v0015-g_lblNameObj#設定画面display画面)]
      - scscore
    - lnkmusicInfo [[lbl.d_MusicInfo](obj-v0015-g_lblNameObj#設定画面display画面)]
      - scmusicInfo
    - lnkfilterLine [[lbl.d_FilterLine](obj-v0015-g_lblNameObj#設定画面display画面)]
      - scfilterLine
    - lnkspeed [[lbl.d_Speed](obj-v0015-g_lblNameObj#設定画面display画面)]
      - scspeed
    - lnkcolor [[lbl.d_Color](obj-v0015-g_lblNameObj#設定画面display画面)]
      - sccolor
    - lnklyrics [[lbl.d_Lyrics](obj-v0015-g_lblNameObj#設定画面display画面)]
      - sclyrics
    - lnkbackground [[lbl.d_Background](obj-v0015-g_lblNameObj#設定画面display画面)]
      - scbackground
    - lnkarrowEffect [[lbl.d_ArrowEffect](obj-v0015-g_lblNameObj#設定画面display画面)]
      - scarrowEffect
    - lnkspecial [[lbl.d_Special](obj-v0015-g_lblNameObj#設定画面display画面)]
      - scspecial
- sdDesc - Display画面の説明エリア
- scMsg - Display画面の注意点表示エリア
- btnBack [[lbl.b_back](obj-v0015-g_lblNameObj#画面移動系ボタン)]
  - scBack
- btnKeyConfig [[lbl.b_keyConfig](obj-v0015-g_lblNameObj#画面移動系ボタン)]
  - scKeyConfig
- btnPlay [[lbl.b_play](obj-v0015-g_lblNameObj#画面移動系ボタン)]
  - scPlay
- btnSettings
  - scSettings
- btnSave [[lbl.dataSave](obj-v0015-g_lblNameObj#固定ボタン)]
  - scSave
- btnReset - ローカルストレージのデータ管理遷移ボタン
- btnPrecond

## 拡張設定画面 (Ex-Settings)

- divBack
- lblTitle
- optionsprite - 設定描画エリア
  - playWindowSprite
    - lblPlayWindow
    - lnkPlayWindow
    - lnkPlayWindowR
    - lnkPlayWindowL
    - scPlayWindow
  - stepAreaSprite
    - lblStepArea
    - lnkStepArea
    - lnkStepAreaR
    - lnkStepAreaL
    - scStepArea
  - frzReturnSprite
    - lblFrzReturn
    - lnkFrzReturn
    - lnkFrzReturnR
    - lnkFrzReturnL
    - scFrzReturn
  - shakingSprite
    - lblShaking
    - lnkShaking
    - lnkShakingR
    - lnkShakingL
    - scShaking
  - effectSprite
    - lblEffect
    - lnkEffect
    - lnkEffectR
    - lnkEffectL
    - scEffect
  - camoufrageSprite
    - lblCamoufrage
    - lnkCamoufrage
    - lnkCamoufrageR
    - lnkCamoufrageL
    - scCamoufrage
  - swappingSprite
    - lblSwapping
    - lnkSwapping
    - lnkSwappingR
    - lnkSwappingL
    - scSwapping
  - judgRangeSprite
    - lblJudgRange
    - lnkJudgRange
    - lnkJudgRangeR
    - lnkJudgRangeL
    - scJudgRange
    - lblJudgRangeView
  - autoRetrySprite
    - lblAutoRetry
    - lnkAutoRetry
    - lnkAutoRetryR
    - lnkAutoRetryL
    - scAutoRetry
- btnBack [[lbl.b_back](obj-v0015-g_lblNameObj#画面移動系ボタン)]
  - scBack
- btnKeyConfig [[lbl.b_keyConfig](obj-v0015-g_lblNameObj#画面移動系ボタン)]
  - scKeyConfig
- btnPlay [[lbl.b_play](obj-v0015-g_lblNameObj#画面移動系ボタン)]
  - scPlay
- btnexSetting
  - scexSetting
- btnSave [[lbl.dataSave](obj-v0015-g_lblNameObj#固定ボタン)]
  - scSave
- btnReset - ローカルストレージのデータ管理遷移ボタン
- btnPrecond

## キーコンフィグ画面
<img src="https://user-images.githubusercontent.com/44026291/226300303-dc94b7fa-37ce-4740-8c94-8eb38d1f7738.png" width="70%">  
<img src="https://user-images.githubusercontent.com/44026291/226300458-69eab142-a16a-49f6-b505-07d7b5111bdc.png" width="70%">  

- divBack
- lblTitle
- kcDesc - キーコンフィグ画面上部の説明エリア
- kcShuffleDesc - キーコンフィグ画面左下の説明エリア
- keyconSprite
  - color*N*
  - arrowShadow*N*
  - arrow*N*
  - sArrow*N*
  - keycon*N*_*P*
  - cursor
- scMsg - ショートカットキーの説明エリア
- scTitleBack - タイトルバックのショートカットキー変更ボタン
- scRetry - リトライのショートカットキー変更ボタン
- kcMsg - 別キーモードの説明エリア
- lblcolorType [[lbl.ColorType](obj-v0015-g_lblNameObj#キーコンフィグ画面)]
- lnkColorType
- lnkColorTypeL
- lnkColorTypeR
- lblImgType [[lbl.ImgType](obj-v0015-g_lblNameObj#キーコンフィグ画面)]
- lnkImgType
- lnkImgTypeL
- lnkImgTypeR
- lblcolorGroup [[lbl.ColorGroup](obj-v0015-g_lblNameObj#キーコンフィグ画面)]
- lnkColorGroup
- lnkColorGroupL
- lnkColorGroupR
- lblshuffleGroup [[lbl.ShuffleGroup](obj-v0015-g_lblNameObj#キーコンフィグ画面)]
- lnkShuffleGroup
- lnkShuffleGroupL
- lnkShuffleGroupR
- lblstepRtnGroup [[lbl.StepRtnGroup](obj-v0015-g_lblNameObj#キーコンフィグ画面)]
- lnkStepRtnGroup
- lnkStepRtnGroupL
- lnkStepRtnGroupR
- colorPickSprite
  - lnkColorR
  - lnkColorCopy
  - lblPickArrow
  - lblPickFrz
  - lnkColorReset
  - pickarrow*N*
  - pickarrowShadow*N*
  - pickfrz*N*
  - pickfrzBar*N*
- lblKey
- key*N*
- btnBack
  - scBack
- lblPattern [[lbl.KeyPattern](obj-v0015-g_lblNameObj#キーコンフィグ画面)]
- btnPtnChangeR
- btnPtnChangeL
- btnPtnChangeRR
- btnPtnChangeLL
- btnReset [[lbl.b_reset](obj-v0015-g_lblNameObj#画面移動系ボタン)]
- btnPlay [[lbl.b_play](obj-v0015-g_lblNameObj#画面移動系ボタン)]

## ロード画面
- divBack
- lblLoading
- lblWarning - 警告メッセージエリア
- btnPlay [[lbl.b_play](obj-v0015-g_lblNameObj#画面移動系ボタン)]

## メイン画面
- divBack
- backSprite - 背景モーション(メイン)
  - backSprite*N*
- mainSprite
  - filterBar*N* - Appearance用の境界線フィルター
  - borderBar*N* - 矢印描画（最大）境界線フィルター
  - filterView - Appearance(Hidden+/Sudden+)の表示率
  - mainSprite*N*
    - stepSprite*N*
      - stepRoot*N* - ステップゾーン
        - stepShadow*N* - 本体の影部分
        - step*N* - 本体
        - stepDiv*N* - 空打ち時のオブジェクト
        - stepHit*N* - ヒット時のオブジェクト
      - stepBar - Flat時のステップゾーンの代わり
    - arrowSprite*N*
      - *arrowNameN*_*Cnt* - 矢印本体
        - *arrowName*Shadow*N*_*Cnt*
        - *arrowName*Top*N*_*Cnt*
      - *frzNameN*_*Cnt* - フリーズアロー本体
        - *frzName*Bar*N*_*Cnt*
        - *frzName*TopRoot*N*_*Cnt*
          - *frzName*TopShadow*N*_*Cnt*
          - *frzName*Top*N*_*Cnt*
        - *frzName*BtmRoot*N*_*Cnt*
          - *frzName*BtmShadow*N*_*Cnt*
          - *frzName*Btm*N*_*Cnt*
    - frzHitSprite*N*
      - frzHit*N* - フリーズアローヒット部分
        - frzHitShadow*N*
        - frzHitTop*N*
- infoSprite
  - lblLife - ライフ（数字）
  - lifeBackObj - ライフ背景オブジェクト
  - lifeBar - ライフゲージ本体
  - lifeBorderObj - ライフのノルマライン
- judgeSprite
  - wordSprite - 歌詞表示エリア
    - lblword*N*
  - lblCredit - 楽曲クレジット
  - lblDifName - 譜面名（一部楽曲クレジット）
  - lblTime1 - 現在の経過時間
  - lblTime2 - 総時間
  - lblRetry ※
  - lblRetrySc ※
  - lblTitleBack ※
  - lblTitleBackSc ※
  - charaJ - 判定キャラクタ（矢印）
  - comboJ - コンボ数（矢印）
  - diffJ - Fast/Slow表示エリア（矢印）
  - charaFJ - 判定キャラクタ（フリーズアロー）
  - comboFJ - コンボ数（フリーズアロー）
  - diffFJ - Fast/Slow表示エリア（フリーズアロー始点）
  - lbl*Judge* - 判定別数
  - finishView - 終了時演出
- maskSprite - マスクモーション(メイン)
  - maskSprite*N*
- lblFrame - 現在のフレーム数
- lblReady - Ready表示

※プレイ時ショートカットキーが既定と異なる場合のみ表示

## 結果画面
<img src="./wiki/id07_result.png" width="100%">

- divBack
- backResultSprite - 背景モーション(結果)
  - backResultSprite*N*
- lblTitle
- playDataWindow
  - lblMusic - 曲名(ラベル)
  - lblMusicData - 曲名(1行目)
  - lblMusicData2 - 曲名(2行目)
  - lblDifficulty - 難易度(ラベル)
  - lblDifData - キー数、難易度名（Shuffle設定）
  - lblStyle - プレイスタイル（ラベル）
  - lblStyleData - プレイスタイル（実際の設定）
  - lblDisplay - Display設定（ラベル）
  - lblDisplayData - Display設定（1行目）
  - lblDisplay2Data - Display設定（2行目）
- resultWindow
  - lbl*Judge* [[lbl.j_ii, ...](obj-v0015-g_lblNameObj#判定キャラクタパーフェクト演出)] - 判定キャラクタ
  - lbl*Judge*S - 判定キャラクタに対する判定数
  - lblFast [[lbl.j_fast](obj-v0015-g_lblNameObj#判定キャラクタパーフェクト演出)] - Fast（ラベル）
  - lblSlow [[lbl.j_slow](obj-v0015-g_lblNameObj#判定キャラクタパーフェクト演出)] - Slow（ラベル）
  - lblFastS - Fast値
  - lblSlowS - Slow値
  - lblAdj [[lbl.j_adj](obj-v0015-g_lblNameObj#判定キャラクタパーフェクト演出)] - 推定Adj値（ラベル）
  - lblAdjS - 推定Adj値
  - lblExcessive [[lbl.j_excessive](obj-v0015-g_lblNameObj#判定キャラクタパーフェクト演出)] - 空押し数（ラベル）
  - lblExcessiveS - 空押し数
  - lblRank - ランク
  - lbl*Judge*L1 - ハイスコア差分（左括弧）
  - lbl*Judge*LS - ハイスコア差分値
  - lbl*Judge*L2 - ハイスコア差分（右括弧）
  - lblAutoView - Autoプレイ時に`(No Record)`を表示
- lblResultPre - Clear/Failed表示（モーション付き）
- lblResultPre2 - Clear/Failed表示（固定）
- btnBack [[lbl.b_back](obj-v0015-g_lblNameObj#画面移動系ボタン)]
  - scBack
- btnCopy [[lbl.b_copy](obj-v0015-g_lblNameObj#画面移動系ボタン)] - リザルトデータをクリップボードへコピーするボタン
  - scCopy
- btnTweet [[lbl.b_tweet](obj-v0015-g_lblNameObj#画面移動系ボタン)] - Twitterへリザルトデータを転送するボタン
  - scTweet
- btnGitter [[lbl.b_gitter](obj-v0015-g_lblNameObj#画面移動系ボタン)] - Gitter（得点報告用）へのリンクボタン　※ver18.8.0より
  - scGitter
- btnRetry [[lbl.b_retry](obj-v0015-g_lblNameObj#画面移動系ボタン)] - リトライボタン
  - scRetry
- btnCopyImage - リザルト画像コピー用ボタン
- maskResultSprite - マスクモーション(メイン)
  - maskResultSprite*N*

| [< 実行時エラー一覧](./ErrorList) || **ID一覧** | [オブジェクト一覧 >](./ObjectReferenceIndex) |
