[← オブジェクトリファレンスに戻る](ObjectReferenceIndex)  

# setShortcutEvent
### 概要
- ショートカットキーを割り当て、実行を管理する関数。
- `document.onKeyDown`, `document.onKeyUp`の上書き処理を行う。  
上書き処理のタイミングは、[g_btnWaitFrame](obj-v0020-g_btnWaitFrame)を参照して、有効化時間経過後に反映する。  
反映するタイミングで画面の切り替えが発生した場合は、上書きしないようになっている。
- ブラウザでサポートするショートカットキーの制御もここで行っている。オブジェクト引数で制御可能。
- ショートカットキーに対応したボタン操作を定義するため、対象のボタンをこの関数が実行される前に定義しておく必要がある。

### 引数（括弧内はデフォルト値）

|引数|型|必須|指定内容|
|----|----|----|----|
|_displayName|string|*|画面名|
|_func|function||ショートカットキー処理後に実行するonKeyDown処理|

### オブジェクト引数
- オブジェクト引数は全て任意。指定が無い場合はデフォルト値が採用される。

|引数|型|デフォルト|用途|
|----|----|----|----|
|displayFlg|boolean|true|対応するショートカットキーを表示するかどうか。<br>デフォルトは`true`(表示する)。|
|dfEvtFlg|boolean|false|表示している画面でブラウザが採用しているショートカットキー操作を有効にするかどうか。例えばF5リロード、F1ヘルプなど。<br>デフォルトは`false`(無効にする)。<br>※WindowsキーなどOS既存のものは無効化されない。|

### 返却値
- なし

### 関数の依存関係
- **setShortcutEvent**
  - [createScTextCommon](fnc-c0034-createScTextCommon)
    - [createScText](fnc-c0008-createScText)
    - [g_btnPatterns](obj-v0021-g_btnPatterns)
  - [commonKeyDown](fnc-c0007-commonKeyDown)
    - [g_shortcutObj](obj-v0017-g_shortcutObj)
    - [keyIsDown](fnc-c0023-keyIsDown)
  - commonKeyUp

### 使用例
```javascript
setShortcutEvent(`title`);
```

### 関連項目
- [**g_shortcutObj**](obj-v0017-g_shortcutObj)
- [g_btnWaitFrame](obj-v0020-g_btnWaitFrame)
- [commonKeyDown](fnc-c0007-commonKeyDown)

### 更新履歴

|Version|変更内容|
|----|----|
|[v27.6.0](https://github.com/cwtickle/danoniplus/releases/tag/v27.6.0)|・オブジェクト引数を追加|
|[v20.4.0](https://github.com/cwtickle/danoniplus/releases/tag/v20.4.0)|・`createScTextCommon`関数を包含するよう変更|
|[v19.5.0](https://github.com/cwtickle/danoniplus/releases/tag/v19.5.0)|・初回実装|