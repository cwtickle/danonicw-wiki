⭐ New Features / 🛠️ Improvements / 🐞 Bug Fixes

[**<- v2**](Changelog-v2) | **v1** | [**v0 ->**](Changelog-v0)  
(**🔖 [41 Releases](https://github.com/cwtickle/danoniplus/releases?q=tag%3Av1)** )

## [Warning] 更新終了後の不具合情報
- [更新を終了したバージョンの不具合情報#v1](DeprecatedVersionBugs#v1) を参照

## v1.15.17 ([2019-10-08](https://github.com/cwtickle/danoniplus/releases/tag/v1.15.17))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v1.15.17/total)](https://github.com/cwtickle/danoniplus/archive/v1.15.17.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v1.15.17/support/v1?style=social)](https://github.com/cwtickle/danoniplus/compare/v1.15.17...support/v1#files_bucket)
- 🐞 速度変化が同一フレームにあると正常に動作しなくなる不具合を修正 ( PR [#477](https://github.com/cwtickle/danoniplus/pull/477) ) <- :boom: **initial**

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v1.15.17)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v1.15.16...v1.15.17#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v1.15.17)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v1.15.17.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v1.15.17.tar.gz) )
/ 🎣 [**v8.7.3**](./Changelog-v8#v873-2019-10-08)

## v1.15.16 ([2019-10-06](https://github.com/cwtickle/danoniplus/releases/tag/v1.15.16))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v1.15.16/total)](https://github.com/cwtickle/danoniplus/archive/v1.15.16.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v1.15.16/support/v1?style=social)](https://github.com/cwtickle/danoniplus/compare/v1.15.16...support/v1#files_bucket)
- 🐞 エスケープ文字の適用順序誤りを修正 ( PR [#472](https://github.com/cwtickle/danoniplus/pull/472), [#473](https://github.com/cwtickle/danoniplus/pull/473) ) <- :boom: [**v0.66.x**](Changelog-v0#v066x-2018-11-16)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v1.15.16)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v1.15.15...v1.15.16#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v1.15.16)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v1.15.16.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v1.15.16.tar.gz) )
/ 🎣 [**v8.7.2**](./Changelog-v8#v872-2019-10-06)

## v1.15.15 ([2019-09-23](https://github.com/cwtickle/danoniplus/releases/tag/v1.15.15))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v1.15.15/total)](https://github.com/cwtickle/danoniplus/archive/v1.15.15.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v1.15.15/support/v1?style=social)](https://github.com/cwtickle/danoniplus/compare/v1.15.15...support/v1#files_bucket)
- 🐞 ロード画面でEnterを押すと複数回ロードが発生する問題を修正 ( PR [#432](https://github.com/cwtickle/danoniplus/pull/432) ) <- :boom: **initial**
- 🐞 メイン画面で曲中リトライキーを連打した場合に譜面がずれることがある問題を修正 ( PR [#433](https://github.com/cwtickle/danoniplus/pull/433) ) <- :boom: **initial**

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v1.15.15)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v1.15.14...v1.15.15#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v1.15.15)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v1.15.15.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v1.15.15.tar.gz) )
/ 🎣 [**v8.0.4**](./Changelog-v8#v804-2019-09-23)

## v1.15.14 ([2019-09-16](https://github.com/cwtickle/danoniplus/releases/tag/v1.15.14))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v1.15.14/total)](https://github.com/cwtickle/danoniplus/archive/v1.15.14.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v1.15.14/support/v1?style=social)](https://github.com/cwtickle/danoniplus/compare/v1.15.14...support/v1#files_bucket)
- 🐞 ゲームオーバー時の200ミリ秒遅延により、ハイスコア表示がおかしくなることがある問題を修正 ( PR [#428](https://github.com/cwtickle/danoniplus/pull/428) ) <- :boom: **initial**

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v1.15.14)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v1.15.13...v1.15.14#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v1.15.14)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v1.15.14.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v1.15.14.tar.gz) )
/ 🎣 [**v8.0.2**](./Changelog-v8#v802-2019-09-16)

## v1.15.13 ([2019-09-15](https://github.com/cwtickle/danoniplus/releases/tag/v1.15.13))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v1.15.13/total)](https://github.com/cwtickle/danoniplus/archive/v1.15.13.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v1.15.13/support/v1?style=social)](https://github.com/cwtickle/danoniplus/compare/v1.15.13...support/v1#files_bucket)
- 🐞 速度変化が補正込みで負のフレームにあるときの不具合を修正 ( PR [#426](https://github.com/cwtickle/danoniplus/pull/426) ) <- :boom: **initial**

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v1.15.13)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v1.15.12...v1.15.13#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v1.15.13)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v1.15.13.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v1.15.13.tar.gz) )
/ 🎣 [**v8.0.1**](./Changelog-v8#v801-2019-09-15)

## v1.15.12 ([2019-08-03](https://github.com/cwtickle/danoniplus/releases/tag/v1.15.12))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v1.15.12/total)](https://github.com/cwtickle/danoniplus/archive/v1.15.12.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v1.15.12/support/v1?style=social)](https://github.com/cwtickle/danoniplus/compare/v1.15.12...support/v1#files_bucket)
- 🐞 back_dataで0フレームが指定された場合に動作しないことがある問題を修正 ( PR [#388](https://github.com/cwtickle/danoniplus/pull/388) ) <- :boom: [**v0.65.x**](Changelog-v0#v065x-2018-11-16)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v1.15.12)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v1.15.11...v1.15.12#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v1.15.12)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v1.15.12.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v1.15.12.tar.gz) )
/ 🎣 [**v7.5.1**](./Changelog-v7#v751-2019-08-03)

## v1.15.11 ([2019-07-21](https://github.com/cwtickle/danoniplus/releases/tag/v1.15.11))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v1.15.11/total)](https://github.com/cwtickle/danoniplus/archive/v1.15.11.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v1.15.11/support/v1?style=social)](https://github.com/cwtickle/danoniplus/compare/v1.15.11...support/v1#files_bucket)
- 🐞 リザルト画面で、フェードアウト中にTitleBack/Retryするとフェードアウト状態が引き継がれてしまう問題を修正 ( PR [#384](https://github.com/cwtickle/danoniplus/pull/384) ) <- :boom: [**v0.68.x**](Changelog-v0#v068x-2018-11-17)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v1.15.11)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v1.15.10...v1.15.11#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v1.15.11)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v1.15.11.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v1.15.11.tar.gz) )
/ 🎣 [**v7.4.0**](./Changelog-v7#v740-2019-07-21)

## v1.15.10 ([2019-07-06](https://github.com/cwtickle/danoniplus/releases/tag/v1.15.10))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v1.15.10/total)](https://github.com/cwtickle/danoniplus/archive/v1.15.10.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v1.15.10/support/v1?style=social)](https://github.com/cwtickle/danoniplus/compare/v1.15.10...support/v1#files_bucket)
- 🐞 フェードイン時に個別加速が掛からない問題を修正 ( PR [#363](https://github.com/cwtickle/danoniplus/pull/363) ) <- :boom: **initial**

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v1.15.10)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v1.15.9...v1.15.10#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v1.15.10)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v1.15.10.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v1.15.10.tar.gz) )
/ 🎣 [**v6.5.1**](./Changelog-v6#v651-2019-07-06)

## v1.15.9 ([2019-06-18](https://github.com/cwtickle/danoniplus/releases/tag/v1.15.9))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v1.15.9/total)](https://github.com/cwtickle/danoniplus/archive/v1.15.9.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v1.15.9/support/v1?style=social)](https://github.com/cwtickle/danoniplus/compare/v1.15.9...support/v1#files_bucket)
- 🛠️ フリーズアローの枠外判定の修正・見直し（条件により止まってしまう問題の修正）( PR [#341](https://github.com/cwtickle/danoniplus/pull/341) )
- 🐞 フリーズアローのみの譜面が再生できない問題を修正 ( PR [#343](https://github.com/cwtickle/danoniplus/pull/343) ) <- :boom: **initial**

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v1.15.9)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v1.15.8...v1.15.9#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v1.15.9)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v1.15.9.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v1.15.9.tar.gz) )
/ 🎣 [**v5.12.2**](./Changelog-v5#v5122-2019-06-18)

## v1.15.8 ([2019-05-31](https://github.com/cwtickle/danoniplus/releases/tag/v1.15.8))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v1.15.8/total)](https://github.com/cwtickle/danoniplus/archive/v1.15.8.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v1.15.8/support/v1?style=social)](https://github.com/cwtickle/danoniplus/compare/v1.15.8...support/v1#files_bucket)
- 🐞 判定基準位置が1フレーム手前になっている問題を修正 ( PR [#318](https://github.com/cwtickle/danoniplus/pull/318), [#323](https://github.com/cwtickle/danoniplus/pull/323) ) <- :boom: **initial**

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v1.15.8)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v1.15.4...v1.15.8#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v1.15.8)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v1.15.8.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v1.15.8.tar.gz) )
/ 🎣 [**v5.6.6**](./Changelog-v5#v566-2019-05-31)

## v1.15.4 ([2019-05-11](https://github.com/cwtickle/danoniplus/releases/tag/v1.15.4))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v1.15.4/total)](https://github.com/cwtickle/danoniplus/archive/v1.15.4.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v1.15.4/support/v1?style=social)](https://github.com/cwtickle/danoniplus/compare/v1.15.4...support/v1#files_bucket)
- 🐞 ライフ回復・ダメージ量計算でフリーズアローのカウントが間違っていたのを修正 ( PR [#284](https://github.com/cwtickle/danoniplus/pull/284) ) <- :boom: [**v0.72.x**](Changelog-v0#v072x-2018-11-20)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v1.15.4)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v1.15.3...v1.15.4#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v1.15.4)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v1.15.4.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v1.15.4.tar.gz) )
/ 🎣 [**v4.8.1**](./Changelog-v4#v481-2019-05-11)

## v1.15.3 ([2019-04-27](https://github.com/cwtickle/danoniplus/releases/tag/v1.15.3))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v1.15.3/total)](https://github.com/cwtickle/danoniplus/archive/v1.15.3.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v1.15.3/support/v1?style=social)](https://github.com/cwtickle/danoniplus/compare/v1.15.3...support/v1#files_bucket)
- 🛠️ back_dataでX/Y座標の整数値制限を緩和（小数が使えるように）( PR [#254](https://github.com/cwtickle/danoniplus/pull/254) ) 
- 🐞 back_dataでOpacityが機能していない問題を修正 ( PR [#254](https://github.com/cwtickle/danoniplus/pull/254) ) <- :boom: [**v0.65.x**](Changelog-v0#v065x-2018-11-16)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v1.15.3)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v1.15.2...v1.15.3#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v1.15.3)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v1.15.3.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v1.15.3.tar.gz) )
/ 🎣 [**v4.0.1**](./Changelog-v4#v401-2019-04-27)

## v1.15.2 ([2019-02-12](https://github.com/cwtickle/danoniplus/releases/tag/v1.15.2))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v1.15.2/total)](https://github.com/cwtickle/danoniplus/archive/v1.15.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v1.15.2/support/v1?style=social)](https://github.com/cwtickle/danoniplus/compare/v1.15.2...support/v1#files_bucket)
- 🐞 フェードイン時間がblankFrameより小さい時にタイミングがずれる問題を修正 ( PR [#192](https://github.com/cwtickle/danoniplus/pull/192) ) <- :boom: [**v0.63.x**](Changelog-v0#v063x-2018-11-13)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v1.15.2)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v1.15.1...v1.15.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v1.15.2)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v1.15.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v1.15.2.tar.gz) )
/ 🎣 [**v2.6.1**](./Changelog-v2#v261-2019-02-12)

## v1.15.1 ([2019-01-24](https://github.com/cwtickle/danoniplus/releases/tag/v1.15.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v1.15.1/total)](https://github.com/cwtickle/danoniplus/archive/v1.15.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v1.15.1/support/v1?style=social)](https://github.com/cwtickle/danoniplus/compare/v1.15.1...support/v1#files_bucket)
- 🐞 フェードアウト中にリザルト画面へ移行させた場合の音量不具合を修正 ( PR [#167](https://github.com/cwtickle/danoniplus/pull/167) ) <- :boom: **initial**
- 🐞 初期ライフ量が常に整数値になるように修正 ( PR [#167](https://github.com/cwtickle/danoniplus/pull/167) ) <- :boom: **initial**

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v1.15.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v1.15.0...v1.15.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v1.15.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v1.15.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v1.15.1.tar.gz) )
/ 🎣 [**v2.0.2**](./Changelog-v2#v202-2019-01-24)

----

## v1.15.0 ([2019-01-14](https://github.com/cwtickle/danoniplus/releases/tag/v1.15.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v1.15.0/total)](https://github.com/cwtickle/danoniplus/archive/v1.15.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v1.15.0/support/v1?style=social)](https://github.com/cwtickle/danoniplus/compare/v1.15.0...support/v1#files_bucket)
- ⭐️ フルコンボ演出の表示切替を譜面側から行えるように変更 (finishView=none) ( PR [#161](https://github.com/cwtickle/danoniplus/pull/161) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v1.15.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v1.14.0...v1.15.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v1.15.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v1.15.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v1.15.0.tar.gz) )<br>❤️ izkdic

## v1.14.0 ([2019-01-12](https://github.com/cwtickle/danoniplus/releases/tag/v1.14.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v1.14.0/total)](https://github.com/cwtickle/danoniplus/archive/v1.14.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v1.14.0/support/v1?style=social)](https://github.com/cwtickle/danoniplus/compare/v1.14.0...support/v1#files_bucket)
- ⭐️ ステップゾーン間隔を譜面側から設定できるように変更 (blankX) ( PR [#160](https://github.com/cwtickle/danoniplus/pull/160) )
- ⭐️ 5key, 9A/9Bkeyのステップゾーン間隔のデフォルトを変更（5keyは広く、9A/9Bkeyは狭く） ( PR [#160](https://github.com/cwtickle/danoniplus/pull/160) ) 
- ⭐️ 既存キーに対して各種キー設定の上書きを可能に変更 ( PR [#160](https://github.com/cwtickle/danoniplus/pull/160) ) 

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v1.14.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v1.13.0...v1.14.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v1.14.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v1.14.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v1.14.0.tar.gz) )

## v1.13.0 ([2019-01-06](https://github.com/cwtickle/danoniplus/releases/tag/v1.13.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v1.13.0/total)](https://github.com/cwtickle/danoniplus/archive/v1.13.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v1.13.0/support/v1?style=social)](https://github.com/cwtickle/danoniplus/compare/v1.13.0...support/v1#files_bucket)
- ⭐️ 判定名をカスタムできるように変更 ( PR [#158](https://github.com/cwtickle/danoniplus/pull/158) ) 
- ⭐️ リザルト画面の右上のクリアマークについて、パーフェクト・フルコンボの表記ができるよう変更 ( PR [#158](https://github.com/cwtickle/danoniplus/pull/158) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v1.13.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v1.12.1...v1.13.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v1.13.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v1.13.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v1.13.0.tar.gz) )

## v1.12.1 ([2019-01-05](https://github.com/cwtickle/danoniplus/releases/tag/v1.12.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v1.12.1/total)](https://github.com/cwtickle/danoniplus/archive/v1.12.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v1.12.1/support/v1?style=social)](https://github.com/cwtickle/danoniplus/compare/v1.12.1...support/v1#files_bucket)
- 🛠️ 外部jsファイルの乱数制御方法変更（乱数→時刻制御）( PR [#157](https://github.com/cwtickle/danoniplus/pull/157) ) 
- 🛠️ Tweetスコアの表示変数変更（ローカル変数→グローバル変数）( PR [#157](https://github.com/cwtickle/danoniplus/pull/157) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v1.12.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v1.12.0...v1.12.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v1.12.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v1.12.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v1.12.1.tar.gz) )<br>❤️ izkdic

## v1.12.0 ([2019-01-04](https://github.com/cwtickle/danoniplus/releases/tag/v1.12.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v1.12.0/total)](https://github.com/cwtickle/danoniplus/archive/v1.12.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v1.12.0/support/v1?style=social)](https://github.com/cwtickle/danoniplus/compare/v1.12.0...support/v1#files_bucket)
- ⭐️ タイトル文字以外のフォントを設定できるように変更 ( PR [#156](https://github.com/cwtickle/danoniplus/pull/156), Issue [#124](https://github.com/cwtickle/danoniplus/pull/124) ) 
  - 譜面データ内：customFont にて設定（既定：Meiryo UI）
- ⭐️ 外部jsファイルのキャッシュを利用しないように変更 ( PR [#156](https://github.com/cwtickle/danoniplus/pull/156) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v1.12.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v1.11.1...v1.12.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v1.12.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v1.12.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v1.12.0.tar.gz) )

## v1.11.1 ([2019-01-03](https://github.com/cwtickle/danoniplus/releases/tag/v1.11.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v1.11.1/total)](https://github.com/cwtickle/danoniplus/archive/v1.11.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v1.11.1/support/v1?style=social)](https://github.com/cwtickle/danoniplus/compare/v1.11.1...support/v1#files_bucket)
- 🐞 Adjustmentがマイナス値のとき、曲終了時間が短くなる問題を修正 ( PR [#155](https://github.com/cwtickle/danoniplus/pull/155) ) <- :boom: **initial**

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v1.11.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v1.11.0...v1.11.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v1.11.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v1.11.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v1.11.1.tar.gz) )<br>❤️ izkdic

## v1.11.0 ([2019-01-02](https://github.com/cwtickle/danoniplus/releases/tag/v1.11.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v1.11.0/total)](https://github.com/cwtickle/danoniplus/archive/v1.11.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v1.11.0/support/v1?style=social)](https://github.com/cwtickle/danoniplus/compare/v1.11.0...support/v1#files_bucket)
- ⭐️ 判定処理のカスタム処理を分離 ( PR [#154](https://github.com/cwtickle/danoniplus/pull/154), Issue [#151](https://github.com/cwtickle/danoniplus/pull/151) ) 
- ⭐️ ライフ表示について、小数点切り捨てで表示するように変更 ( PR [#154](https://github.com/cwtickle/danoniplus/pull/154), Issue [#150](https://github.com/cwtickle/danoniplus/pull/150) - 2 ) 
- ⭐️ ライフのボーダーラインに使用するフォントサイズをjs側で指定 ( PR [#154](https://github.com/cwtickle/danoniplus/pull/154), Issue [#150](https://github.com/cwtickle/danoniplus/pull/150) - 3 )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v1.11.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v1.10.1...v1.11.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v1.11.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v1.11.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v1.11.0.tar.gz) )<br>❤️ izkdic

## v1.10.1 ([2019-01-01](https://github.com/cwtickle/danoniplus/releases/tag/v1.10.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v1.10.1/total)](https://github.com/cwtickle/danoniplus/archive/v1.10.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v1.10.1/support/v1?style=social)](https://github.com/cwtickle/danoniplus/compare/v1.10.1...support/v1#files_bucket)
- 🐞 歌詞表示(追加分)についてフェードイン・アウトが利いていない問題を修正 ( PR [#153](https://github.com/cwtickle/danoniplus/pull/153) ) <- :boom: **initial**

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v1.10.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v1.10.0...v1.10.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v1.10.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v1.10.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v1.10.1.tar.gz) )

## v1.10.0 ([2018-12-30](https://github.com/cwtickle/danoniplus/releases/tag/v1.10.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v1.10.0/total)](https://github.com/cwtickle/danoniplus/archive/v1.10.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v1.10.0/support/v1?style=social)](https://github.com/cwtickle/danoniplus/compare/v1.10.0...support/v1#files_bucket)
- ⭐️ 曲終了フレーム(endFrame)について、譜面別に指定できるように変更 ( PR [#149](https://github.com/cwtickle/danoniplus/pull/149) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v1.10.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v1.9.0...v1.10.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v1.10.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v1.10.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v1.10.0.tar.gz) )

## v1.9.0 ([2018-12-30](https://github.com/cwtickle/danoniplus/releases/tag/v1.9.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v1.9.0/total)](https://github.com/cwtickle/danoniplus/archive/v1.9.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v1.9.0/support/v1?style=social)](https://github.com/cwtickle/danoniplus/compare/v1.9.0...support/v1#files_bucket)
- ⭐️ 歌詞表示で、左揃え/中央揃え/右揃えの設定を可能に変更 ( PR [#148](https://github.com/cwtickle/danoniplus/pull/148) ) 
  - word_dataのキーワード指定でそれぞれ、[left][center][right]とすると変更が可能
- ⭐️ 歌詞表示種類を2から4に拡大 
- ⭐️ 歌詞表示位置を中央にするために、歌詞表示サイズを変更 (横幅 - 200px)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v1.9.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v1.8.0...v1.9.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v1.9.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v1.9.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v1.9.0.tar.gz) )<br>❤️ izkdic

## v1.8.0 ([2018-12-25](https://github.com/cwtickle/danoniplus/releases/tag/v1.8.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v1.8.0/total)](https://github.com/cwtickle/danoniplus/archive/v1.8.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v1.8.0/support/v1?style=social)](https://github.com/cwtickle/danoniplus/compare/v1.8.0...support/v1#files_bucket)
- ⭐️ Tweet内容にPlaystyle (Speed, Motion, Reverse, Gaugeの変更状況)を表示するように変更 ( PR [#144](https://github.com/cwtickle/danoniplus/pull/144) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v1.8.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v1.7.1...v1.8.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v1.8.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v1.8.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v1.8.0.tar.gz) )

## v1.7.1 ([2018-12-24](https://github.com/cwtickle/danoniplus/releases/tag/v1.7.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v1.7.1/total)](https://github.com/cwtickle/danoniplus/archive/v1.7.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v1.7.1/support/v1?style=social)](https://github.com/cwtickle/danoniplus/compare/v1.7.1...support/v1#files_bucket)
- 🛠️ 音楽ファイルとして、base64変換したmp3データ(jsファイル, txtファイル)を指定できるように変更 ( PR [#143](https://github.com/cwtickle/danoniplus/pull/143) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v1.7.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v1.7.0...v1.7.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v1.7.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v1.7.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v1.7.1.tar.gz) )

## v1.7.0 ([2018-12-24](https://github.com/cwtickle/danoniplus/releases/tag/v1.7.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v1.7.0/total)](https://github.com/cwtickle/danoniplus/archive/v1.7.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v1.7.0/support/v1?style=social)](https://github.com/cwtickle/danoniplus/compare/v1.7.0...support/v1#files_bucket)
- ⭐️ 音楽ファイルとして、base64変換したmp3データ(jsファイル)を指定できるように変更 ( PR [#140](https://github.com/cwtickle/danoniplus/pull/140) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v1.7.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v1.6.0...v1.7.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v1.7.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v1.7.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v1.7.0.tar.gz) )<br>❤️ azarakko (@azarakko)

## v1.6.0 ([2018-12-23](https://github.com/cwtickle/danoniplus/releases/tag/v1.6.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v1.6.0/total)](https://github.com/cwtickle/danoniplus/archive/v1.6.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v1.6.0/support/v1?style=social)](https://github.com/cwtickle/danoniplus/compare/v1.6.0...support/v1#files_bucket)
- ⭐️ ゲージ設定の名称変更（Borderless→Original）( PR [#138](https://github.com/cwtickle/danoniplus/pull/138) )  
- ⭐️ ゲージ設定「Light」の既定を見直し（ダメージ半減 → 回復量2倍）( PR [#138](https://github.com/cwtickle/danoniplus/pull/138) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v1.6.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v1.5.1...v1.6.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v1.6.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v1.6.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v1.6.0.tar.gz) )

## v1.5.1 ([2018-12-23](https://github.com/cwtickle/danoniplus/releases/tag/v1.5.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v1.5.1/total)](https://github.com/cwtickle/danoniplus/archive/v1.5.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v1.5.1/support/v1?style=social)](https://github.com/cwtickle/danoniplus/compare/v1.5.1...support/v1#files_bucket)
- 🐞 SuddenDeathで1ミスFailedにならない場合がある問題を修正 ( PR [#132](https://github.com/cwtickle/danoniplus/pull/132) ) <- :boom: [**v1.5.0**](Changelog-v1#v150-2018-12-23)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v1.5.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v1.5.0...v1.5.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v1.5.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v1.5.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v1.5.1.tar.gz) )

## v1.5.0 ([2018-12-23](https://github.com/cwtickle/danoniplus/releases/tag/v1.5.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v1.5.0/total)](https://github.com/cwtickle/danoniplus/archive/v1.5.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v1.5.0/support/v1?style=social)](https://github.com/cwtickle/danoniplus/compare/v1.5.0...support/v1#files_bucket)
- ⭐️ 設定のGaugeに「Easy」「Hard」（ノルマ制の場合）、「Light」（ライフ制の場合）を追加 ( PR [#130](https://github.com/cwtickle/danoniplus/pull/130) ) 
- ⭐️ ノルマ制オプション（Normal, Easy, Hard）に対して、個別にノルマ・回復・ダメージ・初期ライフを設定可能な変数を追加 
- ⭐️ Gaugeオプションにおいて、初期ライフ(Init)の表記を追加 
- ⭐️ Gaugeオプションにおいて、ノルマ(Border)の表記をパーセントから実ライフ値に変更 
- ⭐️ ノルマ(Border)を0にした場合、ライフが空のときに途中終了する仕様に変更 (Hardゲージ実装に合わせ)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v1.5.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v1.4.0...v1.5.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v1.5.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v1.5.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v1.5.0.tar.gz) )<br>❤️ MFV2 (@MFV2)

## v1.4.0 ([2018-12-13](https://github.com/cwtickle/danoniplus/releases/tag/v1.4.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v1.4.0/total)](https://github.com/cwtickle/danoniplus/archive/v1.4.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v1.4.0/support/v1?style=social)](https://github.com/cwtickle/danoniplus/compare/v1.4.0...support/v1#files_bucket)
- 🛠️ コード記述の見直し (機能変更なし) 
  - 厳密等価演算子 === と !== を使うように変更 ( PR [#126](https://github.com/cwtickle/danoniplus/pull/126) )
  - 真偽値の比較に等価演算子を使わないように変更 ( PR [#126](https://github.com/cwtickle/danoniplus/pull/126) )
  - 設定画面のSpeed, Adjustmentの上限・下限値の直接指定を見直し ( PR [#127](https://github.com/cwtickle/danoniplus/pull/127) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v1.4.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v1.3.0...v1.4.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v1.4.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v1.4.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v1.4.0.tar.gz) )<br>❤️ kuroclef (@kuroclef)

## v1.3.0 ([2018-12-11](https://github.com/cwtickle/danoniplus/releases/tag/v1.3.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v1.3.0/total)](https://github.com/cwtickle/danoniplus/archive/v1.3.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v1.3.0/support/v1?style=social)](https://github.com/cwtickle/danoniplus/compare/v1.3.0...support/v1#files_bucket)
- ⭐️ フリーズアローのヒット時のデフォルト色を変更 ( PR [#125](https://github.com/cwtickle/danoniplus/pull/125) )
- ⭐️ ゲージ設定に「Practice」を追加 (ライフ制選択時)  ( PR [#125](https://github.com/cwtickle/danoniplus/pull/125) ) 
- 🛠️ キーコンフィグのカーソル移動をアニメーション化 ( PR [#125](https://github.com/cwtickle/danoniplus/pull/125) )
- 🛠️ 譜面名の文字サイズ調整 (譜面名が長い場合に自動で文字を縮小) ( PR [#125](https://github.com/cwtickle/danoniplus/pull/125) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v1.3.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v1.2.0...v1.3.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v1.3.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v1.3.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v1.3.0.tar.gz) )

## v1.2.0 ([2018-12-04](https://github.com/cwtickle/danoniplus/releases/tag/v1.2.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v1.2.0/total)](https://github.com/cwtickle/danoniplus/archive/v1.2.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v1.2.0/support/v1?style=social)](https://github.com/cwtickle/danoniplus/compare/v1.2.0...support/v1#files_bucket)
- ⭐️ タイトルにリロードボタンを追加 (左上の「R」ボタン) ( PR [#120](https://github.com/cwtickle/danoniplus/pull/120) )
- ⭐️ danoni_custom.js にローカルバージョンを指定することでタイトル右下のバージョンに補記できるように変更 ( PR [#120](https://github.com/cwtickle/danoniplus/pull/120) )
- 🛠️ 一部オブジェクトで、zIndex指定されていた個所を削除 ( PR [#120](https://github.com/cwtickle/danoniplus/pull/120) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v1.2.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v1.1.4...v1.2.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v1.2.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v1.2.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v1.2.0.tar.gz) )

## v1.1.4 ([2018-12-03](https://github.com/cwtickle/danoniplus/releases/tag/v1.1.4))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v1.1.4/total)](https://github.com/cwtickle/danoniplus/archive/v1.1.4.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v1.1.4/support/v1?style=social)](https://github.com/cwtickle/danoniplus/compare/v1.1.4...support/v1#files_bucket)
- 🛠️ 曲時間表示の位置を修正 (10分以上の表示に対応) ( PR [#118](https://github.com/cwtickle/danoniplus/pull/118) )
- 🐞 独自キーの変数posXが数値ではなく文字列として取り込まれていた不具合を修正 ( PR [#119](https://github.com/cwtickle/danoniplus/pull/119) ) <- :boom: **initial**

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v1.1.4)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v1.1.3...v1.1.4#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v1.1.4)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v1.1.4.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v1.1.4.tar.gz) )<br>❤️ goe (@goe0)

## v1.1.3 ([2018-12-02](https://github.com/cwtickle/danoniplus/releases/tag/v1.1.3))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v1.1.3/total)](https://github.com/cwtickle/danoniplus/archive/v1.1.3.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v1.1.3/support/v1?style=social)](https://github.com/cwtickle/danoniplus/compare/v1.1.3...support/v1#files_bucket)
- 🛠️ Firefoxアクセス時の非推奨マークを削除 ( PR [#117](https://github.com/cwtickle/danoniplus/pull/117) )
- 🐞 Firefoxで楽曲が遅れて再生されることがある問題を修正 ( PR [#117](https://github.com/cwtickle/danoniplus/pull/117) ) <- :boom: **initial**

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v1.1.3)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v1.1.2...v1.1.3#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v1.1.3)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v1.1.3.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v1.1.3.tar.gz) )

## v1.1.2 ([2018-12-02](https://github.com/cwtickle/danoniplus/releases/tag/v1.1.2))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v1.1.2/total)](https://github.com/cwtickle/danoniplus/archive/v1.1.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v1.1.2/support/v1?style=social)](https://github.com/cwtickle/danoniplus/compare/v1.1.2...support/v1#files_bucket)
- 🐞 個別加速(boost_data)のみ、譜面やSpeed:OFFしてもデータリセットされない問題を修正 ( PR [#116](https://github.com/cwtickle/danoniplus/pull/116) ) <- :boom: **initial**

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v1.1.2)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v1.1.1...v1.1.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v1.1.2)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v1.1.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v1.1.2.tar.gz) )

## v1.1.1 ([2018-12-02](https://github.com/cwtickle/danoniplus/releases/tag/v1.1.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v1.1.1/total)](https://github.com/cwtickle/danoniplus/archive/v1.1.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v1.1.1/support/v1?style=social)](https://github.com/cwtickle/danoniplus/compare/v1.1.1...support/v1#files_bucket)
- 🐞 フリーズアローヒット時に後続矢印位置が補正されない問題を修正 ( PR [#115](https://github.com/cwtickle/danoniplus/pull/115) ) <- :boom: [**v1.0.2**](Changelog-v1#v102-2018-11-27)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v1.1.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v1.1.0...v1.1.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v1.1.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v1.1.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v1.1.1.tar.gz) )

## v1.1.0 ([2018-12-01](https://github.com/cwtickle/danoniplus/releases/tag/v1.1.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v1.1.0/total)](https://github.com/cwtickle/danoniplus/archive/v1.1.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v1.1.0/support/v1?style=social)](https://github.com/cwtickle/danoniplus/compare/v1.1.0...support/v1#files_bucket)
- ⭐️ Edge V18以降で初期矢印色及び色変化に対応 ( PR [#114](https://github.com/cwtickle/danoniplus/pull/114) )
- 🐞 曲時間直指定時、終了時間指定がおかしくなることがある不具合を修正 ( PR [#114](https://github.com/cwtickle/danoniplus/pull/114) ) <- :boom: **initial**

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v1.1.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v1.0.2...v1.1.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v1.1.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v1.1.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v1.1.0.tar.gz) )

## v1.0.2 ([2018-11-27](https://github.com/cwtickle/danoniplus/releases/tag/v1.0.2))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v1.0.2/total)](https://github.com/cwtickle/danoniplus/archive/v1.0.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v1.0.2/support/v1?style=social)](https://github.com/cwtickle/danoniplus/compare/v1.0.2...support/v1#files_bucket)
- 🛠️ フリーズアローのヒット開始位置をステップゾーン上になるように位置調整 ( PR [#112](https://github.com/cwtickle/danoniplus/pull/112) )
- 🛠️ ゲージ設定で、ノルマ必須の場合のBorder表記を%表記に変更 ( PR [#112](https://github.com/cwtickle/danoniplus/pull/112) )
- 🐞 譜面変更ボタンを変更した場合に、ゲージ設定がリセットされないことがある問題を修正 ( PR [#112](https://github.com/cwtickle/danoniplus/pull/112) ) <- :boom: **initial**

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v1.0.2)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v1.0.1...v1.0.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v1.0.2)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v1.0.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v1.0.2.tar.gz) )

## v1.0.1 ([2018-11-25](https://github.com/cwtickle/danoniplus/releases/tag/v1.0.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v1.0.1/total)](https://github.com/cwtickle/danoniplus/archive/v1.0.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v1.0.1/support/v1?style=social)](https://github.com/cwtickle/danoniplus/compare/v1.0.1...support/v1#files_bucket)
- 🛠️ ローカル時のみフレーム数を表示する仕様に変更 ( PR [#111](https://github.com/cwtickle/danoniplus/pull/111) )
- 🐞 デフォルトのゲージ設定がBorderlessになっていない問題を修正 ( PR [#111](https://github.com/cwtickle/danoniplus/pull/111) ) <- :boom: **initial**

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v1.0.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/7fb6b90...v1.0.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v1.0.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v1.0.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v1.0.1.tar.gz) )

## v1.0.0 ([2018-11-25](https://github.com/cwtickle/danoniplus/releases/tag/v1.0.1))
[![support](https://img.shields.io/badge/downloads@v1.0.0-x-lightgray)](https://github.com/cwtickle/danoniplus/archive/7fb6b90.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/7fb6b90/support/v1?style=social&label=Commits%20since%20v1.0.0)](https://github.com/cwtickle/danoniplus/compare/7fb6b90...support/v1#files_bucket)
- ⭐️ Clear, Failed条件及び結果画面のモーションを追加 ( PR [#109](https://github.com/cwtickle/danoniplus/pull/109) )
- ⭐️ Gaugeオプションの実装 ( PR [#109](https://github.com/cwtickle/danoniplus/pull/109) )
- 🐞 IE11でゲーム開始直後に止まる問題を修正 ( PR [#108](https://github.com/cwtickle/danoniplus/pull/108) ) <- :boom: **initial**

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/7fb6b90)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/f03b2ca...7fb6b90#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v1.0.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/7fb6b90.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/7fb6b90.tar.gz) )

[**<- v2**](Changelog-v2) | **v1** | [**v0 ->**](Changelog-v0)
