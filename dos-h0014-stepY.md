**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-h0014-stepY) | Japanese** 

[Home](./) &gt; [譜面ヘッダー仕様](dos_header) &gt; [プレイ時の初期設定](dos_header#-プレイ時の初期設定) &gt; [プレイ画面位置の設定](dos_header#プレイ画面位置の設定)

| [<- scArea](dos-h0096-scArea) | **stepY** | [stepYR ->](dos-h0049-stepYR) |

## stepY
- ステップゾーンのY座標位置の設定

### 使い方
```
|stepY=100|
```
### 説明
ステップゾーンのY座標を指定します。単位はpx。デフォルトは70pxです。  
Reverseの場合は下からstepYを差し引いた位置に配置します。  

### 使用例
- stepYの値を変えると、ステップゾーンのY座標が上下それぞれ下記のように変わります。

![dos-h0014-01.png](./wiki/dos-h0014-01.png)

### 関連項目
- [stepYR](dos-h0049-stepYR) [:pencil:](dos-h0049-stepYR/_edit) ステップゾーン(下)のY座標現位置からの差分
- [arrowJdgY / frzJdgY](dos-h0058-jdgY) [:pencil:](dos-h0058-jdgY/_edit) 判定キャラクタのY座標位置

### 更新履歴

|Version|変更内容|
|----|----|
|[v1.0.0](https://github.com/cwtickle/danoniplus/releases/tag/v1.0.1)|・初回実装|

| [<- scArea](dos-h0096-scArea) | **stepY** | [stepYR ->](dos-h0049-stepYR) |