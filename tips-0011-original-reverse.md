**[English](https://github.com/cwtickle/danoniplus-docs/wiki/tips-0011-original-reverse) | Japanese** 

[^ Tips Indexに戻る](./tips-index)

| < [キー変化作品の実装例](./tips-0009-key-types-change) | **カスタムキーで独自のReverseを設定** | [カスタムキーの省略形記法](./tips-0016-custom-key-abbreviation) > |

# カスタムキーで独自のReverseを設定
- 通常、Reverse設定はデフォルトのスクロール方向をそのまま反転しますが、  
カスタムキーによってはそうしたくない場合があります。

## 使い方
- 下記のように、スクロール拡張設定(Scroll)に「Reverse」を設定します。
- この場合に限り、通常の「Reverse」設定を無効化することができます。
```
|scrollTr=Reverse::1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,-1,-1,-1,-1,-1,-1,-1,-1,-1|
```
<img src="https://user-images.githubusercontent.com/44026291/215254577-e983e3a7-07b6-4b94-9990-be381d4d3afb.png" width="60%">

### 補足
- Reverseの他、デフォルト(Scroll:`---`) についても上書きが可能です。
```
|scroll9v=---::1,1,-1,-1,-1,-1,-1,1,1/flat::1,1,1,1,1,1,1,1,1$9B_0|
```

- この独自Reverseで独自のword/back/maskデータを作成したい場合、  
`wordReverse_data`, `backReverse_data`などといった変数を使うことで区別が可能です。  
※`wordRev_data`, `backRev_data`は使えません。

## ページ作成者
- ティックル

## 関連ページ
- [キー数定義](./keys)
- [カスタムキーテンプレート](./tips-0004-extrakeys)

[^ Tips Indexに戻る](./tips-index)

| < [キー変化作品の実装例](./tips-0009-key-types-change) | **カスタムキーで独自のReverseを設定** | [カスタムキーの省略形記法](./tips-0016-custom-key-abbreviation) > |
