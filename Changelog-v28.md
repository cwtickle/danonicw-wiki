**[English](https://github.com/cwtickle/danoniplus-docs/wiki/Changelog-v28) | Japanese** 

[^ Back to All Version List (Security Policy)](https://github.com/cwtickle/danoniplus/security/policy)  
[^ Back to Update Info](./UpdateInfo#-v28-changelog)

[**<- v29**](Changelog-v29) | **v28** | [**v27 ->**](Changelog-v27)  
(**🔖 [18 Releases](https://github.com/cwtickle/danoniplus/releases?q=tag%3Av28)** )

## 🔃 Files changed (v28)

|Directory|FileName||Last Updated|
|----|----|----|----|
|/js|danoni_main.js|[📥](https://github.com/cwtickle/danoniplus/releases/download/v28.6.7/danoni_main.js)|**v28.6.7**|
|/js/lib|danoni_constants.js|[📥](https://github.com/cwtickle/danoniplus/releases/download/v28.6.7/danoni_constants.js)|**v28.6.7**|
|/css|danoni_main.css|[📥](https://github.com/cwtickle/danoniplus/releases/download/v28.3.1/danoni_main.css)|[v28.3.1](./Changelog-v28#v2831-2022-10-16)|

<details>
<summary>Changed file list before v27</summary>

|Directory|FileName||Last Updated|
|----|----|----|----|
|/js/lib|danoni_localbinary.js|[📥](https://github.com/cwtickle/danoniplus/releases/download/v15.1.0/danoni_localbinary.js)|[v15.1.0](./Changelog-v15#v1510-2020-05-21)|
|/img|aaShadow.svg<br>arrow.svg<br>arrowShadow.svg<br>borderline.svg<br>c.svg<br>cursor.svg<br>giko.svg<br>iyo.svg<br>monar.svg<br>morara.svg<br>onigiri.svg|[📥](https://github.com/cwtickle/danoniplus/releases/download/v15.1.0/img.zip)|[v15.1.0](./Changelog-v15#v1510-2020-05-21)|
|/skin|danoni_skin_default.css<br>danoni_skin_light.css<br>danoni_skin_skyblue.css|[📥](https://github.com/cwtickle/danoniplus/releases/download/v21.4.2/danoni_skin_default.css)<br>[📥](https://github.com/cwtickle/danoniplus/releases/download/v21.4.2/danoni_skin_light.css)<br>[📥](https://github.com/cwtickle/danoniplus/releases/download/v21.4.2/danoni_skin_skyblue.css)|[v21.4.2](./Changelog-v21#v2142-2021-04-07)|

</details>

⭐ New Features / 🛠️ Improvements / 🐞 Bug Fixes  
( Files changed: ⚪ danoni_main.js / 🔵 danoni_constants.js / 🔴 css files )

## [Warning] 更新終了後の不具合情報
- [更新を終了したバージョンの不具合情報#v28](DeprecatedVersionBugs#v28) を参照

## v28.6.7 ([2023-03-04](https://github.com/cwtickle/danoniplus/releases/tag/v28.6.7))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v28.6.7/total)](https://github.com/cwtickle/danoniplus/archive/v28.6.7.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v28.6.7/support/v28?style=social)](https://github.com/cwtickle/danoniplus/compare/v28.6.7...support/v28#files_bucket)
- 🐞 **C)** 11, 11Lkeyの別キーモードでFlatにしたときにスクロールが想定と逆になる問題を修正 <- :boom: [**v10.2.1**](./Changelog-v10#v1021-2019-11-20)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v28.6.7)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v28.6.6...v28.6.7#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v28.6.7)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v28.6.7.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v28.6.7.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v28.6.7/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v28.6.7/js/lib/danoni_constants.js)
/ 🎣 [**v29.4.3**](./Changelog-v29#v2943-2023-03-04)

## v28.6.6 ([2023-02-26](https://github.com/cwtickle/danoniplus/releases/tag/v28.6.6))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v28.6.6/total)](https://github.com/cwtickle/danoniplus/archive/v28.6.6.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v28.6.6/support/v28?style=social)](https://github.com/cwtickle/danoniplus/compare/v28.6.6...support/v28#files_bucket)
- 🐞 **C)** カスタムキーのdivXが未定義のとき、ステップゾーンが並ばない問題を修正 ( PR [#1414](https://github.com/cwtickle/danoniplus/pull/1414) ) <- :boom: **initial**
- 🐞 **C)** カスタムキーのshuffleXが未定義のとき、エラーで止まる問題を修正 ( PR [#1414](https://github.com/cwtickle/danoniplus/pull/1414) ) <- :boom: **initial**

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v28.6.6)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v28.6.5...v28.6.6#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v28.6.6)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v28.6.6.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v28.6.6.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v28.6.6/js/danoni_main.js)
/ 🎣 [**v30.2.3**](./Changelog-v30#v3023-2023-02-26)

## v28.6.5 ([2023-01-20](https://github.com/cwtickle/danoniplus/releases/tag/v28.6.5))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v28.6.5/total)](https://github.com/cwtickle/danoniplus/archive/v28.6.5.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v28.6.5/support/v28?style=social)](https://github.com/cwtickle/danoniplus/compare/v28.6.5...support/v28#files_bucket)
- 🐞 **C)** カスタムキー定義におけるdivXの第2要素がposX末尾要素の最大値で無い場合、キーコンフィグ保存後にステップゾーン（下段）がずれる問題を修正 ( PR [#1385](https://github.com/cwtickle/danoniplus/pull/1385) ) <- :boom: **initial**

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v28.6.5)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v28.6.4...v28.6.5#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v28.6.5)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v28.6.5.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v28.6.5.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v28.6.5/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v28.6.5/js/lib/danoni_constants.js)
/ 🎣 [**v29.3.5**](./Changelog-v29#v2935-2023-01-20)

## v28.6.4 ([2023-01-07](https://github.com/cwtickle/danoniplus/releases/tag/v28.6.4))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v28.6.4/total)](https://github.com/cwtickle/danoniplus/archive/v28.6.4.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v28.6.4/support/v28?style=social)](https://github.com/cwtickle/danoniplus/compare/v28.6.4...support/v28#files_bucket)
- 🛠️ 同時押し関連の英訳を見直し ( PR [#1378](https://github.com/cwtickle/danoniplus/pull/1378) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v28.6.4)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v28.6.3...v28.6.4#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v28.6.4)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v28.6.4.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v28.6.4.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v28.6.4/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v28.6.4/js/lib/danoni_constants.js)
/ 🎣 [**v29.3.4**](./Changelog-v29#v2934-2023-01-07)

## v28.6.3 ([2022-12-10](https://github.com/cwtickle/danoniplus/releases/tag/v28.6.3))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v28.6.3/total)](https://github.com/cwtickle/danoniplus/archive/v28.6.3.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v28.6.3/support/v28?style=social)](https://github.com/cwtickle/danoniplus/compare/v28.6.3...support/v28#files_bucket)
- 🐞 **B)** 速度が激しく変化する場合にタイミングがステップゾーン位置からずれることがある問題を修正 ( PR [#1372](https://github.com/cwtickle/danoniplus/pull/1372) ) <- :boom: **initial**

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v28.6.3)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v28.6.2...v28.6.3#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v28.6.3)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v28.6.3.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v28.6.3.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v28.6.3/js/danoni_main.js)
/ 🎣 [**v29.3.1**](./Changelog-v29#v2931-2022-12-10)

## v28.6.2 ([2022-11-15](https://github.com/cwtickle/danoniplus/releases/tag/v28.6.2))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v28.6.2/total)](https://github.com/cwtickle/danoniplus/archive/v28.6.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v28.6.2/support/v28?style=social)](https://github.com/cwtickle/danoniplus/compare/v28.6.2...support/v28#files_bucket)
- 🐞 **C)** Motionオプションを使用し、フリーズアローについてタイミングをずらして押したとき、そのズレの中にストップが含まれていると、処理が止まる可能性がある問題を修正 ( PR [#1366](https://github.com/cwtickle/danoniplus/pull/1366) ) <- :boom: [**v23.1.1**](Changelog-v23#v2311-2021-09-17)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v28.6.2)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v28.6.1...v28.6.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v28.6.2)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v28.6.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v28.6.2.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v28.6.2/js/danoni_main.js)
/ 🎣 [**v29.3.0**](./Changelog-v29#v2930-2022-11-15)

## v28.6.1 ([2022-11-04](https://github.com/cwtickle/danoniplus/releases/tag/v28.6.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v28.6.1/total)](https://github.com/cwtickle/danoniplus/archive/v28.6.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v28.6.1/support/v28?style=social)](https://github.com/cwtickle/danoniplus/compare/v28.6.1...support/v28#files_bucket)
- 🐞 **C)** クレジットリンク表示の折り返しが利かない問題を修正 ( PR [#1350](https://github.com/cwtickle/danoniplus/pull/1350) ) <- :boom: [**v28.2.1**](Changelog-v28#v2821-2022-10-05)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v28.6.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v28.6.0...v28.6.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v28.6.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v28.6.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v28.6.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v28.6.1/js/danoni_main.js)
/ 🎣 [**v29.0.1**](./Changelog-v29#v2901-2022-11-05)

---

## v28.6.0 ([2022-10-31](https://github.com/cwtickle/danoniplus/releases/tag/v28.6.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v28.6.0/total)](https://github.com/cwtickle/danoniplus/archive/v28.6.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v28.6.0/support/v28?style=social)](https://github.com/cwtickle/danoniplus/compare/v28.6.0...support/v28#files_bucket)
- ⭐ プレイ画面左上のフレーム数、左下の経過時間表記を譜面データ基準(baseFrame)に変更 ( Issue [#1347](https://github.com/cwtickle/danoniplus/pull/1347), PR [#1345](https://github.com/cwtickle/danoniplus/pull/1345), [#1348](https://github.com/cwtickle/danoniplus/pull/1348) )
- 🐞 **C)** 譜面データの値が極端に小さい場合、baseFrameの値がずれてしまう問題を修正 ( PR [#1348](https://github.com/cwtickle/danoniplus/pull/1348) ) <- :boom: [**v25.3.2**](Changelog-v25#v2532-2022-01-18)

<img src="https://user-images.githubusercontent.com/44026291/199033833-6c079bfc-e320-4d27-8ddc-28143cce14a2.png" width="90%">

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v28.6.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v28.5.0...v28.6.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v28.6.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v28.6.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v28.6.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v28.6.0/js/danoni_main.js)

## v28.5.0 ([2022-10-29](https://github.com/cwtickle/danoniplus/releases/tag/v28.5.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v28.5.0/total)](https://github.com/cwtickle/danoniplus/archive/v28.5.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v28.5.0/support/v28?style=social)](https://github.com/cwtickle/danoniplus/compare/v28.5.0...support/v28#files_bucket)
- ⭐ 主に縦連打で前の矢印の判定が吸われそうなときに、強制的に矢印の判定を後の矢印に移す処理を追加 ( PR [#1342](https://github.com/cwtickle/danoniplus/pull/1342) )
- 🛠️ Motion加算部分のコード整理 ( PR [#1341](https://github.com/cwtickle/danoniplus/pull/1341) )
- 🛠️ 強制的に判定を移す場合のチェック対象をすぐ次の矢印／フリーズアローになるよう変更 ( PR [#1342](https://github.com/cwtickle/danoniplus/pull/1342) )
- 🐞 **C)** フリーズアローを強制削除した場合の判定関数の引数誤りを修正 ( PR [#1343](https://github.com/cwtickle/danoniplus/pull/1343) ) <- :boom: [**v5.12.1**](Changelog-v5#v5121-2019-06-17)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v28.5.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v28.4.0...v28.5.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v28.5.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v28.5.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v28.5.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v28.5.0/js/danoni_main.js)<br>❤️ goe (@goe0)

## v28.4.0 ([2022-10-20](https://github.com/cwtickle/danoniplus/releases/tag/v28.4.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v28.4.0/total)](https://github.com/cwtickle/danoniplus/archive/v28.4.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v28.4.0/support/v28?style=social)](https://github.com/cwtickle/danoniplus/compare/v28.4.0...support/v28#files_bucket)
- ⭐ Motionの軌道加算部分が個別加速の影響を受けないよう変更 ( PR [#1337](https://github.com/cwtickle/danoniplus/pull/1337) )
- ⭐ MotionオプションにHi-Boostを追加 ( PR [#1336](https://github.com/cwtickle/danoniplus/pull/1336) )
- ⭐ ローカル時に表示されるフレーム数を見かけ上のフレーム数に変更 ( PR [#1338](https://github.com/cwtickle/danoniplus/pull/1338) )
- 🛠️ フェードイン中の個別加速の仕様見直し ( PR [#1339](https://github.com/cwtickle/danoniplus/pull/1339) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v28.4.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v28.3.1...v28.4.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v28.4.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v28.4.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v28.4.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v28.4.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v28.4.0/js/lib/danoni_constants.js)

## v28.3.1 ([2022-10-16](https://github.com/cwtickle/danoniplus/releases/tag/v28.3.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v28.3.1/total)](https://github.com/cwtickle/danoniplus/archive/v28.3.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v28.3.1/support/v28?style=social)](https://github.com/cwtickle/danoniplus/compare/v28.3.1...support/v28#files_bucket)
- 🛠️ コメントエリアについて自然折り返しを有効化 ( PR [#1332](https://github.com/cwtickle/danoniplus/pull/1332) )
- 🛠️ Display画面において特定項目未使用時に非表示化する設定を追加 ( PR [#1333](https://github.com/cwtickle/danoniplus/pull/1333), [#1334](https://github.com/cwtickle/danoniplus/pull/1334) )
- 🛠️ CSSのベンダープレフィックス指定を一部削除 ( PR [#1331](https://github.com/cwtickle/danoniplus/pull/1331) )
- 🐞 **C)** 警告ウィンドウについて自然折り返しが利かないことにより、文字が見切れる問題を修正 ( PR [#1332](https://github.com/cwtickle/danoniplus/pull/1332) ) <- :boom: [**v28.2.1**](Changelog-v28#v2821-2022-10-05)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v28.3.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v28.3.0...v28.3.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v28.3.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v28.3.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v28.3.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v28.3.1/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v28.3.1/js/lib/danoni_constants.js)[🔴](https://github.com/cwtickle/danoniplus/blob/v28.3.1/css/danoni_main.css)

## v28.3.0 ([2022-10-13](https://github.com/cwtickle/danoniplus/releases/tag/v28.3.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v28.3.0/total)](https://github.com/cwtickle/danoniplus/archive/v28.3.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v28.3.0/support/v28?style=social)](https://github.com/cwtickle/danoniplus/compare/v28.3.0...support/v28#files_bucket)
- ⭐ 17key(KeyPattern: 1)のサイズ見直し ( PR [#1326](https://github.com/cwtickle/danoniplus/pull/1326) )
- ⭐ シャッフルグループのデフォルトからの差分に対して色付け ( PR [#1328](https://github.com/cwtickle/danoniplus/pull/1328), [#1329](https://github.com/cwtickle/danoniplus/pull/1329), Issue [#1327](https://github.com/cwtickle/danoniplus/pull/1327) )
- 🛠️ 矢印色を変更するカラーピッカー関連の説明文を追加 ( PR [#1325](https://github.com/cwtickle/danoniplus/pull/1325) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v28.3.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v28.2.1...v28.3.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v28.3.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v28.3.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v28.3.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v28.3.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v28.3.0/js/lib/danoni_constants.js)

## v28.2.1 ([2022-10-05](https://github.com/cwtickle/danoniplus/releases/tag/v28.2.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v28.2.1/total)](https://github.com/cwtickle/danoniplus/archive/v28.2.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v28.2.1/support/v28?style=social)](https://github.com/cwtickle/danoniplus/compare/v28.2.1...support/v28#files_bucket)
- 🛠️ 画面内の意図しない文字の折り返し設定を無効化 ( PR [#1322](https://github.com/cwtickle/danoniplus/pull/1322) )
- 🛠️ カラー・シャッフル用のローカルストレージキーを変更 ( PR [#1323](https://github.com/cwtickle/danoniplus/pull/1323) )
- 🛠️ ローカルストレージ（作品別）の保存対象からDisplay設定を対象外に変更 ( PR [#1323](https://github.com/cwtickle/danoniplus/pull/1323) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v28.2.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v28.2.0...v28.2.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v28.2.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v28.2.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v28.2.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v28.2.1/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v28.2.1/js/lib/danoni_constants.js)[🔴](https://github.com/cwtickle/danoniplus/blob/v28.2.1/css/danoni_main.css)


## v28.2.0 ([2022-10-04](https://github.com/cwtickle/danoniplus/releases/tag/v28.2.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v28.2.0/total)](https://github.com/cwtickle/danoniplus/archive/v28.2.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v28.2.0/support/v28?style=social)](https://github.com/cwtickle/danoniplus/compare/v28.2.0...support/v28#files_bucket)
- ⭐ カスタムキーのカラー・シャッフルグループ実装 ( PR [#1318](https://github.com/cwtickle/danoniplus/pull/1318) )
- 🛠️ キーパターン変更ボタンの処理見直し ( PR [#1320](https://github.com/cwtickle/danoniplus/pull/1320) )
- 🐞 **C)** キーパターンがselfのとき、カラー・シャッフルグループが2種類のみになってしまう問題を修正 ( PR [#1317](https://github.com/cwtickle/danoniplus/pull/1317), [#1319](https://github.com/cwtickle/danoniplus/pull/1319) ) <- :boom: [**v28.1.0**](Changelog-v28#v2810-2022-10-02)
- 🐞 **C)** データセーブしない場合などにカラー・シャッフルグループの設定が引き継がれない問題を修正 ( PR [#1320](https://github.com/cwtickle/danoniplus/pull/1320) ) <- :boom: [**v28.1.0**](Changelog-v28#v2810-2022-10-02)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v28.2.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v28.1.2...v28.2.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v28.2.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v28.2.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v28.2.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v28.2.0/js/danoni_main.js)

## v28.1.2 ([2022-10-02](https://github.com/cwtickle/danoniplus/releases/tag/v28.1.2))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v28.1.2/total)](https://github.com/cwtickle/danoniplus/archive/v28.1.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v28.1.2/support/v28?style=social)](https://github.com/cwtickle/danoniplus/compare/v28.1.2...support/v28#files_bucket)
- 🛠️ シャッフル名の表示をグループ毎に区別しないよう変更 ( PR [#1314](https://github.com/cwtickle/danoniplus/pull/1314) )
- 🐞 **C)** カラーグループを変更した際、個々の矢印のカラーグループが反映されない問題を修正 ( PR [#1313](https://github.com/cwtickle/danoniplus/pull/1313) ) <- :boom: [**v28.1.0**](Changelog-v28#v2810-2022-10-02)
- 🐞 **B)** キーパターン変更時、カラー・シャッフルグループが前の設定を引きずってしまう問題を修正 ( PR [#1315](https://github.com/cwtickle/danoniplus/pull/1315) ) <- :boom: [**v28.1.0**](Changelog-v28#v2810-2022-10-02)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v28.1.2)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v28.1.1...v28.1.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v28.1.2)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v28.1.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v28.1.2.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v28.1.2/js/danoni_main.js)

## v28.1.1 ([2022-10-02](https://github.com/cwtickle/danoniplus/releases/tag/v28.1.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v28.1.1/total)](https://github.com/cwtickle/danoniplus/archive/v28.1.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v28.1.1/support/v28?style=social)](https://github.com/cwtickle/danoniplus/compare/v28.1.1...support/v28#files_bucket)
- 🐞 **C)** ColorType: Type0が既定値にならない問題を修正 ( PR [#1311](https://github.com/cwtickle/danoniplus/pull/1311) ) <- :boom: [**v28.1.0**](Changelog-v28#v2810-2022-10-02)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v28.1.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v28.1.0...v28.1.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v28.1.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v28.1.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v28.1.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v28.1.1/js/danoni_main.js)

## v28.1.0 ([2022-10-02](https://github.com/cwtickle/danoniplus/releases/tag/v28.1.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v28.1.0/total)](https://github.com/cwtickle/danoniplus/archive/v28.1.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v28.1.0/support/v28?style=social)](https://github.com/cwtickle/danoniplus/compare/v28.1.0...support/v28#files_bucket)
- ⭐ カラーセット、カラーグループ、シャッフルグループのキー別一括保存に対応 ( PR [#1302](https://github.com/cwtickle/danoniplus/pull/1309) )

<img src="https://user-images.githubusercontent.com/44026291/193415334-80a2e988-9576-4481-9eb0-4c3d2babdbca.png" width="50%">

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v28.1.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v28.0.1...v28.1.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v28.1.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v28.1.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v28.1.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v28.1.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v28.1.0/js/lib/danoni_constants.js) 


## v28.0.1 ([2022-08-21](https://github.com/cwtickle/danoniplus/releases/tag/v28.0.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v28.0.1/total)](https://github.com/cwtickle/danoniplus/archive/v28.0.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v28.0.1/support/v28?style=social)](https://github.com/cwtickle/danoniplus/compare/v28.0.1...support/v28#files_bucket)
- ⭐ 譜面明細画面でカーソル移動用のショートカットを追加 ( PR [#1302](https://github.com/cwtickle/danoniplus/pull/1302) )
- 🛠️ 配列コピーの関数の一部をstructuredCloneを使ったものに変更 ( PR [#1304](https://github.com/cwtickle/danoniplus/pull/1304) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v28.0.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v27.8.1...v28.0.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v28.0.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v28.0.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v28.0.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v28.0.1/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v28.0.1/js/lib/danoni_constants.js) 

[**<- v29**](Changelog-v29) | **v28** | [**v27 ->**](Changelog-v27)  