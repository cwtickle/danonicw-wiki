**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos_score) | Japanese** 

| [< 譜面ヘッダー仕様](./dos_header) | **譜面本体仕様** | [譜面エフェクト仕様 (速度/ 色/ 背景) >](./dos_effect) |

# 譜面本体仕様
- 譜面データは、譜面ヘッダーと譜面本体、画面効果データ(フッター)の3つから構成されます。  
譜面本体の仕様はParaFlaソースのものとほぼ同一です。  

## データ書式
- 数字のデータは、矢印・フリーズアローがステップゾーンに到達したフレーム数を順に記載します。
- フリーズアローの場合は、開始と終了の2つで1セットです。下記の例では2つのフリーズアローが流れます。
```
|left_data=200,220,240,260|
|frzLeft_data=300,320,360,380|
```

## 複数譜面の場合の書式
- 複数譜面（2譜面目以降）の場合、`_data`の前に数字を入れることで区別します。
- left2_data, down2_data なら2譜面目、left3_data, down3_data なら3譜面目といった具合です。

## キー別のデータ名（標準搭載キー）
- ここでは譜面本体の仕様を記述しています。  
Noは上から下、左から右になるように割り振っています。パターン1基準。  
- キーコンフィグ画面上のシャッフルグループ番号は、わかりやすいように1, 2, 3, ...の順に割り振っていますが、この表では実際に指定する0, 1, 2, ...の順で明記しています。


### 5key
<img src="https://user-images.githubusercontent.com/44026291/199854103-75a15326-9c53-4d22-9b6f-f947f04f080e.png" width="70%">

|No|位置<br>Position|矢印<br>データ名<br>Arrow<br>name|フリーズアロー<br>データ名<br>Freeze-arrow<br>name|キー割当<br>Key<br>assignment|色<br>グループ<br>Color<br>group|シャッフル<br>グループ<br>Shuffle<br>group|
|----|----|----|----|----|----|----|
|0|上-0|left_data|frzLeft_data|←|0|0|
|1|上-1|down_data|frzDown_data|↓|0|0|
|2|上-2|up_data|frzUp_data|↑|0|0|
|3|上-3|right_data|frzRight_data|→|0|0|
|4|上-4|space_data|frzSpace_data|Space|2|1|

### 7key
|No|位置<br>Position|矢印<br>データ名<br>Arrow<br>name|フリーズアロー<br>データ名<br>Freeze-arrow<br>name|キー割当<br>Key<br>assignment|色<br>グループ<br>Color<br>group|シャッフル<br>グループ<br>Shuffle<br>group|
|----|----|----|----|----|----|----|
|0|上-0|left_data|frzLeft_data|S|0|0|
|1|上-1|leftdia_data|frzLdia_data|D|1|0|
|2|上-2|down_data|frzDown_data|F|0|0|
|3|上-3|space_data|frzSpace_data|Space|2|1|
|4|上-4|up_data|frzUp_data|J|0|0|
|5|上-5|rightdia_data|frzRdia_data|K|1|0|
|6|上-6|right_data|frzRight_data|L|0|0|

### 7ikey
|No|位置<br>Position|矢印<br>データ名<br>Arrow<br>name|フリーズアロー<br>データ名<br>Freeze-arrow<br>name|キー割当<br>Key<br>assignment|色<br>グループ<br>Color<br>group|シャッフル<br>グループ<br>Shuffle<br>group|
|----|----|----|----|----|----|----|
|0|上-0|left_data|frzLeft_data|Z|2|0|
|1|上-1|leftdia_data|frzLdia_data|X|2|0|
|2|上-2|down_data|frzDown_data|C|2|0|
|3|上-3|space_data|frzSpace_data|←|0|1|
|4|上-4|up_data|frzUp_data|↓|0|1|
|5|上-5|rightdia_data|frzRdia_data|↑|0|1|
|6|上-6|right_data|frzRight_data|→|0|1|

### 8key
|No|位置<br>Position|矢印<br>データ名<br>Arrow<br>name|フリーズアロー<br>データ名<br>Freeze-arrow<br>name|キー割当<br>Key<br>assignment|色<br>グループ<br>Color<br>group|シャッフル<br>グループ<br>Shuffle<br>group|
|----|----|----|----|----|----|----|
|0|上-0|left_data|frzLeft_data|S|0|0|
|1|上-1|leftdia_data|frzLdia_data|D|1|0|
|2|上-2|down_data|frzDown_data|F|0|0|
|3|上-3|space_data|frzSpace_data|Space|2|1|
|4|上-4|up_data|frzUp_data|J|0|0|
|5|上-5|rightdia_data|frzRdia_data|K|1|0|
|6|上-6|right_data|frzRight_data|L|0|0|
|7|上-7|sleft_data|sfrzLeft_data|Enter|2|2|

### 9Akey(DP)
|No|位置<br>Position|矢印<br>データ名<br>Arrow<br>name|フリーズアロー<br>データ名<br>Freeze-arrow<br>name|キー割当<br>Key<br>assignment|色<br>グループ<br>Color<br>group|シャッフル<br>グループ<br>Shuffle<br>group|
|----|----|----|----|----|----|----|
|0|上-0|left_data|frzLeft_data|S|0|0|
|1|上-1|down_data|frzDown_data|D|0|0|
|2|上-2|up_data|frzUp_data|E (R)|0|0|
|3|上-3|right_data|frzRight_data|F|0|0|
|4|上-4|space_data|frzSpace_data|Space|2|1|
|5|上-5|sleft_data|sfrzLeft_data|J|3|0 (2)|
|6|上-6|sdown_data|sfrzDown_data|K|3|0 (2)|
|7|上-7|sup_data|sfrzUp_data|I|3|0 (2)|
|8|上-8|sright_data|sfrzRight_data|L|3|0 (2)|

### 9Bkey
|No|位置<br>Position|矢印<br>データ名<br>Arrow<br>name|フリーズアロー<br>データ名<br>Freeze-arrow<br>name|キー割当<br>Key<br>assignment|色<br>グループ<br>Color<br>group|シャッフル<br>グループ<br>Shuffle<br>group|
|----|----|----|----|----|----|----|
|0|上-0|left_data|frzLeft_data|A|1 (3)|0|
|1|上-1|down_data|frzDown_data|S|0 (2)|0|
|2|上-2|up_data|frzUp_data|D|1|0|
|3|上-3|right_data|frzRight_data|F|0|0|
|4|上-4|space_data|frzSpace_data|Space|2|1|
|5|上-5|sleft_data|sfrzLeft_data|J|0|0|
|6|上-6|sdown_data|sfrzDown_data|K|1|0|
|7|上-7|sup_data|sfrzUp_data|L|0 (2)|0|
|8|上-8|sright_data|sfrzRight_data|+|1 (3)|0|

### 9ikey
|No|位置<br>Position|矢印<br>データ名<br>Arrow<br>name|フリーズアロー<br>データ名<br>Freeze-arrow<br>name|キー割当<br>Key<br>assignment|色<br>グループ<br>Color<br>group|シャッフル<br>グループ<br>Shuffle<br>group|
|----|----|----|----|----|----|----|
|0|上-2|sleft_data|sfrzLeft_data|←|0|0|
|1|上-3|sdown_data|sfrzDown_data|↓|0|0|
|2|上-4|sup_data|sfrzUp_data|↑|0|0|
|3|上-5|sright_data|sfrzRight_data|→|0|0|
|4|下-6|left_data|frzLeft_data|A|2|1|
|5|下-7|down_data|frzDown_data|S|2|1|
|6|下-8|up_data|frzUp_data|D|2|1|
|7|下-9|right_data|frzRight_data|F|2|1|
|8|下-10|space_data|frzSpace_data|Space|2|1|


### 11key, 11Lkey
<img src="https://user-images.githubusercontent.com/44026291/199854337-98af3915-698f-4561-80c0-b713f78953c8.png" width="80%">

|No|位置<br>Position|矢印<br>データ名<br>Arrow<br>name|フリーズアロー<br>データ名<br>Freeze-arrow<br>name|キー割当<br>Key<br>assignment|色<br>グループ<br>Color<br>group|シャッフル<br>グループ<br>Shuffle<br>group|
|----|----|----|----|----|----|----|
|0|上-2 / 0|sleft_data|sfrzLeft_data|← / W|3|0|
|1|上-3 / 1|sdown_data|sfrzDown_data|↓ / E|3|0|
|2|上-4 / 2|sup_data|sfrzUp_data|↑ / 3 (4)|3|0|
|3|上-5 / 3|sright_data|sfrzRight_data|→ / R|3|0|
|4|下-6|left_data|frzLeft_data|S|0|1|
|5|下-7|leftdia_data|frzLdia_data|D|1|1|
|6|下-8|down_data|frzDown_data|F|0|1|
|7|下-9|space_data|frzSpace_data|Space|2|2|
|8|下-10|up_data|frzUp_data|J|0|1 (3)|
|9|下-11|rightdia_data|frzRdia_data|K|1|1 (3)|
|10|下-12|right_data|frzRight_data|L|0|1 (3)|

### 11Wkey
|No|位置<br>Position|矢印<br>データ名<br>Arrow<br>name|フリーズアロー<br>データ名<br>Freeze-arrow<br>name|キー割当<br>Key<br>assignment|色<br>グループ<br>Color<br>group|シャッフル<br>グループ<br>Shuffle<br>group|
|----|----|----|----|----|----|----|
|0|上-0|sleft_data|sfrzLeft_data|1 (2)|2|0|
|1|上-2|sdown_data|sfrzDown_data|T|3|0|
|2|上-3|sup_data|sfrzUp_data|Y|3|0|
|3|上-5|sright_data|sfrzRight_data|0 (-)|2|0|
|4|下-6|left_data|frzLeft_data|S|0|1|
|5|下-7|leftdia_data|frzLdia_data|D|1|1|
|6|下-8|down_data|frzDown_data|F|0|1|
|7|下-9|space_data|frzSpace_data|Space|2|2|
|8|下-10|up_data|frzUp_data|J|0|1|
|9|下-11|rightdia_data|frzRdia_data|K|1|1|
|10|下-12|right_data|frzRight_data|L|0|1|


### 11ikey
|No|位置<br>Position|矢印<br>データ名<br>Arrow<br>name|フリーズアロー<br>データ名<br>Freeze-arrow<br>name|キー割当<br>Key<br>assignment|色<br>グループ<br>Color<br>group|シャッフル<br>グループ<br>Shuffle<br>group|
|----|----|----|----|----|----|----|
|0|上-0|left_data|frzLeft_data|S|0|0|
|1|上-1|down_data|frzDown_data|X (C)|0|0|
|2|上-2|gor_data|frzGor_data|D|2|1|
|3|上-3|up_data|frzUp_data|E (R)|0|0|
|4|上-4|right_data|frzRight_data|F|0|0|
|5|上-5|space_data|frzSpace_data|Space|2|2|
|6|上-6|sleft_data|sfrzLeft_data|J|3|0 (3)|
|7|上-7|sdown_data|sfrzDown_data|M (&lt;)|3|0 (3)|
|8|上-8|siyo_data|sfrzIyo_data|K|2|1 (4)|
|9|上-9|sup_data|sfrzUp_data|I (O)|3|0 (3)|
|10|上-10|sright_data|sfrzRight_data|L|3|0 (3)|

### 12key
|No|位置<br>Position|矢印<br>データ名<br>Arrow<br>name|フリーズアロー<br>データ名<br>Freeze-arrow<br>name|キー割当<br>Key<br>assignment|色<br>グループ<br>Color<br>group|シャッフル<br>グループ<br>Shuffle<br>group|
|----|----|----|----|----|----|----|
|0|上-1|sleft_data|sfrzLeft_data|U|3|0|
|1|上-2|sdown_data|sfrzDown_data|I|3|0|
|2|上-3|sup_data|sfrzUp_data|8 (9)|3|0|
|3|上-4|sright_data|sfrzRight_data|O|3|0|
|4|下-5|oni_data|foni_data|Space|2|1|
|5|下-6|left_data|frzLeft_data|N|0|2|
|6|下-7|leftdia_data|frzLdia_data|J|1|2|
|7|下-8|down_data|frzDown_data|M|0|2|
|8|下-9|space_data|frzSpace_data|K|1|2|
|9|下-10|up_data|frzUp_data|&lt;|0|2|
|10|下-11|rightdia_data|frzRdia_data|L|1|2|
|11|下-12|right_data|frzRight_data|&gt;|0|2|

### 13key(TP)
|No|位置<br>Position|矢印<br>データ名<br>Arrow<br>name|フリーズアロー<br>データ名<br>Freeze-arrow<br>name|キー割当<br>Key<br>assignment|色<br>グループ<br>Color<br>group|シャッフル<br>グループ<br>Shuffle<br>group|
|----|----|----|----|----|----|----|
|0|上-4|tleft_data|tfrzLeft_data|←|4|0|
|1|上-5|tdown_data|tfrzDown_data|↓|4|0|
|2|上-6|tup_data|tfrzUp_data|↑|4|0|
|3|上-7|tright_data|tfrzRight_data|→|4|0|
|4|下-8|left_data|frzLeft_data|S|0|1|
|5|下-9|down_data|frzDown_data|D|0|1|
|6|下-10|up_data|frzUp_data|E (R)|0|1|
|7|下-11|right_data|frzRight_data|F|0|1|
|8|下-12|space_data|frzSpace_data|Space|2|2|
|9|下-13|sleft_data|sfrzLeft_data|J|3|3|
|10|下-14|sdown_data|sfrzDown_data|K|3|3|
|11|下-15|sup_data|sfrzUp_data|I|3|3|
|12|下-16|sright_data|sfrzRight_data|L|3|3|

### 14key
|No|位置<br>Position|矢印<br>データ名<br>Arrow<br>name|フリーズアロー<br>データ名<br>Freeze-arrow<br>name|キー割当<br>Key<br>assignment|色<br>グループ<br>Color<br>group|シャッフル<br>グループ<br>Shuffle<br>group|
|----|----|----|----|----|----|----|
|0|上-1|sleftdia_data|sfrzLdia_data|T (Y)|4|3|
|1|上-2|sleft_data|sfrzLeft_data|U|3|0|
|2|上-3|sdown_data|sfrzDown_data|I|3|0|
|3|上-4|sup_data|sfrzUp_data|8 (7, 9, 0)|3|0|
|4|上-5|sright_data|sfrzRight_data|O|3|0|
|5|上-6|srightdia_data|sfrzRdia_data|@ (P)|4|3|
|6|下-7|oni_data|foni_data|Space|2|1|
|7|下-8|left_data|frzLeft_data|N|0|2|
|8|下-9|leftdia_data|frzLdia_data|J|1|2|
|9|下-10|down_data|frzDown_data|M|0|2|
|10|下-11|space_data|frzSpace_data|K|1|2|
|11|下-12|up_data|frzUp_data|&lt;|0|2|
|12|下-13|rightdia_data|frzRdia_data|L|1|2|
|13|下-14|right_data|frzRight_data|&gt;|0|2|

### 14ikey
|No|位置<br>Position|矢印<br>データ名<br>Arrow<br>name|フリーズアロー<br>データ名<br>Freeze-arrow<br>name|キー割当<br>Key<br>assignment|色<br>グループ<br>Color<br>group|シャッフル<br>グループ<br>Shuffle<br>group|
|----|----|----|----|----|----|----|
|0|上-0|gor_data|frzGor_data|Z (W)|2|0|
|1|上-1|space_data|frzSpace_data|X (E)|2|0|
|2|上-2|iyo_data|frzIyo_data|C (R)|2|0|
|3|上-3|left_data|frzLeft_data|←|3|1|
|4|上-4|down_data|frzDown_data|↓|3|1|
|5|上-5|up_data|frzUp_data|↑|3|1|
|6|上-6|right_data|frzRight_data|→|3|1|
|7|下-8|sleft_data|sfrzLeft_data|S|0|2|
|8|下-9|sleftdia_data|sfrzLdia_data|D|1|2|
|9|下-10|sdown_data|sfrzDown_data|F|0|2|
|10|下-11|sspace_data|sfrzSpace_data|Space|2|3|
|11|下-12|sup_data|sfrzUp_data|J|0|2|
|12|下-13|srightdia_data|sfrzRdia_data|K|1|2|
|13|下-14|sright_data|sfrzRight_data|L|0|2|

### 15Akey, 15Bkey
|No|位置<br>Position|矢印<br>データ名<br>Arrow<br>name|フリーズアロー<br>データ名<br>Freeze-arrow<br>name|キー割当<br>Key<br>assignment|色<br>グループ<br>Color<br>group|シャッフル<br>グループ<br>Shuffle<br>group|
|----|----|----|----|----|----|----|
|0|上-0|sleft_data|sfrzLeft_data|W|3|0|
|1|上-1|sdown_data|sfrzDown_data|E|3|0|
|2|上-2|sup_data|sfrzUp_data|3 (4)|3|0|
|3|上-3|sright_data|sfrzRight_data|R|3|0|
|4|上-4|tleft_data|tfrzLeft_data|← / U|4|1 (2, 1)|
|5|上-5|tdown_data|tfrzDown_data|↓ / I|4|1 (2, 1)|
|6|上-6|tup_data|tfrzUp_data|↑ / 8 (9)|4|1 (2, 1)|
|7|上-7|tright_data|tfrzRight_data|→ / O|4|1 (2, 1)|
|8|下-8|left_data|frzLeft_data|S|0|2|
|9|下-9|leftdia_data|frzLdia_data|D|1|2|
|10|下-10|down_data|frzDown_data|F|0|2|
|11|下-11|space_data|frzSpace_data|Space|2|3|
|12|下-12|up_data|frzUp_data|J|0|2 (4, 2)|
|13|下-13|rightdia_data|frzRdia_data|K|1|2 (4, 2)|
|14|下-14|right_data|frzRight_data|L|0|2 (4, 2)|

### 16ikey
|No|位置<br>Position|矢印<br>データ名<br>Arrow<br>name|フリーズアロー<br>データ名<br>Freeze-arrow<br>name|キー割当<br>Key<br>assignment|色<br>グループ<br>Color<br>group|シャッフル<br>グループ<br>Shuffle<br>group|
|----|----|----|----|----|----|----|
|0|上-1|gor_data|frzGor_data|Z (W)|2|0|
|1|上-2|space_data|frzSpace_data|X (E)|2|0|
|2|上-3|iyo_data|frzIyo_data|C (R)|2|0|
|3|上-4|left_data|frzLeft_data|←|3|1|
|4|上-5|down_data|frzDown_data|↓|3|1|
|5|上-6|up_data|frzUp_data|↑|3|1|
|6|上-7|right_data|frzRight_data|→|3|1|
|7|下-8|sleft_data|sfrzLeft_data|A|1|2|
|8|下-9|sdown_data|sfrzDown_data|S|0|2|
|9|下-10|sup_data|sfrzUp_data|D|1|2|
|10|下-11|sright_data|sfrzRight_data|F|0|2|
|11|下-12|aspace_data|afrzSpace_data|Space|2|3|
|12|下-13|aleft_data|afrzLeft_data|J|0|2|
|13|下-14|adown_data|afrzDown_data|K|1|2|
|14|下-15|aup_data|afrzUp_data|L|0|2|
|15|下-16|aright_data|afrzRight_data|+|1|2|

### 17key
|No|位置<br>Position|矢印<br>データ名<br>Arrow<br>name|フリーズアロー<br>データ名<br>Freeze-arrow<br>name|キー割当<br>Key<br>assignment|色<br>グループ<br>Color<br>group|シャッフル<br>グループ<br>Shuffle<br>group|
|----|----|----|----|----|----|----|
|0|上-0|aleft_data|afrzLeft_data|A|0 (1)|0|
|1|上-1|bleft_data|bfrzLeft_data|Z|1 (0)|0|
|2|上-2|adown_data|afrzDown_data|S|0 (1)|0|
|3|上-3|bdown_data|bfrzDown_data|X|1 (0)|0|
|4|上-4|aup_data|afrzUp_data|D|0 (1)|0|
|5|上-5|bup_data|bfrzUp_data|C|1 (0)|0|
|6|上-6|aright_data|afrzRight_data|F|0 (1)|0|
|7|上-7|bright_data|bfrzRight_data|V|1 (0)|0|
|8|上-8|space_data|frzSpace_data|Space|2|1|
|9|上-9|cleft_data|cfrzLeft_data|N|3 (0)|0 (2)|
|10|上-10|dleft_data|dfrzLeft_data|J|4 (1)|0 (2)|
|11|上-11|cdown_data|cfrzDown_data|M|3 (0)|0 (2)|
|12|上-12|ddown_data|dfrzDown_data|K|4 (1)|0 (2)|
|13|上-13|cup_data|cfrzUp_data|&lt;|3 (0)|20 (2)|
|14|上-14|dup_data|dfrzUp_data|L|4 (1)|0 (2)|
|15|上-15|cright_data|cfrzRight_data|&gt;|3 (0)|0 (2)|
|16|上-16|dright_data|dfrzRight_data|+|4 (1)|0 (2)|

### 23key
|No|位置<br>Position|矢印<br>データ名<br>Arrow<br>name|フリーズアロー<br>データ名<br>Freeze-arrow<br>name|キー割当<br>Key<br>assignment|色<br>グループ<br>Color<br>group|シャッフル<br>グループ<br>Shuffle<br>group|
|----|----|----|----|----|----|----|
|0|上-0|aleft_data|afrzLeft_data|W|3|0|
|1|上-1|adown_data|afrzDown_data|E|3|0|
|2|上-2|aup_data|afrzUp_data|3 (4)|3|0|
|3|上-3|aright_data|afrzRight_data|R|3|0|
|4|上-8|bleft_data|bfrzLeft_data|U|4|1 (0)|
|5|上-9|bdown_data|bfrzDown_data|I|4|1 (0)|
|6|上-10|bup_data|bfrzUp_data|8 (9)|4|1 (0)|
|7|上-11|bright_data|bfrzRight_data|O|4|1 (0)|
|8|下-12|left_data|frzLeft_data|Z|0|2|
|9|下-13|leftdia_data|frzLdia_data|S|1|2|
|10|下-14|down_data|frzDown_data|X|0|2|
|11|下-15|space_data|frzSpace_data|D|1|2|
|12|下-16|up_data|frzUp_data|C|0|2|
|13|下-17|rightdia_data|frzRdia_data|F|1|2|
|14|下-18|right_data|frzRight_data|V|0|2|
|15|下-19|oni_data|foni_data|Space|2|3|
|16|下-20|sleft_data|sfrzLeft_data|N|0|4 (2)|
|17|下-21|sleftdia_data|sfrzLdia_data|J|1|4 (2)|
|18|下-22|sdown_data|sfrzDown_data|M|0|4 (2)|
|19|下-23|sspace_data|sfrzSpace_data|K|1|4 (2)|
|20|下-24|sup_data|sfrzUp_data|&lt;|0|4 (2)|
|21|下-25|srightdia_data|sfrzRdia_data|L|1|4 (2)|
|22|下-26|sright_data|sfrzRight_data|&gt;|0|4 (2)|

| [< 譜面ヘッダー仕様](./dos_header) | **譜面本体仕様** | [譜面エフェクト仕様 (速度/ 色/ 背景) >](./dos_effect) |
