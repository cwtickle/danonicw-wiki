**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-h0029-customReadyUse) | Japanese** 

[Home](./) &gt; [譜面ヘッダー仕様](dos_header) &gt; [デフォルトデザインの利用有無
](dos_header#-デフォルトデザインの利用有無)

| [<- customBackMainUse](dos-h0028-customBackMainUse) | **customReadyUse** || [titleSize ->](dos-h0030-titlesize) |

## customReadyUse
- プレイ開始時演出の利用有無
- 共通設定 ⇒ [g_presetObj.customDesignUse](dos-s0005-defaultDesign#デフォルトデザインの使用有無設定-g_presetobjcustomdesignuse)

### 使い方
```
|customReadyUse=false|
```
### 説明
メイン画面のReady?を個別に設定(もしくは非表示)するか、デフォルトのものを使用するかを指定します。  

|値|既定|内容|
|----|----|----|
|false|*|デフォルトの形式を採用|
|true||個別設定|

### 関連項目
- [readyDelayFrame](dos-h0052-readyDelayFrame) [:pencil:](dos-h0052-readyDelayFrame/_edit) Ready?が表示されるまでの遅延フレーム数
- [customBackUse](dos-h0027-customBackUse) [:pencil:](dos-h0027-customBackUse/_edit) 背景(プレイ画面以外)

### 更新履歴

|Version|変更内容|
|----|----|
|[v3.6.0](https://github.com/cwtickle/danoniplus/releases/tag/v3.6.0)|・デフォルト値変更 (true -> false)|
|[v2.1.0](https://github.com/cwtickle/danoniplus/releases/tag/v2.1.0)|・初回実装|

| [<- customBackMainUse](dos-h0028-customBackMainUse) | **customReadyUse** || [titleSize ->](dos-h0030-titlesize) |