**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-h0063-defaultFrzColorUse) | Japanese** 

[Home](./) &gt; [譜面ヘッダー仕様](dos_header) &gt; [プレイ時の初期設定](dos_header#-プレイ時の初期設定) &gt; [矢印・フリーズアロー色の設定](dos_header#矢印フリーズアロー色の設定)

| [<- defaultColorgrd](dos-h0061-defaultColorgrd) | **defaultFrzColorUse** | [frzScopeFromAC ->](dos-h0088-frzScopeFromAC) |

## defaultFrzColorUse
- フリーズアロー初期色(frzColor)が未指定時の適用方法の設定
- 共通設定 ⇒ [g_presetObj.frzColors](dos-s0004-frzArrow#フリーズアローのデフォルト色セットの利用有無-g_presetobjfrzcolors)

### 使い方
```
|defaultFrzColorUse=false|
```
### 説明
フリーズアロー色(frzColor)が指定されない場合、データ補完が行われます。  
その補完方法について、デフォルトのフリーズアロー色セットから取得するか、  
矢印色の設定(setColor)から取得するかを選択できるようにします。  
デフォルトは`true`です。

|値|既定|内容|
|----|----|----|
|false||矢印色の設定(setColor)から取得する|
|true|*|デフォルトのフリーズアロー色セットから取得する|

### 補足
この設定については、全体の設定として`danoni_setting.js`からも設定できます。  
どちらも設定があった場合は、譜面ヘッダーの値(defaultFrzColorUse)が優先されます。
```javascript
// フリーズアローのデフォルト色セットの利用有無 (true: 使用, false: 矢印色を優先してセット)
g_presetObj.frzColors = true;
```

またv13.3.1以降は、`danoni_setting.js`で定義された`g_presetObj.frzColors`が`false`、  
もしくは譜面ヘッダー`|defaultFrzColorUse=false|`のとき、  
`frzColor`が1要素でも定義されていれば、その`frzColor`の値を使用します。

```
|setColor=a,b,c,d,e|
|frzColor=A,B,C,D|
-> |frzColor=A,B,C,D$A,B,C,D$A,B,C,D$A,B,C,D$A,B,C,D|

|setColor=a,b,c,d,e|
|frzColor=A,B|
-> |frzColor=A,B,A,B$A,B,A,B$A,B,A,B$A,B,A,B$A,B,A,B|

|setColor=a,b,c,d,e|
|frzColor=,,A,B|
-> |frzColor=a,a,A,B$b,b,A,B$c,c,A,B$d,d,A,B$e,e,A,B|

|setColor=a,b,c,d,e|
-> |frzColor=a,a,a,a$b,b,b,b$c,c,c,c$d,d,d,d$e,e,e,e|
```

ver25.1.0以降は、この値をfalseに設定したとき、矢印に適用している個別／全体色変化もフリーズアローに適用するよう変更しましたが、    
ver25.5.0以降は元に戻しています（代わりに、[frzScopeFromAC](dos-h0088-frzScopeFromAC)の設定を追加）。

### 関連項目
- [**frzColor**](dos-h0004-frzColor) [:pencil:](dos-h0004-frzColor/_edit) フリーズアロー色
- [frzScopeFromAC](dos-h0088-frzScopeFromAC) [:pencil:](dos-h0088-frzScopeFromAC/_edit) 矢印色変化をフリーズアロー色変化に自動適用する範囲の設定
- [共通設定ファイル仕様](dos_setting) &gt; [フリーズアロー設定](dos-s0004-frzArrow)

### 更新履歴

|Version|変更内容|
|----|----|
|[v25.5.1](https://github.com/cwtickle/danoniplus/releases/tag/v25.5.1)|・falseに設定したときの矢印色変化→フリーズアロー色変化の自動適用を取り止め|
|[v25.1.0](https://github.com/cwtickle/danoniplus/releases/tag/v25.1.0)|・falseに設定したとき、矢印に適用している個別／全体色変化もフリーズアローに適用するよう変更|
|[v13.3.1](https://github.com/cwtickle/danoniplus/releases/tag/v13.3.1)|・フリーズアロー色(frzColor)の補完ルールを一部変更|
|[v13.1.1](https://github.com/cwtickle/danoniplus/releases/tag/v13.1.1)|・初回実装|

| [<- defaultColorgrd](dos-h0061-defaultColorgrd) | **defaultFrzColorUse** | [frzScopeFromAC ->](dos-h0088-frzScopeFromAC) |