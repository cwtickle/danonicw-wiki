**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-s0003-initialGauge) | Japanese** 

[↑ 共通設定ファイル仕様に戻る](dos_setting)

[ <- カスタムファイル設定 ](dos-s0002-customFile) | **ゲージ設定** | [フリーズアロー設定 -> ](dos-s0004-frzArrow)

## ゲージ設定
### デフォルトのゲージ設定 (g_presetObj.gauge)
⇒ 指定があった場合に優先される譜面ヘッダー：[difData](dos-h0002-difData), [gaugeX](dos-h0022-gaugeX)
- 未指定時のデフォルトのゲージ設定を指定します。  
- 初期状態では未指定で、この場合はソース本体の初期値が適用されます。
```javascript
g_presetObj.gauge = {
	//	Border: 70,  // ノルマ制でのボーダーライン、ライフ制にしたい場合は `x` を指定
	//	Recovery: 2, // 回復量
	//	Damage: 7,   // ダメージ量
	//	Init: 25,    // 初期値
};
```

### デフォルト以外のゲージ値の初期設定 (g_presetObj.gaugeCustom)
⇒ 指定があった場合に優先される譜面ヘッダー：[gaugeX](dos-h0022-gaugeX)
- ゲージ設定の各ゲージ値の初期値を設定別に設定できます。  
`SuddenDeath`は変更すると1ミスFailedにならなくなるため、変更非推奨です。
```javascript
g_presetObj.gaugeCustom = {
	Easy: {
		Border: 70,
		Recovery: 4,
		Damage: 7,
		Init: 25,
	},
	Hard: {
		Border: `x`,
		Recovery: 1,
		Damage: 50,
		Init: 100,
	},
	NoRecovery: {
		Border: `x`,
		Recovery: 0,
		Damage: 50,
		Init: 100,
	},
	SuddenDeath: {
		Border: `x`,
		Recovery: 0,
		Damage: setVal(g_rootObj.maxLifeVal, C_VAL_MAXLIFE, C_TYP_FLOAT),
		Init: 100,
	},
	Practice: {
		Border: `x`,
		Recovery: 0,
		Damage: 0,
		Init: 50,
	}
};
```

### カスタムゲージリスト (g_presetObj.gaugeList)
⇒ 指定があった場合に優先される譜面ヘッダー：[customGauge](dos-h0053-customGauge)
- カスタムゲージでデフォルトで使用するリストを指定します。
この指定がある場合、デフォルトで用意されているゲージ設定のリストは無視されます。

```javascript
// カスタムゲージ設定(デフォルト)
// 'ゲージ名': `回復・ダメージ量設定`　の形式で指定します。
// (F : 矢印数によらず固定, V: 矢印数により変動)
g_presetObj.gaugeList = {
	'Original': `F`,
	'Normal': `V`,
	'Hard': `V`,
};
```

### 空押し判定のデフォルト設定 (g_presetObj.excessiveJdgUse)
⇒ 指定があった場合に優先される譜面ヘッダー：[excessiveJdgUse](dos-h0093-excessiveJdgUse)
- 空押し判定をデフォルトで適用するかどうかを設定します。デフォルトは「適用しない」設定です。
- この設定を"true"にしたとしても、Excessive設定を画面から行うことは可能です。  
（あくまでデフォルト値の設定のみ）

```javascript
/**
  空押し判定を行うか
  判定させる場合は `true` を指定
*/
g_presetObj.excessiveJdgUse = `false`;
```

## 更新履歴

|Version|変更内容|
|----|----|
|[v32.5.0](https://github.com/cwtickle/danoniplus/releases/tag/v32.5.0)|・空押し判定の設定 (g_presetObj.excessiveJdgUse)を実装|
|[v26.3.1](https://github.com/cwtickle/danoniplus/releases/tag/v26.3.1)|・変数名変更<br>　・g_presetGauge -> g_presetObj.gauge<br>　・g_presetGaugeCustom -> g_presetObj.gaugeCustom<br>　・g_presetGaugeList -> g_presetObj.gaugeList|
|[v19.1.0](https://github.com/cwtickle/danoniplus/releases/tag/v19.1.0)|・カスタムゲージリスト (g_presetGaugeList)を実装|
|[v9.4.2](https://github.com/cwtickle/danoniplus/releases/tag/v9.4.2)|・デフォルト以外のゲージ値の初期設定 (g_presetGaugeCustom)の内容更新<br>　(カスタムゲージ実装によるライフ上限値可変に伴い)|
|[v9.4.0](https://github.com/cwtickle/danoniplus/releases/tag/v9.4.0)|・デフォルト以外のゲージ値の初期設定 (g_presetGaugeCustom)の内容更新<br>　(NoRecovery, SuddenDeath, Practiceを追加)|
|[v3.0.0](https://github.com/cwtickle/danoniplus/releases/tag/v3.0.0)|・デフォルトのゲージ設定 (g_presetGauge)、デフォルト以外のゲージ値の初期設定 (g_presetGaugeCustom)を初期実装|

[ <- カスタムファイル設定 ](dos-s0002-customFile) | **ゲージ設定** | [フリーズアロー設定 -> ](dos-s0004-frzArrow)