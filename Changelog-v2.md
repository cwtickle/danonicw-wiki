⭐ New Features / 🛠️ Improvements / 🐞 Bug Fixes

[**<- v3**](Changelog-v3) | **v2** | [**v1 ->**](Changelog-v1)  
(**🔖 [28 Releases](https://github.com/cwtickle/danoniplus/releases?q=tag%3Av2)** )

## [Warning] 更新終了後の不具合情報
- [更新を終了したバージョンの不具合情報#v2](DeprecatedVersionBugs#v2) を参照

## v2.9.11 ([2019-05-31](https://github.com/cwtickle/danoniplus/releases/tag/v2.9.11))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v2.9.11/total)](https://github.com/cwtickle/danoniplus/archive/v2.9.11.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v2.9.11/support/v2?style=social)](https://github.com/cwtickle/danoniplus/compare/v2.9.11...support/v2#files_bucket)
- 🐞 判定基準位置が1フレーム手前になっている問題を修正 ( PR [#318](https://github.com/cwtickle/danoniplus/pull/318), [#323](https://github.com/cwtickle/danoniplus/pull/323) ) <- :boom: **initial**

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v2.9.11)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v2.9.7...v2.9.11#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v2.9.11)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v2.9.11.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v2.9.11.tar.gz) )
/ 🎣 [**v5.6.6**](./Changelog-v5#v566-2019-05-31)

## v2.9.7 ([2019-05-21](https://github.com/cwtickle/danoniplus/releases/tag/v2.9.7))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v2.9.7/total)](https://github.com/cwtickle/danoniplus/archive/v2.9.7.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v2.9.7/support/v2?style=social)](https://github.com/cwtickle/danoniplus/compare/v2.9.7...support/v2#files_bucket)
- 🐞 back_dataのフレーム数が小さすぎる場合に画面が止まる問題を修正 ( PR [#305](https://github.com/cwtickle/danoniplus/pull/305) ) <- :boom: [**v2.4.0**](Changelog-v2#v240-2019-02-08)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v2.9.7)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v2.9.6...v2.9.7#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v2.9.7)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v2.9.7.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v2.9.7.tar.gz) )
/ 🎣 [**v5.2.1**](./Changelog-v5#v521-2019-05-21)

## v2.9.6 ([2019-05-11](https://github.com/cwtickle/danoniplus/releases/tag/v2.9.6))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v2.9.6/total)](https://github.com/cwtickle/danoniplus/archive/v2.9.6.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v2.9.6/support/v2?style=social)](https://github.com/cwtickle/danoniplus/compare/v2.9.6...support/v2#files_bucket)
- 🐞 ライフ回復・ダメージ量計算でフリーズアローのカウントが間違っていたのを修正 ( PR [#284](https://github.com/cwtickle/danoniplus/pull/284) ) <- :boom: [**v0.72.x**](Changelog-v0#v072x-2018-11-20)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v2.9.6)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v2.9.5...v2.9.6#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v2.9.6)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v2.9.6.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v2.9.6.tar.gz) )
/ 🎣 [**v4.8.1**](./Changelog-v4#v481-2019-05-11)

## v2.9.5 ([2019-04-27](https://github.com/cwtickle/danoniplus/releases/tag/v2.9.5))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v2.9.5/total)](https://github.com/cwtickle/danoniplus/archive/v2.9.5.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v2.9.5/support/v2?style=social)](https://github.com/cwtickle/danoniplus/compare/v2.9.5...support/v2#files_bucket)
- 🛠️ back_dataでX/Y座標の整数値制限を緩和（小数が使えるように）( PR [#254](https://github.com/cwtickle/danoniplus/pull/254) ) 
- 🐞 back_dataでOpacityが機能していない問題を修正 ( PR [#254](https://github.com/cwtickle/danoniplus/pull/254) ) <- :boom: [**v0.65.x**](Changelog-v0#v065x-2018-11-16)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v2.9.5)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v2.9.4...v2.9.5#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v2.9.5)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v2.9.5.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v2.9.5.tar.gz) )
/ 🎣 [**v4.0.1**](./Changelog-v4#v401-2019-04-27)

## v2.9.4 ([2019-04-21](https://github.com/cwtickle/danoniplus/releases/tag/v2.9.4))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v2.9.4/total)](https://github.com/cwtickle/danoniplus/archive/v2.9.4.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v2.9.4/support/v2?style=social)](https://github.com/cwtickle/danoniplus/compare/v2.9.4...support/v2#files_bucket)
- 🐞 ローディング100%で数秒止まる問題の修正 ( PR [#217](https://github.com/cwtickle/danoniplus/pull/217) ) <- :boom: [**v2.9.2**](Changelog-v2#v292-2019-02-25)
- 🐞 画像ファイル読込(preloadImages)時にjsエラーになる問題を修正 ( PR [#244](https://github.com/cwtickle/danoniplus/pull/244) ) <- :boom: [**v0.67.x**](Changelog-v0#v067x-2018-11-17)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v2.9.4)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v2.9.3...v2.9.4#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v2.9.4)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v2.9.4.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v2.9.4.tar.gz) )
/ 🎣 [**v3.11.1**](./Changelog-v3#v3111-2019-04-17)

----

## v2.9.3 ([2019-02-25](https://github.com/cwtickle/danoniplus/releases/tag/v2.9.3))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v2.9.3/total)](https://github.com/cwtickle/danoniplus/archive/v2.9.3.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v2.9.3/support/v2?style=social)](https://github.com/cwtickle/danoniplus/compare/v2.9.3...support/v2#files_bucket)
- 🛠️ 共通系関数のIE特殊対応を削除 ( PR [#213](https://github.com/cwtickle/danoniplus/pull/213) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v2.9.3)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v2.9.2...v2.9.3#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v2.9.3)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v2.9.3.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v2.9.3.tar.gz) )

## v2.9.2 ([2019-02-25](https://github.com/cwtickle/danoniplus/releases/tag/v2.9.2))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v2.9.2/total)](https://github.com/cwtickle/danoniplus/archive/v2.9.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v2.9.2/support/v2?style=social)](https://github.com/cwtickle/danoniplus/compare/v2.9.2...support/v2#files_bucket)
- 🐞 Web上で、曲時間がNaNになることがある不具合を修正 ( PR [#208](https://github.com/cwtickle/danoniplus/pull/208) ) <- :boom: [**v2.6.0**](Changelog-v2#v260-2019-02-10)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v2.9.2)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v2.9.1...v2.9.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v2.9.2)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v2.9.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v2.9.2.tar.gz) )<br>❤️ すずめ (@suzme)

## v2.9.1 ([2019-02-21](https://github.com/cwtickle/danoniplus/releases/tag/v2.9.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v2.9.1/total)](https://github.com/cwtickle/danoniplus/archive/v2.9.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v2.9.1/support/v2?style=social)](https://github.com/cwtickle/danoniplus/compare/v2.9.1...support/v2#files_bucket)
- 🐞 back_data利用時、フレーム計算が失敗しエラーとなる問題を修正 ( PR [#207](https://github.com/cwtickle/danoniplus/pull/207) ) <- :boom: [**v2.8.0**](Changelog-v2#v280-2019-02-17)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v2.9.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v2.9.0...v2.9.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v2.9.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v2.9.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v2.9.1.tar.gz) )

## v2.9.0 ([2019-02-17](https://github.com/cwtickle/danoniplus/releases/tag/v2.9.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v2.9.0/total)](https://github.com/cwtickle/danoniplus/archive/v2.9.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v2.9.0/support/v2?style=social)](https://github.com/cwtickle/danoniplus/compare/v2.9.0...support/v2#files_bucket)
- ⭐️ ロード画面をカスタムできるように変更 ( PR [#203](https://github.com/cwtickle/danoniplus/pull/203) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v2.9.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v2.8.1...v2.9.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v2.9.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v2.9.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v2.9.0.tar.gz) )<br>❤️ MFV2 (@MFV2)

## v2.8.1 ([2019-02-17](https://github.com/cwtickle/danoniplus/releases/tag/v2.8.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v2.9.1/total)](https://github.com/cwtickle/danoniplus/archive/v2.9.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v2.8.1/support/v2?style=social)](https://github.com/cwtickle/danoniplus/compare/v2.8.1...support/v2#files_bucket)
- 🐞 再生速度変更時のフェードアウト動作の修正 ( PR [#201](https://github.com/cwtickle/danoniplus/pull/201) ) <- :boom: [**v2.8.0**](Changelog-v2#v280-2019-02-17)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v2.8.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v2.8.0...v2.8.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v2.8.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v2.8.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v2.8.1.tar.gz) )<br>❤️ すずめ (@suzme)

## v2.8.0 ([2019-02-17](https://github.com/cwtickle/danoniplus/releases/tag/v2.8.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v2.8.0/total)](https://github.com/cwtickle/danoniplus/archive/v2.8.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v2.8.0/support/v2?style=social)](https://github.com/cwtickle/danoniplus/compare/v2.8.0...support/v2#files_bucket)
- ⭐️ 譜面ヘッダーで曲の再生速度を指定できるように変更 ( PR [#199](https://github.com/cwtickle/danoniplus/pull/199) ) 
- ⭐️ ソース更新年を表記するように変更

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v2.8.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v2.7.1...v2.8.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v2.8.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v2.8.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v2.8.0.tar.gz) )<br>❤️ すずめ (@suzme), izkdic

## v2.7.1 ([2019-02-16](https://github.com/cwtickle/danoniplus/releases/tag/v2.7.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v2.7.1/total)](https://github.com/cwtickle/danoniplus/archive/v2.7.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v2.7.1/support/v2?style=social)](https://github.com/cwtickle/danoniplus/compare/v2.7.1...support/v2#files_bucket)
- 🛠️ Firefoxかつローカル環境のとき、非推奨として表示するように変更 ( PR [#198](https://github.com/cwtickle/danoniplus/pull/198) )
- 🐞 楽曲再生前に処理落ち（ブラウザを非アクティブにした場合を含む）すると譜面がずれる問題の修正 ( PR [#197](https://github.com/cwtickle/danoniplus/pull/197) ) <- :boom: [**v0.73.x**](Changelog-v0#v073x-2018-11-20)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v2.7.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v2.7.0...v2.7.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v2.7.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v2.7.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v2.7.1.tar.gz) )<br>❤️ すずめ (@suzme)

## v2.7.0 ([2019-02-15](https://github.com/cwtickle/danoniplus/releases/tag/v2.7.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v2.7.0/total)](https://github.com/cwtickle/danoniplus/archive/v2.7.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v2.7.0/support/v2?style=social)](https://github.com/cwtickle/danoniplus/compare/v2.7.0...support/v2#files_bucket)
- 🛠️ 可能な場合にWebAudioAPIで楽曲を再生するように変更 ( PR [#195](https://github.com/cwtickle/danoniplus/pull/195) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v2.7.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v2.6.1...v2.7.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v2.7.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v2.7.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v2.7.0.tar.gz) )<br>❤️ すずめ (@suzme)

## v2.6.1 ([2019-02-12](https://github.com/cwtickle/danoniplus/releases/tag/v2.6.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v2.6.1/total)](https://github.com/cwtickle/danoniplus/archive/v2.6.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v2.6.1/support/v2?style=social)](https://github.com/cwtickle/danoniplus/compare/v2.6.1...support/v2#files_bucket)
- 🐞 フェードイン時間がblankFrameより小さい時にタイミングがずれる問題を修正 ( PR [#192](https://github.com/cwtickle/danoniplus/pull/192) ) <- :boom: [**v0.63.x**](Changelog-v0#v063x-2018-11-13)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v2.6.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v2.6.0...v2.6.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v2.6.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v2.6.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v2.6.1.tar.gz) )<br>❤️ すずめ (@suzme)

## v2.6.0 ([2019-02-10](https://github.com/cwtickle/danoniplus/releases/tag/v2.6.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v2.6.0/total)](https://github.com/cwtickle/danoniplus/archive/v2.6.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v2.6.0/support/v2?style=social)](https://github.com/cwtickle/danoniplus/compare/v2.6.0...support/v2#files_bucket)
- ⭐️ ロード画面の実装 ( PR [#190](https://github.com/cwtickle/danoniplus/pull/190) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v2.6.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v2.5.3...v2.6.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v2.6.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v2.6.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v2.6.0.tar.gz) )<br>❤️ すずめ (@suzme)

## v2.5.3 ([2019-02-09](https://github.com/cwtickle/danoniplus/releases/tag/v2.5.3))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v2.5.3/total)](https://github.com/cwtickle/danoniplus/archive/v2.5.3.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v2.5.3/support/v2?style=social)](https://github.com/cwtickle/danoniplus/compare/v2.5.3...support/v2#files_bucket)
- 🛠️ フェードインのスライダー処理の記述方法変更 ( PR [#189](https://github.com/cwtickle/danoniplus/pull/189) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v2.5.3)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v2.5.2...v2.5.3#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v2.5.3)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v2.5.3.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v2.5.3.tar.gz) )<br>❤️ すずめ (@suzme)

## v2.5.2 ([2019-02-09](https://github.com/cwtickle/danoniplus/releases/tag/v2.5.2))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v2.5.2/total)](https://github.com/cwtickle/danoniplus/archive/v2.5.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v2.5.2/support/v2?style=social)](https://github.com/cwtickle/danoniplus/compare/v2.5.2...support/v2#files_bucket)
- 🐞 ランクがPF/F/X以外のとき、リザルト画面に遷移できない不具合を修正 ( PR [#187](https://github.com/cwtickle/danoniplus/pull/187) ) <- :boom: [**v2.4.0**](Changelog-v2#v240-2019-02-08)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v2.5.2)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v2.5.1...v2.5.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v2.5.2)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v2.5.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v2.5.2.tar.gz) )

## v2.5.1 ([2019-02-09](https://github.com/cwtickle/danoniplus/releases/tag/v2.5.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v2.5.1/total)](https://github.com/cwtickle/danoniplus/archive/v2.5.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v2.5.1/support/v2?style=social)](https://github.com/cwtickle/danoniplus/compare/v2.5.1...support/v2#files_bucket)
- 🐞 フェードインのスライダー処理の不具合を修正 <- :boom: [**v2.4.0**](Changelog-v2#v240-2019-02-08)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v2.5.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v2.5.0...v2.5.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v2.5.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v2.5.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v2.5.1.tar.gz) )

## v2.5.0 ([2019-02-08](https://github.com/cwtickle/danoniplus/releases/tag/v2.5.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v2.5.0/total)](https://github.com/cwtickle/danoniplus/archive/v2.5.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v2.5.0/support/v2?style=social)](https://github.com/cwtickle/danoniplus/compare/v2.5.0...support/v2#files_bucket)
- ⭐️ 譜面データ（外部ファイル）の文字コードを指定できるように変更 ( PR [#180](https://github.com/cwtickle/danoniplus/pull/180) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v2.5.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v2.4.0...v2.5.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v2.5.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v2.5.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v2.5.0.tar.gz) )<br>❤️ すずめ (@suzme)

## v2.4.0 ([2019-02-08](https://github.com/cwtickle/danoniplus/releases/tag/v2.4.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v2.4.0/total)](https://github.com/cwtickle/danoniplus/archive/v2.4.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v2.4.0/support/v2?style=social)](https://github.com/cwtickle/danoniplus/compare/v2.4.0...support/v2#files_bucket)
- 🛠️ コード記述の見直し（IE11依存の記述見直し）( PR [#178](https://github.com/cwtickle/danoniplus/pull/178) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v2.4.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v2.3.0...v2.4.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v2.4.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v2.4.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v2.4.0.tar.gz) )

## v2.3.0 ([2019-02-06](https://github.com/cwtickle/danoniplus/releases/tag/v2.3.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v2.3.0/total)](https://github.com/cwtickle/danoniplus/archive/v2.3.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v2.3.0/support/v2?style=social)](https://github.com/cwtickle/danoniplus/compare/v2.3.0...support/v2#files_bucket)
- ⭐️ 譜面データの外部ファイル化に対応 ( PR [#174](https://github.com/cwtickle/danoniplus/pull/174), Issue [#145](https://github.com/cwtickle/danoniplus/pull/145) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v2.3.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v2.2.0...v2.3.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v2.3.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v2.3.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v2.3.0.tar.gz) )<br>❤️ すずめ (@suzme)

## v2.2.0 ([2019-02-05](https://github.com/cwtickle/danoniplus/releases/tag/v2.2.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v2.2.0/total)](https://github.com/cwtickle/danoniplus/archive/v2.2.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v2.2.0/support/v2?style=social)](https://github.com/cwtickle/danoniplus/compare/v2.2.0...support/v2#files_bucket)
- ⭐️ 譜面ヘッダーにて、譜面別に（製作者側が）タイミング調整できるように変更 ( PR [#173](https://github.com/cwtickle/danoniplus/pull/173) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v2.2.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v2.1.2...v2.2.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v2.2.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v2.2.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v2.2.0.tar.gz) )<br>❤️ すずめ (@suzme)

## v2.1.2 ([2019-01-24](https://github.com/cwtickle/danoniplus/releases/tag/v2.1.2))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v2.1.2/total)](https://github.com/cwtickle/danoniplus/archive/v2.1.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v2.1.2/support/v2?style=social)](https://github.com/cwtickle/danoniplus/compare/v2.1.2...support/v2#files_bucket)
- 🐞 メインのReady?表示が表示されない不具合を修正 ( PR [#171](https://github.com/cwtickle/danoniplus/pull/171) ) <- :boom: [**v2.1.0**](Changelog-v2#v210-2019-01-24)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v2.1.2)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v2.1.1...v2.1.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v2.1.2)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v2.1.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v2.1.2.tar.gz) )

## v2.1.1 ([2019-01-24](https://github.com/cwtickle/danoniplus/releases/tag/v2.1.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v2.1.1/total)](https://github.com/cwtickle/danoniplus/archive/v2.1.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v2.1.1/support/v2?style=social)](https://github.com/cwtickle/danoniplus/compare/v2.1.1...support/v2#files_bucket)
- 🐞 デフォルト背景が前に見えてしまう不具合を修正 ( PR [#170](https://github.com/cwtickle/danoniplus/pull/170) ) <- :boom: [**v2.1.0**](Changelog-v2#v210-2019-01-24)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v2.1.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v2.1.0...v2.1.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v2.1.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v2.1.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v2.1.1.tar.gz) )

## v2.1.0 ([2019-01-24](https://github.com/cwtickle/danoniplus/releases/tag/v2.1.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v2.1.0/total)](https://github.com/cwtickle/danoniplus/archive/v2.1.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v2.1.0/support/v2?style=social)](https://github.com/cwtickle/danoniplus/compare/v2.1.0...support/v2#files_bucket)
- ⭐️ （デフォルト）タイトル文字・矢印及び背景をmain側に移植 ( PR [#169](https://github.com/cwtickle/danoniplus/pull/169) )
- ⭐️ タイトル文字のフォント設定に対応 ( PR [#169](https://github.com/cwtickle/danoniplus/pull/169) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v2.1.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v2.0.2...v2.1.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v2.1.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v2.1.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v2.1.0.tar.gz) )

## v2.0.2 ([2019-01-24](https://github.com/cwtickle/danoniplus/releases/tag/v2.0.2))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v2.0.2/total)](https://github.com/cwtickle/danoniplus/archive/v2.0.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v2.0.2/support/v2?style=social)](https://github.com/cwtickle/danoniplus/compare/v2.0.2...support/v2#files_bucket)
- 🐞 フェードアウト中にリザルト画面へ移行させた場合の音量不具合を修正 ( PR [#167](https://github.com/cwtickle/danoniplus/pull/167) ) <- :boom: **initial**
- 🐞 初期ライフ量が常に整数値になるように修正 ( PR [#167](https://github.com/cwtickle/danoniplus/pull/167) ) <- :boom: **initial**

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v2.0.2)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v2.0.1...v2.0.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v2.0.2)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v2.0.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v2.0.2.tar.gz) )

## v2.0.1 ([2019-01-19](https://github.com/cwtickle/danoniplus/releases/tag/v2.0.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v2.0.1/total)](https://github.com/cwtickle/danoniplus/archive/v2.0.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v2.0.1/support/v2?style=social)](https://github.com/cwtickle/danoniplus/compare/v2.0.1...support/v2#files_bucket)
- 🛠️ IE11の非対応化に伴うコード修正 ( PR [#166](https://github.com/cwtickle/danoniplus/pull/166) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v2.0.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v2.0.0...v2.0.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v2.0.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v2.0.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v2.0.1.tar.gz) )<br>❤️ kuroclef (@kuroclef)

## v2.0.0 ([2019-01-18](https://github.com/cwtickle/danoniplus/releases/tag/v2.0.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v2.0.0/total)](https://github.com/cwtickle/danoniplus/archive/v2.0.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v2.0.0/support/v2?style=social)](https://github.com/cwtickle/danoniplus/compare/v2.0.0...support/v2#files_bucket)
- 🛠️ テンプレート文字列を使用するように変更 ( PR [#162](https://github.com/cwtickle/danoniplus/pull/162), [#163](https://github.com/cwtickle/danoniplus/pull/163) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v2.0.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v1.15.0...v2.0.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v2.0.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v2.0.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v2.0.0.tar.gz) )<br>❤️ kuroclef (@kuroclef)

[**<- v3**](Changelog-v3) | **v2** | [**v1 ->**](Changelog-v1)
