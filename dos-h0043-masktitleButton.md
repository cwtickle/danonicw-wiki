**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-h0043-masktitleButton) | Japanese** 

[Home](./) &gt; [譜面ヘッダー仕様](dos_header) &gt; [タイトル・結果画面の初期設定](dos_header#%EF%B8%8F-タイトル結果画面の初期設定)

| [<- commentExternal](dos-h0068-commentExternal) | **masktitleButton** | [resultDelayFrame ->](dos-h0076-resultDelayFrame) |

## masktitleButton
- タイトル画面上のボタン群の有効/無効設定

### 使い方
```
|masktitleButton=false|
```
### 説明
タイトル画面上のボタンを無効にする（マスクする）かどうかを設定します。  
デフォルトはfalse（マスクしない/ボタン有効）。trueでボタンを無効化します。  

|値|既定|内容|
|----|----|----|
|false|*|マスクしない (ボタン有効)|
|true||マスクする (ボタン無効)|

### 関連項目
- [maskresultButton](dos-h0044-maskresultButton) [:pencil:](dos-h0044-maskresultButton/_edit) リザルト画面上のボタン群の有効/無効設定
- [**customJs**](dos-h0019-customjs) [:pencil:](dos-h0019-customjs/_edit) カスタムjsファイルの指定

### 更新履歴

|Version|変更内容|
|----|----|
|[v6.4.0](https://github.com/cwtickle/danoniplus/releases/tag/v6.4.0)|・初回実装|

| [<- commentExternal](dos-h0068-commentExternal) | **masktitleButton** | [resultDelayFrame ->](dos-h0076-resultDelayFrame) |