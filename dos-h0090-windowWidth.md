**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-h0090-windowWidth) | Japanese** 

[Home](./) &gt; [譜面ヘッダー仕様](dos_header) &gt; [クレジット関連、共通設定](dos_header#-クレジット関連共通設定)

| [<- autoSpread](dos-h0089-autoSpread) | **windowWidth** | [windowHeight ->](dos-h0097-windowHeight) |

## windowWidth
- 画面ウィンドウの横幅設定
- 共通設定 ⇒ [g_presetObj.autoMinWidth](dos-s0001-makerInfo#最小横幅設定-g_presetobjautominwidth)

### 使い方
```
|windowWidth=700|
```
### 説明
この作品の画面ウィンドウの横幅をpx単位で設定します。 

### 関連項目
- [windowHeight](dos-h0097-windowHeight) [:pencil:](dos-h0097-windowHeight/_edit) 画面ウィンドウの高さ
- [**autoSpread**](dos-h0089-autoSpread) [:pencil:](dos-h0089-autoSpread/_edit) 自動横幅拡張設定
- [**windowAlign**](dos-h0091-windowAlign) [:pencil:](dos-h0091-windowAlign/_edit) 画面ウィンドウの位置
- [共通設定ファイル仕様](dos_setting) &gt; [制作者クレジット・共通設定](dos-s0001-makerInfo)

### 更新履歴

|Version|変更内容|
|----|----|
|[v27.1.0](https://github.com/cwtickle/danoniplus/releases/tag/v27.1.0)|・初回実装|

| [<- autoSpread](dos-h0089-autoSpread) | **windowWidth** | [windowHeight ->](dos-h0097-windowHeight) |