
[^ Tips Indexに戻る](./tips-index)

| < [ブックマークレット集](./tips-0019-bookmarklet) | **プレイ画面外のテキスト入力を有効にする** ||

# プレイ画面外のテキスト入力を有効にする
- Dancing☆Onigiri (CW Edition)の画面では、プレイ中に想定しない動作が発生することを防ぐため、  
たいていのショートカットは制限されています。
- 通常は問題ないのですが、テキスト入力を画面外で行いたいときに半角スペースやEnterが使えないため問題となることがあります。

## 使い方
- 下記のようなJavaScriptをHTML本体に記述します。
- 画面内にテキストボックスを用意する場合はcustomjs内でも良いです。

### JavaScript
```javascript
// 譜面エリアにフォーカスが当たっているときだけ、onkeydown, oncontextmenu の設定をリセット
let bkEvent, bkEventCxt;
const dfEvent = evt => {};
const dfCxt = evt => true;

// Dancing☆Onigiri (CW Edition)の本体のショートカット制限を止めたいidを配列でリスト化
// この場合は、"inputData"と"inputMemo"は除外する
[`inputData`, `inputMemo`].forEach(txt => {
    // 対象にフォーカスが当たっているときは、Dancing☆Onigiri (CW Edition)の制限を無効化
    document.getElementById(txt).addEventListener('focus', () => {
        if (document.onkeydown !== dfEvent) {
            bkEvent = document.onkeydown;
            bkEventCxt = document.oncontextmenu;
        }
        document.onkeydown = dfEvent;
        document.oncontextmenu = dfCxt;
    });
    // 対象から外れた場合は、元に戻す（Dancing☆Onigiri (CW Edition)の制限を有効化）
    document.getElementById(txt).addEventListener('blur', () => {
        document.onkeydown = bkEvent;
        document.oncontextmenu = bkEventCxt;
    });
});
```

### HTML
```html
<!-- テキストエリアを定義 -->
<textarea id="inputData" name="inputData" cols="110" rows="10"></textarea>
<textarea id="inputMemo" name="inputMemo" cols="110" rows="10"></textarea>
```

## ページ作成者
- ティックル

[^ Tips Indexに戻る](./tips-index)

| < [ブックマークレット集](./tips-0019-bookmarklet) | **プレイ画面外のテキスト入力を有効にする** ||
