**[English](https://github.com/cwtickle/danoniplus-docs/wiki/tips-0004-extraKeys-v30) | Japanese** 

[← Tips Indexに戻る](./tips-index)
# カスタムキーテンプレート
- キーコンフィグパターン個別のスクロール拡張や略記指定を行っているため、  
本体バージョンがver24.3.0以降で無いと使用できない場合があります。その場合は[こちら](./tips-0004-extrakeys-v23)をご覧ください。
- カスタムキー定義については [キーの仕様について](./keys) もご覧ください。
- 複数のカスタムキーを使用する際は、[共通設定ファイル](./dos-s0010-customKeys)に記載する場合、`|keyExtraList=6,11j|`のようにkeyExtraListのみまとめる必要があります。定義したカスタムキーを他の作品にも使用する場合、譜面ヘッダーではなく**共通設定ファイルに書くのが便利**です。
- Dancing☆Onigiriエディター (CW Edition対応)における定義方法については[こちらのページ](https://github.com/superkuppabros/danoni-editor/wiki/%E9%9D%9E%E6%A8%99%E6%BA%96%E3%82%AD%E3%83%BC%E3%81%AE%E3%82%A8%E3%83%87%E3%82%A3%E3%82%BF%E5%AE%9A%E7%BE%A9%E3%81%BE%E3%81%A8%E3%82%81)をご覧ください。

## 共通（譜面データ本体）
- 下記で定義するキーを全て適用する場合、次の`keyExtraList`を使ってください。  
⇒ ver31.3.1以降は`keyExtraList`の指定が不要になりました。
```
|keyExtraList=1,5g,5p,6,7e,8i,9j,9g,9v,10,10p,11j,11f,11g,12i,14e,20,21i|
```
## 1key
<img src="https://user-images.githubusercontent.com/44026291/221399529-b7f00fb5-d53f-45a5-a8f1-857b07ed1086.png" width="50%">

### 譜面データ本体
```
|keyCtrl1=32/0|
|chara1=space|
|color1=2|
|stepRtn1=onigiri|
```

## 5gkey
<img src="https://user-images.githubusercontent.com/44026291/221360877-393dee96-9b2a-44d8-9b4f-6930a4a34998.png" width="50%">

### 譜面データ本体
```
|keyCtrl5g=112/0,113/0,114/0,115/0,13/16$49/0,50/0,51/0,52/0,13/16$5_0$5_1$5_2|
|chara5g=5_0$5g_0$5_0$5_1$5_2|
|color5g=5_0$5g_0$5_0$5_1$5_2|
|stepRtn5g=0,45,135,180,giko$5g_0$5_0$5_1$5_2|
|blank5g=57.5$57.5$57.5$57.5$57.5|
|shuffle5g=5_0$5g_0$5_0$5_1$5_2|
|scroll5g=5_0$5g_0$5_0$5_1$5_2|
|transKey5g=$$5$5$5|
```

## 5pkey
<img src="https://user-images.githubusercontent.com/44026291/221357788-4b92a28a-f7a2-4352-9f92-a6329749b0d2.png" width="50%">

### 譜面データ本体
```
|keyCtrl5p=88/90,69/87,68/70/83,84/82,86/67$66/78,89/85,72/74/75,73/79,77/188$5_0$5_1$5_2|
|chara5p=5_2$5p_0$5_0$5_1$5_2|
|color5p=0,1,2,1,0$5p_0$5_0$5_1$5_2|
|stepRtn5p=-45,45,onigiri,135,-135$5p_0$5_0$5_1$5_2|
|blank5p=57.5$57.5$57.5$57.5$57.5|
|shuffle5p=0,0,0,0,0$5p_0$5_0$5_1$5_2|
|scroll5p=5_2$5p_0$5_0$5_1$5_2|
|transKey5p=$$5$5$5|
```

## 6key
<img src="https://user-images.githubusercontent.com/44026291/221349590-689c9f8b-f0e5-4ee9-8430-460d91e09418.png" width="55%">

### 譜面データ本体
```
|keyCtrl6=75/0,79/0,76/0,80/0,187/0,32/0$32/0,75/0,79/0,76/0,80/0,187/0|
|chara6=left,leftdia,down,rightdia,right,space$space,left,leftdia,down,rightdia,right|
|color6=0,1,0,1,0,2$2,0,1,0,1,0|
|stepRtn6=0,45,-90,135,180,onigiri$onigiri,0,45,-90,135,180|
|shuffle6=1,1,1,1,1,0$0,1,1,1,1,1|
```

## 7ekey
<img src="https://user-images.githubusercontent.com/44026291/221357898-f16a7765-d4d6-4c8f-82d9-4ff8fa382d52.png" width="70%">

### 譜面データ本体
```
|keyCtrl7e=9/0,83/0,69/0,68/0,82/0,70/0,78/0,54/0,55/0$55/0,56/0,78/0,76/0,80/0,187/0,192/0,186/0,13/0|
|chara7e=oni,left,down,gor,up,right,space,sleft,sright$sright,sleft,space,right,up,gor,down,left,oni|
|color7e=2,0,1,0,1,0,2,3,3$3,3,2,0,1,0,1,0,2|
|stepRtn7e=onigiri,0,45,-90,135,180,onigiri,iyo,giko$giko,iyo,onigiri,0,45,-90,135,180,onigiri|
|blank7e=52.5$52.5|
|shuffle7e=2,0,0,0,0,0,1,0,0$0,0,1,0,0,0,0,0,2|
|scroll7e=Cross::1,-1,-1,-1,-1,-1,-1,1,1$Cross::1,1,-1,-1,-1,-1,-1,-1,1|
```

## 8ikey
<img src="https://user-images.githubusercontent.com/44026291/221349785-4c17952a-35e0-44dd-934e-876658c9a3b4.png" width="60%">

### 譜面データ本体
```
|keyCtrl8i=65/90,83/88,68/67,32/0,37/0,40/0,38/0,39/0$8i_0$8_0$8_1$8_2$8_3$8_4$8_5|
|chara8i=left,leftdia,down,space,up,rightdia,right,sleft$8i_0$left,leftdia,down,up,rightdia,right,sleft,space$space,left,leftdia,down,up,rightdia,right,sleft$8i_3$8i_3$8i_3$8i_3|
|color8i=1,1,1,2,0,0,0,0$8i_0$8_0$8_1$8_2$8_3$8_4$8_5|
|stepRtn8i=0,-90,180,onigiri,0,-90,90,180$giko,morara,iyo,onigiri,0,-90,90,180$8_0$8_1$8_2$8_3$8_4$8_5|
|shuffle8i=0,0,0,2,1,1,1,1$8i_0$8_0$8_1$8_2$8_3$8_4$8_5|
|transKey8i=$$8$8$12$12$12$12|
```

## 9jkey
<img src="https://user-images.githubusercontent.com/44026291/221361969-48acb80f-b78d-4f90-9f86-8159caba3bed.png" width="70%">

### 譜面データ本体
```
|keyCtrl9j=9/0,83/0,68/0,70/0,32/0,74/0,75/0,76/0,13/0$9A_0$9A_1$9A_2|
|chara9j=9A_0$9A_0$9A_1$9A_2|
|color9j=2,0,1,0,2,0,1,0,2$9A_0$9A_1$9A_2|
|stepRtn9j=giko,0,-45,-90,onigiri,90,135,180,iyo$9A_0$9A_1$9A_2|
|shuffle9j=1,0,0,0,1,0,0,0,1$9A_0$9A_1$9A_2|
|scroll9j=9A_0$9A_0$9A_1$9A_2|
|transKey9j=$9A$9A$9B|
```

## 9gkey
<img src="https://user-images.githubusercontent.com/44026291/221360972-d8a1e505-4c46-444c-a237-e83ef4ae58ff.png" width="70%">

### 譜面データ本体
```
|keyCtrl9g=112/0,113/0,114/0,115/0,116/0,117/0,118/0,119/0,13/16$49/0,50/0,51/0,52/0,53/0,54/0,55/0,56/0,13/16|
|chara9g=left,down,up,right,sleft,sdown,sup,sright,space$9g_0|
|color9g=0,0,0,0,1,1,1,1,2$9g_0|
|stepRtn9g=0,45,135,180,0,45,135,180,giko$9g_0|
|blank9g=52.5$52.5|
|shuffle9g=0,0,0,0,1,1,1,1,2$0,0,0,0,1,1,1,1,2|
```

## 9tkey
- 9tkeyはカスタムキー定義だけで完結しません。下記のページをご覧ください。
https://github.com/suzme/danoni-9t

## 9vkey
<img src="https://user-images.githubusercontent.com/44026291/221349811-b692ccbf-9581-4332-9f52-10b016029a24.png" width="70%">

### 譜面データ本体
```
|keyCtrl9v=52/0,82/0,70/0,86/0,32/0,78/0,74/0,73/0,57/0$9B_0|
|chara9v=9B_0$9B_0|
|color9v=1,0,1,0,2,0,1,0,1$9B_0|
|stepRtn9v=90,120,150,180,onigiri,0,30,60,90$9B_0|
|shuffle9v=9B_0$9B_0|
|scroll9v=---::1,1,-1,-1,-1,-1,-1,1,1/flat::1,1,1,1,1,1,1,1,1$9B_0|
|transKey9v=$9B|
```

## 10key
<img src="https://user-images.githubusercontent.com/44026291/221349843-63ec207f-6f2f-4d8d-b747-bef4f711b0af.png" width="70%">

### 譜面データ本体
```
|minWidth10=650|
|keyCtrl10=83/0,68/0,69/82,70/0,32/0,74/0,75/0,73/79,76/0,13/0|
|chara10=left,down,up,right,space,sleft,sdown,sup,sright,sspace|
|color10=0,0,0,0,2,1,1,1,1,2|
|stepRtn10=0,-90,90,180,onigiri,0,-90,90,180,onigiri|
|blank10=52.5|
|shuffle10=0,0,0,0,1,2,2,2,2,3|
|scroll10=Cross::1,1,-1,-1,-1,-1,-1,1,1,1/Split::1,1,1,1,-1,-1,-1,-1,-1,-1/Alternate::1,-1,1,-1,1,-1,1,-1,1,-1/AA-Split::-1,-1,-1,-1,1,-1,-1,-1,-1,1|
```

## 10pkey
<img src="https://user-images.githubusercontent.com/44026291/221349870-272e0ede-c691-4eb5-8508-bd97bff397c3.png" width="70%">

### 譜面データ本体
```
|minWidth10p=650|
|keyCtrl10p=88/90,69/87,68/70/83,84/82,86/67,66/78,89/85,72/74/75,73/79,77/188$88/90,87/81,68/83/70,82/84,67/86,77/78,85/89,75/74/76,79/80,188/190$11i_0|
|chara10p=left,down,gor,up,right,sleft,sdown,siyo,sup,sright$10p_0$11i_0|
|color10p=0,1,2,1,0,0,1,2,1,0$10p_0$11i_0|
|stepRtn10p=-45,45,onigiri,135,-135,-45,45,onigiri,135,-135$10p_0$11i_0|
|div10p=10$10$11|
|blank10p=50$50$50|
|shuffle10p=0,0,0,0,0,0,0,0,0,0$10p_0$11i_0|
|scroll10p=Cross::1,1,1,-1,-1,-1,-1,1,1,1/Split::1,1,1,1,1,-1,-1,-1,-1,-1/Alternate::1,-1,1,-1,1,1,-1,1,-1,1$10p_0$11i_0|
|transKey10p=$$11i|
```

## 11jkey
<img src="https://user-images.githubusercontent.com/44026291/221349884-3f1497f6-8fa8-470b-8ff8-c51e41a7b884.png" width="70%">

### 譜面データ本体
```
|minWidth11j=650|
|keyCtrl11j=9/0,83/0,68/0,69/82,70/0,32/0,74/0,75/0,73/79,76/0,13/0|
|chara11j=gor,left,down,up,right,space,sleft,sdown,sup,sright,siyo|
|color11j=2,0,0,0,0,2,3,3,3,3,2|
|stepRtn11j=giko,0,-90,90,180,onigiri,0,-90,90,180,iyo|
|blank11j=50|
|shuffle11j=0,1,1,1,1,2,3,3,3,3,4|
|scroll11j=Cross::1,1,1,-1,-1,-1,-1,-1,1,1,1/Split::1,1,1,1,1,-1,-1,-1,-1,-1,-1/Alternate::1,-1,1,-1,1,-1,1,-1,1,-1,1/AA-Split::1,-1,-1,-1,-1,1,-1,-1,-1,-1,1|
```

## 11fkey
<img src="https://user-images.githubusercontent.com/44026291/221349907-d8388d66-09b7-4dc1-8d4d-ec6462b7b0b0.png" width="70%">

### 譜面データ本体
```
|minWidth11f=650|
|keyCtrl11f=83/0,69/0,68/0,82/0,70/0,32/0,74/0,73/0,75/0,79/0,76/0$88/0,68/0,67/0,70/0,86/0,32/0,78/0,74/0,77/0,75/0,188/0$69/0,82/0,73/0,79/0,83/0,68/0,70/0,32/0,74/0,75/0,76/0$83/0,88/67,68/0,69/82,70/0,32/0,74/0,77/188,75/0,73/79,76/0|
|chara11f=left,leftdia,down,space,up,rightdia,right,sleft,sdown,sup,sright$11f_0$leftdia,space,sleft,sup,left,down,up,rightdia,right,sdown,sright$11f_0|
|color11f=0,1,0,1,0,2,0,1,0,1,0$11f_0$3,3,3,3,0,1,0,2,0,1,0$0,0,2,0,0,2,3,3,2,3,3|
|stepRtn11f=0,45,-90,135,180,onigiri,0,45,-90,135,180$11f_0$0,-90,90,180,0,-45,-90,onigiri,90,135,180$0,-90,giko,90,180,onigiri,0,-90,iyo,90,180|
|pos11f=0,1,2,3,4,5,6,7,8,9,10$11f_0$0,1,4,5,6,7,8,9,10,11,12$11f_0|
|div11f=11$11$6$11|
|blank11f=50$50$55$50|
|shuffle11f=0,0,0,0,0,1,2,2,2,2,2$11f_0$0,0,0,0,1,1,1,2,1,1,1$0,0,1,0,0,2,3,3,4,3,3|
|scroll11f=Cross::1,1,1,-1,-1,-1,-1,-1,1,1,1/Split::1,1,1,1,1,-1,-1,-1,-1,-1,-1/Alternate::1,-1,1,-1,1,-1,1,-1,1,-1,1$11f_0$Split::1,1,-1,-1,1,1,1,-1,-1,-1,-1$11f_0|
|assist11f=Left::1,1,1,1,1,0,0,0,0,0,0/Right::0,0,0,0,0,0,1,1,1,1,1$11f_0$$11f_0|
|transKey11f=$$11F$11i|
```

## 11gkey
<img src="https://user-images.githubusercontent.com/44026291/221349937-86fe3ce9-5ac3-40d0-9e5f-a40209ff9c1b.png" width="70%">

### 譜面データ本体
```
|minWidth11g=650|
|keyCtrl11g=90/0,83/0,88/0,68/70,67/86,32/0,77/78,75/74,188/0,76/0,190/0$83/0,69/0,68/0,82/0,70/0,32/0,74/0,73/0,75/0,79/0,76/0$69/0,82/0,73/0,79/0,83/0,68/0,70/0,32/0,74/0,75/0,76/0$83/0,88/67,68/0,69/82,70/0,32/0,74/0,77/188,75/0,73/79,76/0|
|chara11g=left,down,up,right,space,tspace,sleft,sdown,sup,sright,sspace$11g_0$down,right,sdown,sright,left,up,space,tspace,sleft,sup,sspace$11g_0|
|color11g=0,1,0,1,0,2,0,1,0,1,0$11g_0$3,3,3,3,0,1,0,2,0,1,0$0,0,2,0,0,2,3,3,2,3,3|
|stepRtn11g=0,-30,-60,-90,-120,onigiri,60,90,120,150,180$0,-45,-90,-135,180,onigiri,0,45,90,135,180$0,-90,90,180,0,-45,-90,onigiri,90,135,180$0,-90,giko,90,180,onigiri,0,-90,iyo,90,180|
|pos11g=0,1,2,3,4,5,6,7,8,9,10$11g_0$0,1,4,5,6,7,8,9,10,11,12$11g_0|
|div11g=11$11$6$11|
|blank11g=50$50$55$50|
|shuffle11g=0,0,0,0,0,1,2,2,2,2,2$11g_0$0,0,0,0,1,1,1,2,1,1,1$0,0,1,0,0,2,3,3,4,3,3|
|scroll11g=Cross::1,1,1,-1,-1,-1,-1,-1,1,1,1/Split::1,1,1,1,1,-1,-1,-1,-1,-1,-1/Alternate::1,-1,1,-1,1,-1,1,-1,1,-1,1$11g_0$Split::1,1,-1,-1,1,1,1,-1,-1,-1,-1$11g_0|
|assist11g=Left::1,1,1,1,1,0,0,0,0,0,0/Right::0,0,0,0,0,0,1,1,1,1,1$11g_0$$11g_0|
|transKey11g=$11f$11F$11i|
```

## 12ikey
<img src="https://user-images.githubusercontent.com/44026291/221349966-82a28036-ecb6-405d-82c2-4be089a501b5.png" width="80%">

### 譜面データ本体
```
|minWidth12i=675|
|keyCtrl12i=112/0,113/0,114/0,115/0,116/0,117/0,118/0,119/0,120/0,121/0,122/0,123/0$81/0,87/0,69/0,82/0,84/0,89/0,85/0,73/0,79/0,80/0,192/0,219/0|
|chara12i=oni,left,leftdia,down,sleft,sdown,sup,sright,space,up,rightdia,right$12i_0|
|color12i=1,0,1,0,3,3,3,3,0,1,0,1$12i_0|
|stepRtn12i=45,0,-45,-90,giko,onigiri,iyo,c,90,135,180,225$12i_0|
|blank12i=50$50|
|shuffle12i=0,0,0,0,1,1,1,1,2,2,2,2$12i_0|
|scroll12i=Cross::1,1,1,1,-1,-1,-1,-1,1,1,1,1/Split::1,1,1,1,1,1,-1,-1,-1,-1,-1,-1$12i_0|
```

### 譜面データ本体(12keyの別キーとして定義)
- 下記の方法では、既存キーの設定に追記する方法を取っています。  
- ver30.4.0以前の場合、`12_(0)`は`12_4`に置き換えてください。
```
|keyExtraList=12|
|minWidth12=675|
|append12=true|
|keyCtrl12=112/0,113/0,114/0,115/0,116/0,117/0,118/0,119/0,120/0,121/0,122/0,123/0$81/0,87/0,69/0,82/0,84/0,89/0,85/0,73/0,79/0,80/0,192/0,219/0|
|chara12=oni,left,leftdia,down,sleft,sdown,sup,sright,space,up,rightdia,right$12_(0)|
|color12=1,0,1,0,3,3,3,3,0,1,0,1$12_(0)|
|pos12=0,1,2,3,4,5,6,7,8,9,10,11$12_(0)|
|stepRtn12=45,0,-45,-90,giko,onigiri,iyo,c,90,135,180,225$12_(0)|
|div12=12$12|
|blank12=50$50|
|shuffle12=0,0,0,0,1,1,1,1,2,2,2,2$12_(0)|
|scroll12=Cross::1,1,1,1,-1,-1,-1,-1,1,1,1,1/Split::1,1,1,1,1,1,-1,-1,-1,-1,-1,-1$12_(0)|
|transKey12=12i$12i|
```

## 14ekey
<img src="https://user-images.githubusercontent.com/44026291/221357975-acf6d090-52d8-4693-90d0-b940d4e340d3.png" width="90%">

### 譜面データ本体
```
|minWidth14e=950|
|keyCtrl14e=9/0,88/0,68/0,67/0,70/0,86/0,84/0,89/0,85/0,73/0,188/0,76/0,190/0,187/0,191/0,13/0|
|chara14e=aleft,bleft,adown,bdown,aup,bup,aright,bright,cleft,dleft,cdown,ddown,cup,dup,cright,dright|
|color14e=2,0,1,0,1,0,3,3,3,3,0,1,0,1,0,2|
|stepRtn14e=onigiri,0,45,-90,135,180,giko,onigiri,iyo,c,0,45,-90,135,180,onigiri|
|pos14e=0,1,2,3,4,5,6.5,7.5,8.5,9.5,11,12,13,14,15,16|
|div14e=17|
|blank14e=50|
|shuffle14e=1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1|
|scroll14e=Cross::1,-1,-1,-1,-1,-1,1,1,1,1,-1,-1,-1,-1,-1,1|
```

## 20key
<img src="https://user-images.githubusercontent.com/44026291/221358365-0cc27707-f5f5-40f2-976a-3b59d0cefa71.png" width="75%">

### 譜面データ本体
```
|minWidth20=850|
|keyCtrl20=87/0,69/0,51/52,82/0,85/0,73/0,56/57,79/0,37/0,40/0,38/0,39/0,32/0,78/0,74/0,77/0,75/0,188/0,76/0,190/0$20_0|
|chara20=aleft,adown,aup,aright,sleft,sdown,sup,sright,bleft,bdown,bup,bright,oni,left,leftdia,down,space,up,rightdia,right$20_0|
|color20=4,4,4,4,3,3,3,3,4,4,4,4,2,0,1,0,1,0,1,0$20_0|
|stepRtn20=0,-90,90,180,0,-90,90,180,0,-90,90,180,onigiri,0,30,60,90,120,150,180$20_0|
|pos20=0,1,2,3,5,6,7,8,10,11,12,13,14,15,16,17,18,19,20,21$0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19|
|div20=14,23$12,21|
|blank20=50$50|
|scale20=1$1| 
|shuffle20=3,3,3,3,0,0,0,0,4,4,4,4,1,2,2,2,2,2,2,2$20_0|
```

### 「気分転換にRPG」のエディター(Nkey版)

<details>
<summary>ここをクリックして表示</summary>

```
map=11110121212133331111

//ここから出力内容
[header]

|aleft[i]_data=[a00]
|adown[i]_data=[a01]
|aup[i]_data=[a02]
|aright[i]_data=[a03]

|oni[i]_data=[a04]

|left[i]_data=[a05]
|leftdia[i]_data=[a06]
|down[i]_data=[a07]
|space[i]_data=[a08]
|up[i]_data=[a09]
|rightdia[i]_data=[a10]
|right[i]_data=[a11]

|sleft[i]_data=[a12]
|sdown[i]_data=[a13]
|sup[i]_data=[a14]
|sright[i]_data=[a15]

|bleft[i]_data=[a16]
|bdown[i]_data=[a17]
|bup[i]_data=[a18]
|bright[i]_data=[a19]

|afrzLeft[i]_data=[f00]
|afrzDown[i]_data=[f01]
|afrzUp[i]_data=[f02]
|afrzRight[i]_data=[f03]

|foni[i]_data=[f04]

|frzLeft[i]_data=[f05]
|frzLdia[i]_data=[f06]
|frzDown[i]_data=[f07]
|frzSpace[i]_data=[f08]
|frzUp[i]_data=[f09]
|frzRdia[i]_data=[f10]
|frzRight[i]_data=[f11]

|sfrzLeft[i]_data=[f12]
|sfrzDown[i]_data=[f13]
|sfrzUp[i]_data=[f14]
|sfrzRight[i]_data=[f15]

|bfrzLeft[i]_data=[f16]
|bfrzDown[i]_data=[f17]
|bfrzUp[i]_data=[f18]
|bfrzRight[i]_data=[f19]

|speed[i]_change=[speed]
|boost[i]_data=[boost]
[footer]

[intext]
|[creator]

//ここまで出力内容
```
</details>

## 21ikey
<img src="https://user-images.githubusercontent.com/44026291/221351254-50ac979d-345f-4bc5-b15e-9c313bef2617.png" width="80%">

### 譜面データ本体
```
|minWidth21i=750|
|keyCtrl21i=112/0,113/0,114/0,115/0,116/0,117/0,118/0,119/0,120/0,121/0,122/0,123/0,65/0,83/0,68/0,70/0,32/0,74/0,75/0,76/0,187/0|
|chara21i=f1,f2,f3,f4,f5,f6,f7,f8,f9,f10,f11,f12,left,down,up,right,space,sleft,sdown,sup,sright|
|color21i=1,0,1,0,3,3,3,3,0,1,0,1,4,0,4,0,2,0,4,0,4|
|stepRtn21i=45,0,-45,-90,giko,onigiri,iyo,c,90,135,180,225,45,0,-45,-90,onigiri,90,135,180,225|
|div21i=12|
|shuffle21i=0,0,0,0,1,1,1,1,2,2,2,2,3,3,3,3,4,3,3,3,3|
```

## 動作確認バージョン
- ver30.2.3, ver29.4.2, ver28.6.6, ver24.6.15以降

## ページ作成者
- ティックル

## 関連項目
- [キーの仕様について](./keys)
- [既存キーのキーパターン上書き](./tips-0006-keypattern-update)
- [共通設定ファイル仕様](./dos_setting) &gt; [カスタムキー定義](./dos-s0010-customKeys)