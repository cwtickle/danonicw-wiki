**[English](https://github.com/cwtickle/danoniplus-docs/wiki/tips-0021-object-layer) | Japanese**

[^ Tips Indexに戻る](./tips-index)

| < [カスタムJS事始め](./tips-0020-firststep-customjs) | **オブジェクトの階層とラベル・ボタンの挿入** | [利便性の高い変数群](./tips-0022-variables) > |

# オブジェクトの階層とラベル・ボタンの挿入

- カスタムJSでラベル・ボタンを追加する場合、オブジェクトの階層構造を理解しておくとやりやすいです。
- どのオブジェクトの配下に追加させるかで、追加したラベルやボタンの見え方が変わるためです。

## プレイ画面でラベルを追加する場合の例

- プレイ画面の階層構造は下記の図の通りです。
- ここでは、「divRoot」「mainSprite」「backSprite」にそれぞれラベル1～3を追加した場合の見え方です。
- 同じラベルの追加でも、どこに挿入するかで手前に表示されるか、奥に表示されるかが変わってきます。
- ラベル1の場合は最も手前に表示され、ラベル3の場合は矢印・フリーズアローの背面に表示されます。

<img src="https://github.com/user-attachments/assets/21b6948d-ff45-425c-812f-8d60d72640e8" width="70%">

## ラベル追加の方法

- `(追加したいオブジェクト名).appendChild(新しく作成したオブジェクト)`の形式で追加します。
- 新しく作成したオブジェクトについては [createDivCss2Label](./fnc-c0002-createDivCss2Label) 関数が便利です。
- 詳細は [カスタムJS事始め#カスタムラベルを作る](./tips-0020-firststep-customjs#具体例2-カスタムラベルを作る) を参照してください。

## ラベル追加時の留意点

- 各画面の階層の概要については [オブジェクト構成・ボタン](./DOM) をご覧ください。
- どの画面にも「canvas-frame」「divRoot」オブジェクトはありますが画面を切り替えると「divRoot」配下にいるオブジェクトはすべて消えます。（divRootそのものは消えません）
- 永続的に表示したい場合は「canvas-frame」になりますが、上の図にあるように「canvas-frame」に追加したラベルは常に手前に表示されることになるため、注意が必要です。

### ラベルが画面全体を覆う場合の解消方法

- ラベルが大きい場合、下記のように状況によっては既存のボタンが押せなくなる場合があります。
- この場合は、`createDivCss2Label`関数指定時に`pointerEvents: "none"`を指定すれば解消されます。

<img src="https://github.com/user-attachments/assets/8cbfb8f8-6b61-4c66-9047-288b0838fcf8" width="90%">

```javascript
    const label = createDivCss2Label(`local_lblName`, `ラベルテスト用`, {
        x: 0, y: 350, w: g_sWidth, h: 30, siz: 14, align: C_ALIGN_LEFT, color: `#cccccc`, 
        pointerEvents: `none`,
    });
```

## ページ作成者
- ティックル

[^ Tips Indexに戻る](./tips-index)

| < [カスタムJS事始め](./tips-0020-firststep-customjs) | **オブジェクトの階層とラベル・ボタンの挿入** | [利便性の高い変数群](./tips-0022-variables) > |
