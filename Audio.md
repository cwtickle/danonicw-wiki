**[English](https://github.com/cwtickle/danoniplus-docs/wiki/Audio) | Japanese** 

| [< ローカルストレージ仕様](./LocalStorage) | **オーディオ仕様** | [速度変化・Motionオプション仕様 >](./SpeedChange) |

# オーディオ仕様 / Audio
- Dancing☆Onigiri (CW Edition)における音源再生は、可能な限りWeb Audio APIを使うことで音源の再生遅延を抑えています。  
しかし、Web Audio APIの仕様で利用できないケースがあり、  
一部でHTMLのAudio要素を使用しています。

## Web Audio APIに対応しているパターン
- :heavy_check_mark: 対応 / :x: 未対応 (HTML Audioで代用)
- ローカル/リモートどちらでも再生遅延を抑えるには、楽曲データをjs化するのが早いです。

※js, txt形式(エンコード有)は楽曲データをjs化(エンコード)したデータのことです。  
　[ダンおに曲データjs化ツール](https://github.com/suzme/danoni-base64)にて変換したデータを作成できます。

|    |mp3, ogg形式|js, txt形式(エンコード有)|
|----|----|----|
|ローカル (htmlを直接開く方法)|:x:|:heavy_check_mark:|
|ローカル ([Xamppを使用してサーバーを立てる方法](HowToLocalPlay#a-ローカルサーバーを立てる))|:heavy_check_mark:|:heavy_check_mark:|
|リモート (Web上にアップロードして公開した状態)|:heavy_check_mark:|:heavy_check_mark:|

## HTMLのAudio要素を使用した場合の制限
- HTMLのAudioはWeb Audio APIよりも機能が制限されます。  
現状、以下の事象を確認しています。

- :heavy_check_mark: 問題なし / :warning: 一部問題あり / :x: 機能使用不可

||Web Audio API|HTML Audio|
|----|----|----|
|フェードインによる<br>譜面ズレ|:heavy_check_mark:<br>ズレなし|:warning:<br>音源によりずれることがある|
|音量設定|:heavy_check_mark:<br>問題なし|:heavy_check_mark: (Windows) 問題なし<br>:x: (Mac / iPad) 常時100％|
|フェードイン音量|:heavy_check_mark:<br>問題なし|:heavy_check_mark: (Windows) 問題なし<br>:x: (Mac / iPad) フェードインしない|
|フェードアウト音量|:heavy_check_mark:<br>問題なし|:heavy_check_mark: (Windows) 問題なし<br>:x: (Mac / iPad) フェードアウトしない|
|小数のAdjustment|:heavy_check_mark:<br>有効|:x:<br>無効|

## iOSにおける開始方法の違い
- iOSの場合、ユーザ操作無しに楽曲を自動再生することができません。  
このため、iOSに限り開始直前に開始確認ボタンを追加しています。

## 関連項目
- [疑似フレーム処理仕様](AboutFrameProcessing)
- [**endFrame**](dos-h0007-endFrame) [:pencil:](dos-h0007-endFrame/_edit) プレイ終了フレーム数
- [**fadeFrame**](dos-h0008-fadeFrame) [:pencil:](dos-h0008-fadeFrame/_edit) フェードアウト開始フレーム数
- [**adjustment**](dos-h0009-adjustment) [:pencil:](dos-h0009-adjustment/_edit) 譜面位置の初期調整- 

| [< ローカルストレージ仕様](./LocalStorage) | **オーディオ仕様** | [速度変化・Motionオプション仕様 >](./SpeedChange) |
