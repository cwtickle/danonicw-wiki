**[English](https://github.com/cwtickle/danoniplus-docs/wiki/Changelog-v34) | Japanese**

[^ Back to All Version List (Security Policy)](https://github.com/cwtickle/danoniplus/security/policy)  
[^ Back to Update Info](./UpdateInfo#-v34-changelog)

[**<- v35**](./Changelog-v35) | **v34** | [**v33 ->**](./Changelog-v33)  
(**🔖 [31 Releases](https://github.com/cwtickle/danoniplus/releases?q=tag%3Av34)** )

## 🔃 Files changed (v34)

| Directory | FileName            |                                                                                            | Last Updated                                |
| --------- | ------------------- | ------------------------------------------------------------------------------------------ | ------------------------------------------- |
| /js       | danoni_main.js      | [📥](https://github.com/cwtickle/danoniplus/releases/download/v34.7.17/danoni_main.js)     | **v34.7.17**                                |
| /js/lib   | danoni_constants.js | [📥](https://github.com/cwtickle/danoniplus/releases/download/v34.7.11/danoni_constants.js) | [v34.7.11](./Changelog-v34#v34711-2024-11-04) |
| /css      | danoni_main.css     | [📥](https://github.com/cwtickle/danoniplus/releases/download/v34.5.1/danoni_main.css)     | [v34.5.1](./Changelog-v34#v3451-2023-11-05) |

<details>
<summary>Changed file list before v33</summary>

| Directory | FileName                                                                                                                                               |                                                                                              | Last Updated                                |
| --------- | ------------------------------------------------------------------------------------------------------------------------------------------------------ | -------------------------------------------------------------------------------------------- | ------------------------------------------- |
| /js/lib   | danoni_localbinary.js                                                                                                                                  | [📥](https://github.com/cwtickle/danoniplus/releases/download/v15.1.0/danoni_localbinary.js) | [v15.1.0](./Changelog-v15#v1510-2020-05-21) |
| /img      | aaShadow.svg<br>arrow.svg<br>arrowShadow.svg<br>borderline.svg<br>c.svg<br>cursor.svg<br>giko.svg<br>iyo.svg<br>monar.svg<br>morara.svg<br>onigiri.svg | [📥](https://github.com/cwtickle/danoniplus/releases/download/v15.1.0/img.zip)               | [v15.1.0](./Changelog-v15#v1510-2020-05-21) |
| /skin     | danoni_skin_default.css<br>danoni_skin_light.css<br>danoni_skin_skyblue.css                                                                            | [📥](https://github.com/cwtickle/danoniplus/releases/download/v33.0.0/skin_css.zip)          | [v33.0.0](./Changelog-v33#v3300-2023-07-29) |

</details>

⭐ New Features / 🛠️ Improvements / 🐞 Bug Fixes  
( Files changed: ⚪ danoni_main.js / 🔵 danoni_constants.js / 🔴 css files )

## v34.7.17 ([2025-02-24](https://github.com/cwtickle/danoniplus/releases/tag/v34.7.17))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v34.7.17/total)](https://github.com/cwtickle/danoniplus/archive/v34.7.17.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v34.7.17/support/v34?style=social)](https://github.com/cwtickle/danoniplus/compare/v34.7.17...support/v34#files_bucket)

- 🐞 **C)** キーパターン違いで対象のキーアシストがいない場合、キーパターン変更時に止まることがある問題を修正 ( PR [#1786](https://github.com/cwtickle/danoniplus/pull/1786) ) <- :boom: [**v15.0.0**](./Changelog-v15#v1500-2020-05-13)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v34.7.17)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v34.7.16...v34.7.17#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v34.7.17)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v34.7.17.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v34.7.17.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v34.7.17/js/danoni_main.js)
> / 🎣 [**v39.8.2**](./Changelog-v39#v3982-2025-02-24)

## v34.7.16 ([2025-02-17](https://github.com/cwtickle/danoniplus/releases/tag/v34.7.16))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v34.7.16/total)](https://github.com/cwtickle/danoniplus/archive/v34.7.16.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v34.7.16/support/v34?style=social)](https://github.com/cwtickle/danoniplus/compare/v34.7.16...support/v34#files_bucket)

- 🐞 **C)** キーパターンが別キーモードで、デフォルトの曲中ショートカットが異なる場合に曲中ショートカットキーが変わらない問題を修正 ( PR [#1773](https://github.com/cwtickle/danoniplus/pull/1773) ) <- :boom: [**v4.8.0**](./Changelog-v4#v480-2019-05-10)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v34.7.16)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v34.7.15...v34.7.16#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v34.7.16)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v34.7.16.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v34.7.16.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v34.7.16/js/danoni_main.js)
> / 🎣 [**v39.6.1**](./Changelog-v39#v3961-2025-02-17)

## v34.7.15 ([2025-02-01](https://github.com/cwtickle/danoniplus/releases/tag/v34.7.15))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v34.7.15/total)](https://github.com/cwtickle/danoniplus/archive/v34.7.15.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v34.7.15/support/v34?style=social)](https://github.com/cwtickle/danoniplus/compare/v34.7.15...support/v34#files_bucket)

- 🐞 **C)** HitPosition有効時にHidden+/Sudden+のフィルター位置がずれる問題を修正 ( PR [#1746](https://github.com/cwtickle/danoniplus/pull/1746) ) <- :boom: [**v31.0.0**](./Changelog-v31#v3100-2023-03-20)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v34.7.15)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v34.7.14...v34.7.15#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v34.7.15)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v34.7.15.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v34.7.15.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v34.7.15/js/danoni_main.js)
> / 🎣 [**v39.0.0**](./Changelog-v39#v3900-2025-02-01)

## v34.7.14 ([2025-01-28](https://github.com/cwtickle/danoniplus/releases/tag/v34.7.14))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v34.7.14/total)](https://github.com/cwtickle/danoniplus/archive/v34.7.14.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v34.7.14/support/v34?style=social)](https://github.com/cwtickle/danoniplus/compare/v34.7.14...support/v34#files_bucket)

- 🐞 **B)** フリーズアロー移動中のフリーズアロー長の計算誤りを修正 ( PR [#1744](https://github.com/cwtickle/danoniplus/pull/1744) ) <- :boom: **initial**
- 🐞 **B)** フリーズアローの末尾付近に矢印がいる場合の判定不具合を修正 ( PR [#1744](https://github.com/cwtickle/danoniplus/pull/1744) ) <- :boom: **initial**

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v34.7.14)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v34.7.13...v34.7.14#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v34.7.14)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v34.7.14.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v34.7.14.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v34.7.14/js/danoni_main.js)
> / 🎣 [**v38.3.1**](./Changelog-v38#v3831-2025-01-28)

## v34.7.13 ([2024-12-07](https://github.com/cwtickle/danoniplus/releases/tag/v34.7.13))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v34.7.13/total)](https://github.com/cwtickle/danoniplus/archive/v34.7.13.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v34.7.13/support/v34?style=social)](https://github.com/cwtickle/danoniplus/compare/v34.7.13...support/v34#files_bucket)

- 🐞 **B)** 加算するモーションフレーム数の計算方法の誤りを修正 ( PR [#1734](https://github.com/cwtickle/danoniplus/pull/1734) ) <- :boom: [**v28.4.0**](./Changelog-v28#v2840-2022-10-20)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v34.7.13)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v34.7.12...v34.7.13#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v34.7.13)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v34.7.13.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v34.7.13.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v34.7.13/js/danoni_main.js)
> / 🎣 [**v38.1.2**](./Changelog-v38#v3812-2024-12-07)

## v34.7.12 ([2024-11-27](https://github.com/cwtickle/danoniplus/releases/tag/v34.7.12))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v34.7.12/total)](https://github.com/cwtickle/danoniplus/archive/v34.7.12.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v34.7.12/support/v34?style=social)](https://github.com/cwtickle/danoniplus/compare/v34.7.12...support/v34#files_bucket)

- 🛠️ リモート接続用サンプルHTMLをnetlifyに変更 ( PR [#1728](https://github.com/cwtickle/danoniplus/pull/1728) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v34.7.12)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v34.7.11...v34.7.12#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v34.7.12)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v34.7.12.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v34.7.12.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v34.7.12/js/danoni_main.js)
> / 🎣 [**v37.8.3**](./Changelog-v37#v3783-2024-11-27)

## v34.7.11 ([2024-11-04](https://github.com/cwtickle/danoniplus/releases/tag/v34.7.11))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v34.7.11/total)](https://github.com/cwtickle/danoniplus/archive/v34.7.11.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v34.7.11/support/v34?style=social)](https://github.com/cwtickle/danoniplus/compare/v34.7.11...support/v34#files_bucket)

- 🛠️ ハッシュタグの前後に半角スペースを追加 ( PR [#1718](https://github.com/cwtickle/danoniplus/pull/1718) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v34.7.11)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v34.7.10...v34.7.11#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v34.7.11)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v34.7.11.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v34.7.11.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v34.7.11/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v34.7.11/js/lib/danoni_constants.js)
> / 🎣 [**v38.0.0**](./Changelog-v38#v3800-2024-11-04)

## v34.7.10 ([2024-09-25](https://github.com/cwtickle/danoniplus/releases/tag/v34.7.10))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v34.7.10/total)](https://github.com/cwtickle/danoniplus/archive/v34.7.10.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v34.7.10/support/v34?style=social)](https://github.com/cwtickle/danoniplus/compare/v34.7.10...support/v34#files_bucket)

- 🐞 **C)** ImgType で拡張子の指定がないときに、強制的に他の拡張子に変更する機能の問題を修正 ( PR [#1708](https://github.com/cwtickle/danoniplus/pull/1708) ) <- :boom: [**v23.1.0**](./Changelog-v23#v2310-2021-09-11)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v34.7.10)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v34.7.9...v34.7.10#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v34.7.10)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v34.7.10.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v34.7.10.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v34.7.10/js/danoni_main.js)
> / 🎣 [**v37.6.2**](./Changelog-v37#v3762-2024-09-25)

## v34.7.9 ([2024-08-14](https://github.com/cwtickle/danoniplus/releases/tag/v34.7.9))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v34.7.9/total)](https://github.com/cwtickle/danoniplus/archive/v34.7.9.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v34.7.9/support/v34?style=social)](https://github.com/cwtickle/danoniplus/compare/v34.7.9...support/v34#files_bucket)

- 🐞 **C)** 一部文字列の置換処理がおかしくなる問題を修正 ( PR [#1697](https://github.com/cwtickle/danoniplus/pull/1697) ) <- :boom: [**v27.0.0**](./Changelog-v27#v2700-2022-03-18)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v34.7.9)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v34.7.8...v34.7.9#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v34.7.9)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v34.7.9.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v34.7.9.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v34.7.9/js/danoni_main.js)
> / 🎣 [**v37.4.0**](./Changelog-v37#v3740-2024-08-14)

## v34.7.8 ([2024-06-25](https://github.com/cwtickle/danoniplus/releases/tag/v34.7.8))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v34.7.8/total)](https://github.com/cwtickle/danoniplus/archive/v34.7.8.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v34.7.8/support/v34?style=social)](https://github.com/cwtickle/danoniplus/compare/v34.7.8...support/v34#files_bucket)

- 🐞 **C)** 速度変化平均（AvgO, AvgB）の表示が想定より高く出ることがある問題を修正 ( PR [#1685](https://github.com/cwtickle/danoniplus/pull/1685) ) <- :boom: [**v32.2.0**](./Changelog-v32#v3220-2023-05-21)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v34.7.8)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v34.7.7...v34.7.8#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v34.7.8)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v34.7.8.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v34.7.8.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v34.7.8/js/danoni_main.js)
> / 🎣 [**v37.1.0**](./Changelog-v37#v3710-2024-06-25)

## v34.7.7 ([2024-06-20](https://github.com/cwtickle/danoniplus/releases/tag/v34.7.7))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v34.7.7/total)](https://github.com/cwtickle/danoniplus/archive/v34.7.7.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v34.7.7/support/v34?style=social)](https://github.com/cwtickle/danoniplus/compare/v34.7.7...support/v34#files_bucket)

- 🐞 **C)** Reverse のときに歌詞表示の自動反転が効かない問題を修正 ( PR [#1679](https://github.com/cwtickle/danoniplus/pull/1679), [#1681](https://github.com/cwtickle/danoniplus/pull/1681) ) <- :boom: [**v30.0.1**](./Changelog-v30#v3001-2023-02-11)
- 🐞 **C)** ダミーフリーズアローヒット時に、開始矢印の塗りつぶし色が消えてしまう問題を修正 ( PR [#1680](https://github.com/cwtickle/danoniplus/pull/1680) ) <- :boom: [**v31.0.0**](./Changelog-v31#v3100-2023-03-20)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v34.7.7)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v34.7.6...v34.7.7#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v34.7.7)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v34.7.7.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v34.7.7.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v34.7.7/js/danoni_main.js)
> / 🎣 [**v37.0.1**](./Changelog-v37#v3701-2024-06-20)

## v34.7.6 ([2024-06-08](https://github.com/cwtickle/danoniplus/releases/tag/v34.7.6))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v34.7.6/total)](https://github.com/cwtickle/danoniplus/archive/v34.7.6.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v34.7.6/support/v34?style=social)](https://github.com/cwtickle/danoniplus/compare/v34.7.6...support/v34#files_bucket)

- 🛠️ X のポスト用 URL の変更 ( PR [#1671](https://github.com/cwtickle/danoniplus/pull/1671) )
- 🐞 **C)** Hudden+/Sudden+で HitPosition 変更時、フィルター用のバーと実際に隠れる位置がずれる問題を修正 ( PR [#1673](https://github.com/cwtickle/danoniplus/pull/1673) ) <- :boom: [**v31.0.0**](./Changelog-v31#v3100-2023-03-20)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v34.7.6)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v34.7.5...v34.7.6#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v34.7.6)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v34.7.6.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v34.7.6.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v34.7.6/js/danoni_main.js)
> / 🎣 [**v36.6.0**](./Changelog-v36#v3660-2024-06-08)

## v34.7.5 ([2024-05-18](https://github.com/cwtickle/danoniplus/releases/tag/v34.7.5))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v34.7.5/total)](https://github.com/cwtickle/danoniplus/archive/v34.7.5.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v34.7.5/support/v34?style=social)](https://github.com/cwtickle/danoniplus/compare/v34.7.5...support/v34#files_bucket)

- 🛠️ 結果画面の旧 Twitter、Gitter のリンクを X, Discord に変更 ( PR [#1661](https://github.com/cwtickle/danoniplus/pull/1661) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v34.7.5)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v34.7.4...v34.7.5#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v34.7.5)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v34.7.5.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v34.7.5.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v34.7.5/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v34.7.5/js/lib/danoni_constants.js)
> / 🎣 [**v36.4.1**](./Changelog-v36#v3641-2024-05-18)

## v34.7.4 ([2024-04-03](https://github.com/cwtickle/danoniplus/releases/tag/v34.7.4))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v34.7.4/total)](https://github.com/cwtickle/danoniplus/archive/v34.7.4.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v34.7.4/support/v34?style=social)](https://github.com/cwtickle/danoniplus/compare/v34.7.4...support/v34#files_bucket)

- 🐞 **C)** キーコンフィグ画面でオブジェクトが画面外になるとき、キーを押しても画面が見える位置に移動しない問題を修正 ( PR [#1634](https://github.com/cwtickle/danoniplus/pull/1634) ) <- :boom: [**v30.0.1**](./Changelog-v30#v3001-2023-02-11)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v34.7.4)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v34.7.3...v34.7.4#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v34.7.4)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v34.7.4.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v34.7.4.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v34.7.4/js/danoni_main.js)
> / 🎣 [**v35.5.0**](./Changelog-v35#v3550-2024-04-03)

## v34.7.3 ([2024-03-17](https://github.com/cwtickle/danoniplus/releases/tag/v34.7.3))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v34.7.3/total)](https://github.com/cwtickle/danoniplus/archive/v34.7.3.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v34.7.3/support/v34?style=social)](https://github.com/cwtickle/danoniplus/compare/v34.7.3...support/v34#files_bucket)

- 🐞 **C)** フリーズアローの終点と始点が近いとき、キーを押しても次のフリーズアローが反応しないことがある問題を修正 ( PR [#1627](https://github.com/cwtickle/danoniplus/pull/1627), [#1628](https://github.com/cwtickle/danoniplus/pull/1628) ) <- :boom: [**v29.0.1**](./Changelog-v29#v2901-2022-11-05)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v34.7.3)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v34.7.2...v34.7.3#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v34.7.3)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v34.7.3.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v34.7.3.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v34.7.3/js/danoni_main.js)
> / 🎣 [**v35.4.3**](./Changelog-v35#v3543-2024-03-17)

## v34.7.2 ([2024-02-29](https://github.com/cwtickle/danoniplus/releases/tag/v34.7.2))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v34.7.2/total)](https://github.com/cwtickle/danoniplus/archive/v34.7.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v34.7.2/support/v34?style=social)](https://github.com/cwtickle/danoniplus/compare/v34.7.2...support/v34#files_bucket)

- 🐞 **C)** 環境によりタイトルの背景矢印が表示されない問題を修正 ( PR [#1625](https://github.com/cwtickle/danoniplus/pull/1625) ) <- :boom: [**v34.7.0**](./Changelog-v34#v3470-2024-01-12)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v34.7.2)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v34.7.1...v34.7.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v34.7.2)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v34.7.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v34.7.2.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v34.7.2/js/danoni_main.js)
> / 🎣 [**v35.4.2**](./Changelog-v35#v3542-2024-02-29)

---

## v34.7.1 ([2024-01-28](https://github.com/cwtickle/danoniplus/releases/tag/v34.7.1))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v34.7.1/total)](https://github.com/cwtickle/danoniplus/archive/v34.7.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v34.7.1/support/v34?style=social)](https://github.com/cwtickle/danoniplus/compare/v34.7.1...support/v34#files_bucket)

- 🐞 **C)** 背景矢印でカラーコードを 0x 始まりにしたときに透明度が適用されない問題を修正 ( PR [#1609](https://github.com/cwtickle/danoniplus/pull/1609) ) <- :boom: [**v33.5.0**](./Changelog-v33#v3350-2023-09-02)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v34.7.1)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v34.7.0...v34.7.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v34.7.1)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v34.7.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v34.7.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v34.7.1/js/danoni_main.js)

## v34.7.0 ([2024-01-12](https://github.com/cwtickle/danoniplus/releases/tag/v34.7.0))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v34.7.0/total)](https://github.com/cwtickle/danoniplus/archive/v34.7.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v34.7.0/support/v34?style=social)](https://github.com/cwtickle/danoniplus/compare/v34.7.0...support/v34#files_bucket)

- ⭐ デフォルトのタイトル矢印の回転・種別を自由に決められるよう変更 ( PR [#1605](https://github.com/cwtickle/danoniplus/pull/1605), [#1606](https://github.com/cwtickle/danoniplus/pull/1606) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v34.7.0)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v34.6.1...v34.7.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v34.7.0)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v34.7.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v34.7.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v34.7.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v34.7.0/js/lib/danoni_constants.js)

## v34.6.1 ([2024-01-08](https://github.com/cwtickle/danoniplus/releases/tag/v34.6.1))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v34.6.1/total)](https://github.com/cwtickle/danoniplus/archive/v34.6.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v34.6.1/support/v34?style=social)](https://github.com/cwtickle/danoniplus/compare/v34.6.1...support/v34#files_bucket)

- 🛠️ 譜面リスト、キー別フィルターの画面位置調整 ( PR [#1602](https://github.com/cwtickle/danoniplus/pull/1602), [#1603](https://github.com/cwtickle/danoniplus/pull/1603) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v34.6.1)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v34.6.0...v34.6.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v34.6.1)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v34.6.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v34.6.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v34.6.1/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v34.6.1/js/lib/danoni_constants.js)

## v34.6.0 ([2024-01-07](https://github.com/cwtickle/danoniplus/releases/tag/v34.6.0))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v34.6.0/total)](https://github.com/cwtickle/danoniplus/archive/v34.6.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v34.6.0/support/v34?style=social)](https://github.com/cwtickle/danoniplus/compare/v34.6.0...support/v34#files_bucket)

- ⭐ 譜面番号固定時に他の譜面ファイル番号や譜面番号を参照できる機能を追加 ( PR [#1600](https://github.com/cwtickle/danoniplus/pull/1600) )
- ⭐ デフォルトのリザルトフォーマットの出力機能を追加 ( PR [#1598](https://github.com/cwtickle/danoniplus/pull/1598) )
- 🛠️ 譜面リストに現在選択中譜面番号と総譜面数の表示を追加 ( PR [#1597](https://github.com/cwtickle/danoniplus/pull/1597), [#1599](https://github.com/cwtickle/danoniplus/pull/1599) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v34.6.0)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v34.5.2...v34.6.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v34.6.0)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v34.6.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v34.6.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v34.6.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v34.6.0/js/lib/danoni_constants.js)

## v34.5.2 ([2023-12-02](https://github.com/cwtickle/danoniplus/releases/tag/v34.5.2))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v34.5.2/total)](https://github.com/cwtickle/danoniplus/archive/v34.5.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v34.5.2/support/v34?style=social)](https://github.com/cwtickle/danoniplus/compare/v34.5.2...support/v34#files_bucket)

- 🐞 **B)** startFrame に空白を指定したときプレイ開始できない問題を修正 ( PR [#1595](https://github.com/cwtickle/danoniplus/pull/1595) ) <- :boom: [**v34.3.0**](./Changelog-v34#v3430-2023-10-13)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v34.5.2)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v34.5.1...v34.5.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v34.5.2)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v34.5.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v34.5.2.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v34.5.2/js/danoni_main.js)

## v34.5.1 ([2023-11-05](https://github.com/cwtickle/danoniplus/releases/tag/v34.5.1))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v34.5.1/total)](https://github.com/cwtickle/danoniplus/archive/v34.5.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v34.5.1/support/v34?style=social)](https://github.com/cwtickle/danoniplus/compare/v34.5.1...support/v34#files_bucket)

- ⭐ 譜面明細子画面のレーン別矢印数で最大・最小がわかるよう表示を追加 ( PR [#1590](https://github.com/cwtickle/danoniplus/pull/1590) )
- 🛠️ 譜面明細子画面の 3 つ押し位置の表示仕様変更 ( PR [#1590](https://github.com/cwtickle/danoniplus/pull/1590) )
- 🛠️ 譜面明細子画面について横幅に合わせて右側を自動拡張するよう変更 ( PR [#1590](https://github.com/cwtickle/danoniplus/pull/1590) )
- 🛠️ 右シフトキー対応のコード整理 ( PR [#1593](https://github.com/cwtickle/danoniplus/pull/1593) )
- 🐞 **C)** keyGroupOrder の一部譜面の設定が空の場合、設定の適用先が本来とずれる問題を修正 ( PR [#1591](https://github.com/cwtickle/danoniplus/pull/1591) ) <- :boom: [**v33.5.0**](./Changelog-v33#v3350-2023-09-02)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v34.5.1)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v34.4.2...v34.5.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v34.5.1)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v34.5.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v34.5.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v34.5.1/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v34.5.1/js/lib/danoni_constants.js)[🔴](https://github.com/cwtickle/danoniplus/blob/v34.5.1/css/danoni_main.css)

<img src="https://github.com/cwtickle/danoniplus/assets/44026291/3fbb8ec1-9be1-4f0b-8087-fa3740ee2131" width="60%">

## v34.4.2 ([2023-11-03](https://github.com/cwtickle/danoniplus/releases/tag/v34.4.2))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v34.4.2/total)](https://github.com/cwtickle/danoniplus/archive/v34.4.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v34.4.2/support/v34?style=social)](https://github.com/cwtickle/danoniplus/compare/v34.4.2...support/v34#files_bucket)

- 🛠️ コードの整理 ( PR [#1587](https://github.com/cwtickle/danoniplus/pull/1587), [#1588](https://github.com/cwtickle/danoniplus/pull/1588) )
- 🐞 **C)** プレイ画面以外の画面で、右シフトキーが押したままになってしまう問題を修正 ( PR [#1588](https://github.com/cwtickle/danoniplus/pull/1588) ) <- :boom: [**v34.4.0**](./Changelog-v34#v3440-2023-10-29)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v34.4.2)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v34.4.1...v34.4.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v34.4.2)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v34.4.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v34.4.2.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v34.4.2/js/danoni_main.js)

## v34.4.1 ([2023-10-31](https://github.com/cwtickle/danoniplus/releases/tag/v34.4.1))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v34.4.1/total)](https://github.com/cwtickle/danoniplus/archive/v34.4.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v34.4.1/support/v34?style=social)](https://github.com/cwtickle/danoniplus/compare/v34.4.1...support/v34#files_bucket)

- 🐞 **C)** R-Shift キーと Unknown キーが分離できていない問題を修正 ( PR [#1585](https://github.com/cwtickle/danoniplus/pull/1585) ) <- :boom: [**v34.4.0**](./Changelog-v34#v3440-2023-10-29)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v34.4.1)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v34.4.0...v34.4.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v34.4.1)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v34.4.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v34.4.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v34.4.1/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v34.4.1/js/lib/danoni_constants.js)

## v34.4.0 ([2023-10-29](https://github.com/cwtickle/danoniplus/releases/tag/v34.4.0))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v34.4.0/total)](https://github.com/cwtickle/danoniplus/archive/v34.4.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v34.4.0/support/v34?style=social)](https://github.com/cwtickle/danoniplus/compare/v34.4.0...support/v34#files_bucket)

- 🛠️ 環境により右シフトキーが動作しない問題を改善 ( PR [#1581](https://github.com/cwtickle/danoniplus/pull/1581), [#1583](https://github.com/cwtickle/danoniplus/pull/1583) )
- 🐞 **C)** 色変化が一部のレーンに適用されない問題を修正 ( PR [#1582](https://github.com/cwtickle/danoniplus/pull/1582) ) <- :boom: [**v33.5.0**](./Changelog-v33#v3350-2023-09-02)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v34.4.0)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v34.3.2...v34.4.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v34.4.0)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v34.4.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v34.4.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v34.4.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v34.4.0/js/lib/danoni_constants.js)<br>❤️ ショウタ

## v34.3.2 ([2023-10-21](https://github.com/cwtickle/danoniplus/releases/tag/v34.3.2))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v34.3.2/total)](https://github.com/cwtickle/danoniplus/archive/v34.3.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v34.3.2/support/v34?style=social)](https://github.com/cwtickle/danoniplus/compare/v34.3.2...support/v34#files_bucket)

- 🐞 **B)** HitPosition 設定が機能していない問題を修正 ( PR [#1579](https://github.com/cwtickle/danoniplus/pull/1579) ) <- :boom: [**v31.0.0**](./Changelog-v31#v3100-2023-03-20)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v34.3.2)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v34.3.1...v34.3.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v34.3.2)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v34.3.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v34.3.2.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v34.3.2/js/danoni_main.js)<br>❤️ ★ ぞろり ★

## v34.3.1 ([2023-10-15](https://github.com/cwtickle/danoniplus/releases/tag/v34.3.1))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v34.3.1/total)](https://github.com/cwtickle/danoniplus/archive/v34.3.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v34.3.1/support/v34?style=social)](https://github.com/cwtickle/danoniplus/compare/v34.3.1...support/v34#files_bucket)

- 🐞 **B)** startFrame 未指定時に起動に失敗する問題を修正 ( PR [#1577](https://github.com/cwtickle/danoniplus/pull/1577) ) <- :boom: [**v34.3.0**](./Changelog-v34#v3430-2023-10-13)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v34.3.1)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v34.3.0...v34.3.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v34.3.1)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v34.3.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v34.3.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v34.3.1/js/danoni_main.js)

## v34.3.0 ([2023-10-13](https://github.com/cwtickle/danoniplus/releases/tag/v34.3.0))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v34.3.0/total)](https://github.com/cwtickle/danoniplus/archive/v34.3.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v34.3.0/support/v34?style=social)](https://github.com/cwtickle/danoniplus/compare/v34.3.0...support/v34#files_bucket)

- 🛠️ 譜面明細画面のショートカット表示位置を変更 ( PR [#1571](https://github.com/cwtickle/danoniplus/pull/1571) )
- 🛠️ コードの整理 ( PR [#1572](https://github.com/cwtickle/danoniplus/pull/1572), [#1573](https://github.com/cwtickle/danoniplus/pull/1573) )
- 🐞 **B)** difData で指定したノルマが反映されない問題を修正 ( PR [#1575](https://github.com/cwtickle/danoniplus/pull/1575), Issue [#1574](https://github.com/cwtickle/danoniplus/pull/1574) ) <- :boom: [**v32.6.0**](./Changelog-v32#v3260-2023-06-30)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v34.3.0)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v34.2.0...v34.3.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v34.3.0)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v34.3.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v34.3.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v34.3.0/js/danoni_main.js)<br>❤️ ぷろろーぐ (@prlg25)

## v34.2.0 ([2023-10-01](https://github.com/cwtickle/danoniplus/releases/tag/v34.2.0))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v34.2.0/total)](https://github.com/cwtickle/danoniplus/archive/v34.2.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v34.2.0/support/v34?style=social)](https://github.com/cwtickle/danoniplus/compare/v34.2.0...support/v34#files_bucket)

- ⭐ US キーボード向けのキーコンフィグ表記に対応 ( PR [#1569](https://github.com/cwtickle/danoniplus/pull/1569) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v34.2.0)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v34.1.0...v34.2.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v34.2.0)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v34.2.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v34.2.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v34.2.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v34.2.0/js/lib/danoni_constants.js)

## v34.1.0 ([2023-09-27](https://github.com/cwtickle/danoniplus/releases/tag/v34.1.0))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v34.1.0/total)](https://github.com/cwtickle/danoniplus/archive/v34.1.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v34.1.0/support/v34?style=social)](https://github.com/cwtickle/danoniplus/compare/v34.1.0...support/v34#files_bucket)

- ⭐ 歌詞表示及びタイトル曲名・アーティスト名表示で、条件付きでカンマが使えるよう変更 ( PR [#1566](https://github.com/cwtickle/danoniplus/pull/1566), [#1567](https://github.com/cwtickle/danoniplus/pull/1567) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v34.1.0)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v34.0.0...v34.1.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v34.1.0)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v34.1.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v34.1.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v34.1.0/js/danoni_main.js)

## v34.0.0 ([2023-09-24](https://github.com/cwtickle/danoniplus/releases/tag/v34.0.0))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v34.0.0/total)](https://github.com/cwtickle/danoniplus/archive/v34.0.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v34.0.0/support/v34?style=social)](https://github.com/cwtickle/danoniplus/compare/v34.0.0...support/v34#files_bucket)

- ⭐ 歌詞表示、背景・マスク・スタイルデータについてキー名・キーパターン毎に設定する機能を追加 ( PR [#1564](https://github.com/cwtickle/danoniplus/pull/1564) )
- ⭐ 譜面エフェクトデータについて、既存の他データが存在していればその設定を引用する機能を追加 ( PR [#1564](https://github.com/cwtickle/danoniplus/pull/1564) )
- 🛠️ コードの整理 ( PR [#1562](https://github.com/cwtickle/danoniplus/pull/1562) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v34.0.0)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v33.7.0...v34.0.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v34.0.0)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v34.0.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v34.0.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v34.0.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v34.0.0/js/lib/danoni_constants.js)

[**<- v35**](./Changelog-v35) | **v34** | [**v33 ->**](./Changelog-v33)
