**[English](https://github.com/cwtickle/danoniplus-docs/wiki/Changelog-v30) | Japanese** 

[^ Back to All Version List (Security Policy)](https://github.com/cwtickle/danoniplus/security/policy)  
[^ Back to Update Info](./UpdateInfo#-v30-changelog)

[**<- v31**](Changelog-v31) | **v30** | [**v29 ->**](Changelog-v29)  
(**🔖 [15 Releases](https://github.com/cwtickle/danoniplus/releases?q=tag%3Av30)** )

## 🔃 Files changed (v30)

|Directory|FileName||Last Updated|
|----|----|----|----|
|/js|danoni_main.js|[📥](https://github.com/cwtickle/danoniplus/releases/download/v30.6.3/danoni_main.js)|**v30.6.3**|
|/js/lib|danoni_constants.js|[📥](https://github.com/cwtickle/danoniplus/releases/download/v30.5.0/danoni_constants.js)|[v30.5.0](./Changelog-v30#v3050-2023-03-12)|

<details>
<summary>Changed file list before v29</summary>

|Directory|FileName||Last Updated|
|----|----|----|----|
|/js/lib|danoni_localbinary.js|[📥](https://github.com/cwtickle/danoniplus/releases/download/v15.1.0/danoni_localbinary.js)|[v15.1.0](./Changelog-v15#v1510-2020-05-21)|
|/css|danoni_main.css|[📥](https://github.com/cwtickle/danoniplus/releases/download/v28.3.1/danoni_main.css)|[v28.3.1](Changelog-v28#v2831-2022-10-16)|
|/img|aaShadow.svg<br>arrow.svg<br>arrowShadow.svg<br>borderline.svg<br>c.svg<br>cursor.svg<br>giko.svg<br>iyo.svg<br>monar.svg<br>morara.svg<br>onigiri.svg|[📥](https://github.com/cwtickle/danoniplus/releases/download/v15.1.0/img.zip)|[v15.1.0](./Changelog-v15#v1510-2020-05-21)|
|/skin|danoni_skin_default.css<br>danoni_skin_light.css<br>danoni_skin_skyblue.css|[📥](https://github.com/cwtickle/danoniplus/releases/download/v21.4.2/danoni_skin_default.css)<br>[📥](https://github.com/cwtickle/danoniplus/releases/download/v21.4.2/danoni_skin_light.css)<br>[📥](https://github.com/cwtickle/danoniplus/releases/download/v21.4.2/danoni_skin_skyblue.css)|[v21.4.2](Changelog-v21#v2142-2021-04-07)|

</details>

⭐ New Features / 🛠️ Improvements / 🐞 Bug Fixes  
( Files changed: ⚪ danoni_main.js / 🔵 danoni_constants.js / 🔴 css files )

## [Warning] 更新終了後の不具合情報
- [更新を終了したバージョンの不具合情報#v30](DeprecatedVersionBugs#v30) を参照

## v30.6.3 ([2023-05-31](https://github.com/cwtickle/danoniplus/releases/tag/v30.6.3))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v30.6.3/total)](https://github.com/cwtickle/danoniplus/archive/v30.6.3.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v30.6.3/support/v30?style=social)](https://github.com/cwtickle/danoniplus/compare/v30.6.3...support/v30#files_bucket)
- 🐞 **C)** customCreditWidthが使用できない問題を修正 ( PR [#1496](https://github.com/cwtickle/danoniplus/pull/1496) ) <- :boom: [**v23.4.0**](./Changelog-v23#v2340-2021-10-02)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v30.6.3)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v30.6.2...v30.6.3#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v30.6.3)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v30.6.3.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v30.6.3.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v30.6.3/js/danoni_main.js)
/ 🎣 [**v32.3.0**](./Changelog-v32#v3230-2023-06-02)

## v30.6.2 ([2023-04-26](https://github.com/cwtickle/danoniplus/releases/tag/v30.6.2))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v30.6.2/total)](https://github.com/cwtickle/danoniplus/archive/v30.6.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v30.6.2/support/v30?style=social)](https://github.com/cwtickle/danoniplus/compare/v30.6.2...support/v30#files_bucket)
- 🛠️ キー数名称（key）を変更した場合に、その名前が譜面リストのキー数フィルタ名にも反映されるよう変更 ( PR [#1468](https://github.com/cwtickle/danoniplus/pull/1468) )
- 🐞 **C)** 譜面リスト選択時にキーコンフィグ画面で止まることがある問題を修正 ( PR [#1470](https://github.com/cwtickle/danoniplus/pull/1470) ) <- :boom: [**v24.5.1**](./Changelog-v24#v2451-2021-12-13)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v30.6.2)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v30.6.1...v30.6.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v30.6.2)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v30.6.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v30.6.2.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v30.6.2/js/danoni_main.js)
/ 🎣 [**v31.6.0**](./Changelog-v31#v3160-2023-04-26)

## v30.6.1 ([2023-03-22](https://github.com/cwtickle/danoniplus/releases/tag/v30.6.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v30.6.1/total)](https://github.com/cwtickle/danoniplus/archive/v30.6.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v30.6.1/support/v30?style=social)](https://github.com/cwtickle/danoniplus/compare/v30.6.1...support/v30#files_bucket)
- 🐞 **C)** 譜面リストで、上下キーを使って選択した譜面がキー数違いのときにキーコンフィグ画面へ移動すると画面が止まる問題を修正 ( PR [#1446](https://github.com/cwtickle/danoniplus/pull/1446) ) <- :boom: [**v24.5.1**](./Changelog-v24#v2451-2021-12-13)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v30.6.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v30.6.0...v30.6.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v30.6.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v30.6.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v30.6.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v30.6.1/js/danoni_main.js)
/ 🎣 [**v31.0.1**](./Changelog-v31#v3101-2023-03-22)

---

## v30.6.0 ([2023-03-18](https://github.com/cwtickle/danoniplus/releases/tag/v30.6.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v30.6.0/total)](https://github.com/cwtickle/danoniplus/archive/v30.6.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v30.6.0/support/v30?style=social)](https://github.com/cwtickle/danoniplus/compare/v30.6.0...support/v30#files_bucket)
- ⭐ ユーザ定義とは別のカスタムキー定義ができるよう対応 ( PR [#1433](https://github.com/cwtickle/danoniplus/pull/1433) )
- 🛠️ キーコンフィグ画面でキー割り当て先をクリックしたときの挙動を変更 ( PR [#1434](https://github.com/cwtickle/danoniplus/pull/1434) )
- 🛠️ キーコンフィグ画面で割り当てキーを直接クリックしたときのキー連続チェックを取り止め ( PR [#1435](https://github.com/cwtickle/danoniplus/pull/1435) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v30.6.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v30.5.0...v30.6.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v30.6.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v30.6.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v30.6.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v30.6.0/js/danoni_main.js)

## v30.5.0 ([2023-03-12](https://github.com/cwtickle/danoniplus/releases/tag/v30.5.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v30.5.0/total)](https://github.com/cwtickle/danoniplus/archive/v30.5.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v30.5.0/support/v30?style=social)](https://github.com/cwtickle/danoniplus/compare/v30.5.0...support/v30#files_bucket)
- ⭐ 既存キーへのキーパターン追記時に相対パターン番号を略記で指定できるよう変更 ( PR [#1428](https://github.com/cwtickle/danoniplus/pull/1428) )
- ⭐ カスタムキーのスクロール拡張、アシスト設定に対して部分参照に対応 ( PR [#1428](https://github.com/cwtickle/danoniplus/pull/1428) )
- ⭐ キーコンフィグ画面においてキーパターンのスキップボタンを追加 ( PR [#1431](https://github.com/cwtickle/danoniplus/pull/1431) )
- 🛠️ 1つのキーにつき常時2キー以上割り当てできるよう処理を自動化 ( PR [#1429](https://github.com/cwtickle/danoniplus/pull/1429) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v30.5.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v30.4.0...v30.5.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v30.5.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v30.5.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v30.5.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v30.5.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v30.5.0/js/lib/danoni_constants.js)

## v30.4.0 ([2023-03-10](https://github.com/cwtickle/danoniplus/releases/tag/v30.4.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v30.4.0/total)](https://github.com/cwtickle/danoniplus/archive/v30.4.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v30.4.0/support/v30?style=social)](https://github.com/cwtickle/danoniplus/compare/v30.4.0...support/v30#files_bucket)
- ⭐ カスタムキーのカラー、シャッフルグループについて組み合わせ参照に対応 ( PR [#1425](https://github.com/cwtickle/danoniplus/pull/1425) )
- ⭐ カスタムキーのcolorXの省略に対応 ( PR [#1425](https://github.com/cwtickle/danoniplus/pull/1425) )
- ⭐ 12key, 23keyについてスクロール拡張設定を追加 ( PR [#1424](https://github.com/cwtickle/danoniplus/pull/1424), [#1426](https://github.com/cwtickle/danoniplus/pull/1426) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v30.4.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v30.3.1...v30.4.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v30.4.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v30.4.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v30.4.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v30.4.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v30.4.0/js/lib/danoni_constants.js)

## v30.3.1 ([2023-03-04](https://github.com/cwtickle/danoniplus/releases/tag/v30.3.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v30.3.1/total)](https://github.com/cwtickle/danoniplus/archive/v30.3.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v30.3.1/support/v30?style=social)](https://github.com/cwtickle/danoniplus/compare/v30.3.1...support/v30#files_bucket)
- 🛠️ 基本キー設定に対して一部記述の省略に対応 ( PR [#1420](https://github.com/cwtickle/danoniplus/pull/1420) )
- 🐞 **B)** 14ikeyのステップゾーンが想定位置になっていない問題を修正 ( PR [#1421](https://github.com/cwtickle/danoniplus/pull/1421) ) <- :boom: [**v30.2.2**](./Changelog-v30#v3022-2023-02-20)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v30.3.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v30.3.0...v30.3.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v30.3.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v30.3.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v30.3.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v30.3.1/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v30.3.1/js/lib/danoni_constants.js)

## v30.3.0 ([2023-03-01](https://github.com/cwtickle/danoniplus/releases/tag/v30.3.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v30.3.0/total)](https://github.com/cwtickle/danoniplus/archive/v30.3.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v30.3.0/support/v30?style=social)](https://github.com/cwtickle/danoniplus/compare/v30.3.0...support/v30#files_bucket)
- ⭐ カスタムキー定義においてdivX, shuffleXの部分省略指定に対応 ( PR [#1416](https://github.com/cwtickle/danoniplus/pull/1416), [#1417](https://github.com/cwtickle/danoniplus/pull/1417) )
- 🛠️ 基本キーの別キーモード等に対して一部略記できるようコードを変更 ( PR [#1418](https://github.com/cwtickle/danoniplus/pull/1418) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v30.3.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v30.2.3...v30.3.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v30.3.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v30.3.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v30.3.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v30.3.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v30.3.0/js/lib/danoni_constants.js)


## v30.2.3 ([2023-02-26](https://github.com/cwtickle/danoniplus/releases/tag/v30.2.3))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v30.2.3/total)](https://github.com/cwtickle/danoniplus/archive/v30.2.3.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v30.2.3/support/v30?style=social)](https://github.com/cwtickle/danoniplus/compare/v30.2.3...support/v30#files_bucket)
- 🐞 **C)** カスタムキーのdivMax値がposXの最大値を取得していないことがある問題を修正 ( PR [#1413](https://github.com/cwtickle/danoniplus/pull/1413) ) <- :boom: [**v30.2.2**](./Changelog-v30#v3022-2023-02-20)
- 🐞 **C)** カスタムキーのdivXが未定義のとき、ステップゾーンが並ばない問題を修正 ( PR [#1414](https://github.com/cwtickle/danoniplus/pull/1414) ) <- :boom: **initial**
- 🐞 **C)** カスタムキーのshuffleXが未定義のとき、エラーで止まる問題を修正 ( PR [#1414](https://github.com/cwtickle/danoniplus/pull/1414) ) <- :boom: **initial**

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v30.2.3)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v30.2.2...v30.2.3#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v30.2.3)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v30.2.3.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v30.2.3.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v30.2.3/js/danoni_main.js)

## v30.2.2 ([2023-02-20](https://github.com/cwtickle/danoniplus/releases/tag/v30.2.2))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v30.2.2/total)](https://github.com/cwtickle/danoniplus/archive/v30.2.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v30.2.2/support/v30?style=social)](https://github.com/cwtickle/danoniplus/compare/v30.2.2...support/v30#files_bucket)
- 🛠️ カスタムキー定義の**divX_Y**の2要素目（右下基準位置）が未定義のとき、**posX_Y**の最大値を基準にするよう変更 ( PR [#1410](https://github.com/cwtickle/danoniplus/pull/1410) )
- 🛠️ キー数変化データ（keych_data）の指定有無によらず、初期値が挿入される仕様に変更 ( PR [#1411](https://github.com/cwtickle/danoniplus/pull/1411) )
- 🐞 **C)** キーグループが未指定のとき、キー数変化データ（keych_data）が使えない問題を修正 ( PR [#1411](https://github.com/cwtickle/danoniplus/pull/1411) ) <- :boom: [**v30.0.1**](./Changelog-v30#v3001-2023-02-11)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v30.2.2)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v30.2.1...v30.2.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v30.2.2)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v30.2.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v30.2.2.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v30.2.2/js/danoni_main.js)

## v30.2.1 ([2023-02-18](https://github.com/cwtickle/danoniplus/releases/tag/v30.2.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v30.2.1/total)](https://github.com/cwtickle/danoniplus/archive/v30.2.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v30.2.1/support/v30?style=social)](https://github.com/cwtickle/danoniplus/compare/v30.2.1...support/v30#files_bucket)
- 🐞 **C)** フェードイン、スクロール反転機能を同時に利用した場合にスクロール初期位置が正しく反映されない問題を修正 ( PR [#1408](https://github.com/cwtickle/danoniplus/pull/1408) ) <- :boom: [**v30.2.0**](./Changelog-v30#v3020-2023-02-17)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v30.2.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v30.2.0...v30.2.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v30.2.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v30.2.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v30.2.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v30.2.1/js/danoni_main.js)

## v30.2.0 ([2023-02-17](https://github.com/cwtickle/danoniplus/releases/tag/v30.2.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v30.2.0/total)](https://github.com/cwtickle/danoniplus/archive/v30.2.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v30.2.0/support/v30?style=social)](https://github.com/cwtickle/danoniplus/compare/v30.2.0...support/v30#files_bucket)
- ⭐ 矢印・フリーズアローをスクロール反転する機能を実装 ( PR [#1406](https://github.com/cwtickle/danoniplus/pull/1406) )
- ⭐ スクロール個別のword/back/maskデータを追加 ( PR [#1405](https://github.com/cwtickle/danoniplus/pull/1405) )
- 🛠️ 別キーモードのword/back/maskデータの優先順を変更 ( PR [#1405](https://github.com/cwtickle/danoniplus/pull/1405) )
- 🛠️ キー数変化（keych_data）について改行区切りに対応 ( PR [#1406](https://github.com/cwtickle/danoniplus/pull/1406) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v30.2.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v30.1.2...v30.2.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v30.2.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v30.2.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v30.2.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v30.2.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v30.2.0/js/lib/danoni_constants.js)

## v30.1.2 ([2023-02-15](https://github.com/cwtickle/danoniplus/releases/tag/v30.1.2))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v30.1.2/total)](https://github.com/cwtickle/danoniplus/archive/v30.1.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v30.1.2/support/v30?style=social)](https://github.com/cwtickle/danoniplus/compare/v30.1.2...support/v30#files_bucket)
- 🐞 **C)** ScrollがDefault以外のときに別キーモード用のword/back/maskデータが適用されない場合がある問題を修正 ( PR [#1403](https://github.com/cwtickle/danoniplus/pull/1403) ) <- :boom: [**v30.0.1**](./Changelog-v30#v3001-2023-02-11)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v30.1.2)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v30.1.1...v30.1.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v30.1.2)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v30.1.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v30.1.2.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v30.1.2/js/danoni_main.js)

## v30.1.1 ([2023-02-14](https://github.com/cwtickle/danoniplus/releases/tag/v30.1.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v30.1.1/total)](https://github.com/cwtickle/danoniplus/archive/v30.1.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v30.1.1/support/v30?style=social)](https://github.com/cwtickle/danoniplus/compare/v30.1.1...support/v30#files_bucket)
- ⭐ 背景・マスクモーションに色付きオブジェクトが使えるよう変更 ( PR [#1399](https://github.com/cwtickle/danoniplus/pull/1399), Issue [#900](https://github.com/cwtickle/danoniplus/pull/900) )
- ⭐ 背景・マスクモーションの座標部分に埋め込み変数が使えるよう変更 ( PR [#1401](https://github.com/cwtickle/danoniplus/pull/1401) )
- ⭐ 背景・マスクモーションの項目に「animation-fill-mode」を追加 ( PR [#1399](https://github.com/cwtickle/danoniplus/pull/1399) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v30.1.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v30.0.1...v30.1.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v30.1.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v30.1.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v30.1.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v30.1.1/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v30.1.1/js/lib/danoni_constants.js)

## v30.0.1 ([2023-02-11](https://github.com/cwtickle/danoniplus/releases/tag/v30.0.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v30.0.1/total)](https://github.com/cwtickle/danoniplus/archive/v30.0.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v30.0.1/support/v30?style=social)](https://github.com/cwtickle/danoniplus/compare/v30.0.1...support/v30#files_bucket)
- ⭐ カスタムキーの中に部分キーを定義する機能を追加 ( PR [#1390](https://github.com/cwtickle/danoniplus/pull/1390), [#1391](https://github.com/cwtickle/danoniplus/pull/1391), [#1394](https://github.com/cwtickle/danoniplus/pull/1394), [#1396](https://github.com/cwtickle/danoniplus/pull/1396) )
- ⭐ Reverse設定無効時、R-FlatをFlatと見做す設定を追加 ( PR [#1392](https://github.com/cwtickle/danoniplus/pull/1392) )
- ⭐ 別キーモード用の歌詞表示・背景/マスクモーションを実装 ( PR [#1393](https://github.com/cwtickle/danoniplus/pull/1393) )
- 🛠️ div要素のスタイル変更処理の記述を整理 ( PR [#1397](https://github.com/cwtickle/danoniplus/pull/1397) )

<img src="https://user-images.githubusercontent.com/44026291/217821439-4dc8a7c1-459e-4fd9-9593-d139c183b994.png" width="50%"><img src="https://user-images.githubusercontent.com/44026291/217821591-9ea4b9ca-9dff-4c02-a52d-b77469fb49b7.png" width="50%">

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v30.0.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v29.4.1...v30.0.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v30.0.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v30.0.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v30.0.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v30.0.1/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v30.0.1/js/lib/danoni_constants.js)

[**<- v31**](Changelog-v31) | **v30** | [**v29 ->**](Changelog-v29)  
