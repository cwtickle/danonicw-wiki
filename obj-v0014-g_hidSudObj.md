[← オブジェクトリファレンスに戻る](ObjectReferenceIndex)  

## g_hidSudObj / g_hidSudFunc

### 概要
- Appearanceフィルタに関する各種情報を管理するオブジェクト。   
 
```javascript
const g_hidSudFunc = {
    filterPos: _filterPos => `${_filterPos}${g_lblNameObj.percent}`,
    range: _ => `${Math.round(g_posObj.arrowHeight - g_posObj.stepY)}px`,
    hidden: _filterPos => `${Math.min(Math.round(g_posObj.arrowHeight * (100 - _filterPos) / 100), g_posObj.arrowHeight - g_posObj.stepY)}`,
    sudden: _filterPos => `${Math.max(Math.round(g_posObj.arrowHeight * (100 - _filterPos) / 100) - g_posObj.stepY, 0)}`,
};

const g_hidSudObj = {
    filterPos: 10,

    'Visible': 1,
    'Hidden': 0,
    'Hidden+': 0,
    'Sudden': 1,
    'Sudden+': 1,
    'Hid&Sud+': 1,

    filterPosDefault: {
        'Visible': 0,
        'Hidden': 50,
        'Sudden': 40,
    },
    pgDown: {
        'Hidden+': { OFF: `PageDown`, ON: `PageUp`, },
        'Sudden+': { OFF: `PageUp`, ON: `PageDown`, },
        'Hid&Sud+': { OFF: `PageUp`, ON: `PageDown`, },
    },
    pgUp: {
        'Hidden+': { OFF: `PageUp`, ON: `PageDown`, },
        'Sudden+': { OFF: `PageDown`, ON: `PageUp`, },
        'Hid&Sud+': { OFF: `PageDown`, ON: `PageUp`, },
    },
    std: {
        'Hidden+': { OFF: 0, ON: 1, },
        'Sudden+': { OFF: 1, ON: 0, },
        'Hid&Sud+': { OFF: 1, ON: 0, },
    },
    distH: {
        'Visible': _ => ``,
        'Hidden': _ => `${g_hidSudFunc.filterPos(50)} (${g_hidSudFunc.hidden(50)} / ${g_hidSudFunc.range()})`,
        'Hidden+': (_filterPos) => `${g_hidSudFunc.filterPos(_filterPos)} (${g_hidSudFunc.hidden(_filterPos)} / ${g_hidSudFunc.range()})`,
        'Sudden': _ => `${g_hidSudFunc.filterPos(40)} (${g_hidSudFunc.sudden(40)} / ${g_hidSudFunc.range()})`,
        'Sudden+': (_filterPos) => `${g_hidSudFunc.filterPos(_filterPos)} (${g_hidSudFunc.sudden(_filterPos)} / ${g_hidSudFunc.range()})`,
        'Hid&Sud+': (_filterPos) => `${g_hidSudFunc.filterPos(_filterPos)} (${Math.max(g_hidSudFunc.sudden(_filterPos)
            - (g_posObj.arrowHeight - g_posObj.stepY - g_hidSudFunc.hidden(_filterPos)), 0)} / ${g_hidSudFunc.range()})`,
    },
};
```

### 生成タイミング
- 初回起動時

### プロパティ
- filterPos (number) - Appearanceフィルタの適用割合
- pgDown (object) - Appearanceフィルタを下げる場合の対応キー
- pgUp (object) - Appearanceフィルタを上げる場合の対応キー
- std (object) - Appearanceフィルタのパーセント表示の表示箇所
- distH (object) - Appearanceフィルタの位置別のパーセント表示と可視範囲の表示
- filterPosDefault (object) - Appearanceフィルタの初期位置 (Visible, Hidden, Sudden時)
- (Visible, Hidden, Hidden+, Sudden, Sudden+, Hid&Sud+) (number)