**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-h0074-readyAnimationName) | Japanese** 

[Home](./) &gt; [譜面ヘッダー仕様](dos_header) &gt; [プレイ時の初期設定](dos_header#-プレイ時の初期設定) &gt; [画面表示・キーコントロール](dos_header#画面表示キーコントロール)

| [<- readyAnimationFrame](dos-h0073-readyAnimationFrame) | **readyAnimationName** | [readyColor ->](dos-h0075-readyColor) |

## readyAnimationName
- Ready?のアニメーション名の設定

### 使い方
```
|readyAnimationName=upToDownFade|
```
### 説明
Ready表示のアニメーション名を指定します。デフォルトは`leftToRightFade`です。  
※指定した名前のアニメーションを別途CSSで指定しておく必要があります。

### 関連項目
- [readyDelayFrame](dos-h0052-readyDelayFrame) [:pencil:](dos-h0052-readyDelayFrame/_edit) Ready?が表示されるまでの遅延フレーム数
- [readyAnimationFrame](dos-h0073-readyAnimationFrame) [:pencil:](dos-h0073-readyAnimationFrame/_edit) Ready?のアニメーションフレーム数
- [readyColor](dos-h0075-readyColor) [:pencil:](dos-h0075-readyColor/_edit) Ready?の先頭色設定
- [readyHtml](dos-h0080-readyHtml) [:pencil:](dos-h0080-readyHtml/_edit) Ready?の文字設定

### 更新履歴

|Version|変更内容|
|----|----|
|[v16.3.0](https://github.com/cwtickle/danoniplus/releases/tag/v16.3.0)|・初回実装|

| [<- readyAnimationFrame](dos-h0073-readyAnimationFrame) | **readyAnimationName** | [readyColor ->](dos-h0075-readyColor) |