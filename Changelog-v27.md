**[English](https://github.com/cwtickle/danoniplus-docs/wiki/Changelog-v27) | Japanese** 

[^ Back to All Version List (Security Policy)](https://github.com/cwtickle/danoniplus/security/policy)  
[^ Back to Update Info](./UpdateInfo#-v27-changelog)

[**<- v28**](Changelog-v28) | **v27** | [**v26 ->**](Changelog-v26)  
(**🔖 [17 Releases](https://github.com/cwtickle/danoniplus/releases?q=tag%3Av27)** )

## 🔃 Files changed (v27)

|Directory|FileName||Last Updated|
|----|----|----|----|
|/js|danoni_main.js|[📥](https://github.com/cwtickle/danoniplus/releases/download/v27.8.7/danoni_main.js)|**v27.8.7**|
|/js/lib|danoni_constants.js|[📥](https://github.com/cwtickle/danoniplus/releases/download/v27.8.7/danoni_constants.js)|**v27.8.7**|
|/css|danoni_main.css|[📥](https://github.com/cwtickle/danoniplus/releases/download/v27.2.0/danoni_main.css)|[v27.2.0](Changelog-v27#v2720-2022-03-27)|

<details>
<summary>Changed file list before v26</summary>

|Directory|FileName||Last Updated|
|----|----|----|----|
|/js/lib|danoni_localbinary.js|[📥](https://github.com/cwtickle/danoniplus/releases/download/v15.1.0/danoni_localbinary.js)|[v15.1.0](./Changelog-v15#v1510-2020-05-21)|
|/img|aaShadow.svg<br>arrow.svg<br>arrowShadow.svg<br>borderline.svg<br>c.svg<br>cursor.svg<br>giko.svg<br>iyo.svg<br>monar.svg<br>morara.svg<br>onigiri.svg|[📥](https://github.com/cwtickle/danoniplus/releases/download/v15.1.0/img.zip)|[v15.1.0](./Changelog-v15#v1510-2020-05-21)|
|/skin|danoni_skin_default.css<br>danoni_skin_light.css<br>danoni_skin_skyblue.css|[📥](https://github.com/cwtickle/danoniplus/releases/download/v21.4.2/danoni_skin_default.css)<br>[📥](https://github.com/cwtickle/danoniplus/releases/download/v21.4.2/danoni_skin_light.css)<br>[📥](https://github.com/cwtickle/danoniplus/releases/download/v21.4.2/danoni_skin_skyblue.css)|[v21.4.2](Changelog-v21#v2142-2021-04-07)|

</details>

⭐ New Features / 🛠️ Improvements / 🐞 Bug Fixes  
( Files changed: ⚪ danoni_main.js / 🔵 danoni_constants.js / 🔴 css files )

## [Warning] 更新終了後の不具合情報
- [更新を終了したバージョンの不具合情報#v27](DeprecatedVersionBugs#v27) を参照

## v27.8.7 ([2023-01-20](https://github.com/cwtickle/danoniplus/releases/tag/v27.8.7))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v27.8.7/total)](https://github.com/cwtickle/danoniplus/archive/v27.8.7.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v27.8.7/support/v27?style=social)](https://github.com/cwtickle/danoniplus/compare/v27.8.7...support/v27#files_bucket) 
- 🐞 **C)** カスタムキー定義におけるdivXの第2要素がposX末尾要素の最大値で無い場合、キーコンフィグ保存後にステップゾーン（下段）がずれる問題を修正 ( PR [#1385](https://github.com/cwtickle/danoniplus/pull/1385) ) <- :boom: **initial**

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v27.8.7)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v27.8.6...v27.8.7#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v27.8.7)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v27.8.7.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v27.8.7.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v27.8.7/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v27.8.7/js/lib/danoni_constants.js)
/ 🎣 [**v29.3.5**](./Changelog-v29#v2935-2023-01-20)

## v27.8.6 ([2023-01-07](https://github.com/cwtickle/danoniplus/releases/tag/v27.8.6))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v27.8.6/total)](https://github.com/cwtickle/danoniplus/archive/v27.8.6.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v27.8.6/support/v27?style=social)](https://github.com/cwtickle/danoniplus/compare/v27.8.6...support/v27#files_bucket) 
- 🛠️ 同時押し関連の英訳を見直し ( PR [#1378](https://github.com/cwtickle/danoniplus/pull/1378) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v27.8.6)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v27.8.5...v27.8.6#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v27.8.6)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v27.8.6.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v27.8.6.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v27.8.6/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v27.8.6/js/lib/danoni_constants.js)
/ 🎣 [**v29.3.4**](./Changelog-v29#v2934-2023-01-07)

## v27.8.5 ([2022-12-10](https://github.com/cwtickle/danoniplus/releases/tag/v27.8.5))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v27.8.5/total)](https://github.com/cwtickle/danoniplus/archive/v27.8.5.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v27.8.5/support/v27?style=social)](https://github.com/cwtickle/danoniplus/compare/v27.8.5...support/v27#files_bucket) 
- 🐞 **B)** 速度が激しく変化する場合にタイミングがステップゾーン位置からずれることがある問題を修正 ( PR [#1372](https://github.com/cwtickle/danoniplus/pull/1372) ) <- :boom: **initial**

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v27.8.5)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v27.8.4...v27.8.5#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v27.8.5)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v27.8.5.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v27.8.5.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v27.8.5/js/danoni_main.js)
/ 🎣 [**v29.3.1**](./Changelog-v29#v2931-2022-12-10)

## v27.8.4 ([2022-11-15](https://github.com/cwtickle/danoniplus/releases/tag/v27.8.4))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v27.8.4/total)](https://github.com/cwtickle/danoniplus/archive/v27.8.4.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v27.8.4/support/v27?style=social)](https://github.com/cwtickle/danoniplus/compare/v27.8.4...support/v27#files_bucket) 
- 🐞 **C)** Motionオプションを使用し、フリーズアローについてタイミングをずらして押したとき、そのズレの中にストップが含まれていると、処理が止まる可能性がある問題を修正 ( PR [#1366](https://github.com/cwtickle/danoniplus/pull/1366) ) <- :boom: [**v23.1.1**](Changelog-v23#v2311-2021-09-17)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v27.8.4)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v27.8.3...v27.8.4#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v27.8.4)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v27.8.4.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v27.8.4.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v27.8.4/js/danoni_main.js)
/ 🎣 [**v29.3.0**](./Changelog-v29#v2930-2022-11-15)

## v27.8.3 ([2022-10-31](https://github.com/cwtickle/danoniplus/releases/tag/v27.8.3))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v27.8.3/total)](https://github.com/cwtickle/danoniplus/archive/v27.8.3.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v27.8.3/support/v27?style=social)](https://github.com/cwtickle/danoniplus/compare/v27.8.3...support/v27#files_bucket) 
- 🐞 **C)** 譜面データの値が極端に小さい場合、baseFrameの値がずれてしまう問題を修正 ( PR [#1348](https://github.com/cwtickle/danoniplus/pull/1348) ) <- :boom: [**v25.3.2**](Changelog-v25#v2532-2022-01-18)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v27.8.3)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v27.8.2...v27.8.3#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v27.8.3)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v27.8.3.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v27.8.3.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v27.8.3/js/danoni_main.js)
/ 🎣 [**v28.6.0**](./Changelog-v28#v2860-2022-10-31)

## v27.8.2 ([2022-10-29](https://github.com/cwtickle/danoniplus/releases/tag/v27.8.2))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v27.8.2/total)](https://github.com/cwtickle/danoniplus/archive/v27.8.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v27.8.2/support/v27?style=social)](https://github.com/cwtickle/danoniplus/compare/v27.8.2...support/v27#files_bucket) 
- 🐞 **C)** フリーズアローを強制削除した場合の判定関数の引数誤りを修正 ( PR [#1343](https://github.com/cwtickle/danoniplus/pull/1343) ) <- :boom: [**v5.12.1**](Changelog-v5#v5121-2019-06-17)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v27.8.2)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v27.8.1...v27.8.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v27.8.2)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v27.8.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v27.8.2.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v27.8.2/js/danoni_main.js)
/ 🎣 [**v28.5.0**](./Changelog-v28#v2850-2022-10-29)

---

## v27.8.1 ([2022-08-01](https://github.com/cwtickle/danoniplus/releases/tag/v27.8.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v27.8.1/total)](https://github.com/cwtickle/danoniplus/archive/v27.8.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v27.8.1/support/v27?style=social)](https://github.com/cwtickle/danoniplus/compare/v27.8.1...support/v27#files_bucket) 
- 🐞 **C)** 重複するID名の解消 ( PR [#1300](https://github.com/cwtickle/danoniplus/pull/1300) ) <- :boom: [**v27.8.0**](Changelog-v27#v2780-2022-07-31) / [**v11.4.0**](Changelog-v11#v1140-2020-02-05)
- 🐞 **C)** 譜面明細画面でID名に英数以外が入りうる問題の修正 ( PR [#1300](https://github.com/cwtickle/danoniplus/pull/1300) ) <- :boom: [**v19.3.0**](Changelog-v19#v1930-2021-01-30)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v27.8.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v27.8.0...v27.8.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v27.8.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v27.8.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v27.8.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v27.8.1/js/danoni_main.js)<br>❤️ ショウタ

## v27.8.0 ([2022-07-31](https://github.com/cwtickle/danoniplus/releases/tag/v27.8.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v27.8.0/total)](https://github.com/cwtickle/danoniplus/archive/v27.8.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v27.8.0/support/v27?style=social)](https://github.com/cwtickle/danoniplus/compare/v27.8.0...support/v27#files_bucket) 
- 🛠️ 譜面明細（速度変化・譜面密度グラフなど）への直接移動ボタン及びショートカットを実装 ( PR [#1297](https://github.com/cwtickle/danoniplus/pull/1297), [#1298](https://github.com/cwtickle/danoniplus/pull/1298) )

<img src="https://user-images.githubusercontent.com/44026291/182018132-b8734cb3-0aea-470d-9c25-f29b464d8174.png" width="45%"><img src="https://user-images.githubusercontent.com/44026291/182018176-815228a0-7a4e-4564-998d-b79431362426.png" width="45%">

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v27.8.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v27.7.0...v27.8.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v27.8.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v27.8.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v27.8.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v27.8.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v27.8.0/js/lib/danoni_constants.js)<br>❤️ FUJI

## v27.7.0 ([2022-07-01](https://github.com/cwtickle/danoniplus/releases/tag/v27.7.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v27.7.0/total)](https://github.com/cwtickle/danoniplus/archive/v27.7.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v27.7.0/support/v27?style=social)](https://github.com/cwtickle/danoniplus/compare/v27.7.0...support/v27#files_bucket) 
- ⭐ Dランク追加、B, Cランク基準見直し ( PR [#1295](https://github.com/cwtickle/danoniplus/pull/1295) )

|ランク名|ランク基準(変更前)|ランク基準(変更後)|
|----|----|----|
|B|スコアが50万以上70万未満でクリア|スコアが**65万**以上70万未満でクリア|
|C|スコアが50万未満でクリア|スコアが**60万以上65万未満**でクリア|
|D|-|スコアが60万未満でクリア|

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v27.7.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v27.6.0...v27.7.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v27.7.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v27.7.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v27.7.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v27.7.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v27.7.0/js/lib/danoni_constants.js) 

## v27.6.0 ([2022-06-07](https://github.com/cwtickle/danoniplus/releases/tag/v27.6.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v27.6.0/total)](https://github.com/cwtickle/danoniplus/archive/v27.6.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v27.6.0/support/v27?style=social)](https://github.com/cwtickle/danoniplus/compare/v27.6.0...support/v27#files_bucket) 
- ⭐ カスタムキーのパターン追記方法を拡張 ( PR [#1293](https://github.com/cwtickle/danoniplus/pull/1293) )
- 🛠️ タイトル・設定画面以外のデフォルトイベントを抑止 ( PR [#1292](https://github.com/cwtickle/danoniplus/pull/1292) )
- 🛠️ currentScriptが取得できないときの処理を見直し ( PR [#1291](https://github.com/cwtickle/danoniplus/pull/1291) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v27.6.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v27.5.1...v27.6.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v27.6.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v27.6.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v27.6.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v27.6.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v27.6.0/js/lib/danoni_constants.js) 

## v27.5.1 ([2022-05-22](https://github.com/cwtickle/danoniplus/releases/tag/v27.5.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v27.5.1/total)](https://github.com/cwtickle/danoniplus/archive/v27.5.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v27.5.1/support/v27?style=social)](https://github.com/cwtickle/danoniplus/compare/v27.5.1...support/v27#files_bucket) 
- 🐞 **B)** フォルダ名に半角スペースや全角文字が含まれている場合に起動できないことがある問題を修正 ( PR [#1289](https://github.com/cwtickle/danoniplus/pull/1289), Gitter [2022-05-22](https://gitter.im/danonicw/community?at=628912bcdb6f627d25931313) ) <- :boom: [**v27.3.1**](Changelog-v27#v2731-2022-04-10)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v27.5.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v27.5.0...v27.5.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v27.5.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v27.5.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v27.5.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v27.5.1/js/danoni_main.js)<br>❤️ すずめ (@suzme)

## v27.5.0 ([2022-05-21](https://github.com/cwtickle/danoniplus/releases/tag/v27.5.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v27.5.0/total)](https://github.com/cwtickle/danoniplus/archive/v27.5.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v27.5.0/support/v27?style=social)](https://github.com/cwtickle/danoniplus/compare/v27.5.0...support/v27#files_bucket) 
- ⭐ $区切りの一部譜面ヘッダーについて改行区切りに対応 ( PR [#1287](https://github.com/cwtickle/danoniplus/pull/1287) )
- 🛠️ 関数名間違い、コードの一部見直し ( PR [#1285](https://github.com/cwtickle/danoniplus/pull/1285), [#1286](https://github.com/cwtickle/danoniplus/pull/1286) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v27.5.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v27.4.0...v27.5.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v27.5.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v27.5.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v27.5.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v27.5.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v27.5.0/js/lib/danoni_constants.js) 

## v27.4.0 ([2022-04-16](https://github.com/cwtickle/danoniplus/releases/tag/v27.4.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v27.4.0/total)](https://github.com/cwtickle/danoniplus/archive/v27.4.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v27.4.0/support/v27?style=social)](https://github.com/cwtickle/danoniplus/compare/v27.4.0...support/v27#files_bucket) 
- ⭐ カラーオブジェクトについて矢印以外の回転に対応 ( PR [#1283](https://github.com/cwtickle/danoniplus/pull/1283) )
- 🛠️ 11ikeyの別パターンのアシスト設定に対応 ( PR [#1282](https://github.com/cwtickle/danoniplus/pull/1282) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v27.4.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v27.3.1...v27.4.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v27.4.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v27.4.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v27.4.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v27.4.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v27.4.0/js/lib/danoni_constants.js) 

## v27.3.1 ([2022-04-10](https://github.com/cwtickle/danoniplus/releases/tag/v27.3.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v27.3.1/total)](https://github.com/cwtickle/danoniplus/archive/v27.3.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v27.3.1/support/v27?style=social)](https://github.com/cwtickle/danoniplus/compare/v27.3.1...support/v27#files_bucket) 
- ⭐ タイトル画面表示前にカスタム関数を挿入できるよう変更 ( PR [#1278](https://github.com/cwtickle/danoniplus/pull/1278), [#1280](https://github.com/cwtickle/danoniplus/pull/1280) )
- 🛠️ ラベル・ボタンの座標・サイズ設定をdanoni_constants.jsへ移動 ( PR [#1275](https://github.com/cwtickle/danoniplus/pull/1275), [#1276](https://github.com/cwtickle/danoniplus/pull/1276) )
- 🛠️ キーコンフィグ、結果画面周りのコードを整理 ( PR [#1274](https://github.com/cwtickle/danoniplus/pull/1274) )
- 🐞 **C)** カレントパス、譜面ファイル取得部分をエスケープするよう修正 ( PR [#1277](https://github.com/cwtickle/danoniplus/pull/1277) ) <- :boom: [**v19.4.1**](Changelog-v19#v1941-2021-02-06)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v27.3.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v27.2.0...v27.3.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v27.3.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v27.3.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v27.3.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v27.3.1/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v27.3.1/js/lib/danoni_constants.js) 

## v27.2.0 ([2022-03-27](https://github.com/cwtickle/danoniplus/releases/tag/v27.2.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v27.2.0/total)](https://github.com/cwtickle/danoniplus/archive/v27.2.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v27.2.0/support/v27?style=social)](https://github.com/cwtickle/danoniplus/compare/v27.2.0...support/v27#files_bucket) 
- ⭐ ColorType:1～4に対して矢印・フリーズアロー色を任意の色に変更する機能を実装 ( PR [#1270](https://github.com/cwtickle/danoniplus/pull/1270), [#1272](https://github.com/cwtickle/danoniplus/pull/1272) )
- 🐞**C)** canvas要素があるとき背景部の表示が欠けることがある問題を修正 ( PR [#1271](https://github.com/cwtickle/danoniplus/pull/1271) ) <- :boom: [**v27.1.0**](Changelog-v27#v2710-2022-03-25)

<img src="https://user-images.githubusercontent.com/44026291/160271672-cb697a62-5dee-43ea-a131-04b656ed8a2e.png" width="45%"><img src="https://user-images.githubusercontent.com/44026291/160271706-b2e1479e-ec24-4349-8764-14cb2219668a.png" width="45%">

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v27.2.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v27.1.0...v27.2.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v27.2.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v27.2.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v27.2.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v27.2.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v27.2.0/js/lib/danoni_constants.js)🔴 

## v27.1.0 ([2022-03-25](https://github.com/cwtickle/danoniplus/releases/tag/v27.1.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v27.1.0/total)](https://github.com/cwtickle/danoniplus/archive/v27.1.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v27.1.0/support/v27?style=social)](https://github.com/cwtickle/danoniplus/compare/v27.1.0...support/v27#files_bucket) 
- ⭐ キー別にウィンドウ横幅・配置を自動制御する機能を実装 ( PR [#1267](https://github.com/cwtickle/danoniplus/pull/1267) )
- ⭐ 作品別のカスタムファイル指定に対して共通設定を参照する機能を追加 ( PR [#1268](https://github.com/cwtickle/danoniplus/pull/1268) )
- 🛠️ ウィンドウサイズのうち、可変なものを後で再設定する仕様に変更 ( PR [#1267](https://github.com/cwtickle/danoniplus/pull/1267) )
- 🛠️ HTMLファイル上で danoni_main.css を読み込む記述を任意化 ( PR [#1265](https://github.com/cwtickle/danoniplus/pull/1265) )
- 🛠️ 型変換周りのコード整理 ( PR [#1266](https://github.com/cwtickle/danoniplus/pull/1266) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v27.1.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v27.0.0...v27.1.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v27.1.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v27.1.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v27.1.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v27.1.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v27.1.0/js/lib/danoni_constants.js) 

## v27.0.0 ([2022-03-18](https://github.com/cwtickle/danoniplus/releases/tag/v27.0.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v27.0.0/total)](https://github.com/cwtickle/danoniplus/archive/v27.0.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v27.0.0/support/v27?style=social)](https://github.com/cwtickle/danoniplus/compare/v27.0.0...support/v27#files_bucket) 
- ⭐ プレイ画面からリザルト画面移行時の100ミリ秒を廃止 ( Issue [#902](https://github.com/cwtickle/danoniplus/pull/902), PR [#1256](https://github.com/cwtickle/danoniplus/pull/1256), [#1261](https://github.com/cwtickle/danoniplus/pull/1261) )
- 🛠️ 全体的にコードを整理 ( PR [#1258](https://github.com/cwtickle/danoniplus/pull/1258), [#1260](https://github.com/cwtickle/danoniplus/pull/1260), [#1262](https://github.com/cwtickle/danoniplus/pull/1262), [#1263](https://github.com/cwtickle/danoniplus/pull/1263) )
- 🛠️ 非推奨関数を danoni_legacy_function.js へ移動 ( PR [#1259](https://github.com/cwtickle/danoniplus/pull/1259) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v27.0.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v26.7.0...v27.0.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v27.0.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v27.0.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v27.0.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v27.0.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v27.0.0/js/lib/danoni_constants.js) 


[**<- v28**](Changelog-v28) | **v27** | [**v26 ->**](Changelog-v26)  