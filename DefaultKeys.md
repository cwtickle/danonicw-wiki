# 既存キーのカスタム定義
- 他の作品で使用しやすいように、既存キーの名前に"z"を付加しています。

## 5key
```
|keyExtraList=5z|keyName5z=5|
|color5z=0,0,0,0,2$2,0,0,0,0$0,0,2,0,0|
|chara5z=left,down,up,right,space$space,left,down,up,right$left,down,space,up,right|
|stepRtn5z=0,-90,90,180,onigiri$onigiri,0,-90,90,180$0,-90,onigiri,90,180|
|pos5z=0,1,2,3,4$0,1,2,3,4$0,1,2,3,4|
|div5z=5$5$5|
|keyCtrl5z=37/0,40/0,38/0,39/0,32/0$32/0,37/0,40/0,38/0,39/0$37/0,40/0,32/0,38/0,39/0|
|blank5z=57.5$57.5$57.5|
|shuffle5z=0,0,0,0,1$1,0,0,0,0$0,0,1,0,0|
|scroll5z=Cross::1,-1,-1,1,1/Split::1,1,-1,-1,-1/Alternate::1,-1,1,-1,1$Cross::1,1,-1,-1,1/Split::1,1,1,-1,-1/Alternate::1,-1,1,-1,1$Cross::1,-1,-1,-1,1/Split::1,1,-1,-1,-1/Alternate::1,-1,1,-1,1|
```

## 7key
```
|keyExtraList=7z|keyName7z=7|
|color7z=0,1,0,2,0,1,0$0,1,0,2,0,1,0$2,2,2,0,0,0,0$2,0,1,0,1,0,1,0$2,0,1,0,1,0,1,0$2,0,1,0,1,0,1,0$2,0,1,0,1,0,1,0|
|chara7z=left,leftdia,down,space,up,rightdia,right$left,leftdia,down,space,up,rightdia,right$left,leftdia,down,space,up,rightdia,right$oni,left,leftdia,down,space,up,rightdia,right$oni,left,leftdia,down,space,up,rightdia,right$oni,left,leftdia,down,space,up,rightdia,right$oni,left,leftdia,down,space,up,rightdia,right|
|stepRtn7z=0,-45,-90,onigiri,90,135,180$0,-45,-90,onigiri,90,135,180$giko,onigiri,iyo,0,-90,90,180$onigiri,0,30,60,90,120,150,180$onigiri,0,30,60,90,120,150,180$onigiri,0,30,60,90,120,150,180$onigiri,0,30,60,90,120,150,180|
|pos7z=0,1,2,3,4,5,6$0,1,2,3,4,5,6$0,1,2,3,4,5,6$0,1,2,3,4,5,6,7$0,1,2,3,4,5,6,7$0,1,2,3,4,5,6,7$0,1,2,3,4,5,6,7|
|div7z=7$7$7$7$7$7$7|
```
