[← オブジェクトリファレンスに戻る](ObjectReferenceIndex)  

# addXY
### 概要

- id名、識別子別のX, Y座標差分を管理する関数。処理後はid名の座標を再計算して反映します。
- 異なる識別子間のX, Y座標はそれぞれ維持されます。
- 過去に使用済みの識別子を使った場合、その識別子の値のみが上書きされます。

### 引数（括弧内はデフォルト値）

|引数|型|必須|指定内容|
|----|----|----|----|
|_id|string|*|オブジェクトラベルに設定されているid名|
|_typeId|string|*|識別子。id別のX, Y座標差分は、この単位で管理する。|
|_x|number|*|X座標差分|
|_y|number|*|Y座標差分|
|_overwrite|boolean||上書きフラグ。デフォルトは`false`(座標設定を上書きしない)|

### 返却値
- なし (指定された_idに合致するオブジェクトのtop, left属性を更新)

### 使用例

```javascript
addXY(`mainSprite0`, `test`, 0, 15);
// mainSprite0 の left = 0px, top = 15px

addXY(`mainSprite0`, `test`, 0, 30);
// mainSprite0 の left = 0px, top = 30px

addXY(`mainSprite0`, `test2`, 0, 20);
// mainSprite0 の left = 0px, top = 50px (30px + 20px)

addXY(`mainSprite0`, `test3`, 10, 0, true);
// mainSprite0 の left = 10px, top = 0px　※上書きフラグがついているためリセット
```

# addX / addY
### 概要

- addXYの単軸版。X軸もしくはY軸しか使わない場合、こちらを使った方が処理は少ない。

### 引数（括弧内はデフォルト値）

|引数|型|必須|指定内容|
|----|----|----|----|
|_id|string|*|オブジェクトラベルに設定されているid名|
|_typeId|string|*|識別子。id別のX, Y座標差分は、この単位で管理する。|
|_x (_y)|number|*|X座標 (Y座標)差分|
|_overwrite|boolean||上書きフラグ。デフォルトは`false`(座標設定を上書きしない)|

### 返却値
- なし (指定された_idに合致するオブジェクトのtopもしくはleft属性を更新)

### 使用例

```javascript
addX(`mainSprite`, `addOption`, 30);
// mainSprite の left = 30px

addY(`mainSprite`, `addOption`, 70);
// mainSprite の top = 70px

addY(`mainSprite`, `addOption2`, 30);
// mainSprite の top = 100px (70px + 30px)

addY(`mainSprite`, `addOption2`, 30, true);
// mainSprite の top = 30px ※上書きフラグがついているためリセット
```

# delXY
### 概要

- id名、識別子別のX, Y座標差分を削除する関数。
- 削除後、対象idのX, Y座標が再計算されます。

### 引数（括弧内はデフォルト値）

|引数|型|必須|指定内容|
|----|----|----|----|
|_id|string|*|オブジェクトラベルに設定されているid名|
|_typeId|string|*|識別子。id別のX, Y座標差分は、この単位で管理する。|

### 返却値
- なし (指定された_idに合致するオブジェクトのtop, left属性を更新)

# delX / delY
### 概要

- delXYの単軸版。

### 引数（括弧内はデフォルト値）

|引数|型|必須|指定内容|
|----|----|----|----|
|_id|string|*|オブジェクトラベルに設定されているid名|
|_typeId|string|*|識別子。id別のX, Y座標差分は、この単位で管理する。|

### 返却値
- なし (指定された_idに合致するオブジェクトのtopもしくはleft属性を更新)


## 更新履歴

|Version|変更内容|
|----|----|
|[v39.4.0](https://github.com/cwtickle/danoniplus/releases/tag/v39.4.0)|・初期実装 (addX, addY, delX, delY)|
|[v39.3.0](https://github.com/cwtickle/danoniplus/releases/tag/v39.3.0)|・初期実装 (addXY, delXY)|
