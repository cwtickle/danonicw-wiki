**[English](https://github.com/cwtickle/danoniplus-docs/wiki/AboutFrameProcessing) | Japanese** 

| [< オブジェクト構成・ボタン](./DOM) | **疑似フレーム処理仕様** | [カスタムjs(スキンjs)による処理割込み >](./AboutCustomJs) |

# 擬似フレーム処理仕様
- Dancing☆Onigiri (CW Edition)では旧Flashでのフレーム処理を再現するため、  
[setTimeout](https://developer.mozilla.org/ja/docs/Web/API/setTimeout) を使って制御しています。そのイメージを下記に示します。  

## フレーム処理の考え方
- fps=60のとき、1秒間に60フレーム処理できるように1フレームに1/60秒が割り当てられ、その時間で描画や計算処理を行います。余った時間は待ち時間になります。  
描画や計算処理が追い付かないときは、後続のフレームで遅れのリカバリーを試みます。  
setTimeoutの仕様で、処理が遅れていても4msはウェイトが入ります。
<img src="./wiki/frameprocess1.png" width="90%">

- playbackRate=1以外（例えば低速）の場合、フレーム数を再計算した結果、情報誤差が発生しやすくなることがあります。これを防ぐため、playbackRate=1以外に限り、計算元のフレーム数の小数を許容することで、情報誤差を最小化します。（計算後のフレーム数は整数部に丸められます）

## 矢印の移動距離と処理フレーム数の関係
- ロード処理で、各矢印の移動距離を決定します。  
表示位置と表示タイミングは、ステップゾーン上の位置から逆算してフレーム毎の移動量を加算して算出します。  
移動量が描画エリアを超えるまで逆算され、その距離だけ戻った状態で矢印の描画が開始されます。

<img src="./wiki/frameprocess2.png" width="90%">

## 音楽再生位置の調整によるAdjustmentの（疑似）小数フレーム
- 音楽の再生位置を調整することで、Adjustmentの幅のみ擬似的に0.1フレーム単位で小数フレームを実現しています。音楽再生位置はフレーム数ではなく、ミリ秒で表現できるためです。

<img src="./wiki/dos-h0008-02.png" width="90%">

## プレイ中のフレーム数の種類
- 上記の音楽再生位置、Adjustmentなどの値によって、現在処理しているフレーム位置は変化します。  
実際の制作やプレイに支障が出ないように、複数のフレーム(位置)を保持しています。

|変数名|概要|
|----|----|
|g_scoreObj.frameNum|譜面ヘッダー及び設定画面のAdjustment、補正フレームを反映した実際のフレーム数(位置)|
|g_scoreObj.baseFrame|すべての補正フレームを除いた、譜面作成側から見たフレーム数(位置)<br>演出等で利用することを考慮し、小数フレームは切り捨てる<br>(譜面データで指定したフレーム数と一致)|

<img src="https://user-images.githubusercontent.com/44026291/199033833-6c079bfc-e320-4d27-8ddc-28143cce14a2.png" width="90%">

## フレームの制約を受ける項目、受けない項目

### フレームの制約を受ける項目
- 矢印・フリーズアロー生成／移動（再描画）、速度変化、色変化、背景・マスクモーション、矢印・フリーズアローモーション
- その他、mainEnterFrame上にある情報種

### フレームの制約を受けない項目
- 音楽再生位置、キー操作イベント（判定位置からの誤差フレーム数はフレーム数に依存）


## 関連項目
- [**adjustment**](dos-h0009-adjustment) [:pencil:](dos-h0009-adjustment/_edit) 譜面位置の初期調整
- [**playbackRate**](dos-h0010-playbackRate) [:pencil:](dos-h0010-playbackRate/_edit) 楽曲再生速度（主にテストプレイ用）

## 更新履歴

|Version|変更内容|
|----|----|
|[v23.0.1](https://github.com/cwtickle/danoniplus/releases/tag/v23.0.1)|・譜面データが極端に小さいフレーム数のときにAdjustmentが一部利かない問題を修正（計算式を修正）|
|[v23.0.0](https://github.com/cwtickle/danoniplus/releases/tag/v23.0.0)|・音楽再生位置の調整により、疑似的に0.1フレーム単位のAdjustmentが使えるよう拡張|
|[v1.0.0](https://github.com/cwtickle/danoniplus/releases/tag/v1.0.1)|・初期実装|

| [< オブジェクト構成・ボタン](./DOM) | **疑似フレーム処理仕様** | [カスタムjs(スキンjs)による処理割込み >](./AboutCustomJs) |
