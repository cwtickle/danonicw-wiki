**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-h0049-stepYR) | Japanese** 

[Home](./) &gt; [譜面ヘッダー仕様](dos_header) &gt; [プレイ時の初期設定](dos_header#-プレイ時の初期設定) &gt; [プレイ画面位置の設定](dos_header#プレイ画面位置の設定)

| [<- stepY](dos-h0014-stepY) | **stepYR** | [arrowJdgY / frzJdgY ->](dos-h0058-jdgY) |

## stepYR
- ステップゾーン(下)のY座標現位置からの差分設定

### 使い方
```
|stepYR=-100|
```
### 説明
ステップゾーン(下)のY座標の差分を指定します。単位はpx。デフォルトは0pxです。    

### 使用例
- stepYRの値を変えると、ステップゾーン(下)のY座標だけが変わります。  
ステップゾーン(上)は変化しません。
- Ready?位置、判定キャラクタ・コンボ位置も合わせて補正されます。

![dos-h0049-01.png](./wiki/dos-h0049-01.png)

### 関連項目
- [stepY](dos-h0014-stepY) [:pencil:](dos-h0014-stepY/_edit) ステップゾーンのY座標位置
- [arrowJdgY / frzJdgY](dos-h0058-jdgY) [:pencil:](dos-h0058-jdgY/_edit) 判定キャラクタのY座標位置
- [bottomWordSet](dos-h0059-bottomWordSet) [:pencil:](dos-h0059-buttomWordSet/_edit) 下側の歌詞表示位置をステップゾーン位置に連動させる設定

### 更新履歴

|Version|変更内容|
|----|----|
|[v8.0.0](https://github.com/cwtickle/danoniplus/releases/tag/v8.0.0)|・初回実装|

| [<- stepY](dos-h0014-stepY) | **stepYR** | [arrowJdgY / frzJdgY ->](dos-h0058-jdgY) |