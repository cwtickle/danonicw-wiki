**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-h0093-excessiveJdgUse) | Japanese** 

[Home](./) &gt; [譜面ヘッダー仕様](dos_header) &gt; [プレイ時の初期設定](dos_header#-プレイ時の初期設定) &gt; [ライフゲージ・判定関連の設定](dos_header#ライフゲージ判定関連の設定)

| [<- frzAttempt](dos-h0038-frzAttempt) | **excessiveJdgUse** || [readyDelayFrame ->](dos-h0052-readyDelayFrame) |

## excessiveJdgUse
- 空押し判定のデフォルト設定
- 共通設定 ⇒ [g_presetObj.excessiveJdgUse](dos-s0003-initialGauge#空押し判定のデフォルト設定-g_presetobjexcessivejdguse)

### 使い方
```
|excessiveJdgUse=true|
```
### 説明
- 空押し判定をデフォルトで適用するかどうかを設定します。デフォルトは「適用しない」設定です。
- この設定を"true"にしたとしても、Excessive設定を画面から行うことは可能です。  
（あくまでデフォルト値の設定のみ）

### 関連項目
- [**settingUse**](dos-h0035-settingUse) [:pencil:](dos-h0035-settingUse/_edit) オプション有効化設定
- [共通設定ファイル仕様](dos_setting) &gt; [ゲージ設定](dos-s0003-initialGauge)
- [共通設定ファイル仕様](dos_setting) &gt; [オプション有効化](dos-s0006-settingUse)

### 更新履歴

|Version|変更内容|
|----|----|
|[v32.5.0](https://github.com/cwtickle/danoniplus/releases/tag/v32.5.0)|・初回実装|

| [<- frzAttempt](dos-h0038-frzAttempt) | **excessiveJdgUse** || [readyDelayFrame ->](dos-h0052-readyDelayFrame) |