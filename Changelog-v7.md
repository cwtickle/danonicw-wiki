⭐ New Features / 🛠️ Improvements / 🐞 Bug Fixes

[**<- v8**](Changelog-v8) | **v7** | [**v6 ->**](Changelog-v6)  
(**🔖 [28 Releases](https://github.com/cwtickle/danoniplus/releases?q=tag%3Av7)** )

## [Warning] 更新終了後の不具合情報
- [更新を終了したバージョンの不具合情報#v7](DeprecatedVersionBugs#v7) を参照

## v7.9.13 ([2019-10-14](https://github.com/cwtickle/danoniplus/releases/tag/v7.9.13))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v7.9.13/total)](https://github.com/cwtickle/danoniplus/archive/v7.9.13.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v7.9.13/support/v7?style=social)](https://github.com/cwtickle/danoniplus/compare/v7.9.13...support/v7#files_bucket) 
- 🐞 背景・マスクモーションでオールクリアが使用できない問題を修正 ( PR [#488](https://github.com/cwtickle/danoniplus/pull/488) ) <- :boom: [**v7.7.0**](Changelog-v7#v770-2019-08-25)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v7.9.13)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v7.9.12...v7.9.13#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v7.9.13)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v7.9.13.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v7.9.13.tar.gz) )
/ 🎣 [**v9.1.0**](./Changelog-v9#v910-2019-10-15)

## v7.9.12 ([2019-10-13](https://github.com/cwtickle/danoniplus/releases/tag/v7.9.12))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v7.9.12/total)](https://github.com/cwtickle/danoniplus/archive/v7.9.12.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v7.9.12/support/v7?style=social)](https://github.com/cwtickle/danoniplus/compare/v7.9.12...support/v7#files_bucket) 
- 🐞 ダミーIDの譜面ヘッダー誤りを修正 ( PR [#483](https://github.com/cwtickle/danoniplus/pull/483) ) <- :boom: [**v6.0.0**](Changelog-v6#v600-2019-06-22)
- 🐞 ダミー矢印/フリーズアロー用カスタム関数が定義できない問題を修正 ( PR [#485](https://github.com/cwtickle/danoniplus/pull/485) ) <- :boom: [**v6.1.0**](Changelog-v6#v610-2019-06-22)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v7.9.12)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v7.9.10...v7.9.12#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v7.9.12)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v7.9.12.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v7.9.12.tar.gz) )
/ 🎣 [**v9.0.3**](./Changelog-v9#v903-2019-10-13)

## v7.9.10 ([2019-10-12](https://github.com/cwtickle/danoniplus/releases/tag/v7.9.10))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v7.9.10/total)](https://github.com/cwtickle/danoniplus/archive/v7.9.10.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v7.9.10/support/v7?style=social)](https://github.com/cwtickle/danoniplus/compare/v7.9.10...support/v7#files_bucket)
- 🐞 低速時かつフリーズアロー開始判定時にPF判定が早まる問題を修正 ( PR [#481](https://github.com/cwtickle/danoniplus/pull/481) ) <- :boom: [**v5.7.0**](Changelog-v5#v570-2019-06-01)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v7.9.10)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v7.9.9...v7.9.10#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v7.9.10)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v7.9.10.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v7.9.10.tar.gz) )
/ 🎣 [**v9.0.1**](./Changelog-v9#v901-2019-10-12)

## v7.9.9 ([2019-10-08](https://github.com/cwtickle/danoniplus/releases/tag/v7.9.9))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v7.9.9/total)](https://github.com/cwtickle/danoniplus/archive/v7.9.9.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v7.9.9/support/v7?style=social)](https://github.com/cwtickle/danoniplus/compare/v7.9.9...support/v7#files_bucket)
- 🐞 速度変化が同一フレームにあると正常に動作しなくなる不具合を修正 ( PR [#477](https://github.com/cwtickle/danoniplus/pull/477) ) <- :boom: **initial**

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v7.9.9)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v7.9.8...v7.9.9#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v7.9.9)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v7.9.9.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v7.9.9.tar.gz) )
/ 🎣 [**v8.7.3**](./Changelog-v8#v873-2019-10-08)

## v7.9.8 ([2019-10-06](https://github.com/cwtickle/danoniplus/releases/tag/v7.9.8))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v7.9.8/total)](https://github.com/cwtickle/danoniplus/archive/v7.9.8.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v7.9.8/support/v7?style=social)](https://github.com/cwtickle/danoniplus/compare/v7.9.8...support/v7#files_bucket)
- 🐞 エスケープ文字の適用順序誤りを修正 ( PR [#472](https://github.com/cwtickle/danoniplus/pull/472), [#473](https://github.com/cwtickle/danoniplus/pull/473) ) <- :boom: [**v0.66.x**](Changelog-v0#v066x-2018-11-16)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v7.9.8)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v7.9.7...v7.9.8#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v7.9.8)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v7.9.8.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v7.9.8.tar.gz) )
/ 🎣 [**v8.7.2**](./Changelog-v8#v872-2019-10-06)

## v7.9.7 ([2019-09-30](https://github.com/cwtickle/danoniplus/releases/tag/v7.9.7))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v7.9.7/total)](https://github.com/cwtickle/danoniplus/archive/v7.9.7.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v7.9.7/support/v7?style=social)](https://github.com/cwtickle/danoniplus/compare/v7.9.7...support/v7#files_bucket)
- 🐞 エラーウィンドウが表示されない問題を修正 ( PR [#459](https://github.com/cwtickle/danoniplus/pull/459) ) <- :boom: [**v2.6.0**](Changelog-v2#v260-2019-02-10)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v7.9.7)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v7.9.6...v7.9.7#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v7.9.7)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v7.9.7.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v7.9.7.tar.gz) )
/ 🎣 [**v8.6.1**](./Changelog-v8#v861-2019-09-30)

## v7.9.6 ([2019-09-29](https://github.com/cwtickle/danoniplus/releases/tag/v7.9.6))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v7.9.6/total)](https://github.com/cwtickle/danoniplus/archive/v7.9.6.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v7.9.6/support/v7?style=social)](https://github.com/cwtickle/danoniplus/compare/v7.9.6...support/v7#files_bucket)
- 🐞 クエリで譜面番号が指定されていた場合に、初期速度が1譜面目の設定になる問題を修正 ( PR [#453](https://github.com/cwtickle/danoniplus/pull/453) ) <- :boom: [**v7.3.0**](Changelog-v7#v730-2019-07-20)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v7.9.6)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v7.9.5...v7.9.6#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v7.9.6)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v7.9.6.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v7.9.6.tar.gz) )
/ 🎣 [**v8.5.2**](./Changelog-v8#v852-2019-09-29)

## v7.9.5 ([2019-09-28](https://github.com/cwtickle/danoniplus/releases/tag/v7.9.5))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v7.9.5/total)](https://github.com/cwtickle/danoniplus/archive/v7.9.5.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v7.9.5/support/v7?style=social)](https://github.com/cwtickle/danoniplus/compare/v7.9.5...support/v7#files_bucket)
- 🛠️ 画像のプリロードを二重に行わないよう変更 ( PR [#447](https://github.com/cwtickle/danoniplus/pull/447) )
- 🐞 クエリで譜面番号が指定されていた場合に、キー数が異なることがある問題を修正 ( PR [#447](https://github.com/cwtickle/danoniplus/pull/447) ) <- :boom: [**v7.7.0**](Changelog-v7#v770-2019-08-25)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v7.9.5)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v7.9.4...v7.9.5#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v7.9.5)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v7.9.5.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v7.9.5.tar.gz) )
/ 🎣 [**v8.5.0**](./Changelog-v8#v850-2019-09-28)

## v7.9.4 ([2019-09-23](https://github.com/cwtickle/danoniplus/releases/tag/v7.9.4))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v7.9.4/total)](https://github.com/cwtickle/danoniplus/archive/v7.9.4.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v7.9.4/support/v7?style=social)](https://github.com/cwtickle/danoniplus/compare/v7.9.4...support/v7#files_bucket)
- 🐞 ロード画面でEnterを押すと複数回ロードが発生する問題を修正 ( PR [#432](https://github.com/cwtickle/danoniplus/pull/432) ) <- :boom: **initial**
- 🐞 メイン画面で曲中リトライキーを連打した場合に譜面がずれることがある問題を修正 ( PR [#433](https://github.com/cwtickle/danoniplus/pull/433) ) <- :boom: **initial**

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v7.9.4)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v7.9.3...v7.9.4#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v7.9.4)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v7.9.4.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v7.9.4.tar.gz) )
/ 🎣 [**v8.0.4**](./Changelog-v8#v804-2019-09-23)

## v7.9.3 ([2019-09-16](https://github.com/cwtickle/danoniplus/releases/tag/v7.9.3))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v7.9.3/total)](https://github.com/cwtickle/danoniplus/archive/v7.9.3.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v7.9.3/support/v7?style=social)](https://github.com/cwtickle/danoniplus/compare/v7.9.3...support/v7#files_bucket)
- 🐞 ゲームオーバー時の200ミリ秒遅延により、ハイスコア表示がおかしくなることがある問題を修正 ( PR [#428](https://github.com/cwtickle/danoniplus/pull/428) ) <- :boom: **initial**

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v7.9.3)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v7.9.2...v7.9.3#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v7.9.3)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v7.9.3.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v7.9.3.tar.gz) )
/ 🎣 [**v8.0.2**](./Changelog-v8#v802-2019-09-16)

## v7.9.2 ([2019-09-15](https://github.com/cwtickle/danoniplus/releases/tag/v7.9.2))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v7.9.2/total)](https://github.com/cwtickle/danoniplus/archive/v7.9.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v7.9.2/support/v7?style=social)](https://github.com/cwtickle/danoniplus/compare/v7.9.2...support/v7#files_bucket)
- 🐞 速度変化が補正込みで負のフレームにあるときの不具合を修正 ( PR [#426](https://github.com/cwtickle/danoniplus/pull/426) ) <- :boom: **initial**

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v7.9.2)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v7.9.1...v7.9.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v7.9.2)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v7.9.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v7.9.2.tar.gz) )
/ 🎣 [**v8.0.1**](./Changelog-v8#v801-2019-09-15)

----

## v7.9.1 ([2019-09-03](https://github.com/cwtickle/danoniplus/releases/tag/v7.9.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v7.9.1/total)](https://github.com/cwtickle/danoniplus/archive/v7.9.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v7.9.1/support/v7?style=social)](https://github.com/cwtickle/danoniplus/compare/v7.9.1...support/v7#files_bucket)
- 🛠️ 速度変化においてコメント文を実装 ( PR [#411](https://github.com/cwtickle/danoniplus/pull/411) )
- 🐞 色変化・歌詞表示のコメント行にカンマが入ったときの不具合を修正 ( PR [#411](https://github.com/cwtickle/danoniplus/pull/411) ) <- :boom: [**v7.9.0**](Changelog-v7#v790-2019-09-02)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v7.9.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v7.9.0...v7.9.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v7.9.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v7.9.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v7.9.1.tar.gz) )

## v7.9.0 ([2019-09-02](https://github.com/cwtickle/danoniplus/releases/tag/v7.9.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v7.9.0/total)](https://github.com/cwtickle/danoniplus/archive/v7.9.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v7.9.0/support/v7?style=social)](https://github.com/cwtickle/danoniplus/compare/v7.9.0...support/v7#files_bucket)
- ⭐️ クリア失敗時のリザルトモーションを実装 ( PR [#409](https://github.com/cwtickle/danoniplus/pull/409) )
- ⭐️ 背景・マスクモーション、色変化、歌詞表示でコメント文を実装 ( PR [#408](https://github.com/cwtickle/danoniplus/pull/408) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v7.9.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v7.8.2...v7.9.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v7.9.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v7.9.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v7.9.0.tar.gz) )<br>❤️ ショウタ

## v7.8.2 ([2019-09-01](https://github.com/cwtickle/danoniplus/releases/tag/v7.8.2))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v7.8.2/total)](https://github.com/cwtickle/danoniplus/archive/v7.8.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v7.8.2/support/v7?style=social)](https://github.com/cwtickle/danoniplus/compare/v7.8.2...support/v7#files_bucket)
- 🐞 キーコンフィグの別パターンが選択できない問題を修正 ( PR [#406](https://github.com/cwtickle/danoniplus/pull/406) ) <- :boom: [**v7.7.0**](Changelog-v7#v770-2019-08-25)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v7.8.2)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v7.8.1...v7.8.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v7.8.2)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v7.8.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v7.8.2.tar.gz) )<br>❤️ MFV2 (@MFV2), aconite

## v7.8.1 ([2019-08-31](https://github.com/cwtickle/danoniplus/releases/tag/v7.8.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v7.8.1/total)](https://github.com/cwtickle/danoniplus/archive/v7.8.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v7.8.1/support/v7?style=social)](https://github.com/cwtickle/danoniplus/compare/v7.8.1...support/v7#files_bucket)
- 🛠️ 設定画面のAdjustmentによらず、見た目の曲時間を表示するように変更 ( PR [#404](https://github.com/cwtickle/danoniplus/pull/404) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v7.8.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v7.8.0...v7.8.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v7.8.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v7.8.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v7.8.1.tar.gz) )<br>❤️ ショウタ

## v7.8.0 ([2019-08-30](https://github.com/cwtickle/danoniplus/releases/tag/v7.8.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v7.8.0/total)](https://github.com/cwtickle/danoniplus/archive/v7.8.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v7.8.0/support/v7?style=social)](https://github.com/cwtickle/danoniplus/compare/v7.8.0...support/v7#files_bucket)
- ⭐️ 楽曲の終了判定タイミングを秒単位からフレーム単位に変更 ( PR [#402](https://github.com/cwtickle/danoniplus/pull/402) )
- ⭐️ リザルトモーションをDisplay:Backgroundと連動してON/OFFする仕様に変更 ( PR [#402](https://github.com/cwtickle/danoniplus/pull/402) )
- ⭐️ リザルトモーションを譜面データ同様に、毎回読み直す仕様に変更 ( PR [#402](https://github.com/cwtickle/danoniplus/pull/402) )
- 🛠️ 設定画面で設定したAdjustmentを楽曲終了位置に加算するように変更 ( PR [#402](https://github.com/cwtickle/danoniplus/pull/402) )
- 🛠️ リザルトモーションで0フレームを指定した場合に、結果画面が一瞬表示される事象を修正 ( PR [#402](https://github.com/cwtickle/danoniplus/pull/402) )
- 🛠️ 結果画面のDisplay表示処理の見直し ( PR [#402](https://github.com/cwtickle/danoniplus/pull/402) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v7.8.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v7.7.1...v7.8.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v7.8.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v7.8.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v7.8.0.tar.gz) )<br>❤️ ショウタ

## v7.7.1 ([2019-08-25](https://github.com/cwtickle/danoniplus/releases/tag/v7.7.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v7.7.1/total)](https://github.com/cwtickle/danoniplus/archive/v7.7.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v7.7.1/support/v7?style=social)](https://github.com/cwtickle/danoniplus/compare/v7.7.1...support/v7#files_bucket)
- 🐞 複数譜面時に2譜面目以降（初期選択譜面以外）が読み込めない問題を修正 ( PR [#400](https://github.com/cwtickle/danoniplus/pull/400) ) <- :boom: [**v7.7.0**](Changelog-v7#v770-2019-08-25)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v7.7.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v7.7.0...v7.7.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v7.7.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v7.7.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v7.7.1.tar.gz) )

## v7.7.0 ([2019-08-25](https://github.com/cwtickle/danoniplus/releases/tag/v7.7.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v7.7.0/total)](https://github.com/cwtickle/danoniplus/archive/v7.7.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v7.7.0/support/v7?style=social)](https://github.com/cwtickle/danoniplus/compare/v7.7.0...support/v7#files_bucket)
- ⭐️ 譜面読込画面に入ったときに譜面データを再読込できるように変更 ( PR [#397](https://github.com/cwtickle/danoniplus/pull/397) )
- ⭐️ 歌詞表示・背景/マスクモーションにおいて同一フレームの複数同時描画に対応 ( PR [#398](https://github.com/cwtickle/danoniplus/pull/398) )
- ⭐️ 背景/マスクモーションにおいて、深度に「ALL」を指定することで、全てのオブジェクトを一斉クリアできるように対応 ( PR [#398](https://github.com/cwtickle/danoniplus/pull/398) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v7.7.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v7.6.0...v7.7.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v7.7.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v7.7.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v7.7.0.tar.gz) )

## v7.6.0 ([2019-08-18](https://github.com/cwtickle/danoniplus/releases/tag/v7.6.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v7.6.0/total)](https://github.com/cwtickle/danoniplus/archive/v7.6.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v7.6.0/support/v7?style=social)](https://github.com/cwtickle/danoniplus/compare/v7.6.0...support/v7#files_bucket)
- ⭐️ タイトル・リザルトモーションのジャンプで確率でジャンプ先を変えられるように変更 ( PR [#393](https://github.com/cwtickle/danoniplus/pull/393), Issue [#392](https://github.com/cwtickle/danoniplus/pull/392) )
- ⭐️ サンプル用HTMLを新規に作成 ( PR [#394](https://github.com/cwtickle/danoniplus/pull/394) )
- 🐞 タイトル・リザルトモーションでループカウンターが機能していない問題を修正 ( PR [#393](https://github.com/cwtickle/danoniplus/pull/393) ) <- :boom: [**v6.3.0**](Changelog-v6#v630-2019-06-27)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v7.6.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v7.5.1...v7.6.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v7.6.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v7.6.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v7.6.0.tar.gz) )<br>❤️ ショウタ

## v7.5.1 ([2019-08-03](https://github.com/cwtickle/danoniplus/releases/tag/v7.5.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v7.5.1/total)](https://github.com/cwtickle/danoniplus/archive/v7.5.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v7.5.1/support/v7?style=social)](https://github.com/cwtickle/danoniplus/compare/v7.5.1...support/v7#files_bucket)
- 🛠️ back_data, mask_dataで0フレームが指定された場合、ステップゾーンやライフゲージの表示より早く表示されるように変更 ( PR [#388](https://github.com/cwtickle/danoniplus/pull/388) )
- 🐞 back_data, mask_dataで0フレームが指定された場合に動作しないことがある問題を修正 ( PR [#388](https://github.com/cwtickle/danoniplus/pull/388) ) <- :boom: [**v4.2.0**](Changelog-v4#v420-2019-04-30), [**v0.65.x**](Changelog-v0#v065x-2018-11-16)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v7.5.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v7.5.0...v7.5.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v7.5.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v7.5.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v7.5.1.tar.gz) )<br>❤️ ショウタ

## v7.5.0 ([2019-07-29](https://github.com/cwtickle/danoniplus/releases/tag/v7.5.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v7.5.0/total)](https://github.com/cwtickle/danoniplus/archive/v7.5.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v7.5.0/support/v7?style=social)](https://github.com/cwtickle/danoniplus/compare/v7.5.0...support/v7#files_bucket)
- ⭐️ 譜面ヘッダー：preloadImages（事前読み込み画像）の複数まとめ記述の実装 ( PR [#386](https://github.com/cwtickle/danoniplus/pull/386) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v7.5.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v7.4.0...v7.5.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v7.5.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v7.5.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v7.5.0.tar.gz) )<br>❤️ izkdic

## v7.4.0 ([2019-07-21](https://github.com/cwtickle/danoniplus/releases/tag/v7.4.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v7.4.0/total)](https://github.com/cwtickle/danoniplus/archive/v7.4.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v7.4.0/support/v7?style=social)](https://github.com/cwtickle/danoniplus/compare/v7.4.0...support/v7#files_bucket)
- ⭐️ 初期矢印色・初期フリーズアロー色について、カラーコードの左パディング機能を実装 ( PR [#383](https://github.com/cwtickle/danoniplus/pull/383) )
- ⭐️ F5キーのキーブロックを解除 ( PR [#383](https://github.com/cwtickle/danoniplus/pull/383) )
- ⭐️ リザルト画面へ移行後に楽曲フェードアウトできるように変更 ( PR [#384](https://github.com/cwtickle/danoniplus/pull/384) )
- 🐞 リザルト画面で、フェードアウト中にTitleBack/Retryするとフェードアウト状態が引き継がれてしまう問題を修正 ( PR [#384](https://github.com/cwtickle/danoniplus/pull/384) ) <- :boom: [**v0.68.x**](Changelog-v0#v068x-2018-11-17)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v7.4.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v7.3.1...v7.4.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v7.4.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v7.4.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v7.4.0.tar.gz) )<br>❤️ ぷろろーぐ (@prlg25)

## v7.3.1 ([2019-07-21](https://github.com/cwtickle/danoniplus/releases/tag/v7.3.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v7.3.1/total)](https://github.com/cwtickle/danoniplus/archive/v7.3.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v7.3.1/support/v7?style=social)](https://github.com/cwtickle/danoniplus/compare/v7.3.1...support/v7#files_bucket)
- 🐞 Display画面へ遷移したときにボタンアニメーションが解除されない問題を修正 ( PR [#381](https://github.com/cwtickle/danoniplus/pull/381) ) <- :boom: [**v7.3.0**](Changelog-v7#v730-2019-07-20)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v7.3.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v7.3.0...v7.3.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v7.3.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v7.3.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v7.3.1.tar.gz) )

## v7.3.0 ([2019-07-20](https://github.com/cwtickle/danoniplus/releases/tag/v7.3.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v7.3.0/total)](https://github.com/cwtickle/danoniplus/archive/v7.3.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v7.3.0/support/v7?style=social)](https://github.com/cwtickle/danoniplus/compare/v7.3.0...support/v7#files_bucket)
- ⭐️ キーコンフィグ画面のボタン配置を見直し ( PR [#377](https://github.com/cwtickle/danoniplus/pull/377) )
  - キーコンフィグ画面におけるボタンアニメーションを廃止
  - キーコンフィグパターン変更について逆回しボタンを追加
  - 現在選択しているキーコンフィグパターンと、別キーモード状態を表示するように変更
- ⭐️ デフォルトで選択状態にする譜面をURLで指定できる機能を実装 ( PR [#378](https://github.com/cwtickle/danoniplus/pull/378) )
- ⭐️ 色変化の過去バージョン互換対応 ( PR [#379](https://github.com/cwtickle/danoniplus/pull/379) )
- 🐞 特殊キーを複数譜＆複数キー指定していた場合で、片方のキーのみキーコンフィグ設定を保存していた場合に、もう片方のキーのキーコンフィグが表示できないことがある問題を修正 ( PR [#377](https://github.com/cwtickle/danoniplus/pull/377) ) <- :boom: [**v6.6.0**](Changelog-v6#v660-2019-07-08)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v7.3.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v7.2.0...v7.3.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v7.3.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v7.3.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v7.3.0.tar.gz) )<br>❤️ goe (@goe0), izkdic

## v7.2.0 ([2019-07-15](https://github.com/cwtickle/danoniplus/releases/tag/v7.2.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v7.2.0/total)](https://github.com/cwtickle/danoniplus/archive/v7.2.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v7.2.0/support/v7?style=social)](https://github.com/cwtickle/danoniplus/compare/v7.2.0...support/v7#files_bucket)
- ⭐️ 画面遷移用ボタンにアニメーションを導入 ( PR [#375](https://github.com/cwtickle/danoniplus/pull/375) )
- ⭐️ Escキーをキーコンフィグ割り当てキーから除外 ( PR [#375](https://github.com/cwtickle/danoniplus/pull/375) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v7.2.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v7.1.1...v7.2.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v7.2.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v7.2.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v7.2.0.tar.gz) )

## v7.1.1 ([2019-07-12](https://github.com/cwtickle/danoniplus/releases/tag/v7.1.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v7.1.1/total)](https://github.com/cwtickle/danoniplus/archive/v7.1.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v7.1.1/support/v7?style=social)](https://github.com/cwtickle/danoniplus/compare/v7.1.1...support/v7#files_bucket)
- 🐞 矢印・フリーズアローのCSSモーションがデフォルトOFFになっていた問題を修正 ( PR [#374](https://github.com/cwtickle/danoniplus/pull/374) ) <- :boom: [**v7.1.0**](Changelog-v7#v710-2019-07-12)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v7.1.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v7.1.0...v7.1.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v7.1.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v7.1.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v7.1.1.tar.gz) )

## v7.1.0 ([2019-07-12](https://github.com/cwtickle/danoniplus/releases/tag/v7.1.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v7.1.0/total)](https://github.com/cwtickle/danoniplus/archive/v7.1.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v7.1.0/support/v7?style=social)](https://github.com/cwtickle/danoniplus/compare/v7.1.0...support/v7#files_bucket)
- ⭐️ フリーズアローヒット時の個別色変化のタイミングを変更 ( PR [#369](https://github.com/cwtickle/danoniplus/pull/369) , Issue [#354](https://github.com/cwtickle/danoniplus/pull/354) )
- ⭐️ ボタン色の見直し 及び ボタン色を個別指定できるように変更 ( PR [#370](https://github.com/cwtickle/danoniplus/pull/370) )
- ⭐️ 矢印・フリーズアローのCSSモーションのON/OFFボタンを追加 ( PR [#371](https://github.com/cwtickle/danoniplus/pull/371) )
- ⭐️ 歌詞表示（word_data）の3番目の要素を省略できるように変更（4番目未指定の場合）( PR [#372](https://github.com/cwtickle/danoniplus/pull/372) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v7.1.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v7.0.0...v7.1.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v7.1.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v7.1.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v7.1.0.tar.gz) )<br>❤️ ショウタ

## v7.0.0 ([2019-07-08](https://github.com/cwtickle/danoniplus/releases/tag/v7.0.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v7.0.0/total)](https://github.com/cwtickle/danoniplus/archive/v7.0.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v7.0.0/support/v7?style=social)](https://github.com/cwtickle/danoniplus/compare/v7.0.0...support/v7#files_bucket)
- ⭐️ 矢印・フリーズアローにCSSモーション機能を実装 ( PR [#367](https://github.com/cwtickle/danoniplus/pull/367) )
- ⭐️ キーブロック対象にEscapeキーを追加 ( PR [#368](https://github.com/cwtickle/danoniplus/pull/368) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v7.0.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v6.6.0...v7.0.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v7.0.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v7.0.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v7.0.0.tar.gz) )

[**<- v8**](Changelog-v8) | **v7** | [**v6 ->**](Changelog-v6)
