⭐ New Features / 🛠️ Improvements / 🐞 Bug Fixes

[**<- v4**](Changelog-v4) | **v3** | [**v2 ->**](Changelog-v2)  
(**🔖 [26 Releases](https://github.com/cwtickle/danoniplus/releases?q=tag%3Av3)** )

## [Warning] 更新終了後の不具合情報
- [更新を終了したバージョンの不具合情報#v3](DeprecatedVersionBugs#v3) を参照

## v3.13.9 ([2019-05-31](https://github.com/cwtickle/danoniplus/releases/tag/v3.13.9))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v3.13.9/total)](https://github.com/cwtickle/danoniplus/archive/v3.13.9.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v3.13.9/support/v3?style=social)](https://github.com/cwtickle/danoniplus/compare/v3.13.9...support/v3#files_bucket)
- 🐞 判定基準位置が1フレーム手前になっている問題を修正 ( PR [#318](https://github.com/cwtickle/danoniplus/pull/318), [#323](https://github.com/cwtickle/danoniplus/pull/323) ) <- :boom: **initial**

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v3.13.9)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v3.13.5...v3.13.9#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v3.13.9)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v3.13.9.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v3.13.9.tar.gz) )
/ 🎣 [**v5.6.6**](./Changelog-v5#v566-2019-05-31)

## v3.13.5 ([2019-05-26](https://github.com/cwtickle/danoniplus/releases/tag/v3.13.5))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v3.13.5/total)](https://github.com/cwtickle/danoniplus/archive/v3.13.5.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v3.13.5/support/v3?style=social)](https://github.com/cwtickle/danoniplus/compare/v3.13.5...support/v3#files_bucket)
- 🐞 譜面ヘッダー：titlesizeを64以上に指定すると文字欠けが起こる問題を修正 ( PR [#308](https://github.com/cwtickle/danoniplus/pull/308) ) <- :boom: [**v3.5.0**](Changelog-v3#v350-2019-03-23)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v3.13.5)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v3.13.4...v3.13.5#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v3.13.5)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v3.13.5.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v3.13.5.tar.gz) )
/ 🎣 [**v5.2.2**](./Changelog-v5#v522-2019-05-26)

## v3.13.4 ([2019-05-21](https://github.com/cwtickle/danoniplus/releases/tag/v3.13.4))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v3.13.4/total)](https://github.com/cwtickle/danoniplus/archive/v3.13.4.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v3.13.4/support/v3?style=social)](https://github.com/cwtickle/danoniplus/compare/v3.13.4...support/v3#files_bucket)
- 🐞 back_dataのフレーム数が小さすぎる場合に画面が止まる問題を修正 ( PR [#305](https://github.com/cwtickle/danoniplus/pull/305) ) <- :boom: [**v2.4.0**](Changelog-v2#v240-2019-02-08) 

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v3.13.4)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v3.13.3...v3.13.4#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v3.13.4)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v3.13.4.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v3.13.4.tar.gz) )
/ 🎣 [**v5.2.1**](./Changelog-v5#v521-2019-05-21)

## v3.13.3 ([2019-05-11](https://github.com/cwtickle/danoniplus/releases/tag/v3.13.3))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v3.13.3/total)](https://github.com/cwtickle/danoniplus/archive/v3.13.3.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v3.13.3/support/v3?style=social)](https://github.com/cwtickle/danoniplus/compare/v3.13.3...support/v3#files_bucket)
- 🐞 ライフ回復・ダメージ量計算でフリーズアローのカウントが間違っていたのを修正 ( PR [#284](https://github.com/cwtickle/danoniplus/pull/284) ) <- :boom: [**v0.72.x**](Changelog-v0#v072x-2018-11-20)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v3.13.3)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v3.13.2...v3.13.3#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v3.13.3)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v3.13.3.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v3.13.3.tar.gz) )
/ 🎣 [**v4.8.1**](./Changelog-v4#v481-2019-05-11)

## v3.13.2 ([2019-04-27](https://github.com/cwtickle/danoniplus/releases/tag/v3.13.2))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v3.13.2/total)](https://github.com/cwtickle/danoniplus/archive/v3.13.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v3.13.2/support/v3?style=social)](https://github.com/cwtickle/danoniplus/compare/v3.13.2...support/v3#files_bucket)
- 🛠️ back_dataでX/Y座標の整数値制限を緩和（小数が使えるように）( PR [#254](https://github.com/cwtickle/danoniplus/pull/254) ) 
- 🐞 back_dataでOpacityが機能していない問題を修正 ( PR [#254](https://github.com/cwtickle/danoniplus/pull/254) ) <- :boom: [**v0.65.x**](Changelog-v0#v065x-2018-11-16)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v3.13.2)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v3.13.1...v3.13.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v3.13.2)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v3.13.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v3.13.2.tar.gz) )
/ 🎣 [**v4.0.1**](./Changelog-v4#v401-2019-04-27)

----

## v3.13.1 ([2019-04-21](https://github.com/cwtickle/danoniplus/releases/tag/v3.13.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v3.13.1/total)](https://github.com/cwtickle/danoniplus/archive/v3.13.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v3.13.1/support/v3?style=social)](https://github.com/cwtickle/danoniplus/compare/v3.13.1...support/v3#files_bucket)
- 🐞 startFrameが指定されないときにプレイ画面で止まる問題を修正 ( PR [#250](https://github.com/cwtickle/danoniplus/pull/250) ) <- :boom: [**v3.11.0**](Changelog-v3#v3110-2019-04-17)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v3.13.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v3.13.0...v3.13.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v3.13.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v3.13.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v3.13.1.tar.gz) )<br>❤️ すずめ (@suzme)

## v3.13.0 ([2019-04-21](https://github.com/cwtickle/danoniplus/releases/tag/v3.13.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v3.13.0/total)](https://github.com/cwtickle/danoniplus/archive/v3.13.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v3.13.0/support/v3?style=social)](https://github.com/cwtickle/danoniplus/compare/v3.13.0...support/v3#files_bucket)
- 🛠️ ColorTypeがDefault以外の場合、Display:ColorをOFFにするよう修正 ( PR [#247](https://github.com/cwtickle/danoniplus/pull/247) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v3.13.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v3.12.0...v3.13.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v3.13.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v3.13.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v3.13.0.tar.gz) )

## v3.12.0 ([2019-04-21](https://github.com/cwtickle/danoniplus/releases/tag/v3.12.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v3.12.0/total)](https://github.com/cwtickle/danoniplus/archive/v3.12.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v3.12.0/support/v3?style=social)](https://github.com/cwtickle/danoniplus/compare/v3.12.0...support/v3#files_bucket)
- ⭐️ キーコンフィグ画面で作品毎の矢印色ではない、
共通デフォルト色を指定できるように変更 (ColorTypeの追加) ( PR [#245](https://github.com/cwtickle/danoniplus/pull/245) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v3.12.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v3.11.1...v3.12.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v3.12.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v3.12.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v3.12.0.tar.gz) )<br>❤️ すずめ (@suzme)

## v3.11.1 ([2019-04-17](https://github.com/cwtickle/danoniplus/releases/tag/v3.11.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v3.11.1/total)](https://github.com/cwtickle/danoniplus/archive/v3.11.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v3.11.1/support/v3?style=social)](https://github.com/cwtickle/danoniplus/compare/v3.11.1...support/v3#files_bucket)
- 🐞 画像ファイル読込(preloadImages)時にjsエラーになる問題を修正 ( PR [#244](https://github.com/cwtickle/danoniplus/pull/244) ) <- :boom: [**v0.67.x**](Changelog-v0#v067x-2018-11-17)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v3.11.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v3.11.0...v3.11.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v3.11.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v3.11.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v3.11.1.tar.gz) )

## v3.11.0 ([2019-04-17](https://github.com/cwtickle/danoniplus/releases/tag/v3.11.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v3.11.0/total)](https://github.com/cwtickle/danoniplus/archive/v3.11.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v3.11.0/support/v3?style=social)](https://github.com/cwtickle/danoniplus/compare/v3.11.0...support/v3#files_bucket)
- ⭐️ 全ての矢印を判定させていれば、フェードイン位置によらずクリアと見做すように変更 ( PR [#242](https://github.com/cwtickle/danoniplus/pull/242) ) 
- ⭐️ 譜面ヘッダー「startFrame」について、譜面毎に設定できるように変更 ( PR [#242](https://github.com/cwtickle/danoniplus/pull/242) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v3.11.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v3.10.0...v3.11.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v3.11.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v3.11.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v3.11.0.tar.gz) )

## v3.10.0 ([2019-04-17](https://github.com/cwtickle/danoniplus/releases/tag/v3.10.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v3.10.0/total)](https://github.com/cwtickle/danoniplus/archive/v3.10.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v3.10.0/support/v3?style=social)](https://github.com/cwtickle/danoniplus/compare/v3.10.0...support/v3#files_bucket)
- ⭐️ 複数譜面で歌詞表示（wordX_data）が無い場合、自動的に1譜面目のword_dataも見るように変更 ( PR [#240](https://github.com/cwtickle/danoniplus/pull/240) ) 
- ⭐️ 速度変化、色変化、歌詞表示のセット毎改行区切りに対応 ( PR [#240](https://github.com/cwtickle/danoniplus/pull/240) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v3.10.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v3.9.0...v3.10.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v3.10.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v3.10.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v3.10.0.tar.gz) )

## v3.9.0 ([2019-04-14](https://github.com/cwtickle/danoniplus/releases/tag/v3.9.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v3.9.0/total)](https://github.com/cwtickle/danoniplus/archive/v3.9.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v3.9.0/support/v3?style=social)](https://github.com/cwtickle/danoniplus/compare/v3.9.0...support/v3#files_bucket)
- ⭐️ パターン付きのキー指定（DP, 9A-2など）を自動修正するように変更 ( PR [#238](https://github.com/cwtickle/danoniplus/pull/238) ) 
- ⭐️ CSSモーションパターンを追加し、デフォルトの曲名表示に対してモーションを指定 ( PR [#239](https://github.com/cwtickle/danoniplus/pull/239) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v3.9.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v3.8.0...v3.9.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v3.9.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v3.9.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v3.9.0.tar.gz) )<br>❤️ すずめ (@suzme)

## v3.8.0 ([2019-04-13](https://github.com/cwtickle/danoniplus/releases/tag/v3.8.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v3.8.0/total)](https://github.com/cwtickle/danoniplus/archive/v3.8.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v3.8.0/support/v3?style=social)](https://github.com/cwtickle/danoniplus/compare/v3.8.0...support/v3#files_bucket)
- 🛠️ タイトル画面のカスタム部分の表示深度が手前になるように変更 ( PR [#237](https://github.com/cwtickle/danoniplus/pull/237) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v3.8.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v3.7.0...v3.8.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v3.8.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v3.8.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v3.8.0.tar.gz) )

## v3.7.0 ([2019-04-06](https://github.com/cwtickle/danoniplus/releases/tag/v3.7.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v3.7.0/total)](https://github.com/cwtickle/danoniplus/archive/v3.7.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v3.7.0/support/v3?style=social)](https://github.com/cwtickle/danoniplus/compare/v3.7.0...support/v3#files_bucket)
- 🛠️ カスタムデザイン（背景）の使用について、メインとそれ以外で設定を分離 ( PR [#234](https://github.com/cwtickle/danoniplus/pull/234) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v3.7.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v3.6.0...v3.7.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v3.7.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v3.7.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v3.7.0.tar.gz) )

## v3.6.0 ([2019-04-03](https://github.com/cwtickle/danoniplus/releases/tag/v3.6.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v3.6.0/total)](https://github.com/cwtickle/danoniplus/archive/v3.6.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v3.6.0/support/v3?style=social)](https://github.com/cwtickle/danoniplus/compare/v3.6.0...support/v3#files_bucket)
- ⭐️ 設定ファイル ( danoni_setting.js )のデフォルト値を指定 ( PR [#232](https://github.com/cwtickle/danoniplus/pull/232) ) 
- ⭐️ カスタムデザインの有無を設定ファイルで一律指定できるように変更 ( PR [#232](https://github.com/cwtickle/danoniplus/pull/232) )
- 🐞 総ノート数が0のときスコアがNaNになる不具合の修正 <- :boom: **initial**

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v3.6.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v3.5.0...v3.6.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v3.6.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v3.6.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v3.6.0.tar.gz) )<br>❤️ すずめ (@suzme), MFV2 (@MFV2)

## v3.5.0 ([2019-03-23](https://github.com/cwtickle/danoniplus/releases/tag/v3.5.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v3.5.0/total)](https://github.com/cwtickle/danoniplus/archive/v3.5.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v3.5.0/support/v3?style=social)](https://github.com/cwtickle/danoniplus/compare/v3.5.0...support/v3#files_bucket)
- ⭐️ 曲名タイトル（デフォルト）の複数行対応 ( PR [#229](https://github.com/cwtickle/danoniplus/pull/229) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v3.5.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v3.4.0...v3.5.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v3.5.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v3.5.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v3.5.0.tar.gz) )

## v3.4.0 ([2019-03-22](https://github.com/cwtickle/danoniplus/releases/tag/v3.4.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v3.4.0/total)](https://github.com/cwtickle/danoniplus/archive/v3.4.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v3.4.0/support/v3?style=social)](https://github.com/cwtickle/danoniplus/compare/v3.4.0...support/v3#files_bucket)
- 🛠️ 矢印毎にスクロール情報を保持するように変更 ( PR [#228](https://github.com/cwtickle/danoniplus/pull/228) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v3.4.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v3.3.1...v3.4.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v3.4.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v3.4.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v3.4.0.tar.gz) )

## v3.3.1 ([2019-03-16](https://github.com/cwtickle/danoniplus/releases/tag/v3.3.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v3.3.1/total)](https://github.com/cwtickle/danoniplus/archive/v3.3.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v3.3.1/support/v3?style=social)](https://github.com/cwtickle/danoniplus/compare/v3.3.1...support/v3#files_bucket)
- 🐞 12, 14key(Type1)のキーコンフィグ間違いを修正 ( PR [#227](https://github.com/cwtickle/danoniplus/pull/227) ) <- :boom: **initial**

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v3.3.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v3.3.0...v3.3.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v3.3.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v3.3.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v3.3.1.tar.gz) )

## v3.3.0 ([2019-03-15](https://github.com/cwtickle/danoniplus/releases/tag/v3.3.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v3.3.0/total)](https://github.com/cwtickle/danoniplus/archive/v3.3.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v3.3.0/support/v3?style=social)](https://github.com/cwtickle/danoniplus/compare/v3.3.0...support/v3#files_bucket)
- 🛠️ 17keyのデフォルト配置を横一列に変更 ( PR [#226](https://github.com/cwtickle/danoniplus/pull/226) )  
- 🛠️ Displayオプションのデザイン変更 ( PR [#225](https://github.com/cwtickle/danoniplus/pull/225) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v3.3.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v3.2.0...v3.3.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v3.3.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v3.3.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v3.3.0.tar.gz) )

## v3.2.0 ([2019-03-12](https://github.com/cwtickle/danoniplus/releases/tag/v3.2.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v3.2.0/total)](https://github.com/cwtickle/danoniplus/archive/v3.2.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v3.2.0/support/v3?style=social)](https://github.com/cwtickle/danoniplus/compare/v3.2.0...support/v3#files_bucket)
- ⭐️ タイトル画面の曲名にWebフォントが使用できるように変更 ( PR [#223](https://github.com/cwtickle/danoniplus/pull/223) ) 
- ⭐️ 曲名のグラデーション対応 ( PR [#223](https://github.com/cwtickle/danoniplus/pull/223) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v3.2.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v3.1.4...v3.2.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v3.2.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v3.2.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v3.2.0.tar.gz) )<br>❤️ MFV2 (@MFV2)

## v3.1.4 ([2019-03-01](https://github.com/cwtickle/danoniplus/releases/tag/v3.1.4))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v3.1.4/total)](https://github.com/cwtickle/danoniplus/archive/v3.1.4.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v3.1.4/support/v3?style=social)](https://github.com/cwtickle/danoniplus/compare/v3.1.4...support/v3#files_bucket)
- 🐞 追加キー定義でシャッフルグループが正しく読み込めない問題の修正 ( PR [#222](https://github.com/cwtickle/danoniplus/pull/222) ) <- :boom: [**v3.1.0**](Changelog-v3#v310-2019-02-26)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v3.1.4)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v3.1.3...v3.1.4#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v3.1.4)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v3.1.4.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v3.1.4.tar.gz) )<br>❤️ すずめ (@suzme)

## v3.1.3 ([2019-02-27](https://github.com/cwtickle/danoniplus/releases/tag/v3.1.3))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v3.1.3/total)](https://github.com/cwtickle/danoniplus/archive/v3.1.3.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v3.1.3/support/v3?style=social)](https://github.com/cwtickle/danoniplus/compare/v3.1.3...support/v3#files_bucket)
- 🐞 不正な歌詞データで画面が止まる不具合を修正 ( PR [#219](https://github.com/cwtickle/danoniplus/pull/219) ) <- :boom: [**v0.65.x**](Changelog-v0#v065x-2018-11-16)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v3.1.3)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v3.1.2...v3.1.3#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v3.1.3)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v3.1.3.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v3.1.3.tar.gz) )<br>❤️ すずめ (@suzme)

## v3.1.2 ([2019-02-26](https://github.com/cwtickle/danoniplus/releases/tag/v3.1.2))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v3.1.2/total)](https://github.com/cwtickle/danoniplus/archive/v3.1.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v3.1.2/support/v3?style=social)](https://github.com/cwtickle/danoniplus/compare/v3.1.2...support/v3#files_bucket)
- 🐞 ローディング100%で数秒止まる問題の修正 ( PR [#217](https://github.com/cwtickle/danoniplus/pull/217) ) <- :boom: [**v2.9.2**](Changelog-v2#v292-2019-02-25)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v3.1.2)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v3.1.1...v3.1.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v3.1.2)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v3.1.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v3.1.2.tar.gz) )<br>❤️ すずめ (@suzme)

## v3.1.1 ([2019-02-26](https://github.com/cwtickle/danoniplus/releases/tag/v3.1.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v3.1.1/total)](https://github.com/cwtickle/danoniplus/archive/v3.1.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v3.1.1/support/v3?style=social)](https://github.com/cwtickle/danoniplus/compare/v3.1.1...support/v3#files_bucket)
- 🛠️ Gaugeに対して製作者側で設定可否を指定できるように変更 ( PR [#212](https://github.com/cwtickle/danoniplus/pull/212) , [#216](https://github.com/cwtickle/danoniplus/pull/216) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v3.1.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v3.1.0...v3.1.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v3.1.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v3.1.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v3.1.1.tar.gz) )<br>❤️ izkdic

## v3.1.0 ([2019-02-26](https://github.com/cwtickle/danoniplus/releases/tag/v3.1.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v3.1.0/total)](https://github.com/cwtickle/danoniplus/archive/v3.1.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v3.1.0/support/v3?style=social)](https://github.com/cwtickle/danoniplus/compare/v3.1.0...support/v3#files_bucket)
- ⭐️ Shuffle機能（Mirror, Randomなど）を追加 ( PR [#209](https://github.com/cwtickle/danoniplus/pull/209) ) 
- ⭐️ Motion/Shuffle/AutoPlayに対して製作者側で設定可否を指定できるように変更 ( PR [#212](https://github.com/cwtickle/danoniplus/pull/212) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v3.1.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v3.0.0...v3.1.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v3.1.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v3.1.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v3.1.0.tar.gz) )<br>❤️ すずめ (@suzme), izkdic

## v3.0.0 ([2019-02-25](https://github.com/cwtickle/danoniplus/releases/tag/v3.0.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v3.0.0/total)](https://github.com/cwtickle/danoniplus/archive/v3.0.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v3.0.0/support/v3?style=social)](https://github.com/cwtickle/danoniplus/compare/v3.0.0...support/v3#files_bucket)
- 🛠️ 製作者別設定ファイルを追加 ( PR [#210](https://github.com/cwtickle/danoniplus/pull/210) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v3.0.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v2.9.3...v3.0.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v3.0.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v3.0.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v3.0.0.tar.gz) )<br>❤️ goe (@goe0)

[**<- v4**](Changelog-v4) | **v3** | [**v2 ->**](Changelog-v2)
