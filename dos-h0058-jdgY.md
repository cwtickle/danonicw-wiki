**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-h0058-jdgY) | Japanese** 

[Home](./) &gt; [譜面ヘッダー仕様](dos_header) &gt; [プレイ時の初期設定](dos_header#-プレイ時の初期設定) &gt; [プレイ画面位置の設定](dos_header#プレイ画面位置の設定)

| [<- stepYR](dos-h0049-stepYR) | **arrowJdgY / frzJdgY** | [jdgPosReset ->](dos-h0065-jdgPosReset) |

## arrowJdgY, frzJdgY
- 判定キャラクタのY座標位置の設定

### 使い方
```
|arrowJdgY=10|
|frzJdgY=10|
```
### 説明
判定キャラクタ(矢印、フリーズアロー)のY座標位置を、現在位置を基準に変更します。  
デフォルトはそれぞれ`0px`です。

<img src="https://user-images.githubusercontent.com/44026291/168832182-bbcaef19-da10-4852-9c41-d7358d3978ac.png" width="70%">


### 関連項目
- [displayUse](dos-h0057-displayUse) [:pencil:](dos-h0057-displayUse/_edit) Display項目の利用有無
- [stepY](dos-h0014-stepY) [:pencil:](dos-h0014-stepY/_edit) ステップゾーンのY座標位置
- [stepYR](dos-h0049-stepYR) [:pencil:](dos-h0049-stepYR/_edit) ステップゾーン(下)のY座標現位置からの差分
- [jdgPosReset](dos-h0065-jdgPosReset) [:pencil:](dos-h0065-jdgPosReset/_edit) 判定キャラクタ位置のリセット設定(Background:OFF時)

### 更新履歴

|Version|変更内容|
|----|----|
|[v11.0.0](https://github.com/cwtickle/danoniplus/releases/tag/v11.0.0)|・初回実装|

| [<- stepYR](dos-h0049-stepYR) | **arrowJdgY / frzJdgY** | [jdgPosReset ->](dos-h0065-jdgPosReset) |