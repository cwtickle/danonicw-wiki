**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-h0020-customFont) | Japanese** 

[Home](./) &gt; [譜面ヘッダー仕様](dos_header) &gt; [設定時の初期設定](dos_header#-設定時の初期設定)

| [<- keyGroupOrder](dos-h0092-keyGroupOrder) | **customFont** | [customGauge ->](dos-h0053-customGauge) |

## customFont
- 画面全般のフォント設定

### 使い方
```
|customFont=Fauna One|
```
### 説明
タイトル文字を除く、基本文字のフォントを指定します。  
表示できない文字は「Meiryo UI」「Sans-Serif」にて補完されます。    
[Google Fonts](https://fonts.google.com/)などのWebフォントを指定することをおススメします。  

<img src="https://user-images.githubusercontent.com/44026291/218242731-a8f5e3fd-4fb4-44ca-bece-1ba11cab5a5e.png" width="50%"><img src="https://user-images.githubusercontent.com/44026291/218242751-224c71bf-96de-4067-a63c-559fb8f5f830.png" width="50%">

### 関連項目
- [**titleFont**](dos-h0031-titlefont) [:pencil:](dos-h0031-titlefont/_edit) タイトルフォント

### 更新履歴

|Version|変更内容|
|----|----|
|[v1.12.0](https://github.com/cwtickle/danoniplus/releases/tag/v1.12.0)|・初回実装|

| [<- keyGroupOrder](dos-h0092-keyGroupOrder) | **customFont** | [customGauge ->](dos-h0053-customGauge) |