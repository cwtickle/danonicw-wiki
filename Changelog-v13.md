⭐ New Features / 🛠️ Improvements / 🐞 Bug Fixes  
( Files changed: ⚪ danoni_main.js / 🔵 danoni_constants.js / 🔴 css files )

[**<- v14**](Changelog-v14) | **v13** | [**v12 ->**](Changelog-v12)  
(**🔖 [16 Releases](https://github.com/cwtickle/danoniplus/releases?q=tag%3Av13)** )

## [Warning] 更新終了後の不具合情報
- [更新を終了したバージョンの不具合情報#v13](DeprecatedVersionBugs#v13) を参照

## v13.6.8 ([2020-07-02](https://github.com/cwtickle/danoniplus/releases/tag/v13.6.8))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v13.6.8/total)](https://github.com/cwtickle/danoniplus/archive/v13.6.8.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v13.6.8/support/v13?style=social)](https://github.com/cwtickle/danoniplus/compare/v13.6.8...support/v13#files_bucket) 
- 🛠️ バージョン比較用のリンクをSecurity Policyに変更 ( Issue [#759](https://github.com/cwtickle/danoniplus/pull/759), PR [#760](https://github.com/cwtickle/danoniplus/pull/760) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v13.6.8)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v13.6.7...v13.6.8#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v13.6.8)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v13.6.8.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v13.6.8.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v13.6.8/js/danoni_main.js)
/ 🎣 [**v15.5.0**](./Changelog-v15#v1530-2020-07-02)

## v13.6.7 ([2020-06-27](https://github.com/cwtickle/danoniplus/releases/tag/v13.6.7))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v13.6.7/total)](https://github.com/cwtickle/danoniplus/archive/v13.6.7.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v13.6.7/support/v13?style=social)](https://github.com/cwtickle/danoniplus/compare/v13.6.7...support/v13#files_bucket) 
- 🐞 カスタムゲージ使用時にクリア失敗時のリザルトモーションが使用できない問題を修正 ( Issue [#749](https://github.com/cwtickle/danoniplus/pull/749), PR [#751](https://github.com/cwtickle/danoniplus/pull/751) ) <- :boom: [**v9.4.0**](Changelog-v9#v940-2019-10-20)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v13.6.7)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v13.6.6...v13.6.7#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v13.6.7)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v13.6.7.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v13.6.7.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v13.6.7/js/danoni_main.js)
/ 🎣 [**v15.3.0**](./Changelog-v15#v1530-2020-06-27)

## v13.6.6 ([2020-06-21](https://github.com/cwtickle/danoniplus/releases/tag/v13.6.6))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v13.6.6/total)](https://github.com/cwtickle/danoniplus/archive/v13.6.6.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v13.6.6/support/v13?style=social)](https://github.com/cwtickle/danoniplus/compare/v13.6.6...support/v13#files_bucket) 
- 🛠️ プレイ中に他のキーイベントが呼ばれないようにするよう変更 ( PR [#741](https://github.com/cwtickle/danoniplus/pull/741) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v13.6.6)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v13.6.5...v13.6.6#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v13.6.6)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v13.6.6.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v13.6.6.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v13.6.6/js/danoni_main.js)
/ 🎣 [**v15.2.2**](./Changelog-v15#v1522-2020-06-21)

## v13.6.5 ([2020-06-18](https://github.com/cwtickle/danoniplus/releases/tag/v13.6.5))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v13.6.5/total)](https://github.com/cwtickle/danoniplus/archive/v13.6.5.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v13.6.5/support/v13?style=social)](https://github.com/cwtickle/danoniplus/compare/v13.6.5...support/v13#files_bucket) 
- 🐞 通常キーであらかじめデフォルトセットされているキーコンフィグパターンと異なるキーを割り当てた際、初回保存時のみそのキーがリセットされる問題を修正 ( PR [#738](https://github.com/cwtickle/danoniplus/pull/738) ) <- :boom: [**v6.6.0**](Changelog-v6#v660-2019-07-08)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v13.6.5)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v13.6.4...v13.6.5#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v13.6.5)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v13.6.5.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v13.6.5.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v13.6.5/js/danoni_main.js)
/ 🎣 [**v15.2.1**](./Changelog-v15#v1522-2020-06-18)

## v13.6.4 ([2020-05-24](https://github.com/cwtickle/danoniplus/releases/tag/v13.6.4))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v13.6.4/total)](https://github.com/cwtickle/danoniplus/archive/v13.6.4.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v13.6.4/support/v13?style=social)](https://github.com/cwtickle/danoniplus/compare/v13.6.4...support/v13#files_bucket) 
- 🐞 特殊キーの後に通常キーの譜面があると譜面選択できない問題を修正 ( PR [#729](https://github.com/cwtickle/danoniplus/pull/729) ) <- :boom: [**v4.6.2**](Changelog-v4#v462-2019-05-04)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v13.6.4)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v13.6.3...v13.6.4#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v13.6.4)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v13.6.4.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v13.6.4.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v13.6.4/js/danoni_main.js)
/ 🎣 [**v15.1.2**](./Changelog-v15#v1512-2020-05-24)

## v13.6.3 ([2020-05-13](https://github.com/cwtickle/danoniplus/releases/tag/v13.6.3))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v13.6.3/total)](https://github.com/cwtickle/danoniplus/archive/v13.6.3.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v13.6.3/support/v13?style=social)](https://github.com/cwtickle/danoniplus/compare/v13.6.3...support/v13#files_bucket) 
- 🛠️ バージョン比較用リンクを作成 ( PR [#711](https://github.com/cwtickle/danoniplus/pull/711) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v13.6.3)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v13.6.2...v13.6.3#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v13.6.3)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v13.6.3.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v13.6.3.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v13.6.3/js/danoni_main.js)
/ 🎣 [**v14.5.2**](./Changelog-v14#v1452-2020-05-13)

## v13.6.2 ([2020-05-04](https://github.com/cwtickle/danoniplus/releases/tag/v13.6.2))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v13.6.2/total)](https://github.com/cwtickle/danoniplus/archive/v13.6.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v13.6.2/support/v13?style=social)](https://github.com/cwtickle/danoniplus/compare/v13.6.2...support/v13#files_bucket) 
- 🐞 フリーズ始点判定有効時、シャキン以下の判定関数に差分フレーム数としてマイナス値が渡せていない問題を修正 ( PR [#684](https://github.com/cwtickle/danoniplus/pull/684) ) <- :boom: [**v5.7.0**](Changelog-v5#v570-2019-06-01)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v13.6.2)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v13.6.1...v13.6.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v13.6.2)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v13.6.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v13.6.2.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v13.6.2/js/danoni_main.js)
/ 🎣 [**v14.3.4**](./Changelog-v14#v1434-2020-05-04)

## v13.6.1 ([2020-04-29](https://github.com/cwtickle/danoniplus/releases/tag/v13.6.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v13.6.1/total)](https://github.com/cwtickle/danoniplus/archive/v13.6.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v13.6.1/support/v13?style=social)](https://github.com/cwtickle/danoniplus/compare/v13.6.1...support/v13#files_bucket) 
- 🐞 キーコンフィグ画面で、割り当てキーがundefinedとなる場合がある問題を修正 ( PR [#668](https://github.com/cwtickle/danoniplus/pull/668) ) <- :boom: [**v13.3.1**](Changelog-v13#v1331-2020-04-12)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v13.6.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v13.6.0...v13.6.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v13.6.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v13.6.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v13.6.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v13.6.1/js/danoni_main.js)
/ 🎣 [**v14.0.2**](./Changelog-v14#v1402-2020-04-29)

----

## v13.6.0 ([2020-04-25](https://github.com/cwtickle/danoniplus/releases/tag/v13.6.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v13.6.0/total)](https://github.com/cwtickle/danoniplus/archive/v13.6.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v13.6.0/support/v13?style=social)](https://github.com/cwtickle/danoniplus/compare/v13.6.0...support/v13#files_bucket) 
- ⭐ 開始/終了フレームなどを指定するstartFrame, fadeFrame, endFrameについて
疑似タイマー表記に対応 ( PR [#660](https://github.com/cwtickle/danoniplus/pull/660) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v13.6.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v13.5.1...v13.6.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v13.6.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v13.6.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v13.6.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v13.6.0/js/danoni_main.js)

## v13.5.1 ([2020-04-16](https://github.com/cwtickle/danoniplus/releases/tag/v13.5.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v13.5.1/total)](https://github.com/cwtickle/danoniplus/archive/v13.5.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v13.5.1/support/v13?style=social)](https://github.com/cwtickle/danoniplus/compare/v13.5.1...support/v13#files_bucket) 
- ⭐ 判定表示、Hidden+/Sudden+の境界線表示の透明度を調整する「Opacity」を実装 ( PR [#656](https://github.com/cwtickle/danoniplus/pull/656), [#658](https://github.com/cwtickle/danoniplus/pull/658) )
- ⭐ Hidden+, Sudden+で表示する境界線のDisplay設定(FilterLine)を追加 ( PR [#655](https://github.com/cwtickle/danoniplus/pull/655) )
- 🛠️ 厳密等価演算子が使用されていない箇所を修正 ( PR [#654](https://github.com/cwtickle/danoniplus/pull/654) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v13.5.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v13.4.0...v13.5.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v13.5.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v13.5.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v13.5.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v13.5.1/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v13.5.1/js/lib/danoni_constants.js)🔴

## v13.4.0 ([2020-04-15](https://github.com/cwtickle/danoniplus/releases/tag/v13.4.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v13.4.0/total)](https://github.com/cwtickle/danoniplus/archive/v13.4.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v13.4.0/support/v13?style=social)](https://github.com/cwtickle/danoniplus/compare/v13.4.0...support/v13#files_bucket) 
- ⭐ Hidden+とSudden+を同時適用する「Hid&Sud+」を実装 ( PR [#649](https://github.com/cwtickle/danoniplus/pull/649) )
- ⭐ Display:JudgmentをJudgment(判定キャラクタ)、FastSlow、Score(判定数)に分離 ( PR [#650](https://github.com/cwtickle/danoniplus/pull/650) )
- ⭐ Displayオプションのデフォルト無効化設定を追加 ( PR [#650](https://github.com/cwtickle/danoniplus/pull/650) )
- 🛠️ Hidden+, Sudden+適用時、適用するフィルターが片方しかない場合
もう片方のフィルターの境界線を非表示にするよう変更 ( PR [#649](https://github.com/cwtickle/danoniplus/pull/649) )
- 🛠️ Hidden+, Sudden+で使用するショートカットキーをDisplay画面に表示するよう変更 ( PR [#649](https://github.com/cwtickle/danoniplus/pull/649) )
- 🛠️ Shuffle機能を使用した場合のTweet表示を一部変更 ( PR [#649](https://github.com/cwtickle/danoniplus/pull/649) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v13.4.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v13.3.1...v13.4.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v13.4.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v13.4.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v13.4.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v13.4.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v13.4.0/js/lib/danoni_constants.js)<br>❤️ ★ぞろり★

## v13.3.1 ([2020-04-12](https://github.com/cwtickle/danoniplus/releases/tag/v13.3.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v13.3.1/total)](https://github.com/cwtickle/danoniplus/archive/v13.3.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v13.3.1/support/v13?style=social)](https://github.com/cwtickle/danoniplus/compare/v13.3.1...support/v13#files_bucket) 
- ⭐ 既定のキーに対して最低1つの代替キーを設定するよう変更 ( PR [#645](https://github.com/cwtickle/danoniplus/pull/645) )
- ⭐ フリーズアロー色の補完について、矢印色を優先してセットした場合の適用ルールを一部変更 ( PR [#644](https://github.com/cwtickle/danoniplus/pull/644) )
- 🐞 キーコンフィグ画面で代替キーの無効化ができない問題を修正 ( PR [#647](https://github.com/cwtickle/danoniplus/pull/647) ) <- :boom: [**v10.3.0**](Changelog-v10#v1030-2019-12-01)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v13.3.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v13.2.1...v13.3.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v13.3.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v13.3.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v13.3.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v13.3.1/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v13.3.1/js/lib/danoni_constants.js)<br>❤️ izkdic

## v13.2.1 ([2020-04-11](https://github.com/cwtickle/danoniplus/releases/tag/v13.2.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v13.2.1/total)](https://github.com/cwtickle/danoniplus/archive/v13.2.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v13.2.1/support/v13?style=social)](https://github.com/cwtickle/danoniplus/compare/v13.2.1...support/v13#files_bucket) 
- 🐞 設定画面（フェードイン表示）、キーコンフィグ画面（カラータイプ）のid重複を修正 ( PR [#642](https://github.com/cwtickle/danoniplus/pull/642) ) <- :boom: [**v3.12.0**](Changelog-v3#v3120-2019-04-21), [**v0.76.x**](Changelog-v0#v076x-2018-11-24)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v13.2.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v13.2.0...v13.2.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v13.2.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v13.2.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v13.2.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v13.2.1/js/danoni_main.js)<br>❤️ ショウタ

## v13.2.0 ([2020-04-04](https://github.com/cwtickle/danoniplus/releases/tag/v13.2.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v13.2.0/total)](https://github.com/cwtickle/danoniplus/archive/v13.2.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v13.2.0/support/v13?style=social)](https://github.com/cwtickle/danoniplus/compare/v13.2.0...support/v13#files_bucket) 
- ⭐ Appearance設定に「Hidden+」「Sudden+」を追加 ( PR [#640](https://github.com/cwtickle/danoniplus/pull/640) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v13.2.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v13.1.1...v13.2.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v13.2.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v13.2.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v13.2.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v13.2.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v13.2.0/js/lib/danoni_constants.js)<br>❤️ apoi

## v13.1.1 ([2020-03-30](https://github.com/cwtickle/danoniplus/releases/tag/v13.1.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v13.1.1/total)](https://github.com/cwtickle/danoniplus/archive/v13.1.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v13.1.1/support/v13?style=social)](https://github.com/cwtickle/danoniplus/compare/v13.1.1...support/v13#files_bucket) 
- ⭐ 従来のfrzColor補完ができる設定を追加 ( PR [#636](https://github.com/cwtickle/danoniplus/pull/636) )
- 🛠️ フリーズアロー(矢印)塗りつぶし色を6桁カラーコードで指定した場合、
透明度が100％になるように変更 ( PR [#636](https://github.com/cwtickle/danoniplus/pull/636) )
- 🛠️ danoni_setting(-template).js について、最終更新日を追加 ( PR [#637](https://github.com/cwtickle/danoniplus/pull/637) )
- 🛠️ 一部変数のconstをletに変更 ( PR [#638](https://github.com/cwtickle/danoniplus/pull/638) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v13.1.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v13.0.0...v13.1.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v13.1.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v13.1.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v13.1.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v13.1.1/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v13.1.1/js/lib/danoni_constants.js)<br>❤️ ショウタ

## v13.0.0 ([2020-03-29](https://github.com/cwtickle/danoniplus/releases/tag/v13.0.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v13.0.0/total)](https://github.com/cwtickle/danoniplus/archive/v13.0.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v13.0.0/support/v13?style=social)](https://github.com/cwtickle/danoniplus/compare/v13.0.0...support/v13#files_bucket) 
- ⭐ 矢印塗りつぶし部分の初期色に対してグラデーション他に対応 ( PR [#626](https://github.com/cwtickle/danoniplus/pull/626) )
- ⭐ フリーズアロー初期色 (frzColor)が未定義の場合、
矢印初期色 (setColor) の値から適用する形式に変更 ( PR [#633](https://github.com/cwtickle/danoniplus/pull/633) )
- ⭐ フリーズアロー(矢印)の塗りつぶし部分に対する初期色設定を追加 ( PR [#634](https://github.com/cwtickle/danoniplus/pull/634) )
- 🛠️ 自動グラデーション設定 (defaultColorgrd) が有効のとき、
フリーズアロー失敗時の色がグラデーションしないように変更 ( PR [#633](https://github.com/cwtickle/danoniplus/pull/633) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v13.0.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v12.3.1...v13.0.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v13.0.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v13.0.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v13.0.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v13.0.0/js/danoni_main.js)<br>❤️ ショウタ, izkdic

[**<- v14**](Changelog-v14) | **v13** | [**v12 ->**](Changelog-v12)
