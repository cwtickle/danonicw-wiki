[← オブジェクトリファレンスに戻る](ObjectReferenceIndex)  

# replaceStr
### 概要
- 文字列から指定文字列を検索し、置換する関数。
- [検索文字列, 置換文字列]の組を複数指定することが可能。

### 引数（括弧内はデフォルト値）

|引数|型|必須|指定内容|
|----|----|----|----|
|_str|string|*|入力文字列|
|_pairs|array2||[検索文字列, 置換文字列]を組とした二次元配列|

### 返却値
- 置換後の文字列

### 使用例
```javascript
const str = `A & B | C`;
const pairs = [[`&`, `and`], [`|`, `or`]];
console.log(replaceStr(str, pairs)); // -> `A and B or C`
```

### 更新履歴

|Version|変更内容|
|----|----|
|[v17.4.2](https://github.com/cwtickle/danoniplus/releases/tag/v17.4.2)|・初期実装|