# 7key
- 難易度表 :link: : [通常](http://dodl4.g3.xrea.com/normal7/) / [発狂](http://dodl4.g3.xrea.com/insane7/)
- メジャー多鍵データベース :link: : [7key](https://apoi108.sakura.ne.jp/danoni/danoni_all_list/?key=7)

<img src="https://raw.githubusercontent.com/wiki/cwtickle/danoniplus/wiki/keyimg/key7_01.png" width="40%">

## 概要
- おにぎりを中央に、左右に3つずつ並ぶキー。初出は**2005年**。

## キーパターン
- CW Editionでは、7ikey/12keyの別キーモードに対応。

|Pattern|Default Key Config (Ja/En)|
|----|----|
|1|<img src="https://github.com/cwtickle/danoniplus/assets/44026291/bb8c4428-51e1-4a48-967e-11137fcbef4d" width="40%">|
|2|<img src="https://github.com/cwtickle/danoniplus/assets/44026291/b079a8ae-e0da-428c-af77-94d408cf0faf" width="40%">|
|3<br>【7i】|<img src="https://github.com/cwtickle/danoniplus/assets/44026291/6b61b2fb-88c7-4879-bd94-6735ceb2533a" width="40%">|
|4<br>【12】|<img src="https://github.com/cwtickle/danoniplus/assets/44026291/fa15aff3-ce11-4028-82d3-b7a435928c97" width="50%">|
|5<br>【12】|<img src="https://github.com/cwtickle/danoniplus/assets/44026291/63c729fc-a098-44e5-a6ff-1047870e1fc5" width="50%">|
|6<br>【12】|<img src="https://github.com/cwtickle/danoniplus/assets/44026291/df96111d-ca56-4cb6-b258-d9a2e79101a5" width="50%">|
|7<br>【12】|<img src="https://github.com/cwtickle/danoniplus/assets/44026291/275934df-2843-492d-807a-3575904deea9" width="50%">|

## キー特有のオプション
### Scroll

|Pattern|Image|
|----|----|
|Cross|<img src="https://github.com/cwtickle/danoniplus/assets/44026291/638d1e1e-bc42-4f78-9ac3-d0a52f827dd1" width="50%">|
|Split|<img src="https://github.com/cwtickle/danoniplus/assets/44026291/37707ff2-eb28-4c0c-b4e3-a601837d8078" width="50%">|
|Alternate|<img src="https://github.com/cwtickle/danoniplus/assets/44026291/35dedf9a-1eb3-4eb3-9cff-a40c2f9a1adb" width="50%">|
|Twist|<img src="https://github.com/cwtickle/danoniplus/assets/44026291/54fc65d5-73d0-483f-94bb-87e7baad6bd1" width="50%">|
|Asymmetry|<img src="https://github.com/cwtickle/danoniplus/assets/44026291/23113dfa-96ea-4982-a356-386bb17edd6b" width="50%">|

