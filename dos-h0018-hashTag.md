**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-h0018-hashTag) | Japanese** 

[Home](./) &gt; [譜面ヘッダー仕様](dos_header) &gt; [クレジット関連、共通設定](dos_header#-クレジット関連共通設定)

| [<- makerView](dos-h0050-makerView) | **hashTag** | [releaseDate ->](dos-h0036-releaseDate) |

## hashTag
- ハッシュタグの設定

### 使い方
```
|hashTag=#festa7_18|
```
### 説明
イベント開催時にハッシュタグを付加したい場合に指定します。  
指定すると、Tweet用のリザルトコピーに付与されます。通常は不要です。 

### 関連項目
- [**tuning**](dos-h0017-tuning) [:pencil:](dos-h0017-tuning/_edit) 製作者クレジット
- [releaseDate](dos-h0036-releaseDate) [:pencil:](dos-h0036-releaseDate/_edit) 作品公開日

### 更新履歴

|Version|変更内容|
|----|----|
|[v1.0.0<br>(v0.41.2)](https://github.com/cwtickle/danoniplus/releases/tag/v1.0.1)|・初回実装|

| [<- makerView](dos-h0050-makerView) | **hashTag** | [releaseDate ->](dos-h0036-releaseDate) |