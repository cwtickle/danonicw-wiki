[← オブジェクトリファレンスに戻る](ObjectReferenceIndex)  

## g_stateObj

### 概要
- 各種設定の状態を管理するオブジェクト。初期化時に作成される。  
- 設定画面やDisplay画面で表示される設定の変更と連動して、g_stateObjも変化する。
```javascript
g_stateObj.speed = 7;
g_stateObj.motion = `Brake`;
```

### 生成タイミング
- 初期に生成される。  
ただし、設定を変更するとそれに応じて値が変わる。

## プロパティと初期値
### 譜面読込設定
- dosDivideFlg (boolean): false
    - 譜面の分割有無を設定。デフォルトは分割しない(false)。
- scoreLockFlg (boolean): false
    - 譜面番号を固定するかどうかの設定。デフォルトは固定しない（2譜面目は2、3譜面目は3、など）。

### 譜面番号
- scoreId (number): 0
    - 譜面番号。1譜面目は0, 2譜面目は1, 3譜面目は2のように指定。  
`scoreLockFlg`の値には左右されない。
- dummyId (string): ``
    - ダミーノーツとして割り当てた譜面に指定した番号。

### 全般設定
これらの設定は [g_settings](obj-v0025-g_settings) で設定されたものから選択する。
- speed (float): 3.5
- motion (string): `OFF`
- reverse (switch): `OFF`
- scroll (string): `---`
- shuffle (string): `OFF`
- autoPlay (string): `OFF`
- autoAll (switch): `OFF`
- gauge (string): `Normal`
- adjustment (number): 0
- fadein (number): 0
- volume (number): 100
- scoreDetail (string): `Speed`
    - 譜面明細画面で設定中の表示名。
- appearance (string): `Visible`
- filterLock (switch): `OFF`
    - Appearance の Hidden+/Sudden+ でプレイ中のカバーを固定するかどうかを設定。  
デフォルトはOFF(固定しない)。
- opacity (number): 100

### ライフゲージ設定
- lifeRcv (float): 2
- lifeDmg (float): 7
- lifeMode (string): `Border`
- lifeBorder (float): 70
- lifeInit (float): 25
- lifeVariable (switch): `OFF`

### カスタムキーの状態
- extraKeyFlg (boolean): false

### 設定画面上の見え方
- dataSaveFlg (boolean): true
- scoreDetailViewFlg (boolean): false

### ディスプレイ設定
- d_stepzone (switch): `ON`
- d_judgment (switch): `ON`
- d_fastslow (switch): `ON`
- d_lifegauge (switch): `ON`
- d_musicinfo (switch): `ON`
- d_score (switch): `ON`
- d_filterline (switch): `ON`
- d_color (switch): `ON`
- d_speed (switch): `ON`
- d_arroweffect (switch): `ON`
- d_lyrics (switch): `ON`
- d_background (switch): `ON`
- d_special (switch): `ON`

### Adjustmentの詳細設定
- realAdjustment (number) ※初期定義なし
- intAdjustment (number) ※初期定義なし
- decimalAdjustment (number) ※初期定義なし

### imgTypeごとの設定
項目詳細については [imgType](dos-h0082-imgType) もご覧ください。
- rotateEnabled (boolean): true
   - 画像回転有無。デフォルトはtrue(回転する)。
- flatStepHeight (number) : 50
   - Flat時ステップ間隔。デフォルトは50(矢印サイズ)。