⭐ New Features / 🛠️ Improvements / 🐞 Bug Fixes  
( Files changed: ⚪ danoni_main.js / 🔵 danoni_constants.js / 🔴 css files )

[**<- v13**](Changelog-v13) | **v12** | [**v11 ->**](Changelog-v11)  
(**🔖 [14 Releases](https://github.com/cwtickle/danoniplus/releases?q=tag%3Av12)** )

## [Warning] 更新終了後の不具合情報
- [更新を終了したバージョンの不具合情報#v12](DeprecatedVersionBugs#v12) を参照

## v12.3.6 ([2020-05-13](https://github.com/cwtickle/danoniplus/releases/tag/v12.3.6))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v12.3.6/total)](https://github.com/cwtickle/danoniplus/archive/v12.3.6.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v12.3.6/support/v12?style=social)](https://github.com/cwtickle/danoniplus/compare/v12.3.6...support/v12#files_bucket) 
- 🛠️ バージョン比較用リンクを作成 ( PR [#711](https://github.com/cwtickle/danoniplus/pull/711) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v12.3.6)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v12.3.5...v12.3.6#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v12.3.6)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v12.3.6.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v12.3.6.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v12.3.6/js/danoni_main.js)
/ 🎣 [**v14.5.2**](./Changelog-v14#v1452-2020-05-13)

## v12.3.5 ([2020-05-04](https://github.com/cwtickle/danoniplus/releases/tag/v12.3.5))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v12.3.5/total)](https://github.com/cwtickle/danoniplus/archive/v12.3.5.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v12.3.5/support/v12?style=social)](https://github.com/cwtickle/danoniplus/compare/v12.3.5...support/v12#files_bucket) 
- 🛠️ 警告メッセージを常に再作成し手前に来るよう変更 ( PR [#665](https://github.com/cwtickle/danoniplus/pull/665) )
- 🛠️ 警告メッセージエリアの最大長を設定し、超過した場合はスクロールするよう変更 ( PR [#670](https://github.com/cwtickle/danoniplus/pull/670) )
- 🐞 フリーズ始点判定有効時、シャキン以下の判定関数に差分フレーム数としてマイナス値が渡せていない問題を修正 ( PR [#684](https://github.com/cwtickle/danoniplus/pull/684) ) <- :boom: [**v5.7.0**](Changelog-v5#v570-2019-06-01)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v12.3.5)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v12.3.4...v12.3.5#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v12.3.5)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v12.3.5.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v12.3.5.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v12.3.5/js/danoni_main.js)
/ 🎣 [**v14.3.4**](./Changelog-v14#v1434-2020-05-04)

## v12.3.4 ([2020-04-16](https://github.com/cwtickle/danoniplus/releases/tag/v12.3.4))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v12.3.4/total)](https://github.com/cwtickle/danoniplus/archive/v12.3.4.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v12.3.4/support/v12?style=social)](https://github.com/cwtickle/danoniplus/compare/v12.3.4...support/v12#files_bucket) 
- 🛠️ 厳密等価演算子が使用されていない箇所を修正 ( PR [#654](https://github.com/cwtickle/danoniplus/pull/654) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v12.3.4)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v12.3.3...v12.3.4#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v12.3.4)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v12.3.4.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v12.3.4.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v12.3.4/js/danoni_main.js)
/ 🎣 [**v13.5.1**](./Changelog-v13#v1351-2020-04-16)

## v12.3.3 ([2020-04-12](https://github.com/cwtickle/danoniplus/releases/tag/v12.3.3))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v12.3.3/total)](https://github.com/cwtickle/danoniplus/archive/v12.3.3.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v12.3.3/support/v12?style=social)](https://github.com/cwtickle/danoniplus/compare/v12.3.3...support/v12#files_bucket) 
- 🐞 キーコンフィグ画面で代替キーの無効化ができない問題を修正 ( PR [#647](https://github.com/cwtickle/danoniplus/pull/647) ) <- :boom: [**v10.3.0**](Changelog-v10#v1030-2019-12-01)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v12.3.3)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v12.3.2...v12.3.3#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v12.3.3)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v12.3.3.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v12.3.3.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v12.3.3/js/danoni_main.js)
/ 🎣 [**v13.3.1**](./Changelog-v13#v1321-2020-04-12)

## v12.3.2 ([2020-04-11](https://github.com/cwtickle/danoniplus/releases/tag/v12.3.2))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v12.3.2/total)](https://github.com/cwtickle/danoniplus/archive/v12.3.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v12.3.2/support/v12?style=social)](https://github.com/cwtickle/danoniplus/compare/v12.3.2...support/v12#files_bucket) 
- 🐞 設定画面（フェードイン表示）、キーコンフィグ画面（カラータイプ）のid重複を修正 ( PR [#642](https://github.com/cwtickle/danoniplus/pull/642) ) <- :boom: [**v3.12.0**](Changelog-v3#v3120-2019-04-21), [**v0.76.x**](Changelog-v0#v076x-2018-11-24)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v12.3.2)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v12.3.1...v12.3.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v12.3.2)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v12.3.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v12.3.2.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v12.3.2/js/danoni_main.js)
/ 🎣 [**v13.2.1**](./Changelog-v13#v1321-2020-04-11)

----

## v12.3.1 ([2020-03-25](https://github.com/cwtickle/danoniplus/releases/tag/v12.3.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v12.3.1/total)](https://github.com/cwtickle/danoniplus/archive/v12.3.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v12.3.1/support/v12?style=social)](https://github.com/cwtickle/danoniplus/compare/v12.3.1...support/v12#files_bucket) 
- 🐞 htmlファイルを直接開いた場合、ギコの画像のみ表示されない問題を修正 ( PR [#630](https://github.com/cwtickle/danoniplus/pull/630) ) <- :boom: [**v12.3.0**](Changelog-v12#v1230-2020-03-22)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v12.3.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v12.3.0...v12.3.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v12.3.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v12.3.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v12.3.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v12.3.1/js/danoni_main.js)

## v12.3.0 ([2020-03-22](https://github.com/cwtickle/danoniplus/releases/tag/v12.3.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v12.3.0/total)](https://github.com/cwtickle/danoniplus/archive/v12.3.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v12.3.0/support/v12?style=social)](https://github.com/cwtickle/danoniplus/compare/v12.3.0...support/v12#files_bucket) 
- ⭐️ ローカルで直接htmlファイルを開いても画像が表示されるように変更 ( PR [#627](https://github.com/cwtickle/danoniplus/pull/627), [#628](https://github.com/cwtickle/danoniplus/pull/628) )
- 🛠️ Fast/Slowの表示をcustom関数の前に来るように修正 ( PR [#625](https://github.com/cwtickle/danoniplus/pull/625) )
- 🛠️ 読込部分のコード整理 ( PR [#587](https://github.com/cwtickle/danoniplus/pull/587) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v12.3.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v12.2.1...v12.3.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v12.3.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v12.3.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v12.3.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v12.3.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v12.3.0/js/lib/danoni_constants.js)<br>❤️ すずめ (@suzme)

## v12.2.1 ([2020-03-08](https://github.com/cwtickle/danoniplus/releases/tag/v12.2.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v12.2.1/total)](https://github.com/cwtickle/danoniplus/archive/v12.2.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v12.2.1/support/v12?style=social)](https://github.com/cwtickle/danoniplus/compare/v12.2.1...support/v12#files_bucket) 
- 🐞 ダミー矢印のヒット位置が前の通常矢印のヒット結果に引きずられる問題を修正 ( PR [#623](https://github.com/cwtickle/danoniplus/pull/623) ) <- :boom: [**v10.4.0**](Changelog-v10#v1040-2019-12-07)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v12.2.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v12.2.0...v12.2.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v12.2.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v12.2.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v12.2.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v12.2.1/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v12.2.1/js/lib/danoni_constants.js)<br>❤️ ★ぞろり★

## v12.2.0 ([2020-03-06](https://github.com/cwtickle/danoniplus/releases/tag/v12.2.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v12.2.0/total)](https://github.com/cwtickle/danoniplus/archive/v12.2.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v12.2.0/support/v12?style=social)](https://github.com/cwtickle/danoniplus/compare/v12.2.0...support/v12#files_bucket) 
- ⭐️ 7ikeyのScroll: Asymmetryについて、左矢印のスクロールを反転する仕様に変更 ( PR [#621](https://github.com/cwtickle/danoniplus/pull/621) )
- 🛠️ ゲージ詳細テーブルをtableタグからdiv要素に変更 ( PR [#620](https://github.com/cwtickle/danoniplus/pull/620) )
- 🛠️ タイトル・リザルトモーション周りのコードを修正 ( PR [#613](https://github.com/cwtickle/danoniplus/pull/613) )
- 🛠️ 本体cssファイルに更新日表記を追加 ( PR [#622](https://github.com/cwtickle/danoniplus/pull/622) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v12.2.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v12.1.2...v12.2.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v12.2.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v12.2.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v12.2.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v12.2.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v12.2.0/js/lib/danoni_constants.js)🔴

## v12.1.2 ([2020-02-28](https://github.com/cwtickle/danoniplus/releases/tag/v12.1.2))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v12.1.2/total)](https://github.com/cwtickle/danoniplus/archive/v12.1.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v12.1.2/support/v12?style=social)](https://github.com/cwtickle/danoniplus/compare/v12.1.2...support/v12#files_bucket) 
- 🐞 タイトルのグラデーション(titlegrd)が1行分だけ指定されているとき、曲名の2行目が表示されない問題を修正 ( PR [#618](https://github.com/cwtickle/danoniplus/pull/618) ) <- :boom: [**v12.0.0**](Changelog-v12#v1200-2020-02-09)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v12.1.2)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v12.1.1...v12.1.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v12.1.2)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v12.1.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v12.1.2.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v12.1.2/js/danoni_main.js)<br>❤️ すずめ (@suzme), MFV2 (@MFV2)

## v12.1.1 ([2020-02-25](https://github.com/cwtickle/danoniplus/releases/tag/v12.1.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v12.1.1/total)](https://github.com/cwtickle/danoniplus/archive/v12.1.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v12.1.1/support/v12?style=social)](https://github.com/cwtickle/danoniplus/compare/v12.1.1...support/v12#files_bucket) 
- 🐞 スコア上書き時ハイスコア差分が計算されない問題を修正 ( PR [#616](https://github.com/cwtickle/danoniplus/pull/616) ) <- :boom: [**v10.5.0**](Changelog-v10#v1050-2019-12-13)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v12.1.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v12.1.0...v12.1.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v12.1.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v12.1.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v12.1.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v12.1.1/js/danoni_main.js)<br>❤️ izkdic

## v12.1.0 ([2020-02-24](https://github.com/cwtickle/danoniplus/releases/tag/v12.1.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v12.1.0/total)](https://github.com/cwtickle/danoniplus/archive/v12.1.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v12.1.0/support/v12?style=social)](https://github.com/cwtickle/danoniplus/compare/v12.1.0...support/v12#files_bucket) 
- ⭐️ 矢印描画エリアを縮小する設定を追加 ( PR [#614](https://github.com/cwtickle/danoniplus/pull/614) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v12.1.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v12.0.2...v12.1.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v12.1.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v12.1.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v12.1.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v12.1.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v12.1.0/js/lib/danoni_constants.js)

## v12.0.2 ([2020-02-10](https://github.com/cwtickle/danoniplus/releases/tag/v12.0.2))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v12.0.2/total)](https://github.com/cwtickle/danoniplus/archive/v12.0.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v12.0.2/support/v12?style=social)](https://github.com/cwtickle/danoniplus/compare/v12.0.2...support/v12#files_bucket) 
- 🛠️ 譜面明細画面中のレベル表示で、3つ押しが無い場合に「None」を記載するように変更 ( PR [#608](https://github.com/cwtickle/danoniplus/pull/608) )
- 🐞 setColorの1・3番目に方向(to rightなど)が指定された場合で、titlearrowgrdが未指定の場合タイトルの背景矢印が表示されない問題を修正 ( PR [#606](https://github.com/cwtickle/danoniplus/pull/606) ) <- :boom: [**v12.0.0**](Changelog-v12#v1200-2020-02-09)
- 🐞 譜面明細用データを全て読み込んでからタイトルへ移動するよう変更 ( PR [#607](https://github.com/cwtickle/danoniplus/pull/607) ) <- :boom: [**v12.0.0**](Changelog-v12#v1200-2020-02-09)
- 🐞 レベル計算中の無効な値を対象にしないよう修正 ( PR [#608](https://github.com/cwtickle/danoniplus/pull/608) ) <- :boom: [**v12.0.0**](Changelog-v12#v1200-2020-02-09)
- 🐞 矢印グラデーションを利用時、KeyConfig画面でColorTypeを変更してDefaultに戻すと矢印色が適用できなくなる問題を修正 ( PR [#610](https://github.com/cwtickle/danoniplus/pull/610) ) <- :boom: [**v12.0.0**](Changelog-v12#v1200-2020-02-09)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v12.0.2)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v12.0.0...v12.0.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v12.0.2)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v12.0.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v12.0.2.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v12.0.2/js/danoni_main.js)<br>❤️ izkdic, MFV2 (@MFV2)

## v12.0.0 ([2020-02-09](https://github.com/cwtickle/danoniplus/releases/tag/v12.0.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v12.0.0/total)](https://github.com/cwtickle/danoniplus/archive/v12.0.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v12.0.0/support/v12?style=social)](https://github.com/cwtickle/danoniplus/compare/v12.0.0...support/v12#files_bucket) 
- ⭐️ 矢印色、フリーズアロー色のグラデーションを実装 ( PR [#601](https://github.com/cwtickle/danoniplus/pull/601) )
- ⭐️ 常時グラデーション設定の追加 ( PR [#601](https://github.com/cwtickle/danoniplus/pull/601) )
- ⭐️ タイトル曲名、背景矢印のグラデーション拡張を実装 ( Issue [#390](https://github.com/cwtickle/danoniplus/pull/390), PR [#601](https://github.com/cwtickle/danoniplus/pull/601) )
- ⭐️ レベル計算ツール++の移植 ( PR [#602](https://github.com/cwtickle/danoniplus/pull/602) )
- 🛠️ レベル値や譜面密度情報をHTML上で利用できるように、事前に読み込む仕様に変更 ( PR [#599](https://github.com/cwtickle/danoniplus/pull/599), [#600](https://github.com/cwtickle/danoniplus/pull/600) )
- 🛠️ データ読込関数(loadScript)について読込結果を取得できるように変更 ( PR [#600](https://github.com/cwtickle/danoniplus/pull/600) )
- 🐞 譜面番号固定時、リザルトモーションが反映されないことがある問題を修正 ( PR [#599](https://github.com/cwtickle/danoniplus/pull/599) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v12.0.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v11.4.0...v12.0.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v12.0.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v12.0.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v12.0.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v12.0.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v12.0.0/js/lib/danoni_constants.js)<br>❤️ MFV2 (@MFV2), ショウタ

[**<- v13**](Changelog-v13) | **v12** | [**v11 ->**](Changelog-v11)
