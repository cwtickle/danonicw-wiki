**[English](https://github.com/cwtickle/danoniplus-docs/wiki/AboutBaseDirectory) | Japanese** 

| [< スキンファイル仕様](./AboutSkin) | **基準ディレクトリ仕様** | [作品URLのクエリパラメーター仕様 >](./QueryParameter) |

# 基準ディレクトリ仕様
- 基準となるディレクトリ（フォルダ）は設定により異なります。主なものを以下にまとめました。
- なお、カレントディレクトリ指定`(..)`を入れれば、基本的に作品htmlのあるディレクトリに統一されます。  
※背景/マスクモーションで、`syncBackPath=false (デフォルト)`のときは使用できません。

|譜面ヘッダー/エフェクト名|基準ディレクトリ|
|----|----|
|customJs|(danoni_main.jsのあるディレクトリ)../js/|
|customCss|(danoni_main.jsのあるディレクトリ)../css/|
|skinType|(danoni_main.jsのあるディレクトリ)../skin/|
|settingType|(danoni_main.jsのあるディレクトリ)../js/|
|背景/マスクモーション|syncBachPath=false -> 作品htmlのあるディレクトリ<br>syncBachPath=true -> (danoni_main.jsのあるディレクトリ)../js/|

## 具体例

### danoni_main.jsの場所: /work/js/<br>作品ディレクトリ: /work/danoni/

|譜面ヘッダー/エフェクト名|基準ディレクトリ|カレントディレクトリ指定時|
|----|----|----|
|customJs|/work/js/|/work/danoni/|
|customCss|/work/css/|/work/danoni/|
|skinType|/work/skin/|/work/danoni/|
|settingType|/work/js/|/work/danoni/|

### danoni_main.jsの場所: /work/js/<br>作品ディレクトリ: /work/danoni/2/

|譜面ヘッダー/エフェクト名|基準ディレクトリ|カレントディレクトリ指定時|
|----|----|----|
|customJs|/work/js/|/work/danoni/2/|
|customCss|/work/css/|/work/danoni/2/|
|skinType|/work/skin/|/work/danoni/2/|
|settingType|/work/js/|/work/danoni/2/|

### danoni_main.jsの場所: cwtickle.github.io/danoniplus/js/<br>作品ディレクトリ: /work/danoni/
- danoni_main.jsがcwtickle.github.ioの場合、ver38.1.0以降は下記の通り作品ディレクトリに合わせて自動修正

|譜面ヘッダー/エフェクト名|基準ディレクトリ|カレントディレクトリ指定時|
|----|----|----|
|customJs|/work/js/|/work/danoni/|
|customCss|/work/css/|/work/danoni/|
|skinType|/work/skin/|/work/danoni/|
|settingType|/work/js/|/work/danoni/|

### danoni_main.jsの場所: cwtickle.github.io/danoniplus/js/<br>作品ディレクトリ: /work/danoni/2/

|譜面ヘッダー/エフェクト名|基準ディレクトリ|カレントディレクトリ指定時|
|----|----|----|
|customJs|/work/danoni/js/|/work/danoni/2/|
|customCss|/work/danoni/css/|/work/danoni/2/|
|skinType|/work/danoni/skin/|/work/danoni/2/|
|settingType|/work/danoni/js/|/work/danoni/2/|

## 関連項目
- [**customJs**](dos-h0019-customjs) [:pencil:](dos-h0019-customjs/_edit) カスタムjsファイルの指定
- [customCss](dos-h0086-customcss) [:pencil:](dos-h0086-customcss/_edit) カスタムcssファイルの指定
- [**settingType**](dos-h0056-settingType) [:pencil:](dos-h0056-settingType/_edit) 共通設定名
- [**skinType**](dos-h0054-skinType) [:pencil:](dos-h0054-skinType/_edit) スキン設定
- [背景・マスクモーション (back_data, mask_data)](dos-e0004-animationData) [:pencil:](dos-e0004-animationData/_edit) 

## 更新履歴

|Version|変更内容|
|----|----|
|[v38.1.0](https://github.com/cwtickle/danoniplus/releases/tag/v38.1.0)|・danoni_main.jsがcwtickle.github.ioにいる場合、作品ディレクトリに合わせて自動修正するよう変更|

| [< スキンファイル仕様](./AboutSkin) | **基準ディレクトリ仕様** | [作品URLのクエリパラメーター仕様 >](./QueryParameter) |
