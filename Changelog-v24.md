**[English](https://github.com/cwtickle/danoniplus-docs/wiki/Changelog-v24) | Japanese** 

[^ Back to All Version List (Security Policy)](https://github.com/cwtickle/danoniplus/security/policy)  
[^ Back to Update Info](./UpdateInfo#-v24-changelog)

[**<- v25**](Changelog-v25) | **v24** | [**v23 ->**](Changelog-v23)  
(**🔖 [29 Releases](https://github.com/cwtickle/danoniplus/releases?q=tag%3Av24)** )

## 🔃 Files changed (v24)

|Directory|FileName||Last Updated|
|----|----|----|----|
|/js|danoni_main.js|[📥](https://github.com/cwtickle/danoniplus/releases/download/v24.6.19/danoni_main.js)|**v24.6.19**|
|/js/lib|danoni_constants.js|[📥](https://github.com/cwtickle/danoniplus/releases/download/v24.6.16/danoni_constants.js)|[v24.6.16](./Changelog-v24#v24616-2023-03-04)|

<details>
<summary>Changed file list before v23</summary>

|Directory|FileName||Last Updated|
|----|----|----|----|
|/js/lib|danoni_localbinary.js|[📥](https://github.com/cwtickle/danoniplus/releases/download/v15.1.0/danoni_localbinary.js)|[v15.1.0](./Changelog-v15#v1510-2020-05-21)|
|/css|danoni_main.css|[📥](https://github.com/cwtickle/danoniplus/releases/download/v21.4.2/danoni_main.css)|[v21.4.2](Changelog-v21#v2142-2021-04-07)|
|/img|aaShadow.svg<br>arrow.svg<br>arrowShadow.svg<br>borderline.svg<br>c.svg<br>cursor.svg<br>giko.svg<br>iyo.svg<br>monar.svg<br>morara.svg<br>onigiri.svg|[📥](https://github.com/cwtickle/danoniplus/releases/download/v15.1.0/img.zip)|[v15.1.0](./Changelog-v15#v1510-2020-05-21)|
|/skin|danoni_skin_default.css<br>danoni_skin_light.css<br>danoni_skin_skyblue.css|[📥](https://github.com/cwtickle/danoniplus/releases/download/v21.4.2/danoni_skin_default.css)<br>[📥](https://github.com/cwtickle/danoniplus/releases/download/v21.4.2/danoni_skin_light.css)<br>[📥](https://github.com/cwtickle/danoniplus/releases/download/v21.4.2/danoni_skin_skyblue.css)|[v21.4.2](./Changelog-v21#v2142-2021-04-07)|

</details>

⭐ New Features / 🛠️ Improvements / 🐞 Bug Fixes  
( Files changed: ⚪ danoni_main.js / 🔵 danoni_constants.js / 🔴 css files )

## [Warning] 更新終了後の不具合情報
- [更新を終了したバージョンの不具合情報#v24](DeprecatedVersionBugs#v24) を参照

## v24.6.19 ([2023-05-31](https://github.com/cwtickle/danoniplus/releases/tag/v24.6.19))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v24.6.19/total)](https://github.com/cwtickle/danoniplus/archive/v24.6.19.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v24.6.19/support/v24?style=social)](https://github.com/cwtickle/danoniplus/compare/v24.6.19...support/v24#files_bucket) 
- 🐞 **C)** customCreditWidthが使用できない問題を修正 ( PR [#1496](https://github.com/cwtickle/danoniplus/pull/1496) ) <- :boom: [**v23.4.0**](./Changelog-v23#v2340-2021-10-02)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v24.6.19)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v24.6.18...v24.6.19#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v24.6.19)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v24.6.19.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v24.6.19.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v24.6.19/js/danoni_main.js)
/ 🎣 [**v32.3.0**](./Changelog-v32#v3230-2023-06-02)

## v24.6.18 ([2023-04-26](https://github.com/cwtickle/danoniplus/releases/tag/v24.6.18))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v24.6.18/total)](https://github.com/cwtickle/danoniplus/archive/v24.6.18.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v24.6.18/support/v24?style=social)](https://github.com/cwtickle/danoniplus/compare/v24.6.18...support/v24#files_bucket) 
- 🐞 **C)** 譜面リスト選択時にキーコンフィグ画面で止まることがある問題を修正 ( PR [#1470](https://github.com/cwtickle/danoniplus/pull/1470) ) <- :boom: [**v24.5.1**](./Changelog-v24#v2451-2021-12-13)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v24.6.18)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v24.6.17...v24.6.18#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v24.6.18)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v24.6.18.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v24.6.18.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v24.6.18/js/danoni_main.js)
/ 🎣 [**v31.6.0**](./Changelog-v31#v3160-2023-04-26)

## v24.6.17 ([2023-03-22](https://github.com/cwtickle/danoniplus/releases/tag/v24.6.17))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v24.6.17/total)](https://github.com/cwtickle/danoniplus/archive/v24.6.17.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v24.6.17/support/v24?style=social)](https://github.com/cwtickle/danoniplus/compare/v24.6.17...support/v24#files_bucket) 
- 🐞 **C)** 譜面リストで、上下キーを使って選択した譜面がキー数違いのときにキーコンフィグ画面へ移動すると画面が止まる問題を修正 ( PR [#1446](https://github.com/cwtickle/danoniplus/pull/1446) ) <- :boom: [**v24.5.1**](./Changelog-v24#v2451-2021-12-13)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v24.6.17)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v24.6.16...v24.6.17#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v24.6.17)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v24.6.17.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v24.6.17.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v24.6.17/js/danoni_main.js)
/ 🎣 [**v31.0.1**](./Changelog-v31#v3101-2023-03-22)

## v24.6.16 ([2023-03-04](https://github.com/cwtickle/danoniplus/releases/tag/v24.6.16))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v24.6.16/total)](https://github.com/cwtickle/danoniplus/archive/v24.6.16.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v24.6.16/support/v24?style=social)](https://github.com/cwtickle/danoniplus/compare/v24.6.16...support/v24#files_bucket) 
- 🐞 **C)** 11, 11Lkeyの別キーモードでFlatにしたときにスクロールが想定と逆になる問題を修正 <- :boom: [**v10.2.1**](./Changelog-v10#v1021-2019-11-20)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v24.6.16)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v24.6.15...v24.6.16#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v24.6.16)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v24.6.16.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v24.6.16.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v24.6.16/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v24.6.16/js/lib/danoni_constants.js)
/ 🎣 [**v29.4.3**](./Changelog-v29#v2943-2023-03-04)

## v24.6.15 ([2023-02-26](https://github.com/cwtickle/danoniplus/releases/tag/v24.6.15))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v24.6.15/total)](https://github.com/cwtickle/danoniplus/archive/v24.6.15.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v24.6.15/support/v24?style=social)](https://github.com/cwtickle/danoniplus/compare/v24.6.15...support/v24#files_bucket) 
- 🐞 **C)** カスタムキーのdivXが未定義のとき、ステップゾーンが並ばない問題を修正 ( PR [#1414](https://github.com/cwtickle/danoniplus/pull/1414) ) <- :boom: **initial**
- 🐞 **C)** カスタムキーのshuffleXが未定義のとき、エラーで止まる問題を修正 ( PR [#1414](https://github.com/cwtickle/danoniplus/pull/1414) ) <- :boom: **initial**

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v24.6.15)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v24.6.14...v24.6.15#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v24.6.15)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v24.6.15.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v24.6.15.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v24.6.15/js/danoni_main.js)
/ 🎣 [**v30.2.3**](./Changelog-v30#v3023-2023-02-26)

## v24.6.14 ([2023-01-20](https://github.com/cwtickle/danoniplus/releases/tag/v24.6.14))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v24.6.14/total)](https://github.com/cwtickle/danoniplus/archive/v24.6.14.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v24.6.14/support/v24?style=social)](https://github.com/cwtickle/danoniplus/compare/v24.6.14...support/v24#files_bucket) 
- 🐞 **C)** カスタムキー定義におけるdivXの第2要素がposX末尾要素の最大値で無い場合、キーコンフィグ保存後にステップゾーン（下段）がずれる問題を修正 ( PR [#1385](https://github.com/cwtickle/danoniplus/pull/1385) ) <- :boom: **initial**

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v24.6.14)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v24.6.13...v24.6.14#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v24.6.14)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v24.6.14.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v24.6.14.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v24.6.14/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v24.6.14/js/lib/danoni_constants.js)
/ 🎣 [**v29.3.5**](./Changelog-v29#v2935-2023-01-20)

## v24.6.13 ([2023-01-07](https://github.com/cwtickle/danoniplus/releases/tag/v24.6.13))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v24.6.13/total)](https://github.com/cwtickle/danoniplus/archive/v24.6.13.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v24.6.13/support/v24?style=social)](https://github.com/cwtickle/danoniplus/compare/v24.6.13...support/v24#files_bucket) 
- 🛠️ 同時押し関連の英訳を見直し ( PR [#1378](https://github.com/cwtickle/danoniplus/pull/1378) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v24.6.13)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v24.6.12...v24.6.13#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v24.6.13)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v24.6.13.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v24.6.13.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v24.6.13/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v24.6.13/js/lib/danoni_constants.js)
/ 🎣 [**v29.3.4**](./Changelog-v29#v2934-2023-01-07)

## v24.6.12 ([2022-12-10](https://github.com/cwtickle/danoniplus/releases/tag/v24.6.12))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v24.6.12/total)](https://github.com/cwtickle/danoniplus/archive/v24.6.12.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v24.6.12/support/v24?style=social)](https://github.com/cwtickle/danoniplus/compare/v24.6.12...support/v24#files_bucket) 
- 🐞 **B)** 速度が激しく変化する場合にタイミングがステップゾーン位置からずれることがある問題を修正 ( PR [#1372](https://github.com/cwtickle/danoniplus/pull/1372) ) <- :boom: **initial**

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v24.6.12)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v24.6.11...v24.6.12#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v24.6.12)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v24.6.12.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v24.6.12.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v24.6.12/js/danoni_main.js)
/ 🎣 [**v29.3.1**](./Changelog-v29#v2931-2022-12-10)

## v24.6.11 ([2022-11-15](https://github.com/cwtickle/danoniplus/releases/tag/v24.6.11))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v24.6.11/total)](https://github.com/cwtickle/danoniplus/archive/v24.6.11.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v24.6.11/support/v24?style=social)](https://github.com/cwtickle/danoniplus/compare/v24.6.11...support/v24#files_bucket) 
- 🐞 **C)** Motionオプションを使用し、フリーズアローについてタイミングをずらして押したとき、そのズレの中にストップが含まれていると、処理が止まる可能性がある問題を修正 ( PR [#1366](https://github.com/cwtickle/danoniplus/pull/1366) ) <- :boom: [**v23.1.1**](Changelog-v23#v2311-2021-09-17)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v24.6.11)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v24.6.10...v24.6.11#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v24.6.11)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v24.6.11.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v24.6.11.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v24.6.11/js/danoni_main.js)
/ 🎣 [**v29.3.0**](./Changelog-v29#v2930-2022-11-15)

## v24.6.10 ([2022-10-29](https://github.com/cwtickle/danoniplus/releases/tag/v24.6.10))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v24.6.10/total)](https://github.com/cwtickle/danoniplus/archive/v24.6.10.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v24.6.10/support/v24?style=social)](https://github.com/cwtickle/danoniplus/compare/v24.6.10...support/v24#files_bucket) 
- 🐞 **C)** フリーズアローを強制削除した場合の判定関数の引数誤りを修正 ( PR [#1343](https://github.com/cwtickle/danoniplus/pull/1343) ) <- :boom: [**v5.12.1**](Changelog-v5#v5121-2019-06-17)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v24.6.10)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v24.6.9...v24.6.10#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v24.6.10)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v24.6.10.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v24.6.10.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v24.6.10/js/danoni_main.js)
/ 🎣 [**v28.5.0**](./Changelog-v28#v2850-2022-10-29)

## v24.6.9 ([2022-08-03](https://github.com/cwtickle/danoniplus/releases/tag/v24.6.9))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v24.6.9/total)](https://github.com/cwtickle/danoniplus/archive/v24.6.9.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v24.6.9/support/v24?style=social)](https://github.com/cwtickle/danoniplus/compare/v24.6.9...support/v24#files_bucket) 
- 🐞 **C)** 重複するID名の解消 ( PR [#1300](https://github.com/cwtickle/danoniplus/pull/1300) ) <- :boom: [**v11.4.0**](Changelog-v11#v1140-2020-02-05)
- 🐞 **C)** 譜面明細画面でID名に英数以外が入りうる問題の修正 ( PR [#1300](https://github.com/cwtickle/danoniplus/pull/1300) ) <- :boom: [**v19.3.0**](Changelog-v19#v1930-2021-01-30)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v24.6.9)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v24.6.8...v24.6.9#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v24.6.9)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v24.6.9.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v24.6.9.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v24.6.9/js/danoni_main.js)
/ 🎣 [**v27.8.1**](./Changelog-v27#v2781-2022-08-01)

## v24.6.8 ([2022-05-22](https://github.com/cwtickle/danoniplus/releases/tag/v24.6.8))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v24.6.8/total)](https://github.com/cwtickle/danoniplus/archive/v24.6.8.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v24.6.8/support/v24?style=social)](https://github.com/cwtickle/danoniplus/compare/v24.6.8...support/v24#files_bucket) 
- 🐞 **B)** フォルダ名に半角スペースや全角文字が含まれている場合に起動できないことがある問題を修正 ( PR [#1289](https://github.com/cwtickle/danoniplus/pull/1289) ) <- :boom: [**v24.6.6**](Changelog-v24#v2466-2022-04-10)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v24.6.8)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v24.6.7...v24.6.8#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v24.6.8)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v24.6.8.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v24.6.8.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v24.6.8/js/danoni_main.js)
/ 🎣 [**v27.5.1**](./Changelog-v27#v2751-2022-05-22)

## v24.6.7 ([2022-04-16](https://github.com/cwtickle/danoniplus/releases/tag/v24.6.7))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v24.6.7/total)](https://github.com/cwtickle/danoniplus/archive/v24.6.7.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v24.6.7/support/v24?style=social)](https://github.com/cwtickle/danoniplus/compare/v24.6.7...support/v24#files_bucket) 
- 🛠️ 11ikeyの別パターンのアシスト設定に対応 ( PR [#1282](https://github.com/cwtickle/danoniplus/pull/1282) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v24.6.7)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v24.6.6...v24.6.7#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v24.6.7)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v24.6.7.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v24.6.7.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v24.6.7/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v24.6.7/js/lib/danoni_constants.js)
/ 🎣 [**v27.4.0**](./Changelog-v27#v2740-2022-04-16)

## v24.6.6 ([2022-04-10](https://github.com/cwtickle/danoniplus/releases/tag/v24.6.6))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v24.6.6/total)](https://github.com/cwtickle/danoniplus/archive/v24.6.6.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v24.6.6/support/v24?style=social)](https://github.com/cwtickle/danoniplus/compare/v24.6.6...support/v24#files_bucket) 
- 🐞 **C)** カレントパス、譜面ファイル取得部分をエスケープするよう修正 ( PR [#1277](https://github.com/cwtickle/danoniplus/pull/1277) ) <- :boom: [**v19.4.1**](Changelog-v19#v1941-2021-02-06)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v24.6.6)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v24.6.5...v24.6.6#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v24.6.6)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v24.6.6.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v24.6.6.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v24.6.6/js/danoni_main.js)
/ 🎣 [**v27.3.1**](./Changelog-v27#v2731-2022-04-10)

## v24.6.5 ([2022-02-20](https://github.com/cwtickle/danoniplus/releases/tag/v24.6.5))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v24.6.5/total)](https://github.com/cwtickle/danoniplus/archive/v24.6.5.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v24.6.5/support/v24?style=social)](https://github.com/cwtickle/danoniplus/compare/v24.6.5...support/v24#files_bucket) 
- 🐞**C)** g_lblRenames未定義時に各種名称の上書きができない問題を修正 ( PR [#1230](https://github.com/cwtickle/danoniplus/pull/1230) ) <- :boom: [**v20.3.1**](Changelog-v20#v2031-2021-02-24)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v24.6.5)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v24.6.4...v24.6.5#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v24.6.5)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v24.6.5.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v24.6.5.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v24.6.5/js/danoni_main.js)
/ 🎣 [**v26.2.0**](./Changelog-v26#v2620-2022-02-20)

## v24.6.4 ([2022-02-16](https://github.com/cwtickle/danoniplus/releases/tag/v24.6.4))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v24.6.4/total)](https://github.com/cwtickle/danoniplus/archive/v24.6.4.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v24.6.4/support/v24?style=social)](https://github.com/cwtickle/danoniplus/compare/v24.6.4...support/v24#files_bucket) 
- 🛠️ npm環境用にディレクトリ構成を見直し ( PR [#1228](https://github.com/cwtickle/danoniplus/pull/1228) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v24.6.4)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v24.6.3...v24.6.4#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v24.6.4)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v24.6.4.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v24.6.4.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v24.6.4/js/danoni_main.js)
/ 🎣 [**v26.1.2**](./Changelog-v26#v2612-2022-02-16)

## v24.6.3 ([2022-01-29](https://github.com/cwtickle/danoniplus/releases/tag/v24.6.3))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v24.6.3/total)](https://github.com/cwtickle/danoniplus/archive/v24.6.3.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v24.6.3/support/v24?style=social)](https://github.com/cwtickle/danoniplus/compare/v24.6.3...support/v24#files_bucket) 
- 🐞**B)** シャッフル・カラーグループを変更した際にその内容が反映されない問題を修正 ( PR [#1215](https://github.com/cwtickle/danoniplus/pull/1215), Gitter [2022-01-29](https://gitter.im/danonicw/community?at=61f42696bfe2f54b2e3f0422) ) <- :boom: [**v24.5.1**](Changelog-v24#v2451-2021-12-13)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v24.6.3)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v24.6.2...v24.6.3#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v24.6.3)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v24.6.3.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v24.6.3.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v24.6.3/js/danoni_main.js)
/ 🎣 [**v25.5.2**](./Changelog-v25#v2552-2022-01-29)

## v24.6.2 ([2022-01-26](https://github.com/cwtickle/danoniplus/releases/tag/v24.6.2))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v24.6.2/total)](https://github.com/cwtickle/danoniplus/archive/v24.6.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v24.6.2/support/v24?style=social)](https://github.com/cwtickle/danoniplus/compare/v24.6.2...support/v24#files_bucket) 
- 🛠️ 二重定義箇所、定義コメントの誤りを修正 ( PR [#1209](https://github.com/cwtickle/danoniplus/pull/1209) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v24.6.2)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v24.6.1...v24.6.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v24.6.2)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v24.6.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v24.6.2.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v24.6.2/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v24.6.2/js/lib/danoni_constants.js)
/ 🎣 [**v25.5.1**](./Changelog-v25#v2551-2022-01-26)

## v24.6.1 ([2022-01-18](https://github.com/cwtickle/danoniplus/releases/tag/v24.6.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v24.6.1/total)](https://github.com/cwtickle/danoniplus/archive/v24.6.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v24.6.1/support/v24?style=social)](https://github.com/cwtickle/danoniplus/compare/v24.6.1...support/v24#files_bucket) 
- 🛠️ 0フレーム指定の色変化、モーションデータに対応 ( PR [#1197](https://github.com/cwtickle/danoniplus/pull/1197) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v24.6.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v24.6.0...v24.6.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v24.6.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v24.6.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v24.6.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v24.6.1/js/danoni_main.js)
/ 🎣 [**v25.3.2**](./Changelog-v25#v2532-2022-01-18)

---

## v24.6.0 ([2022-01-02](https://github.com/cwtickle/danoniplus/releases/tag/v24.6.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v24.6.0/total)](https://github.com/cwtickle/danoniplus/archive/v24.6.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v24.6.0/support/v24?style=social)](https://github.com/cwtickle/danoniplus/compare/v24.6.0...support/v24#files_bucket) 
- ⭐ フェードイン時にそれまでの色変化・モーションデータを保持するように変更 ( PR [#1181](https://github.com/cwtickle/danoniplus/pull/1181) )
- ⭐ 13keyのスクロール拡張にCrossを追加 ( PR [#1180](https://github.com/cwtickle/danoniplus/pull/1180) )
- 🛠️ 色変化計算部分について計算スキップを廃止 ( PR [#1182](https://github.com/cwtickle/danoniplus/pull/1182) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v24.6.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v24.5.1...v24.6.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v24.6.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v24.6.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v24.6.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v24.6.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v24.6.0/js/lib/danoni_constants.js)<br>❤️ MFV2 (@MFV2)

## v24.5.1 ([2021-12-13](https://github.com/cwtickle/danoniplus/releases/tag/v24.5.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v24.5.1/total)](https://github.com/cwtickle/danoniplus/archive/v24.5.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v24.5.1/support/v24?style=social)](https://github.com/cwtickle/danoniplus/compare/v24.5.1...support/v24#files_bucket) 
- 🛠️ シャッフルグループ・カラーグループ周りの処理を見直し ( PR [#1176](https://github.com/cwtickle/danoniplus/pull/1176), [#1177](https://github.com/cwtickle/danoniplus/pull/1177), [#1178](https://github.com/cwtickle/danoniplus/pull/1178) )
- 🐞**B)** 9Akeyでキーパターン移動中に9Bkeyを通過すると9Akey(Pattern: 1)の配色がおかしくなる問題を修正 ( PR [#1174](https://github.com/cwtickle/danoniplus/pull/1174), [#1175](https://github.com/cwtickle/danoniplus/pull/1175) ), Gitter [2021-12-12](https://gitter.im/danonicw/community?at=61b5b044abdd6644e3d30f14) ) <- :boom: [**v22.4.1**](Changelog-v22#v2241-2021-05-16)
- 🐞**C)** データ保存有効時、シャッフルグループを変更してもタイトルに戻った後、元に戻ってしまう問題を修正 ( PR [#1174](https://github.com/cwtickle/danoniplus/pull/1174) ) <- :boom: [**v24.3.0**](Changelog-v24#v2430-2021-11-24)
- 🐞**C)** キーコン画面で矢印色を一時的に変更した際、他のKeyPatternにも色変更が反映されることがある問題を修正 ( PR [#1174](https://github.com/cwtickle/danoniplus/pull/1174) ) <- :boom: [**v22.4.1**](Changelog-v22#v2241-2021-05-16)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v24.5.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v24.5.0...v24.5.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v24.5.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v24.5.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v24.5.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v24.5.1/js/danoni_main.js)<br>❤️ izkdic

## v24.5.0 ([2021-12-11](https://github.com/cwtickle/danoniplus/releases/tag/v24.5.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v24.5.0/total)](https://github.com/cwtickle/danoniplus/archive/v24.5.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v24.5.0/support/v24?style=social)](https://github.com/cwtickle/danoniplus/compare/v24.5.0...support/v24#files_bucket) 
- ⭐ Hidden+/Sudden+/Hid&Sud+のレーンカバーの初期位置、ロックの設定を実装 ( PR [#1171](https://github.com/cwtickle/danoniplus/pull/1171), [#1172](https://github.com/cwtickle/danoniplus/pull/1172), Gitter [2021-12-10](https://gitter.im/danonicw/community?at=61b3598ecdb5c1081a57b0d1) )
- 🛠️ createGeneralSetting関数で真ん中のボタンに対してボタン拡張が行えるように変更 ( PR [#1171](https://github.com/cwtickle/danoniplus/pull/1171) )

<img src="https://user-images.githubusercontent.com/44026291/145671821-4962fdde-30f7-4255-86f7-9798d33e6269.png" width="45%">

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v24.5.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v24.4.0...v24.5.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v24.5.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v24.5.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v24.5.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v24.5.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v24.5.0/js/lib/danoni_constants.js)<br>❤️ apoi

## v24.4.0 ([2021-12-03](https://github.com/cwtickle/danoniplus/releases/tag/v24.4.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v24.4.0/total)](https://github.com/cwtickle/danoniplus/archive/v24.4.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v24.4.0/support/v24?style=social)](https://github.com/cwtickle/danoniplus/compare/v24.4.0...support/v24#files_bucket) 
- ⭐ 9A/9B/9i/11ikeyにおいてTwist/Asymmetryを実装 ( PR [#1167](https://github.com/cwtickle/danoniplus/pull/1167) )
- 🛠️ キーコンフィグ画面のPlayボタンにショートカットを追加 ( PR [#1168](https://github.com/cwtickle/danoniplus/pull/1168) )
- 🛠️ autoPreload設定をデフォルト有効に変更 ( PR [#1169](https://github.com/cwtickle/danoniplus/pull/1169) )

<img src="https://user-images.githubusercontent.com/44026291/144327263-89a78d94-3e6d-4778-9ad4-eb46a70ed3fb.png" width="45%"><img src="https://user-images.githubusercontent.com/44026291/144327351-bbc0bb1e-aaaf-4917-9ddf-f2a68ac65a9a.png" width="45%">
<img src="https://user-images.githubusercontent.com/44026291/144560729-8fcc304a-c84a-4898-bc5f-523ebf41ecf9.png" width="45%">

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v24.4.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v24.3.0...v24.4.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v24.4.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v24.4.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v24.4.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v24.4.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v24.4.0/js/lib/danoni_constants.js)

## v24.3.0 ([2021-11-24](https://github.com/cwtickle/danoniplus/releases/tag/v24.3.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v24.3.0/total)](https://github.com/cwtickle/danoniplus/archive/v24.3.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v24.3.0/support/v24?style=social)](https://github.com/cwtickle/danoniplus/compare/v24.3.0...support/v24#files_bucket) 
- ⭐ キーコンフィグパターン個別にスクロール拡張、アシスト設定が行える機能を実装 ( PR [#1163](https://github.com/cwtickle/danoniplus/pull/1163) )
- ⭐ 9ikey, 17keyに対して部分的にスクロール拡張を追加 ( PR [#1163](https://github.com/cwtickle/danoniplus/pull/1163) )
- ⭐ カスタムキーパターン設定の略記指定に対応 ( PR [#1164](https://github.com/cwtickle/danoniplus/pull/1164) )
- 🛠️ カスタムキーでスクロール拡張、アシスト設定を行う際に同じ設定を二重に指定した場合、二重にリスト登録されないように変更 ( PR [#1163](https://github.com/cwtickle/danoniplus/pull/1163) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v24.3.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v24.2.1...v24.3.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v24.3.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v24.3.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v24.3.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v24.3.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v24.3.0/js/lib/danoni_constants.js)

## v24.2.1 ([2021-11-19](https://github.com/cwtickle/danoniplus/releases/tag/v24.2.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v24.2.1/total)](https://github.com/cwtickle/danoniplus/archive/v24.2.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v24.2.1/support/v24?style=social)](https://github.com/cwtickle/danoniplus/compare/v24.2.1...support/v24#files_bucket) 
- ⭐ 歌詞表示、背景・マスクモーションについて多言語表示に対応 ( PR [#1157](https://github.com/cwtickle/danoniplus/pull/1157), [#1158](https://github.com/cwtickle/danoniplus/pull/1158) )
- ⭐ コメント文の多言語表示に対応 ( PR [#1161](https://github.com/cwtickle/danoniplus/pull/1161) )
- ⭐ playbackRate使用時に小数フレーム値が考慮されるよう変更 ( PR [#1159](https://github.com/cwtickle/danoniplus/pull/1159) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v24.2.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v24.1.0...v24.2.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v24.2.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v24.2.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v24.2.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v24.2.1/js/danoni_main.js)

## v24.1.0 ([2021-11-10](https://github.com/cwtickle/danoniplus/releases/tag/v24.1.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v24.1.0/total)](https://github.com/cwtickle/danoniplus/archive/v24.1.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v24.1.0/support/v24?style=social)](https://github.com/cwtickle/danoniplus/compare/v24.1.0...support/v24#files_bucket) 
- 🛠️ 音楽ファイルが無いとき、Backボタンで復帰できるように変更 ( PR [#1151](https://github.com/cwtickle/danoniplus/pull/1151) )
- 🛠️ musicUrlが未指定で強制スタートした場合、nosound.mp3を見るように変更 ( PR [#1151](https://github.com/cwtickle/danoniplus/pull/1151) ) 
- 🛠️ デフォルト音源利用時、無音であることをプレイ画面で表示するよう変更 ( PR [#1152](https://github.com/cwtickle/danoniplus/pull/1152) ) 
- 🛠️ 判定キャラクタ位置（X座標）とコンボ位置の間を少し広げるよう変更 ( PR [#1153](https://github.com/cwtickle/danoniplus/pull/1153) ) 
- 🛠️ 英語時の判定名を大文字のみから小文字混じりに変更 ( PR [#1153](https://github.com/cwtickle/danoniplus/pull/1153) ) 
- 🛠️ 共通系部分のコード見直し ( PR [#1151](https://github.com/cwtickle/danoniplus/pull/1151), [#1154](https://github.com/cwtickle/danoniplus/pull/1154), [#1155](https://github.com/cwtickle/danoniplus/pull/1155) ) 
- 🐞**C)** musicUrlが未指定の場合にタイトル画面で止まることがある問題を修正 ( PR [#1151](https://github.com/cwtickle/danoniplus/pull/1151) ) <- :boom: [**v23.0.0**](Changelog-v23#v2300-2021-09-04)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v24.1.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v24.0.2...v24.1.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v24.1.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v24.1.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v24.1.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v24.1.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v24.1.0/js/lib/danoni_constants.js)

## v24.0.2 ([2021-10-30](https://github.com/cwtickle/danoniplus/releases/tag/v24.0.2))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v24.0.2/total)](https://github.com/cwtickle/danoniplus/archive/v24.0.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v24.0.2/support/v24?style=social)](https://github.com/cwtickle/danoniplus/compare/v24.0.2...support/v24#files_bucket) 
- 🐞**C)** Firefox時、警告ウィンドウの高さが最小化される問題を修正 ( PR [#1149](https://github.com/cwtickle/danoniplus/pull/1149) ) <- :boom: [**v24.0.0**](Changelog-v24#v2400-2021-10-24)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v24.0.2)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v24.0.1...v24.0.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v24.0.2)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v24.0.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v24.0.2.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v24.0.2/js/danoni_main.js)

## v24.0.1 ([2021-10-27](https://github.com/cwtickle/danoniplus/releases/tag/v24.0.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v24.0.1/total)](https://github.com/cwtickle/danoniplus/archive/v24.0.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v24.0.1/support/v24?style=social)](https://github.com/cwtickle/danoniplus/compare/v24.0.1...support/v24#files_bucket) 
- 🛠️ 速度変化・色変化・矢印モーションデータのソート部分のコード見直し ( PR [#1147](https://github.com/cwtickle/danoniplus/pull/1147) )
- 🐞**B)** Reverse時のarrowMotionDataが正しく反映されない問題を修正 ( PR [#1146](https://github.com/cwtickle/danoniplus/pull/1146) ) <- :boom: [**v23.4.1**](Changelog-v23#v2341-2021-10-04)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v24.0.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v24.0.0...v24.0.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v24.0.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v24.0.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v24.0.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v24.0.1/js/danoni_main.js)<br>❤️ SKB (@superkuppabros)

## v24.0.0 ([2021-10-24](https://github.com/cwtickle/danoniplus/releases/tag/v24.0.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v24.0.0/total)](https://github.com/cwtickle/danoniplus/archive/v24.0.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v24.0.0/support/v24?style=social)](https://github.com/cwtickle/danoniplus/compare/v24.0.0...support/v24#files_bucket) 
- ⭐ 各種メッセージの日本語・英語の切り替え機能を追加 ( PR [#1144](https://github.com/cwtickle/danoniplus/pull/1144) )
- ⭐ プレイ画面の譜面名にシャッフル名称を追加 ( PR [#1141](https://github.com/cwtickle/danoniplus/pull/1141) )
- ⭐ 他キー移動時でも該当の拡張スクロールがあれば設定を引き継ぐよう変更 ( PR [#1142](https://github.com/cwtickle/danoniplus/pull/1142) )
- ⭐ アシストプレイ時の設定名を変更できる機能を追加 ( PR [#1143](https://github.com/cwtickle/danoniplus/pull/1143) )
- 🛠️ 警告メッセージ枠の縦の長さについて、メッセージの折り返しに連動して決まるように変更 ( PR [#1144](https://github.com/cwtickle/danoniplus/pull/1144) )
- 🛠️ 設定引き継ぎ関連のコードを一部見直し ( PR [#1142](https://github.com/cwtickle/danoniplus/pull/1142) )
- 🛠️ 独自のラベルテキスト、オンマウステキストについて言語で振り分けできるよう変更 ( PR [#1144](https://github.com/cwtickle/danoniplus/pull/1144) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v24.0.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v23.5.1...v24.0.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v24.0.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v24.0.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v24.0.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v24.0.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v24.0.0/js/lib/danoni_constants.js)

<img src="./wiki/changelog/v24_0001.png" alt="v24_0001" width="80%">

[**<- v25**](Changelog-v25) | **v24** | [**v23 ->**](Changelog-v23)   