**[English](https://github.com/cwtickle/danoniplus-docs/wiki/Tips-index) | Japanese** 

| [< Pull Requestに上がっているソースの取得方法](./fetch-source) || **Tips一覧** || [機能実装方針 >](./FunctionalPolicy) |

# Tips Index
- Dancing☆Onigiri (CW Edition) を利用する際のTipsをまとめました。

## カスタムキー関連
- [カスタムキーテンプレート](tips-0004-extrakeys) [:pencil:](tips-0004-extrakeys/_edit)
- [カスタムキーの作成方法](tips-0015-make-custom-key-types) [:pencil:](tips-0015-make-custom-key-types/_edit)
- [既存キーのキーパターン上書き](tips-0006-keypattern-update) [:pencil:](tips-0006-keypattern-update/_edit)
- [キー変化作品の実装例](tips-0009-key-types-change) [:pencil:](tips-0009-key-types-change/_edit)
- [カスタムキーで独自のReverseを設定](tips-0011-original-reverse) [:pencil:](tips-0011-original-reverse/_edit)
- [カスタムキーの省略形記法](tips-0016-custom-key-abbreviation) [:pencil:](tips-0016-custom-key-abbreviation/_edit)

## ローカルプレイ・Webアップロード
- [作品ページを任意の場所に配置](tips-0007-works-folder) [:pencil:](tips-0007-works-folder/_edit)
- [WordPressを使って作品を公開](tips-0008-wordpress) [:pencil:](tips-0008-wordpress/_edit)
- [複数バージョンのファイル切り替え](tips-0018-multiple-versions) [:pencil:](tips-0018-multiple-versions/_edit)
- [ローカルでのプレイ方法(補足)](./HowToLocalPlay-Appendix) [:pencil:](./HowToLocalPlay-Appendix/_edit)

## カスタムJS共通
⇒ [カスタムjs(スキンjs)による処理割込み](./AboutCustomJs) / [カスタム関数の定義](./AboutCustomFunction) もご覧ください。
- [カスタムJS事始め](tips-0020-firststep-customjs) [:pencil:](tips-0020-firststep-customjs)
- [オブジェクトの階層とラベル・ボタンの挿入](tips-0021-object-layer) [:pencil:](tips-0021-object-layer/_edit)
- [利便性の高い変数群](tips-0022-variables) [:pencil:](tips-0022-variables/_edit)
- [ボタン処理の拡張・上書き](tips-0005-button-expansion) [:pencil:](tips-0005-button-expansion/_edit)
- [ボタン処理のコピー](tips-0014-copy-button-process) [:pencil:](tips-0014-copy-button-process/_edit)
- [イベントリスナーの追加・削除](tips-0012-event-listener) [:pencil:](tips-0012-event-listener/_edit)

## プレイ画面のカスタマイズ
- [スキップボタンの作成方法(タイトル画面)](tips-0013-titleskip) [:pencil:](tips-0013-titleskip/_edit)
- [背景の表示方法](tips-0002-background) [:pencil:](tips-0002-background/_edit)
- [画面の位置調整方法](tips-0003-position-adjustment) [:pencil:](tips-0003-position-adjustment/_edit)
- [ステップゾーンや矢印全体を一時的に隠す](tips-0010-hide-objects) [:pencil:](tips-0010-hide-objects/_edit)
- [スコアドラムロールの作成](tips-0023-score-drum-roll) [:pencil:](tips-0023-score-drum-roll/_edit)
- [カスタム画面の作成](tips-0024-custom-window) [:pencil:](tips-0024-custom-window/_edit)
- [設定追加の実装例](tips-0025-freeze-return) [:pencil:](tips-0025-freeze-return/_edit)

## その他
- [タイトルや歌詞表示に半角コンマ(,)を使う](tips-0001-comma) [:pencil:](tips-0001-comma/_edit)
- [ブックマークレット集](tips-0019-bookmarklet) [:pencil:](tips-0019-bookmarklet/_edit)
- [プレイ画面外のテキスト入力を有効にする](tips-0017-external-input) [:pencil:](tips-0017-external-input/_edit)

## テンプレート
- [Tipsテンプレート](tips-XXXX-template) [:pencil:](tips-XXXX-template/_edit)

| [< Pull Requestに上がっているソースの取得方法](./fetch-source) || **Tips一覧** || [機能実装方針 >](./FunctionalPolicy) |
