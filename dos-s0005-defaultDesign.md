**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-s0005-defaultDesign) | Japanese** 

[↑ 共通設定ファイル仕様に戻る](dos_setting)

[ <- フリーズアロー設定 ](dos-s0004-frzArrow) | **デフォルトデザイン・画像設定** | [オプション有効化 -> ](dos-s0006-settingUse)

## デフォルトデザイン・画像設定
### デフォルトデザインの使用有無設定 (g_presetObj.customDesignUse)
⇒ 指定があった場合に優先される譜面ヘッダー：[customTitleUse](dos-h0025-customTitleUse), [customTitleArrowUse](dos-h0026-customTitleArrowUse), [customTitleAnimationUse](dos-h0078-customTitleAnimationUse), [customBackUse](dos-h0027-customBackUse), [customBackMainUse](dos-h0028-customBackMainUse), [customReadyUse](dos-h0029-customReadyUse)
- タイトルで表示される曲名、背景矢印、背景などを使用するかどうかを指定します。  
使用せず独自のものを使用する場合は`true`にします。デフォルトはいずれも`false`となっています。
```javascript
g_presetObj.customDesignUse = {
	title: `false`,           // タイトル曲名
	titleArrow: `false`,      // タイトルの背景矢印
	titleAnimation: `false`,  // タイトルのアニメーション
	back: `false`,            // メイン画面以外の背景
	backMain: `false`,        // メイン画面の背景
	ready: `false`,           // メイン開始時の「Ready」表示
}
```

### デフォルト画像セットの設定 (g_presetObj.imageSets)
⇒ 指定があった場合に優先される譜面ヘッダー：[imgType](dos-h0082-imgType)
- デフォルトで指定する矢印・フリーズアロー等の基本画像を変更できます。  
[img]フォルダ内にサブフォルダを作成し、そこに画像一式を格納してから使用します。  
`サブフォルダ名,拡張子,画像回転有無(true or false),Flat時ステップ間隔`の順に指定します。  
※詳細は[imgType](dos-h0082-imgType)の項目もご覧ください。
```javascript
g_presetObj.imageSets = [`classic,png`, `classic-thin,png`, `note,svg,true,10`];
// 1つ目：「classic」フォルダに基本画像一式があり、拡張子「png」のものを使用
// 2つ目：「classic-thin」フォルダに基本画像一式があり、拡張子「png」のものを使用
// 3つ目：「note」フォルダに基本画像一式があり、拡張子「svg」のものを使用
```

|番号|設定例|内容|
|----|----|----|
|1|classic|使用する画像セットのサブフォルダ名。<br>デフォルトは`空白`(imgフォルダ直下の画像を使用)|
|2|png|使用する画像セットの拡張子。デフォルトはsvg形式。|
|3|true|画像回転有無。デフォルトはtrue(回転する)。<br>矢印のように向きの関係のないオブジェクトの場合、回転が不要のためこのオプションが存在している。|
|4|10|Flat時ステップ間隔。デフォルトは50(矢印サイズ)。<br>Scroll: Flat時、ステップゾーンの代わりに2つのバーが用意されるが、その2つのバー間隔をpx単位で設定できる。|

※ver22.5.1のみ、`g_presetImageSet`(配列で無く文字列で1要素のみ定義)となっています。

### デフォルト画像の拡張子設定 (g_presetObj.overrideExtension)
⇒ 指定があった場合に優先される譜面ヘッダー：なし
- 矢印、おにぎり等のデフォルト画像（独自で追加している画像は除きます）について、  
svg画像を使用するか、他の拡張子（png画像など）を使用するかを設定します。  
- 未指定の場合のデフォルトは`svg`画像です。  
独自のデフォルト画像を使用している場合で  
それが`svg`形式以外の場合は下記設定にて拡張子を設定します。
```javascript
g_presetObj.overrideExtension = `svg`;
```

### 追加指定する画像のリスト (g_presetObj.customImageList)
⇒ 指定があった場合に優先される譜面ヘッダー：なし
- ここで設定した画像をimgフォルダに指定した名前で格納しておくことで、  
stepRtnX_Yで設定する名前に使用することができる（＝キャラクタとして利用可能）。

なお、`ball`と指定した場合、下記の画像を準備する必要があります。  
- ball.svg, ballShadow.svg, ballStepHit.svg (g_presetOverrideExtension を pngにすれば、pngに変更可)

```javascript
g_presetObj.customImageList = [`ball`, `square`];
```

### 背景・マスクモーションで利用する「animationFillMode」のデフォルト値 (g_presetObj.animationFillMode)
⇒ 指定があった場合に優先される譜面ヘッダー：なし
- cssスタイルの「animation-fill-mode」に対応した設定の初期値を設定します。
    - none      : 初期画像へ戻す
    - forwards  : アニメーション100%の状態を維持
    - backwards : アニメーション  0%の状態に戻す
```javascript
g_presetObj.animationFillMode = `forwards`;
```

## 更新履歴

|Version|変更内容|
|----|----|
|[v30.1.1](https://github.com/cwtickle/danoniplus/releases/tag/v30.1.1)|・背景、マスクモーションの"Animation-Fill-Mode"の初期値設定を追加|
|[v27.4.0](https://github.com/cwtickle/danoniplus/releases/tag/v27.4.0)|・追加指定する画像のリスト (g_presetCustomImageList)について回転に対応|
|[v26.3.1](https://github.com/cwtickle/danoniplus/releases/tag/v26.3.1)|・変数名変更<br>　・g_presetCustomDesignUse -> g_presetObj.customDesignUse<br>　・g_presetImageSets -> g_presetObj.imageSets<br>　・g_presetOverrideExtension -> g_presetObj.overrideExtension<br>　・g_presetCustomImageList -> g_presetObj.customImageList|
|[v23.1.0](https://github.com/cwtickle/danoniplus/releases/tag/v23.1.0)|・デフォルト画像セットの設定(g_presetImageSets)に3つ目、4つ目の項目を追加|
|[v23.0.0](https://github.com/cwtickle/danoniplus/releases/tag/v23.0.0)|・デフォルト画像セットの設定名、定義方法を変更<br>　(g_presetImageSet -> g_presetImageSets)|
|[v22.5.1](https://github.com/cwtickle/danoniplus/releases/tag/v22.5.1)|・デフォルト画像セットの設定 (g_presetImageSet)、<br>　追加指定する画像のリスト (g_presetCustomImageList)を実装|
|[v18.5.0](https://github.com/cwtickle/danoniplus/releases/tag/v18.5.0)|・タイトル文字のアニメーション設定 (g_presetCustomDesignUse.titleAnimation)を実装|
|[v15.1.0](https://github.com/cwtickle/danoniplus/releases/tag/v15.1.0)|・デフォルト画像の拡張子設定 (g_presetOverrideExtension)を実装|
|[v3.6.0](https://github.com/cwtickle/danoniplus/releases/tag/v3.6.0)|・デフォルトデザインの使用有無設定 (g_presetCustomDesignUse)を実装|

[ <- フリーズアロー設定 ](dos-s0004-frzArrow) | **デフォルトデザイン・画像設定** | [オプション有効化 -> ](dos-s0006-settingUse)