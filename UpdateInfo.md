**[English](https://github.com/cwtickle/danoniplus-docs/wiki/UpdateInfo) | Japanese**

| [< アップグレードガイド](./MigrationGuide) | **更新情報概要** | [更新を終了したバージョンの不具合情報 >](./DeprecatedVersionBugs) |

# 更新情報 / UpdateInfo

- 新機能を中心にまとめています。
- 最新の情報は[Release](https://github.com/cwtickle/danoniplus/releases) もしくは Changelog(下記バージョン別リンク) をご利用ください。
- アップグレードについては[アップグレードガイド(Migration Guide)](MigrationGuide)もご覧ください。

## ⭐ v40 ([Changelog](./Changelog-latest))

- デバッグ用に主要オブジェクトの中身を表示する画面を実装
- カスタムキー定義を複数定義できるよう変更
- 未使用のg_keyObjプロパティをタイトル表示後に削除するよう変更
- エディターのカスタムキー定義を自動生成する機能を追加
- 前提条件画面のオブジェクトに対してクリップボードへコピーする機能を追加

### 💡 Feature Updates

- [v40.3.0](https://github.com/cwtickle/danoniplus/releases/tag/v40.3.0) / [v40.2.0](https://github.com/cwtickle/danoniplus/releases/tag/v40.2.0) / [v40.1.0](https://github.com/cwtickle/danoniplus/releases/tag/v40.1.0) / [**v40.0.0**](https://github.com/cwtickle/danoniplus/releases/tag/v40.0.0)

### 📔 Documentation

- [ID一覧](./IdReferenceIndex)
- [オブジェクト一覧](./ObjectReferenceIndex)
- [カスタム関数の定義](./AboutCustomFunction)
- [アップグレードガイド](./MigrationGuide)

## ⭐ v39 ([Changelog](./Changelog-v39))

- Ex-Settings画面を追加
- PlayWindow, StepArea, FrzReturn, Shaking, Effect, Camoufrage, Swapping, JudgRange, AutoRetry機能を追加
- プレイ画面の makeArrow / makeFrzArrow 関数に対してカスタムJSを追加 
- フリーズアローの階層構造を一部変更
- プレイ画面の階層構造を変更、多層化に対応
- 結果画面に表示される設定名（PlayStyle）について全体に収まるようにレイアウトを見直し
- カスタムキー定義にレーン別の割当階層と階層別のCSS Transition定義を追加
- スクロール反転（scrollch_data）データを拡張し、別階層への追加に対応
- transform管理用の変数を追加
- Appearance用のフィルターを階層別に定義するよう変更
- ローカルストレージのデータを管理する画面を実装
- ローカルストレージ情報を使わないセーフモードの実装

### 💡 Feature Updates

- [v39.8.0](https://github.com/cwtickle/danoniplus/releases/tag/v39.8.0) / [v39.7.0](https://github.com/cwtickle/danoniplus/releases/tag/v39.7.0) / [**v39.6.0**](https://github.com/cwtickle/danoniplus/releases/tag/v39.6.0) / [**v39.5.0**](https://github.com/cwtickle/danoniplus/releases/tag/v39.5.0) / [v39.4.0](https://github.com/cwtickle/danoniplus/releases/tag/v39.4.0) / [**v39.3.0**](https://github.com/cwtickle/danoniplus/releases/tag/v39.3.0) / [**v39.2.0**](https://github.com/cwtickle/danoniplus/releases/tag/v39.2.0) / [**v39.1.0**](https://github.com/cwtickle/danoniplus/releases/tag/v39.1.0) / [**v39.0.0**](https://github.com/cwtickle/danoniplus/releases/tag/v39.0.0)

### 📔 Documentation

- [ゲーム画面の説明（拡張設定）](./AboutGameSystem-ex)
- [ゲーム内で使用できるショートカット](./Shortcut)
- [ID一覧](./IdReferenceIndex)
- [オブジェクト一覧](./ObjectReferenceIndex) ( [addTransform / addTempTransform](./fnc-c0047-addTransform) / [addXY / addX / addY / delXY / delX / delY](./fnc-c0048-addXY) )
- [スキンファイル仕様](./AboutSkin)
- [カスタム関数の定義](./AboutCustomFunction)
- [ローカルストレージ仕様](./LocalStorage)
- [キー数仕様](./keys)
- [スクロール反転 (scrollch_data)](./dos-e0007-scrollchData)
- 譜面ヘッダー仕様 ( [settingUse](./dos-h0035-settingUse) )
- 共通設定ファイル仕様 ( [オプション有効化](./dos-s0006-settingUse) )
- [アップグレードガイド](./MigrationGuide)

## ⭐ v38 ([Changelog](./Changelog-v38))

- ゲージ設定部分で数式が使えるように変更
- MotionオプションにCompress, Fountainを追加
- リモート参照時のみ、カレントパス指定なしで作品URLの相対パスとしてファイル参照するよう変更
- Scatterオプションのシャッフル方法を一部見直し
- プレイ中ショートカットの変更に対応

### 💡 Feature Updates

- [**v38.3.0**](https://github.com/cwtickle/danoniplus/releases/tag/v38.3.0) / [v38.2.0](https://github.com/cwtickle/danoniplus/releases/tag/v38.2.0) / [**v38.1.0**](https://github.com/cwtickle/danoniplus/releases/tag/v38.1.0) / [**v38.0.0**](https://github.com/cwtickle/danoniplus/releases/tag/v38.0.0)

### 📔 Documentation
- [ゲーム画面の説明](./AboutGameSystem)
- [基準ディレクトリ仕様](./AboutBaseDirectory)
- [速度変化・Motionオプション仕様](./SpeedChange)
- 譜面ヘッダー仕様 ( [gaugeX](./dos-h0022-gaugeX) )

## ⭐ v37 ([Changelog](./Changelog-v37))

- キー数変化 (keych_data) で同時に複数のキーを共存させる機能を実装
- Appearance 設定の Lock ボタンにショートカットキーを割り当て
- 速度変化グラフにおいて最低/最高変化量を表記するように変更
- playbackRate の値に合わせて Adjustment の設定幅を伸縮するよう変更
- 生成タイミングが同じで Y 座標違いの場合でも矢印・フリーズアローを区別して生成できるよう対応
- 9Akey, 13key にカラーグループを追加
- リバース設定を制限する機能を実装
- カスタムキーの単位名を変更できる設定を追加

### 💡 Feature Updates

- [v37.8.0](https://github.com/cwtickle/danoniplus/releases/tag/v37.8.0) / [v37.7.0](https://github.com/cwtickle/danoniplus/releases/tag/v37.7.0) / [v37.6.0](https://github.com/cwtickle/danoniplus/releases/tag/v37.6.0) / [v37.5.0](https://github.com/cwtickle/danoniplus/releases/tag/v37.5.0) / [v37.4.0](https://github.com/cwtickle/danoniplus/releases/tag/v37.4.0) / [v37.3.0](https://github.com/cwtickle/danoniplus/releases/tag/v37.3.0) / [v37.2.0](https://github.com/cwtickle/danoniplus/releases/tag/v37.2.0) / [**v37.1.0**](https://github.com/cwtickle/danoniplus/releases/tag/v37.1.0) / [**v37.0.0**](https://github.com/cwtickle/danoniplus/releases/tag/v37.0.0)

### 📔 Documentation

- [ゲーム画面の説明](./AboutGameSystem)
- [ゲーム内で使用できるショートカット](./Shortcut)
- [キー数変化 (keych_data)](./dos-e0006-keychData)
- [キー数仕様](./keys)
- [速度変化・Motion オプション仕様](./SpeedChange)
- 譜面ヘッダー仕様 ( [settingUse](./dos-h0035-settingUse) )

## ⭐ v36 ([Changelog](./Changelog-v36))

- 色変化の新記述形式を実装
- 色変化(新仕様)において矢印・フリーズアローの塗りつぶし部分の変化に対応
- 色変化(新仕様)においてフリーズアローのレーン個別の色変化に対応
- 譜面明細画面にハイスコア表示を実装
- カラーグループ数を 5 から 10 に拡張、カラーピッカーで設定した色をリセットする機能を実装
- 一時的に変更できるシャッフルグループ数を 10(固定)からキー数毎に自動決定するよう変更
- シャッフル設定に矢印・フリーズアローが散らばる S-Random の設定（Scatter/Scatter+）を追加

### 💡 Feature Updates

- [v36.6.0](https://github.com/cwtickle/danoniplus/releases/tag/v36.6.0) / [**v36.5.0**](https://github.com/cwtickle/danoniplus/releases/tag/v36.5.0) / [v36.4.0](https://github.com/cwtickle/danoniplus/releases/tag/v36.4.0) / [**v36.3.0**](https://github.com/cwtickle/danoniplus/releases/tag/v36.3.0) / [**v36.2.0**](https://github.com/cwtickle/danoniplus/releases/tag/v36.2.0) / [**v36.1.0**](https://github.com/cwtickle/danoniplus/releases/tag/v36.1.0) / [**v36.0.0**](https://github.com/cwtickle/danoniplus/releases/tag/v36.0.0)

### 📔 Documentation

- [ゲーム画面の説明](./AboutGameSystem)
- [ゲーム内で使用できるショートカット](./Shortcut)
- [ID 一覧](./IdReferenceIndex)
- [色変化 (ncolor_data)](./dos-e0002-ncolorData)
- [スキンファイル仕様](./AboutSkin)
- [ローカルストレージ仕様](./LocalStorage)
- 譜面ヘッダー仕様 ( [setColor](./dos-h0003-setColor) / [frzColor](./dos-h0004-frzColor) )

## ⭐ v35 ([Changelog](./Changelog-v35))

- ウィンドウサイズ（高さ）とステップゾーン位置により基準速度を変更
- Appearance 設定で可視範囲を明記するよう変更
- ウィンドウサイズ（高さ）を外部パラメータから変更できる機能を追加
- keyGroupOrder をカスタムキー定義の 1 項目として追加
- カスタムキー用に一部の部分キー定義を標準実装
- カスタムキーの略記指定パターンを追加
- カスタムキーの略記指定の組み合わせに対応
- カスタムキー定義に強制的にステップゾーンを Flat 化する設定を追加
- Display オプションの「StepZone」に「FlatBar」設定を追加
- 上下両方にステップゾーンがある場合、両方に「Flat」の線が出るように変更
- 拡張スクロール名の末尾が「Flat」なら、自動でステップゾーンを Flat 化するよう変更
- 横幅が 900px を超えるときに、ボタンの伸縮を固定するよう変更

### 💡 Feature Updates

- [**v35.5.0**](https://github.com/cwtickle/danoniplus/releases/tag/v35.5.0) / [**v35.4.0**](https://github.com/cwtickle/danoniplus/releases/tag/v35.4.0) / [**v35.3.0**](https://github.com/cwtickle/danoniplus/releases/tag/v35.3.0) / [**v35.2.1**](https://github.com/cwtickle/danoniplus/releases/tag/v35.2.1) / [v35.1.0](https://github.com/cwtickle/danoniplus/releases/tag/v35.1.0) / [**v35.0.0**](https://github.com/cwtickle/danoniplus/releases/tag/v35.0.0)

### 📔 Documentation

- [ID 一覧](./IdReferenceIndex)
- [ゲーム内で使用できるショートカット](./Shortcut)
- [共通設定ファイル仕様](./dos_setting) ( [制作者クレジット・基本設定](./dos-s0001-makerInfo) )
- 譜面ヘッダー仕様 ( [heightVariable](./dos-h0100-heightVariable) / [keyGroupOrder](./dos-h0092-keyGroupOrder) )
- [キー数仕様](./keys)
- [作品 URL のクエリパラメーター仕様](./QueryParameter)
- [カスタムキーの省略形記法](./tips-0016-custom-key-abbreviation)

## ⭐ v34 ([Changelog](./Changelog-v34))

- 歌詞表示、背景・マスク・スタイルデータについてキー名・キーパターン毎に設定する機能を追加
- 譜面エフェクトデータについて、既存の他データが存在していればその設定を引用する機能を追加
- 歌詞表示及びタイトル曲名・アーティスト名表示で、条件付きでカンマが使えるよう変更
- US キーボード向けのキーコンフィグ表記に対応
- 譜面明細子画面のレーン別矢印数で最大・最小がわかるよう表示を追加
- 譜面番号固定時に他の譜面ファイル番号や譜面番号を参照できる機能を追加
- デフォルトのリザルトフォーマットの出力機能を追加

### 💡 Feature Updates

- [**v34.6.0**](https://github.com/cwtickle/danoniplus/releases/tag/v34.6.0) / [**v34.5.1**](https://github.com/cwtickle/danoniplus/releases/tag/v34.5.1) / [v34.4.0](https://github.com/cwtickle/danoniplus/releases/tag/v34.4.0) / [v34.3.0](https://github.com/cwtickle/danoniplus/releases/tag/v34.3.0) / [v34.2.0](https://github.com/cwtickle/danoniplus/releases/tag/v34.2.0) / [**v34.1.0**](https://github.com/cwtickle/danoniplus/releases/tag/v34.1.0) / [**v34.0.0**](https://github.com/cwtickle/danoniplus/releases/tag/v34.0.0)

### 📔 Documentation

- [ゲーム内で使用できるショートカット](./Shortcut)
- [譜面エフェクト仕様](./dos_effect)
- [KeyCtrl 属性で使用するキーコード](./KeyCtrlCodeList)
- [スキンファイル仕様](./AboutSkin)
- 譜面ヘッダー仕様 ( [dosNo](./dos-h0098-dosNo) )

## ⭐ v33 ([Changelog](./Changelog-v33))

- **スキンを一時変更**できる機能を実装
- グラデーション指定について、グラデーション以外のものも追加できるよう拡張
- コンボ表示、Fast/Slow、推定 Adj、Excessive 表示、譜面明細画面子画面背景、譜面選択子画面背景について CSS クラスを分離
- 結果画面の SNS 投稿用画像コピー機能を実装
- 画面の上下に共通の枠線を一律追加
- デフォルトスキン時、Canvas 背景を有効化するかどうかの設定を追加
- リザルト画像用にカスタム変数を表示する設定を追加
- ショートカットキーが通常と異なる場合の表示とその表示エリアの設定を追加
- カスタムキー位置の下段略記に対応
- ウィンドウの高さや Y 座標調整ができる機能を実装

### 💡 Feature Updates

- [v33.7.0](https://github.com/cwtickle/danoniplus/releases/tag/v33.7.0) / [v33.6.0](https://github.com/cwtickle/danoniplus/releases/tag/v33.6.0) / [v33.5.0](https://github.com/cwtickle/danoniplus/releases/tag/v33.5.0) / [**v33.4.0**](https://github.com/cwtickle/danoniplus/releases/tag/v33.4.0) / [v33.3.0](https://github.com/cwtickle/danoniplus/releases/tag/v33.3.0) / [v33.2.0](https://github.com/cwtickle/danoniplus/releases/tag/v33.2.0) / [**v33.1.0**](https://github.com/cwtickle/danoniplus/releases/tag/v33.1.0) / [**v33.0.0**](https://github.com/cwtickle/danoniplus/releases/tag/v33.0.0)

### 📔 Documentation

- [ゲーム内で使用できるショートカット](./Shortcut)
- [スキンファイル仕様](./AboutSkin)
- [スキン変更 (style_data)](./dos-e0008-styleData)
- [グラデーション仕様](./dos-c0001-gradation)
- 譜面ヘッダー仕様 ( [bgCanvasUse](./dos-h0094-bgCanvasUse) / [resultValsView](./dos-h0095-resultValsView) / [scArea](./dos-h0096-scArea) / [playingX / playingY](./dos-h0070-playingX) / [playingWidth / playingHeight](./dos-h0071-playingWidth) / [windowHeight](./dos-h0097-windowHeight) )
- [共通設定ファイル仕様](./dos_setting) ( [制作者クレジット・基本設定](./dos-s0001-makerInfo) / [カスタムファイル設定](./dos-s0002-customFile) / [プレイ画面制御](./dos-s0007-viewControl) / [リザルトデータ](./dos-s0008-resultVals) )

## ⭐ v32 ([Changelog](./Changelog-v32))

- Shift, Ctrl, Alt キーの左右キーの割り当てを分離
- カスタムキー定義の略記指定を強化
- ボタンのデフォルト処理を参照する機能を追加
- 曲開始までの空白フレーム数設定(blankFrame)の譜面別設定に対応
- 速度変化グラフにおいて速度変化の平均値表示を追加
- 譜面明細画面の部分表示に対応
- Shuffle オプションの「Asym-Mirror」を拡張し「X-Mirror」に名称変更
- 9Akey, 11ikey, 17key のデフォルトシャッフルグループを変更
- 17key のデフォルトカラーグループを変更
- 設定画面のゲージ詳細に**必要達成率**（Accuracy）を追加
- 設定画面のゲージ詳細で表示する回復・ダメージ量について、常に実数値を表示するように変更
- **空押し判定**を実装
- 譜面明細表示の順番、名称を変更
- フリーズアローの始点判定有効時、譜面明細画面の Velocity, DifLevel の表示が変わるよう変更
- ゲージ設定について、Difficulty を変更しても同じゲージ設定があれば初期化しないよう変更
- タイトル表示系、カスタム関数設定の多言語化対応

### 💡 Feature Updates

- [v32.7.0](https://github.com/cwtickle/danoniplus/releases/tag/v32.7.0) / [v32.6.0](https://github.com/cwtickle/danoniplus/releases/tag/v32.6.0) / [**v32.5.0**](https://github.com/cwtickle/danoniplus/releases/tag/v32.5.0) / [**v32.4.0**](https://github.com/cwtickle/danoniplus/releases/tag/v32.4.0) / [v32.3.0](https://github.com/cwtickle/danoniplus/releases/tag/v32.3.0) / [**v32.2.0**](https://github.com/cwtickle/danoniplus/releases/tag/v32.2.0) / [v32.1.0](https://github.com/cwtickle/danoniplus/releases/tag/v32.1.0) / [**v32.0.0**](https://github.com/cwtickle/danoniplus/releases/tag/v32.0.0)

### 📔 Documentation

- [ゲーム画面の説明](./AboutGameSystem)
- [ID 一覧](./IdReferenceIndex)
- 譜面ヘッダー仕様 ( [blankFrame](./dos-h0006-blankFrame) / [scoreDetailUse (chartDetailUse)](./dos-h0060-scoreDetailUse) / [customCreditWidth](./dos-h0083-customCreditWidth) / [excessiveJdgUse](./dos-h0093-excessiveJdgUse) / [settingUse](./dos-h0035-settingUse) / [titleSize](./dos-h0030-titlesize) / [titleFont](./dos-h0031-titlefont) / [titlegrd / titleArrowgrd](./dos-h0032-titlegrd) / [titlePos](./dos-h0033-titlepos) / [titleLineHeight](./dos-h0034-titlelineheight) / [titleAnimation](./dos-h0077-titleanimation) / [titleAnimationClass](./dos-h0079-titleanimationclass) / [customJs](./dos-h0019-customjs) / [customCss](./dos-h0086-customcss) )
- 共通設定ファイル仕様 ( [ゲージ設定](./dos-s0003-initialGauge) / [オプション有効化](./dos-s0006-settingUse) )
- [カスタムキーテンプレート](./tips-0004-extrakeys)
- [KeyCtrl 属性で使用するキーコード](./KeyCtrlCodeList)
- [判定仕様](./AboutJudgment)

## ⭐ v31 ([Changelog](./Changelog-v31))

- 矢印・フリーズアローの**ヒット位置をステップゾーンからずらす**機能を実装
- 矢印の**回転、キャラクタ**に関するグループ（ShapeGroup）を実装
- 17key においてシャッフル、カラー、シェイプグループを追加
- 譜面密度グラフについて、上位 3 番目まで色付けするよう変更
- 譜面エフェクトデータ及び初期矢印色データに別譜面の変数名が入っていた場合、別譜面のデータを参照するよう変更
- キー割当設定について **KeyboardEvent.code** の値及び略名が使えるよう変更
- リトライ、タイトルバックのショートカットキー設定について **KeyboardEvent.code** の値及び略名が使えるよう変更
- Shuffle 設定に「Turning」を実装
- 別キーモード、ミラー（Mirror, Asym-Mirror）時のハイスコア保存に対応
- 7ikey に別キーモード追加、23key にシャッフルグループを追加
- 設定系のサブ関数をグローバル関数に変更
- フリーズアロー始点判定成功時の割込み処理を実装

### 💡 Feature Updates

- [v31.7.0](https://github.com/cwtickle/danoniplus/releases/tag/v31.7.0) / [v31.6.0](https://github.com/cwtickle/danoniplus/releases/tag/v31.6.0) / [**v31.5.0**](https://github.com/cwtickle/danoniplus/releases/tag/v31.5.0) / [**v31.4.0**](https://github.com/cwtickle/danoniplus/releases/tag/v31.4.0) / [v31.3.1](https://github.com/cwtickle/danoniplus/releases/tag/v31.3.1) / [v31.2.0](https://github.com/cwtickle/danoniplus/releases/tag/v31.2.0) / [**v31.1.0**](https://github.com/cwtickle/danoniplus/releases/tag/v31.1.0) / [**v31.0.0**](https://github.com/cwtickle/danoniplus/releases/tag/v31.0.0)

### 📔 Documentation

- [ゲーム画面の説明](./AboutGameSystem)
- [ゲーム内で使用できるショートカット](./Shortcut)
- [キー数仕様](./keys)
- [ID 一覧](./IdReferenceIndex)
- [ローカルストレージ仕様](./LocalStorage)
- 譜面ヘッダー仕様 ( [setColor](./dos-h0003-setColor) / [frzColor](./dos-h0004-frzColor) / [setShadowColor](./dos-h0041-setShadowColor) / [frzShadowColor](./dos-h0062-frzShadowColor) / [keyRetry](./dos-h0039-keyRetry) / [keyTitleBack](./dos-h0040-keyTitleBack) )
- [譜面エフェクト仕様](./dos_effect)
- [カスタム関数の定義](./AboutCustomFunction)

## ⭐ v30 ([Changelog](Changelog-v30))

- カスタムキーの中に**部分キーを定義**する機能を追加
- Reverse 設定無効時、R-Flat を Flat と見做す設定を追加
- 別キーモード用の歌詞表示・背景/マスクモーションを実装
- 背景・マスクモーションに**色付きオブジェクト**が使えるよう変更
- 背景・マスクモーションの座標・サイズ部分に**埋め込み変数**が使えるよう変更
- 背景・マスクモーションの項目に「animation-fill-mode」を追加
- 矢印・フリーズアローを**スクロール反転**する機能を実装
- スクロール個別の word/back/mask データを追加
- カスタムキー定義において divX, colorX, shuffleX, scrollX, assistX の部分省略指定に対応
- 12key, 23key についてスクロール拡張設定を追加
- 既存キー上書き時に相対パターン番号を略記で指定できるよう変更
- キーコンフィグ画面においてキーパターンのスキップボタンを追加
- ユーザ定義とは別のカスタムキー定義ができるよう対応

### 💡 Feature Updates

- [v30.6.0](https://github.com/cwtickle/danoniplus/releases/tag/v30.6.0) / [v30.5.0](https://github.com/cwtickle/danoniplus/releases/tag/v30.5.0) / [v30.4.0](https://github.com/cwtickle/danoniplus/releases/tag/v30.4.0) / [v30.3.0](https://github.com/cwtickle/danoniplus/releases/tag/v30.3.0) / [**v30.2.0**](https://github.com/cwtickle/danoniplus/releases/tag/v30.2.0) / [**v30.1.1**](https://github.com/cwtickle/danoniplus/releases/tag/v30.1.1) / [**v30.0.1**](https://github.com/cwtickle/danoniplus/releases/tag/v30.0.1)

### 📔 Documentation

- [ゲーム画面の説明](./AboutGameSystem)
- [キー数仕様](./keys)
- [歌詞表示 (word_data)](./dos-e0003-wordData)
- [背景・マスクモーション (back_data, mask_data)](./dos-e0004-animationData)
- [キー数変化 (keych_data)](./dos-e0006-keychData)
- [スクロール反転 (scrollch_data)](./dos-e0007-scrollchData)
- 譜面ヘッダー仕様 ( [keyGroupOrder](./dos-h0092-keyGroupOrder) )
- 共通設定ファイル仕様 ( [デフォルトデザイン・画像設定](./dos-s0005-defaultDesign) / [カスタムキー定義](./dos-s0010-customKeys) )
- [色付きオブジェクト仕様](./AboutColorObject)
- [既存キーのキーパターン上書き](./tips-0006-keypattern-update)

## ⭐ v29 ([Changelog](Changelog-v29))

- フリーズアロー（始点）に対して**Fast/Slow を常時カウント**するよう変更
- フリーズアロー（始点）の判定範囲をマターリ、ショボーンへ拡大
- 途中終了時でもクリア判定を行うよう変更
- 楽曲クレジット、制作者クレジット表示の多言語対応
- ショボーンに対して Fast/Slow 表示するよう変更
- 拡張スクロール設定に「Reverse」を追加すると Reverse 設定を無効化するよう変更

### 💡 Feature Updates

- [v29.4.1](https://github.com/cwtickle/danoniplus/releases/tag/v29.4.1) / [v29.3.0](https://github.com/cwtickle/danoniplus/releases/tag/v29.3.0) / [v29.2.0](https://github.com/cwtickle/danoniplus/releases/tag/v29.2.0) / [**v29.1.0**](https://github.com/cwtickle/danoniplus/releases/tag/v29.1.0) / [**v29.0.1**](https://github.com/cwtickle/danoniplus/releases/tag/v29.0.1)

### 📔 Documentation

- [判定仕様](AboutJudgment)
- 譜面ヘッダー仕様 ( [musicTitle](dos-h0001-musicTitle) / [tuning](dos-h0017-tuning) / [titlesize](dos-h0030-titlesize) / [titlefont](dos-h0031-titlefont) / [titlepos](dos-h0033-titlepos) / [titlelineheight](dos-h0034-titlelineheight) )
- 共通設定ファイル仕様 ( [制作者クレジット・共通設定](dos-s0001-makerInfo) )

## ⭐ v28 ([Changelog](Changelog-v28))

- 譜面明細画面でカーソル移動用のショートカットを追加
- カラーセット、カラーグループ、シャッフルグループの**キー別一括保存**に対応
- カスタムキーのカラー・シャッフルグループ実装
- 17key(KeyPattern: 1)のサイズ見直し
- Motion の軌道加算部分が**個別加速の影響を受けないよう変更**
- Motion オプションに Hi-Boost を追加（v23 以前の Boost に相当）
- 主に縦連打で前の矢印の判定が吸われそうなときに、**強制的に矢印の判定を後の矢印に移す**処理を追加
- ローカル時に表示されるフレーム数を**譜面データ基準のフレーム数**に変更
- プレイ画面左上のフレーム数、左下の経過時間表記を譜面データ基準(baseFrame)に変更

### 💡 Feature Updates

- [**v28.6.0**](https://github.com/cwtickle/danoniplus/releases/tag/v28.6.0) / [**v28.5.0**](https://github.com/cwtickle/danoniplus/releases/tag/v28.5.0) / [**v28.4.0**](https://github.com/cwtickle/danoniplus/releases/tag/v28.4.0) / [v28.3.0](https://github.com/cwtickle/danoniplus/releases/tag/v28.3.0) / [**v28.2.0**](https://github.com/cwtickle/danoniplus/releases/tag/v28.2.0) / [**v28.1.0**](https://github.com/cwtickle/danoniplus/releases/tag/v28.1.0) / [v28.0.1](https://github.com/cwtickle/danoniplus/releases/tag/v28.0.1)

### 📔 Documentation

- [ゲーム画面の説明](AboutGameSystem)
- [ゲーム内で使用できるショートカット](Shortcut)
- [キー数仕様](keys)
- [ローカルストレージ仕様](LocalStorage)
- [速度変化・Motion オプション仕様](SpeedChange)
- [判定仕様](AboutJudgment)
- [疑似フレーム処理仕様](AboutFrameProcessing)

## ⭐ v27 ([Changelog](Changelog-v27))

- プレイ画面からリザルト画面移行時の 100 ミリ秒を廃止
- キー別に**ウィンドウ横幅・配置を自動制御**する機能を実装
- 作品別のカスタムファイル指定に対して共通設定を参照する機能を追加
- ColorType:1 ～ 4 に対して**矢印・フリーズアロー色を任意の色に変更**する機能を実装
- タイトル画面表示前にカスタム関数を挿入できるよう変更
- カラーオブジェクトについて矢印以外の回転に対応
- $区切りの一部譜面ヘッダーについて改行区切りに対応
- カスタムキーのパターン追記方法を拡張
- D ランク追加、B, C ランク基準見直し
- 譜面明細（速度変化・譜面密度グラフなど）への直接移動ボタン及びショートカットを実装

### 💡 Feature Updates

- [v27.8.0](https://github.com/cwtickle/danoniplus/releases/tag/v27.8.0) / [v27.7.0](https://github.com/cwtickle/danoniplus/releases/tag/v27.7.0) / [v27.6.0](https://github.com/cwtickle/danoniplus/releases/tag/v27.6.0) / [v27.5.0](https://github.com/cwtickle/danoniplus/releases/tag/v27.5.0) / [v27.4.0](https://github.com/cwtickle/danoniplus/releases/tag/v27.4.0) / [v27.3.1](https://github.com/cwtickle/danoniplus/releases/tag/v27.3.1) / [**v27.2.0**](https://github.com/cwtickle/danoniplus/releases/tag/v27.2.0) / [**v27.1.0**](https://github.com/cwtickle/danoniplus/releases/tag/v27.1.0) / [v27.0.0](https://github.com/cwtickle/danoniplus/releases/tag/v27.0.0)

### 📔 Documentation

- [ゲーム画面の説明](AboutGameSystem)
- [ゲーム内で使用できるショートカット](Shortcut)
- [譜面の作成概要](HowtoMake)
- [共通設定ファイル仕様](dos_setting) ( [制作者クレジット・基本設定](dos-s0001-makerInfo) )
- 譜面ヘッダー仕様 ( [autoSpread](dos-h0089-autoSpread) / [windowWidth](dos-h0090-windowWidth) / [windowAlign](dos-h0091-windowAlign) / [customjs](dos-h0019-customjs) / [customcss](dos-h0086-customcss) / [musicTitle](dos-h0001-musicTitle) / [difData](dos-h0002-difData) / [musicUrl](dos-h0011-musicUrl) / [frzColor](dos-h0004-frzColor) / [gaugeX](dos-h0022-gaugeX) / [imgType](dos-h0082-imgType) )
- [オブジェクト一覧](ObjectReferenceIndex) ( [setShortcutEvent](fnc-c0035-setShortcutEvent) / [commonKeyDown](fnc-c0007-commonKeyDown) )
- [キー数仕様](keys)

## ⭐ v26 ([Changelog](Changelog-v26))

- **ダミー矢印・フリーズアロー**に対して色変化・モーションデータを実装
- コメント文中に定義済み変数を埋め込む機能を追加
- 譜面密度グラフで二重押し/多重押し状況を表示するよう変更
- 推定 Adj の計算方法を単純平均から誤差が正規分布になると仮定した場合の推定値になるよう変更
- danoni_setting.js 上で共通のカスタムキー定義ができるように対応
- 作品別のカスタムキー定義について、keyExtraList 指定を任意化
- danoni_setting.js 上の変数群を **g_presetObj** へ移行
- customjs の読込位置を headerConvert 関数の前に変更
- カスタムキー定義の pos 項目について小数点に対応
- ローカル時、リザルトコピー内容に URL 情報を付加しないよう変更

### 💡 Feature Updates

- [v26.7.0](https://github.com/cwtickle/danoniplus/releases/tag/v26.7.0) / [v26.6.0](https://github.com/cwtickle/danoniplus/releases/tag/v26.6.0) / [v26.5.0](https://github.com/cwtickle/danoniplus/releases/tag/v26.5.0) / [v26.4.0](https://github.com/cwtickle/danoniplus/releases/tag/v26.4.0) / [**v26.3.1**](https://github.com/cwtickle/danoniplus/releases/tag/v26.3.1) / [**v26.2.0**](https://github.com/cwtickle/danoniplus/releases/tag/v26.2.0) / [**v26.1.0**](https://github.com/cwtickle/danoniplus/releases/tag/v26.1.0) / [**v26.0.0**](https://github.com/cwtickle/danoniplus/releases/tag/v26.0.0)

### 📔 Documentation

- [共通設定ファイル仕様](dos_setting)
- 譜面ヘッダー仕様 ( [commentVal](dos-h0066-commentVal) )
- [オブジェクト一覧](ObjectReferenceIndex) ( [loadScript2](fnc-c0010-loadScript) / [importCssFile2](fnc-c0020-importCssFile) / [loadMultipleFiles](fnc-c0044-loadMultipleFiles) / [loadChartFile](fnc-c0011-loadChartFile) )
- [譜面データにおける特殊文字の取り扱い](SpecialCharacters)
- [キー数仕様](keys)

## ⭐ v25 ([Changelog](Changelog-v25))

- フェードイン時にそれまでの歌詞表示、背景・マスク表示データを保持するように変更
- 条件付きで矢印の色変化に追随して**フリーズアローにも同じ色変化を適用**する機能を実装
- フェードイン時に個別色変化と全体色変化が混在している場合でも色変化が反映されるよう変更
- キーコンフィグ画面でオブジェクトの一部が隠れる場合に横スクロールできるよう対応
- customjs, skinType の**2 ファイル制限を撤廃**
- カスタム関数（XXXInit, XXXInit2 など）について、XXXInit が無くても XXXInit2 の直接呼び出しが可能になるよう変更
- 独自の css ファイルを読み込む設定を実装
- 14i, 16ikey の AA 部分の代替キーを設定
- 背景・マスクモーションで使用する画像パスのカレントディレクトリ指定に対応
- 結果画面に**推定 Adjustment 値を表示**する機能を追加

### 💡 Feature Updates

- [**v25.5.1**](https://github.com/cwtickle/danoniplus/releases/tag/v25.5.1) / [v25.4.0](https://github.com/cwtickle/danoniplus/releases/tag/v25.4.0) / [**v25.3.0**](https://github.com/cwtickle/danoniplus/releases/tag/v25.3.0) / [v25.2.0](https://github.com/cwtickle/danoniplus/releases/tag/v25.2.0) / [**v25.1.0**](https://github.com/cwtickle/danoniplus/releases/tag/v25.1.0) / [**v25.0.0**](https://github.com/cwtickle/danoniplus/releases/tag/v25.0.0)

### 📔 Documentation

- [ID 一覧](IdReferenceIndex)
- [共通設定ファイル仕様](dos_setting)
- [カスタム関数の定義](AboutCustomFunction)
- [カスタム js(スキン js)による処理割込み](AboutCustomJs)
- 譜面ヘッダー設定 ( [unStockCategory](dos-h0084-unStockCategory) / [wordStockForceDel / backStockForceDel / maskStockForceDel](dos-h0085-stockForceDel) / [customjs](dos-h0019-customjs) / [customcss](dos-h0086-customcss) / [skinType](dos-h0054-skinType) / [syncBackPath](dos-h0087-syncBackPath) / [frzScopeFromAC](dos-h0088-frzScopeFromAC) / [defaultFrzColorUse](dos-h0063-defaultFrzColorUse) )

## ⭐ v24 ([Changelog](Changelog-v24))

- 各種メッセージの**日本語・英語の切り替え**機能を追加
- プレイ画面の譜面名にシャッフル名称を追加
- 他キー移動時でも該当の拡張スクロールがあれば設定を引き継ぐよう変更
- アシストプレイ時の設定名を変更できる機能を追加
- 歌詞表示、背景・マスクモーション、コメント文の多言語表示に対応
- playbackRate 使用時に小数フレーム値が考慮されるよう変更
- **キーコンフィグパターン個別に**スクロール拡張、アシスト設定が行える機能を実装
- 9ikey, 17key に対して**部分的にスクロール拡張**を追加
- カスタムキーパターン設定の**略記指定**に対応
- 9A/9B/9i/11ikey において Twist/Asymmetry を実装
- Hidden+/Sudden+/Hid&Sud+の**レーンカバーの初期位置、ロックの設定**を実装
- フェードイン時にそれまでの色変化・モーションデータを保持するように変更
- 13key のスクロール拡張に Cross を追加

### 💡 Feature Updates

- [**v24.6.0**](https://github.com/cwtickle/danoniplus/releases/tag/v24.6.0) / [**v24.5.0**](https://github.com/cwtickle/danoniplus/releases/tag/v24.5.0) / [v24.4.0](https://github.com/cwtickle/danoniplus/releases/tag/v24.4.0) / [**v24.3.0**](https://github.com/cwtickle/danoniplus/releases/tag/v24.3.0) / [v24.2.1](https://github.com/cwtickle/danoniplus/releases/tag/v24.2.1) / [v24.1.0](https://github.com/cwtickle/danoniplus/releases/tag/v24.1.0) / [**v24.0.0**](https://github.com/cwtickle/danoniplus/releases/tag/v24.0.0)

### 📔 Documentation

- [ID 一覧](IdReferenceIndex)
- [ゲーム画面の説明](AboutGameSystem)
- [ゲーム内で使用できるショートカット](Shortcut)
- [オブジェクト一覧](ObjectReferenceIndex) ( [makeWarningWindow](fnc-c0013-makeWarningWindow) )
- [キーの仕様について](keys)
- 譜面エフェクト仕様 ( [word_data](dos-e0003-wordData) / [back/mask_data](dos-e0004-animationData) )
- 譜面ヘッダー仕様 ( [commentVal / commentValJa / commentValEn ](dos-h0066-commentVal) / [autoPreload](dos-h0055-autoPreload) )

## ⭐ v23 ([Changelog](Changelog-v23))

- Adjustment を**0.1 フレーム単位で設定**できる機能を追加
- Adjustment (画面)の新たな刻み幅ボタンを追加 (最内側: 0.5f)
- Adjustment (画面)について、ショートカットキー専用で 0.1f 幅の調整ができるよう変更
- デフォルト画像セット(ImgType)を**複数指定**できるように変更
- ImgType の追加画像セットとして「note」を追加、**各種設定**を追加
- ImgType: Original の位置を自由に変更できるよう対応
- 7key の別キーモードとして 7ikey を追加
- 速度設定をショートカットキー限定で 0.05 倍速刻みで設定できるように変更
- 12, 14key にキーボードの左側を使ったキーパターンを追加
- ランク AP の実装
- プレイ画面に譜面名を表示、それに合わせレイアウトを見直し
- キーコンフィグ画面のプレイボタンを追加、レイアウト修正
- カスタムキーの名前を変更できる機能を追加
- 譜面変更時、キーが同一の場合に Reverse/Scroll/AutoPlay/キーパターンを引き継ぐよう変更
- 曲中リトライキーの猶予フレームを廃止

※Adjustment (画面)について、v23.0.0 ～ v23.1.1 の間のみ  
　外側のボタン：3frame 刻み、内側のボタン：0.5frame 刻みの仕様です。

### 💡 Feature Updates

- [v23.5.0](https://github.com/cwtickle/danoniplus/releases/tag/v23.5.0) / [**v23.4.0**](https://github.com/cwtickle/danoniplus/releases/tag/v23.4.0) / [v23.3.0](https://github.com/cwtickle/danoniplus/releases/tag/v23.3.0) / [**v23.2.0**](https://github.com/cwtickle/danoniplus/releases/tag/v23.2.0) / [**v23.1.0**](https://github.com/cwtickle/danoniplus/releases/tag/v23.1.0) / [**v23.0.0**](https://github.com/cwtickle/danoniplus/releases/tag/v23.0.0)

### 📔 Documentation

- [ID 一覧](IdReferenceIndex)
- [共通設定ファイル仕様](dos_setting)
- 譜面ヘッダー仕様 ( [imgType](dos-h0082-imgType) / [adjustment](dos-h0009-adjustment) / [customCreditWidth](dos-h0083-customCreditWidth) )
- [色付きオブジェクト仕様](AboutColorObject)
- [キー数仕様](keys)
- [ゲーム内で使用できるショートカット](Shortcut)

## ⭐ v22 ([Changelog](Changelog-v22))

- 通常キーにおいて**シャッフルグループ、色グループの複数指定**に対応
- 一時的に**初期矢印色、シャッフルグループ番号の割当先を変更**できる機能を実装
- キーコンフィグ画面内の設定ボタンに対し、オンマウスで説明が出るよう変更
- Shuffle 設定において**非対称ミラー** (Asym-Mirror) を実装
- Shuffle 設定でシャッフルグループ内の入れ替えのみ可能な設定を追加
- ColorType に「Type3」「Type4」を追加
- サーバ上のデフォルト画像セットの切り替え、設定戻しに対応
- キャラクタ画像の追加に部分的に対応

### 💡 Feature Updates

- [**v22.5.1**](https://github.com/cwtickle/danoniplus/releases/tag/v22.5.1) / [**v22.4.1**](https://github.com/cwtickle/danoniplus/releases/tag/v22.4.1) / [**v22.3.2**](https://github.com/cwtickle/danoniplus/releases/tag/v22.3.2) / [**v22.2.0**](https://github.com/cwtickle/danoniplus/releases/tag/v22.2.0) / [**v22.1.0**](https://github.com/cwtickle/danoniplus/releases/tag/v22.1.0) / [**v22.0.0**](https://github.com/cwtickle/danoniplus/releases/tag/v22.0.0)

### 📔 Documentation

- [ゲーム画面の説明](AboutGameSystem)
- [ID 一覧](IdReferenceIndex)
- [オブジェクト一覧](ObjectReferenceIndex) ( [g_lblNameObj / g_local_lblNameObj](obj-v0015-g_lblNameObj) / [g_msgObj / g_lbl_msgObj](obj-v0016-g_msgObj) )
- [共通設定ファイル仕様](dos_setting)
- 譜面ヘッダー仕様 ( [settingUse](dos-h0035-settingUse) / [imgType](dos-h0082-imgType) )
- [色付きオブジェクト仕様](AboutColorObject)

## ⭐ v21 ([Changelog](Changelog-v21))

- 初期矢印・フリーズアロー色(setColor/frzColor)の**譜面別設定**実装
- 譜面分割＆譜面番号固定時、個別の譜面ファイル内に  
  setColor/frzColor/setShadowColor/frzShadowColor の記述があれば、その値を採用するよう変更
- **譜面セレクター表示時**のショートカットキー実装
- 矢印・フリーズアローモーションの 20 以上のキー対応
- キーコンフィグ画面の影矢印部分が ColorType の変化に対応するよう変更
- **譜面毎にカスタムゲージリスト**が作成できるように変更
- ゲージ個別設定（譜面ヘッダー）について、**譜面毎の分割記法**に対応
- カスタムゲージリストについて、ライフ制ゲージ/ノルマ制ゲージ/共通設定ファイルで  
  指定のリストを設定できるよう変更
- 背景状況により**明暗用のカラーセットを使用**するよう変更
- 警告メッセージ群の管理方法を見直し
- 空スプライト作成処理を`createEmptySprite`関数へ移行
- キーコンフィグの対応キー箇所を**ボタンに変更**
- 譜面明細表示の逆回し、Save 切替/Display 切替ボタンの右クリック対応

### 💡 Feature Updates

- [v21.5.0](https://github.com/cwtickle/danoniplus/releases/tag/v21.5.0) / [**v21.4.2**](https://github.com/cwtickle/danoniplus/releases/tag/v21.4.2) / [v21.3.0](https://github.com/cwtickle/danoniplus/releases/tag/v21.3.0) / [**v21.2.0**](https://github.com/cwtickle/danoniplus/releases/tag/v21.2.0) / [**v21.1.0**](https://github.com/cwtickle/danoniplus/releases/tag/v21.1.0) / [**v21.0.0**](https://github.com/cwtickle/danoniplus/releases/tag/v21.0.0)

### 📔 Documentation

- [ID 一覧](IdReferenceIndex)
- [ゲーム内で使用できるショートカット](Shortcut)
- [オブジェクト一覧](ObjectReferenceIndex) ( [g_shortcutObj](obj-v0017-g_shortcutObj) / [g_gaugeOptionObj](obj-v0007-g_gaugeOptionObj) / [fuzzyListMatching](fnc-c0039-fuzzyListMatching) / [makeWarningWindow](fnc-c0013-makeWarningWindow) / [importCssFile](fnc-c0020-importCssFile) / [createEmptySprite](fnc-c0043-createEmptySprite) / [g_errMsgObj](obj-v0026-g_errMsgObj) )
- 譜面ヘッダー仕様 ( [customGauge](dos-h0053-customGauge) / [gaugeX](dos-h0022-gaugeX) / [setColor](dos-h0003-setColor) / [frzColor](dos-h0004-frzColor) / [setShadowColor](dos-h0041-setShadowColor) / [frzShadowColor](dos-h0062-frzShadowColor) / [baseBright](dos-h0081-baseBright) / [defaultColorgrd](dos-h0061-defaultColorgrd) )
- 譜面エフェクト仕様 ( [矢印・フリーズアローモーション](dos-e0005-motionData) )

## ⭐ v20 ([Changelog](Changelog-v20))

- ボタン処理を**画面移動系とそれ以外**に分離
- ボタンの割り込み処理を本体処理より前に挿入できるよう変更
- 右クリック操作の無いボタンに対して処理を追加できるように変更
- タイトル文字、譜面名の**フォントサイズ**の自動設定方法を改善
- ボタン・ショートカットキー有効化までのフレーム数設定を実装
- 11ikey に方向表示が異なるキーパターンを追加
- 設定名、結果画面用の省略名を置き換える機能を実装
- タイトル画面、ロード画面(iOS のみ)のショートカットキーを追加

### 💡 Feature Updates

- [v20.5.1](https://github.com/cwtickle/danoniplus/releases/tag/v20.5.1) / [v20.4.0](https://github.com/cwtickle/danoniplus/releases/tag/v20.4.0) / [**v20.3.1**](https://github.com/cwtickle/danoniplus/releases/tag/v20.3.1) / [**v20.2.0**](https://github.com/cwtickle/danoniplus/releases/tag/v20.2.0) / [v20.1.2](https://github.com/cwtickle/danoniplus/releases/tag/v20.1.2) / [**v20.0.0**](https://github.com/cwtickle/danoniplus/releases/tag/v20.0.0)

### 📔 Documentation

- [ゲーム内で使用できるショートカット](Shortcut)
- [共通設定ファイル仕様](dos_setting) - 設定名の上書き可否設定 (g_lblRenames)
- [オブジェクト一覧](ObjectReferenceIndex) ( [createCss2Button](fnc-c0004-createCss2Button) / [g_btnAddFunc / g_cxtAddFunc](obj-v0018-g_btnAddFunc) / [g_btnDeleteFlg / g_cxtDeleteFlg](obj-v0019-g_btnDeleteFlg) / [g_shortcutObj](obj-v0017-g_shortcutObj) / [g_btnWaitFrame](obj-v0020-g_btnWaitFrame) / [g_btnPatterns](obj-v0021-g_btnPatterns) / [g_lblNameObj / g_local_lblNameObj](obj-v0015-g_lblNameObj) / [getBasicFont](fnc-c0029-getBasicFont) / [setShortcutEvent](fnc-c0035-setShortcutEvent) / [clearWindow](fnc-c0021-clearWindow) )

## ⭐ v19 ([Changelog](Changelog-v19))

- ゲージ設定(Gauge)の**ゲージ種・譜面別の入力補完**に対応
- danoni_setting.js に**カスタムゲージリスト、デフォルトスキン、デフォルトカスタム Js 指定**を追加
- タイトルの背景矢印の透明度を指定できるように変更
- グラデーションで色名を指定する場合に、透明度を合わせて指定できるよう変更
- ラベルテキスト及び説明文を danoni_constants.js へ移動・集約
- musicFolder のカレントディレクトリ指定に対応
- settingType, skinType, customjs について**カレント＋サブディレクトリ指定**に対応
- Ready? 文字を直接書き換える設定を追加
- 作品 html の**配置場所の変更**に対応
- 8key において Enter おにぎりを左側にするモードと 12key モードを実装
- **画面別ショートカットキー**を実装
- Ready? の文字幅を画面幅に合わせるよう変更

### 💡 Feature Updates

- [**v19.5.0**](https://github.com/cwtickle/danoniplus/releases/tag/v19.5.0) / [**v19.4.1**](https://github.com/cwtickle/danoniplus/releases/tag/v19.4.1) / [**v19.3.0**](https://github.com/cwtickle/danoniplus/releases/tag/v19.3.0) / [**v19.2.0**](https://github.com/cwtickle/danoniplus/releases/tag/v19.2.0) / [v19.1.0](https://github.com/cwtickle/danoniplus/releases/tag/v19.1.0) / [v19.0.0](https://github.com/cwtickle/danoniplus/releases/tag/v19.0.0)

### 📔 Documentation

- [ID 一覧](idReferenceIndex)
- [ゲーム内ショートカット](Shortcut)
- 譜面ヘッダー仕様 ( [gaugeX](dos-h0022-gaugeX) / [titlegrd / titlearrowgrd](dos-h0032-titlegrd) / [musicFolder](dos-h0013-musicFolder) / [settingType](dos-h0056-settingType) / [skinType](dos-h0054-skinType) / [customjs](dos-h0019-customjs) / [readyHtml](dos-h0080-readyHtml) )
- [共通設定ファイル仕様](dos_setting)
- [グラデーション仕様](dos-c0001-gradation)
- [オブジェクト一覧](ObjectReferenceIndex) ( [g_lblNameObj](obj-v0015-g_lblNameObj) / [g_msgObj](obj-v0016-g_msgObj) / [g_shortcutObj](obj-v0017-g_shortcutObj) / [commonKeyDown](fnc-c0007-commonKeyDown) )

## ⭐ v18 ([Changelog](Changelog-v18))

- 矢印・フリーズアローの個別属性設定を見直し
- Display 設定, Appearance, Opacity, ColorType のローカルストレージ保存に対応
- Adjustment, Volume, Appearance, Opacity, ColorType に限り、  
  ローカル保存している設定を選択時にアスタリスクを付けるように変更
- **23key**を既定キーとして実装
- 矢印・フリーズアロー色の**強制グラデーション指定/解除**設定を実装
- 背景・マスクモーションで使用可能な画像ファイル拡張子に**svg 形式**を追加
- 背景・マスクモーションのテキスト部分において**HTML タグを許容**するよう変更
- **タイトル文字のアニメーション**を 1・2 行目で個別に設定できるよう変更
- 9A/9Bkey の別キーモード時の配色を一部変更
- Firefox で画像のプリロードを有効にするよう変更
- ライフ制ゲージ設定に「Heavy」を追加
- ColorType: 2 のおにぎりの初期色を白から薄青に変更
- Gitter(得点報告用), ゲーム画面説明へのリンクを追加

### 💡 Feature Updates

- [**v18.9.0**](https://github.com/cwtickle/danoniplus/releases/tag/v18.9.0) / [v18.8.0](https://github.com/cwtickle/danoniplus/releases/tag/v18.8.0) / [v18.7.0](https://github.com/cwtickle/danoniplus/releases/tag/v18.7.0) / [v18.6.0](https://github.com/cwtickle/danoniplus/releases/tag/v18.6.0) / [**v18.5.0**](https://github.com/cwtickle/danoniplus/releases/tag/v18.5.0) / [**v18.4.0**](https://github.com/cwtickle/danoniplus/releases/tag/v18.4.0) / [**v18.3.0**](https://github.com/cwtickle/danoniplus/releases/tag/v18.3.0) / [**v18.2.0**](https://github.com/cwtickle/danoniplus/releases/tag/v18.2.0) / [**v18.1.0**](https://github.com/cwtickle/danoniplus/releases/tag/v18.1.0) / [v18.0.0](https://github.com/cwtickle/danoniplus/releases/tag/v18.0.0)

### 📔 Documentation

- [ゲーム画面の説明](AboutGameSystem)
- [ID 一覧](IdReferenceIndex)
- 譜面ヘッダー仕様 ( [gaugeX](dos-h0022-gaugeX) / [defaultColorgrd](dos-h0061-defaultColorgrd) / [titleanimation](dos-h0077-titleanimation) / [customTitleAnimationUse](dos-h0078-customTitleAnimationUse) / [titleanimationclass](dos-h0079-titleanimationclass) )
- [譜面本体仕様](dos_score)
- 譜面エフェクト仕様 ( [back/mask_data](dos-e0004-animationData) )
- [共通設定ファイル仕様](dos_setting)
- [主要オブジェクトのプロパティ](objectMainRef)
- [ローカルストレージ仕様](LocalStorage)
- [譜面データにおける特殊文字の取り扱い](SpecialCharacters)

## ⭐ v17 ([Changelog](Changelog-v17))

- ラベル・ボタン・色付きオブジェクト作成用関数を作り直し
- ボタンクリック、右クリック後の処理に対して処理を追加できる機能を実装
- ボタン及びラベルのスタイル一括変更関数を実装
- タイトル文字のフォントと位置について 2 行目指定ができるように変更
- strict モードを再有効化

### 💡 Feature Updates

- [v17.5.0](https://github.com/cwtickle/danoniplus/releases/tag/v17.5.0) / [**v17.4.0**](https://github.com/cwtickle/danoniplus/releases/tag/v17.4.0) / [v17.3.0](https://github.com/cwtickle/danoniplus/releases/tag/v17.3.0) / [v17.2.0](https://github.com/cwtickle/danoniplus/releases/tag/v17.2.0) / [v17.1.0](https://github.com/cwtickle/danoniplus/releases/tag/v17.1.0) / [v17.0.1](https://github.com/cwtickle/danoniplus/releases/tag/v17.0.1)

### 📔 Documentation

- 譜面ヘッダー仕様 ( [titlesize](dos-h0030-titlesize) / [titlefont](dos-h0031-titlefont) / [titlepos](dos-h0033-titlepos) )
- [オブジェクト一覧](objectReferenceIndex) ( [createDivCss2Label](fnc-c0002-createDivCss2Label) / [createColorObject2](fnc-c0003-createColorObject2) / [createCss2Button](fnc-c0004-createCss2Button) / [changeStyle](fnc-c0005-changeStyle) )

## ⭐ v16 ([Changelog](Changelog-v16))

- プレイ画面範囲の横幅・位置調整機能を追加
- `KeyboardEvent.keyCode`を`KeyboardEvent.code`へ置き換え
- リザルトデータの**クリップボードコピー機能**を実装
- CapsLock キーの無効化設定及び無効キーのメッセージを追加
- **20 個以上のキー**に対する色変化（単発矢印・個別）に対応
- リザルトデータの**フォーマット機能**を実装
- Ready 表示の**アニメーション変更、先頭色変更**に対応
- Ready 表示の遅延フレーム設定について、プレイ中のフェードイン有効時は無効にするよう変更
- 結果画面の CLEARED / FAILED 表示に対する遅延フレーム設定を実装

### 💡 Feature Updates

- [v16.4.0](https://github.com/cwtickle/danoniplus/releases/tag/v16.4.0) / [**v16.3.0**](https://github.com/cwtickle/danoniplus/releases/tag/v16.3.0) / [**v16.2.1**](https://github.com/cwtickle/danoniplus/releases/tag/v16.2.1) / [**v16.1.0**](https://github.com/cwtickle/danoniplus/releases/tag/v16.1.0) / [v16.0.0](https://github.com/cwtickle/danoniplus/releases/tag/v16.0.0)

### 📔 Documentation

- [ID 一覧](IdReferenceIndex)
- 譜面ヘッダー仕様 ( [playingX](dos-h0070-playingX) / [playingWidth](dos-h0071-playingWidth) / [resultFormat](dos-h0072-resultFormat) / [readyAnimationFrame](dos-h0073-readyAnimationFrame) / [readyAnimationName](dos-h0074-readyAnimationName) / [readyColor](dos-h0075-readyColor) )
- [共通設定ファイル仕様](dos_setting)
- 譜面エフェクト仕様 ( [acolor/color_data](dos-e0002-colorData) )

## ⭐ v15 ([Changelog](Changelog-v15))

- AutoPlay 設定に対して一部キーを**アシスト**する設定を追加
- 外部リンクに対してオンマウスで URL を表示
- 譜面効果データでフレーム・深度等に**数式**が使えるよう変更
- 主要画像を png 形式から**svg 形式**へ変更
- 作品コメント文をタイトル画面に表示する機能を実装
- 作品を**動的に切り替え**られる機能及び関連機能を実装
- Background:OFF 時に判定表示位置(jdgY)を初期化する設定を追加
- Reverse 時に条件付きで**歌詞を上下逆**の位置に表示するよう変更
- 歌詞表示で**フォントサイズが変更**できる構文を追加
- 7, 11, 11L, 11Wkey において 12key モードを実装
- Win キー+Shift キーでキーコンが反応しないように処理を見直し

### 💡 Feature Updates

- [v15.7.0](https://github.com/cwtickle/danoniplus/releases/tag/v15.7.0) / [v15.6.0](https://github.com/cwtickle/danoniplus/releases/tag/v15.6.0) / [v15.5.0](https://github.com/cwtickle/danoniplus/releases/tag/v15.5.0) / [v15.4.0](https://github.com/cwtickle/danoniplus/releases/tag/v15.4.0) / [v15.3.0](https://github.com/cwtickle/danoniplus/releases/tag/v15.3.0) / [**v15.2.0**](https://github.com/cwtickle/danoniplus/releases/tag/v15.2.0) / [**v15.1.0**](https://github.com/cwtickle/danoniplus/releases/tag/v15.1.0) / [**v15.0.0**](https://github.com/cwtickle/danoniplus/releases/tag/v15.0.0)

### 📔 Documentation

- [キーの仕様について](keys)
- [ソースの構成](AboutSource)
- [ID 一覧](idReferenceIndex)
- [共通設定ファイル仕様](dos_setting)
- 譜面ヘッダー仕様 ( [jdgPosReset](dos-h0065-jdgPosReset) / [commentVal](dos-h0066-commentVal) / [commentAutoBr](dos-h0067-commentAutoBr) / [commentExternal](dos-h0068-commentExternal) / [wordAutoReverse](dos-h0069-wordAutoReverse) )
- 譜面エフェクト仕様 ( [speed/boost_data](dos-e0001-speedData) / [acolor/color_data](dos-e0002-colorData) / [word_data](dos-e0003-wordData) / [back/mask_data](dos-e0004-animationData) )
- [譜面データにおける特殊文字の取り扱い](SpecialCharacters)

## ⭐ v14 ([Changelog](Changelog-v14))

- Display オプションの**デフォルト ON/OFF**設定及び**有効/無効化**設定の実装
- Display オプションのボタンが ON の状態のとき、  
  他のボタンを**連動して OFF**に自動変更する機能を実装
- オンマウスでオプション設定の説明文を表示するよう変更
- 矢印描画について、ステップゾーン位置に応じて描画領域が変わるように変更
- Display: MusicInfo 適用時、楽曲のクレジットを最初の数秒のみ表示する形式に変更
- グラデーション表記で**色名** (blue や red など) が使えるよう変更
- 影矢印のデザインを見直し
- APM の計算式をレベル計算ツール++の形式に寄せるよう修正
- ローカル時に限り、誤差 1 フレームでも Fast/Slow を表示するように変更  
  (合わせて、±1 フレーム以内で表示する Just 表記を廃止)
- 結果画面に**Fast/Slow のカウント数を表示**するよう変更
- Twitter 投稿用リザルトの**URL 表示に譜面番号を付加**するよう変更
- 譜面データで**パイプ、アンパサンド等の特殊文字**を利用するための文字列を作成
- 譜面数が 5 を超える場合、自動で譜面セレクターを使用するよう変更
- バージョン比較用リンクを作成

### 💡 Feature Updates

- [v14.5.0](https://github.com/cwtickle/danoniplus/releases/tag/v14.5.0) / [v14.4.0](https://github.com/cwtickle/danoniplus/releases/tag/v14.4.0) / [**v14.3.4**](https://github.com/cwtickle/danoniplus/releases/tag/v14.3.4) / [v14.2.0](https://github.com/cwtickle/danoniplus/releases/tag/v14.2.0) / [v14.1.0](https://github.com/cwtickle/danoniplus/releases/tag/v14.1.0) / [**v14.0.2**](https://github.com/cwtickle/danoniplus/releases/tag/v14.0.2)

### 📔 Documentation

- [共通設定ファイル仕様](dos_setting)
- [グラデーション仕様](dos-c0001-gradation)
- 譜面ヘッダー仕様 ( [displayUse](dos-h0057-displayUse) / [displayChainOFF](dos-h0064-displayChainOFF) / [difSelectorUse](dos-h0051-difSelectorUse) )
- [譜面データにおける特殊文字の取り扱い](SpecialCharacters)

## ⭐ v13 ([Changelog](Changelog-v13))

- **矢印塗りつぶし部分の初期色**に対してグラデーション他に対応
- フリーズアロー初期色 (frzColor)が未定義の場合、  
  矢印初期色 (setColor) の値から適用する形式に変更
- フリーズアローの塗りつぶし部分に対する設定を追加
- 従来の frzColor 補完ができる設定を追加
- Appearance 設定に「**Hidden+**」「**Sudden+**」「**Hid&Sud+**」を追加
- 既定のキーに対して最低 1 つの代替キーを設定するよう変更
- Display:Judgment を Judgment(判定キャラクタ)、FastSlow、Score(判定数)に分離
- Display オプションのデフォルト無効化設定を追加
- 判定表示、Hidden+/Sudden+の境界線表示の透明度を調整する「Opacity」を実装
- 開始/終了フレームなどを指定する startFrame, fadeFrame, endFrame について  
  疑似タイマー表記に対応

### 💡 Feature Updates

- [v13.6.0](https://github.com/cwtickle/danoniplus/releases/tag/v13.6.0) / [v13.5.1](https://github.com/cwtickle/danoniplus/releases/tag/v13.5.1) / [**v13.4.0**](https://github.com/cwtickle/danoniplus/releases/tag/v13.4.0) / [v13.3.1](https://github.com/cwtickle/danoniplus/releases/tag/v13.3.1) / [**v13.2.0**](https://github.com/cwtickle/danoniplus/releases/tag/v13.2.0) / [v13.1.1](https://github.com/cwtickle/danoniplus/releases/tag/v13.1.1) / [**v13.0.0**](https://github.com/cwtickle/danoniplus/releases/tag/v13.0.0)

### 📔 Documentation

- [ゲーム画面の説明](AboutGameSystem)
- [共通設定ファイル仕様](dos_setting)
- 譜面ヘッダー仕様 ( [setColor](dos-h0003-setColor) / [frzColor](dos-h0004-frzColor) / [setShadowColor](dos-h0041-setShadowColor) / [frzShadowColor](dos-h0062-frzShadowColor) / [defaultFrzColorUse](dos-h0063-defaultFrzColorUse) / [displayUse](dos-h0057-displayUse) / [startFrame](dos-h0005-startFrame) / [fadeFrame](dos-h0008-fadeFrame) / [endFrame](dos-h0007-endFrame) )

## ⭐ v12 ([Changelog](Changelog-v12))

- 矢印色、フリーズアロー色の**グラデーション**を実装
- 常時グラデーション設定の追加
- タイトル曲名、背景矢印のグラデーション拡張を実装
- **レベル計算**ツール++の移植
- 矢印描画エリアを縮小する設定を追加
- 7ikey の Scroll: Asymmetry について、左矢印のスクロールを反転する仕様に変更
- ローカルで直接 html ファイルを開いても画像が表示されるように変更

### 💡 Feature Updates

- [**v12.3.0**](https://github.com/cwtickle/danoniplus/releases/tag/v12.3.0) / [v12.2.0](https://github.com/cwtickle/danoniplus/releases/tag/v12.2.0) / [v12.1.0](https://github.com/cwtickle/danoniplus/releases/tag/v12.1.0) / [**v12.0.0**](https://github.com/cwtickle/danoniplus/releases/tag/v12.0.0)

### 📔 Documentation

- [キーの仕様について](keys)
- [グラデーション仕様](dos-c0001-gradation)
- 譜面ヘッダー仕様 ( [setColor](dos-h0003-setColor) / [frzColor](dos-h0004-frzColor) / [titlegrd / titlearrowgrd](dos-h0032-titlegrd) / [defaultColorgrd](dos-h0061-defaultColorgrd) )
- 譜面エフェクト仕様 ( [acolor/color_data](dos-e0002-colorData) )
- [ローカルでのプレイ方法](HowToLocalPlay)

## ⭐ v11 ([Changelog](Changelog-v11))

- Display オプションのデフォルト設定を追加
- カスタム用の Display オプション「Special」を追加
- 判定キャラクタの下に**Fast/Slow/Just**表記を追加
- 判定キャラクタの位置調整機能を追加
- 5key の KeyPattern: 3(おにぎり中央)の既定のキーコンフィグを変更
- 7key の既定のキーコンフィグを追加
- 下側の歌詞表示位置をステップゾーン位置に追随させる設定を追加
- 判定キャラクタ、Ready の Y 座標を下側のステップゾーン位置(stepYR)に合わせて  
  自動調整するように変更
- **速度変化の遷移グラフ、譜面密度分布グラフ**の実装

### 💡 Feature Updates

- [v11.4.0](https://github.com/cwtickle/danoniplus/releases/tag/v11.4.0) / [**v11.3.0**](https://github.com/cwtickle/danoniplus/releases/tag/v11.3.0) / [v11.1.2](https://github.com/cwtickle/danoniplus/releases/tag/v11.1.2) / [**v11.0.0**](https://github.com/cwtickle/danoniplus/releases/tag/v11.0.0)

### 📔 Documentation

- 譜面ヘッダー仕様 ( [displayUse](dos-h0057-displayUse) / [arrowJdgY / frzJdgY](dos-h0058-jdgY) / [bottomWordSet](dos-h0059-bottomWordSet) / [scoreDetailUse](dos-h0060-scoreDetailUse) )
- [画面の位置調整方法](tips-0003-position-adjustment)

## ⭐ v10 ([Changelog](Changelog-v10))

- **スキン**機能の実装
- 背景・マスクに対象拡張子のデータがあれば、**自動で preload**する機能を実装
- danoni_setting.js を**グループごとに分けられる**ように変更
- js, css, skin フォルダ配下のファイル指定に対して**カレントディレクトリ指定**に対応  
  (skinType, settingType, musicUrl, customjs でファイルの先頭に(..) を付加することで可能)
- danoni_custom.js や danoni_setting.js が無くても動作するように変更
- Canvas タグを使用しなくても動作するように変更
- **MacOS, iOS** (Safari)において動作しない問題を修正 (v5, v8, v9 にも適用済)
- **スクロール拡張**（Cross / Split / Flat 他）に対応
- 矢印のヒット部分を**押したタイミングに応じて変える**ように変更
- 譜面データで使用する矢印名で命名できる種類を拡張  
  （ある程度任意の名前を指定しても、フリーズアローが自動で対応するように変更）

### 💡 Feature Updates

- [v10.4.0](https://github.com/cwtickle/danoniplus/releases/tag/v10.4.0) / [**v10.2.1**](https://github.com/cwtickle/danoniplus/releases/tag/v10.2.1) / [v10.1.1](https://github.com/cwtickle/danoniplus/releases/tag/v10.1.1) / [**v10.0.0**](https://github.com/cwtickle/danoniplus/releases/tag/v10.0.0)

### 📔 Documentation

- [更新情報 (UpdateInfo-v10)](UpdateInfo-v10)
- [アップグレードガイド](MigrationGuide)
- [ソースの構成](AboutSource)
- [共通設定ファイル仕様](dos_setting)
- 譜面ヘッダー仕様 ( [musicUrl](dos-h0011-musicUrl) / [customjs](dos-h0019-customjs) / [skinType](dos-h0054-skinType) / [settingType](dos-h0056-settingType) / [autoPreload](dos-h0055-autoPreload) / [scrollUse](dos-h0035-settingUse) )
- 譜面エフェクト仕様 ( [word_data](dos-e0003-wordData) / [back_data / mask_data](dos-e0004-animationData) )

## ⭐ v9 ([Changelog](Changelog-v9))

- **Appearance**オプションの実装
- Ready 表示の**遅延フレーム設定**を追加
- リバース用の歌詞表示（wordRev_data）を実装
- **カスタムゲージ設定**の実装

### 💡 Feature Updates

- [v9.4.2](https://github.com/cwtickle/danoniplus/releases/tag/v9.4.2) / [**v9.4.0**](https://github.com/cwtickle/danoniplus/releases/tag/v9.4.0) / [v9.3.0](https://github.com/cwtickle/danoniplus/releases/tag/v9.3.0) / [v9.2.0](https://github.com/cwtickle/danoniplus/releases/tag/v9.2.0) / [v9.1.0](https://github.com/cwtickle/danoniplus/releases/tag/v9.1.0) / [**v9.0.0**](https://github.com/cwtickle/danoniplus/releases/tag/v9.0.0)

### 📔 Documentation

- [共通設定ファイル仕様](dos_setting)
- 譜面ヘッダー仕様 ( [appearanceUse](dos-h0035-settingUse) / [readyDelayFrame](dos-h0052-readyDelayFrame) / [customGauge](dos-h0053-customGauge) / [gaugeX](dos-h0022-gaugeX) )
- 譜面エフェクト仕様 ( [back_data / mask_data](dos-e0004-animationData) / [word_data](dos-e0003-wordData) )

## ⭐ v8 ([Changelog](Changelog-v8))

- タイトル/リザルトモーションの**音楽同期**に対応
- リザルトモーションを譜面別に作成できるように変更
- **楽曲のフェードアウト長**を指定できるように変更
- 速度変化表記 (speed_data/change) の統一
- ステップゾーン(下)位置を変更できる設定を追加
- **制作者表示の複数化**に対応（設定画面・結果画面・リザルトコピー）
- 曲名（複数行）を 1 行で表示する場合に間を空白で埋めない（全角で埋める）設定を追加
- 譜面変更用の**セレクター**を実装
- **譜面毎のファイル分割**に対応
- 譜面をファイル分割した場合に、譜面番号を常時固定するかどうかの設定を追加

### 💡 Feature Updates

- [v8.7.0](https://github.com/cwtickle/danoniplus/releases/tag/v8.7.0) / [**v8.5.0**](https://github.com/cwtickle/danoniplus/releases/tag/v8.5.0) / [v8.4.0](https://github.com/cwtickle/danoniplus/releases/tag/v8.4.0) / [**v8.3.0**](https://github.com/cwtickle/danoniplus/releases/tag/v8.3.0) / [v8.2.0](https://github.com/cwtickle/danoniplus/releases/tag/v8.2.0) / [v8.1.0](https://github.com/cwtickle/danoniplus/releases/tag/v8.1.0) / [v8.0.4](https://github.com/cwtickle/danoniplus/releases/tag/v8.0.4) / [**v8.0.0**](https://github.com/cwtickle/danoniplus/releases/tag/v8.0.0)

### 📔 Documentation

- [譜面の作成概要](HowToMake)
- 譜面ヘッダー仕様 ( [musicTitle](dos-h0001-musicTitle) / [difData](dos-h0002-difData) / [fadeFrame](dos-h0008-fadeFrame) / [stepYR](dos-h0049-stepYR) / [makerView](dos-h0050-makerView) / [difSelectorUse](dos-h0051-difSelectorUse) )
- 譜面エフェクト仕様 ( [back_data / mask_data](dos-e0004-animationData) / [speed_data/change / boost_data](dos-e0001-speedData) )
- [ローカルストレージ仕様](LocalStorage)

## ⭐ v7 ([Changelog](Changelog-v7))

- 矢印・フリーズアローに**CSS モーション**機能を実装
- フリーズアローヒット時の個別色変化のタイミングを変更
- デフォルトで選択状態にする譜面を URL で指定できる機能を実装
- 譜面読込画面に入ったときに譜面データ・リザルトモーションを再読込できるように変更
- タイトル/リザルトモーションのジャンプ(loop/jump)で  
  **確率でジャンプ先を変えられる**ように変更
- 歌詞表示・背景/マスクモーションにおいて**同一フレームの複数同時描画**に対応
- 背景/マスクモーションにおいて、深度に「ALL」を指定することで、  
  全てのオブジェクトを**一斉クリア**できるように対応
- 楽曲の終了判定タイミングを秒単位から**フレーム単位**に変更
- クリア失敗時のリザルトモーションを実装
- 背景・マスクモーション、速度変化、色変化、歌詞表示で**コメント文**を実装

### 💡 Feature Updates

- [**v7.9.0**](https://github.com/cwtickle/danoniplus/releases/tag/v7.9.0) / [v7.8.1](https://github.com/cwtickle/danoniplus/releases/tag/v7.8.1) / [**v7.8.0**](https://github.com/cwtickle/danoniplus/releases/tag/v7.8.0) / [**v7.7.0**](https://github.com/cwtickle/danoniplus/releases/tag/v7.7.0) / [v7.6.0](https://github.com/cwtickle/danoniplus/releases/tag/v7.6.0) / [**v7.5.0**](https://github.com/cwtickle/danoniplus/releases/tag/v7.5.0) / [v7.4.0](https://github.com/cwtickle/danoniplus/releases/tag/v7.4.0) / [**v7.3.0**](https://github.com/cwtickle/danoniplus/releases/tag/v7.3.0) / [v7.1.0](https://github.com/cwtickle/danoniplus/releases/tag/v7.1.0) / [**v7.0.0**](https://github.com/cwtickle/danoniplus/releases/tag/v7.0.0)

### 📔 Documentation

- 譜面ヘッダー仕様 ( [colorDataType](dos-h0046-colorDataType) / [colorCdPaddingUse](dos-h0047-colorCdPaddingUse) / [resultMotionSet](dos-h0048-resultMotionSet) )
- 譜面エフェクト仕様 ( [矢印・フリーズアローモーション](dos-e0005-motionData) / [back_data / mask_data](dos-e0004-animationData) )

## ⭐ v6 ([Changelog](Changelog-v6))

- **ダミー**矢印・フリーズアロー機能の実装
- 配点と最大スコアを変数化し、danoni_custom/setting.js から変更できるように対応
- リバース用背景・マスクモーション(backRev_data, maskRev_data)の実装
- **リザルトモーション**(backresult_data, maskresult_data)の実装
- タイトルに対してマスクモーション(masktitle_data)を実装
- ライフ上限値の設定を追加
- **ライフ制ゲージ** (Original, Light, NoRecovery)の設定を譜面ヘッダーから設定できるように変更
- 特殊キー(keyExtraList 指定キー)に対してリバース、キーコンフィグ保存に対応

### 💡 Feature Updates

- [v6.6.0](https://github.com/cwtickle/danoniplus/releases/tag/v6.6.0) / [**v6.5.0**](https://github.com/cwtickle/danoniplus/releases/tag/v6.5.0) / [v6.4.0](https://github.com/cwtickle/danoniplus/releases/tag/v6.4.0) / [**v6.3.0**](https://github.com/cwtickle/danoniplus/releases/tag/v6.3.0) / [v6.2.0](https://github.com/cwtickle/danoniplus/releases/tag/v6.2.0) / [v6.1.0](https://github.com/cwtickle/danoniplus/releases/tag/v6.1.0) / [**v6.0.0**](https://github.com/cwtickle/danoniplus/releases/tag/v6.0.0)

### 📔 Documentation

- 譜面ヘッダー仕様 ( [dummyId](dos-h0042-dummyId) / [maxLifeVal](dos-h0045-maxLifeVal) / [gaugeOriginal / gaugeLight / gaugeNoRecovery](dos-h0022-gaugeX) )
- 譜面エフェクト仕様 ( [back_data / mask_data](dos-e0004-animationData) )
- [ローカルストレージ仕様](LocalStorage)

## ⭐ v5 ([Changelog](Changelog-v5))

- タイトル（デフォルト）の**行別グラデーション**に対応
- 縦方向のスケーリングに対応
- 最低・最高速度、譜面変更ボタン(setDifficulty)に対してカスタム関数を追加
- 歌詞表示(word_data)の**複数階層化**に対応
- 歌詞フェードイン・アウトの時間の可変対応
- フリーズアロー**開始判定**の実装
- 譜面データの URI デコードに対応
- 譜面データの&区切り可否の切替機能を追加
- フリーズアローの許容フレームを譜面ヘッダーから指定できるように変更
- キー毎にタイトルバック、リトライ用ショートカットキーの指定を行えるように変更
- 通常矢印の**内側を塗りつぶす**機能を追加

### 💡 Feature Updates

- [**v5.12.0**](https://github.com/cwtickle/danoniplus/releases/tag/v5.12.0) / [v5.11.0](https://github.com/cwtickle/danoniplus/releases/tag/v5.11.0) / [**v5.10.0**](https://github.com/cwtickle/danoniplus/releases/tag/v5.10.0) / [v5.9.0](https://github.com/cwtickle/danoniplus/releases/tag/v5.9.0) / [**v5.8.0**](https://github.com/cwtickle/danoniplus/releases/tag/v5.8.0) / [v5.7.0](https://github.com/cwtickle/danoniplus/releases/tag/v5.7.0) / [**v5.6.6**](https://github.com/cwtickle/danoniplus/releases/tag/v5.6.6) / [v5.6.0](https://github.com/cwtickle/danoniplus/releases/tag/v5.6.0) / [**v5.4.0**](https://github.com/cwtickle/danoniplus/releases/tag/v5.4.0) / [v5.2.0](https://github.com/cwtickle/danoniplus/releases/tag/v5.2.0) / [**v5.1.0**](https://github.com/cwtickle/danoniplus/releases/tag/v5.1.0) / [v5.0.0](https://github.com/cwtickle/danoniplus/releases/tag/v5.0.0)

### 📔 Documentation

- [キーの仕様について](keys)
- [共通設定ファイル仕様](dos_setting)
- 譜面ヘッダー仕様 ( [titlegrd](dos-h0032-titlegrd) / [minSpeed](dos-h0015-minSpeed) / [maxSpeed](dos-h0016-maxSpeed) / [frzStartjdgUse](dos-h0037-frzStartjdgUse) / [frzAttempt](dos-h0038-frzAttempt) / [keyRetry](dos-h0039-keyRetry) / [keyTitleBack](dos-h0040-keyTitleBack) )
- 譜面エフェクト仕様 ( [word_data](dos-e0003-wordData) )

## ⭐ v4 ([Changelog](Changelog-v4))

- 譜面別の**複数曲読込**に対応 ![v4.0.0](https://img.shields.io/badge/-v4.0.0-blue)
- **ローカルストレージ**(作品別、キー別)を実装 ![v4.6.0](https://img.shields.io/badge/-v4.6.0-blue)
- 画面全体に**マスク**を設定するためのデータ(mask_data)を実装 ![v4.2.0](https://img.shields.io/badge/-v4.2.0-blue)
- タイトル画面に対して背景表示をつける機能 (backtitle_data) を追加 ![v4.7.0](https://img.shields.io/badge/-v4.7.0-blue)
- 11key 譜面を**11Lkey として**プレイできるモードを追加 ![v4.8.0](https://img.shields.io/badge/-v4.8.0-blue)
- 曲中リトライ以外のタイミングで曲の再読み込みを行う仕様に変更 ![v4.10.0](https://img.shields.io/badge/-v4.10.0-blue)

### 💡 Feature Updates

- [**v4.10.0**](https://github.com/cwtickle/danoniplus/releases/tag/v4.10.0) / [**v4.8.0**](https://github.com/cwtickle/danoniplus/releases/tag/v4.8.0) / [v4.7.0](https://github.com/cwtickle/danoniplus/releases/tag/v4.7.0) / [**v4.6.0**](https://github.com/cwtickle/danoniplus/releases/tag/v4.6.0) / [**v4.4.0**](https://github.com/cwtickle/danoniplus/releases/tag/v4.4.0) / [v4.3.0](https://github.com/cwtickle/danoniplus/releases/tag/v4.3.0) / [**v4.2.0**](https://github.com/cwtickle/danoniplus/releases/tag/v4.2.0) / [v4.1.0](https://github.com/cwtickle/danoniplus/releases/tag/v4.1.0) / [**v4.0.0**](https://github.com/cwtickle/danoniplus/releases/tag/v4.0.0)

### 📔 Documentation

- 譜面ヘッダー仕様 ( [musicUrl](dos-h0011-musicUrl) / [musicNo](dos-h0012-musicNo) / [transKeyUse](dos-h0024-transKeyUse) )
- 譜面エフェクト仕様 ( [mask_data / backtitle_data](dos-e0004-animationData) )
- [ローカルストレージ仕様](LocalStorage)

## ⭐ v3 ([Changelog](Changelog-v3))

- 製作者別設定ファイルを追加 ![v3.0.0](https://img.shields.io/badge/-v3.0.0-blue)
- **Shuffle**機能（Mirror, Random など）を追加 ![v3.1.0](https://img.shields.io/badge/-v3.1.0-blue)
- 曲名タイトル（デフォルト）の複数行、モーション、グラデーション対応 ![v3.5.0](https://img.shields.io/badge/-v3.5.0-blue)
- 速度変化、色変化、歌詞表示の**セット毎改行区切り**に対応 ![v3.10.0](https://img.shields.io/badge/-v3.10.0-blue)
- 譜面ヘッダー「startFrame」について、譜面毎に設定できるように変更 ![v3.11.0](https://img.shields.io/badge/-v3.11.0-blue)
- キーコンフィグ画面で作品毎の矢印色ではない、  
  共通デフォルト色を指定できるように変更 (ColorType の追加) ![v3.12.0](https://img.shields.io/badge/-v3.12.0-blue)

### 💡 Feature Updates

- [v3.12.0](https://github.com/cwtickle/danoniplus/releases/tag/v3.12.0) / [v3.11.0](https://github.com/cwtickle/danoniplus/releases/tag/v3.11.0) / [**v3.10.0**](https://github.com/cwtickle/danoniplus/releases/tag/v3.10.0) / [v3.9.0](https://github.com/cwtickle/danoniplus/releases/tag/v3.9.0) / [v3.7.0](https://github.com/cwtickle/danoniplus/releases/tag/v3.7.0) / [v3.6.0](https://github.com/cwtickle/danoniplus/releases/tag/v3.6.0) / [**v3.5.0**](https://github.com/cwtickle/danoniplus/releases/tag/v3.5.0) / [**v3.2.0**](https://github.com/cwtickle/danoniplus/releases/tag/v3.2.0) / [**v3.1.0**](https://github.com/cwtickle/danoniplus/releases/tag/v3.1.0) / [**v3.0.0**](https://github.com/cwtickle/danoniplus/releases/tag/v3.0.0)

### 📔 Documentation

- [ソースの構成](AboutSource)
- [共通設定ファイル仕様](dos_setting)
- 譜面ヘッダー仕様 ( [motionUse / shuffleUse / autoPlayUse / gaugeUse](dos-h0035-settingUse) /  
  [musicTitle](dos-h0001-musicTitle) / [titlesize](dos-h0030-titlesize) / [titlelineheight](dos-h0034-titlelineheight) /
  [startFrame](dos-h0005-startFrame) )
- 譜面エフェクト仕様 ( [acolor/color_data](dos-e0002-colorData) / [word_data](dos-e0003-wordData) )

## ⭐ v2 ([Changelog](Changelog-v2))

- 譜面データの**外部ファイル化**に対応 ![v2.3.0](https://img.shields.io/badge/-v2.3.0-blue)
- ロード画面の実装 ![v2.6.0](https://img.shields.io/badge/-v2.6.0-blue)
- オンライン時に WebAudioAPI で楽曲を再生するように変更 ![v2.7.0](https://img.shields.io/badge/-v2.7.0-blue)
- 譜面ヘッダーで**曲の再生速度を指定**できる機能を追加 ![v2.8.0](https://img.shields.io/badge/-v2.8.0-blue)
- 譜面ヘッダーにて、譜面別に（製作者側が）**タイミング調整**できるように変更 ![v2.2.0](https://img.shields.io/badge/-v2.2.0-blue)
- タイトル文字のフォント設定に対応 ![v2.1.0](https://img.shields.io/badge/-v2.1.0-blue)

### 💡 Feature Updates

- [v2.9.0](https://github.com/cwtickle/danoniplus/releases/tag/v2.9.0) / [**v2.8.0**](https://github.com/cwtickle/danoniplus/releases/tag/v2.8.0) / [v2.7.0](https://github.com/cwtickle/danoniplus/releases/tag/v2.7.0) / [v2.6.0](https://github.com/cwtickle/danoniplus/releases/tag/v2.6.0) / [v2.5.0](https://github.com/cwtickle/danoniplus/releases/tag/v2.5.0) / [**v2.3.0**](https://github.com/cwtickle/danoniplus/releases/tag/v2.3.0) / [**v2.2.0**](https://github.com/cwtickle/danoniplus/releases/tag/v2.2.0) / [**v2.1.0**](https://github.com/cwtickle/danoniplus/releases/tag/v2.1.0) / [v2.0.0](https://github.com/cwtickle/danoniplus/releases/tag/v2.0.0)

### 📔 Documentation

- [譜面の作成概要](HowToMake)
- 譜面ヘッダー仕様 ( [adjustment](dos-h0009-adjustment) / [playbackRate](dos-h0010-playbackRate) )

## ⭐ v1 ([Changelog](Changelog-v1))

- **Gauge**オプションの実装 ![v1.0.0](https://img.shields.io/badge/-v1.0.0-blue)
- **ノルマ制ゲージ** (Normal, Easy, Hard)の設定を譜面ヘッダーから設定できるように変更 ![v1.5.0](https://img.shields.io/badge/-v1.5.0-blue)
- 音楽ファイルについて**base64 エンコード**に対応 ![v1.7.0](https://img.shields.io/badge/-v1.7.0-blue)
- 歌詞表示で、左揃え/中央揃え/右揃えの設定を追加 ![v1.9.0](https://img.shields.io/badge/-v1.9.0-blue)
- 歌詞表示の二重階層化に対応 ![v1.9.0](https://img.shields.io/badge/-v1.9.0-blue)
- 曲終了フレーム(endFrame)の譜面別指定に対応 ![v1.10.0](https://img.shields.io/badge/-v1.10.0-blue)
- 判定処理のカスタム処理を分離 ![v1.11.0](https://img.shields.io/badge/-v1.11.0-blue)
- 外部 js ファイルのキャッシュを利用しないように変更 ![v1.12.0](https://img.shields.io/badge/-v1.12.0-blue)
- タイトル文字以外のフォントを設定できるように変更 ![v1.12.0](https://img.shields.io/badge/-v1.12.0-blue)
- ステップゾーン間隔を譜面側から設定できるように変更 (blankX) ![v1.14.0](https://img.shields.io/badge/-v1.14.0-blue)

### 💡 Feature Updates

- [v1.15.0](https://github.com/cwtickle/danoniplus/releases/tag/v1.15.0) / [v1.14.0](https://github.com/cwtickle/danoniplus/releases/tag/v1.14.0) / [v1.13.0](https://github.com/cwtickle/danoniplus/releases/tag/v1.13.0) / [v1.12.0](https://github.com/cwtickle/danoniplus/releases/tag/v1.12.0) / [**v1.11.0**](https://github.com/cwtickle/danoniplus/releases/tag/v1.11.0) / [v1.10.0](https://github.com/cwtickle/danoniplus/releases/tag/v1.10.0) / [**v1.9.0**](https://github.com/cwtickle/danoniplus/releases/tag/v1.9.0) / [**v1.7.0**](https://github.com/cwtickle/danoniplus/releases/tag/v1.7.0) / [**v1.5.0**](https://github.com/cwtickle/danoniplus/releases/tag/v1.5.0) / [v1.3.0](https://github.com/cwtickle/danoniplus/releases/tag/v1.3.0) / [v1.2.0](https://github.com/cwtickle/danoniplus/releases/tag/v1.2.0) / [v1.0.0](https://github.com/cwtickle/danoniplus/releases/tag/v1.0.1)

### 📔 Documentation

- [ゲーム画面の説明](AboutGameSystem)
- [譜面の作成概要](HowToMake)
- [キーの仕様について](keys)
- 譜面ヘッダー仕様 ( [endFrame](dos-h0007-endFrame) / [musicUrl](dos-h0011-musicUrl) / [customFont](dos-h0020-customFont) / [gaugeX](dos-h0022-gaugeX) / [finishView](dos-h0023-finishView) )
- 譜面エフェクト仕様 ( [word_data](dos-e0003-wordData) )

| [< アップグレードガイド](./MigrationGuide) | **更新情報概要** | [更新を終了したバージョンの不具合情報 >](./DeprecatedVersionBugs) |
