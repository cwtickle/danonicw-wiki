**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-h0068-commentExternal) | Japanese** 

[Home](./) &gt; [譜面ヘッダー仕様](dos_header) &gt; [タイトル・結果画面の初期設定](dos_header#%EF%B8%8F-タイトル結果画面の初期設定)

| [<- commentAutoBr](dos-h0067-commentAutoBr) | **commentExternal** | [masktitleButton ->](dos-h0043-masktitleButton) |

## commentExternal
- コメントを本体の外部に置く設定

### 使い方
```
|commentExternal=true|
```
### 説明
[commentVal](dos-h0066-commentVal)をタイトル内ではなく、画面の外に表示するかを設定します。
画面外に表示する場合、以下いずれかをhtml内に記述する必要があります。
```html
<span id="commentArea"></span>
<div id="commentArea"></div>
<p id="commentArea"></p>
```

|値|既定|内容|
|----|----|----|
|false|*|作品コメント(commentVal)をタイトル画面内に配置する|
|true||作品コメント(commentVal)を画面外に表示する|

### 関連項目
- [commentVal](dos-h0066-commentVal) [:pencil:](dos-h0066-commentVal/_edit) コメント表示
- [commentAutoBr](dos-h0067-commentAutoBr) [:pencil:](dos-h0067-commentAutoBr/_edit) コメント表示時に改行タグを自動挿入する設定

### 更新履歴

|Version|変更内容|
|----|----|
|[v15.2.0](https://github.com/cwtickle/danoniplus/releases/tag/v15.2.0)|・初回実装|

| [<- commentAutoBr](dos-h0067-commentAutoBr) | **commentExternal** | [masktitleButton ->](dos-h0043-masktitleButton) |