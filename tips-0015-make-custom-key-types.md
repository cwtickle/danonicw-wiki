**[English](https://github.com/cwtickle/danoniplus-docs/wiki/tips-0015-make-custom-key-types) | Japanese**

[^ Tips Indexに戻る](./tips-index)

| < [カスタムキーテンプレート](./tips-0004-extrakeys) | **カスタムキーの作成方法** | [既存キーのキーパターン上書き](./tips-0006-keypattern-update) > |

# カスタムキーの作成方法

## 作成方法
### 今回作成するカスタムキー
- `11x`という名前のカスタムキーを作ることにします。
```
|difData=11x,Normal,3.5|
```
### 1. 使用するキー対象を列挙（keyCtrlX）
- 最初に、割り当てるキーの数をざっくり決めます。
- この時点で割り当てるキーを決める必要はありません。下記のように数だけ割り当てます。
```
|keyCtrl11x=0,0,0,0,0,0,0,0,0,0,0|
```

<img src="https://github.com/cwtickle/danoniplus/assets/44026291/3e713b89-5639-40e2-b9e8-2544e3d08203" width="60%">

### 2. 上下スクロールが必要な場合、折り返し位置を決定（divX）
- 最初の4つを上段、残りを下段にしたい場合は下記のように設定します。
```
|keyCtrl11x=0,0,0,0,0,0,0,0,0,0,0|
|div11x=4|
```
<img src="https://github.com/cwtickle/danoniplus/assets/44026291/3dd94567-ad65-4871-b9d3-30681033a53d" width="60%">

### 3. 配置する位置を調整（posX, divX）
- 上段の位置を少し変えたいので、下記のように数字を動かして調整します。
- posXを変えると折り返し位置が変わるので、divXも合わせて位置を変えます。
```
|keyCtrl11x=0,0,0,0,0,0,0,0,0,0,0|
|pos11x=0,1,8,9,10,11,12,13,14,15,16|
|div11x=10|
```
- ver33.4.0以降は、posXについては下記のように下段相対指定(`b`+下段左端からの番号)も可能です。
```
|pos11x=0,1,8,9,b0,b1,b2,b3,b4,b5,b6|
```

<img src="https://github.com/cwtickle/danoniplus/assets/44026291/4ad9a09c-8054-4e83-93fb-f3f5257ba508" width="60%">

### 4. 矢印の種類を変更（stepRtnX）
- このままだと左矢印だけなので、矢印の種類を変えて調整します。
```
|keyCtrl11x=0,0,0,0,0,0,0,0,0,0,0|
|pos11x=0,1,8,9,10,11,12,13,14,15,16|
|div11x=10|
|stepRtn11x=45,0,180,135,0,-45,-90,onigiri,90,135,180|
```
<img src="https://github.com/cwtickle/danoniplus/assets/44026291/a9c6369d-da76-476d-aea7-0bce1f9a74a7" width="60%">

### 5. 配色の変更（colorX）
- colorXにより配色を変えます。
```
|keyCtrl11x=0,0,0,0,0,0,0,0,0,0,0|
|pos11x=0,1,8,9,10,11,12,13,14,15,16|
|div11x=10|
|stepRtn11x=45,0,180,135,0,-45,-90,onigiri,90,135,180|
|color11x=3,4,4,3,0,1,0,2,0,1,0|
```
<img src="https://github.com/cwtickle/danoniplus/assets/44026291/d4991c9d-2dfd-4977-b2df-fd27f22f129d" width="60%">

### 6. 割り当てるキーを指定（keyCtrlX）
- 実際に割り当てるキーを指定します。
```
|keyCtrl11x=D1,W,P,Minus,S,D,F,Space,J,K,L|
|pos11x=0,1,8,9,10,11,12,13,14,15,16|
|div11x=10|
|stepRtn11x=45,0,180,135,0,-45,-90,onigiri,90,135,180|
|color11x=3,4,4,3,0,1,0,2,0,1,0|
```
<img src="https://github.com/cwtickle/danoniplus/assets/44026291/43bd5aef-4aa2-48ba-8ac2-70eb15bbc4ee" width="60%">

### 7. シャッフルの割り当てグループを設定（shuffleX）
- シャッフル設定を行う場合のグルーピング設定を行います。
```
|keyCtrl11x=D1,W,P,Minus,S,D,F,Space,J,K,L|
|pos11x=0,1,8,9,10,11,12,13,14,15,16|
|div11x=10|
|stepRtn11x=45,0,180,135,0,-45,-90,onigiri,90,135,180|
|color11x=3,4,4,3,0,1,0,2,0,1,0|
|shuffle11x=0,0,0,0,1,1,1,2,1,1,1|
```
<img src="https://github.com/cwtickle/danoniplus/assets/44026291/ced41710-6f49-4190-a9f7-b204021f322f" width="60%">

### 8. 譜面の変数名を設定（charaX）
- エディターで設定するための変数名を設定します。  
特に名前に制限はないのですが、似た名前をつけておくと他のキーを作るときに共用しやすくなります。
```
|keyCtrl11x=D1,W,P,Minus,S,D,F,Space,J,K,L|
|pos11x=0,1,8,9,10,11,12,13,14,15,16|
|div11x=10|
|stepRtn11x=45,0,180,135,0,-45,-90,onigiri,90,135,180|
|color11x=3,4,4,3,0,1,0,2,0,1,0|
|shuffle11x=0,0,0,0,1,1,1,2,1,1,1|
|chara11x=sleftdia,sleft,sright,srightdia,left,leftdia,down,space,up,rightidia,right|
```
- 上記の例の場合、譜面データとしては下記となります。  
`=`と`|`の間に数字を入れると、矢印が流れてくることが確認できます。
```
|sleftdia_data=|sleft_data=|sright_data=|srightdia_data=|left_data=|leftdia_data=|down_data=|space_data=|up_data=|rightdia_data=|right_data=|
```

### 9. 追加の設定など
- 基本的にここまでの設定が終われば、一通りの設定は完了です。  
残りの細かい設定については関連項目をご覧ください。

## 関連項目
- [キー数仕様](./keys)
- [KeyCtrl属性で使用するキーコード](./KeyCtrlCodeList)
- [共通設定ファイル仕様](./dos_setting) &gt; [カスタムキー定義](./dos-s0010-customKeys)

[^ Tips Indexに戻る](./tips-index)

| < [カスタムキーテンプレート](./tips-0004-extrakeys) | **カスタムキーの作成方法** | [既存キーのキーパターン上書き](./tips-0006-keypattern-update) > |
