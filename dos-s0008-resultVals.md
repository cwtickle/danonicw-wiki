**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-s0008-resultVals) | Japanese** 

[↑ 共通設定ファイル仕様に戻る](dos_setting)

[ <- プレイ画面制御 ](dos-s0007-viewControl) | **リザルトデータ** | [ラベルテキスト・メッセージ -> ](dos-s0009-labelUpdate)

## リザルトデータ
### リザルトデータのフォーマット設定 (g_presetObj.resultFormat)
⇒ 指定があった場合に優先される譜面ヘッダー：[resultFormat](dos-h0072-resultFormat)
- リザルトデータ（Twitter貼り付け用データ）のフォーマット変更を行います。  
使い方は [resultFormat](dos-h0072-resultFormat) をご覧ください。  
カスタム変数については、`g_presetObj.resultVals`にて定義可能です。

```javascript
g_presetObj.resultFormat = `【#danoni[hashTag]】[musicTitle]([keyLabel]) /[maker] /Rank:[rank]/Score:[score]/Playstyle:[playStyle]/[arrowJdg]/[frzJdg]/[maxCombo] [url]`;
```

### リザルトデータ用のカスタム変数群 (g_presetObj.resultVals)
⇒ 指定があった場合に優先される譜面ヘッダー：なし
- プロパティ名(左側)を定義すると、リザルトフォーマット上で  
`[プロパティ名]`というフォーマットを使用できます。  
右側に定義する値は、g_resultObj配下にカスタムjsを利用して定義する必要があります。

- 例えば、下記の場合はリザルトフォーマットに`[exScore]`と指定すると  
実際には`g_resultObj.exScores`に置き換えられます。
```javascript
g_presetObj.resultVals = {
	exScore: `exScores`,
};
```

- customJs上の定義
```javascript
g_resultObj.exScores = 300;  // [exScore]は`300`に置き換えられる
```

### リザルトデータ用のカスタム変数の画像表示設定 (g_presetObj.resultValsView)
⇒ 指定があった場合に優先される譜面ヘッダー：[resultValsView](dos-h0095-resultValsView)
- リザルト画像に表示するカスタム変数を `g_presetObj.resultVals` の項目から配列形式で指定します。 

```javascript
g_presetObj.resultValsView = [`exScore`, `extra`];
```


## 更新履歴

|Version|変更内容|
|----|----|
|[v33.3.0](https://github.com/cwtickle/danoniplus/releases/tag/v33.3.0)|・リザルトデータ用のカスタム変数の画像表示設定 (g_presetObj.resultValsView)を追加|
|[v26.3.1](https://github.com/cwtickle/danoniplus/releases/tag/v26.3.1)|・変数名変更<br>　・g_presetResultFormat -> g_presetObj.resultFormat<br>　・g_presetResultVals -> g_presetObj.resultVals|
|[v16.2.1](https://github.com/cwtickle/danoniplus/releases/tag/v16.2.1)|・リザルトデータのフォーマット設定 (g_presetResultFormat, g_presetResultVals)を実装|

[ <- プレイ画面制御 ](dos-s0007-viewControl) | **リザルトデータ** | [ラベルテキスト・メッセージ -> ](dos-s0009-labelUpdate)