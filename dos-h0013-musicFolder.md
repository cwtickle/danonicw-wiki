**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-h0013-musicFolder) | Japanese** 

[Home](./) &gt; [譜面ヘッダー仕様](dos_header) &gt; [楽曲・譜面情報](dos_header#-楽曲譜面情報)

| [<- musicNo](dos-h0012-musicNo) | **musicFolder** | [dummyId ->](dos-h0042-dummyId) |

## musicFolder
- 楽曲ファイルの格納先の設定

### 使い方
```
|musicFolder=music123|
|musicFolder=(..)music|
```
### 説明
楽曲を入れるフォルダ名を指定します。デフォルトは「music」フォルダ。 

### 関連項目
- [**musicUrl**](dos-h0011-musicUrl) [:pencil:](dos-h0011-musicUrl/_edit) 楽曲ファイル名

### 更新履歴

|Version|変更内容|
|----|----|
|[v19.3.0](https://github.com/cwtickle/danoniplus/releases/tag/v19.3.0)|・カレントディレクトリ指定に対応|
|[v1.0.0](https://github.com/cwtickle/danoniplus/releases/tag/v1.0.1)|・初回実装|

| [<- musicNo](dos-h0012-musicNo) | **musicFolder** | [dummyId ->](dos-h0042-dummyId) |