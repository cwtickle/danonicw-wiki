**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-h0017-tuning) | Japanese** 

[Home](./) &gt; [譜面ヘッダー仕様](dos_header) &gt; [クレジット関連、共通設定](dos_header#-クレジット関連共通設定)

| [<- windowAlign](dos-h0091-windowAlign) | **tuning** | [makerView ->](dos-h0050-makerView) |

## tuning
- 製作者クレジットの設定
- 共通設定 ⇒ [g_presetObj.tuning](dos-s0001-makerInfo#譜面製作者名-g_presetobjtuning), [g_presetObj.tuningUrl](dos-s0001-makerInfo#譜面製作者url-g_presetobjtuningurl)

### 使い方
```
|tuning=ティックル,https://cw7.sakura.ne.jp/|
|tuningEn=tickle,https://cw7.sakura.ne.jp/|
```
### 説明
製作者名及び製作者のホームページを指定します。  
ParaFla版では読込完了用の一変数でしたが、CW Editionではヘッダー扱いです。  

#### 言語別設定 (ver29.2.0以降)

|通常設定|言語別設定(Ja)|言語別設定(En)|
|----|----|----|
|tuning|tuning**Ja**|tuning**En**|

### 補足
「danoni_setting.js」に以下の記述を行うことでデフォルト値を指定できます。  
```javascript
// 譜面製作者名
g_presetObj.tuning = `name`;

// 譜面製作者URL
g_presetObj.tuningUrl = `https://www.google.co.jp/`;
```

ver2以前の場合は、「danoni_custom.js」（ユーザカスタムファイル）の
customTitleInit() に以下を追加することで、
製作者ホームページのデフォルト値を指定することができます。  
```javascript
if (g_headerObj.creatorUrl === location.href) {
	g_headerObj.creatorUrl = "http://(製作者ホームページのデフォルト値)";
}
```
この記述を入れた場合、製作者ホームページの省略が可能となります。
```
|tuning=ティックル|
```

### 関連項目
- [**makerView**](dos-h0050-makerView) [:pencil:](dos-h0050-makerView/_edit) 譜面別の制作者名表示設定 
- [releaseDate](dos-h0036-releaseDate) [:pencil:](dos-h0036-releaseDate/_edit) 作品公開日
- [共通設定ファイル仕様](dos_setting) &gt; [制作者クレジット・共通設定](dos-s0001-makerInfo)

### 更新履歴

|Version|変更内容|
|----|----|
|[v29.2.0](https://github.com/cwtickle/danoniplus/releases/tag/v29.2.0)|・言語別の設定に対応|
|[v1.0.0<br>(v0.40.0)](https://github.com/cwtickle/danoniplus/releases/tag/v1.0.1)|・初回実装|

| [<- windowAlign](dos-h0091-windowAlign) | **tuning** | [makerView ->](dos-h0050-makerView) |