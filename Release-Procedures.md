| [< ライフサイクルポリシー](./LifecyclePolicy) | **リリースまでの流れ** || [Ruffleの使い方 >](./Ruffle) |

# リリースまでの流れ / Release Procedures
- Dancing☆Onigiri (CW Edition)では、作品別に独自のカスタム(利用者定義)が入ることがあるため、  
「直近の変化点」「最近追加・変更された機能」「更新時の留意事項」といった情報が必要。  
単にリリースするだけではなく、変更経緯が辿れるように各種リンクも合わせて更新している。

## 通常更新時の対応事項
### 0. 準備するもの
- テキストエディター ( [Visual Source Code](https://azure.microsoft.com/ja-jp/products/visual-studio-code) など )
- gitクライアント ( [fork](https://git-fork.com/), [GitKraken](https://www.gitkraken.com/) )
- Web上のテキスト置換ツール ( Chrome拡張: [Find &amp; Replace for Text Editing](https://chromewebstore.google.com/detail/find-replace-for-text-edi/mogefjlnnldblkakpbakdaffphhjfjba) )

### 1. 変更ファイルに対してバージョン名と日付を更新し、developブランチにcommit
#### 1-1. 現行バージョンの場合
- /js/danoni_main.js
  - `Revised`, `g_version`, `g_revisedDate`の3か所を更新する。

<img src="https://user-images.githubusercontent.com/44026291/222408290-186b7819-a280-4244-9e3e-06572e56c987.png" width="60%">

- /js/lib/danoni_constants.js (変更した場合)
  - このファイルを変更した場合、`Revised` の日付及びバージョンを更新する。

<img src="https://user-images.githubusercontent.com/44026291/222408443-a4666fc8-3b04-4817-807a-b3184a871905.png" width="60%">

- /js/template/danoni_setting.js
  - このファイルを変更した場合、`Template Update` の日付を更新する。

<img src="https://user-images.githubusercontent.com/44026291/222408633-eaf346ef-19b7-44e7-8b40-31f9c3c84e0c.png" width="60%">

- SECURITY.md
  - バージョン変更に合わせ、リンク先のバージョン名を更新する。  
メジャーバージョンアップ時は更新終了バージョンの変更・反映も行う。

<img src="https://user-images.githubusercontent.com/44026291/222410769-e5c68c36-f353-48c4-a5de-64bf4429b1e9.png" width="90%">

- package.json
  - バージョン変更に合わせ、"version"のバージョン名を更新する。

<img src="https://user-images.githubusercontent.com/44026291/222411025-87d35414-b62e-4680-983d-5390ab5f97b5.png" width="60%">

- ここでは表記していないが、`danoni_main.css`など他ファイルを更新した場合も  
日付・バージョン名を更新する。

#### 1-2. 過去バージョン（サポートバージョン）の場合
- バージョン別のsupportブランチ（例: support/v29）に対して直接commitする。
- バージョン名が異なるだけで、変更対象は同じ。

### 2. developブランチからmasterブランチへPull Requestを送り、マージする
#### 2-1. 現行バージョンの場合
- Pull Request名は `[バージョン名] 変更内容の概要`の形式にする。
- このバージョンまでにマージされたPull Requestをリストにして記載する。  
ここでリスト化しておくと、リスト化したPull Request側でどのバージョンで反映したかが後で追えるので便利。
```
## :hammer: 変更内容 / Details of Changes
- #1416 
- #1417 
- #1418 
```
<img src="https://user-images.githubusercontent.com/44026291/222412563-74a2b2f4-44bc-46ff-8e5c-c1e656b6d6d7.png" width="60%">

#### 2-2. 過去バージョン（サポートバージョン）の場合
- supportブランチは独立したブランチのため、他のブランチへのPull Requestやマージは**絶対に行わない**こと。  

### 3. masterとdevelopブランチを同期化し、masterへのマージコミットに対してタグを生成
- masterブランチだけマージコミット分が増えているので、developブランチにmasterブランチを逆マージする。
- Gitクライアントで`fork`を使っている場合、一度Fetchした上でマージコミットに対して「Merge into 'develop'」する。
<img src="https://user-images.githubusercontent.com/44026291/222902860-f5116337-69eb-4dda-b98b-0c245f1e680c.png" width="90%">

- その後、masterへのマージコミットに対して「New Tag」でタグを生成し、名前をバージョン名 (例: v30.3.0など) にして「Push」する。
<img src="https://user-images.githubusercontent.com/44026291/222414650-f4c6dc46-0444-48e2-8761-e4e3d0505627.png" width="50%">
<img src="https://user-images.githubusercontent.com/44026291/222903192-fc565efc-38b1-4b27-a0db-23a0d9ace086.png" width="90%">

### 4. リリースノートの作成
#### 4-1. 現行バージョンの場合
- リリースノートを1から作るのは手間のため、前回のリリースノートを編集してコピーする。
<img src="https://user-images.githubusercontent.com/44026291/222430618-8f763030-ae2e-46c8-bf4f-69dd85d31a8c.png" width="60%">

- Chrome拡張: [Find & Replace for Text Editing](https://chromewebstore.google.com/detail/find-replace-for-text-edi/mogefjlnnldblkakpbakdaffphhjfjba) を使ってバージョン名を一斉置換する。  
Changelogのリンクはバージョン名で置換できないため、手動で修正する。
```
Changelog-latest#v3030-2023-03-01  // バージョン名のドットを取って、更新日を付与する
Changelog-v29#v2941-2023-01-28
```
<img src="https://user-images.githubusercontent.com/44026291/222431521-530486a5-6d8b-4ca3-b01d-9305e7b252bc.png" width="80%">

- File changedについて、今回変更したファイルに合わせてリンクを修正する。  
変更しない場合は、最終更新したバージョンのReleaseリンクを張る。
```md
**v30.3.0** <- 最新バージョンで変更するファイル
[v30.1.1](https://github.com/cwtickle/danoniplus/releases/tag/v30.1.1) <- 今回更新しないファイル（最終で更新したバージョンとリンクをつける）
``` 
- `danoni_setting.js`の場合は、前回変更したファイルのバージョン名を控え、compareリンクを作成する。  
下記は、前回変更したバージョンがv27.1.0で、変更先がv30.1.1である例。
```md
|/js/template|[danoni_setting(-template).js](https://github.com/cwtickle/danoniplus/compare/v27.1.0...v30.1.1#files_bucket)|[📥](https://github.com/cwtickle/danoniplus/releases/download/v30.1.1/danoni_setting-template.js)|[v30.1.1](https://github.com/cwtickle/danoniplus/releases/tag/v30.1.1)|
```

- マージされた内容を New Features, Improvements, Bug Fixes に振り分ける。
参照先の Pull Request の番号をリンクさせる。
<img src="https://user-images.githubusercontent.com/44026291/222433483-6bc95de6-72a4-4ecb-b13d-51ac554bbc69.png" width="60%">

- 今回の更新で留意が必要な点を「Remarks」として記載する。  
あらかじめ元となるPull Requestに仕様や変更理由を記入しておくと、そのまま利用しやすい。

<img src="https://user-images.githubusercontent.com/44026291/222434151-8f066244-6c2c-47fe-9f39-9d18e16ba1b2.png" width="60%">

- 直近のバージョンリンクを「Recent Changes」に追加する。
- バグフィックス系の場合は過去バージョンも修正するので、過去バージョンのリリースノート及び影響元のリリースノートへのリンクを付与する。
<img src="https://user-images.githubusercontent.com/44026291/222434728-96966f67-ca0f-4d7f-a48b-7bb7f0821f4a.png" width="50%">

- 変更ファイルを「Download asset」としてファイルアップロードする。  
「danoni_setting.js」が変更対象の場合は、ファイルを上書きしてしまうと  
既存の設定が上書きされ問題になるため、  
「danoni_setting-template.js」に名前を変更してアップロードする。

#### 4-2. 過去バージョン（サポートバージョン）の場合
- 基本的な流れは 4-1.と同じだが、参照するリリースノートは過去バージョンのものを使用すること。
- リンク形式や仕様が他と異なっているため。

<img src="https://user-images.githubusercontent.com/44026291/222676957-9f7c154d-80e0-4717-8b95-3f879b33fe90.png" width="50%"><img src="https://user-images.githubusercontent.com/44026291/222677082-a1796ad6-2dc2-439b-9e0a-953260b9f0c2.png" width="50%">

- バージョン比較対象は、現行バージョンの場合は`master`ブランチだが、過去バージョンの場合は`support`ブランチになる。
v29の場合、`support/v29`が比較対象となる。
```md
[vs. latest v29](https://github.com/cwtickle/danoniplus/compare/v29.4.2...support/v29#files_bucket)
```

#### 4-3. 更新終了バージョンに対して情報追記
- 過去バージョンに影響のある不具合の場合、[更新終了バージョンの不具合情報](./DeprecatedVersionBugs)に関係リンクを追記する。  
その不具合を修正したバージョンと対応するPull Request（無い場合は、前回バージョンとの差分）を記載する。
- **影響するバージョン全て**に対して同じ内容を記載する。英語版もv19以降について対応する。

<img src="https://user-images.githubusercontent.com/44026291/227493831-b7ed23f8-04c4-45f3-8db0-9f596f8653f8.png" width="70%">

```md
<!-- 通常例 -->
- 譜面リストで、上下キーを使って選択した譜面がキー数違いのときにキーコンフィグ画面へ移動すると画面が止まる ⇒ ( [v31.0.1](https://github.com/cwtickle/danoniplus/releases/tag/v31.0.1) / [fix](https://github.com/cwtickle/danoniplus/pull/1446) )

<!-- Pull Requestが存在しない例 -->
- 11, 11Lkeyの別キーモードでFlatにしたときにスクロールが想定と逆に流れる ⇒ ( [v29.4.3](https://github.com/cwtickle/danoniplus/releases/tag/v29.4.3) / [fix](https://github.com/cwtickle/danoniplus/compare/v29.4.2...v29.4.3) )
```

### 5. Changelogの作成
- リリースノートのうち、「File changed」「New Features」「Improvements」「Bug Fixes」と  
コード一覧、前回バージョンとのDiff、ダウンロードリンクを含んだ情報をChangelogに反映する。
<img src="https://user-images.githubusercontent.com/44026291/222436294-c00f581b-d616-408b-8638-21e416bf6dd1.png" width="70%">

- Changelogは英語版もあるため、「[DeepL](https://www.deepl.com/ja/translator)」を使って翻訳する。  
ダンおにや音ゲー特有の用語はそのままではうまく変換できない場合があるため、  
下記のページを参考に修正する。  
https://github.com/cwtickle/danoniplus-docs/issues/1
<img src="https://user-images.githubusercontent.com/44026291/222437487-8d9f1b70-4bd3-45f2-920f-b61d9753bd31.png" width="70%">

### 6. 関連ページの作成・修正（機能更新時）
- 機能更新を伴う場合は、関連ページを作成・修正する。
- 修正後、更新履歴があるページの場合は「対象のバージョン」と更新内容を記載する。

<img src="https://user-images.githubusercontent.com/44026291/226215139-bf313d40-7117-4dd7-acb9-c22a635053bd.png" width="70%">

- バージョン番号には履歴をつけており、日本語の場合はリリースノート、英語の場合はChangelogのリンクをつけている。（現状、リリースノートは日本語のみの対応のため）
```
|[v30.6.0](https://github.com/cwtickle/danoniplus/releases/tag/v30.6.0)|・ライブラリ用カスタムキー定義 (g_presetObj.keysDataLib)を追加|
|[v30.6.0](Changelog-latest#v3060-2023-03-18)|- Added custom key definitions for libraries (g_presetKeysData).|
```

### 7. ID一覧の修正（レイアウト変更時）
- レイアウトの変更やIDの追加/変更があった場合、IDリファレンスを更新する。
<img src="https://user-images.githubusercontent.com/44026291/226305170-ae507cce-e81e-4515-bad1-0a08430f19ef.png" width="70%">

### 8. 更新情報概要の修正（機能更新時）
- リリースノートで「New Features」に記載した内容及び6. で更新したwikiのページへのリンクを張る。
<img src="https://user-images.githubusercontent.com/44026291/222440127-0ce8e40c-72be-4902-8b77-add21be6de67.png" width="70%">

### 9. リリースノートに更新したページのリンクを追加（機能更新時）
- 4.で作成したリリースノートのDocumentationに、6.で追加したページのリンクを張る。
<img src="https://user-images.githubusercontent.com/44026291/226215404-1a32240e-7bec-40cb-8856-e435c7550a5b.png" width="70%">

## メジャーバージョンアップ時の対応事項
### 10. アップグレードガイドの作成
- 過去バージョンからの非互換内容をアップグレードガイドへ記載する。
- 通常、留意事項はあらかじめPull Requestなどで草案を書いておき、文書化する。
- 「変更点」「変更時の対応事項」の順にリスト化して書く。
<img src="https://user-images.githubusercontent.com/44026291/226215739-2aedb964-1977-44b7-808e-23adcc68f7e1.png" width="70%">

- メジャーバージョンアップだけでなく、ひとつ前のバージョンで項目や変数が追加された場合には対象バージョンを記載してその内容を書く。
- リリースノートのRemarksに記載があっても、それを毎回見るとは限らないため。

<img src="https://user-images.githubusercontent.com/44026291/226215973-ce8210b1-1ed4-4cb2-9738-89b71735d5ba.png" width="70%">

### 11. リリースノート(現行バージョン)のリンク先修正
- リリースノート(現行バージョン)にはmasterブランチとの比較用にリンクが張られているが、メジャーバージョンが上がると過去バージョンになるため、比較対象がsupportブランチに変わる。
- Chrome拡張: [Find & Replace for Text Editing](https://chromewebstore.google.com/detail/find-replace-for-text-edi/mogefjlnnldblkakpbakdaffphhjfjba) を使って`master`を`support/v30`(ver30の場合)へ一斉置換する。
- 同じく、Changelogのリンクも変わるため`latest`を`v30`(ver30の場合)に置換する。
- これらを現行バージョンのリリースノートすべてに対して行う。

<img src="https://user-images.githubusercontent.com/44026291/226216379-ec45661c-c2d4-40d9-834b-7300803f94d7.png" width="70%">

### 12. Changelog-latest のデータ移動
- 最新のChangelogが古くなるため、(ver30の場合)`Changelog-v30`を作成し`Changelog-latest`から引っ越しを行う。
- Chrome拡張: [Find & Replace for Text Editing](https://chromewebstore.google.com/detail/find-replace-for-text-edi/mogefjlnnldblkakpbakdaffphhjfjba) を使って`master`を`support/v30`(ver30の場合)へ一斉置換する。
- 同じく、Changelogのリンクも変わるため`latest`を`v30`(ver30の場合)に置換する。

<img src="https://user-images.githubusercontent.com/44026291/226216817-76b7d33d-2ec6-4a56-afcc-b3f24f30ad6d.png" width="100%">

### 13. Changelog-latest関連のリンク変更
- `Changelog-latest`から`Changelog-v30`にリンク変更したため、他のwikiの**全ページ**に対してリンク置換を行う。
- なお、1つ1つに対してリンク変更するのは非常に手間のため、あらかじめwikiを`git clone`し、「Visual Studio Code」の「検索」から複数ファイルをまとめて置換する。

<img src="https://user-images.githubusercontent.com/44026291/226217174-2d7b3e54-be34-43f4-b24b-18ce373fc3c3.png" width="70%">

<img src="https://user-images.githubusercontent.com/44026291/226217219-6a76bf78-4404-4a11-82d7-da144b40c0c9.png" width="70%">

- 置換対象が合っているかどうかを確認して、問題が無ければ`push`する。
<img src="https://user-images.githubusercontent.com/44026291/226217290-9cbb5257-dbc0-4ab6-a10b-76a26db016b7.png" width="70%">

- 英語版も同様に修正する。こちらはChangelogの最新版をトップページとしてリンクしていることが多いため、注意が必要。
<img src="https://user-images.githubusercontent.com/44026291/226217448-cee3483d-b46c-422d-9e07-ce5921d01849.png" width="70%">

### 14. 更新終了バージョンの情報反映（Changelog）
- 更新終了バージョンのChangelogに、更新が終了した旨の記載を追記する。

#### 日本語
```md
## [Warning] 更新終了後の不具合情報
- [更新を終了したバージョンの不具合情報#v28](DeprecatedVersionBugs#v28) を参照
```

#### 英語
```md
## [Warning] Defect information for unsupported versions
- See [Defect information for unsupported versions#v28](DeprecatedVersionBugs#v28).
```

<img src="https://user-images.githubusercontent.com/44026291/226219163-7ab9026a-e273-4974-9d74-373ae2fa8994.png" width="70%">


### 15. 更新終了バージョンの情報反映（リリースノート）
- 更新終了バージョンのリリースノートに、更新が終了した旨の記載を追記する。
- 下記はver28の更新終了を表すアナウンス。
```md
## ⚠️ Warning
- ver28の最終バージョンです。以後、不具合が発生しても修正は行われません。
後継バージョンの利用を推奨します。
⇒ [Security Policy (セキュリティポリシー)](https://github.com/cwtickle/danoniplus/security/policy)
⇒ [サポートを終了したバージョンの不具合情報](https://github.com/cwtickle/danoniplus/wiki/DeprecatedVersionBugs#v28)
```

- 通常、同一バージョンの最終版との比較リンクがついているが、不要のため外す。
```md
( [vs. latest v28](https://github.com/cwtickle/danoniplus/compare/v28.6.7...master#files_bucket) )
( latest v28 )
```

<img src="https://user-images.githubusercontent.com/44026291/226218739-b1bbb1e1-8c9b-4785-8891-5f61a289820c.png" width="70%">

### 16. 更新終了したバージョンの情報反映（更新終了バージョンの不具合情報）
- [更新終了バージョンの不具合情報](./DeprecatedVersionBugs)へフォーマットのみ追記する。

#### 日本語
```md
## v28
- (現状なし)
```

#### 英語
```md
## v28
- (Nothing)
```
<img src="https://user-images.githubusercontent.com/44026291/226219288-9a312f9e-eb43-4fbb-931d-3bd05d44a1bd.png" width="70%">

| [< ライフサイクルポリシー](./LifecyclePolicy) | **リリースまでの流れ** || [Ruffleの使い方 >](./Ruffle) |
