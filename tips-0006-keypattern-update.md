**[English](https://github.com/cwtickle/danoniplus-docs/wiki/tips-0006-keypattern-update) | Japanese** 

[^ Tips Indexに戻る](./tips-index)

| < [カスタムキーの作成方法](./tips-0015-make-custom-key-types) | **既存キーのキーパターン上書き** | [キー変化作品の実装例](./tips-0009-key-types-change) > |

# 既存キーのキーパターン上書き
## 注意
- 既存キーの設定上書きは、設定方法によっては予期しない問題が発生する可能性があります。
- 自作品で一通りの動作を確認してから実施してください。

## 使い方
- いずれの方法でも、danoni_setting.js など共通設定ファイルへの記述を推奨します。
- 特定の作品にのみ適用したい場合は、譜面ファイル内に記述し、その中で設定できないものは独自のカスタムJsへ記述します。

### 対応方法1: キーパターンを追加
- v27.6.0より採用している、|appendX=true|(Xはキー数)を使用して既存のキーパターンに追加します。
- v30.5.0からは追加したキーパターン定義の参照指定(`12_(0)`)ができるようになりました。
```
|minWidth12=675|
|append12=true|
|keyCtrl12=F1,F2,F3,F4,F5,F6,F7,F8,F9,F10,F11,F12$Q,W,E,R,T,Y,U,I,O,P,Ja-@,Ja-[|
|chara12=oni,left,leftdia,down,sleft,sdown,sup,sright,space,up,rightdia,right$12_(0)|
|color12=1,0,1,0,3,3,3,3,0,1,0,1$12_(0)|
|stepRtn12=45,0,-45,-90,giko,onigiri,iyo,c,90,135,180,225$12_(0)|
|blank12=50$12_(0)|
|shuffle12=0,0,0,0,1,1,1,1,2,2,2,2$12_(0)|
|scroll12=Cross::1,1,1,1,-,-,-,-,1,1,1,1/Split::1,1,1,1,1,1,-,-,-,-,-,-$12_(0)|
|transKey12=12i$12i|
```

### 対応方法2: 既存キーパターンの一部を変更
- 上記で|appendX=false|(Xはキー数)とし、全てのキーパターンを記述する方法もありますが、推奨しません。
- 基本は別の名前のキー数(例:12X)を定義し、そのキー数の名前を元のキー数(例:12)にする方法を取ります。  
例の場合、|keyName12X=12|で設定が可能です。
- ただ既存作品が多い場合、キー数を全て変更するのは面倒です。このため、次のように対処します。

#### danoni_setting.js での定義

- g_keyObj.keyTransPattern を使って、キー数の読み替え設定を行います。
```javascript
g_keyObj.keyTransPattern['12'] = `12X`; // difDataのキー数が12keyなら、12Xkeyとして読み替える
```

- カスタムキー定義により、12Xkeyとして12keyを再定義します。  
12key作品は12Xkey作品としてキー設定が読み込まれますが、|keyName12X=12|としているため、表示上は12keyのままです。
```javascript
g_presetObj.keysData = `

|keyName12X=12|
|minWidth12X=675|
|chara12X=12_0$12_1$12_2$12_3$oni,left,leftdia,down,sleft,sdown,sup,sright,space,up,rightdia,right$12X_4|
|color12X=12_0$12_1$12_2$12_3$1,0,1,0,3,3,3,3,0,1,0,1$12X_4|
|pos12X=12_0$12_1$12_2$12_3$0,1,2,3,4,5,6,7,8,9,10,11$12X_4|
|div12X=5$5$5$5$12$12|
|stepRtn12X=12_0$12_1$12_2$12_3$45,0,-45,-90,giko,onigiri,iyo,c,90,135,180,225$12X_4|
|keyCtrl12X=12_0$12_1$12_2$12_3$112/0,113/0,114/0,115/0,116/0,117/0,118/0,119/0,120/0,121/0,122/0,123/0$81/0,87/0,69/0,82/0,84/0,89/0,85/0,73/0,79/0,80/0,192/0,219/0|
|shuffle12X=12_0$12_1$12_2$12_3$0,0,0,0,1,1,1,1,2,2,2,2$12X_4|
|blank12X=55$55$55$55$50$50|
|scroll12X=12_0$12_0$12_0$12_0$Cross::1,1,1,1,-1,-1,-1,-1,1,1,1,1/Split::1,1,1,1,1,1,-1,-1,-1,-1,-1,-1$12X_4|
|transKey12X=$$$$12i$12i|

`;
```

## 動作確認バージョン
- [v27.6.0](https://github.com/cwtickle/danoniplus/releases/tag/v24.6.0)以降  
※キーパターン定義の参照指定(`12_(0)`)を使う場合は [v30.5.0](https://github.com/cwtickle/danoniplus/releases/tag/v30.5.0) 以降

## ページ作成者
- ティックル

## 関連項目
- [キー数仕様](keys)
- [カスタムキーテンプレート](tips-0004-extrakeys)

[^ Tips Indexに戻る](./tips-index)

| < [カスタムキーの作成方法](./tips-0015-make-custom-key-types) | **既存キーのキーパターン上書き** | [キー変化作品の実装例](./tips-0009-key-types-change) > |
