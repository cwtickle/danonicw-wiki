**[English](https://github.com/cwtickle/danoniplus-docs/wiki/AboutSkin) | Japanese** 

| [< 共通設定ファイル仕様](./dos_setting) | **スキンファイル仕様** | [基準ディレクトリ仕様 >](./AboutBaseDirectory) |

# スキンファイル仕様
- 「skin」フォルダ内にある「danoni_skin_XXX.css」(XXXはスキン名)で設定できる項目の一覧です。
- 設定したスキンは[共通設定ファイル仕様](./dos_setting)や譜面ヘッダー:[skinType](./dos-h0054-skinType)にて設定が可能です。

<img src="https://github.com/cwtickle/danoniplus/assets/44026291/b2e01d41-9bca-4766-a7b4-f8bb078318ce" width="50%"><img src="https://github.com/cwtickle/danoniplus/assets/44026291/78b896f9-2108-4652-8dd9-e3c3d7b74391" width="50%">

## 設定可能項目一覧
- ver33からはカスタムプロパティ名を直接指定することで変更が可能です。  
スキンcssファイルで指定する他に、譜面ヘッダーにカスタムプロパティ名を指定することで直接変更も可能です。
- 設定属性が「background」「border-image」の場合は、色指定にグラデーションや画像ファイルが利用できます。  
※カスタムプロパティ名の末尾が「-x」のものはグラデーション記法が使用できません。

### 背景、タイトル画面
```
|--background=45deg:#333399:#000000:#993333|
```

<img src="https://github.com/cwtickle/danoniplus/assets/44026291/27e41b6c-9df8-40dc-a0de-51d7ec513469" width="50%">

|カスタムプロパティ名|スタイル適用先|設定属性|備考|
|----|----|----|----|
|--back-border-x|#canvas-frame|border-color|既定は`#666666`|
|--background|#divBack|background||
|--back-chartDetail|#scoreDetail|background|譜面明細子画面<br>既定は`--background`|
|--back-difListL|#difCover|background|譜面選択子画面(左半分)<br>既定は`--background`|
|--back-difListR|#difList|background|譜面選択子画面(右半分)<br>既定は`--back-difListL`|
|--title-base|.title_base|color||
|--title-dancing|.settings_Title::first-letter|**background**|v32以前はcolor|
|--title-onigiri|.settings_Title2::first-letter|**background**|v32以前はcolor|
|--title-star|.settings_TitleStar::first-letter|**background**|v32以前はcolor|
|--title-display|.settings_Display::first-letter|**background**|v32以前はcolor|
|--title-siz-x|.settings_Title::first-letter<br>.settings_Title2::first-letter<br>.settings_TitleStar::first-letter<br>.settings_Display::first-letter|font-size||

### 設定画面
```
|--settings-lifeVal-x=#ff6666|
|--settings-fadeinBar-x=#9999ff|
|--settings-bc-gaugeTable=#ffff99:#ff9999|
```

<img src="https://github.com/cwtickle/danoniplus/assets/44026291/64cd7cfc-e7f3-42bf-9468-0abb4f801106" width="70%">

|カスタムプロパティ名|スタイル適用先|設定属性|備考|
|----|----|----|----|
|--settings-difficulty-x|.settings_Difficulty::first-letter|color||
|--settings-speed-x|.settings_Speed::first-letter|color||
|--settings-motion-x|.settings_Motion::first-letter|color||
|--settings-scroll-x|.settings_Reverse::first-letter<br>.settings_Scroll::first-letter|color||
|--settings-shuffle-x|.settings_Shuffle::first-letter|color||
|--settings-autoPlay-x|.settings_AutoPlay::first-letter|color||
|--settings-gauge-x|.settings_Gauge::first-letter|color||
|--settings-adjustment-x|.settings_Adjustment::first-letter|color||
|--settings-fadein-x|.settings_Fadein::first-letter|color||
|--settings-volume-x|.settings_Volume::first-letter|color||
|--settings-appearance-x|.settings_Appearance::first-letter|color||
|--settings-opacity-x|.settings_Opacity::first-letter|color||
|--settings-hitPosition-x|.settings_HitPosition::first-letter|color||
|--settings-playWindow-x|.settings_PlayWindow::first-letter|color||
|--settings-stepArea-x|.settings_StepArea::first-letter|color||
|--settings-frzReturn-x|.settings_FrzReturn::first-letter|color||
|--settings-shaking-x|.settings_Shaking::first-letter|color||
|--settings-effect-x|.settings_Effect::first-letter|color||
|--settings-camoufrage-x|.settings_Camoufrage::first-letter|color||
|--settings-swapping-x|.settings_Swapping::first-letter|color||
|--settings-judgRange-x|.settings_JudgRange::first-letter|color||
|--settings-autoRetry-x|.settings_AutoRetry::first-letter|color||
|--settings-difSelector|.settings_DifSelector|background|(現在利用不可)|
|--settings-disabled-x|.settings_Disabled|color||
|--settings-fadeinBar-x|.settings_FadeinBar|color|フェードインの設定文字|
|--settings-lifeVal-x|.settings_lifeVal|color|ゲージ詳細の可変部分|
|--settings-bc-gaugeTable|.settings_gaugeDivCover|border-image|ゲージ詳細の枠色(グラデーション可)|
|--settings-slice-gaugeTable|.settings_gaugeDivCover|border-image|ゲージ詳細の枠の使用範囲(border-image-slice)|
|--settings-border-gaugeTable-x|.settings_gaugeDivCover|border-image|ゲージ詳細の枠色、slice以外の設定|
|--settings-maxArrowCnts-x|.settings_maxArrowCnts|color|譜面明細子画面(DifLevel)のレーン別矢印数の最大値の色<br>(未指定時は`--common-uwan`の色を適用)|
|--settings-minArrowCnts-x|.settings_minArrowCnts|color|譜面明細子画面(DifLevel)のレーン別矢印数の最小値の色<br>(未指定時は`--common-ii`の色を適用)|

### キーコンフィグ画面
```
|--keyconfig-defaultkey-x=#ff99cc|
```

<img src="https://github.com/cwtickle/danoniplus/assets/44026291/8edc546a-3fc2-42c7-8e33-689e0a0e1297" width="50%">

|カスタムプロパティ名|スタイル適用先|設定属性|備考|
|----|----|----|----|
|--keyconfig-warning-x|.keyconfig_warning|color||
|--keyconfig-imgType-x|.keyconfig_ConfigType::first-letter|color||
|--keyconfig-colorType-x|.keyconfig_ColorType::first-letter|color||
|--keyconfig-changekey-x|.keyconfig_Changekey|color||
|--keyconfig-defaultkey-x|.keyconfig_Defaultkey|color||
|--keyconfig-colorGr-x|.keyconfig_ColorGr::first-letter|color|未指定時は`--keyconfig-colorType-x`の色を適用|
|--keyconfig-shuffleGr-x|.keyconfig_ShuffleGr::first-letter|color|未指定時は`--settings-shuffle-x`の色を適用|
|--keyconfig-stepRtnGr-x|.keyconfig_StepRtnGr::first-letter|color|未指定時は`--settings-adjustment-x`の色を適用|

### ステップゾーン
```
|--main-stepKeyDown=#ff9999:#ffff99:#99ffff|
```

<img src="https://github.com/cwtickle/danoniplus/assets/44026291/0549c266-e0e5-4832-a4b0-a36e4fe88bcf" width="50%">

|カスタムプロパティ名|スタイル適用先|設定属性|備考|
|----|----|----|----|
|--main-stepKeyDown|.main_stepKeyDown|background||
|--main-stepDefault|.main_stepDefault|background||
|--main-stepDummy|.main_stepDummy|background||
|--main-stepIi|.main_stepIi|background||
|--main-stepShakin|.main_stepShakin|background||
|--main-stepMatari|.main_stepMatari|background||
|--main-stepShobon|.main_stepShobon|background||
|--main-stepExcessive|.main_stepExcessive|background||
|--main-objStepShadow|.main_objStepShadow|background||
|--main-objFrzShadow|.main_objShadow|background||
|--main-frzHitTop|.main_frzHitTop|background||

### ライフゲージ
```
|--life-failed=#333333:#ff9999|
```

<img src="https://github.com/cwtickle/danoniplus/assets/44026291/aff58f0b-cfb5-4991-9a0c-3008e3dbcb4b" width="50%">

|カスタムプロパティ名|スタイル適用先|設定属性|備考|
|----|----|----|----|
|--life-max|.life_Max|background||
|--life-cleared|.life_Cleared|background||
|--life-failed|.life_Failed|background||
|--life-background|.life_Background|background||
|--life-bg-border|.life_BorderColor|background||
|--life-color-border-x|.life_BorderColor|color||

### 判定・結果画面
```
|--common-ii=pink|
|--result-bc-playwindow=#666666:#ffff99:#6666ff|
|--result-failed=180deg:#ff9999:#666666|
```

<img src="https://github.com/cwtickle/danoniplus/assets/44026291/bb9bb4b3-62ba-4c53-bac9-074428a19d62" width="50%">

|カスタムプロパティ名|スタイル適用先|設定属性|備考|
|----|----|----|----|
|--result-lbl-x|.result_lbl|color||
|--result-style-x|.result_style|color||
|--common-ii|.common_ii|**color**||
|--common-shakin|.common_shakin|**color**||
|--common-matari|.common_matari|**color**||
|--common-shobon|.common_shobon|**color**||
|--common-uwan|.common_uwan|**color**||
|--common-kita|.common_kita|**color**||
|--common-iknai|.common_iknai|**color**||
|--common-combo|.common_combo|**color**||
|--common-score-x|.common_score|color||
|--common-comboJ|.common_comboJ|color|既定は`--common-kita`|
|--common-comboFJ|.common_comboFJ|color|既定は`--common-ii`|
|--common-diffFast|.common_diffFast|color|既定は`--common-matari`|
|--common-diffSlow|.common_diffSlow|color|既定は`--common-shobon`|
|--common-estAdj|.common_estAdj|color|既定は`--common-shakin`|
|--common-excessive|.common_excessive|color|既定は`--common-kita`|
|--result-bc-playwindow|.result_PlayDataWindow|border-image|結果画面のプレイ設定表示枠色(グラデーション可)|
|--result-slice-playwindow-x|.result_PlayDataWindow|border-image|結果画面のプレイ設定表示枠の使用範囲(border-image-slice)|
|--result-border-playwindow-x|.result_PlayDataWindow|border-image|結果画面のプレイ設定表示枠色、slice以外の設定|
|--result-score-x|.result_score|color||
|--result-scoreHiBracket-x|.result_scoreHiBlanket|color|ハイスコア差分(括弧)|
|--result-scoreHi-x|.result_scoreHi|color|ハイスコア差分|
|--result-scoreHiPlus-x|.result_scoreHiPlus|color|ハイスコア差分(更新時)|
|--result-allPerfect|.result_AllPerfect|**background**|v32以前はcolor|
|--result-perfect|.result_Perfect|**background**|v32以前はcolor|
|--result-fullCombo|.result_FullCombo|**background**|v32以前はcolor|
|--result-cleared|.result_Cleared|**background**|v32以前はcolor|
|--result-failed|.result_Failed|**background**|v32以前はcolor|
|--result-siz-window-x|.result_Window::first-letter|color||

### ボタン (Click Here用)
<img src="https://github.com/cwtickle/danoniplus/assets/44026291/b978ca97-d37a-457f-b106-b78a32a1f57f" width="50%">

|カスタムプロパティ名|スタイル適用先|設定属性|備考|
|----|----|----|----|
|--button-color-start-x|.button_Start|color||
|--button-bg-start|.button_Start|background||
|--button-colorHover-start-x|.button_Start:hover|color|オンマウス用|
|--button-bgHover-start|.button_Start:hover|background|オンマウス用|

### ボタン (汎用)
<img src="https://github.com/cwtickle/danoniplus/assets/44026291/3f9c99e5-4c71-4a07-b63e-ba4e9e94fef6" width="70%">

|カスタムプロパティ名|スタイル適用先|設定属性|備考|
|----|----|----|----|
|--button-color-default-x|.button_Default|color||
|--button-bg-default|.button_Default|background||
|--button-colorHover-default-x|.button_Default:hover|color|オンマウス用|
|--button-bgHover-default|.button_Default:hover|background|オンマウス用|

### ボタン (汎用：色指定なし)
<img src="https://github.com/cwtickle/danoniplus/assets/44026291/25b0f2a9-4e9a-4212-8c37-5b317ad56500" width="50%">

|カスタムプロパティ名|スタイル適用先|設定属性|備考|
|----|----|----|----|
|--button-bg-defaultNo|.button_Default_NoColor|background||
|--button-bgHover-defaultNo|.button_Default_NoColor:hover|background|オンマウス用|

### ボタン (設定用ミニボタン)
<img src="https://github.com/cwtickle/danoniplus/assets/44026291/f1e9d206-305e-4351-85ae-3760e8cd850a" width="20%">

|カスタムプロパティ名|スタイル適用先|設定属性|備考|
|----|----|----|----|
|--button-color-mini-x|.button_Mini|color||
|--button-bg-mini|.button_Mini|background||
|--button-colorHover-mini-x|.button_Mini:hover|color|オンマウス用|
|--button-bgHover-mini|.button_Mini:hover|background|オンマウス用|

### ボタン (戻るボタン)
<img src="https://github.com/cwtickle/danoniplus/assets/44026291/fdfb58c2-eb14-41ac-8b61-c88df3ab0012" width="20%"><img src="https://github.com/cwtickle/danoniplus/assets/44026291/be71f51e-1364-4dd7-b16f-64a3a19647aa" width="20%">


|カスタムプロパティ名|スタイル適用先|設定属性|備考|
|----|----|----|----|
|--button-color-back-x|.button_Back|color||
|--button-bg-back|.button_Back|background||
|--button-colorHover-back-x|.button_Back:hover|color|オンマウス用|
|--button-bgHover-back|.button_Back:hover|background|オンマウス用|

### ボタン (設定ボタン)
<img src="https://github.com/cwtickle/danoniplus/assets/44026291/eb98bdc7-dafd-4d3b-9146-5d5d9bc86e8f" width="20%"><img src="https://github.com/cwtickle/danoniplus/assets/44026291/1b482268-c2ae-42e0-bc8b-af583e350a2e" width="20%">


|カスタムプロパティ名|スタイル適用先|設定属性|備考|
|----|----|----|----|
|--button-color-setting-x|.button_Setting|color||
|--button-bg-setting|.button_Setting|background||
|--button-colorHover-setting-x|.button_Setting:hover|color|オンマウス用|
|--button-bgHover-setting|.button_Setting:hover|background|オンマウス用|

### ボタン (進むボタン)
<img src="https://github.com/cwtickle/danoniplus/assets/44026291/05d2a9b3-db31-40eb-9a53-a1394743f5db" width="20%"><img src="https://github.com/cwtickle/danoniplus/assets/44026291/f0cc2530-d88a-4811-b775-3ab459213d3c" width="20%">


|カスタムプロパティ名|スタイル適用先|設定属性|備考|
|----|----|----|----|
|--button-color-next-x|.button_Next|color||
|--button-bg-next|.button_Next|background||
|--button-colorHover-next-x|.button_Next:hover|color|オンマウス用|
|--button-bgHover-next|.button_Next:hover|background|オンマウス用|

### ボタン (リセットボタン)
<img src="https://github.com/cwtickle/danoniplus/assets/44026291/b6ac51bc-69b8-4ebe-82ab-81b20a3864a7" width="20%"><img src="https://github.com/cwtickle/danoniplus/assets/44026291/bfeca1bf-f0a4-4141-94d5-9daadb0767dd" width="20%">

|カスタムプロパティ名|スタイル適用先|設定属性|備考|
|----|----|----|----|
|--button-color-reset-x|.button_Reset|color||
|--button-bg-reset|.button_Reset|background||
|--button-colorHover-reset-x|.button_Reset:hover|color|オンマウス用|
|--button-bgHover-reset|.button_Reset:hover|background|オンマウス用|

### ボタン (SNSボタン)
<img src="https://github.com/cwtickle/danoniplus/assets/44026291/540586b7-c918-4f9e-b9e8-883a16280295" width="20%"><img src="https://github.com/cwtickle/danoniplus/assets/44026291/0959b929-223c-46ca-bc5a-ca71b1279583" width="20%">

|カスタムプロパティ名|スタイル適用先|設定属性|備考|
|----|----|----|----|
|--button-color-sns-x|.button_Tweet|color||
|--button-bg-sns|.button_Tweet|background||
|--button-colorHover-sns-x|.button_Tweet:hover|color|オンマウス用|
|--button-bgHover-sns|.button_Tweet:hover|background|オンマウス用|
|--button-bgHover-discord|.button_Discord:hover|background|オンマウス用　※Discordリンクで使用|

### ボタン (ON/OFFボタン)
<img src="https://github.com/cwtickle/danoniplus/assets/44026291/6f6b6135-b863-4ba4-89bd-3024bdcae8fa" width="50%">

|カスタムプロパティ名|スタイル適用先|設定属性|備考|
|----|----|----|----|
|--button-color-off-x|.button_OFF|color||
|--button-border-off-x|.button_OFF|border-color||
|--button-color-on-x|.button_ON|color||
|--button-border-on-x|.button_ON|border-color||

### ボタン (ON/OFFボタン リバース用)
<img src="https://github.com/cwtickle/danoniplus/assets/44026291/f33e966d-8efc-48a4-8464-bf807482b929" width="20%">
<img src="https://github.com/cwtickle/danoniplus/assets/44026291/ec7f31dd-8b8e-43ec-a451-191b35fedcab" width="20%">

|カスタムプロパティ名|スタイル適用先|設定属性|備考|
|----|----|----|----|
|--button-color-offRev-x|.button_RevOFF|color||
|--button-border-offRev-x|.button_RevOFF|border-color||
|--button-color-onRev-x|.button_RevON|color||
|--button-border-onRev-x|.button_RevON|border-color||

### ボタン (ON/OFFボタン 無効化ボタン用)
<img src="https://github.com/cwtickle/danoniplus/assets/44026291/c56ce773-c9e6-4cca-86f6-d6e53b05a20b" width="50%">

|カスタムプロパティ名|スタイル適用先|設定属性|備考|
|----|----|----|----|
|--button-color-offDisabled-x|.button_DisabledOFF|color||
|--button-border-offDisabled-x|.button_DisabledOFF|border-color||
|--button-color-onDisabled-x|.button_DisabledON|color||
|--button-border-onDisabled-x|.button_DisabledON|border-color||

## 関連項目
- [スキン変更 (style_data)](./dos-e0008-styleData)
- [グラデーション仕様](./dos-c0001-gradation)
- [CSS カスタムプロパティ（変数）の使用 [MDN]🔗](https://developer.mozilla.org/ja/docs/Web/CSS/Using_CSS_custom_properties)
- [border-image [MDN]🔗](https://developer.mozilla.org/ja/docs/Web/CSS/border-image)

## 更新履歴

|Version|変更内容|
|----|----|
|[v39.0.0](https://github.com/cwtickle/danoniplus/releases/tag/v39.0.0)|・PlayWindow, StepArea, FrzReturn, Shaking, Effect, Camoufrage, Swapping, JudgRange, AutoRetry設定の先頭文字色を追加|
|[v36.4.1](https://github.com/cwtickle/danoniplus/releases/tag/v36.4.1)|・Discordリンクのオンマウスの色(`--button-bgHover-discord`)を追加|
|[v36.4.0](https://github.com/cwtickle/danoniplus/releases/tag/v36.4.0)|・キーコンフィグのColorGr./ShuffleGr./StepRtnGr.の色(`--keyconfig-colorGr-x`, `--keyconfig-shuffleGr-x``--keyconfig-stepRtnGr-x`)を追加|
|[v34.5.1](https://github.com/cwtickle/danoniplus/releases/tag/v34.5.1)|・譜面明細子画面(DifLevel)のレーン別矢印数の最大・最小値の色(`--settings-maxArrowCnts-x`, `--settings-minArrowCnts-x`)を追加|
|[v33.2.0](https://github.com/cwtickle/danoniplus/releases/tag/v33.2.0)|・全体の画面枠の設定(`--back-border-x`)を追加|
|[v33.1.1](https://github.com/cwtickle/danoniplus/releases/tag/v33.1.1)|・判定系の表記においてグラデーション指定を取り止め|
|[v33.0.0](https://github.com/cwtickle/danoniplus/releases/tag/v33.0.0)|・カスタムプロパティ名の設定を実装|
|[v10.0.0](https://github.com/cwtickle/danoniplus/releases/tag/v10.0.0)|・初期実装|

| [< 共通設定ファイル仕様](./dos_setting) | **スキンファイル仕様** | [基準ディレクトリ仕様 >](./AboutBaseDirectory) |
