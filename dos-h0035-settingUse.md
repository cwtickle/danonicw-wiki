**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-h0035-settingUse) | Japanese** 

[Home](./) &gt; [譜面ヘッダー仕様](dos_header) &gt; [設定時の初期設定](dos_header#-設定時の初期設定)

| [<- scoreDetailUse](dos-h0060-scoreDetailUse) | **settingUse** | [displayUse ->](dos-h0057-displayUse) |

## settingUse
- オプション有効化設定
- 共通設定 ⇒ [g_presetObj.settingUse](dos-s0006-settingUse#オプション有効化設定-g_presetobjsettinguse)

### 使い方
```
|motionUse=false|
|scrollUse=false|
|reverseUse=false|
|shuffleUse=false|
|autoPlayUse=false|
|gaugeUse=false|
|excessiveUse=false|
|appearanceUse=false|

|playWindowUse=false|
|stepAreaUse=false|
|frzReturnUse=false|
|shakingUse=false|
|effectUse=false|
|camoufrageUse=false|
|swappingUse=false|
|judgRangeUse=false|
|autoRetryUse=false|
```
### 説明
- 設定画面の項目を作品の都合上使用させない場合に使用します。  
settingUseという設定があるわけではなく、実際には各項目別の設定が存在します。  
以下のリストを参照。
- 「scrollUse」については、falseにした場合でもReverseは設定可能です。  
「reverseUse」をfalseにすることで、Reverseも設定不可とすることができます。（ver37.5.0以降）  
- 「shuffleUse」限定で、`|shuffleUse=group|`と指定することで「Random+」「S-Random+」「Scatter+」を除外した設定に変更することができます。  
この設定は譜面ヘッダー限定の設定で、danoni_setting.jsでは使用できません。

|譜面ヘッダー名|制御対象|値と概要|
|----|----|----|
|motionUse|Motion|`true`: Motionを有効化<br>`false`: Motionを無効化|
|scrollUse|Scroll|`true`: Scrollを有効化<br>`false`: Scrollを無効化 (Reverseは無効化しない)|
|reverseUse|Reverse|`true`: Reverseを有効化<br>`false`: Reverseを無効化|
|shuffleUse|Shuffle|`true`: Shuffleを有効化<br>`group`: 「Random+」「S-Random+」「Scatter+」を除外<br>`false`: Reverseを無効化|
|autoPlayUse|AutoPlay|`true`: AutoPlayを有効化<br>`false`: AutoPlayを無効化|
|gaugeUse|Gauge|`true`: Gaugeを有効化<br>`false`: Gaugeを無効化|
|excessiveUse|Excessive<br>(空押し判定)|`true`: 空押し判定を有効化<br>`false`: 空押し判定を無効化|
|appearanceUse|Appearance|`true`: Appearanceを有効化<br>`false`: Appearanceを無効化|
|playWindowUse|PlayWindow|`true`: PlayWindowを有効化<br>`false`: PlayWindowを無効化|
|stepAreaUse|StepArea|`true`: StepAreaを有効化<br>`false`: StepAreaを無効化|
|frzReturnUse|FrzReturn|`true`: FrzReturnを有効化<br>`false`: FrzReturnを無効化|
|shakingUse|Shaking|`true`: Shakingを有効化<br>`false`: Shakingを無効化|
|effectUse|Effect|`true`: Effectを有効化<br>`false`: Effectを無効化|
|camoufrageUse|Camoufrage|`true`: Camoufrageを有効化<br>`false`: Camoufrageを無効化|
|swappingUse|Swapping|`true`: Swappingを有効化<br>`false`: Swappingを無効化|
|judgRangeUse|JudgRange|`true`: JudgRangeを有効化<br>`false`: JudgRangeを無効化|
|autoRetryUse|AutoRetry|`true`: AutoRetryを有効化<br>`false`: AutoRetryを無効化|

|値|既定|内容|
|----|----|----|
|false||設定変更不可|
|true|*|設定変更可|

### 使用例

- 一部オプションを使用禁止にした場合の例

![dos-h0035-01.png](./wiki/dos-h0035-01.png)

### 補足
- danoni_setting.jsの`g_presetObj.settingUse`で作品共通に指定できます。  
両方指定があった場合、譜面ヘッダー側 (settingUse) が優先されます。  

```javascript
g_presetObj.settingUse = {
	motion: `true`,
	scroll: `true`,
	reverse: `true`,
	shuffle: `true`,
	autoPlay: `true`,
	gauge: `true`,
	excessive: `true`,
	appearance: `true`,
};
```

### 関連項目
- [displayUse](dos-h0057-displayUse) [:pencil:](dos-h0035-displayUse/_edit) Display項目の利用有無, 初期値設定
   - stepZoneUse, judgmentUse, fastSlowUse, ... etc
- [transKeyUse](dos-h0024-transKeyUse) [:pencil:](dos-h0024-transKeyUse/_edit) 別キーモードの利用有無
- [customGauge](dos-h0053-customGauge) [:pencil:](dos-h0053-customGauge/_edit) カスタムゲージ設定
- [**gaugeX**](dos-h0022-gaugeX) [:pencil:](dos-h0022-gaugeX/_edit) ゲージ設定の詳細
- [excessiveJdgUse](dos-h0093-excessiveJdgUse) [:pencil:](dos-h0093-excessiveJdgUse/_edit) 空押し判定のデフォルト設定
- [共通設定ファイル仕様](dos_setting) &gt; [オプション有効化](dos-s0006-settingUse)

### 更新履歴

|Version|変更内容|
|----|----|
|[v39.0.0](https://github.com/cwtickle/danoniplus/releases/tag/v39.0.0)|・PlayWindow, StepArea, FrzReturn, Shaking, Effect, Camoufrage, Swapping, JudgRange, AutoRetryの利用有無設定を追加|
|[v37.5.0](https://github.com/cwtickle/danoniplus/releases/tag/v37.5.0)|・Reverseの利用有無設定を追加|
|[v32.5.0](https://github.com/cwtickle/danoniplus/releases/tag/v32.5.0)|・空判定設定実装に伴い、Excessiveの利用有無設定を追加|
|[v22.1.0](https://github.com/cwtickle/danoniplus/releases/tag/v22.1.0)|・Shuffle設定について、部分適用するオプションを追加|
|[v10.2.1](https://github.com/cwtickle/danoniplus/releases/tag/v10.2.1)|・スクロール拡張実装に伴い、Scrollの利用有無設定を追加|
|[v9.0.0](https://github.com/cwtickle/danoniplus/releases/tag/v9.0.0)|・Appearance実装に伴い、Appearanceの利用有無設定を追加|
|[v3.1.0](https://github.com/cwtickle/danoniplus/releases/tag/v3.1.0)|・初回実装|

| [<- scoreDetailUse](dos-h0060-scoreDetailUse) | **settingUse** | [displayUse ->](dos-h0057-displayUse) |