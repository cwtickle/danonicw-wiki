⭐ New Features / 🛠️ Improvements / 🐞 Bug Fixes  
( Files changed: ⚪ danoni_main.js / 🔵 danoni_constants.js / 🔴 css files )

[**<- v19**](Changelog-v19) | **v18** | [**v17 ->**](Changelog-v17)  
(**🔖 [20 Releases](https://github.com/cwtickle/danoniplus/releases?q=tag%3Av18)** )

## [Warning] 更新終了後の不具合情報
- [更新を終了したバージョンの不具合情報#v18](DeprecatedVersionBugs#v18) を参照

## v18.9.6 ([2021-03-06](https://github.com/cwtickle/danoniplus/releases/tag/v18.9.6))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v18.9.6/total)](https://github.com/cwtickle/danoniplus/archive/v18.9.6.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v18.9.6/support/v18?style=social)](https://github.com/cwtickle/danoniplus/compare/v18.9.6...support/v18#files_bucket) 
- 🛠️ Base64エンコードした曲データを直接デコードするよう変更 ( PR [#1009](https://github.com/cwtickle/danoniplus/pull/1009) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v18.9.6)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v18.9.5...v18.9.6#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v18.9.6)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v18.9.6.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v18.9.6.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v18.9.6/js/danoni_main.js)
/ 🎣 [**v20.5.1**](./Changelog-v20#v2051-2021-03-06)

## v18.9.5 ([2021-02-20](https://github.com/cwtickle/danoniplus/releases/tag/v18.9.5))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v18.9.5/total)](https://github.com/cwtickle/danoniplus/archive/v18.9.5.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v18.9.5/support/v18?style=social)](https://github.com/cwtickle/danoniplus/compare/v18.9.5...support/v18#files_bucket) 
- 🛠️ imgタグ生成時においてcrossOrigin属性を付与するよう変更 ( PR [#988](https://github.com/cwtickle/danoniplus/pull/988) )
- 🐞 preloadFile関数において画像ファイル以外が指定された場合の問題を修正 ( PR [#989](https://github.com/cwtickle/danoniplus/pull/989) ) <- :boom: [**v18.5.0**](Changelog-v18#v1850-2020-12-20) 

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v18.9.5)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v18.9.4...v18.9.5#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v18.9.5)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v18.9.5.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v18.9.5.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v18.9.5/js/danoni_main.js)
/ 🎣 [**v20.2.1**](./Changelog-v20#v2021-2021-02-20)

## v18.9.4 ([2021-02-12](https://github.com/cwtickle/danoniplus/releases/tag/v18.9.4))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v18.9.4/total)](https://github.com/cwtickle/danoniplus/archive/v18.9.4.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v18.9.4/support/v18?style=social)](https://github.com/cwtickle/danoniplus/compare/v18.9.4...support/v18#files_bucket) 
- 🐞 createCss2Button関数にて右クリック拡張ができない場合がある問題を修正 ( PR [#970](https://github.com/cwtickle/danoniplus/pull/970) ) <- :boom: [**v17.5.0**](Changelog-v17#v1750-2020-10-17)
- 🐞 changeStyle関数のコードミスを修正 ( PR [#958](https://github.com/cwtickle/danoniplus/pull/958) ) <- :boom: [**v17.1.0**](Changelog-v17#v1710-2020-09-28)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v18.9.4)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v18.9.3...v18.9.4#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v18.9.4)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v18.9.4.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v18.9.4.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v18.9.4/js/danoni_main.js)
/ 🎣 [**v19.5.2**](./Changelog-v19#v1952-2021-02-12), [**v19.4.1**](./Changelog-v19#v1941-2021-02-06)

----

## v18.9.3 ([2021-01-07](https://github.com/cwtickle/danoniplus/releases/tag/v18.9.3))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v18.9.3/total)](https://github.com/cwtickle/danoniplus/archive/v18.9.3.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v18.9.3/support/v18?style=social)](https://github.com/cwtickle/danoniplus/compare/v18.9.3...support/v18#files_bucket) 
- 🐞 AppearanceとOpacityのカーソル設定誤りを修正 ( PR [#933](https://github.com/cwtickle/danoniplus/pull/933) ) <- :boom: [**v18.1.0**](Changelog-v18#v1810-2020-11-02) 

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v18.9.3)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v18.9.2...v18.9.3#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v18.9.3)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v18.9.3.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v18.9.3.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v18.9.3/js/danoni_main.js)<br>❤️ aconite, すずめ (@suzme)

## v18.9.2 ([2021-01-05](https://github.com/cwtickle/danoniplus/releases/tag/v18.9.2))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v18.9.2/total)](https://github.com/cwtickle/danoniplus/archive/v18.9.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v18.9.2/support/v18?style=social)](https://github.com/cwtickle/danoniplus/compare/v18.9.2...support/v18#files_bucket) 
- 🐞 空押し時のヒットモーションが一部動かない問題を修正 ( PR [#932](https://github.com/cwtickle/danoniplus/pull/932) ) <- :boom: [**v18.9.1**](Changelog-v18#v1891-2021-01-03) 

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v18.9.2)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v18.9.1...v18.9.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v18.9.2)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v18.9.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v18.9.2.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v18.9.2/js/danoni_main.js)

## v18.9.1 ([2021-01-03](https://github.com/cwtickle/danoniplus/releases/tag/v18.9.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v18.9.1/total)](https://github.com/cwtickle/danoniplus/archive/v18.9.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v18.9.1/support/v18?style=social)](https://github.com/cwtickle/danoniplus/compare/v18.9.1...support/v18#files_bucket) 
- 🐞 同一レーン上に通常矢印とフリーズアローが短い間隔で存在するとき、通常矢印の判定が優先されてしまう事象を修正 ( PR [#929](https://github.com/cwtickle/danoniplus/pull/929), [#930](https://github.com/cwtickle/danoniplus/pull/930) ) <- :boom: **initial**

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v18.9.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v18.9.0...v18.9.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v18.9.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v18.9.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v18.9.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v18.9.1/js/danoni_main.js)<br>❤️ goe (@goe0)

## v18.9.0 ([2021-01-02](https://github.com/cwtickle/danoniplus/releases/tag/v18.9.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v18.9.0/total)](https://github.com/cwtickle/danoniplus/archive/v18.9.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v18.9.0/support/v18?style=social)](https://github.com/cwtickle/danoniplus/compare/v18.9.0...support/v18#files_bucket) 
- ⭐ タイトルアニメーション設定でアニメーション方法やクラスが指定できるよう変更 ( Gitter [2021-01-01](https://gitter.im/danonicw/community?at=5fef307469ee7f0422d76450), PR [#927](https://github.com/cwtickle/danoniplus/pull/927) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v18.9.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v18.8.2...v18.9.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v18.9.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v18.9.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v18.9.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v18.9.0/js/danoni_main.js)<br>❤️ izkdic

## v18.8.2 ([2020-12-30](https://github.com/cwtickle/danoniplus/releases/tag/v18.8.2))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v18.8.2/total)](https://github.com/cwtickle/danoniplus/archive/v18.8.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v18.8.2/support/v18?style=social)](https://github.com/cwtickle/danoniplus/compare/v18.8.2...support/v18#files_bucket) 
- 🐞 一部のキーを押すと全押し状態になる問題を修正 ( PR [#924](https://github.com/cwtickle/danoniplus/pull/924) ) <- :boom: [**v16.0.0**](Changelog-v16#v1600-2020-08-06) 

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v18.8.2)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v18.8.1...v18.8.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v18.8.2)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v18.8.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v18.8.2.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v18.8.2/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v18.8.2/js/lib/danoni_constants.js)<br>❤️ すずめ (@suzme)

## v18.8.1 ([2020-12-28](https://github.com/cwtickle/danoniplus/releases/tag/v18.8.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v18.8.1/total)](https://github.com/cwtickle/danoniplus/archive/v18.8.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v18.8.1/support/v18?style=social)](https://github.com/cwtickle/danoniplus/compare/v18.8.1...support/v18#files_bucket) 
- 🐞 画像拡張子指定時に画像読込チェックに失敗する問題を修正 ( Gitter [2020-12-28](https://gitter.im/danonicw/community?at=5fe8ae4fdbb17f28c5a284ff), PR [#922](https://github.com/cwtickle/danoniplus/pull/922) ) <- :boom: [**v18.3.0**](Changelog-v18#v1830-2020-11-18) 

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v18.8.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v18.8.0...v18.8.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v18.8.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v18.8.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v18.8.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v18.8.1/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v18.8.1/js/lib/danoni_constants.js)<br>❤️ MFV2 (@MFV2)

## v18.8.0 ([2020-12-27](https://github.com/cwtickle/danoniplus/releases/tag/v18.8.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v18.8.0/total)](https://github.com/cwtickle/danoniplus/archive/v18.8.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v18.8.0/support/v18?style=social)](https://github.com/cwtickle/danoniplus/compare/v18.8.0...support/v18#files_bucket) 
- ⭐ 結果画面にGitterへのリンクを追加 ( Issue [#919](https://github.com/cwtickle/danoniplus/pull/919), PR [#920](https://github.com/cwtickle/danoniplus/pull/920) )
- ⭐ タイトル画面にゲーム画面説明画面へのリンクを追加 ( Issue [#919](https://github.com/cwtickle/danoniplus/pull/919), PR [#920](https://github.com/cwtickle/danoniplus/pull/920) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v18.8.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v18.7.0...v18.8.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v18.8.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v18.8.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v18.8.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v18.8.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v18.8.0/js/lib/danoni_constants.js)

<img src="./wiki/changelog/v18_0001.png" alt="v18_0001" width="70%">

## v18.7.0 ([2020-12-25](https://github.com/cwtickle/danoniplus/releases/tag/v18.7.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v18.7.0/total)](https://github.com/cwtickle/danoniplus/archive/v18.7.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v18.7.0/support/v18?style=social)](https://github.com/cwtickle/danoniplus/compare/v18.7.0...support/v18#files_bucket) 
- 🛠️ ColorType: 2のおにぎりの初期色を白から薄青に変更 ( PR [#916](https://github.com/cwtickle/danoniplus/pull/916) )
- 🐞 カスタムjs, スキンjsを二重読込している問題を修正 ( PR [#917](https://github.com/cwtickle/danoniplus/pull/917) ) <- :boom: [**v7.7.0**](Changelog-v7#v770-2019-08-25) 
- 🐞 楽曲・画像読込チェックの待ち時間誤りを修正 ( PR [#915](https://github.com/cwtickle/danoniplus/pull/915) ) <- :boom: [**v10.1.1**](Changelog-v10#v1011-2019-11-11) 

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v18.7.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v18.6.1...v18.7.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v18.7.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v18.7.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v18.7.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v18.7.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v18.7.0/js/lib/danoni_constants.js)

<img src="./wiki/changelog/v18_0002.png" alt="v18_0002" width="50%">

## v18.6.1 ([2020-12-22](https://github.com/cwtickle/danoniplus/releases/tag/v18.6.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v18.6.1/total)](https://github.com/cwtickle/danoniplus/archive/v18.6.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v18.6.1/support/v18?style=social)](https://github.com/cwtickle/danoniplus/compare/v18.6.1...support/v18#files_bucket) 
- 🐞 追加した画像ファイルがある場合、Firefoxでプレイできない問題を修正 ( PR [#913](https://github.com/cwtickle/danoniplus/pull/913) ) <- :boom: [**v18.5.0**](Changelog-v18#v1850-2020-12-20) 

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v18.6.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v18.6.0...v18.6.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v18.6.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v18.6.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v18.6.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v18.6.1/js/danoni_main.js)

## v18.6.0 ([2020-12-21](https://github.com/cwtickle/danoniplus/releases/tag/v18.6.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v18.6.0/total)](https://github.com/cwtickle/danoniplus/archive/v18.6.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v18.6.0/support/v18?style=social)](https://github.com/cwtickle/danoniplus/compare/v18.6.0...support/v18#files_bucket) 
- ⭐ ライフ制ゲージ設定に「Heavy」を追加 ( Issue [#909](https://github.com/cwtickle/danoniplus/pull/909), PR [#910](https://github.com/cwtickle/danoniplus/pull/910) )
- 🛠️ ゲージ設定順をゲージの緩い順になるよう変更 ( Issue [#909](https://github.com/cwtickle/danoniplus/pull/909), PR [#910](https://github.com/cwtickle/danoniplus/pull/910) )
- 🐞 曲終了直後にリトライキーを押すとALL0のリザルトが表示される問題を修正 ( PR [#908](https://github.com/cwtickle/danoniplus/pull/908) ) <- :boom: [**v0.67.x**](Changelog-v0#v067x-2018-11-17) 

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v18.6.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v18.5.0...v18.6.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v18.6.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v18.6.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v18.6.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v18.6.0/js/danoni_main.js)<br>❤️ izkdic, すずめ (@suzme)

## v18.5.0 ([2020-12-20](https://github.com/cwtickle/danoniplus/releases/tag/v18.5.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v18.5.0/total)](https://github.com/cwtickle/danoniplus/archive/v18.5.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v18.5.0/support/v18?style=social)](https://github.com/cwtickle/danoniplus/compare/v18.5.0...support/v18#files_bucket) 
- ⭐ タイトル文字のアニメーションを1・2行目で個別に設定できるよう変更 ( Issue [#901](https://github.com/cwtickle/danoniplus/pull/901), PR [#904](https://github.com/cwtickle/danoniplus/pull/904) )
- ⭐ 9A/9Bkeyの別キーモード時の配色を一部変更 ( PR [#905](https://github.com/cwtickle/danoniplus/pull/905) )
- ⭐ Firefoxで画像のプリロードを有効にするよう変更 ( Issue [#899](https://github.com/cwtickle/danoniplus/pull/899), PR [#906](https://github.com/cwtickle/danoniplus/pull/906) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v18.5.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v18.4.0...v18.5.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v18.5.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v18.5.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v18.5.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v18.5.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v18.5.0/js/lib/danoni_constants.js)<br>❤️ izkdic

<img src="./wiki/changelog/v18_0003.png" alt="v18_0003" width="70%">

<img src="./wiki/changelog/v18_0004.png" alt="v18_0004" width="70%">

## v18.4.0 ([2020-11-23](https://github.com/cwtickle/danoniplus/releases/tag/v18.4.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v18.4.0/total)](https://github.com/cwtickle/danoniplus/archive/v18.4.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v18.4.0/support/v18?style=social)](https://github.com/cwtickle/danoniplus/compare/v18.4.0...support/v18#files_bucket) 
- ⭐ 背景・マスクモーションのテキスト部分においてHTMLタグを許容するよう変更 ( Gitter [2020-11-21](https://gitter.im/danonicw/community?at=5fb8d6170451324f15281a98), PR [#895](https://github.com/cwtickle/danoniplus/pull/895) )
- 🛠️ タグ用括弧のエスケープ文字を追加 ( PR [#896](https://github.com/cwtickle/danoniplus/pull/896) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v18.4.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v18.3.0...v18.4.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v18.4.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v18.4.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v18.4.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v18.4.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v18.4.0/js/lib/danoni_constants.js)<br>❤️ goe (@goe0)

## v18.3.0 ([2020-11-18](https://github.com/cwtickle/danoniplus/releases/tag/v18.3.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v18.3.0/total)](https://github.com/cwtickle/danoniplus/archive/v18.3.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v18.3.0/support/v18?style=social)](https://github.com/cwtickle/danoniplus/compare/v18.3.0...support/v18#files_bucket) 
- ⭐ 背景・マスクモーションで使用可能な画像ファイル拡張子にsvg形式を追加 ( Gitter [2020-11-16](https://gitter.im/danonicw/community?at=5fb2820cb4283c208a6acbad), PR [#891](https://github.com/cwtickle/danoniplus/pull/891) )
- ⭐ ColorTypeのローカルストレージ保存に対応 ( Gitter [2020-11-16](https://gitter.im/danonicw/community?at=5fb2820cb4283c208a6acbad), PR [#892](https://github.com/cwtickle/danoniplus/pull/892) )
- 🛠️ 背景・マスクモーションで使用可能な画像ファイル拡張子について
英大文字の場合でも対象とするよう変更 ( PR [#891](https://github.com/cwtickle/danoniplus/pull/891) )
- 🐞 Display:Colorのボタンが無効化されていた場合にColorTypeの挙動により、Display:Colorの切り替えができてしまう問題を修正 ( PR [#892](https://github.com/cwtickle/danoniplus/pull/892) ) <- :boom: [**v14.0.2**](Changelog-v14#v1402-2020-04-29) 

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v18.3.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v18.2.0...v18.3.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v18.3.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v18.3.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v18.3.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v18.3.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v18.3.0/js/lib/danoni_constants.js)<br>❤️ izkdic

## v18.2.0 ([2020-11-08](https://github.com/cwtickle/danoniplus/releases/tag/v18.2.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v18.2.0/total)](https://github.com/cwtickle/danoniplus/archive/v18.2.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v18.2.0/support/v18?style=social)](https://github.com/cwtickle/danoniplus/compare/v18.2.0...support/v18#files_bucket) 
- ⭐ 23keyを既定キーとして実装 ( Issue [#887](https://github.com/cwtickle/danoniplus/pull/887), PR [#888](https://github.com/cwtickle/danoniplus/pull/888) )
- ⭐ 矢印・フリーズアロー色の強制グラデーション指定/解除設定を実装 ( Gitter [2020-11-08](https://gitter.im/danonicw/community?at=5fa7a00406fa0513dd9d1316), PR [#890](https://github.com/cwtickle/danoniplus/pull/890) )
- 🛠️ ConfigType/ColorTypeについて右クリックによる逆回し設定に対応 ( PR [#890](https://github.com/cwtickle/danoniplus/pull/890) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v18.2.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v18.1.0...v18.2.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v18.2.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v18.2.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v18.2.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v18.2.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v18.2.0/js/lib/danoni_constants.js)<br>❤️ izkdic

<img src="./wiki/changelog/v18_0005.png" alt="v18_0005" width="70%">

## v18.1.0 ([2020-11-02](https://github.com/cwtickle/danoniplus/releases/tag/v18.1.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v18.1.0/total)](https://github.com/cwtickle/danoniplus/archive/v18.1.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v18.1.0/support/v18?style=social)](https://github.com/cwtickle/danoniplus/compare/v18.1.0...support/v18#files_bucket) 
- ⭐ Display設定周りのローカルストレージ保存に対応 ( Issue [#881](https://github.com/cwtickle/danoniplus/pull/881), PR [#885](https://github.com/cwtickle/danoniplus/pull/885) )
- ⭐ Adjustment, Volume, Appearance, Opacityに限り、ローカル保存している設定を選択時に
アスタリスクを付けるように変更 ( PR [#885](https://github.com/cwtickle/danoniplus/pull/885) )
- 🐞 速度データ(speed_change)が空の場合、データがあると判断されてしまいspeed_changeが優先されてしまう問題を修正 ( PR [#884](https://github.com/cwtickle/danoniplus/pull/884) ) <- :boom: [**v8.0.0**](Changelog-v8#v800-2019-09-08) 

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v18.1.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v18.0.1...v18.1.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v18.1.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v18.1.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v18.1.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v18.1.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v18.1.0/js/lib/danoni_constants.js)

## v18.0.1 ([2020-10-31](https://github.com/cwtickle/danoniplus/releases/tag/v18.0.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v18.0.1/total)](https://github.com/cwtickle/danoniplus/archive/v18.0.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v18.0.1/support/v18?style=social)](https://github.com/cwtickle/danoniplus/compare/v18.0.1...support/v18#files_bucket) 
- 🐞 複数曲搭載時に曲名の2段目がundefinedになる問題を修正 ( PR [#882](https://github.com/cwtickle/danoniplus/pull/882) ) <- :boom: [**v17.3.0**](Changelog-v17#v1730-2020-10-08)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v18.0.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v18.0.0...v18.0.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v18.0.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v18.0.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v18.0.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v18.0.1/js/danoni_main.js)

## v18.0.0 ([2020-10-25](https://github.com/cwtickle/danoniplus/releases/tag/v18.0.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v18.0.0/total)](https://github.com/cwtickle/danoniplus/archive/v18.0.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v18.0.0/support/v18?style=social)](https://github.com/cwtickle/danoniplus/compare/v18.0.0...support/v18#files_bucket) 
- 🛠️ 矢印・フリーズアロー個別属性を見直し ( Issue [#878](https://github.com/cwtickle/danoniplus/pull/878), PR [#879](https://github.com/cwtickle/danoniplus/pull/879) )
- 🛠️ その他コードの整理 ( PR [#876](https://github.com/cwtickle/danoniplus/pull/876), [#879](https://github.com/cwtickle/danoniplus/pull/879) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v18.0.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v17.5.1...v18.0.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v18.0.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v18.0.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v18.0.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v18.0.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v18.0.0/js/lib/danoni_constants.js)

[**<- v19**](Changelog-v19) | **v18** | [**v17 ->**](Changelog-v17)