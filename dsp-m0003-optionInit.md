[← オブジェクトリファレンスに戻る](ObjectReferenceIndex)  

# 設定画面表示
### 概要
- 設定画面の各項目を表示する。

### フローチャート
- カスタムJs/スキンJsによる差し込み処理は水色で表記。
- 関連ページがあるボックスは青枠で表記。

※setDifficultyの部分はカスタムキーかどうかの条件式などが実際には存在するが、  
　どちらの条件でもほぼ同じ処理を行うため、便宜上省略している。
```mermaid
flowchart TD
    A([start])-->optionInit-->Z([end]);

subgraph optionInit
    B["画面クリア・背景再描画<br>clearWindow"]-->C["タイトル文字描画<br>getTitleDivLabel"]
    C-->D-->E["ユーザカスタムイベント<br>g_customObj.option"]
    E-->F["ボタン作成処理<br>commonSettingBtn"]-->G["キー操作イベントの作成<br>setShortcutEvent"]
    G-->H["ユーザスキン設定<br>g_skinJsObj.option"]
end;

subgraph D["設定用ウィンドウの作成 / createOptionWindow"]
    x1([start])-->a1["設定用ラベル・ボタンの作成<br>createGeneralSetting"]-->b1-->y1([end])
end;

subgraph b1["譜面変更ボタンの処理 / setDifficulty"]
    x2([start])-->a2["現在の譜面のキー数を取得"]-->b2{"設定画面に切替済で、<br>ひとつ前の譜面と同じキー数か？<br>もしくは、初期化フラグが有効か？"};
    b2-->|Yes| c2["初期表示か？"];
    b2-->|No| i2["スクロール、アシスト用の配列を入れ替え"];
    c2-->|Yes| d2["キーパターン、シャッフルグループを初期化"]-->e2{"ひとつ前の譜面と同じキー数か？"};
    c2-->|No| i2;
    e2-->|Yes| f2["ローカルストレージから<br>キーコンフィグを取得"]-->i2;
    e2-->|No| i2;
    i2-->|名称の設定| j2["譜面名の設定"]-->k2["速度の初期化"]-->l2{"譜面明細表示が有効？"};
    l2-->|Yes| m2["速度変化グラフの描画<br>drawSpeedGraph"]-->n2["譜面密度グラフの描画<br>drawDensityGraph"]-->o2["譜面難易度等の表示<br>makeDifInfo"]-->p2["リバース/スクロール設定の初期化"]-->q2["オート/アシスト設定の初期化"]-->r2["カスタムゲージの設定入れ替え"]-->s2["ゲージ設定の初期化<br>setGauge"]-->t2["ユーザカスタムイベント<br>g_customJsObj.difficulty"]-->u2["設定画面を切替済みにする<br>g_canLoadDifInfoFlg = true"]-->y2([end]);
    l2-->|No| p2;
end;

click B "https://github.com/cwtickle/danoniplus/wiki/fnc-c0021-clearWindow" _blank;
style B stroke:#0000ff,stroke-width:4px;
style E fill:#00ffff;
click G "https://github.com/cwtickle/danoniplus/wiki/fnc-c0035-setShortcutEvent" _blank;
style G stroke:#0000ff,stroke-width:4px;
style H fill:#00ffff;
style t2 fill:#00ffff;
```

### 関連項目
- [ローカルストレージ仕様](LocalStorage)
- [カスタムJs(スキンJs)による処理割り込み](AboutCustomJs)