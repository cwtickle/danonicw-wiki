[← オブジェクトリファレンスに戻る](ObjectReferenceIndex)  

## g_lblNameObj / g_lang_lblNameObj

### 概要
- ラベル全般を管理するオブジェクト。
- `g_lang_lblNameObj`は`g_lblNameObj`の言語別版で、  
`g_lang_lblNameObj.Ja/En`側のプロパティが優先される設定になっている。
- どこにマッピングされるかについては、[ID一覧](IdReferenceIndex)を参照。

### 生成タイミング
- 初回起動時  
(言語別の箇所は headerConvert関数呼び出し時)

### プロパティ

#### ページタイトル
```javascript
{
    dancing: `DANCING`,
    star: `☆`,
    onigiri: `ONIGIRI`,
    settings: `SETTINGS`,
    display: `DISPLAY`,
    key: `KEY`,
    config: `CONFIG`,
    result: `RESULT`,
}
```

#### 補足メッセージ
- 日本語/英語で表記が異なる
```javascript
{
    kcDesc: `[{0}:スキップ / {1}:(代替キーのみ)キー無効化]`,
    kcShuffleDesc: `番号をクリックでシャッフルグループ、矢印をクリックでカラーグループを一時的に変更`,
    sdDesc: `[クリックでON/OFFを切替、灰色でOFF]`,
    kcShortcutDesc: `プレイ中ショートカット：「{0}」タイトルバック / 「{1}」リトライ`,
    transKeyDesc: `別キーモードではハイスコア、キーコンフィグ等は保存されません`,
    sdShortcutDesc: `Hid+/Sud+時ショートカット：「pageUp」カバーを上へ / 「pageDown」下へ`,
    resultImageDesc: `画像を右クリックしてコピーできます`,
}
```

#### 固定ボタン
```javascript
{
    maker: `Maker`,
    artist: `Artist`,

    dataReset: `Data Reset`,
    dataSave: `Data Save`,
    clickHere: `Click Here!!`,
    comment: `Comment`,
}
```

#### ロード時表示部分
```javascript
{
    nowLoading: `Now Loading...`,
    pleaseWait: `Please Wait...`,
}
```

#### 画面移動系ボタン
```javascript
{
    b_back: `Back`,
    b_keyConfig: `KeyConfig`,
    b_play: `PLAY!`,
    b_reset: `Reset`,
    b_settings: `To Settings`,
    b_copy: `CopyResult`,
    b_tweet: `Post X`,
    b_gitter: `Discord`,
    b_retry: `Retry`,
    b_close: `Close`,
    b_cReset: `Reset`,
}
```

#### 設定画面（ラベル）
```javascript
{
    Difficulty: `Difficulty`,
    Speed: `Speed`,
    Motion: `Motion`,
    Scroll: `Scroll`,
    Reverse: `Reverse`,
    Shuffle: `Shuffle`,
    AutoPlay: `AutoPlay`,
    Gauge: `Gauge`,
    Excessive: `Excessive`,
    Adjustment: `Adjustment`,
    Fadein: `Fadein`,
    Volume: `Volume`,

    multi: `x`,
    frame: `f`,
    percent: `%`,
    pixel: `px`,
    filterLock: `Lock`,
}
```

#### 設定画面（ショートカット表示）
```javascript
{
    sc_speed: `←→`,
    sc_scroll: `R/↑↓`,
    sc_adjustment: `- +`,
    sc_hitPosition: `T B`,
    sc_keyConfigPlay: g_isMac ? `Del+Enter` : `BS+Enter`,
}
```

#### 設定画面（ゲージ詳細表示）
```javascript
{
    g_start: `Start`,
    g_border: `Border`,
    g_recovery: `Recovery`,
    g_damage: `Damage`,
    g_rate: `Accuracy`,
}
```

#### 設定画面（速度・密度グラフ、ツール値表示）
- s_level ~ s_printHeader は日本語/英語で異なる
```javascript
{
    s_speed: `Overall`,
    s_boost: `Boost`,
    s_avg: `Avg.`,
    s_avgDspeed: `AvgS)`,
    s_avgDboost: `AvgB)`,

    s_apm: `APM`,
    s_time: `Time`,
    s_arrow: `Arrow`,
    s_frz: `Frz`,

    s_level: `Level`,
    s_douji: `同時補正`,
    s_tate: `縦連補正`,
    s_cnts: `All Arrows`,
    s_linecnts: `- 矢印 Arrow:<br><br>- 氷矢 Frz:<br><br>- 3つ押し位置 ({0}):`,
    s_print: `データ出力`,
    s_printTitle: `Dancing☆Onigiri レベル計算ツール+++`,
    s_printHeader: `難易度\t同時\t縦連\t総数\t矢印\t氷矢印\tAPM\t時間`,
}
```

#### 設定画面（Display画面）
```javascript
{
    d_StepZone: `StepZone`,
    d_Judgment: `Judgment`,
    d_FastSlow: `FastSlow`,
    d_LifeGauge: `LifeGauge`,
    d_Score: `Score`,
    d_MusicInfo: `MusicInfo`,
    d_FilterLine: `FilterLine`,
    d_Speed: `Speed`,
    d_Color: `Color`,
    d_Lyrics: `Lyrics`,
    d_Background: `Background`,
    d_ArrowEffect: `ArrowEffect`,
    d_Special: `Special`,

    Appearance: `Appearance`,
    Opacity: `Opacity`,
    HitPosition: `HitPosition`,
}
```

#### 各種設定項目
```javascript
{
    'u_x': `x`,
    'u_%': `%`,
    'u_key': `key`,
    'u_k-': `k-`,

    'u_OFF': `OFF`,
    'u_ON': `ON`,
    'u_Boost': `Boost`,
    'u_Hi-Boost': `Hi-Boost`,
    'u_Brake': `Brake`,

    'u_Cross': `Cross`,
    'u_Split': `Split`,
    'u_Alternate': `Alternate`,
    'u_Twist': `Twist`,
    'u_Asymmetry': `Asymmetry`,
    'u_Flat': `Flat`,
    'u_R-': `R-`,
    'u_Reverse': `Reverse`,

    'u_Mirror': `Mirror`,
    'u_X-Mirror': `X-Mirror`,
    'u_Turning': `Turning`,
    'u_Random': `Random`,
    'u_Random+': `Random+`,
    'u_S-Random': `S-Random`,
    'u_S-Random+': `S-Random+`,
    'u_(S)': `(S)`,

    'u_ALL': `ALL`,
    'u_Onigiri': `Onigiri`,
    'u_Left': `Left`,
    'u_Right': `Right`,
    'u_less': `less`,

    'u_Original': `Original`,
    'u_Heavy': `Heavy`,
    'u_NoRecovery': `NoRecovery`,
    'u_SuddenDeath': `SuddenDeath`,
    'u_Practice': `Practice`,
    'u_Light': `Light`,

    'u_Normal': `Normal`,
    'u_Hard': `Hard`,
    'u_Easy': `Easy`,

    'u_FlatBar': `FlatBar`,

    'u_Visible': `Visible`,
    'u_Hidden': `Hidden`,
    'u_Hidden+': `Hidden+`,
    'u_Sudden': `Sudden`,
    'u_Sudden+': `Sudden+`,
    'u_Hid&Sud+': `Hid&Sud+`,

    'u_Speed': `Speed`,
    'u_Density': `Density`,
    'u_ToolDif': `ToolDif`,
    'u_HighScore': `HighScore`,

    'u_Main': `Main`,
    'u_Replaced': `Replaced`,

    'u_Default': `Default`,
    'u_Type0': `Type0`,
    'u_Type1': `Type1`,
    'u_Type2': `Type2`,
    'u_Type3': `Type3`,
    'u_Type4': `Type4`,
}
```

#### キーコンフィグ画面
```javascript
{
    ConfigType: `ConfigType`,
    ColorType: `ColorType`,
    ImgType: `ImgType`,
    ColorGroup: `ColorGr`,
    ShuffleGroup: `ShuffleGr`,
    StepRtnGroup: `ShapeGr.`,
    KeyPattern: `KeyPattern`,
}
```

#### 判定キャラクタ、パーフェクト演出
- 判定キャラクタは日本語/英語で異なる
```javascript
{
    j_ii: "(・∀・)ｲｲ!!",
    j_shakin: "(`・ω・)ｼｬｷﾝ",
    j_matari: "( ´∀`)ﾏﾀｰﾘ",
    j_shobon: "(´・ω・`)ｼｮﾎﾞｰﾝ",
    j_uwan: "( `Д´)ｳﾜｧﾝ!!",

    j_kita: "(ﾟ∀ﾟ)ｷﾀ-!!",
    j_iknai: "(・A・)ｲｸﾅｲ",

    j_maxCombo: `MaxCombo`,
    j_fmaxCombo: `FreezeCombo`,
    j_score: `Score`,

    j_fast: `Fast`,
    j_slow: `Slow`,
    j_adj: `推定Adj`,
    j_excessive: `Excessive`,

    allPerfect: `All Perfect!!`,
    perfect: `Perfect!!`,
    fullCombo: `FullCombo!`,
    cleared: `CLEARED!`,
    failed: `FAILED...`,
}
```

#### 結果画面（ラベル、略名表示）
```javascript
{
    rt_Music: `Music`,
    rt_Difficulty: `Difficulty`,
    rt_Style: `Playstyle`,
    rt_Display: `Display`,

    rd_StepZone: `Step`,
    rd_Judgment: `Judge`,
    rd_FastSlow: `FS`,
    rd_LifeGauge: `Life`,
    rd_Score: `Score`,
    rd_MusicInfo: `MusicInfo`,
    rd_FilterLine: `Filter`,
    rd_Speed: `Speed`,
    rd_Color: `Color`,
    rd_Lyrics: `Lyrics`,
    rd_Background: `Back`,
    rd_ArrowEffect: `Effect`,
    rd_Special: `SP`,
}
```

### 更新履歴

|Version|変更内容|
|----|----|
|[v33.1.0](https://github.com/cwtickle/danoniplus/releases/tag/v33.1.0)|・リザルト画像コピー用メッセージ(resultImageDesc), リザルト画像コピー画面のClose文字(b_close)を追加|
|[v32.5.0](https://github.com/cwtickle/danoniplus/releases/tag/v32.5.0)|・空押し設定及び表示部分のラベル(Excessive, j_excessive)を追加|
|[v32.4.0](https://github.com/cwtickle/danoniplus/releases/tag/v32.4.0)|・ゲージ詳細部分のAccuracyを表示するラベル(g_rate)を追加|
|[v32.2.0](https://github.com/cwtickle/danoniplus/releases/tag/v32.2.0)|・速度変化グラフの速度変化平均値を表示するラベル(s_avg, s_avgDspeed, s_avgDboost)を追加|
|[v31.0.0](https://github.com/cwtickle/danoniplus/releases/tag/v31.0.0)|・HitPosition設定追加に伴いラベル(pixel, sc_hitPosition, HitPosition)を追加|
|[v25.5.1](https://github.com/cwtickle/danoniplus/releases/tag/v25.5.1)|・推定Adjustment値のラベル(j_adj)を追加|
|[v25.4.0](https://github.com/cwtickle/danoniplus/releases/tag/v25.4.0)|・キー名部分(u_key, u_k-)を追加|
|[v24.5.0](https://github.com/cwtickle/danoniplus/releases/tag/v24.5.0)|・設定画面（ラベル）にfilterLock(Hidden+, Sudden+のレーンカバーロック用)を追加|
|[v24.4.0](https://github.com/cwtickle/danoniplus/releases/tag/v24.4.0)|・設定画面（ショートカット表示）にsc_keyConfigPlay(キーコンフィグ画面のプレイボタン用)を追加|
|[v24.0.0](https://github.com/cwtickle/danoniplus/releases/tag/v24.0.0)|・各種設定項目にアシストプレイ時の設定名(u_less)を追加<br>・言語別のオブジェクト(g_lang_lblNameObj)を追加|
|[v23.3.1](https://github.com/cwtickle/danoniplus/releases/tag/v23.3.1)|・ショートカットキー変更に伴い、設定画面（ショートカット表示）の sc_scroll の値を変更|
|[v23.0.0](https://github.com/cwtickle/danoniplus/releases/tag/v23.0.0)|・設定画面（ラベル）にフレーム単位を追加|
|[v22.5.1](https://github.com/cwtickle/danoniplus/releases/tag/v22.5.1)|・キーコンフィグ画面にImgTypeを追加|
|[v22.4.1](https://github.com/cwtickle/danoniplus/releases/tag/v22.4.1)|・キーコンフィグ画面にColorGroupを追加<br>・補足メッセージにkcShuffleDescを追加|
|[v22.2.0](https://github.com/cwtickle/danoniplus/releases/tag/v22.2.0)|・各種設定項目にType3, Type4(u_Type3, u_Type4)を追加|
|[v22.1.0](https://github.com/cwtickle/danoniplus/releases/tag/v22.1.0)|・各種設定項目にAsym-Mirror(u_Asym-Mirror)を追加|
|[v22.0.0](https://github.com/cwtickle/danoniplus/releases/tag/v22.0.0)|・キーコンフィグ画面にShuffleGroupを追加|
|[v20.3.1](https://github.com/cwtickle/danoniplus/releases/tag/v20.3.1)|・各種設定項目、結果画面（ラベル、略名表示）を追加|
|[v19.3.0](https://github.com/cwtickle/danoniplus/releases/tag/v19.3.0)|・初回実装|