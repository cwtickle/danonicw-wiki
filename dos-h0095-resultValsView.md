**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-h0095-resultValsView) | Japanese** 

[Home](./) &gt; [譜面ヘッダー仕様](dos_header) &gt; [タイトル・結果画面の初期設定](dos_header#%EF%B8%8F-タイトル結果画面の初期設定)

| [<- resultFormat](dos-h0072-resultFormat) | **resultValsView** || [customJs ->](dos-h0019-customjs) |

## resultValsView
- リザルトデータ用のカスタム変数の画像表示設定
- 共通設定 ⇒ [g_presetObj.resultValsView](dos-s0008-resultVals#リザルトデータ用のカスタム変数の画像表示設定-g_presetobjresultvalsview)

### 使い方
```
|resultValsView=exScore|
```

### 説明
リザルト画像に表示するカスタム変数を `g_presetObj.resultVals` の項目から配列形式で指定します。  
カンマ区切りで複数選択することが可能です。

設定がない場合は、`g_presetObj.resultVals`で定義した項目すべてを出力します。

#### g_presetObj.resultVals の実装例
- resultValsViewでは、下記の項目(コロンより前の値)を指定します。
```javascript
g_presetObj.resultVals = {
    exScore: `exScores`;     // g_resultObj.exScores が対応
};
```
<img src="https://github.com/cwtickle/danoniplus/assets/44026291/2ba43b21-1782-4f8c-bd3f-46868aa96917" width="50%">

### 関連項目
- [共通設定ファイル仕様](dos_setting) &gt; [リザルトデータ](dos-s0008-resultVals)

### 更新履歴

|Version|変更内容|
|----|----|
|[v33.3.0](https://github.com/cwtickle/danoniplus/releases/tag/v33.3.0)|・初回実装|

| [<- resultFormat](dos-h0072-resultFormat) | **resultValsView** || [customJs ->](dos-h0019-customjs) |