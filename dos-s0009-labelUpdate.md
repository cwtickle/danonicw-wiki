**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-s0009-labelUpdate) | Japanese** 

[↑ 共通設定ファイル仕様に戻る](dos_setting)

[ <- リザルトデータ ](dos-s0008-resultVals) | **ラベルテキスト・メッセージ** | [カスタムキー定義 -> ](dos-s0010-customKeys)

## ラベルテキスト・メッセージ
### ラベルテキスト設定 (g_presetObj.lblName)
⇒ 指定があった場合に優先される譜面ヘッダー：なし
- danoni_constants.jsにある [g_lblNameObj](obj-v0015-g_lblNameObj) を上書きする設定を定義できます。  
上書きするプロパティに対して、適用したい値を指定してください。変更したい分だけ追加が可能です。  
この設定は、カスタムjsやスキンjsでも利用可能です。  
※v24以降は言語別(Ja:日本語, En:英語)に指定します。指定しない場合はデフォルト値が適用されます。
```javascript
g_presetObj.lblName = {
    Ja: {
        settings: `オプション`,
    },
    En: {
        settings: `Options`,
    }
};
```
<details>
<summary>g_lblNameObj で定義されているプロパティ一覧</summary>

- ここで定義されているプロパティを、`g_presetObj.lblName`で上書きできます。  
※下記は一例です。詳細は [g_lblNameObj (g_lang_lblNameObj)](obj-v0015-g_lblNameObj) をご覧ください。

```javascript
g_lang_lblNameObj.Ja = {
    dancing: `DANCING`,
    star: `☆`,
    onigiri: `ONIGIRI`,
    settings: `SETTINGS`,
    display: `DISPLAY`,
    key: `KEY`,
    config: `CONFIG`,
    result: `RESULT`,

    b_back: `Back`,
    b_keyConfig: `KeyConfig`,
    b_play: `PLAY!`,
    b_reset: `Reset`,
    b_settings: `To Settings`,
    b_copy: `CopyResult`,
    b_tweet: `Tweet`,
    b_gitter: `Gitter`,
    b_retry: `Retry`,

    Difficulty: `Difficulty`,
    Speed: `Speed`,
    Motion: `Motion`,
    Scroll: `Scroll`,
    Reverse: `Reverse`,
    Shuffle: `Shuffle`,
    AutoPlay: `AutoPlay`,
    Gauge: `Gauge`,
    Adjustment: `Adjustment`,
    Fadein: `Fadein`,
    Volume: `Volume`,

    d_StepZone: `StepZone`,
    d_Judgment: `Judgment`,
    d_FastSlow: `FastSlow`,
    d_LifeGauge: `LifeGauge`,
    d_Score: `Score`,
    d_MusicInfo: `MusicInfo`,
    d_FilterLine: `FilterLine`,
    d_Speed: `Speed`,
    d_Color: `Color`,
    d_Lyrics: `Lyrics`,
    d_Background: `Background`,
    d_ArrowEffect: `ArrowEffect`,
    d_Special: `Special`,

    Appearance: `Appearance`,
    Opacity: `Opacity`,

    ConfigType: `ConfigType`,
    ColorType: `ColorType`,
    KeyPattern: `KeyPattern`,

    j_ii: "(・∀・)ｲｲ!!",
    j_shakin: "(`・ω・)ｼｬｷﾝ",
    j_matari: "( ´∀`)ﾏﾀｰﾘ",
    j_shobon: "(´・ω・`)ｼｮﾎﾞｰﾝ",
    j_uwan: "( `Д´)ｳﾜｧﾝ!!",

    j_kita: "(ﾟ∀ﾟ)ｷﾀ-!!",
    j_iknai: "(・A・)ｲｸﾅｲ",

    j_maxCombo: `MaxCombo`,
    j_fmaxCombo: `FreezeCombo`,
    j_score: `Score`,

    j_fast: `Fast`,
    j_slow: `Slow`,

    allPerfect: `All Perfect!!`,
    perfect: `Perfect!!`,
    fullCombo: `FullCombo!`,
    cleared: `CLEARED!`,
    failed: `FAILED...`,
};
```
</details>

### オンマウステキスト、確認メッセージ定義設定 (g_presetObj.msg)
⇒ 指定があった場合に優先される譜面ヘッダー：なし
- danoni_constants.jsにある [g_msgObj](obj-v0016-g_msgObj) を上書きする設定を定義できます。  
この設定は、カスタムjsやスキンjsでも利用可能です。  
※v24以降は言語別(Ja:日本語, En:英語)に指定します。指定しない場合はデフォルト値が適用されます。
```javascript
g_presetObj.msg = {
    Ja: {
        reload: `ページを再読込します。`,
    },
    En: {
        reload: `Reload this page.`,
    },
};
```
<details>
<summary>g_msgObj で定義されているプロパティ一覧</summary>

- ここで定義されているプロパティを、`g_presetObj.msg`で上書きできます。  
※下記は一例です。詳細は [g_msgObj (g_lang_msgObj)](obj-v0016-g_msgObj) をご覧ください。

```javascript
g_lang_msgObj.Ja = {

    reload: `ページを再読込します。`,
    howto: `ゲーム画面の見方や設定の詳細についてのページへ移動します（GitHub Wiki）。`,
    dataReset: `この作品で保存されているハイスコアや\nAdjustment情報等をリセットします。`,
    github: `Dancing☆Onigiri (CW Edition)のGitHubページへ移動します。`,
    security: `Dancing☆Onigiri (CW Edition)のサポート情報ページへ移動します。`,

    dataResetConfirm: `この作品のローカル設定をクリアします。よろしいですか？\n(ハイスコアやAdjustment等のデータがクリアされます)`,
    keyResetConfirm: `キーを初期配置に戻します。よろしいですか？`,

    difficulty: `譜面を選択します。`,
    speed: `矢印の流れる速度を設定します。`,
    motion: `矢印の速度を一定ではなく、\n変動させるモーションをつけるか設定します。`,
    reverse: `矢印の流れる向きを設定します。`,
    scroll: `各レーンのスクロール方向をパターンに沿って設定します。`,
    shuffle: `譜面を左右反転したり、ランダムにします。\nランダムにした場合は別譜面扱いとなり、ハイスコアは保存されません。`,
    autoPlay: `オートプレイや一部キーを自動で打たせる設定を行います。\nオートプレイ時はハイスコアを保存しません。`,
    gauge: `クリア条件を設定します。`,
    adjustment: `タイミングにズレを感じる場合、\n数値を変えることでズレを直すことができます。`,
    fadein: `譜面を途中から再生します。\n途中から開始した場合はハイスコアを保存しません。`,
    volume: `ゲーム内の音量を設定します。`,

    graph: `速度変化や譜面密度状況、\n譜面の難易度など譜面の詳細情報を表示します。`,
    dataSave: `ハイスコア、リバース設定、\nキーコンフィグの保存の有無を設定します。`,
    toDisplay: `プレイ画面上のオブジェクトの\n表示・非表示（一部透明度）を設定します。`,
    toSettings: `SETTINGS画面へ戻ります。`,

    d_stepzone: `ステップゾーンの表示`,
    d_judgment: `判定キャラクタ・コンボの表示`,
    d_fastslow: `Fast/Slow表示`,
    d_lifegauge: `ライフゲージの表示`,
    d_score: `現時点の判定数を表示`,
    d_musicinfo: `音楽情報（時間表示含む）`,
    d_filterline: `Hidden+, Sudden+使用時のフィルターの境界線表示`,
    d_speed: `途中変速、個別加速の有効化設定`,
    d_color: `色変化の有効化設定`,
    d_lyrics: `歌詞表示の有効化設定`,
    d_background: `背景・マスクモーションの有効化設定`,
    d_arroweffect: `矢印・フリーズアローモーションの有効化設定`,
    d_special: `作品固有の特殊演出の有効化設定`,

    appearance: `流れる矢印の見え方を制御します。`,
    opacity: `判定キャラクタ、コンボ数、Fast/Slow、Hidden+/Sudden+の\n境界線表示の透明度を設定します。`,

};
```

</details>

### 設定名の上書き可否設定 (g_presetObj.lblRenames)
⇒ 指定があった場合に優先される譜面ヘッダー：なし
- [g_lblNameObj / g_lang_lblNameObj](obj-v0015-g_lblNameObj) のうち、  
プロパティ名が`u_`で始まるプロパティを設定した値で置き換えるかを画面別に設定します。 
- デフォルトはすべて`true`で、設定した値で書き換えるようになっています。 `false`にすると書き換えません。
- `u_`で始まらないプロパティは無条件で置き換えます。  
`u_`で始まるプロパティは画面をまたがって設定する項目のため、  
画面によってはそのままにしたい場合に使用します。

```javascript
g_presetObj.lblRenames = {
	option: true,
	settingsDisplay: true,
	keyConfig: true,
        main: true,
	result: true,
};
```

#### `u_`で始まるプロパティの例
- 設定画面の設定名が主に該当します。[g_lblNameObj](obj-v0015-g_lblNameObj) もご覧ください。

```javascript
{
    'u_Cross': `Cross`,
    'u_Split': `Split`,
    'u_Alternate': `Alternate`,
    'u_Twist': `Twist`,
    'u_Asymmetry': `Asymmetry`,
    'u_Flat': `Flat`,
    'u_R-': `R-`,
    'u_Reverse': `Reverse`,
}
```

## 更新履歴

|Version|変更内容|
|----|----|
|[v26.3.1](https://github.com/cwtickle/danoniplus/releases/tag/v26.3.1)|・変数名変更<br>　・g_local_lblNameObj -> g_presetObj.lblName<br>　・g_local_msgObj -> g_presetObj.msg<br>　・g_lblRenames -> g_presetObj.lblRenames|
|[v20.3.1](https://github.com/cwtickle/danoniplus/releases/tag/v20.3.1)|・設定名の上書き可否設定 (g_lblRenames)を実装|
|[v19.3.0](https://github.com/cwtickle/danoniplus/releases/tag/v19.3.0)|・ラベルテキスト、オンマウステキスト、確認メッセージ定義設定<br>　 (g_local_lblNameObj, g_local_msgObj)を実装|

[ <- リザルトデータ ](dos-s0008-resultVals) | **ラベルテキスト・メッセージ** | [カスタムキー定義 -> ](dos-s0010-customKeys)