
[^ Tips Indexに戻る](./tips-index)

| < [WordPressを使って作品を公開](./tips-0008-wordpress) | **複数バージョンのファイル切り替え** | [ローカルでのプレイ方法(補足)](./HowToLocalPlay-Appendix) > |

# 複数バージョンのファイル切り替え
- customjsを多用している場合、バージョンが上がると動かず、元に戻したいときがあります。 
- あらかじめサブフォルダに一式を退避しておき、.htaccessの設定で参照先を切り替えることでファイルの切り替えができます。

## 使い方
### フォルダ階層
```
[danoni]
  └ .htaccess
  └ [js]
  └ [css]
  └ [skin]
  └ [img]
  └ [music]
  └ [work]  - 作品本体の格納先
  └ [work2]
  └ [old]   - 過去バージョンのjsファイルなど一式
    └ [v37.3.1]  - バージョン名（一例）
      └ [js]
      └ [css]
      └ [skin]
      └ [img]
```

### .htaccess
- ダンおにのルートフォルダに「.htaccess」というファイルを作成して、下記を記述します。
- このファイルを格納すると、HTMLファイルを書き換えることなく、old/v37.3.1/配下にいるデータを見に行くようになります（musicフォルダを除く）。
```perl
RewriteEngine on
RewriteRule js/(.*)$ old/v37.3.1/js/$1 [L]
RewriteRule css/(.*)$ old/v37.3.1/css/$1 [L]
RewriteRule skin/(.*)$ old/v37.3.1/skin/$1 [L]
RewriteRule img/(.*)$ old/v37.3.1/img/$1 [L]
```
- 元に戻したいときは、行の先頭を`#`でコメント化すれば元に戻ります。
```perl
RewriteEngine on
#RewriteRule js/(.*)$ old/v37.3.1/js/$1 [L]
#RewriteRule css/(.*)$ old/v37.3.1/css/$1 [L]
#RewriteRule skin/(.*)$ old/v37.3.1/skin/$1 [L]
#RewriteRule img/(.*)$ old/v37.3.1/img/$1 [L]
```
- work2フォルダだけルール適用したい場合は、work2フォルダに以下の.htaccessを配置します。
- この場合、danoniフォルダには.htaccessを配置しないほうが良いです（ルールが重複するため）。
```perl
RewriteEngine on
RewriteBase /danoni/work2/
RewriteRule ../js/(.*)$ ../old/v37.3.1/js/$1 [L]
RewriteRule ../css/(.*)$ ../old/v37.3.1/css/$1 [L]
RewriteRule ../skin/(.*)$ ../old/v37.3.1/skin/$1 [L]
RewriteRule ../img/(.*)$ ../old/v37.3.1/img/$1 [L]
```

## ページ作成者
- ティックル

[^ Tips Indexに戻る](./tips-index)

| < [WordPressを使って作品を公開](./tips-0008-wordpress) | **複数バージョンのファイル切り替え** | [ローカルでのプレイ方法(補足)](./HowToLocalPlay-Appendix) > |
