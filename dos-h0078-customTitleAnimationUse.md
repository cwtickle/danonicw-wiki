**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-h0078-customTitleAnimationUse) | Japanese** 

[Home](./) &gt; [譜面ヘッダー仕様](dos_header) &gt; [デフォルトデザインの利用有無
](dos_header#-デフォルトデザインの利用有無)

| [<- customTitleArrowUse](dos-h0026-customTitleArrowUse) | **customTitleAnimationUse** | [customBackUse ->](dos-h0027-customBackUse) |

## customTitleAnimationUse
- タイトルのアニメーション設定の利用有無
- 共通設定 ⇒ [g_presetObj.customDesignUse](dos-s0005-defaultDesign#デフォルトデザインの使用有無設定-g_presetobjcustomdesignuse)

### 使い方
```
|customTitleAnimationUse=false|
```
### 説明
タイトル文字のアニメーション用譜面ヘッダー`titleAnimation`, `titleAnimationClass`を  
利用するかどうかを設定します。  

|値|既定|内容|
|----|----|----|
|false|*|アニメーション用譜面ヘッダーを利用|
|true||個別設定（直接CSSにて指定）|

### 関連項目
- [titleAnimation](dos-h0077-titleanimation) [:pencil:](dos-h0077-titleanimation/_edit) タイトル文字のアニメーション設定
- [titleAnimationClass](dos-h0079-titleanimationclass) [:pencil:](dos-h0079-titleanimationclass/_edit) タイトル文字のアニメーションクラス設定

### 更新履歴

|Version|変更内容|
|----|----|
|[v18.5.0](https://github.com/cwtickle/danoniplus/releases/tag/v18.5.0)|・初回実装|

| [<- customTitleArrowUse](dos-h0026-customTitleArrowUse) | **customTitleAnimationUse** | [customBackUse ->](dos-h0027-customBackUse) |