**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-h0054-skinType) | Japanese** 

[Home](./) &gt; [譜面ヘッダー仕様](dos_header) &gt; [カスタムデータの取込](dos_header#-カスタムデータの取込)

| [<- autoPreload](dos-h0055-autoPreload) | **skinType** | [bgCanvasUse ->](dos-h0094-bgCanvasUse) |

## skinType
- 作品に適用するスキンの設定
- 共通設定 ⇒ [g_presetObj.skinType](dos-s0002-customFile#デフォルトスキン-g_presetobjskintype)

### 使い方
```
|skinType=skyblue|
|skinType=skyblue,background|
|skinType=skyblue,(..)background|
|skinType=(..)skin/|
```
### 説明
適用するスキン名を指定します。カンマ区切りで、最大2つまで指定が可能です。  
未指定の場合、`default`スキンが適用されます。  
1つ目は共通スキン、2つ目は作品個別スキンのような使い方が可能です。

ver25.3.0以降は最大2つの制限が無くなり、3つ以上も指定できるようになりました。

### 補足  
両方で同じスタイルが指定された場合は、以下の優先度で適用されます。  
なお、`!important`指定した場合はこの限りではありません。

2つ目のスキン ＞ 1つ目のスキン ＞ ページ直指定 ＞ danoni_main.css

### 関連項目
- [**setColor**](dos-h0003-setColor) [:pencil:](dos-h0003-setColor/_edit) 矢印色
- [**frzColor**](dos-h0004-frzColor) [:pencil:](dos-h0004-frzColor/_edit) フリーズアロー色
- [setShadowColor](dos-h0041-setShadowColor) [:pencil:](dos-h0041-setShadowColor/_edit) 矢印の内側を塗りつぶす設定および色の設定
- [**customJs**](dos-h0019-customjs) [:pencil:](dos-h0019-customjs/_edit) カスタムjsファイルの指定
- [customCss](dos-h0086-customcss) [:pencil:](dos-h0086-customcss/_edit) カスタムcssファイルの指定
- [**settingType**](dos-h0056-settingType) [:pencil:](dos-h0056-settingType/_edit) 共通設定名

### 更新履歴

|Version|変更内容|
|----|----|
|[v25.3.0](https://github.com/cwtickle/danoniplus/releases/tag/v25.3.0)|・最大2ファイルの制限を撤廃|
|[v19.3.0](https://github.com/cwtickle/danoniplus/releases/tag/v19.3.0)|・カレント＋サブディレクトリ指定に対応|
|[v10.0.0](https://github.com/cwtickle/danoniplus/releases/tag/v10.0.0)|・初回実装|

| [<- autoPreload](dos-h0055-autoPreload) | **skinType** | [bgCanvasUse ->](dos-h0094-bgCanvasUse) |