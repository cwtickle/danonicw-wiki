**[English](https://github.com/cwtickle/danoniplus-docs/wiki/Changelog-v26) | Japanese** 

[^ Back to All Version List (Security Policy)](https://github.com/cwtickle/danoniplus/security/policy)  
[^ Back to Update Info](./UpdateInfo#-v26-changelog)

[**<- v27**](Changelog-v27) | **v26** | [**v25 ->**](Changelog-v25)  
(**🔖 [17 Releases](https://github.com/cwtickle/danoniplus/releases?q=tag%3Av26)** )

## 🔃 Files changed (v26)

|Directory|FileName||Last Updated|
|----|----|----|----|
|/js|danoni_main.js|[📥](https://github.com/cwtickle/danoniplus/releases/download/v26.7.6/danoni_main.js)|**v26.7.6**|
|/js/lib|danoni_constants.js|[📥](https://github.com/cwtickle/danoniplus/releases/download/v26.7.2/danoni_constants.js)|[v26.7.2](Changelog-v26#v2672-2022-04-16)|

<details>
<summary>Changed file list before v25</summary>

|Directory|FileName||Last Updated|
|----|----|----|----|
|/js/lib|danoni_localbinary.js|[📥](https://github.com/cwtickle/danoniplus/releases/download/v15.1.0/danoni_localbinary.js)|[v15.1.0](./Changelog-v15#v1510-2020-05-21)|
|/css|danoni_main.css|[📥](https://github.com/cwtickle/danoniplus/releases/download/v21.4.2/danoni_main.css)|[v21.4.2](https://github.com/cwtickle/danoniplus/releases/tag/v21.4.2)|
|/img|aaShadow.svg<br>arrow.svg<br>arrowShadow.svg<br>borderline.svg<br>c.svg<br>cursor.svg<br>giko.svg<br>iyo.svg<br>monar.svg<br>morara.svg<br>onigiri.svg|[📥](https://github.com/cwtickle/danoniplus/releases/download/v15.1.0/img.zip)|[v15.1.0](./Changelog-v15#v1510-2020-05-21)|
|/skin|danoni_skin_default.css<br>danoni_skin_light.css<br>danoni_skin_skyblue.css|[📥](https://github.com/cwtickle/danoniplus/releases/download/v21.4.2/danoni_skin_default.css)<br>[📥](https://github.com/cwtickle/danoniplus/releases/download/v21.4.2/danoni_skin_light.css)<br>[📥](https://github.com/cwtickle/danoniplus/releases/download/v21.4.2/danoni_skin_skyblue.css)|[v21.4.2](Changelog-v21#v2142-2021-04-07)|

</details>

⭐ New Features / 🛠️ Improvements / 🐞 Bug Fixes  
( Files changed: ⚪ danoni_main.js / 🔵 danoni_constants.js / 🔴 css files )

## [Warning] 更新終了後の不具合情報
- [更新を終了したバージョンの不具合情報#v26](DeprecatedVersionBugs#v26) を参照

## v26.7.6 ([2022-10-31](https://github.com/cwtickle/danoniplus/releases/tag/v26.7.6))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v26.7.6/total)](https://github.com/cwtickle/danoniplus/archive/v26.7.6.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v26.7.6/support/v26?style=social)](https://github.com/cwtickle/danoniplus/compare/v26.7.6...support/v26#files_bucket) 
- 🐞 **C)** 譜面データの値が極端に小さい場合、baseFrameの値がずれてしまう問題を修正 ( PR [#1348](https://github.com/cwtickle/danoniplus/pull/1348) ) <- :boom: [**v25.3.2**](Changelog-v25#v2532-2022-01-18)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v26.7.6)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v26.7.5...v26.7.6#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v26.7.6)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v26.7.6.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v26.7.6.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v26.7.6/js/danoni_main.js)
/ 🎣 [**v28.6.0**](./Changelog-v28#v2860-2022-10-31)

## v26.7.5 ([2022-10-29](https://github.com/cwtickle/danoniplus/releases/tag/v26.7.5))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v26.7.5/total)](https://github.com/cwtickle/danoniplus/archive/v26.7.5.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v26.7.5/support/v26?style=social)](https://github.com/cwtickle/danoniplus/compare/v26.7.5...support/v26#files_bucket) 
- 🐞 **C)** フリーズアローを強制削除した場合の判定関数の引数誤りを修正 ( PR [#1343](https://github.com/cwtickle/danoniplus/pull/1343) ) <- :boom: [**v5.12.1**](Changelog-v5#v5121-2019-06-17)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v26.7.5)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v26.7.4...v26.7.5#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v26.7.5)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v26.7.5.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v26.7.5.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v26.7.5/js/danoni_main.js)
/ 🎣 [**v28.5.0**](./Changelog-v28#v2850-2022-10-29)

## v26.7.4 ([2022-08-03](https://github.com/cwtickle/danoniplus/releases/tag/v26.7.4))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v26.7.4/total)](https://github.com/cwtickle/danoniplus/archive/v26.7.4.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v26.7.4/support/v26?style=social)](https://github.com/cwtickle/danoniplus/compare/v26.7.4...support/v26#files_bucket) 
- 🐞 **C)** 重複するID名の解消 ( PR [#1300](https://github.com/cwtickle/danoniplus/pull/1300) ) <- :boom: [**v11.4.0**](Changelog-v11#v1140-2020-02-05)
- 🐞 **C)** 譜面明細画面でID名に英数以外が入りうる問題の修正 ( PR [#1300](https://github.com/cwtickle/danoniplus/pull/1300) ) <- :boom: [**v19.3.0**](Changelog-v19#v1930-2021-01-30)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v26.7.4)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v26.7.3...v26.7.4#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v26.7.4)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v26.7.4.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v26.7.4.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v26.7.4/js/danoni_main.js)
/ 🎣 [**v27.8.1**](./Changelog-v27#v2781-2022-08-01)

## v26.7.3 ([2022-05-22](https://github.com/cwtickle/danoniplus/releases/tag/v26.7.3))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v26.7.3/total)](https://github.com/cwtickle/danoniplus/archive/v26.7.3.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v26.7.3/support/v26?style=social)](https://github.com/cwtickle/danoniplus/compare/v26.7.3...support/v26#files_bucket) 
- 🐞 **B)** フォルダ名に半角スペースや全角文字が含まれている場合に起動できないことがある問題を修正 ( PR [#1289](https://github.com/cwtickle/danoniplus/pull/1289) ) <- :boom: [**v26.7.1**](Changelog-v26#v2671-2022-04-10)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v26.7.3)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v26.7.2...v26.7.3#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v26.7.3)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v26.7.3.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v26.7.3.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v26.7.3/js/danoni_main.js)
/ 🎣 [**v27.5.1**](./Changelog-v27#v2751-2022-05-22)

## v26.7.2 ([2022-04-16](https://github.com/cwtickle/danoniplus/releases/tag/v26.7.2))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v26.7.2/total)](https://github.com/cwtickle/danoniplus/archive/v26.7.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v26.7.2/support/v26?style=social)](https://github.com/cwtickle/danoniplus/compare/v26.7.2...support/v26#files_bucket) 
- 🛠️ 11ikeyの別パターンのアシスト設定に対応 ( PR [#1282](https://github.com/cwtickle/danoniplus/pull/1282) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v26.7.2)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v26.7.1...v26.7.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v26.7.2)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v26.7.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v26.7.2.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v26.7.2/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v26.7.2/js/lib/danoni_constants.js)
/ 🎣 [**v27.4.0**](./Changelog-v27#v2740-2022-04-16)

## v26.7.1 ([2022-04-10](https://github.com/cwtickle/danoniplus/releases/tag/v26.7.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v26.7.1/total)](https://github.com/cwtickle/danoniplus/archive/v26.7.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v26.7.1/support/v26?style=social)](https://github.com/cwtickle/danoniplus/compare/v26.7.1...support/v26#files_bucket) 
- 🐞 **C)** カレントパス、譜面ファイル取得部分をエスケープするよう修正 ( PR [#1277](https://github.com/cwtickle/danoniplus/pull/1277) ) <- :boom: [**v19.4.1**](Changelog-v19#v1941-2021-02-06)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v26.7.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v26.7.0...v26.7.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v26.7.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v26.7.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v26.7.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v26.7.1/js/danoni_main.js)
/ 🎣 [**v27.3.1**](./Changelog-v27#v2731-2022-04-10)

----

## v26.7.0 ([2022-03-12](https://github.com/cwtickle/danoniplus/releases/tag/v26.7.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v26.7.0/total)](https://github.com/cwtickle/danoniplus/archive/v26.7.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v26.7.0/support/v26?style=social)](https://github.com/cwtickle/danoniplus/compare/v26.7.0...support/v26#files_bucket) 
- 🛠️ ローカル時、リザルトコピー内容にURL情報を付加しないよう変更 ( Issue [#1253](https://github.com/cwtickle/danoniplus/pull/1253), PR [#1254](https://github.com/cwtickle/danoniplus/pull/1254) )
- 🛠️ 文字列置き換え周りのコードを整理 ( PR [#1255](https://github.com/cwtickle/danoniplus/pull/1255) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v26.7.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v26.6.1...v26.7.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v26.7.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v26.7.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v26.7.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v26.7.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v26.7.0/js/lib/danoni_constants.js) 

## v26.6.1 ([2022-03-06](https://github.com/cwtickle/danoniplus/releases/tag/v26.6.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v26.6.1/total)](https://github.com/cwtickle/danoniplus/archive/v26.6.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v26.6.1/support/v26?style=social)](https://github.com/cwtickle/danoniplus/compare/v26.6.1...support/v26#files_bucket) 
- 🐞**A)** マターリを出すとキーが利かなくなる問題を修正 ( PR [#1251](https://github.com/cwtickle/danoniplus/pull/1251) ) <- :boom: [**v26.6.0**](Changelog-v26#v2660-2022-03-05)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v26.6.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v26.6.0...v26.6.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v26.6.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v26.6.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v26.6.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v26.6.1/js/danoni_main.js)<br>❤️ ふみのり


## v26.6.0 ([2022-03-05](https://github.com/cwtickle/danoniplus/releases/tag/v26.6.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v26.6.0/total)](https://github.com/cwtickle/danoniplus/archive/v26.6.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v26.6.0/support/v26?style=social)](https://github.com/cwtickle/danoniplus/compare/v26.6.0...support/v26#files_bucket) 
- 🛠️ 通常の関数をアロー関数へ置き換え ( PR [#1249](https://github.com/cwtickle/danoniplus/pull/1249) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v26.6.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v26.5.0...v26.6.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v26.6.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v26.6.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v26.6.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v26.6.0/js/danoni_main.js)<br>❤️ goe (@goe0)

## v26.5.0 ([2022-02-27](https://github.com/cwtickle/danoniplus/releases/tag/v26.5.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v26.5.0/total)](https://github.com/cwtickle/danoniplus/archive/v26.5.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v26.5.0/support/v26?style=social)](https://github.com/cwtickle/danoniplus/compare/v26.5.0...support/v26#files_bucket) 
- ⭐ カスタムキー定義のpos項目について小数点に対応 ( PR [#1244](https://github.com/cwtickle/danoniplus/pull/1244) )
- 🛠️ 関数の表示順序を見直し ( PR [#1246](https://github.com/cwtickle/danoniplus/pull/1246), [#1247](https://github.com/cwtickle/danoniplus/pull/1247) )
- 🐞**C)** 譜面密度グラフで同時押しのカウント誤りを修正 ( PR [#1245](https://github.com/cwtickle/danoniplus/pull/1245) ) <- :boom: [**v26.1.0**](Changelog-v26#v2610-2022-02-05)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v26.5.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v26.4.0...v26.5.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v26.5.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v26.5.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v26.5.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v26.5.0/js/danoni_main.js)<br>❤️ Tarwil, goe (@goe0)

## v26.4.0 ([2022-02-25](https://github.com/cwtickle/danoniplus/releases/tag/v26.4.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v26.4.0/total)](https://github.com/cwtickle/danoniplus/archive/v26.4.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v26.4.0/support/v26?style=social)](https://github.com/cwtickle/danoniplus/compare/v26.4.0...support/v26#files_bucket) 
- 🛠️ 譜面ファイル読込部分のコード見直し ( PR [#1240](https://github.com/cwtickle/danoniplus/pull/1240), [#1241](https://github.com/cwtickle/danoniplus/pull/1241), [#1242](https://github.com/cwtickle/danoniplus/pull/1242) )
- 🛠️ 必要以上に同じ譜面ファイルを読み込んでいる問題を改善 ( PR [#1241](https://github.com/cwtickle/danoniplus/pull/1241) )
- 🛠️ 外部ファイル名末尾のランダム値を一部固定化 ( PR [#1242](https://github.com/cwtickle/danoniplus/pull/1242) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v26.4.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v26.3.1...v26.4.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v26.4.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v26.4.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v26.4.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v26.4.0/js/danoni_main.js)

## v26.3.1 ([2022-02-23](https://github.com/cwtickle/danoniplus/releases/tag/v26.3.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v26.3.1/total)](https://github.com/cwtickle/danoniplus/archive/v26.3.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v26.3.1/support/v26?style=social)](https://github.com/cwtickle/danoniplus/compare/v26.3.1...support/v26#files_bucket) 
- ⭐ danoni_setting.js 上の変数群を g_presetObj へ移行 ( PR [#1235](https://github.com/cwtickle/danoniplus/pull/1235) )
- ⭐ customjsの読込位置を headerConvert関数の前に変更 ( PR [#1236](https://github.com/cwtickle/danoniplus/pull/1236) )
- 🛠️ 従来の danoni_setting.js の変数の互換用関数を作成 ( PR [#1235](https://github.com/cwtickle/danoniplus/pull/1235) )
- 🛠️ ファイル読込部分のコード見直し ( PR [#1236](https://github.com/cwtickle/danoniplus/pull/1236), [#1238](https://github.com/cwtickle/danoniplus/pull/1238) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v26.3.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v26.2.0...v26.3.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v26.3.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v26.3.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v26.3.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v26.3.1/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v26.3.1/js/lib/danoni_constants.js) 


## v26.2.0 ([2022-02-20](https://github.com/cwtickle/danoniplus/releases/tag/v26.2.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v26.2.0/total)](https://github.com/cwtickle/danoniplus/archive/v26.2.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v26.2.0/support/v26?style=social)](https://github.com/cwtickle/danoniplus/compare/v26.2.0...support/v26#files_bucket) 
- ⭐ danoni_setting.js 上で共通のカスタムキー定義ができるように対応 ( PR [#1231](https://github.com/cwtickle/danoniplus/pull/1231) )
- ⭐ 作品別のカスタムキー定義について、keyExtraList指定を任意化 ( PR [#1231](https://github.com/cwtickle/danoniplus/pull/1231), [#1232](https://github.com/cwtickle/danoniplus/pull/1232) )
- 🐞**C)** g_lblRenames未定義時に各種名称の上書きができない問題を修正 ( PR [#1230](https://github.com/cwtickle/danoniplus/pull/1230) ) <- :boom: [**v20.3.1**](Changelog-v20#v2031-2021-02-24)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v26.2.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v26.1.2...v26.2.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v26.2.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v26.2.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v26.2.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v26.2.0/js/danoni_main.js)


## v26.1.2 ([2022-02-16](https://github.com/cwtickle/danoniplus/releases/tag/v26.1.2))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v26.1.2/total)](https://github.com/cwtickle/danoniplus/archive/v26.1.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v26.1.2/support/v26?style=social)](https://github.com/cwtickle/danoniplus/compare/v26.1.2...support/v26#files_bucket) 
- 🛠️ npm環境用にディレクトリ構成を見直し ( PR [#1228](https://github.com/cwtickle/danoniplus/pull/1228) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v26.1.2)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v26.1.1...v26.1.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v26.1.2)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v26.1.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v26.1.2.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v26.1.2/js/danoni_main.js)

## v26.1.1 ([2022-02-10](https://github.com/cwtickle/danoniplus/releases/tag/v26.1.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v26.1.1/total)](https://github.com/cwtickle/danoniplus/archive/v26.1.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v26.1.1/support/v26?style=social)](https://github.com/cwtickle/danoniplus/compare/v26.1.1...support/v26#files_bucket) 
- 🐞**C)** 推定Adjの計算式を修正 ( PR [#1226](https://github.com/cwtickle/danoniplus/pull/1226), Gitter [2022-02-09](https://gitter.im/danonicw/community?at=6203b7a1708e9c3dd76da0d5) ) <- :boom: [**v26.1.0**](Changelog-v26#v2610-2022-02-05)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v26.1.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v26.1.0...v26.1.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v26.1.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v26.1.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v26.1.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v26.1.1/js/danoni_main.js)<br>❤️ Tarwil

## v26.1.0 ([2022-02-05](https://github.com/cwtickle/danoniplus/releases/tag/v26.1.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v26.1.0/total)](https://github.com/cwtickle/danoniplus/archive/v26.1.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v26.1.0/support/v26?style=social)](https://github.com/cwtickle/danoniplus/compare/v26.1.0...support/v26#files_bucket) 
- ⭐ コメント文中に定義済み変数を埋め込む機能を追加 ( PR [#1222](https://github.com/cwtickle/danoniplus/pull/1222), Issue [#1195](https://github.com/cwtickle/danoniplus/pull/1195) )
- ⭐ 譜面密度グラフで二重押し/多重押し状況を表示するよう変更 ( PR [#1223](https://github.com/cwtickle/danoniplus/pull/1223) )
- ⭐ 推定Adjの計算方法を単純平均から誤差が正規分布になると仮定した場合の推定値になるよう変更 ( PR [#1224](https://github.com/cwtickle/danoniplus/pull/1224) )
- 🛠️ コメントエリアを選択できるように変更、ボタンサイズ調整 ( PR [#1221](https://github.com/cwtickle/danoniplus/pull/1221) )
- 🐞**C)** メイン画面のスキン拡張が利いていない問題を修正 ( PR [#1220](https://github.com/cwtickle/danoniplus/pull/1220) ) <- :boom: [**v25.3.0**](Changelog-v25#v2530-2022-01-16)

<img src="https://user-images.githubusercontent.com/44026291/152635989-119a6537-f35d-49de-9060-699c9ccb0083.png" width="45%"><img src="https://user-images.githubusercontent.com/44026291/152636016-6802b87d-5f15-4c8f-8cfc-22f943f9d313.png" width="45%">

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v26.1.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v26.0.0...v26.1.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v26.1.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v26.1.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v26.1.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v26.1.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v26.1.0/js/lib/danoni_constants.js)<br>❤️ Tarwil, izkdic

## v26.0.0 ([2022-01-30](https://github.com/cwtickle/danoniplus/releases/tag/v26.0.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v26.0.0/total)](https://github.com/cwtickle/danoniplus/archive/v26.0.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v26.0.0/support/v26?style=social)](https://github.com/cwtickle/danoniplus/compare/v26.0.0...support/v26#files_bucket) 
- ⭐ ダミー矢印・フリーズアローに対して色変化・モーションデータを実装 ( PR [#1218](https://github.com/cwtickle/danoniplus/pull/1218) )
- 🐞**B)** 全体色変化かつフリーズヒット時、AAの色が変わらない問題を修正 ( PR [#1218](https://github.com/cwtickle/danoniplus/pull/1218) ) <- :boom: [**v25.1.0**](Changelog-v25#v2510-2022-01-07)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v26.0.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v25.5.2...v26.0.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v26.0.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v26.0.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v26.0.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v26.0.0/js/danoni_main.js)<br>❤️ eintritt. (decresc.)

[**<- v27**](Changelog-v27) | **v26** | [**v25 ->**](Changelog-v25)  
