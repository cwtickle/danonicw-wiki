**[English](https://github.com/cwtickle/danoniplus-docs/wiki/AboutColorObject) | Japanese** 

| [< カスタム関数の定義](./AboutCustomFunction) | **色付きオブジェクト仕様** | [ローカルストレージ仕様 >](./LocalStorage) |

# 色付きオブジェクト仕様
- 色付きオブジェクトとは、Dancing☆Onigiri (CW Edition)の表示で使用している  
基本的な画像群のことです。通常、これに色を付けて表示します。  
<img src="./wiki/colorobject1.png" width="40%">

- 色付きオブジェクトの生成仕様については[`createColorObject2`関数](fnc-c0003-createColorObject2)の項目を参照ください。

## 色付きオブジェクト群の入替方法
- 色付きオブジェクト群は、現状を維持したまま入れ替えることが可能です。  
[img]フォルダ配下に任意の名前のサブフォルダを作成し、その中に一式を入れます。  
<img src="./wiki/colorobject2.png" width="40%">

- サブフォルダ内には、以下のファイル全てが揃っている必要があります。  
また、拡張子は全てのファイルで揃える必要があります。  
画像ファイルは、[img]フォルダ直下のものを一部流用しても問題ありません。

|ファイル名|内容|
|----|----|
|cursor.svg(png)|キーコンフィグのカーソル|
|frzbar.svg(png)|フリーズアローバー、ライフゲージ本体|
|borderline.svg(png)|ノルマ制ゲージのとき、ボーダー位置を表示する画像。|
|arrow.svg(png)|矢印本体|
|arrowShadow.svg(png)|矢印の裏側描画用。フリーズアローで使う。|
|c.svg(png)|AA(しぃ)|
|giko.svg(png)|AA(ギコ)|
|iyo.svg(png)|AA(ぃょぅ)|
|monar.svg(png)|AA(モナー)|
|morara.svg(png)|AA(モララー)|
|onigiri.svg(png)|AA(おにぎり)|
|aaShadow.svg(png)|AAキャラクタの裏側描画用。フリーズアローで使う。|

- ファイル一式を揃えた後、[danoni_setting.js](./dos-s0005-defaultDesign#デフォルト画像セットの設定-g_presetobjimagesets) (作品共通) もしくは [imgType](dos-h0082-imgType) (作品個別) にて  
上記サブフォルダの画像を使用するよう、設定を変更します。

### 譜面ヘッダーで設定する場合
```
|imgType=classic,png|  // サブフォルダ名を1番目に、拡張子を2番目に指定
```

### danoni_setting.js で設定する場合
```javascript
g_presetObj.imageSets = [``, `classic,png`];
```

## 色付きオブジェクトの追加方法（通常）
- 色付きオブジェクトが追加できるのは、  
現状矢印・おにぎりなどステップゾーンで利用している画像のみです。

### 1. サーバ上の場合
- danoni_setting.js の g_presetObj.customImageListへ登録が必要です。  
ここでは、`ball`という名前の色付きオブジェクトを追加する例を示します。
```javascript
g_presetObj.customImageList = [`ball`];
```
- ここに例えば「ball」という名前の画像を追加する場合、  
次の名前の画像データを用意する必要があります。  
「img」フォルダ直下に置いてください。
[imgType](dos-h0082-imgType)を指定している場合は、imgTypeで指定したサブフォルダ配下にも置くようにしてください。

|画像名|用途|
|----|----|
|ball.svg(png)|ステップゾーン、単発ノート|
|ball**Shadow**.svg(png)|ステップゾーン、単発ノートの影部分|
|ball**StepHit**.svg(png)|ステップゾーンヒット時のオブジェクト|

- なお、拡張子については danoni_setting.js の `g_presetObj.overrideExtension`の値に従います。  
未指定の場合は、`svg`形式の画像を用意する必要があります。

### 2. ローカル・ファイル直接起動の場合
- ローカル（ファイル直接起動）の場合、1の方法では画像が表示されません。
どうしてもファイル直接起動時でも画像を表示させたい場合は、
`danoni_localbinary.js`内の「loadBinary()」関数の追記が必要です。

#### svgファイルの挿入例（上記で`ball`を追加した場合）
- 作成したsvgファイルを右クリック→テキストエディターで開いたものをコピーして  
下記のコメント部分に挿入します。
```javascript
function loadBinary() {
    if (location.href.match(`^file`)) {
        // 省略
        C_IMG_LIFEBORDER = `data:image/svg+xml,${encodeURIComponent('<svg id="borderline" data-name="borderline" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 400 160"><path d="M400,160H80L0,80,80.8,0H400Z"/></svg>')}`;
        // ここに追加
        g_imgObj.ball = `data:image/svg+xml,${encodeURIComponent('/*ここにsvg本体コード（単発ノート用）を入れる*/')}`;
        g_imgObj.ballShadow = `data:image/svg+xml,${encodeURIComponent('/*ここにsvg本体コード（単発ノート影用）を入れる*/')}`;
        g_imgObj.ballStep = `data:image/svg+xml,${encodeURIComponent('/*ここにsvg本体コード（ステップゾーン用）を入れる*/')}`;
        g_imgObj.ballStepHit = `data:image/svg+xml,${encodeURIComponent('/*ここにsvg本体コード（ステップヒット時オブジェクト）を入れる*/')}`;
    }
}
```

#### pngファイルの挿入例（上記で`ball`を追加した場合）
- pngファイルをbase64変換したテキストをコピーして下記のコメント部分に挿入します。  
変換方法は、「[Base64エンコーダ](https://lab.syncer.jp/Tool/Base64-encode/)」など外部サイトより変換してください。
```javascript
function loadBinary() {
    if (location.href.match(`^file`)) {
        // 省略
        C_IMG_LIFEBORDER = `data:image/svg+xml,${encodeURIComponent('<svg id="borderline" data-name="borderline" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 400 160"><path d="M400,160H80L0,80,80.8,0H400Z"/></svg>')}`;
        // ここに追加
        g_imgObj.ball = `data:image/png;base64,/*ここにbase64変換したコード（単発ノート用）を入れる*/`;
        g_imgObj.ballShadow = `data:image/png;base64,/*ここにbase64変換したコード（単発ノート影用）を入れる*/`;
        g_imgObj.ballStep = `data:image/png;base64,/*ここにbase64変換したコード（ステップゾーン用）を入れる*/`;
        g_imgObj.ballStepHit = `data:image/png;base64,/*ここにbase64変換したコード（ステップヒット時オブジェクト）を入れる*/`;
    }
}
```

## 色付きオブジェクトの追加方法（カスタムJs利用）
- 背景・マスクに一時的に使用したいなどの理由で、色付きオブジェクトが必要な場合があります。  
この場合、customJs側で直接`g_imgObj`に対象画像のパスを設定することで使えるようになります。
- なおこの方法ではカスタムキーの用途には使えません。
- ローカルファイルでも使用できるようにしたい場合は、上記「2. ローカル・ファイル直接起動の場合」と同じ（エンコードデータを直接代入する）方法で指定が必要です。

#### customJsでのカスタム画像指定例
```javascript
function addImages(){
	g_imgObj.arrowSpecial = `../img/classic/arrow.png`;  // 対象の画像を直接指定
}
g_customJsObj.preTitle.push(addImages); // 挿入する場所はcustomJsのpreTitle（読込前）
```
#### 譜面側の定義（上で定義したarrowSpecialを指定）
```
|backtitle_data=
330,3,[c]arrowSpecial/#ff9999:yellow,,5,200,200,200,0,spinY,120
480,3
|
```

## 関連項目
- [背景・マスクモーション (back_data, mask_data)](./dos-e0004-animationData)

## 更新履歴

|Version|変更内容|
|----|----|
|[v23.0.0](https://github.com/cwtickle/danoniplus/releases/tag/v23.0.0)|・デフォルト画像セット(ImgType)を複数指定できるように変更|
|[v22.5.1](https://github.com/cwtickle/danoniplus/releases/tag/v22.5.1)|・初期実装|

| [< カスタム関数の定義](./AboutCustomFunction) | **色付きオブジェクト仕様** | [ローカルストレージ仕様 >](./LocalStorage) |
