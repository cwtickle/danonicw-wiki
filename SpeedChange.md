**[English](https://github.com/cwtickle/danoniplus-docs/wiki/SpeedChange) | Japanese** 

| [< オーディオ仕様](./Audio) | **速度変化・Motionオプション仕様** | [判定仕様 >](./AboutJudgment) |

# 速度変化・Motionオプション仕様<br> / Speed change &amp; Motions
- Dancing☆Onigiri (CW Edition)における矢印の速度変化は、大きく3種類あります。

|No|名前|適用方法|特徴|
|----|----|----|----|
|1|全体変速 (Overall)|[speed_data<br>speed_change](dos-e0001-speedData)|画面上にある矢印・フリーズアローの速度を即時に変更します。<br>速度をゼロにするとストップ、マイナス値にすると逆走します。|
|2|個別加速 (Boost)|[boost_data](dos-e0001-speedData)|指定したフレーム以降に判定処理を行う矢印・フリーズアロー以降より速度変化させます。<br>それ以前の矢印の速度はそのままです。<br>後からやってくる矢印が前の矢印を追い越すこともあることから、別名追い越し加速とも呼ばれます。|
|3|Motionオプション<br>(Boost/Hi-Boost/Brake)|Motion<br>オプション|矢印の軌道全体に加減速のモーションを掛けます。|

- 全体変速、個別加速状況については速度変化グラフにて確認することができます。
<img src="https://github.com/cwtickle/danoniplus/assets/44026291/dd43764c-c240-4a79-924c-9f370987d41f" width="50%">

## 1フレームで移動する計算式
### ver28.4.0以降
- (( Speedオプションの数値 )×( 1: 全体変速 )×( 2: 個別加速 ) + (3: Motionオプション(大きさ)) × (3: Motionオプション(向き))

### ver28.3.1以前
- ( Speedオプションの数値 )×( 1: 全体変速 + 3: Motionオプション )×( 2: 個別加速 )

## 譜面で指定するフレーム数と矢印・フリーズアローの生成フレーム数の関係
- フレーム毎に1フレームで進むピクセル数を内部的に保持しており、その合計が枠外～ステップゾーン間の距離に初めて到達したフレーム数だけ遡ります。
- ピクセル数が枠外～ステップゾーン間の距離を超える場合は、そのピクセル数だけずらして表示します。

<img src="https://github.com/cwtickle/danoniplus/assets/44026291/629bb907-66f7-423a-9f41-de0cd90a00d1" width="80%">
<img src="https://github.com/user-attachments/assets/26e23b79-dc32-42fb-851f-619672af444b" width="80%">

## 個別加速における描画開始位置の計算方法
- 矢印・フリーズアローは、生成した順序で判定を行います。  
- 個別加速により矢印が追い越されるようなケースであっても生成順序を揃えるため、  
個別加速では矢印・フリーズアローの移動距離を伸縮させることで実現しています。

<img src="./wiki/speedchange1.png" width="20%">

## Motionオプションにおける速度加算仕様
- Motionオプションを有効にした場合、その設定に応じて矢印の流れる速度に一律変則的な速度を加算します。  
内部的には`g_workObj.motionOnFrames`でフレーム毎の移動量を管理しています。
- Motion「Hi-Boost」を設定した場合、Speedオプションの設定により加算される速度幅が**設定した速度に比例して**変わります。

<img src="https://user-images.githubusercontent.com/44026291/198289791-b29df453-3d16-4f38-91e7-c619fad2e480.png" width="80%">

### Motionオプション別のY座標加算グラフ
- Boostは、Hi-Boost (1.5x)と同じ加算グラフになります。

<img src="https://github.com/user-attachments/assets/ae97ceb0-9e0d-40a8-837e-011821f34f82" width="70%">

<img src="https://github.com/user-attachments/assets/6f9aea59-8e05-42ae-a3d6-7f598a7f015d" width="70%">

<img src="https://github.com/user-attachments/assets/7ad4905c-009c-4e1e-8b2c-57afe24e5c42" width="70%">

## 更新履歴

|Version|変更内容|
|----|----|
|[v38.1.0](https://github.com/cwtickle/danoniplus/releases/tag/v38.1.0)|・Motionに「Compress」「Fountain」を追加|
|[v28.4.0](https://github.com/cwtickle/danoniplus/releases/tag/v28.4.0)|・Motionの軌道加算分の計算式を変更<br>・Motionに「Hi-Boost」を追加|
|[v23.1.1](https://github.com/cwtickle/danoniplus/releases/tag/v23.1.1)|・Motion「Boost」の軌道計算式を変更|
|[v8.0.0](https://github.com/cwtickle/danoniplus/releases/tag/v8.0.0)|・「speed_data」「speed_change」の区別を廃止|
|[v1.0.0](https://github.com/cwtickle/danoniplus/releases/tag/v1.0.0)|・初期実装|

| [< オーディオ仕様](./Audio) | **速度変化・Motionオプション仕様** | [判定仕様 >](./AboutJudgment) |
