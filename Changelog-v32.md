**[English](https://github.com/cwtickle/danoniplus-docs/wiki/Changelog-v32) | Japanese** 

[^ Back to All Version List (Security Policy)](https://github.com/cwtickle/danoniplus/security/policy)  
[^ Back to Update Info](./UpdateInfo#-v32-changelog)

[**<- v33**](./Changelog-v33) | **v32** | [**v31 ->**](./Changelog-v31)  
(**🔖 [18 Releases](https://github.com/cwtickle/danoniplus/releases?q=tag%3Av32)** )

## 🔃 Files changed (v32)

|Directory|FileName||Last Updated|
|----|----|----|----|
|/js|danoni_main.js|[📥](https://github.com/cwtickle/danoniplus/releases/download/v32.7.7/danoni_main.js)|**v32.7.7**|
|/js/lib|danoni_constants.js|[📥](https://github.com/cwtickle/danoniplus/releases/download/v32.6.1/danoni_constants.js)|[v32.6.1](./Changelog-v32#v3261-2023-07-08)|
|/css|danoni_main.css|[📥](https://github.com/cwtickle/danoniplus/releases/download/v32.6.0/danoni_main.css)|[v32.6.0](./Changelog-v32#v3260-2023-06-30)|
|/skin|danoni_skin_default.css<br>danoni_skin_light.css<br>danoni_skin_skyblue.css|[📥](https://github.com/cwtickle/danoniplus/releases/download/v32.5.0/skin_css.zip)|[v32.5.0](./Changelog-v32#v3250-2023-06-24)|

<details>
<summary>Changed file list before v31</summary>

|Directory|FileName||Last Updated|
|----|----|----|----|
|/js/lib|danoni_localbinary.js|[📥](https://github.com/cwtickle/danoniplus/releases/download/v15.1.0/danoni_localbinary.js)|[v15.1.0](./Changelog-v15#v1510-2020-05-21)|
|/img|aaShadow.svg<br>arrow.svg<br>arrowShadow.svg<br>borderline.svg<br>c.svg<br>cursor.svg<br>giko.svg<br>iyo.svg<br>monar.svg<br>morara.svg<br>onigiri.svg|[📥](https://github.com/cwtickle/danoniplus/releases/download/v15.1.0/img.zip)|[v15.1.0](./Changelog-v15#v1510-2020-05-21)|

</details>

⭐ New Features / 🛠️ Improvements / 🐞 Bug Fixes  
( Files changed: ⚪ danoni_main.js / 🔵 danoni_constants.js / 🔴 css files )

## [Warning] 更新終了後の不具合情報
- [更新を終了したバージョンの不具合情報#v32](./DeprecatedVersionBugs#v32) を参照

## v32.7.7 ([2023-11-05](https://github.com/cwtickle/danoniplus/releases/tag/v32.7.7))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v32.7.7/total)](https://github.com/cwtickle/danoniplus/archive/v32.7.7.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v32.7.7/support/v32?style=social)](https://github.com/cwtickle/danoniplus/compare/v32.7.7...support/v32#files_bucket)
- 🛠️ 右シフトキーが環境により動作しない問題を改善 ( PR [#1593](https://github.com/cwtickle/danoniplus/pull/1593) ) <- :boom: [**v16.0.0**](./Changelog-v16#v1600-2020-08-06)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v32.7.7)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v32.7.6...v32.7.7#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v32.7.7)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v32.7.7.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v32.7.7.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v32.7.7/js/danoni_main.js)
/ 🎣 [**v34.5.1**](./Changelog-v34#v3451-2023-11-05)

## v32.7.6 ([2023-10-21](https://github.com/cwtickle/danoniplus/releases/tag/v32.7.6))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v32.7.6/total)](https://github.com/cwtickle/danoniplus/archive/v32.7.6.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v32.7.6/support/v32?style=social)](https://github.com/cwtickle/danoniplus/compare/v32.7.6...support/v32#files_bucket)
- 🐞 **B)** HitPosition設定が機能していない問題を修正 ( PR [#1579](https://github.com/cwtickle/danoniplus/pull/1579) ) <- :boom: [**v31.0.0**](./Changelog-v31#v3100-2023-03-20)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v32.7.6)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v32.7.5...v32.7.6#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v32.7.6)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v32.7.6.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v32.7.6.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v32.7.6/js/danoni_main.js)
/ 🎣 [**v34.3.2**](./Changelog-v34#v3432-2023-10-21)

## v32.7.5 ([2023-10-13](https://github.com/cwtickle/danoniplus/releases/tag/v32.7.5))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v32.7.5/total)](https://github.com/cwtickle/danoniplus/archive/v32.7.5.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v32.7.5/support/v32?style=social)](https://github.com/cwtickle/danoniplus/compare/v32.7.5...support/v32#files_bucket)
- 🐞 **B)** difDataで指定したノルマが反映されない問題を修正 ( PR [#1575](https://github.com/cwtickle/danoniplus/pull/1575), Issue [#1574](https://github.com/cwtickle/danoniplus/pull/1574) ) <- :boom: [**v32.6.0**](./Changelog-v32#v3260-2023-06-30)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v32.7.5)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v32.7.4...v32.7.5#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v32.7.5)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v32.7.5.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v32.7.5.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v32.7.5/js/danoni_main.js)
/ 🎣 [**v34.3.0**](./Changelog-v34#v3430-2023-10-13)

## v32.7.4 ([2023-09-02](https://github.com/cwtickle/danoniplus/releases/tag/v32.7.4))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v32.7.4/total)](https://github.com/cwtickle/danoniplus/archive/v32.7.4.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v32.7.4/support/v32?style=social)](https://github.com/cwtickle/danoniplus/compare/v32.7.4...support/v32#files_bucket)
- 🐞 **C)** エラー時に表示される音楽ファイルの参照先URLが実際と異なる問題を修正 ( PR [#1540](https://github.com/cwtickle/danoniplus/pull/1540) ) <- :boom: [**v29.4.1**](./Changelog-v29#v2941-2023-01-28)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v32.7.4)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v32.7.3...v32.7.4#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v32.7.4)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v32.7.4.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v32.7.4.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v32.7.4/js/danoni_main.js)
/ 🎣 [**v33.3.0**](./Changelog-v33#v3330-2023-08-22)

## v32.7.3 ([2023-08-20](https://github.com/cwtickle/danoniplus/releases/tag/v32.7.3))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v32.7.3/total)](https://github.com/cwtickle/danoniplus/archive/v32.7.3.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v32.7.3/support/v32?style=social)](https://github.com/cwtickle/danoniplus/compare/v32.7.3...support/v32#files_bucket)
- 🐞 **C)** stockForceDel使用中に除外する検索対象が未定義の場合、エラーになる問題を修正 ( PR [#1536](https://github.com/cwtickle/danoniplus/pull/1536) ) <- :boom: [**v25.0.0**](./Changelog-v25#v2500-2022-01-04)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v32.7.3)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v32.7.2...v32.7.3#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v32.7.3)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v32.7.3.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v32.7.3.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v32.7.3/js/danoni_main.js)
/ 🎣 [**v33.2.0**](./Changelog-v33#v3320-2023-08-20)

## v32.7.2 ([2023-08-19](https://github.com/cwtickle/danoniplus/releases/tag/v32.7.2))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v32.7.2/total)](https://github.com/cwtickle/danoniplus/archive/v32.7.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v32.7.2/support/v32?style=social)](https://github.com/cwtickle/danoniplus/compare/v32.7.2...support/v32#files_bucket)
- 🐞 **B)** キー変化でadjustmentやplaybackRateが反映されない問題を修正 ( PR [#1534](https://github.com/cwtickle/danoniplus/pull/1534) ) <- :boom: [**v30.0.1**](./Changelog-v30#v3001-2023-02-11)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v32.7.2)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v32.7.1...v32.7.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v32.7.2)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v32.7.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v32.7.2.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v32.7.2/js/danoni_main.js)
/ 🎣 [**v33.1.4**](./Changelog-v33#v3314-2023-08-19)

## v32.7.1 ([2023-08-14](https://github.com/cwtickle/danoniplus/releases/tag/v32.7.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v32.7.1/total)](https://github.com/cwtickle/danoniplus/archive/v32.7.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v32.7.1/support/v32?style=social)](https://github.com/cwtickle/danoniplus/compare/v32.7.1...support/v32#files_bucket)
- 🐞 **B)** フリーズアローの終点と次のフリーズアローの始点が近い場合の判定不具合を修正 ( PR [#1530](https://github.com/cwtickle/danoniplus/pull/1530), Issue [#1529](https://github.com/cwtickle/danoniplus/pull/1529) ) <- :boom: [**v29.0.1**](./Changelog-v29#v2901-2022-11-05)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v32.7.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v32.7.0...v32.7.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v32.7.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v32.7.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v32.7.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v32.7.1/js/danoni_main.js)
/ 🎣 [**v33.1.2**](./Changelog-v33#v3312-2023-08-14)

---

## v32.7.0 ([2023-07-17](https://github.com/cwtickle/danoniplus/releases/tag/v32.7.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v32.7.0/total)](https://github.com/cwtickle/danoniplus/archive/v32.7.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v32.7.0/support/v32?style=social)](https://github.com/cwtickle/danoniplus/compare/v32.7.0...support/v32#files_bucket)
- ⭐ タイトル表示系、カスタム関数設定の多言語化対応 ( PR [#1517](https://github.com/cwtickle/danoniplus/pull/1517) )
- 🛠️ コードの整理 ( PR [#1516](https://github.com/cwtickle/danoniplus/pull/1516) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v32.7.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v32.6.1...v32.7.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v32.7.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v32.7.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v32.7.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v32.7.0/js/danoni_main.js)

## v32.6.1 ([2023-07-08](https://github.com/cwtickle/danoniplus/releases/tag/v32.6.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v32.6.1/total)](https://github.com/cwtickle/danoniplus/archive/v32.6.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v32.6.1/support/v32?style=social)](https://github.com/cwtickle/danoniplus/compare/v32.6.1...support/v32#files_bucket)
- 🛠️ Excessive設定にショートカットキーを割り当て ( PR [#1512](https://github.com/cwtickle/danoniplus/pull/1512) )
- 🛠️ 矢印・フリーズアローが削除されたとき、それに付属するg_attrObjの値を動的に削除する処理を追加 ( PR [#1514](https://github.com/cwtickle/danoniplus/pull/1514) )
- 🛠️ コードの整理 ( PR [#1514](https://github.com/cwtickle/danoniplus/pull/1514) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v32.6.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v32.6.0...v32.6.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v32.6.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v32.6.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v32.6.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v32.6.1/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v32.6.1/js/lib/danoni_constants.js)

## v32.6.0 ([2023-06-30](https://github.com/cwtickle/danoniplus/releases/tag/v32.6.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v32.6.0/total)](https://github.com/cwtickle/danoniplus/archive/v32.6.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v32.6.0/support/v32?style=social)](https://github.com/cwtickle/danoniplus/compare/v32.6.0...support/v32#files_bucket)
- ⭐ フリーズアローの始点判定有効時、譜面明細画面のVelocity, DifLevelの表示が変わるよう変更 ( PR [#1508](https://github.com/cwtickle/danoniplus/pull/1508) )
- ⭐ ゲージ設定について、Difficultyを変更しても同じゲージ設定があれば初期化しないよう変更 ( PR [#1509](https://github.com/cwtickle/danoniplus/pull/1509) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v32.6.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v32.5.0...v32.6.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v32.6.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v32.5.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v32.6.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v32.6.0/js/danoni_main.js)[🔴](https://github.com/cwtickle/danoniplus/blob/v32.6.0/css/danoni_main.css)

## v32.5.0 ([2023-06-24](https://github.com/cwtickle/danoniplus/releases/tag/v32.5.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v32.5.0/total)](https://github.com/cwtickle/danoniplus/archive/v32.5.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v32.5.0/support/v32?style=social)](https://github.com/cwtickle/danoniplus/compare/v32.5.0...support/v32#files_bucket)
- ⭐ 空押し判定を実装 ( PR [#1503](https://github.com/cwtickle/danoniplus/pull/1503), [#1505](https://github.com/cwtickle/danoniplus/pull/1505), [#1506](https://github.com/cwtickle/danoniplus/pull/1506) )
- ⭐ 譜面明細表示の順番、名称を変更 ( PR [#1504](https://github.com/cwtickle/danoniplus/pull/1504) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v32.5.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v32.4.0...v32.5.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v32.5.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v32.5.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v32.5.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v32.5.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v32.5.0/js/lib/danoni_constants.js)🔴<br>❤️ goe (@goe0)

<img src="https://github.com/cwtickle/danoniplus/assets/44026291/49dcfddd-29b5-4280-9cd3-fe5be17b0693" width="60%">

## v32.4.0 ([2023-06-09](https://github.com/cwtickle/danoniplus/releases/tag/v32.4.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v32.4.0/total)](https://github.com/cwtickle/danoniplus/archive/v32.4.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v32.4.0/support/v32?style=social)](https://github.com/cwtickle/danoniplus/compare/v32.4.0...support/v32#files_bucket)
- ⭐ 設定画面のゲージ詳細に必要達成率（Accuracy）を追加 ( PR [#1500](https://github.com/cwtickle/danoniplus/pull/1500), [#1502](https://github.com/cwtickle/danoniplus/pull/1502), Issue [#744](https://github.com/cwtickle/danoniplus/pull/744) )
- ⭐ 設定画面のゲージ詳細で表示する回復・ダメージ量について、常に実数値を表示するように変更 ( PR [#1500](https://github.com/cwtickle/danoniplus/pull/1500) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v32.4.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v32.3.0...v32.4.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v32.4.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v32.4.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v32.4.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v32.4.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v32.4.0/js/lib/danoni_constants.js)[🔴](https://github.com/cwtickle/danoniplus/blob/v32.4.0/css/danoni_main.css)

<img src="https://github.com/cwtickle/danoniplus/assets/44026291/d923f11c-9f60-457d-98fb-73f25d8a9923" width="60%">

## v32.3.0 ([2023-06-02](https://github.com/cwtickle/danoniplus/releases/tag/v32.3.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v32.3.0/total)](https://github.com/cwtickle/danoniplus/archive/v32.3.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v32.3.0/support/v32?style=social)](https://github.com/cwtickle/danoniplus/compare/v32.3.0...support/v32#files_bucket)
- ⭐ 9Akey, 11ikey, 17keyのデフォルトシャッフルグループを変更 ( PR [#1497](https://github.com/cwtickle/danoniplus/pull/1497) )
- ⭐ 17keyのデフォルトカラーグループを変更 ( PR [#1497](https://github.com/cwtickle/danoniplus/pull/1497) )
- 🐞 **C)** customCreditWidthが使用できない問題を修正 ( PR [#1496](https://github.com/cwtickle/danoniplus/pull/1496) ) <- :boom: [**v23.4.0**](./Changelog-v23#v2340-2021-10-02)
- 🐞 **C)** 矢印数がゼロの場合にツール値がNaNになる問題を修正 ( PR [#1498](https://github.com/cwtickle/danoniplus/pull/1498) ) <- :boom: [**v31.7.0**](./Changelog-v31#v3170-2023-05-03)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v32.3.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v32.2.2...v32.3.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v32.3.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v32.3.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v32.3.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v32.3.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v32.3.0/js/lib/danoni_constants.js)<br>❤️ MFV2 (@MFV2)

## v32.2.2 ([2023-05-26](https://github.com/cwtickle/danoniplus/releases/tag/v32.2.2))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v32.2.2/total)](https://github.com/cwtickle/danoniplus/archive/v32.2.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v32.2.2/support/v32?style=social)](https://github.com/cwtickle/danoniplus/compare/v32.2.2...support/v32#files_bucket)
- 🐞 **B)** blankFrameが未指定のときにblankFrameが正しく反映されない問題を修正 ( PR [#1494](https://github.com/cwtickle/danoniplus/pull/1494) ) <- :boom: [**v32.1.0**](./Changelog-v32#v3210-2023-05-13)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v32.2.2)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v32.2.1...v32.2.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v32.2.2)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v32.2.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v32.2.2.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v32.2.2/js/danoni_main.js)<br>❤️ MFV2 (@MFV2)

## v32.2.1 ([2023-05-25](https://github.com/cwtickle/danoniplus/releases/tag/v32.2.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v32.2.1/total)](https://github.com/cwtickle/danoniplus/archive/v32.2.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v32.2.1/support/v32?style=social)](https://github.com/cwtickle/danoniplus/compare/v32.2.1...support/v32#files_bucket)
- 🛠️ 速度変化が無いときの速度変化グラフにおける全体加速・個別加速の表記を変更 ( PR [#1492](https://github.com/cwtickle/danoniplus/pull/1492) )
- 🐞 **C)** 譜面明細画面でQキーを押してもカーソル移動しない問題を修正 ( PR [#1491](https://github.com/cwtickle/danoniplus/pull/1491) ) <- :boom: [**v32.2.0**](./Changelog-v32#v3220-2023-05-21)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v32.2.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v32.2.0...v32.2.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v32.2.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v32.2.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v32.2.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v32.2.1/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v32.2.1/js/lib/danoni_constants.js)

## v32.2.0 ([2023-05-21](https://github.com/cwtickle/danoniplus/releases/tag/v32.2.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v32.2.0/total)](https://github.com/cwtickle/danoniplus/archive/v32.2.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v32.2.0/support/v32?style=social)](https://github.com/cwtickle/danoniplus/compare/v32.2.0...support/v32#files_bucket)
- ⭐ 速度変化グラフにおいて速度変化の平均値表示を追加 ( PR [#1489](https://github.com/cwtickle/danoniplus/pull/1489) )
- ⭐ 譜面明細画面の部分表示に対応 ( PR [#1487](https://github.com/cwtickle/danoniplus/pull/1487) )
- ⭐ Shuffleオプションの「Asym-Mirror」を拡張し「X-Mirror」に名称変更 ( PR [#1488](https://github.com/cwtickle/danoniplus/pull/1488) )
- 🛠️ 「Mirror」と「X-Mirror」のハイスコア保存先を分離 ( PR [#1488](https://github.com/cwtickle/danoniplus/pull/1488) )
- 🛠️ 14keyのシャッフル配列を以前のものに修正 ( PR [#1488](https://github.com/cwtickle/danoniplus/pull/1488) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v32.2.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v32.1.0...v32.2.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v32.2.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v32.2.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v32.2.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v32.2.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v32.2.0/js/lib/danoni_constants.js)

## v32.1.0 ([2023-05-13](https://github.com/cwtickle/danoniplus/releases/tag/v32.1.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v32.1.0/total)](https://github.com/cwtickle/danoniplus/archive/v32.1.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v32.1.0/support/v32?style=social)](https://github.com/cwtickle/danoniplus/compare/v32.1.0...support/v32#files_bucket)
- ⭐ ボタンのデフォルト処理を参照する機能を追加 ( PR [#1485](https://github.com/cwtickle/danoniplus/pull/1485) )
- ⭐ 曲開始までの空白フレーム数設定(blankFrame)の譜面別設定に対応 ( PR [#1484](https://github.com/cwtickle/danoniplus/pull/1484) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v32.1.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v32.0.0...v32.1.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v32.1.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v32.1.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v32.1.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v32.1.0/js/danoni_main.js)

## v32.0.0 ([2023-05-07](https://github.com/cwtickle/danoniplus/releases/tag/v32.0.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v32.0.0/total)](https://github.com/cwtickle/danoniplus/archive/v32.0.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v32.0.0/support/v32?style=social)](https://github.com/cwtickle/danoniplus/compare/v32.0.0...support/v32#files_bucket)
- ⭐ Shift, Ctrl, Altキーの左右キーの割り当てを分離 ( PR [#1481](https://github.com/cwtickle/danoniplus/pull/1481) )
- 🛠️ カスタムキー定義の略記指定を強化 ( PR [#1480](https://github.com/cwtickle/danoniplus/pull/1480), [#1481](https://github.com/cwtickle/danoniplus/pull/1481), [#1482](https://github.com/cwtickle/danoniplus/pull/1482) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v32.0.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v31.7.1...v32.0.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v32.0.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v32.0.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v32.0.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v32.0.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v32.0.0/js/lib/danoni_constants.js)

<img src="https://user-images.githubusercontent.com/44026291/236628807-ee7a8696-0829-4c0f-a8f1-cf4b845cbd59.png" width="70%">

[**<- v33**](./Changelog-v33) | **v32** | [**v31 ->**](./Changelog-v31)  
