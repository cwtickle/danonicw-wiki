**[English](https://github.com/cwtickle/danoniplus-docs/wiki/LifecyclePolicy) | Japanese** 

| [< バージョン管理ポリシー](./VersioningPolicy) | **ライフサイクルポリシー** | [リリースまでの流れ >](./Release-Procedures) |

# ライフサイクルポリシー / Lifecycle Policy
- Dancing☆Onigiri (CW Edition) におけるバージョン別のライフサイクルは下記の通りです。  
原則メジャーバージョンが3回上がるまでは不具合修正を行う予定です。
- 各サイトによってしばらく本体更新ができない可能性を考慮し、5世代ごとに長めの不具合修正を行うバージョンを準備します。

|メジャーバージョン名|実バージョン例|更新終了時期|
|----|----|----|
|**v(5n + 4)**|v29, v34|**9バージョン先**のメジャーバージョンリリースまで|
|上記以外|v30～33, v35～38|**3バージョン先**のメジャーバージョンリリースまで|

**ライフサイクルイメージ** : 縦軸 > 対象のバージョン、横軸 > サポートされる時間軸

<img src="https://github.com/cwtickle/danoniplus/assets/44026291/30bdeae5-eb16-4c3b-8403-259e4f7ae0ba" width="100%">

## 関連項目
- [Security Policy](https://github.com/cwtickle/danoniplus/security/policy)
- [バージョン管理ポリシー](VersioningPolicy)
- [更新を終了したバージョンの不具合情報](DeprecatedVersionBugs)

## 注意点
- 今後の更新頻度によって、予告なく見直す場合があります。

| [< バージョン管理ポリシー](./VersioningPolicy) | **ライフサイクルポリシー** | [リリースまでの流れ >](./Release-Procedures) |

