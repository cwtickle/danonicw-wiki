
[^ Tips Indexに戻る](./tips-index)

| < [背景の表示方法](./tips-0002-background) | **画面の位置調整方法** | [ステップゾーンや矢印全体を一時的に隠す](./tips-0010-hide-objects) > |

# 画面の位置調整方法
- ステップゾーン、判定キャラクタ、歌詞表示位置(一部)を変更できます。  
変更できるのはいずれもY座標のみです。
- X座標についてはプレイ画面全体を変更するオプションがあります。

## 方法
### 1. ステップゾーン(Y座標)
- 譜面ヘッダー「stepY」「stepYR」を使用します。（関連項目を参照してください）

![dos-h0014-01.png](./wiki/dos-h0014-01.png)

### 2. 判定キャラクタ、コンボ、Fast/Slow表示(Y座標)
- 譜面ヘッダー「arrowJdgY」「frzJdgY」を使用します。（関連項目を参照してください）
- 判定キャラクタ、コンボ、Fast/Slow表示そのものを非表示にしたい場合は、  
下記の譜面ヘッダーを指定してください。
```
|judgmentUse=false|  // 判定キャラクタ、コンボ
|fastSlowUse=false|  // Fast/Slow表示
```

### 3. 歌詞表示(Y座標、リバースのみ)
- 歌詞表示の位置そのものの変更はできませんが、  
「stepYR」の値に合わせて位置を追随させる設定が可能です。  
譜面ヘッダー「bottomWordSet」を使用します。（関連項目を参照してください）

![dos-h0059-01.png](./wiki/dos-h0059-01.png)

### 4. 背景、マスクを除くプレイ画面全体(X座標)
- 譜面ヘッダー「playingX」「playingWidth」を使って調整します。  
前者は左端の位置、後者はプレイ中の横幅を表します。  
（下記の例ではわかりやすいように境界線を入れています）

<img src="https://user-images.githubusercontent.com/44026291/199850377-f4516af7-d8c4-452e-8561-4f6f4eade429.png" width="100%">

### 5. プレイ画面のカスタム表示部分のエリア確保(X方向、横幅指定)
- プレイ画面のクレジット表示のうち、スコアや速度変化表示などカスタムを行っている理由で、クレジット表示を**被せたくない幅**を「customCreditWidth」で指定できます。

<img src="./wiki/dos-h0083-01.png" width="80%">

## 動作確認バージョン
- 1～3はv11以降、4はv16以降、5はv23.4以降で利用できます。

## ページ作成者
- ティックル

## 関連項目
- [stepY](dos-h0014-stepY) [:pencil:](dos-h0014-stepY/_edit) ステップゾーンのY座標位置
- [stepYR](dos-h0049-stepYR) [:pencil:](dos-h0049-stepYR/_edit) ステップゾーン(下)のY座標現位置からの差分
- [arrowJdgY / frzJdgY](dos-h0058-jdgY) [:pencil:](dos-h0058-jdgY/_edit) 判定キャラクタのY座標位置
- [displayUse](dos-h0057-displayUse) [:pencil:](dos-h0035-displayUse/_edit) Display項目の利用有無
- [bottomWordSet](dos-h0059-bottomWordSet) [:pencil:](dos-h0059-buttomWordSet/_edit) 下側の歌詞表示位置をステップゾーン位置に連動させる設定
- [playingX](dos-h0070-playingX) [:pencil:](dos-h0070-playingX/_edit) ゲーム表示エリアのX座標
- [playingWidth](dos-h0071-playingWidth) [:pencil:](dos-h0071-playingWidth/_edit) ゲーム表示エリアの横幅
- [customCreditWidth](dos-h0083-customCreditWidth) [:pencil:](dos-h0071-playingWidth/_edit) カスタムクレジットエリアの横幅

[^ Tips Indexに戻る](./tips-index)

| < [背景の表示方法](./tips-0002-background) | **画面の位置調整方法** | [ステップゾーンや矢印全体を一時的に隠す](./tips-0010-hide-objects) > |
