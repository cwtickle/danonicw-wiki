**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-h0092-keyGroupOrder) | Japanese** 

[Home](./) &gt; [譜面ヘッダー仕様](dos_header) &gt; [設定時の初期設定](dos_header#-設定時の初期設定)

| [<- transKeyUse](dos-h0024-transKeyUse) | **keyGroupOrder** | [customFont ->](dos-h0020-customFont) |

## keyGroupOrder
- キーコンフィグで設定可能な部分キーグループの設定

### 使い方
```
|keyGroupOrder=11L$11L,5,12,7i|
// 1譜面目はラベル：11Lのみ表示
// 2譜面目はラベル：11L, 5, 12, 7i の順に表示
```

### 説明
キーコンフィグ画面で選択可能なキーグループ名をカンマ区切りで指定します。  
指定した順でキーコンフィグを設定することができます。  
キーグループ名は、[カスタムキー定義](./keys)の **keyGroupX**で指定した名前から選びます。  
複数譜面に跨るときは、`$`区切りで記述します。

この設定を行うことで、制作者が意図した範囲でのみキーコンフィグ機能を表示することが可能です。  
トランスキー（キー変化を伴う譜面）で使用することを想定しています。

指定が無い場合、キーグループ名を数字に変換した上で昇順になるように並び替えます。  
トランスキー以外の場合は、一律キーグループ「0」に割り当てます。

<img src="https://user-images.githubusercontent.com/44026291/217821439-4dc8a7c1-459e-4fd9-9593-d139c183b994.png" width="50%"><img src="https://user-images.githubusercontent.com/44026291/217821591-9ea4b9ca-9dff-4c02-a52d-b77469fb49b7.png" width="50%">

### 補足
- トランスキーのような一時的な利用で無い場合は、カスタムキー定義とすることも可能です。

#### 全てを表示しておきキーコンフィグ画面のみ分割する例 (keyGroupOrderX)
```
|keyCtrl25=D2,D3,D4,D7,D8,D9,W,E,R,U,I,O,7_0,Z,X,C,M,Comma,Period|
|chara25=aleft,aleftdia,adown,aup,arightdia,aright,bleft,bleftdia,bdown,bup,brightdia,bright,7_0,cleft,cleftdia,cdown,cup,crightdia,cright|
|color25=4,4,4,4,4,4,3,3,3,3,3,3,0,0,0,2,0,0,0,1,1,1,1,1,1|
|stepRtn25=0,-45,-90,90,135,180,0,-45,-90,90,135,180,7_0_0,0,-45,-90,90,135,180|
|pos25=0,1,2,4,5,6,0.01,1.01,2.01,4.01,5.01,6.01,0.02,1.02,2.02,3.02,4.02,5.02,6.02,0.03,1.03,2.03,4.03,5.03,6.03|
|keyGroup25=0/1,0/1,0/1,0/1,0/1,0/1,0/2,0/2,0/2,0/2,0/2,0/2,0/3,0/3,0/3,0/1/2/3/4,0/3,0/3,0/3,0/4,0/4,0/4,0/4,0/4,0/4|

|keyGroupOrder25=1,2,3,4|
```

### 関連項目
- [キー数仕様](./keys)
- [キー数変化 (keych_data)](./dos-e0006-keychData)
- [キー変化作品の実装例](./tips-0009-key-types-change)

### 更新履歴

|Version|変更内容|
|----|----|
|[v30.0.1](https://github.com/cwtickle/danoniplus/releases/tag/v30.0.1)|・初回実装|

| [<- transKeyUse](dos-h0024-transKeyUse) | **keyGroupOrder** | [customFont ->](dos-h0020-customFont) |
