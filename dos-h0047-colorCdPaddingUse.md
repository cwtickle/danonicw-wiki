**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-h0047-colorCdPaddingUse) | Japanese** 

[Home](./) &gt; [譜面ヘッダー仕様](dos_header) &gt; [設定時の初期設定](dos_header#-設定時の初期設定)

| [<- colorDataType](dos-h0046-colorDataType) | **colorCdPaddingUse** || [setColor ->](dos-h0003-setColor) |

## colorCdPaddingUse
- 初期矢印色/フリーズアロー色の前ゼロパディング有無設定

### 使い方
```
|colorCdPaddingUse=true|
```
### 説明
初期矢印色・初期フリーズアロー色のカラーコードについて、  
前ゼロパディングするかどうかの設定を行います。  
例えば、`0xff`を`#0000ff`、`0xffff`を`#00ffff`のように補完します。  

デフォルトは`false`(ゼロパディングしない)です。

|値|既定|内容|
|----|----|----|
|false|*|ゼロパディングしない (JavaScript基準)|
|true||ゼロパディングする (旧Flash基準)|

### 補足
この設定は、旧FlashとJavaScriptで、カラーコードの補完方法が異なるため残しています。

この設定による適用範囲は、譜面ヘッダー：setColor/frzColorのみです。  
個別色変化(color_data), 全体色変化(acolor_data)には適用されません。

### 関連項目
- [**setColor**](dos-h0003-setColor) [:pencil:](dos-h0003-setColor/_edit) 矢印色
- [**frzColor**](dos-h0004-frzColor) [:pencil:](dos-h0004-frzColor/_edit) フリーズアロー色

### 更新履歴

|Version|変更内容|
|----|----|
|[v7.4.0](https://github.com/cwtickle/danoniplus/releases/tag/v7.4.0)|・初回実装|

| [<- colorDataType](dos-h0046-colorDataType) | **colorCdPaddingUse** || [setColor ->](dos-h0003-setColor) |