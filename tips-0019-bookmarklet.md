
[^ Tips Indexに戻る](./tips-index)

| < [タイトルや歌詞表示に半角コンマ(,)を使う](./tips-0001-comma) | **ブックマークレット集** | [プレイ画面外のテキスト入力を有効にする](./tips-0017-external-input) > |

# ブックマークレット集

- ブックマークのアドレス欄に記載し、  
ダンおにの作品ページから起動することで使用できるブックマークレット集です。
- Discordで公開されていたものを中心にまとめています。

## 作品リプレイ (from: apoi)

### 概要

- プレイ後に表示される「DL replay data」で出力されたテキストファイルをタイトル画面の「Read replay data」から読み込むと、プレイした内容が再現されます。
- 判定タイミングを再現しているだけなので、Difficultyやオプションは適切な値に直す必要があります。

<img src="https://github.com/user-attachments/assets/5b5ba98a-7317-4126-be05-d099adb83723" width="50%"><img src="https://github.com/user-attachments/assets/3f79ae03-5834-479a-944d-87670ea9bf1c" width="50%">

### ブックマークレット

```javascript
javascript:g_customJsObj.titleEnterFrame.push(_=>{const s=()=>{return new Promise(resolve=>{const p=document.createElement('input');p.type='file';p.accept='.txt,text/plain';p.onchange=event=>{resolve(event.target.files[0]);};p.click();});};const t=f=>{return new Promise(resolve=>{const r=new FileReader();r.readAsText(f);r.onload=()=>{resolve(r.result);};});};const r=async()=>{const f=await s();const c=await t(f);try{g_rootObj.d=JSON.parse(c);alert("データを読み込みました。\nこの先の画面でPLAY!ボタンを押すとプレイデータを再生します。");}catch{alert("不正なフォーマットです。");}};const b=document.querySelector("#b");if(b===null){multiAppend(divRoot,createCss2Button(%60b%60,"Read replay data",_=>true,Object.assign({x:g_sWidth-200,y:400-40,w:200,h:40,siz:20},{resetFunc:r,}),g_cssObj.button_Default));}});g_customJsObj.main.push(()=>{g_rootObj.h=new Array(g_scoreObj.fullFrame+1);g_rootObj.h.fill({});});g_customJsObj.mainEnterFrame.push(_=>{if(!g_scoreObj.frameNum&&hasVal(g_rootObj.d)){document.onkeydown=e=>{e.preventDefault();const c=transCode(e);if(e.repeat&&!g_mainRepeatObj.key.includes(c)){return blockCode(c);}if(c===g_kCdN[g_headerObj.keyRetry]){if(g_isMac&&(keyIsDown(g_kCdNameObj.shiftLKey)||keyIsDown(g_kCdNameObj.shiftRKey))){g_audio.pause();clearTimeout(g_timeoutEvtId);titleInit();}else{g_audio.pause();clearTimeout(g_timeoutEvtId);clearWindow();musicAfterLoaded();}}else if(c===g_kCdN[g_headerObj.keyTitleBack]){g_audio.pause();clearTimeout(g_timeoutEvtId);titleInit();}else if(g_appearanceRanges.includes(g_stateObj.appearance)&&g_stateObj.filterLock===C_FLG_OFF){if(c===g_hidSudObj.pgDown[g_stateObj.appearance][g_stateObj.reverse]){changeAppearanceFilter(g_stateObj.appearance,g_hidSudObj.filterPos<100?g_hidSudObj.filterPos+1:g_hidSudObj.filterPos);}else if(c===g_hidSudObj.pgUp[g_stateObj.appearance][g_stateObj.reverse]){changeAppearanceFilter(g_stateObj.appearance,g_hidSudObj.filterPos>0?g_hidSudObj.filterPos-1:g_hidSudObj.filterPos);}}return blockCode(c);};document.onkeyup=e=>{};}if(hasVal(g_rootObj.d)){if(g_scoreObj.baseFrame>=0){g_inputKeyBuffer=g_rootObj.d[g_scoreObj.baseFrame];for(const p in g_inputKeyBuffer){if(g_inputKeyBuffer[p]){const m=g_workObj.keyCtrlN;for(let j=0;j<getKeyInfo().keyNum;j++){m[j].filter((m,k)=>p===m&&!g_workObj.keyHitFlg[j][k]&&!g_judgObj.lockFlgs[j]).forEach(()=>{g_judgObj.lockFlgs[j]=true;judgeArrow(j);g_judgObj.lockFlgs[j]=false;});}}else{for(let j=0;j<getKeyInfo().keyNum;j++){if(g_workObj.keyCtrlN[j].find(m=>keyIsDown(m))===undefined){$id(%60stepDiv${j}%60).display=C_DIS_NONE;}}}}}}else{g_rootObj.h[g_scoreObj.baseFrame]=Object.assign({},g_inputKeyBuffer);}});g_customJsObj.result.push(_=>{const r=_=>{const f=new Intl.DateTimeFormat(undefined,{year:'numeric',month:'2-digit',day:'2-digit',hour:'2-digit',minute:'2-digit',second:'2-digit',});const d=f.format(new Date()).replace(/[\/:]/g,"").replace(/\s/,"_");const i=JSON.stringify(g_rootObj.h);const b=new Blob([i],{type:"text/plain"});const u=URL.createObjectURL(b);const a=document.createElement("a");a.href=u;a.download=%60Replay_${g_headerObj.musicTitle}[${g_headerObj.difLabels[g_stateObj.scoreId]}]_${d}.txt%60;a.click();URL.revokeObjectURL(u);};if(!("d"in g_rootObj)){multiAppend(divRoot,createCss2Button(%60b%60,"DL replay data",_=>true,Object.assign({x:g_sWidth-200,y:464,w:200,h:39,siz:20},{resetFunc:r,}),g_cssObj.button_Default));}});
```

## プレイ中のリトライとタイトルバックを無効にする (from: apoi)

### 概要

- 画面下部にプレイ中のリトライとタイトルバックを無効にするチェックボックスが表示されます。
- チェックがついている間はプレイ中のショートカットが無効化されます。
- チェックを外すとリトライ⇒「Tab」キー、タイトルバック⇒「Esc」キーに割り当てられます。

<img src="https://github.com/user-attachments/assets/a9951365-60af-4834-9fc2-f041c23de80a" width="50%">

### ブックマークレット

```javascript
javascript:(function(){if(document.getElementById("preventJourney")){return;}let d=document.createElement("div");d.style.textAlign="center";d.style.marginTop="1em";let e=document.createElement("input");e.type="checkbox";e.id="preventJourney";e.style.transform="scale(1.6)";e.style.marginRight="1em";e.checked=true;g_headerObj.keyRetry=g_headerObj.keyTitleBack=0;g_headerObj.keyRetryDef=9;g_headerObj.keyTitleBackDef=27;let l=document.createElement("label");l.setAttribute("for","preventJourney");l.textContent="Disable Retry and Title back";l.style.fontSize="1.4em";d.appendChild(e);d.appendChild(l);let p=document.getElementById("canvas-frame").parentElement;p.appendChild(d);e.addEventListener("change",()=>{if(e.checked){g_headerObj.keyRetry=0;g_headerObj.keyTitleBack=0;}else{g_headerObj.keyRetry=g_headerObj.keyRetryDef;g_headerObj.keyTitleBack=g_headerObj.keyTitleBackDef;document.getElementById("lblRetrySc").innerText=g_kCd[g_headerObj.keyRetry];document.getElementById("lblTitleBackSc").innerText=g_kCd[g_headerObj.keyTitleBack];}});})()
```

## EX SCOREとMAX差分を画面上に付加 (from: すずめ)

### 概要

- EX SCOREとMAX差分をプレイ中とリザルト画面に表示します。

<img src="https://github.com/user-attachments/assets/c8c714ed-72a8-4157-8fe8-f54429953072" width="50%"><img src="https://github.com/user-attachments/assets/1749c3f8-70b2-4fbd-bb68-3f8089d7ff5b" width="50%">

### ブックマークレット

```javascript
javascript:(function(){g_customJsObj.main.push(()=>{document.getElementById("infoSprite").appendChild(makeCounterSymbol("lblExScore",g_headerObj.playingWidth-110+(g_workObj.nonDefaultSc?g_headerObj.scAreaWidth:0),"exscore",13,0)),document.getElementById("infoSprite").appendChild(makeCounterSymbol("lblMaxDiff",g_headerObj.playingWidth-110+(g_workObj.nonDefaultSc?g_headerObj.scAreaWidth:0),"maxdiff",14,0)),document.getElementById("lblMaxDiff").style.color="#ff6666",document.getElementById("lblMaxDiff").style.fontSize="12px"});const calc=()=>{let e=3*g_resultObj.ii+3*g_resultObj.kita+2*g_resultObj.shakin+g_resultObj.matari,t=(g_resultObj.ii+g_resultObj.shakin+g_resultObj.matari+g_resultObj.shobon+g_resultObj.uwan+g_resultObj.kita+g_resultObj.iknai)*3,l="MAX"+(t>e?"-":"+")+(t-e);return[e,l]};g_customJsObj.mainEnterFrame.push(()=>{let[e,t]=calc();document.getElementById("lblExScore").textContent=e,document.getElementById("lblMaxDiff").textContent=t}),g_customJsObj.result.push(()=>{let[e,t]=calc(),l=260;g_stateObj.excessive===C_FLG_ON&&(l=360),multiAppend(document.getElementById("resultWindow"),makeCssResultSymbol("lblExScore",l+90,g_cssObj.common_kita,6,"ExScore"),makeCssResultSymbol("lblExScoreS",l,g_cssObj.score,7,e,C_ALIGN_RIGHT),makeCssResultSymbol("lblMaxDiff",l,g_cssObj.score,8,t,C_ALIGN_RIGHT)),document.getElementById("lblMaxDiff").style.color="#ff6666",document.getElementById("lblMaxDiff").style.fontSize="12px"})})()
```

## ページ作成者
- ティックル

[^ Tips Indexに戻る](./tips-index)

| < [タイトルや歌詞表示に半角コンマ(,)を使う](./tips-0001-comma) | **ブックマークレット集** | [プレイ画面外のテキスト入力を有効にする](./tips-0017-external-input) > |
