**[English](https://github.com/cwtickle/danoniplus-docs/wiki/Changelog-v22) | Japanese** 

[^ Back to All Version List (Security Policy)](https://github.com/cwtickle/danoniplus/security/policy)  
[^ Back to Update Info](./UpdateInfo#-v22-changelog)

 [**<- v23**](Changelog-v23) | **v22** | [**v21 ->**](Changelog-v21)  
(**🔖 [11 Releases](https://github.com/cwtickle/danoniplus/releases?q=tag%3Av22)** )

## 🔃 Files changed (v22)

|Directory|FileName||Last Updated|
|----|----|----|----|
|/js|danoni_main.js|[📥](https://github.com/cwtickle/danoniplus/releases/download/v22.5.6/danoni_main.js)|**v22.5.6**|
|/js/lib|danoni_constants.js|[📥](https://github.com/cwtickle/danoniplus/releases/download/v22.5.3/danoni_constants.js)|[v22.5.3](Changelog-v22#v2253-2021-09-24)|

<details>
<summary>Changed file list before v21</summary>

|Directory|FileName||Last Updated|
|----|----|----|----|
|/js/lib|danoni_localbinary.js|[📥](https://github.com/cwtickle/danoniplus/releases/download/v15.1.0/danoni_localbinary.js)|[v15.1.0](./Changelog-v15#v1510-2020-05-21)|
|/css|danoni_main.css|[📥](https://github.com/cwtickle/danoniplus/releases/download/v21.4.2/danoni_main.css)|[v21.4.2](Changelog-v21#v2142-2021-04-07)|
|/img|aaShadow.svg<br>arrow.svg<br>arrowShadow.svg<br>borderline.svg<br>c.svg<br>cursor.svg<br>giko.svg<br>iyo.svg<br>monar.svg<br>morara.svg<br>onigiri.svg|[📥](https://github.com/cwtickle/danoniplus/releases/download/v15.1.0/img.zip)|[v15.1.0](./Changelog-v15#v1510-2020-05-21)|
|/skin|danoni_skin_default.css<br>danoni_skin_light.css<br>danoni_skin_skyblue.css|[📥](https://github.com/cwtickle/danoniplus/releases/download/v21.4.2/danoni_skin_default.css)<br>[📥](https://github.com/cwtickle/danoniplus/releases/download/v21.4.2/danoni_skin_light.css)<br>[📥](https://github.com/cwtickle/danoniplus/releases/download/v21.4.2/danoni_skin_skyblue.css)|[v21.4.2](Changelog-v21#v2142-2021-04-07)|

</details>

⭐ New Features / 🛠️ Improvements / 🐞 Bug Fixes  
( Files changed: ⚪ danoni_main.js / 🔵 danoni_constants.js / 🔴 css files )

## [Warning] 更新終了後の不具合情報
- [更新を終了したバージョンの不具合情報#v22](DeprecatedVersionBugs#v22) を参照

## v22.5.6 ([2021-10-27](https://github.com/cwtickle/danoniplus/releases/tag/v22.5.6))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v22.5.6/total)](https://github.com/cwtickle/danoniplus/archive/v22.5.6.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v22.5.6/support/v22?style=social)](https://github.com/cwtickle/danoniplus/compare/v22.5.6...support/v22#files_bucket) 
- 🐞 Reverse時のarrowMotionDataが正しく反映されない問題を修正 ( PR [#1146](https://github.com/cwtickle/danoniplus/pull/1146) ) <- :boom: [**v22.5.4**](Changelog-v22#v2254-2021-10-04)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v22.5.6)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v22.5.5...v22.5.6#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v22.5.6)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v22.5.6.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v22.5.6.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v22.5.6/js/danoni_main.js)
/ 🎣 [**v24.0.1**](./Changelog-v24#v2401-2021-10-27)

## v22.5.5 ([2021-10-14](https://github.com/cwtickle/danoniplus/releases/tag/v22.5.5))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v22.5.5/total)](https://github.com/cwtickle/danoniplus/archive/v22.5.5.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v22.5.5/support/v22?style=social)](https://github.com/cwtickle/danoniplus/compare/v22.5.5...support/v22#files_bucket) 
- 🛠️ 曲中リトライキーの猶予フレームを廃止 ( PR [#1137](https://github.com/cwtickle/danoniplus/pull/1137), Gitter [2021-10-14](https://gitter.im/danonicw/community?at=6167f968fb8ca0572bbf48d6) )
- 🐞 Display画面から直接開始した場合、プレイ後キーコンフィグ画面に戻るとキーパターンがデータセーブ有効時、Selfにならない問題を修正 ( PR [#1136](https://github.com/cwtickle/danoniplus/pull/1136) ) <- :boom: [**v4.6.0**](Changelog-v4#v460-2019-05-03)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v22.5.5)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v22.5.4...v22.5.5#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v22.5.5)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v22.5.5.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v22.5.5.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v22.5.5/js/danoni_main.js)
/ 🎣 [**v23.5.0**](./Changelog-v23#v2350-2021-10-14)

## v22.5.4 ([2021-10-04](https://github.com/cwtickle/danoniplus/releases/tag/v22.5.4))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v22.5.4/total)](https://github.com/cwtickle/danoniplus/archive/v22.5.4.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v22.5.4/support/v22?style=social)](https://github.com/cwtickle/danoniplus/compare/v22.5.4...support/v22#files_bucket) 
- 🐞 矢印データが昇順で無いときに一部矢印が表示されないことがある問題を修正 ( PR [#1134](https://github.com/cwtickle/danoniplus/pull/1134), Gitter [2021-10-03](https://gitter.im/danonicw/community?at=61595d062197144e84443743) ) <- :boom: **initial**

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v22.5.4)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v22.5.3...v22.5.4#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v22.5.4)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v22.5.4.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v22.5.4.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v22.5.4/js/danoni_main.js)
/ 🎣 [**v23.4.1**](./Changelog-v23#v2341-2021-10-04)

## v22.5.3 ([2021-09-24](https://github.com/cwtickle/danoniplus/releases/tag/v22.5.3))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v22.5.3/total)](https://github.com/cwtickle/danoniplus/archive/v22.5.3.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v22.5.3/support/v22?style=social)](https://github.com/cwtickle/danoniplus/compare/v22.5.3...support/v22#files_bucket) 
- 🛠️ 設定画面のショートカットキーについて、ReverseをRキー、Scrollを上下キーに変更 ( PR [#1120](https://github.com/cwtickle/danoniplus/pull/1120), [#1121](https://github.com/cwtickle/danoniplus/pull/1121) )
- 🐞 Reverseのショートカットキー(Rキー)について、拡張スクロールがある場合でもバックグラウンドで動いてしまう問題を修正 ( PR [#1120](https://github.com/cwtickle/danoniplus/pull/1120) ) <- :boom: [**v19.5.0**](Changelog-v19#v1950-2021-02-09)
- 🐞 Scroll無効時、Reverseの左キーとラベルボタンの一部が押せない問題を修正 ( PR [#1120](https://github.com/cwtickle/danoniplus/pull/1120) ) <- :boom: [**v10.2.1**](Changelog-v10#v1021-2019-11-20)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v22.5.3)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v22.5.2...v22.5.3#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v22.5.3)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v22.5.3.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v22.5.3.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v22.5.3/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v22.5.3/js/lib/danoni_constants.js)
/ 🎣 [**v23.3.1**](./Changelog-v23#v2331-2021-09-24)

## v22.5.2 ([2021-09-17](https://github.com/cwtickle/danoniplus/releases/tag/v22.5.2))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v22.5.2/total)](https://github.com/cwtickle/danoniplus/archive/v22.5.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v22.5.2/support/v22?style=social)](https://github.com/cwtickle/danoniplus/compare/v22.5.2...support/v22#files_bucket) 
- 🛠️ Motion: Boost時の軌道計算方法を見直し ( PR [#1111](https://github.com/cwtickle/danoniplus/pull/1111) )
- 🐞 フリーズアローを押し直したとき終端の位置がずれる問題を修正 ( PR [#1110](https://github.com/cwtickle/danoniplus/pull/1110) ) <- :boom: **initial**
- 🐞 フリーズアローを離したとき全体の位置がずれる問題を修正 ( PR [#1110](https://github.com/cwtickle/danoniplus/pull/1110) ) <- :boom: **initial**
- 🐞 Motion: Boost, Brake時、早めにフリーズアローを押し始めると終端の位置がずれる問題を修正 ( PR [#1110](https://github.com/cwtickle/danoniplus/pull/1110) ) <- :boom: **initial**
- 🐞 遅めにフリーズアローを押し始めると、終端の直後に次のフリーズアローが存在する場合にのみ、最後まで押し切ってもイクナイ判定となってしまう問題を修正 ( PR [#1110](https://github.com/cwtickle/danoniplus/pull/1110) ) <- :boom: [**v5.12.1**](Changelog-v5#v5121-2019-06-17)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v22.5.2)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v22.5.1...v22.5.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v22.5.2)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v22.5.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v22.5.2.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v22.5.2/js/danoni_main.js)
/ 🎣 [**v23.1.1**](./Changelog-v23#v2311-2021-09-17)

---

## v22.5.1 ([2021-06-13](https://github.com/cwtickle/danoniplus/releases/tag/v22.5.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v22.5.1/total)](https://github.com/cwtickle/danoniplus/archive/v22.5.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v22.5.1/support/v22?style=social)](https://github.com/cwtickle/danoniplus/compare/v22.5.1...support/v22#files_bucket) 
- ⭐ サーバ上のデフォルト画像セットの切り替え、設定戻しに対応 ( PR [#1090](https://github.com/cwtickle/danoniplus/pull/1090), [#1092](https://github.com/cwtickle/danoniplus/pull/1092) )
- ⭐ キャラクタ画像の追加に部分的に対応 ( PR [#1090](https://github.com/cwtickle/danoniplus/pull/1090) )
- 🛠️ メイン画面以外でキーリピート禁止を緩和 ( PR [#1088](https://github.com/cwtickle/danoniplus/pull/1088) )
- 🛠️ 譜面詳細開閉ボタンについて、右クリックでも動作するように変更 ( PR [#1089](https://github.com/cwtickle/danoniplus/pull/1089) )
- 🛠️ 画像フォルダの整理、svgファイル出力元ファイルを付属 ( PR [#1090](https://github.com/cwtickle/danoniplus/pull/1090) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v22.5.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v22.4.1...v22.5.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v22.5.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v22.5.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v22.5.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v22.5.1/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v22.5.1/js/lib/danoni_constants.js)<br>❤️ apoi, しょーでん

## v22.4.1 ([2021-05-16](https://github.com/cwtickle/danoniplus/releases/tag/v22.4.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v22.4.1/total)](https://github.com/cwtickle/danoniplus/archive/v22.4.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v22.4.1/support/v22?style=social)](https://github.com/cwtickle/danoniplus/compare/v22.4.1...support/v22#files_bucket) 
- ⭐ 色グループのまとめ変更機能の実装（通常キーのみ） ( PR [#1084](https://github.com/cwtickle/danoniplus/pull/1084) )
- ⭐ 17keyのシャッフルグループを追加 ( PR [#1084](https://github.com/cwtickle/danoniplus/pull/1084) )
- 🛠️ シャッフルグループ番号、初期色の一時変更に関するメッセージ記述を画面左下に追加 ( PR [#1084](https://github.com/cwtickle/danoniplus/pull/1084), [#1086](https://github.com/cwtickle/danoniplus/pull/1086) )
- 🐞 17key(パターン2)のシャッフルグループ間違いを修正 ( PR [#1084](https://github.com/cwtickle/danoniplus/pull/1084) ) <- :boom: [**v3.1.0**](Changelog-v3#v310-2019-02-26)
- 🐞 シャッフルグループを変更後、途中終了して再開するとシャッフルグループが初期化されることがある問題を修正 ( PR [#1084](https://github.com/cwtickle/danoniplus/pull/1084) ) <- :boom: [**v22.0.0**](Changelog-v22#v2200-2021-04-28)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v22.4.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v22.3.2...v22.4.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v22.4.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v22.4.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v22.4.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v22.4.1/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v22.4.1/js/lib/danoni_constants.js)<br>❤️ SKB (@superkuppabros)


## v22.3.2 ([2021-05-13](https://github.com/cwtickle/danoniplus/releases/tag/v22.3.2))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v22.3.2/total)](https://github.com/cwtickle/danoniplus/archive/v22.3.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v22.3.2/support/v22?style=social)](https://github.com/cwtickle/danoniplus/compare/v22.3.2...support/v22#files_bucket) 
- ⭐ 一時的にシャッフルグループ番号を変更できる機能を実装 ( PR [#1075](https://github.com/cwtickle/danoniplus/pull/1075), [#1079](https://github.com/cwtickle/danoniplus/pull/1079), [#1081](https://github.com/cwtickle/danoniplus/pull/1081) )
- ⭐ 9A/11ikeyのシャッフルグループを追加 ( PR [#1074](https://github.com/cwtickle/danoniplus/pull/1074) )
- 🛠️ キーコンフィグ設定、カーソル処理に関するコード見直し ( PR [#1076](https://github.com/cwtickle/danoniplus/pull/1076), [#1077](https://github.com/cwtickle/danoniplus/pull/1077) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v22.3.2)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v22.2.0...v22.3.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v22.3.2)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v22.3.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v22.3.2.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v22.3.2/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v22.3.2/js/lib/danoni_constants.js)

## v22.2.0 ([2021-05-05](https://github.com/cwtickle/danoniplus/releases/tag/v22.2.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v22.2.0/total)](https://github.com/cwtickle/danoniplus/archive/v22.2.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v22.2.0/support/v22?style=social)](https://github.com/cwtickle/danoniplus/compare/v22.2.0...support/v22#files_bucket) 
- ⭐ ColorTypeに「Type3」「Type4」を追加 ( PR [#1071](https://github.com/cwtickle/danoniplus/pull/1071) )
- ⭐ Asym-Mirrorが適用できるように14keyのシャッフルグループを追加 ( PR [#1069](https://github.com/cwtickle/danoniplus/pull/1069) )
- 🛠️ 一時矢印色変更時に影矢印も色変更するよう見直し ( PR [#1070](https://github.com/cwtickle/danoniplus/pull/1070) )
- 🛠️ ColorTypeの「Type1」「Type2」について影矢印が指定されていた場合、黒（白）の塗りつぶしを指定するように変更 ( PR [#1071](https://github.com/cwtickle/danoniplus/pull/1071) )
- 🛠️ ColorType, ShuffleGroupに対して順方向、逆方向のボタンを追加 ( PR [#1071](https://github.com/cwtickle/danoniplus/pull/1071) )
- 🛠️ 明背景用のColorTypeについてフリーズアローヒット時の色味を調整 ( PR [#1071](https://github.com/cwtickle/danoniplus/pull/1071) )
- 🛠️ オンマウス時のメッセージを更新 ( PR [#1072](https://github.com/cwtickle/danoniplus/pull/1072) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v22.2.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v22.1.0...v22.2.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v22.2.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v22.2.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v22.2.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v22.2.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v22.2.0/js/lib/danoni_constants.js)

<img src="./wiki/changelog/v22_0005.png" alt="v22_0005" width="75%">

## v22.1.0 ([2021-05-03](https://github.com/cwtickle/danoniplus/releases/tag/v22.1.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v22.1.0/total)](https://github.com/cwtickle/danoniplus/archive/v22.1.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v22.1.0/support/v22?style=social)](https://github.com/cwtickle/danoniplus/compare/v22.1.0...support/v22#files_bucket) 
- ⭐ Shuffle設定において非対称ミラー (Asym-Mirror) を実装 ( PR [#1067](https://github.com/cwtickle/danoniplus/pull/1067) )
- ⭐ Shuffle設定でシャッフルグループ内の入れ替えのみ可能な設定を追加 ( Issue [#1065](https://github.com/cwtickle/danoniplus/pull/1065), PR [#1066](https://github.com/cwtickle/danoniplus/pull/1066) )
- 🛠️ シャッフルグループが複数の場合、「Random+」「S-Random+」の表記を統一 ( PR [#1064](https://github.com/cwtickle/danoniplus/pull/1064) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v22.1.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v22.0.0...v22.1.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v22.1.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v22.1.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v22.1.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v22.1.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v22.1.0/js/lib/danoni_constants.js)

## v22.0.0 ([2021-04-28](https://github.com/cwtickle/danoniplus/releases/tag/v22.0.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v22.0.0/total)](https://github.com/cwtickle/danoniplus/archive/v22.0.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v22.0.0/support/v22?style=social)](https://github.com/cwtickle/danoniplus/compare/v22.0.0...support/v22#files_bucket) 
- ⭐ 通常キーにおいてシャッフルグループの複数指定に対応 ( PR [#1059](https://github.com/cwtickle/danoniplus/pull/1059) )
- ⭐ 一時的に初期矢印色の割当先を変更できる機能を実装 ( PR [#1060](https://github.com/cwtickle/danoniplus/pull/1060) )
- ⭐ キーコンフィグ画面内の設定ボタンに対し、オンマウスで説明が出るよう変更 ( PR [#1059](https://github.com/cwtickle/danoniplus/pull/1059) )
- 🛠️ キーコンフィグ画面でシャッフル有効時、シャッフルグループを表示するように変更 ( PR [#1059](https://github.com/cwtickle/danoniplus/pull/1059) )
- 🛠️ 共通系処理のコード見直し ( PR [#1061](https://github.com/cwtickle/danoniplus/pull/1061) )
- 🛠️ 非推奨関数を danoni_legacy_function.js へ移動 ( PR [#1062](https://github.com/cwtickle/danoniplus/pull/1062) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v22.0.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v21.5.0...v22.0.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v22.0.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v22.0.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v22.0.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v22.0.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v22.0.0/js/lib/danoni_constants.js)<br>❤️ apoi, SKB (@superkuppabros)

<img src="./wiki/changelog/v22_0001.png" alt="v22_0001" width="55%"><img src="./wiki/changelog/v22_0002.png" alt="v22_0002" width="45%">

<img src="./wiki/changelog/v22_0003.png" alt="v22_0003" width="45%"><img src="./wiki/changelog/v22_0004.png" alt="v22_0004" width="55%">

 [**<- v23**](Changelog-v23)  | **v22** | [**v21 ->**](Changelog-v21)
