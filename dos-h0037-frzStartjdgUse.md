**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-h0037-frzStartjdgUse) | Japanese** 

[Home](./) &gt; [譜面ヘッダー仕様](dos_header) &gt; [プレイ時の初期設定](dos_header#-プレイ時の初期設定) &gt; [ライフゲージ・判定関連の設定](dos_header#ライフゲージ判定関連の設定)

| [<- maxLifeVal](dos-h0045-maxLifeVal) | **frzStartjdgUse** | [frzAttempt ->](dos-h0038-frzAttempt) |

## frzStartjdgUse
- フリーズアロー開始判定の有無設定
- 共通設定 ⇒ [g_presetObj.frzStartjdgUse](dos-s0004-frzArrow#フリーズアローの始点判定設定-g_presetobjfrzstartjdguse)

### 使い方
```
|frzStartjdgUse=true|
```
### 説明
フリーズアローが最初に押されたタイミングで判定を取るかどうかを指定します。  
指定しない場合は false。  

フリーズアロー開始判定を使用した場合、それに合わせてスコアも修正されます。  
(矢印総数が変わるため)  

|値|既定|内容|
|----|----|----|
|false|*|フリーズアロー開始判定を使用しない|
|true||フリーズアロー開始判定を使用する|

### 補足：始点判定有効時の画面差異について
- 右側が始点判定時の表示です。DifLevel(レベル表示)ではAll Arrowsが判定数になるように変わっています。

<img src="https://github.com/cwtickle/danoniplus/assets/44026291/81701427-25b2-481e-b270-a14fda704ed2" width="40%"><img src="https://github.com/cwtickle/danoniplus/assets/44026291/591cde8d-b0d9-48e4-9de3-bda292742a34" width="40%">

<img src="https://github.com/cwtickle/danoniplus/assets/44026291/5b842b75-e0ea-411f-926f-51f0d3605267" width="40%"><img src="https://github.com/cwtickle/danoniplus/assets/44026291/8e576240-d135-4a70-b6aa-a256a8dd06b7" width="40%"> 

### 関連項目
- [customGauge](dos-h0053-customGauge) [:pencil:](dos-h0053-customGauge/_edit) カスタムゲージ設定
- [**gaugeX**](dos-h0022-gaugeX) [:pencil:](dos-h0022-gaugeX/_edit) ゲージ設定の詳細
- [frzAttempt](dos-h0038-frzAttempt) [:pencil:](dos-h0038-frzAttempt/_edit) フリーズアローヒット時の許容フレーム数
- [共通設定ファイル仕様](dos_setting) &gt; [フリーズアロー設定](dos-s0004-frzArrow)

### 更新履歴

|Version|変更内容|
|----|----|
|[v5.7.0](https://github.com/cwtickle/danoniplus/releases/tag/v5.7.0)|・初回実装|

| [<- maxLifeVal](dos-h0045-maxLifeVal) | **frzStartjdgUse** | [frzAttempt ->](dos-h0038-frzAttempt) |