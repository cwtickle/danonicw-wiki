**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-h0085-stockForceDel) | Japanese** 

[Home](./) &gt; [譜面ヘッダー仕様](dos_header) &gt; [プレイ時間制御・譜面位置調整](dos_header#-プレイ時間制御譜面位置調整)

| [<- unStockCategory](dos-h0084-unStockCategory) | **stockForceDel** || [minSpeed ->](dos-h0015-minSpeed) |

## wordStockForceDel / backStockForceDel / maskStockForceDel
- フェードイン前のデータを保持しないパターンの設定
- 共通設定 ⇒ [g_presetObj.stockForceDelList](dos-s0007-viewControl#フェードイン前のデータを保持しないパターンの設定-g_presetobjstockforcedellist)

### 使い方
```
|wordStockForceDel=|         // 無指定のため、除外条件なし
|backStockForceDel=fade|     // leftToRightFade、fadeoutなどのアニメーションが除外対象
|maskStockForceDel=fade,disappear|
```
※英字は小文字で指定する必要があります。

### 説明
ver25.0.0より、フェードイン時にフェードイン前のデータを原則保持し、  
プレイ開始時にそのデータを同時に表示する形式に変わりました。  
ただ、作品によってはCSSアニメーションでフェードアウトを行っている場合など、そのまま出したくない場合があります。  
[unStockCategory](dos-h0084-unStockCategory)では全ての設定を無効にしますが、
この設定では種別ごとにどのパターンを除外するかを指定することが可能です。

|種別|無効にするパターンの対象|
|----|----|
|word (歌詞表示)|歌詞表示本体|
|back, mask (背景・マスク)|アニメーション名|

上記の例では、back_dataやmask_dataに「fade」や「Fade」、「FADE」といった文字列がフェードイン直前のアニメーション名に含まれているとその階層ごと表示対象外にします。
（その他の部分はそのまま表示します）

共通設定ファイルとの重ね掛けが可能です。

### 関連項目
- [unStockCategory](dos-h0084-unStockCategory) [:pencil:](dos-h0084-unStockCategory/_edit) フェードイン前のデータを保持しない種別のリスト
- [共通設定ファイル仕様](dos_setting) &gt; [プレイ画面制御](dos-s0007-viewControl)

### 更新履歴

|Version|変更内容|
|----|----|
|[v25.0.0](https://github.com/cwtickle/danoniplus/releases/tag/v25.0.0)|・初回実装|

| [<- unStockCategory](dos-h0084-unStockCategory) | **stockForceDel** || [minSpeed ->](dos-h0015-minSpeed) |