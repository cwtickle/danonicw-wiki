**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-h0098-dosNo) | Japanese** 


[Home](./) &gt; [譜面ヘッダー仕様](./dos_header) &gt; [楽曲・譜面情報](./dos_header#-楽曲譜面情報)

| [<- difData](./dos-h0002-difData) | **dosNo** | [musicUrl ->](./dos-h0011-musicUrl) |

## dosNo
- 譜面に割り当てる譜面ファイル番号及び譜面番号の設定  

### 前提条件
- この設定は、html側で譜面分割(externalDosDivide)及び譜面番号固定(externalDosLock)が有効になっている必要があります。
```html
<input type="hidden" name="externalDosDivide" id="externalDosDivide" value="true">
<input type="hidden" name="externalDosLock" id="externalDosLock" value="true">
```

## 使い方
```
|dosNo=1$1$2$2$3|
|dosNo=1,1$1,2$2,1$2,2$3,1|
```

### 説明
- $区切りで譜面毎の設定です。譜面ファイル番号、譜面番号の順にカンマ区切りで指定します。  
なお譜面番号は省略が可能で、その場合は同一譜面ファイル番号の連番が割り当てられます。
- 上記2つの意味は同じ意味で、読み込むファイル名(externalDos)が「dos.txt」なら次の意味になります。
    - 1譜面目：「dos.txt」の1譜面目
    - 2譜面目：「dos.txt」の2譜面目
    - 3譜面目：「dos2.txt」の1譜面目
    - 4譜面目：「dos2.txt」の2譜面目
    - 5譜面目：「dos3.txt」の1譜面目
- ただし、difDataについてはこれまで通り1ファイル目（上記ならdos.txt）に書く必要があります。

|番号|必須|内容|
|----|----|----|
|1|*|譜面ファイル分割時のファイル名の添え字番号 (上記の例なら、「dos3.txt」だとすると「3」を指定)|
|2||1で指定したファイル内の対応する譜面番号|

### 関連項目
- [譜面の作成概要 > 譜面毎のファイル分割設定](./HowtoMake#譜面毎のファイル分割設定)
- [**difData**](dos-h0002-difData) [:pencil:](dos-h0002-difData/_edit) 譜面情報 

### 更新履歴

|Version|変更内容|
|----|----|
|[v34.6.0](https://github.com/cwtickle/danoniplus/releases/tag/v34.6.0)|・初回実装|

| [<- difData](./dos-h0002-difData) | **dosNo** | [musicUrl ->](./dos-h0011-musicUrl) |
