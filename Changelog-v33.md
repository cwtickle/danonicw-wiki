**[English](https://github.com/cwtickle/danoniplus-docs/wiki/Changelog-v33) | Japanese** 

[^ Back to All Version List (Security Policy)](https://github.com/cwtickle/danoniplus/security/policy)  
[^ Back to Update Info](./UpdateInfo#-v33-changelog)

[**<- v34**](./Changelog-v34) | **v33** | [**v32 ->**](./Changelog-v32)  
(**🔖 [21 Releases](https://github.com/cwtickle/danoniplus/releases?q=tag%3Av33)** )

## 🔃 Files changed (v33)

|Directory|FileName||Last Updated|
|----|----|----|----|
|/js|danoni_main.js|[📥](https://github.com/cwtickle/danoniplus/releases/download/v33.7.8/danoni_main.js)|**v33.7.8**|
|/js/lib|danoni_constants.js|[📥](https://github.com/cwtickle/danoniplus/releases/download/v33.6.0/danoni_constants.js)|[v33.6.0](./Changelog-v33#v3360-2023-09-07)|
|/css|danoni_main.css|[📥](https://github.com/cwtickle/danoniplus/releases/download/v33.2.0/danoni_main.css)|[v33.2.0](./Changelog-v33#v3320-2023-08-20)|
|/skin|danoni_skin_default.css<br>danoni_skin_light.css<br>danoni_skin_skyblue.css|[📥](https://github.com/cwtickle/danoniplus/releases/download/v33.0.0/skin_css.zip)|[v33.0.0](./Changelog-v33#v3300-2023-07-29)|

<details>
<summary>Changed file list before v32</summary>

|Directory|FileName||Last Updated|
|----|----|----|----|
|/js/lib|danoni_localbinary.js|[📥](https://github.com/cwtickle/danoniplus/releases/download/v15.1.0/danoni_localbinary.js)|[v15.1.0](./Changelog-v15#v1510-2020-05-21)|
|/img|aaShadow.svg<br>arrow.svg<br>arrowShadow.svg<br>borderline.svg<br>c.svg<br>cursor.svg<br>giko.svg<br>iyo.svg<br>monar.svg<br>morara.svg<br>onigiri.svg|[📥](https://github.com/cwtickle/danoniplus/releases/download/v15.1.0/img.zip)|[v15.1.0](./Changelog-v15#v1510-2020-05-21)|

</details>

⭐ New Features / 🛠️ Improvements / 🐞 Bug Fixes  
( Files changed: ⚪ danoni_main.js / 🔵 danoni_constants.js / 🔴 css files )

## [Warning] 更新終了後の不具合情報
- [更新を終了したバージョンの不具合情報#v33](./DeprecatedVersionBugs#v33) を参照

## v33.7.8 ([2024-04-03](https://github.com/cwtickle/danoniplus/releases/tag/v33.7.8))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v33.7.8/total)](https://github.com/cwtickle/danoniplus/archive/v33.7.8.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v33.7.8/support/v33?style=social)](https://github.com/cwtickle/danoniplus/compare/v33.7.8...support/v33#files_bucket)
- 🐞 **C)** キーコンフィグ画面でオブジェクトが画面外になるとき、キーを押しても画面が見える位置に移動しない問題を修正 ( PR [#1634](https://github.com/cwtickle/danoniplus/pull/1634) ) <- :boom: [**v30.0.1**](./Changelog-v30#v3001-2023-02-11)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v33.7.8)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v33.7.7...v33.7.8#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v33.7.8)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v33.7.8.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v33.7.8.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v33.7.8/js/danoni_main.js)
/ 🎣 [**v35.5.0**](./Changelog-v35#v3550-2024-04-03)

## v33.7.7 ([2024-03-17](https://github.com/cwtickle/danoniplus/releases/tag/v33.7.7))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v33.7.7/total)](https://github.com/cwtickle/danoniplus/archive/v33.7.7.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v33.7.7/support/v33?style=social)](https://github.com/cwtickle/danoniplus/compare/v33.7.7...support/v33#files_bucket)
- 🐞 **C)** フリーズアローの終点と始点が近いとき、キーを押しても次のフリーズアローが反応しないことがある問題を修正 ( PR [#1627](https://github.com/cwtickle/danoniplus/pull/1627), [#1628](https://github.com/cwtickle/danoniplus/pull/1628) ) <- :boom: [**v29.0.1**](./Changelog-v29#v2901-2022-11-05)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v33.7.7)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v33.7.6...v33.7.7#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v33.7.7)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v33.7.7.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v33.7.7.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v33.7.7/js/danoni_main.js)
/ 🎣 [**v35.4.3**](./Changelog-v35#v3543-2024-03-17)

## v33.7.6 ([2024-01-28](https://github.com/cwtickle/danoniplus/releases/tag/v33.7.6))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v33.7.6/total)](https://github.com/cwtickle/danoniplus/archive/v33.7.6.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v33.7.6/support/v33?style=social)](https://github.com/cwtickle/danoniplus/compare/v33.7.6...support/v33#files_bucket)
- 🐞 **C)** 背景矢印でカラーコードを0x始まりにしたときに透明度が適用されない問題を修正 ( PR [#1609](https://github.com/cwtickle/danoniplus/pull/1609) ) <- :boom: [**v33.5.0**](./Changelog-v33#v3350-2023-09-02)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v33.7.6)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v33.7.5...v33.7.6#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v33.7.6)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v33.7.6.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v33.7.6.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v33.7.6/js/danoni_main.js)
/ 🎣 [**v34.7.1**](./Changelog-v34#v3471-2024-01-28)

## v33.7.5 ([2023-11-05](https://github.com/cwtickle/danoniplus/releases/tag/v33.7.5))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v33.7.5/total)](https://github.com/cwtickle/danoniplus/archive/v33.7.5.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v33.7.5/support/v33?style=social)](https://github.com/cwtickle/danoniplus/compare/v33.7.5...support/v33#files_bucket)
- 🛠️ 右シフトキーが環境により動作しない問題を改善 ( PR [#1593](https://github.com/cwtickle/danoniplus/pull/1593) ) <- :boom: [**v16.0.0**](./Changelog-v16#v1600-2020-08-06)
- 🐞 **C)** keyGroupOrderの一部譜面の設定が空の場合、設定の適用先が本来とずれる問題を修正 ( PR [#1591](https://github.com/cwtickle/danoniplus/pull/1591) ) <- :boom: [**v33.5.0**](./Changelog-v33#v3350-2023-09-02)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v33.7.5)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v33.7.3...v33.7.5#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v33.7.5)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v33.7.5.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v33.7.5.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v33.7.5/js/danoni_main.js)
/ 🎣 [**v34.5.1**](./Changelog-v34#v3451-2023-11-05)

## v33.7.3 ([2023-10-29](https://github.com/cwtickle/danoniplus/releases/tag/v33.7.3))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v33.7.3/total)](https://github.com/cwtickle/danoniplus/archive/v33.7.3.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v33.7.3/support/v33?style=social)](https://github.com/cwtickle/danoniplus/compare/v33.7.3...support/v33#files_bucket)
- 🐞 **C)** 色変化が一部のレーンに適用されない問題を修正 ( PR [#1582](https://github.com/cwtickle/danoniplus/pull/1582) ) <- :boom: [**v33.5.0**](./Changelog-v33#v3350-2023-09-02)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v33.7.3)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v33.7.2...v33.7.3#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v33.7.3)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v33.7.3.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v33.7.3.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v33.7.3/js/danoni_main.js)
/ 🎣 [**v34.4.0**](./Changelog-v34#v3440-2023-10-29)

## v33.7.2 ([2023-10-21](https://github.com/cwtickle/danoniplus/releases/tag/v33.7.2))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v33.7.2/total)](https://github.com/cwtickle/danoniplus/archive/v33.7.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v33.7.2/support/v33?style=social)](https://github.com/cwtickle/danoniplus/compare/v33.7.2...support/v33#files_bucket)
- 🐞 **B)** HitPosition設定が機能していない問題を修正 ( PR [#1579](https://github.com/cwtickle/danoniplus/pull/1579) ) <- :boom: [**v31.0.0**](./Changelog-v31#v3100-2023-03-20)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v33.7.2)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v33.7.1...v33.7.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v33.7.2)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v33.7.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v33.7.2.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v33.7.2/js/danoni_main.js)
/ 🎣 [**v34.3.2**](./Changelog-v34#v3432-2023-10-21)

## v33.7.1 ([2023-10-13](https://github.com/cwtickle/danoniplus/releases/tag/v33.7.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v33.7.1/total)](https://github.com/cwtickle/danoniplus/archive/v33.7.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v33.7.1/support/v33?style=social)](https://github.com/cwtickle/danoniplus/compare/v33.7.1...support/v33#files_bucket)
- 🐞 **B)** difDataで指定したノルマが反映されない問題を修正 ( PR [#1575](https://github.com/cwtickle/danoniplus/pull/1575), Issue [#1574](https://github.com/cwtickle/danoniplus/pull/1574) ) <- :boom: [**v32.6.0**](./Changelog-v32#v3260-2023-06-30)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v33.7.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v33.7.0...v33.7.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v33.7.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v33.7.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v33.7.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v33.7.1/js/danoni_main.js)
/ 🎣 [**v34.3.0**](./Changelog-v34#v3430-2023-10-13)

---

## v33.7.0 ([2023-09-08](https://github.com/cwtickle/danoniplus/releases/tag/v33.7.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v33.7.0/total)](https://github.com/cwtickle/danoniplus/archive/v33.7.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v33.7.0/support/v33?style=social)](https://github.com/cwtickle/danoniplus/compare/v33.7.0...support/v33#files_bucket)
- ⭐ 共通設定ファイルに画面位置調整を行う設定を追加 ( PR [#1559](https://github.com/cwtickle/danoniplus/pull/1559) )
- 🐞 **B)** 空のレーンが存在するときに、preblankframeが正しく計算されない問題を修正 ( PR [#1558](https://github.com/cwtickle/danoniplus/pull/1558) ) <- :boom: [**v33.5.0**](./Changelog-v33#v3350-2023-09-02)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v33.7.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v33.6.0...v33.7.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v33.7.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v33.7.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v33.7.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v33.7.0/js/danoni_main.js)

## v33.6.0 ([2023-09-07](https://github.com/cwtickle/danoniplus/releases/tag/v33.6.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v33.6.0/total)](https://github.com/cwtickle/danoniplus/archive/v33.6.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v33.6.0/support/v33?style=social)](https://github.com/cwtickle/danoniplus/compare/v33.6.0...support/v33#files_bucket)
- ⭐ ウィンドウの高さやY座標調整ができる機能を実装 ( PR [#1556](https://github.com/cwtickle/danoniplus/pull/1556) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v33.6.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v33.5.1...v33.6.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v33.6.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v33.6.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v33.6.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v33.6.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v33.6.0/js/lib/danoni_constants.js)<br>❤️ LaV ( @lvenier )

<img src="https://github.com/cwtickle/danoniplus/assets/44026291/2c9eb38a-2f9d-46b4-937a-9522eb6b30d6" width="50%"><img src="https://github.com/cwtickle/danoniplus/assets/44026291/9fb21a72-ea58-4055-a4e0-6836e7cb1f1e" width="50%">

## v33.5.1 ([2023-09-05](https://github.com/cwtickle/danoniplus/releases/tag/v33.5.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v33.5.1/total)](https://github.com/cwtickle/danoniplus/archive/v33.5.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v33.5.1/support/v33?style=social)](https://github.com/cwtickle/danoniplus/compare/v33.5.1...support/v33#files_bucket)
- ⭐ グラデーション記法において、グラデーション外を指定する場合のセパレート文字を `;` から `;;` に変更 ( PR [#1554](https://github.com/cwtickle/danoniplus/pull/1554) )
- 🐞 **C)** グラデーション記法の仕様矛盾により、色名＋透明度の指定ができない問題を修正 ( PR [#1554](https://github.com/cwtickle/danoniplus/pull/1554) ) <- :boom: [**v33.0.0**](./Changelog-v33#v3300-2023-07-29)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v33.5.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v33.5.0...v33.5.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v33.5.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v33.5.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v33.5.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v33.5.1/js/danoni_main.js)

## v33.5.0 ([2023-09-02](https://github.com/cwtickle/danoniplus/releases/tag/v33.5.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v33.5.0/total)](https://github.com/cwtickle/danoniplus/archive/v33.5.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v33.5.0/support/v33?style=social)](https://github.com/cwtickle/danoniplus/compare/v33.5.0...support/v33#files_bucket)
- 🛠️ SNS投稿用リザルト画像のプレビュー機能を追加 ( PR [#1550](https://github.com/cwtickle/danoniplus/pull/1550) )
- 🛠️ コードの整理 ( PR [#1551](https://github.com/cwtickle/danoniplus/pull/1551) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v33.5.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v33.4.1...v33.5.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v33.5.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v33.5.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v33.5.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v33.5.0/js/danoni_main.js)

## v33.4.1 ([2023-08-28](https://github.com/cwtickle/danoniplus/releases/tag/v33.4.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v33.4.1/total)](https://github.com/cwtickle/danoniplus/archive/v33.4.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v33.4.1/support/v33?style=social)](https://github.com/cwtickle/danoniplus/compare/v33.4.1...support/v33#files_bucket)
- 🐞 **B)** ショートカットキーエリア有効時、背景・マスクの表示位置がずれることがある問題を修正 ( PR [#1548](https://github.com/cwtickle/danoniplus/pull/1548) ) <- :boom: [**v33.4.0**](./Changelog-v33#v3340-2023-08-27)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v33.4.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v33.4.0...v33.4.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v33.4.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v33.4.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v33.4.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v33.4.1/js/danoni_main.js)

## v33.4.0 ([2023-08-27](https://github.com/cwtickle/danoniplus/releases/tag/v33.4.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v33.4.0/total)](https://github.com/cwtickle/danoniplus/archive/v33.4.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v33.4.0/support/v33?style=social)](https://github.com/cwtickle/danoniplus/compare/v33.4.0...support/v33#files_bucket)
- ⭐ プレイ画面のショートカットキーが通常と異なる場合の表示とその表示エリアの設定を追加 ( PR [#1544](https://github.com/cwtickle/danoniplus/pull/1544) )
- ⭐ カスタムキー位置の下段略記に対応 ( PR [#1546](https://github.com/cwtickle/danoniplus/pull/1546), Issue [#1545](https://github.com/cwtickle/danoniplus/pull/1545) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v33.4.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v33.3.0...v33.4.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v33.4.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v33.4.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v33.4.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v33.4.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v33.4.0/js/lib/danoni_constants.js)<br>❤️ izkdic

<img src="https://github.com/cwtickle/danoniplus/assets/44026291/8bf2e98d-854f-4052-b51e-0882ba817e8e" width="50%">

## v33.3.0 ([2023-08-22](https://github.com/cwtickle/danoniplus/releases/tag/v33.3.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v33.3.0/total)](https://github.com/cwtickle/danoniplus/archive/v33.3.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v33.3.0/support/v33?style=social)](https://github.com/cwtickle/danoniplus/compare/v33.3.0...support/v33#files_bucket)
- ⭐ デフォルトスキン時、Canvas背景を有効化するかどうかの設定を追加 ( PR [#1539](https://github.com/cwtickle/danoniplus/pull/1539) )
- ⭐ リザルト画像用にカスタム変数を表示する設定を追加 ( PR [#1541](https://github.com/cwtickle/danoniplus/pull/1541) )
- 🛠️ 共通設定ファイルの設定でbooleanで定義すべき変数をstring型からboolean型へ変更 ( PR [#1542](https://github.com/cwtickle/danoniplus/pull/1542) )
- 🐞 **C)** エラー時に表示される音楽ファイルの参照先URLが実際と異なる問題を修正 ( PR [#1540](https://github.com/cwtickle/danoniplus/pull/1540) ) <- :boom: [**v29.4.1**](./Changelog-v29#v2941-2023-01-28)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v33.3.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v33.2.0...v33.3.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v33.3.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v33.3.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v33.3.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v33.3.0/js/danoni_main.js)

## v33.2.0 ([2023-08-20](https://github.com/cwtickle/danoniplus/releases/tag/v33.2.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v33.2.0/total)](https://github.com/cwtickle/danoniplus/archive/v33.2.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v33.2.0/support/v33?style=social)](https://github.com/cwtickle/danoniplus/compare/v33.2.0...support/v33#files_bucket)
- 🛠️ 画面の上下に共通の枠線を一律追加 ( PR [#1537](https://github.com/cwtickle/danoniplus/pull/1537) )
- 🐞 **C)** stockForceDel使用中に除外する検索対象が未定義の場合、エラーになる問題を修正 ( PR [#1536](https://github.com/cwtickle/danoniplus/pull/1536) ) <- :boom: [**v25.0.0**](./Changelog-v25#v2500-2022-01-04)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v33.2.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v33.1.4...v33.2.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v33.2.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v33.2.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v33.2.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v33.2.0/js/danoni_main.js)[🔴](https://github.com/cwtickle/danoniplus/blob/v33.2.0/css/danoni_main.css)<br>❤️ izkdic, MFV2 ( @MFV2 )

## v33.1.4 ([2023-08-20](https://github.com/cwtickle/danoniplus/releases/tag/v33.1.4))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v33.1.4/total)](https://github.com/cwtickle/danoniplus/archive/v33.1.4.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v33.1.4/support/v33?style=social)](https://github.com/cwtickle/danoniplus/compare/v33.1.4...support/v33#files_bucket)
- 🐞 **B)** キー変化でadjustmentやplaybackRateが反映されない問題を修正 ( PR [#1534](https://github.com/cwtickle/danoniplus/pull/1534) ) <- :boom: [**v30.0.1**](./Changelog-v30#v3001-2023-02-11)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v33.1.4)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v33.1.3...v33.1.4#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v33.1.4)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v33.1.4.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v33.1.4.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v33.1.4/js/danoni_main.js)<br>❤️ MFV2 ( @MFV2 )

## v33.1.3 ([2023-08-15](https://github.com/cwtickle/danoniplus/releases/tag/v33.1.3))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v33.1.3/total)](https://github.com/cwtickle/danoniplus/archive/v33.1.3.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v33.1.3/support/v33?style=social)](https://github.com/cwtickle/danoniplus/compare/v33.1.3...support/v33#files_bucket)
- 🐞 **C)** リザルト画像コピー用のテキストのエスケープ文字が戻されていない問題を修正 ( PR [#1532](https://github.com/cwtickle/danoniplus/pull/1532) ) <- :boom: [**v33.1.0**](./Changelog-v33#v3310-2023-08-03)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v33.1.3)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v33.1.2...v33.1.3#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v33.1.3)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v33.1.3.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v33.1.3.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v33.1.3/js/danoni_main.js)<br>❤️ izkdic

## v33.1.2 ([2023-08-14](https://github.com/cwtickle/danoniplus/releases/tag/v33.1.2))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v33.1.2/total)](https://github.com/cwtickle/danoniplus/archive/v33.1.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v33.1.2/support/v33?style=social)](https://github.com/cwtickle/danoniplus/compare/v33.1.2...support/v33#files_bucket)
- 🐞 **B)** フリーズアローの終点と次のフリーズアローの始点が近い場合の判定不具合を修正 ( PR [#1530](https://github.com/cwtickle/danoniplus/pull/1530), Issue [#1529](https://github.com/cwtickle/danoniplus/pull/1529) ) <- :boom: [**v29.0.1**](./Changelog-v29#v2901-2022-11-05)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v33.1.2)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v33.1.1...v33.1.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v33.1.2)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v33.1.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v33.1.2.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v33.1.2/js/danoni_main.js)<br>❤️ ぷろろーぐ ( @prlg25 ), SKB ( @superkuppabros ), 長月

## v33.1.1 ([2023-08-04](https://github.com/cwtickle/danoniplus/releases/tag/v33.1.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v33.1.1/total)](https://github.com/cwtickle/danoniplus/archive/v33.1.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v33.1.1/support/v33?style=social)](https://github.com/cwtickle/danoniplus/compare/v33.1.1...support/v33#files_bucket)
- 🛠️ 判定周りの表示部分においてグラデーション指定を取り止め ( PR [#1526](https://github.com/cwtickle/danoniplus/pull/1526) )
- 🐞 **B)** 背景・マスクモーション、スキン変更においてフレーム計算誤りを修正 ( PR [#1527](https://github.com/cwtickle/danoniplus/pull/1527) ) <- :boom: [**v33.0.0**](./Changelog-v33#v3300-2023-07-29)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v33.1.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v33.1.0...v33.1.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v33.1.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v33.1.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v33.1.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v33.1.1/js/danoni_main.js)[🔴](https://github.com/cwtickle/danoniplus/blob/v33.1.1/css/danoni_main.css)

## v33.1.0 ([2023-08-03](https://github.com/cwtickle/danoniplus/releases/tag/v33.1.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v33.1.0/total)](https://github.com/cwtickle/danoniplus/archive/v33.1.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v33.1.0/support/v33?style=social)](https://github.com/cwtickle/danoniplus/compare/v33.1.0...support/v33#files_bucket)
- ⭐ 結果画面のSNS投稿用画像コピー機能を実装 ( PR [#1521](https://github.com/cwtickle/danoniplus/pull/1521), [#1524](https://github.com/cwtickle/danoniplus/pull/1524) )
- 🐞 **B)** Firefoxでドメイン外のCSSファイルを読み込んだ場合に起動できない問題を修正 ( PR [#1522](https://github.com/cwtickle/danoniplus/pull/1522) ) <- :boom: [**v33.0.0**](./Changelog-v33#v3300-2023-07-29)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v33.1.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v33.0.0...v33.1.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v33.1.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v33.1.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v33.1.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v33.1.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v33.1.0/js/lib/danoni_constants.js)

<img src="https://github.com/cwtickle/danoniplus/assets/44026291/1739b46e-83c1-49ad-8d16-3a363eace872" width="50%"><img src="https://github.com/cwtickle/danoniplus/assets/44026291/4edb2fe1-f494-42d4-95b2-98918a60ec9f" width="50%">

## v33.0.0 ([2023-07-29](https://github.com/cwtickle/danoniplus/releases/tag/v33.0.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v33.0.0/total)](https://github.com/cwtickle/danoniplus/archive/v33.0.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v33.0.0/support/v33?style=social)](https://github.com/cwtickle/danoniplus/compare/v33.0.0...support/v33#files_bucket)
- ⭐ スキンを一時変更できる機能を実装 ( PR [#1519](https://github.com/cwtickle/danoniplus/pull/1519) )
- ⭐ グラデーション指定について、グラデーション以外のものも追加できるよう拡張 ( PR [#1519](https://github.com/cwtickle/danoniplus/pull/1519) )
- ⭐ コンボ表示、Fast/Slow、推定Adj、Excessive表示、譜面明細画面子画面背景、譜面選択子画面背景についてCSSクラスを分離 ( PR [#1519](https://github.com/cwtickle/danoniplus/pull/1519) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v33.0.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v32.7.0...v33.0.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v33.0.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v33.0.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v33.0.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v33.0.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v33.0.0/js/lib/danoni_constants.js)[🔴](https://github.com/cwtickle/danoniplus/blob/v33.0.0/css/danoni_main.css)

<img src="https://github.com/cwtickle/danoniplus/assets/44026291/64cd7cfc-e7f3-42bf-9468-0abb4f801106" width="70%">

[**<- v34**](./Changelog-v34) | **v33** | [**v32 ->**](./Changelog-v32)   
