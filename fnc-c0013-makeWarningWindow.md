[← オブジェクトリファレンスに戻る](ObjectReferenceIndex)  

# makeWarningWindow
### 概要
- 画面上部に常時手前表示の警告用メッセージウィンドウを表示する関数。
- [makeInfoWindow](fnc-c0012-makeInfoWindow)とは常時表示する点と複数追記ができる点が異なる。
- このメッセージウィンドウはver21.1.0までは画面切り替えを行った際（clearWindow呼び出し時）にリセットされる仕様だったが、
ver21.2.0以降は基本保持される仕様に変更された。

#### 仕様補足
- メッセージは [g_errMsgObj](obj-v0026-g_errMsgObj) の画面別プロパティに格納される仕様になっている。  
- メッセージがすでに `g_errMsgObj`の画面別プロパティに存在する場合は、二重登録しない仕組み。
- 第二引数の`resetFlg`を`true`にした場合、この画面別プロパティがクリアされる。（デフォルトは`false`）
- メッセージ枠の縦の長さは、(メッセージ内の`<br>`タグの数＋`g_errMsgObj`の画面別プロパティの配列長 -１)×21px で決まる。  
v24以降は、[range.getClientRects()](https://developer.mozilla.org/en-US/docs/Web/API/Range/getClientRects) が使える場合はそちらを優先する。  
ここで算出された数値が150pxを超える場合は、縦スクロール付きのメッセージ枠になる。

#### 警告メッセージ格納イメージ
```html
<p>
お使いのブラウザは動作保証外です。<br>
Chrome/Opera/Vivaldiなど、WebKit系ブラウザの利用を推奨します。(W-0001)
</p>
<p>
fileスキームでの動作のため、内蔵の画像データを使用します。(W-0011)<br>
imgフォルダ以下の画像の変更は適用されません。
</p>
```

### 引数（括弧内はデフォルト値）

|引数|型|必須|指定内容|
|----|----|----|----|
|_text|string|*|メッセージウィンドウに追記するテキスト|
|----|object||※下記のオブジェクト引数を参照|

### オブジェクト引数
- オブジェクト引数は全て任意。指定が無い場合はデフォルト値が採用される。

|引数|型|デフォルト|用途|
|----|----|----|----|
|resetFlg|boolean|false|メッセージをリセットするかどうかのフラグ<br>(false:リセットしない / true:リセットする)|
|backBtnUse|boolean|false|警告ウィンドウ表示時にBackボタンを表示するか設定<br>(false:表示しない / true:表示する)|

### 返却値
- なし

### 関数の依存関係
- makeWarningWindow
  - setWindowStyle

### 使用例
- 下記の場合、該当する場合は`W_0001`, `W_0011`に対応するメッセージを同時に表示
```javascript
// 非推奨ブラウザに対して警告文を表示
// Firefoxはローカル環境時、Ver65以降矢印が表示されなくなるため非推奨表示
if (g_userAgent.indexOf(`msie`) !== -1 ||
	g_userAgent.indexOf(`trident`) !== -1 ||
	g_userAgent.indexOf(`edge`) !== -1 ||
	(g_userAgent.indexOf(`firefox`) !== -1 && location.href.match(`^file`))) {

	makeWarningWindow(g_msgInfoObj.W_0001);
}

if (location.href.match(/^file/)) {
	makeWarningWindow(g_msgInfoObj.W_0011);
}
```

### 関連項目
- [makeInfoWindow](fnc-c0012-makeInfoWindow)
- setWindowStyle

### 更新履歴

|Version|変更内容|
|----|----|
|[v24.1.0](https://github.com/cwtickle/danoniplus/releases/tag/v24.1.0)|・引数`resetFlg`をオブジェクト引数に追加<br>・オブジェクト引数に`backBtnUse`を追加|
|[v21.2.0](https://github.com/cwtickle/danoniplus/releases/tag/v21.2.0)|・引数`resetFlg`を追加|
|[v16.1.0](https://github.com/cwtickle/danoniplus/releases/tag/v16.1.0)|・makeInfoWindow関数との共通部をsetWindowStyle関数へ切り出し|
|[v1.0.0<br>(v0.59.x)](https://github.com/cwtickle/danoniplus/releases/tag/v1.0.1)|・初回実装|