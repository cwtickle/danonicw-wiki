**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-h0052-readyDelayFrame) | Japanese** 

[Home](./) &gt; [譜面ヘッダー仕様](dos_header) &gt; [プレイ時の初期設定](dos_header#-プレイ時の初期設定) &gt; [画面表示・キーコントロール](dos_header#画面表示キーコントロール)

| [<- excessiveJdgUse](dos-h0093-excessiveJdgUse) || **readyDelayFrame** | [readyAnimationFrame ->](dos-h0073-readyAnimationFrame) |

## readyDelayFrame
- Ready?が表示されるまでの遅延フレーム数の設定

### 使い方
```
|readyDelayFrame=60|
```
### 説明
Ready表示を遅延させるフレーム数を指定します。デフォルトは0フレームです。    

### 関連項目
- [readyAnimationFrame](dos-h0073-readyAnimationFrame) [:pencil:](dos-h0073-readyAnimationFrame/_edit) Ready?のアニメーションフレーム数
- [readyAnimationName](dos-h0074-readyAnimationName) [:pencil:](dos-h0074-readyAnimationName/_edit) Ready?のアニメーション名
- [readyColor](dos-h0075-readyColor) [:pencil:](dos-h0075-readyColor/_edit) Ready?の先頭色設定
- [readyHtml](dos-h0080-readyHtml) [:pencil:](dos-h0080-readyHtml/_edit) Ready?の文字設定
- [displayUse](dos-h0057-displayUse) [:pencil:](dos-h0035-displayUse/_edit) Display項目の利用有無
- [**customJs**](dos-h0019-customjs) [:pencil:](dos-h0019-customjs/_edit) カスタムjsファイルの指定

### 更新履歴

|Version|変更内容|
|----|----|
|[v9.3.0](https://github.com/cwtickle/danoniplus/releases/tag/v9.3.0)|・初回実装|

| [<- excessiveJdgUse](dos-h0093-excessiveJdgUse) || **readyDelayFrame** | [readyAnimationFrame ->](dos-h0073-readyAnimationFrame) |