**[English](https://github.com/cwtickle/danoniplus-docs/wiki/Changelog-v39) | Japanese**

[^ Back to All Version List (Security Policy)](https://github.com/cwtickle/danoniplus/security/policy)  
[^ Back to Update Info](./UpdateInfo#-v39-changelog)

[**<- v40**](./Changelog-latest) | **v39** | [**v38 ->**](./Changelog-v38)  
(**🔖 [17 Releases](https://github.com/cwtickle/danoniplus/releases?q=tag%3Av39)** )

## 🔃 Files changed (v39)

| Directory | FileName            |                                                                                            | Last Updated |
| --------- | ------------------- | ------------------------------------------------------------------------------------------ | ------------ |
| /js       | danoni_main.js      | [📥](https://github.com/cwtickle/danoniplus/releases/download/v39.8.3/danoni_main.js)      | **v39.8.3**  |
| /js/lib   | danoni_constants.js | [📥](https://github.com/cwtickle/danoniplus/releases/download/v39.8.1/danoni_constants.js) | [v39.8.1](./Changelog-v39#v3981-2025-02-22) |
| /skin     | danoni_skin_default.css<br>danoni_skin_light.css<br>danoni_skin_skyblue.css                                                                            | [📥](https://github.com/cwtickle/danoniplus/releases/download/v39.0.0/skin_css.zip)          | [v39.0.0](./Changelog-v39#v3900-2025-02-01) |
| /css      | danoni_main.css                                                                                                                                        | [📥](https://github.com/cwtickle/danoniplus/releases/download/v39.0.0/danoni_main.css)       | [v39.0.0](./Changelog-v39#v3900-2025-02-01) |

<details>
<summary>Changed file list before v38</summary>

| Directory | FileName                                                                                                                                               |                                                                                              | Last Updated                                   |
| --------- | ------------------------------------------------------------------------------------------------------------------------------------------------------ | -------------------------------------------------------------------------------------------- | ---------------------------------------------- |
| /js/lib   | danoni_localbinary.js                                                                                                                                  | [📥](https://github.com/cwtickle/danoniplus/releases/download/v15.1.0/danoni_localbinary.js) | [v15.1.0](./Changelog-v15#v1510-2020-05-21)    |
| /img      | aaShadow.svg<br>arrow.svg<br>arrowShadow.svg<br>borderline.svg<br>c.svg<br>cursor.svg<br>giko.svg<br>iyo.svg<br>monar.svg<br>morara.svg<br>onigiri.svg | [📥](https://github.com/cwtickle/danoniplus/releases/download/v15.1.0/img.zip)               | [v15.1.0](./Changelog-v15#v1510-2020-05-21)    |

</details>

⭐ New Features / 🛠️ Improvements / 🐞 Bug Fixes  
( Files changed: ⚪ danoni_main.js / 🔵 danoni_constants.js / 🔴 css files )

## v39.8.3 ([2025-03-08](https://github.com/cwtickle/danoniplus/releases/tag/v39.8.3))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v39.8.3/total)](https://github.com/cwtickle/danoniplus/archive/v39.8.3.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v39.8.3/support/v39?style=social)](https://github.com/cwtickle/danoniplus/compare/v39.8.3...support/v39#files_bucket)

- 🐞 **C)** キー名が英小文字から始まる場合にData Management画面で止まる問題を修正 ( PR [#1808](https://github.com/cwtickle/danoniplus/pull/1808) ) <- :boom: [**v39.5.0**](./Changelog-v39#v3950-2025-02-15)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v39.8.3)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v39.8.2...v39.8.3#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v39.8.3)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v39.8.3.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v39.8.3.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v39.8.3/js/danoni_main.js)
> / 🎣 [**v40.3.0**](./Changelog-latest#v4030-2025-03-08)

----

## v39.8.2 ([2025-02-24](https://github.com/cwtickle/danoniplus/releases/tag/v39.8.2))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v39.8.2/total)](https://github.com/cwtickle/danoniplus/archive/v39.8.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v39.8.2/support/v39?style=social)](https://github.com/cwtickle/danoniplus/compare/v39.8.2...support/v39#files_bucket)

- 🐞 **C)** キーパターン違いで対象のキーアシストがいない場合、キーパターン変更時に止まることがある問題を修正 ( PR [#1786](https://github.com/cwtickle/danoniplus/pull/1786) ) <- :boom: [**v15.0.0**](./Changelog-v15#v1500-2020-05-13)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v39.8.2)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v39.8.1...v39.8.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v39.8.2)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v39.8.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v39.8.2.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v39.8.2/js/danoni_main.js)

## v39.8.1 ([2025-02-22](https://github.com/cwtickle/danoniplus/releases/tag/v39.8.1))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v39.8.1/total)](https://github.com/cwtickle/danoniplus/archive/v39.8.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v39.8.1/support/v39?style=social)](https://github.com/cwtickle/danoniplus/compare/v39.8.1...support/v39#files_bucket)

- 🛠️ Data Management画面における表示エリア・ボタンの高さ可変対応 ( PR [#1784](https://github.com/cwtickle/danoniplus/pull/1784) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v39.8.1)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v39.8.0...v39.8.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v39.8.1)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v39.8.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v39.8.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v39.8.1/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v39.8.1/js/lib/danoni_constants.js)

## v39.8.0 ([2025-02-21](https://github.com/cwtickle/danoniplus/releases/tag/v39.8.0))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v39.8.0/total)](https://github.com/cwtickle/danoniplus/archive/v39.8.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v39.8.0/support/v39?style=social)](https://github.com/cwtickle/danoniplus/compare/v39.8.0...support/v39#files_bucket)

- ⭐ ローカルストレージ情報のコピー機能を追加 ( PR [#1782](https://github.com/cwtickle/danoniplus/pull/1782) )
- 🛠️ キー別のストレージ情報について、初期値を選択中の譜面から取得するよう変更 ( PR [#1782](https://github.com/cwtickle/danoniplus/pull/1782) )
- 🐞 **C)** 横幅が長いときにData Management画面のボタンが余計に左に表示される問題を修正 ( PR [#1781](https://github.com/cwtickle/danoniplus/pull/1781) ) <- :boom: [**v39.7.0**](./Changelog-v39#v3970-2025-02-20)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v39.8.0)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v39.7.2...v39.8.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v39.8.0)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v39.8.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v39.8.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v39.8.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v39.8.0/js/lib/danoni_constants.js)

## v39.7.2 ([2025-02-20](https://github.com/cwtickle/danoniplus/releases/tag/v39.7.2))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v39.7.2/total)](https://github.com/cwtickle/danoniplus/archive/v39.7.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v39.7.2/support/v39?style=social)](https://github.com/cwtickle/danoniplus/compare/v39.7.2...support/v39#files_bucket)

- 🐞 **C)** Data Management画面のJSON表示で、実際の割り当てキーがカスタムキーの場合に表示されない問題を修正 ( PR [#1777](https://github.com/cwtickle/danoniplus/pull/1777) ) <- :boom: [**v39.7.0**](./Changelog-v39#v3970-2025-02-20)
- 🐞 **C)** Data Management画面のJSON表示でカスタムキーの場合に色データが上下両方に出てしまう問題を修正 ( PR [#1779](https://github.com/cwtickle/danoniplus/pull/1779) ) <- :boom: [**v39.6.0**](./Changelog-v39#v3960-2025-02-16)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v39.7.2)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v39.7.0...v39.7.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v39.7.2)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v39.7.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v39.7.2.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v39.7.2/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v39.7.2/js/lib/danoni_constants.js)

## v39.7.0 ([2025-02-20](https://github.com/cwtickle/danoniplus/releases/tag/v39.7.0))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v39.7.0/total)](https://github.com/cwtickle/danoniplus/archive/v39.7.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v39.7.0/support/v39?style=social)](https://github.com/cwtickle/danoniplus/compare/v39.7.0...support/v39#files_bucket)

- 🛠️ Data Managementのレイアウト見直し ( PR [#1775](https://github.com/cwtickle/danoniplus/pull/1775) )
- 🛠️ Data Management画面のJSON表示でカラーコード部分に実際の色を表示するよう変更 ( PR [#1775](https://github.com/cwtickle/danoniplus/pull/1775) )
- 🛠️ Data Management画面のJSON表示で実際の割り当てキーを併記するよう変更 ( PR [#1775](https://github.com/cwtickle/danoniplus/pull/1775) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v39.7.0)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v39.6.1...v39.7.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v39.7.0)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v39.7.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v39.7.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v39.7.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v39.7.0/js/lib/danoni_constants.js)

## v39.6.1 ([2025-02-17](https://github.com/cwtickle/danoniplus/releases/tag/v39.6.1))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v39.6.1/total)](https://github.com/cwtickle/danoniplus/archive/v39.6.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v39.6.1/support/v39?style=social)](https://github.com/cwtickle/danoniplus/compare/v39.6.1...support/v39#files_bucket)

- 🐞 **C)** キーパターンが別キーモードで、デフォルトの曲中ショートカットが異なる場合に曲中ショートカットキーが変わらない問題を修正 ( PR [#1773](https://github.com/cwtickle/danoniplus/pull/1773) ) <- :boom: [**v4.8.0**](./Changelog-v4#v480-2019-05-10)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v39.6.1)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v39.6.0...v39.6.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v39.6.1)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v39.6.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v39.6.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v39.6.1/js/danoni_main.js)<br>❤️ MFV2 ( @MFV2 )

## v39.6.0 ([2025-02-16](https://github.com/cwtickle/danoniplus/releases/tag/v39.6.0))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v39.6.0/total)](https://github.com/cwtickle/danoniplus/archive/v39.6.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v39.6.0/support/v39?style=social)](https://github.com/cwtickle/danoniplus/compare/v39.6.0...support/v39#files_bucket)

- ⭐ ローカルストレージ情報を使わないセーフモードの実装 ( PR [#1771](https://github.com/cwtickle/danoniplus/pull/1771) )
- 🛠️ Data Management画面からの「Back」ボタンの移動先の見直し ( PR [#1771](https://github.com/cwtickle/danoniplus/pull/1771) )
- 🛠️ JSONデータをフォーマットする関数をグローバル関数へ変更 ( PR [#1771](https://github.com/cwtickle/danoniplus/pull/1771) )
- 🛠️ getFontsize関数で文字列が複数行にわたるときのロジックを変更 ( PR [#1771](https://github.com/cwtickle/danoniplus/pull/1771) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v39.6.0)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v39.5.0...v39.6.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v39.6.0)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v39.6.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v39.6.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v39.6.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v39.6.0/js/lib/danoni_constants.js)

<img src="https://github.com/user-attachments/assets/6b52dbe2-6eb4-4b07-8ed6-b7a0c912fa36" width="50%"><img src="https://github.com/user-attachments/assets/a3d70efb-a61d-4d6e-a02c-2ea3cac18d04" width="50%">

## v39.5.0 ([2025-02-15](https://github.com/cwtickle/danoniplus/releases/tag/v39.5.0))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v39.5.0/total)](https://github.com/cwtickle/danoniplus/archive/v39.5.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v39.5.0/support/v39?style=social)](https://github.com/cwtickle/danoniplus/compare/v39.5.0...support/v39#files_bucket)

- ⭐ ローカルストレージのデータを管理する画面を実装 ( PR [#1769](https://github.com/cwtickle/danoniplus/pull/1769) )
- 🛠️ ローカルストレージのパース処理を共通化 ( PR [#1769](https://github.com/cwtickle/danoniplus/pull/1769) )
- 🛠️ Ex-Settings画面での画面共通系の定義を追加 ( PR [#1769](https://github.com/cwtickle/danoniplus/pull/1769) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v39.5.0)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v39.4.3...v39.5.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v39.5.0)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v39.5.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v39.5.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v39.5.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v39.5.0/js/lib/danoni_constants.js)

## v39.4.3 ([2025-02-13](https://github.com/cwtickle/danoniplus/releases/tag/v39.4.3))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v39.4.3/total)](https://github.com/cwtickle/danoniplus/archive/v39.4.3.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v39.4.3/support/v39?style=social)](https://github.com/cwtickle/danoniplus/compare/v39.4.3...support/v39#files_bucket)

- 🛠️ スライダー処理を共通化、ラベル作成時にpointer-eventsの値をautoにする属性を追加 ( PR [#1766](https://github.com/cwtickle/danoniplus/pull/1766), [#1767](https://github.com/cwtickle/danoniplus/pull/1767) )
- 🐞 **B)** フェードイン、Appearanceのフィルター位置が動作しない問題を修正 ( PR [#1766](https://github.com/cwtickle/danoniplus/pull/1766) ) <- :boom: [**v39.4.1**](./Changelog-v39#v3941-2025-02-11)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v39.4.3)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v39.4.2...v39.4.3#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v39.4.3)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v39.4.3.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v39.4.3.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v39.4.3/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v39.4.3/js/lib/danoni_constants.js)

## v39.4.2 ([2025-02-12](https://github.com/cwtickle/danoniplus/releases/tag/v39.4.2))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v39.4.2/total)](https://github.com/cwtickle/danoniplus/archive/v39.4.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v39.4.2/support/v39?style=social)](https://github.com/cwtickle/danoniplus/compare/v39.4.2...support/v39#files_bucket)

- 🐞 **C)** CSSのoverflow属性にautoを適用しているオブジェクトが動かない問題を修正 ( PR [#1764](https://github.com/cwtickle/danoniplus/pull/1764) ) <- :boom: [**v39.4.1**](./Changelog-v39#v3941-2025-02-11)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v39.4.2)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v39.4.1...v39.4.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v39.4.2)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v39.4.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v39.4.2.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v39.4.2/js/danoni_main.js)

## v39.4.1 ([2025-02-11](https://github.com/cwtickle/danoniplus/releases/tag/v39.4.1))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v39.4.1/total)](https://github.com/cwtickle/danoniplus/archive/v39.4.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v39.4.1/support/v39?style=social)](https://github.com/cwtickle/danoniplus/compare/v39.4.1...support/v39#files_bucket)

- 🛠️ CSSのpointer-events属性のデフォルト値を種別ごとに設定するよう変更 ( PR [#1762](https://github.com/cwtickle/danoniplus/pull/1762) )
- 🛠️ コードの整理 ( PR [#1761](https://github.com/cwtickle/danoniplus/pull/1761) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v39.4.1)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v39.4.0...v39.4.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v39.4.1)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v39.4.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v39.4.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v39.4.1/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v39.4.1/js/lib/danoni_constants.js)

## v39.4.0 ([2025-02-10](https://github.com/cwtickle/danoniplus/releases/tag/v39.4.0))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v39.4.0/total)](https://github.com/cwtickle/danoniplus/archive/v39.4.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v39.4.0/support/v39?style=social)](https://github.com/cwtickle/danoniplus/compare/v39.4.0...support/v39#files_bucket)

- 🛠️ X, Y座標の管理関数を分離 ( PR [#1756](https://github.com/cwtickle/danoniplus/pull/1756), [#1757](https://github.com/cwtickle/danoniplus/pull/1757) )
- 🛠️ 設定分岐部分をObjectからMapに変更 ( PR [#1758](https://github.com/cwtickle/danoniplus/pull/1758) )
- 🛠️ Shaking設定、プレイ画面の主要オブジェクトについてX, Y座標の管理関数を使うよう変更 ( PR [#1758](https://github.com/cwtickle/danoniplus/pull/1758) )
- 🛠️ プレイ画面のPlayWindow, Effect処理をカスタムJSの前に変更 ( PR [#1759](https://github.com/cwtickle/danoniplus/pull/1759) )
- 🛠️ AutoRetryを行う関数の引数を数字から設定名に変更 ( PR [#1759](https://github.com/cwtickle/danoniplus/pull/1759) )
- 🐞 **C)** Shakingで座標位置を直接書き換えていることで、左端が0pxで無いときにDrunkが機能しない問題を修正 ( PR [#1758](https://github.com/cwtickle/danoniplus/pull/1758) ) <- :boom: [**v39.0.0**](./Changelog-v39#v3900-2025-02-01)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v39.4.0)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v39.3.0...v39.4.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v39.4.0)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v39.4.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v39.4.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v39.4.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v39.4.0/js/lib/danoni_constants.js)

## v39.3.0 ([2025-02-09](https://github.com/cwtickle/danoniplus/releases/tag/v39.3.0))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v39.3.0/total)](https://github.com/cwtickle/danoniplus/archive/v39.3.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v39.3.0/support/v39?style=social)](https://github.com/cwtickle/danoniplus/compare/v39.3.0...support/v39#files_bucket)

- ⭐ StepArea設定に 2Step を追加 ( PR [#1752](https://github.com/cwtickle/danoniplus/pull/1752) )
- ⭐ StepArea設定の Mismatched, R-Mismatched についてスクロール方向がすべて同じとき、一部ステップを段違いにするよう変更 ( PR [#1752](https://github.com/cwtickle/danoniplus/pull/1752) )
- 🛠️ X, Y座標の管理用関数を作成 ( PR [#1752](https://github.com/cwtickle/danoniplus/pull/1752), [#1753](https://github.com/cwtickle/danoniplus/pull/1753), [#1754](https://github.com/cwtickle/danoniplus/pull/1754) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v39.3.0)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v39.2.0...v39.3.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v39.3.0)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v39.3.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v39.3.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v39.3.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v39.3.0/js/lib/danoni_constants.js)

## v39.2.0 ([2025-02-08](https://github.com/cwtickle/danoniplus/releases/tag/v39.2.0))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v39.2.0/total)](https://github.com/cwtickle/danoniplus/archive/v39.2.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v39.2.0/support/v39?style=social)](https://github.com/cwtickle/danoniplus/compare/v39.2.0...support/v39#files_bucket)

- ⭐ Appearance用のフィルターを階層別に定義するよう変更 ( PR [#1750](https://github.com/cwtickle/danoniplus/pull/1750) )
- 🛠️ StepArea: Mismatched, R-Mismatched, X-Flowerについて、mainSprite*N*を回転軸にするよう変更 ( PR [#1750](https://github.com/cwtickle/danoniplus/pull/1750) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v39.2.0)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v39.1.0...v39.2.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v39.2.0)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v39.2.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v39.2.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v39.2.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v39.2.0/js/lib/danoni_constants.js)

## v39.1.0 ([2025-02-04](https://github.com/cwtickle/danoniplus/releases/tag/v39.1.0))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v39.1.0/total)](https://github.com/cwtickle/danoniplus/archive/v39.1.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v39.1.0/support/v39?style=social)](https://github.com/cwtickle/danoniplus/compare/v39.1.0...support/v39#files_bucket)

- ⭐ StepArea: Halfway, Mismatched, R-Mismatchedについて階層の数によらず適用するように変更 ( PR [#1748](https://github.com/cwtickle/danoniplus/pull/1748) )
- ⭐ スクロール反転時のStepArea設定の制限を撤廃 ( PR [#1748](https://github.com/cwtickle/danoniplus/pull/1748) )
- ⭐ カスタムキー定義にレーン別の割当階層と階層別のCSS Transition定義を追加 ( PR [#1748](https://github.com/cwtickle/danoniplus/pull/1748) )
- ⭐ スクロール反転（scrollch_data）データを拡張し、別階層への追加に対応 ( PR [#1748](https://github.com/cwtickle/danoniplus/pull/1748) )
- 🛠️ transform管理用の変数を追加 ( PR [#1748](https://github.com/cwtickle/danoniplus/pull/1748) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v39.1.0)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v39.0.0...v39.1.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v39.1.0)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v39.1.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v39.1.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v39.1.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v39.1.0/js/lib/danoni_constants.js)

## v39.0.0 ([2025-02-01](https://github.com/cwtickle/danoniplus/releases/tag/v39.0.0))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v39.0.0/total)](https://github.com/cwtickle/danoniplus/archive/v39.0.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v39.0.0/support/v39?style=social)](https://github.com/cwtickle/danoniplus/compare/v39.0.0...support/v39#files_bucket)

- ⭐ Ex-Settings画面を追加 ( PR [#1746](https://github.com/cwtickle/danoniplus/pull/1746) )
- ⭐ PlayWindow, StepArea, FrzReturn, Shaking, Effect, Camoufrage, Swapping, JudgRange, AutoRetry機能を追加 ( PR [#1746](https://github.com/cwtickle/danoniplus/pull/1746) )
- ⭐ プレイ画面の makeArrow / makeFrzArrow 関数に対してカスタムJSを追加 ( PR [#1746](https://github.com/cwtickle/danoniplus/pull/1746) )
- 🛠️ フリーズアローの階層構造を一部変更 ( PR [#1746](https://github.com/cwtickle/danoniplus/pull/1746) )
- 🛠️ プレイ画面の階層構造を変更、多層化に対応 ( PR [#1746](https://github.com/cwtickle/danoniplus/pull/1746) )
- 🛠️ 結果画面に表示される設定名（PlayStyle）について全体に収まるようにレイアウトを見直し ( PR [#1746](https://github.com/cwtickle/danoniplus/pull/1746) )
- 🛠️ ImgTypeが「Note」のとき、タイトル画面の矢印が左向きになるのを見直し ( PR [#1746](https://github.com/cwtickle/danoniplus/pull/1746) )
- 🛠️ 設定項目別のラベル部分の横幅を100pxから110pxに変更 ( PR [#1746](https://github.com/cwtickle/danoniplus/pull/1746) )
- 🐞 **C)** HitPosition有効時にHidden+/Sudden+のフィルター位置がずれる問題を修正 ( PR [#1746](https://github.com/cwtickle/danoniplus/pull/1746) ) <- :boom: [**v31.0.0**](./Changelog-v31#v3100-2023-03-20)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v39.0.0)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v38.3.1...v39.0.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v39.0.0)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v39.0.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v39.0.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v39.0.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v39.0.0/js/lib/danoni_constants.js)[🔴](https://github.com/cwtickle/danoniplus/blob/v39.0.0/css/danoni_main.css)

<img src="https://github.com/user-attachments/assets/e400e4e7-9208-4bd0-84f4-27d22be2d54d" width="60%">

<img src="https://github.com/user-attachments/assets/91b0f3de-f6e8-400c-a177-6c6e2344819c" width="50%"><img src="https://github.com/user-attachments/assets/bfb9138f-24bc-4a36-938a-72a845e148fc" width="50%">

[**<- v40**](./Changelog-latest) | **v39** | [**v38 ->**](./Changelog-v38)
