**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-h0010-playbackRate) | Japanese** 

[Home](./) &gt; [譜面ヘッダー仕様](dos_header) &gt; [プレイ時間制御・譜面位置調整](dos_header#-プレイ時間制御譜面位置調整)

| [<- endFrame](dos-h0009-adjustment) | **playbackRate** | [unStockCategory ->](dos-h0084-unStockCategory) |

## playbackRate
- 楽曲再生速度（主にテストプレイ用）の設定

### 使い方
```
|playbackRate=0.5|
```
### 説明
この値を指定することで、楽曲の再生速度を変更できます。  
デフォルトは1 (1倍)。楽曲の再生速度に合わせ、譜面も自動的に修正されます。    
※danoni_custom.js等でタイミングを別途調整している場合、ずれることがあります。  

### 関連項目
- [**musicUrl**](dos-h0011-musicUrl) [:pencil:](dos-h0011-musicUrl/_edit) 楽曲ファイル名
- [**adjustment**](dos-h0009-adjustment) [:pencil:](dos-h0009-adjustment/_edit) 譜面位置の初期調整

### 更新履歴

|Version|変更内容|
|----|----|
|[v2.8.0](https://github.com/cwtickle/danoniplus/releases/tag/v2.8.0)|・初回実装|

| [<- endFrame](dos-h0009-adjustment) | **playbackRate** | [unStockCategory ->](dos-h0084-unStockCategory) |