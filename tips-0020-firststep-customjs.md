**[English](https://github.com/cwtickle/danoniplus-docs/wiki/tips-0020-firststep-customjs) | Japanese**

[^ Tips Indexに戻る](./tips-index)

| < [ローカルでのプレイ方法(補足)](./HowToLocalPlay-Appendix) || **カスタムJS事始め** | [オブジェクトの階層とラベル・ボタンの挿入](./tips-0021-object-layer) > |

# カスタムJS事始め

## カスタムJS (スキンJS)とは

- カスタムJS (danoni_custom.js) とは、Dancing☆Onigiri (CW Edition)における拡張スクリプトファイルです。
- 既存の画面に処理を割り込ませて使います。
- JavaScriptを使って記述します。標準の他、CW Edition本体で定義している共通関数群が使用できます。
- 従来の譜面ヘッダーでは定義が難しい機能でも実装が可能です。（一部制約あり）

### 利用例

- [キリズマ](https://cw7.sakura.ne.jp/danoni/2022/0282K_noapusa.html) ( [GitHubリポジトリ](https://github.com/cwtickle/kirizma-cw) )
- [Punching◇Panels](https://cw7.sakura.ne.jp/danoni/2022/0341P_ChanceTime.html) ( [GitHubリポジトリ](https://github.com/cwtickle/punching-panels) )
- [ボタン配置を変えたり非表示にしている例](https://cw7.sakura.ne.jp/danoni/2020/0001_Positive3rdShort_ev.html)
- [スコアドラムロール (プレイ画面右下)](https://cw7.sakura.ne.jp/danoni/2013/0237_Cllema.html)

### メリット

- 本体ソースを変えることなく記述が可能
- 既存機能にない機能の実装が可能
- 画面外（ページ背景）との連動も可能

### デメリット

- 本体ソースのバージョンアップにより、動作しなくなる可能性あり
  - 主にメジャーバージョンアップを行った場合。
  - 想定される主な影響については[アップグレードガイド](./MigrationGuide)を確認する必要があります。

## 準備するもの

- JavaScriptを記述するので、対応するテキストエディターがあるとよいです。
- [Visual Studio Code](https://code.visualstudio.com/) がおすすめです。
- [オブジェクト構成・ボタン](./DOM) / [ID一覧](./IdReferenceIndex) : 既存のオブジェクトの関係性を記載したページです。

## 基本的な使い方

- jsファイルを新たに作成し、「js」フォルダに格納します。
- 譜面ヘッダーに次を指定します。
```
|customjs=作成したファイル名.js|
```
- 差し込みたい箇所を[g_customJsObjのプロパティ](./AboutCustomFunction#g_customjsobj)から探します。
- タイトル画面で呼び出したい場合は「title (タイトル画面（初期）表示後)」なので、下記のように記述します。

```javascript
g_customJsObj.title.push(() => {
    // ここに処理を記述
});
```

### 具体例1：設定画面のタイトルにモーションをつける

- 下記は、既存のラベルに対してアニメーションをつける例です。
- 既存ラベルのidは画面要素別に定義されており、ブラウザで確認できるほかに[ID一覧](./IdReferenceIndex)で確認することができます。

```javascript
g_customJsObj.option.push(() => {

    // タイトルを示すid: lblTitleに対して追加のモーションを設定
    // 1.5秒のアニメーションで上から下へ移動するアニメーションをつける
    // 本来は「upToDown」という名前のCSSアニメーションを別途準備する必要があるが、既存でついているためそのまま使える
    const lblTitle = document.getElementById(`lblTitle`);
    lblTitle.style.animationDuration = `1.5s`;
    lblTitle.style.animationName = `upToDown`;

});
```

### 具体例2: カスタムラベルを作る

- [createDivCss2Label](./fnc-c0002-createDivCss2Label) 関数を使ってラベルが作成できます。
- 下記は、タイトル画面に任意のラベルを作成して表示する例です。
- idの名前は、本体と被らないように名前の先頭に「local_」「308_」（作品番号）など区別できる文字をつけると良いです。

```javascript
g_customJsObj.title.push(() => {

    // id: local_lblName という名前のラベルを作成
    // 第二引数はラベルに表示する文字、第三引数はラベルの位置・サイズの情報を表す
    // ここでは x: 0px, y: 350px, 横幅: 画面横サイズ(g_sWidth), 縦: 30pxのラベルを作る
    const label = createDivCss2Label(`local_lblName`, `ラベルテスト用。ここに書いた文字が表示されます`, {
        x: 0, y: 350, w: g_sWidth, h: 30, siz: 14, align: C_ALIGN_LEFT, color: `#cccccc`, 
    });

    // 追加したいオブジェクトに対して上に格納した変数を追加するとラベルが表示される
    divRoot.appendChild(label);

    // 作成したラベルは document.getElementById で呼び出せます
    const newLabel = document.getElementById(`local_lblName`);
});
```

### 具体例3: カスタムボタンを作る

- [createCss2Button](./fnc-c0004-createCss2Button) 関数を使ってボタンが作成できます。
- 下記は、タイトル画面に任意のボタンを作成して表示する例です。

```javascript
g_customJsObj.title.push(() => {

    // id: local_btnInfo という名前のボタンを作成
    // 第二引数に処理する関数、第三引数はボタンの位置・サイズ、付属処理を表す
    // ここでは x: 画面横サイズの3/4の位置, y: 340px, 横幅: 画面横サイズの1/4 のボタンを作る

    // このボタンは別のカスタム画面へ移動するときに使う想定のボタン
    // clearTimeout(g_timeoutEvtTitleId)はタイトル画面内のループ処理を止めるために必要
    // customCommentInit() は次に処理する関数で、別に定義が必要

    const btn = createCss2Button(`local_btnInfo`, `Info`, _ => clearTimeout(g_timeoutEvtTitleId), {
        x: g_sWidth / 4 * 3, y: 340, w: g_sWidth / 4,
        resetFunc: _ => customCommentInit(),
    }, g_cssObj.button_Tweet);

    // 追加したいオブジェクトに対して上に格納した変数を追加するとボタンが表示される
    divRoot.appendChild(btn);

    // 作成したボタンは document.getElementById で呼び出せます
    const newButton = document.getElementById(`local_btnInfo`);
});
```

### 具体例4: カスタム譜面ヘッダーを作る

- 独自の譜面ヘッダーもカスタムJSで作れます。
- `|変数=設定名|`の形式で定義された変数は、すべて`g_rootObj.変数`で取得できるのでそれを利用します。
- 譜面ヘッダー名は独自であることが分かるように名前の先頭に「local_」など区別できる文字をつけると良いです。

#### 譜面側の設定

```
|local_workNo=123|
```

#### カスタムJSの記述

```javascript
g_customJsObj.title.push(() => {

    // 譜面側で定義したカスタム変数を格納する処理
    // 指定されない場合を見越して、未定義(undefined)かどうかを確認すると良い
    let workNo = ``;
    if (g_rootObj.local_workNo !== undefined) {
        workNo = g_rootObj.local_workNo;
    }

});
```

### 具体例5: プレイ中の特定時間で処理を行う

- プレイ中の特定時間に処理を行うことでちょっとしたギミックを入れることができます。
- プレイ中のフレーム数は`g_scoreObj.baseFrame`で取得できるので、この変数を利用します。
- プレイ中のループ処理を扱う「mainEnterFrame」に定義します。

```javascript
g_customJsObj.mainEnterFrame.push(() => {

    if (g_scoreObj.baseFrame === 1000) {
        // 1000フレームで矢印・フリーズアローエリアの半分を非表示にする
        $id(`arrowSprite0`).display = `none`;

    } else if (g_scoreObj.baseFrame === 1050) {
        // 1050フレームで元に戻す
        $id(`arrowSprite0`).display = `inherit`;
    }

});
```

## ページ作成者
- ティックル

[^ Tips Indexに戻る](./tips-index)

| < [ローカルでのプレイ方法(補足)](./HowToLocalPlay-Appendix) || **カスタムJS事始め** | [オブジェクトの階層とラベル・ボタンの挿入](./tips-0021-object-layer) > |
