**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-e0003-wordData) | Japanese** 

[↑ 譜面エフェクト仕様に戻る](dos_effect)

| [< 色変化](dos-e0002-ncolorData) | **歌詞表示** | [背景・マスクモーション >](dos-e0004-animationData) |

## 歌詞表示 ( word_data )
### 概要
- ステップゾーンの上下に表示するテキスト（歌詞など）を設定します。

<img src="https://user-images.githubusercontent.com/44026291/216037271-3668db67-3f62-4bd7-b297-a78bd39c5797.png" width="70%">

### 使い方
```
|word_data=
1436,0,[fadein]
1436,0,　　♪ 降りしきる流星の雨
1638,0,　　♪ 鮮やかに染まるビジョンに
1852,0,　　♪ 瞬く閃光　気づかないまま
2028,0,　　♪ 無限の海に溺れるだけ
2242,0,[fadeout]
9000,0,[center]
9000,0,Thank You For Playing!
9120,0,[fadeout],120
|
```
- 3～4つで1セット。改行区切りです。  
ParaFla版と比較して、フェードイン・アウトなどの機能が追加されています。  


#### 1. 通常表記

|番号|論理名|設定例|内容|
|----|----|----|----|
|1|Frame|1436|変更するタイミングのフレーム数を指定します。|
|2|Position|0|歌詞表示する位置。<br>"0","2", ..(偶数)が上段、"1","3", ..(奇数)が下段です。|
|3|Lyrics|♪ 歌詞を表示|歌詞表示。文字色などのタグが利用できます。<br>カンマなどの特殊文字を使用する際は、[譜面データにおける特殊文字の取り扱い](./SpecialCharacters)に従って文字を置き換える必要があります。|

- 歌詞表示部分のカンマについては、ver34.1.0以降は例外的に「カンマ+半角スペース」がある場合で
歌詞表示中にカンマの間に数字単独になることが無い限り、区切り文字として扱わないようにしました。
```
8945,0,They search my face in a silky voice, <br>but they*squo*re scared of me at the bottom of their heart.
9097,0,It makes me annoyed tremendously...<br>Their behavior disgusts me !
```

#### 2. 歌詞変化(フェードイン・アウト、表示位置変更)

|番号|論理名|設定例|内容|
|----|----|----|----|
|1|Frame|1436|変更するタイミングのフレーム数を指定します。|
|2|Position|0|歌詞表示を制御する位置。<br>"0","2", ..(偶数)が上段、"1","3", ..(奇数)が下段です。|
|3|Fadein/Out|[fadein]|キーワード指定。<br>[fadein] 歌詞フェードイン(徐々に表示)<br>[fadeout] 歌詞フェードアウト(徐々に非表示)<br>[left] 歌詞表示を左揃え<br>[center] 歌詞表示を中央揃え<br>[right] 歌詞表示を右揃え<br>[fontSize=XX] フォントサイズをXXpxに変更|
|4|FadeFrame|60|歌詞フェードイン・アウトするフレーム数を指定。<br>キーワードに[fadein][fadeout]が指定された場合のみ有効。<br>デフォルトは30フレーム。|

#### 3. コメント

|番号|論理名|設定例|内容|
|----|----|----|----|
|1|Frame|200|フレーム数指定。指定は任意です。|
|2|Position|-|ハイフン固定。|
|3|Comment|＜コメント入力＞|コメント文を自由に入力できます。|

#### 4. 他設定の流用

```
|wordCross_data=wordRev_data|  // Scroll:Cross時の歌詞表示はReverse時の歌詞表示を流用
```

|番号|論理名|設定例|内容|
|----|----|----|----|
|1|Data Name|wordRev2_data|コピー元の歌詞表示設定名を指定します。|

### データ名の種類
- 使う場面の違いだけで、使い方は同じです。  
リバース用はReverse専用です。指定が無ければ、通常のものが使用されます。  
Cross, Split 他についてはScrollが`---`以外が指定された場合（通常、ステップゾーンが上下両方にある場合）のものです。
- wordA_data, wordRevA_dataのように`A`がついているものは[別キーモード](./AnotherKeysMode)用です。

|画面|歌詞表示<br>(言語未指定)|歌詞表示<br>(日本語)|歌詞表示<br>(英語)|
|----|----|----|----|
|メイン(通常)|word_data, <br>word2_data, <br>word**A**_data,<br>word**A**2_data, <br>...|word**Ja**_data, <br>word**Ja**2_data, <br>word**A**Ja_data,<br>word**A**Ja2_data, <br>...|word**En**_data, <br>word**En**2_data, <br>word**A**En_data,<br>word**A**En2_data, <br>...|
|メイン(リバース)|wordRev_data, <br>wordRev2_data, <br>...|wordRev**Ja**_data, <br>wordRev**Ja**2_data, <br>...|wordRev**En**_data, <br>wordRev**En**2_data, <br>...|
|メイン(Cross, Split 他)|wordAlt_data, <br>wordAlt2_data, <br>...<br>wordCross_data, <br>wordFlat_data, <br>...|wordAlt**Ja**_data, <br>wordAlt**Ja**2_data, <br>...<br>wordCross**Ja**_data, <br>wordFlat**Ja**_data, <br>...|wordAlt**En**_data, <br>wordAlt**En**2_data, <br>...<br>wordCross**En**_data, <br>wordFlat**En**_data, <br>...|

例えば 2譜面目リバース時については以下の順で適用されます。

|適用順|適用モーション|意味|
|----|----|----|
|1|wordRevJa2_data / wordRevEn2_data|歌詞表示(リバース時)の２譜面目 ※言語指定あり|
|2|wordRevJa_data / wordRevEn_data|歌詞表示(リバース時)の１譜面目 ※言語指定あり|
|3|wordRev2_data|歌詞表示(リバース時)の２譜面目|
|4|wordRev_data|歌詞表示(リバース時)の１譜面目|
|5|wordJa2_data / wordEn2_data|歌詞表示(通常時)の２譜面目 ※言語指定あり|
|6|wordJa_data / wordEn_data|歌詞表示(通常時)の１譜面目 ※言語指定あり|
|7|word2_data|歌詞表示(通常時)の２譜面目|
|8|word_data|歌詞表示(通常時)の１譜面目|

### 補足
- ver15.3.0より、次の条件を全て満たした場合のみ、歌詞表示の上下を反転します。  
この制御設定は、[譜面ヘッダーやdanoni_setting.js](dos-h0069-wordAutoReverse)にて設定が可能です。
  - 上下スクロールを挟まないキーに限定（5key, 7key, 7ikey, 9A/9Bkeyなど）
  - リバース・スクロール拡張用の歌詞表示（wordRev_data / wordAlt_data）が設定されていない作品
  - SETTINGS 画面で Reverse：ON、Scroll：--- (指定なし) を指定してプレイ開始した場合
  - 歌詞表示がすべて1段表示の場合（2段の場合、反転すると文字が重なる場合があるため）

### 補足2
- ver34.0.0より、キー別・パターン別の指定が可能になりました。
```
|transKey9A=$$9B$9C|

|word_data=...|         // 全体共通
|word2_data=...|        // 2譜面目共通
|wordEn2_data=...|      // 2譜面目、英語モードのみ適用
|wordAEn2_data=...|     // 2譜面目、英語モード、別キーモード(9B, 9C)のみ適用
|word<9C>En2_data=...|  // 2譜面目、英語モード、別キーモード(9C)のみ適用
|word<9A>En2_data=...|  // 2譜面目、英語モード、別キーモード外のみ適用
|word[2]En2_data=...|   // 2譜面目、英語モード、キーパターン:2のみ適用
```

### 関連項目
- [displayUse](dos-h0057-displayUse) [:pencil:](dos-h0035-displayUse/_edit) Display項目の利用有無
- [wordAutoReverse](dos-h0069-wordAutoReverse) [:pencil:](dos-h0069-wordAutoReverse/_edit) Reverse時に歌詞表示を条件付きで反転させる設定
- [bottomWordSet](dos-h0059-bottomWordSet) [:pencil:](dos-h0059-bottomWordSet/_edit) 下側の歌詞表示位置をステップゾーン位置に連動させる設定
- [背景・マスクモーション (back_data, mask_data)](dos-e0004-animationData) [:pencil:](dos-e0004-animationData/_edit) 
- [譜面データにおける特殊文字の取り扱い](./SpecialCharacters)

### 更新履歴

|Version|変更内容|
|----|----|
|[v36.0.0](https://github.com/cwtickle/danoniplus/releases/tag/v36.0.0)|・タブとカンマ前後にある半角スペースを自動除去するよう変更|
|[v34.1.0](https://github.com/cwtickle/danoniplus/releases/tag/v34.1.0)|・カンマ＋半角スペースの組合せで歌詞表示箇所のみ区切り文字としてではなく通常のカンマが使えるよう変更|
|[v34.0.0](https://github.com/cwtickle/danoniplus/releases/tag/v34.0.0)|・他譜面のデータを参照しつつ、個別の歌詞表示を組み合わせる機能を追加<br>・キー別、キーパターン別の歌詞表示設定を実装|
|[v31.2.0](https://github.com/cwtickle/danoniplus/releases/tag/v31.2.0)|・他譜面の`word_data`名を指定することで他譜面のデータを参照する機能を追加|
|[v30.2.0](https://github.com/cwtickle/danoniplus/releases/tag/v30.2.0)|・スクロール別の歌詞表示に対応 (wordCross_data, wordSplit_data など)|
|[v30.0.1](https://github.com/cwtickle/danoniplus/releases/tag/v30.0.1)|・歌詞表示の別キーモード用記述対応|
|[v24.2.1](https://github.com/cwtickle/danoniplus/releases/tag/v24.2.1)|・歌詞表示の多言語対応|
|[v15.5.0](https://github.com/cwtickle/danoniplus/releases/tag/v15.5.0)|・歌詞表示のフォントサイズ変更に対応|
|[v15.3.0](https://github.com/cwtickle/danoniplus/releases/tag/v15.3.0)|・Reverse時に条件付きで歌詞を上下逆の位置に表示するよう変更|
|[v15.1.0](https://github.com/cwtickle/danoniplus/releases/tag/v15.1.0)|・フレーム数、深度において数式記法に対応|
|[v11.1.2](https://github.com/cwtickle/danoniplus/releases/tag/v11.1.2)|・下側の歌詞表示位置をステップゾーン位置に追随させる設定を追加|
|[v10.2.1](https://github.com/cwtickle/danoniplus/releases/tag/v10.2.1)|・スクロール拡張設定用歌詞表示(wordAlt_data)の実装|
|[v9.3.0](https://github.com/cwtickle/danoniplus/releases/tag/v9.3.0)|・リバース用歌詞表示(wordRev_data)の実装|
|[v7.9.0](https://github.com/cwtickle/danoniplus/releases/tag/v7.9.0)|・コメント行を実装|
|[v7.7.0](https://github.com/cwtickle/danoniplus/releases/tag/v7.7.0)|・同一フレームの複数同時描画に対応|
|[v5.6.0](https://github.com/cwtickle/danoniplus/releases/tag/v5.6.0)|・セット毎改行区切りに限り、歌詞フェードイン・アウトするフレーム数を<br>指定できるように変更（歌詞変化の4番目の項目を追加）|
|[v5.4.0](https://github.com/cwtickle/danoniplus/releases/tag/v5.4.0)|・複数階層化に対応|
|[v3.10.0](https://github.com/cwtickle/danoniplus/releases/tag/v3.10.0)|・セット毎改行区切りに対応、改行区切り時は行末のカンマを任意に変更|
|[v1.9.0](https://github.com/cwtickle/danoniplus/releases/tag/v1.9.0)|・左揃え/中央揃え/右揃えの設定に対応<br>・歌詞表示種類を2から4へ拡大|
|[v1.0.0<br>(v0.46.0)](https://github.com/cwtickle/danoniplus/releases/tag/v1.0.1)|初回実装|

| [< 色変化](dos-e0002-ncolorData) | **歌詞表示** | [背景・マスクモーション >](dos-e0004-animationData) |
