**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-h0045-maxLifeVal) | Japanese** 

[Home](./) &gt; [譜面ヘッダー仕様](dos_header) &gt; [プレイ時の初期設定](dos_header#-プレイ時の初期設定) &gt; [ライフゲージ・判定関連の設定](dos_header#ライフゲージ判定関連の設定)

| [<- wordAutoReverse](dos-h0069-wordAutoReverse) || **maxLifeVal** | [frzStartjdgUse ->](dos-h0037-frzStartjdgUse) |

## maxLifeVal
- ライフの上限値設定

### 使い方
```
|maxLifeVal=3000|
```
### 説明
最大ライフ値を設定します。デフォルトは1000。  

### 関連項目
- [customGauge](dos-h0053-customGauge) [:pencil:](dos-h0053-customGauge/_edit) カスタムゲージ設定
- [**gaugeX**](dos-h0022-gaugeX) [:pencil:](dos-h0022-gaugeX/_edit) ゲージ設定の詳細

### 更新履歴

|Version|変更内容|
|----|----|
|[v6.5.0](https://github.com/cwtickle/danoniplus/releases/tag/v6.5.0)|・初回実装|

| [<- wordAutoReverse](dos-h0069-wordAutoReverse) || **maxLifeVal** | [frzStartjdgUse ->](dos-h0037-frzStartjdgUse) |