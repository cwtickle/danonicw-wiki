[← オブジェクトリファレンスに戻る](ObjectReferenceIndex)  

# splitLF
### 概要
- 文字列を改行コードで区切り、配列化する関数。

### 引数（括弧内はデフォルト値）

|引数|型|必須|指定内容|
|----|----|----|----|
|_str|string|*|入力文字列|

### 返却値
- 配列

### 使用例
```
|testData=test
test2
test3|
```

```javascript
// 上記のtestDataを変数に入れた前提
console.log(splitLF(testData)); // -> [`test`, `test2`, `test3`]
```

# splitLF2
### 概要
- 改行コード区切りを本来の区切り文字に変換して配列展開する関数。
- 改行区切りで間が空行だった場合は無効化する。

### 引数（括弧内はデフォルト値）

|引数|型|必須|指定内容|
|----|----|----|----|
|_str|string|*|入力文字列|
|_delim|string||最終的に区切り文字として指定する文字。デフォルトは`$`。|

### 返却値
- 配列

### 使用例
```
|testData=test$test4
test2

test3|
```

```javascript
// 上記のtestDataを変数に入れた前提
console.log(splitLF2(testData)); // -> [`test`, `test4`, `test2`, `test3`]
```

### 更新履歴

|Version|変更内容|
|----|----|
|[v27.5.0](https://github.com/cwtickle/danoniplus/releases/tag/v27.5.0)|・splitLF2を実装|
|[v21.0.0](https://github.com/cwtickle/danoniplus/releases/tag/v21.0.0)|・splitLFを実装|