[← オブジェクトリファレンスに戻る](ObjectReferenceIndex)  

# importCssFile
### 概要
- 外部CSSファイルを読み込む関数。
- これを使うと、htmlファイル内に読み込む外部CSSファイルが指定されていなくても  
引数に渡した外部CSSファイルが読み込まれるようになる。
- ただし、この関数を指定した以後で読込を開始するため、基本的に遅延読み込みになる。
- 読み込みに成功した場合、g_loadObj[ファイル名]に`true`を返し、失敗した場合は`false`を返す。
- **importCssFile**関数の後継として、ver26.3.1より**importCssFile2**関数に切り替えている。  
コールバック関数が不要で、async/awaitを使って呼び出す。

## importCssFile2
### 引数（括弧内はデフォルト値）

|引数|型|必須|指定内容|
|----|----|----|----|
|_href|string|*|外部CSSファイルのURL|

### 返却値
- Promise&lt;any&gt;

### 使用例
```javascript
// 呼び出す関数に'async'をつける
async function load() {
    const randTime = new Date().getTime();
    await importCssFile(`${g_headerObj.skinRoot}danoni_skin_${g_headerObj.skinType}.css?${randTime}`);  // 呼び出し時にawaitを前につける
    if (g_headerObj.skinType2 !== ``) {
        await importCssFile(`${g_headerObj.skinRoot2}danoni_skin_${g_headerObj.skinType2}.css?${randTime}`);  // 呼び出し時にawaitを前につける
    }
    initAfterCssLoaded();
}
```

## importCssFile
- ver26.2.0以前の関数です。今後、非推奨となる予定です。

### 引数（括弧内はデフォルト値）

|引数|型|必須|指定内容|
|----|----|----|----|
|_href|string|*|外部CSSファイルのURL|
|_func|function||読込完了後に実行する処理|

### 返却値
- なし

### 使用例
```javascript
const randTime = new Date().getTime();
importCssFile(`${g_headerObj.skinRoot}danoni_skin_${g_headerObj.skinType}.css?${randTime}`, _ => {
	if (g_headerObj.skinType2 !== ``) {
		importCssFile(`${g_headerObj.skinRoot2}danoni_skin_${g_headerObj.skinType2}.css?${randTime}`, _ => initAfterCssLoaded());
	} else {
		initAfterCssLoaded();
	}
});
```

### 関連項目
- [g_loadObj](obj-v0006-g_loadObj)
- [loadScript](fnc-c0010-loadScript)
- [loadMultipleFiles](fnc-c0044-loadMultipleFiles)

### 更新履歴

|Version|変更内容|
|----|----|
|[v26.3.1](https://github.com/cwtickle/danoniplus/releases/tag/v26.3.1)|・importCssFile2関数として作り直し|
|[v21.2.0](https://github.com/cwtickle/danoniplus/releases/tag/v21.2.0)|・読込完了を待つように変更、引数`_func`を追加|
|[v10.0.0](https://github.com/cwtickle/danoniplus/releases/tag/v10.0.0)|・初回実装|