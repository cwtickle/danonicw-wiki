**[English](https://github.com/cwtickle/danoniplus-docs/wiki/AboutCustomFunction) | Japanese**

| [< カスタムjs(スキンjs)による処理割込み](./AboutCustomJs) | **カスタム関数の定義** | [色付きオブジェクト仕様 >](./AboutColorObject) |

# カスタム関数の定義

## カスタム関数（任意）の追加方法
```javascript
function anotherCustomFunction() {

}
// anotherCustomFunctionをメインのフレーム毎処理に追加
g_customJsObj.mainEnterFrame.push(anotherCustomFunction); 
```

## カスタム関数オブジェクトの仕組み概要
- [カスタムjs(スキンjs)による処理割込み](AboutCustomJs)をご覧ください。  
既存の関数に割り込みを行うことでカスタムできる範囲を作成しています。
- カスタム関数オブジェクトは、挿入場所ごとにプロパティが定義されており、  
関数を配列の一要素として持たせて、挿入場所に到達した際にその関数群をまとめて実行しています。

### メインプログラムでの実行例
```javascript
g_customJsObj.title.forEach(func => func());
// g_customJsObj.title = [customTitleInit, customTitleInit2];
```

## g_customJsObj
- カスタムjsで定義した関数を格納しているオブジェクトです。  
ver25.3.0より導入され、既存の関数もここで定義される形式に変わっています。　　

|プロパティ名|内容|従来関数|指定可能引数|
|----|----|----|----|
|preTitle|タイトル画面（初期）表示前|-||
|title|タイトル画面（初期）表示後|customTitleInit||
|titleEnterFrame|タイトル画面（フレーム毎）|customTitleInitEnterFrame||
|dataMgt|データ管理画面（初期）|||
|precondition|デバッグ確認画面（初期）|||
|option|主要設定画面（初期）|customOptionInit||
|difficulty|主要設定画面（難易度選択時）|customSetDifficulty|_initFlg, _canLoadDifInfoFlg|
|settingsDisplay|Display設定画面（初期）|customSettingsDisplayInit||
|exSetting|拡張設定画面（初期）|||
|keyconfig|キーコンフィグ画面（初期）|customKeyConfigInit||
|preloading|ロード開始時|customPreloadingInit||
|loading|ロード画面|customLoadingInit||
|progress|音源読込中の動作|customLoadingProgress|_event|
|main|プレイ画面（初期）|customMainInit||
|makeArrow|矢印生成時|-|_attrs, _arrowName, _name, _arrowCnt|
|makeFrzArrow|フリーズアロー生成時|-|_attrs, _arrowName, _name, _arrowCnt|
|dummyArrow|ダミー矢印判定時|customJudgeDummyArrow||
|dummyFrz|ダミーフリーズアロー判定時|customJudgeDummyFrz||
|judg_ii|判定時（イイ）|customJudgeIi|_difFrame|
|judg_shakin|判定時（シャキン）|customJudgeShakin|_difFrame|
|judg_matari|判定時（マターリ）|customJudgeMatari|_difFrame|
|judg_shobon|判定時（ショボーン）|customJudgeShobon|_difFrame|
|judg_uwan|判定時（ウワァン）|customJudgeUwan|_difFrame|
|judg_kita|判定時（キター）|customJudgeKita|_difFrame|
|judg_iknai|判定時（イクナイ）|customJudgeIknai|_difFrame|
|judg_frzHit|通常フリーズアローヒット時|-|_difFrame|
|judg_dummyFrzHit|ダミーフリーズアローヒット時|-|_difFrame|
|mainEnterFrame|プレイ画面（フレーム毎）|customMainEnterFrame||
|result|結果画面（初期）|customResultInit||
|resultEnterFrame|結果画面（フレーム毎）|customResultEnterFrame||

### 判定時で使える引数

|引数名|内容|
|----|----|
|_difFrame|ジャストのタイミングからのフレーム数（正負あり）|

### g_customJsObj.difficulty (難易度選択時)で使える引数

|引数名|内容|
|----|----|
|_initFlg|初期表示時に`true`が入るフラグ|
|_canLoadDifInfoFlg|譜面初期情報ロード許可フラグ<br>初期化したくない場合は`false`を指定。|

### g_customJsObj.makeArrow / makeFrzArrowで使える引数

|引数名|内容|
|----|----|
|_attrs|矢印の属性。オブジェクト形式で以下のプロパティを持つ。<br>　pos: 矢印種類<br>　arrivalFrame: 到達フレーム数<br>　initY: 初期表示位置<br>　initBoostY: Motion有効時の初期表示位置加算<br>　motionFrame: アニメーション有効フレーム数|
|_arrowName|作成した矢印のid名|
|_name|矢印の種類。arrowかdummyArrowが入る。|
|_arrowCnt|作成した矢印の連番|


## g_skinJsObj
- スキンjsで定義した関数を格納しているオブジェクトです。  
ver25.3.0より導入され、既存の関数もここで定義される形式に変わっています。　　

|プロパティ名|内容|従来関数|
|----|----|----|
|title|タイトル画面（初期）|skinTitleInit|
|dataMgt|データ管理画面（初期）||
|precondition|デバッグ確認画面（初期）||
|option|主要設定画面（初期）|skinOptionInit|
|settingsDisplay|Display設定画面（初期）|skinSettingsDisplayInit|
|exSetting|拡張設定画面（初期）||
|keyconfig|キーコンフィグ画面（初期）|skinKeyConfigInit|
|preloading|ロード開始時|skinPreloadingInit|
|main|プレイ画面（初期）|skinMainInit|
|result|結果画面（初期）|skinResultInit|

## 更新履歴

|Version|変更内容|
|----|----|
|[v40.0.0](https://github.com/cwtickle/danoniplus/releases/tag/v40.0.0)|・デバッグ確認画面の割込み処理を追加<br>　(g_customJsObj.precondition)|
|[v39.5.0](https://github.com/cwtickle/danoniplus/releases/tag/v39.5.0)|・データ管理画面の割込み処理を追加<br>　(g_customJsObj.dataMgt)|
|[v39.0.0](https://github.com/cwtickle/danoniplus/releases/tag/v39.0.0)|・拡張設定画面の割込み処理を追加<br>　(g_customJsObj.exSetting)<br>・矢印／フリーズアロー生成時の割込み処理を追加<br>　(g_customJsObj.makeArrow, makeFrzArrow)|
|[v31.7.0](https://github.com/cwtickle/danoniplus/releases/tag/v31.7.0)|・フリーズアローヒット時の割込み処理を追加<br>　(g_customJsObj.judg_frzHit, judg_dummyFrzHit)|
|[v26.3.1](https://github.com/cwtickle/danoniplus/releases/tag/v26.3.1)|・タイトル画面表示前にカスタム関数を挿入できるよう変更<br>　(g_customJsObj.preTitle)|
|[v25.3.0](https://github.com/cwtickle/danoniplus/releases/tag/v25.3.0)|・カスタム関数の定義見直し|

| [< カスタムjs(スキンjs)による処理割込み](./AboutCustomJs) | **カスタム関数の定義** | [色付きオブジェクト仕様 >](./AboutColorObject) |
