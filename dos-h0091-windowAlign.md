**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-h0091-windowAlign) | Japanese** 

[Home](./) &gt; [譜面ヘッダー仕様](dos_header) &gt; [クレジット関連、共通設定](dos_header#-クレジット関連共通設定)

| [<- heightVariable](dos-h0100-heightVariable) | **windowAlign** | [tuning -> ](dos-h0017-tuning) | 

## windowAlign
- 画面ウィンドウの位置設定
- 共通設定 ⇒ [g_presetObj.windowAlign](dos-s0001-makerInfo#ウィンドウ位置-g_presetobjwindowalign)

### 使い方
```
|windowAlign=right|
```
### 説明
作品部分のウィンドウ位置を左寄せ(left)/中央(center)/右寄せ(right)から選択します。
デフォルトは`center`(中央)です。

|値|既定|内容|
|----|----|----|
|left||ウィンドウを左寄せにする|
|center|*|ウィンドウを中央にする|
|right||ウィンドウを右寄せにする|

### 関連項目
- [**autoSpread**](dos-h0089-autoSpread) [:pencil:](dos-h0089-autoSpread/_edit) 自動横幅拡張設定
- [**windowWidth**](dos-h0090-windowWidth) [:pencil:](dos-h0090-windowWidth/_edit) 画面ウィンドウの横幅
- [共通設定ファイル仕様](dos_setting) &gt; [制作者クレジット・共通設定](dos-s0001-makerInfo)

### 更新履歴

|Version|変更内容|
|----|----|
|[v27.1.0](https://github.com/cwtickle/danoniplus/releases/tag/v27.1.0)|・初回実装|

| [<- heightVariable](dos-h0100-heightVariable) | **windowAlign** | [tuning -> ](dos-h0017-tuning) | 