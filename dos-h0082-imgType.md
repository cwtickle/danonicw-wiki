**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-h0082-imgType) | Japanese** 

[Home](./) &gt; [譜面ヘッダー仕様](dos_header) &gt; [カスタムデータの取込](dos_header#-カスタムデータの取込)

| [<- settingType](dos-h0056-settingType) | **imgType** | [baseBright ->](dos-h0081-baseBright) |

## imgType
- 矢印・フリーズアロー等の基本画像セット名及び設定
- 共通設定 ⇒ [g_presetObj.imageSets](dos-s0005-defaultDesign#デフォルト画像セットの設定-g_presetobjimagesets)

### 使い方
```
|imgType=classic,png|
|imgType=classic,png$classic-thin,png|
|imgType=$classic,png$classic-thin,png$note,svg,false,10|
```
### 説明
[共通設定ファイル設定](dos_setting)の[デフォルト画像セットの設定](dos-s0005-defaultDesign#デフォルト画像セットの設定-g_presetobjimagesets)の作品別設定版です。  
デフォルトで指定する矢印・フリーズアロー等の基本画像を変更できます。  
[img]フォルダ内にサブフォルダを作成し、そこに画像一式を格納してから使用します。  

ver23.0.0より $区切り(ver27.5.0以降は改行区切りも可能)による複数指定に対応しました。

なお、imgTypeをローカルで有効にするには、ローカルサーバーを立てる必要があります。  
詳細は関連項目のページもご覧ください。

|番号|設定例|内容|
|----|----|----|
|1|classic|使用する画像セットのサブフォルダ名。<br>デフォルトは`空白`(imgフォルダ直下の画像を使用)|
|2|png|使用する画像セットの拡張子。デフォルトはsvg形式。|
|3|true|画像回転有無。デフォルトはtrue(回転する)。<br>矢印のように向きの関係のないオブジェクトの場合、回転が不要のためこのオプションが存在している。|
|4|10|Flat時ステップ間隔。デフォルトは50(矢印サイズ)。<br>Scroll: Flat時、ステップゾーンの代わりに2つのバーが用意されるが、その2つのバー間隔をpx単位で設定できる。|

#### 画像回転有無の設定例 (左：false, 右：true)
<img src="./wiki/dos-h0082-01.png" width="50%"><img src="./wiki/dos-h0082-02.png" width="50%">

#### Flat時ステップ間隔の設定例 (左：10px, 右：50px)
<img src="./wiki/dos-h0082-03.png" width="50%"><img src="./wiki/dos-h0082-04.png" width="50%">

### 関連項目
- [共通設定ファイル設定](dos_setting)
- [色付きオブジェクト仕様](AboutColorObject)
- [ローカルでのプレイ方法 (Xamppを使ったローカルサーバーの立て方)](HowToLocalPlay)

### 更新履歴

|Version|変更内容|
|----|----|
|[v27.5.0](https://github.com/cwtickle/danoniplus/releases/tag/v27.5.0)|・$区切りの代替として改行区切りに対応|
|[v23.1.0](https://github.com/cwtickle/danoniplus/releases/tag/v23.1.0)|・無指定（デフォルト）指定に対応<br>・3番目、4番目項目を追加|
|[v23.0.0](https://github.com/cwtickle/danoniplus/releases/tag/v23.0.0)|・$区切りによる複数指定に対応|
|[v22.5.1](https://github.com/cwtickle/danoniplus/releases/tag/v22.5.1)|・初回実装|

| [<- settingType](dos-h0056-settingType) | **imgType** | [baseBright ->](dos-h0081-baseBright) |