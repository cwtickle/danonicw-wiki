
[^ Tips Indexに戻る](./tips-index)

| < [スコアドラムロールの作成](./tips-0023-score-drum-roll) | **カスタム画面の作成** | [設定追加の実装例](./tips-0025-freeze-return) > |

# カスタム画面の作成

- カスタムJSを使って、独自の画面を実装します。
- ここでは設定画面を拡張した、独自の設定画面（Ex-Settings）を作成します。

## タイトル画面前の読み込み処理

- `g_customJsObj.preTitle`に変数を初期化しておきます。
- 既存のオブジェクトのプロパティーとして定義すれば、他の関数でも利用できます。

```javascript
g_customJsObj.preTitle.push(() => {

    // StepArea設定の初期化
    g_stateObj.stepArea = C_FLG_OFF;
    g_settings.stepAreas = [C_FLG_OFF, `Stairs`, `R-Stairs`];
    g_settings.stepAreaNum = 0;
    g_lblNameObj.StepArea = `StepArea`;
    g_lang_msgObj.Ja.stepArea = `ステップゾーン及び矢印の位置を全体的に回転する等の設定です。\n[Stairs] ステップゾーンを階段状にします`;
    g_lang_msgObj.En.stepArea = `This is the setting for overall rotation of the step zone and arrow position, etc.\n[Stairs] The step zone is in a staircase shape.`;

    // リザルトフォーマットの加工 (Ex-Settings設定を追加)
    g_headerObj.resultFormatOrg = g_headerObj.resultFormat.replace(`[playStyle]`, `[playStyle], [exSettings]`);

    // カスタムで設定したメッセージ群を再設定
    Object.assign(g_msgObj, g_lang_msgObj[g_localeObj.val], g_presetObj.msg?.[g_localeObj.val]);

    // Setting画面の追加初期設定
    g_btnPatterns.option._exSetting = 0;
    g_shortcutObj.option.KeyX = { id: `btn_exSetting` };

    // Display画面の追加初期設定
    g_btnPatterns.settingsDisplay._exSetting = 0;
    g_shortcutObj.settingsDisplay.KeyX = { id: `btn_exSetting` };

    // Ex-Settings画面の初期設定
    // - g_btnWaitFrameは画面に移動してからの無効時間を設定（この画面では特に指定なし）
    // - g_btnPatternsは "btn" + 名称で指定されるボタンに対してどの位置(px)にショートカットキーを表示するかを設定
    g_btnWaitFrame._exSetting = { b_frame: 0, s_frame: 0 };
    g_btnPatterns._exSetting = { Back: 0, _exBack: 0, KeyConfig: 0, Play: 0, _exSetting: -5, Save: -10, }
    g_shortcutObj._exSetting = {
        ShiftLeft_KeyS: { id: `lnkStepAreaL` },
        KeyS: { id: `lnkStepAreaR` },

        KeyX: { id: `btn_exBack` },
        KeyZ: { id: `btnSave` },
        Escape: { id: `btnBack` },
        Space: { id: `btnKeyConfig` },
        Enter: { id: `btnPlay` },
        Tab: { id: `btn_exSetting` },
    };
    g_jumpSettingWindow._exSetting = () => (g_baseDisp === `Settings` ? optionInit() : settingsDisplayInit());
});
```

## 設定画面

- Settings / Display画面双方に遷移するボタンを作成します。
- 処理が同じなので、1つの関数にまとめています。
- `_exSettingInit()`という関数はこの後作成するカスタム画面の処理です。

```javascript
const _addExButton = () => {
    multiAppend(divRoot,
        createCss2Button(`btn_exSetting`, `Ex`, _ => true, {
            x: g_btnX(), y: 60, w: 50, h: 50, siz: 20,
            resetFunc: _ => _exSettingInit(),
        }, g_cssObj.button_Tweet),
    );
};
g_customJsObj.option.push(() => _addExButton());
g_customJsObj.settingsDisplay.push(() => _addExButton());
```

<img src="https://github.com/user-attachments/assets/8c1e210d-7aa0-456b-b64b-64e61f998984" width="60%">

## 独自の設定画面（Ex-Settings）

- 独自の画面を作成する部分です。大きく分けて以下に留意します。
- 既存画面の描画を消去: `clearWindow()`
- ショートカットキーを作成する場合は、`g_currentPage`の値を変更  
※この値を設定すると、`g_customJsObj.preTitle`にて「g_btnWaitFrame」「g_btnPatterns」「g_shortcutObj」「g_jumpSettingWindow」の4種を設定する必要があります。
- 設定項目を作成する部分は、「createEmptySprite」「g_settingPos」「setSpriteList」「createGeneralSetting」までが1セット。
- Settings / Display画面での主要ボタンは「commonSettingBtn」にてまとめて作成可能です。
- ショートカットキーを有効にするには、「setShortcutEvent」以降の指定が必要です。

```javascript
const _exSettingInit = () => {
    clearWindow(true);
    g_currentPage = `_exSetting`;

    multiAppend(divRoot,

        // 画面タイトル
        getTitleDivLabel(`lblTitle`,
            `<div class="settings_Title">EX-</div><div class="settings_Title2">SETTINGS</div>`
                .replace(/[\t\n]/g, ``), 0, 15, g_cssObj.flex_centering),

    );

    // 各ボタン用のスプライトを作成
    createEmptySprite(divRoot, `optionsprite`, g_windowObj.optionSprite);

    // 設定毎に個別のスプライトを作成し、その中にラベル・ボタン類を配置
    // g_settingPos.exSettingにて各設定の位置を設定
    g_settingPos.exSetting = {
        stepArea: { heightPos: 0, y: 0, dw: 0, dh: 0 },
    };
    const spriteList = setSpriteList(g_settingPos.exSetting);

    createGeneralSetting(spriteList.stepArea, `stepArea`);

    multiAppend(divRoot,
        createCss2Button(`btn_exBack`, `Back`, _ => true, {
            x: g_btnX(), y: 60, w: 50, h: 50, siz: 20,
            resetFunc: _ => (g_baseDisp === `Settings` ? optionInit() : settingsDisplayInit()),
        }, g_cssObj.button_Back),
    );

    // 設定系のボタン群をまとめて作成（Data Save, Display切替, Back, KeyConfig, Playボタン）
    commonSettingBtn(g_currentPage);

    // キー操作イベント（デフォルト）
    setShortcutEvent(g_currentPage, () => true, { dfEvtFlg: true });
    document.oncontextmenu = () => true;
};
```

<img src="https://github.com/user-attachments/assets/09be6eaf-27fe-46b6-8c71-2ad3664481c7" width="60%">

## ロード／プレイ画面

- 独自の設定を行うために、プレイ画面の初期処理にて設定を行います。
- `g_stateObj.stepArea`の値によって処理分岐するようにしています。

```javascript
g_customJsObj.loading.push(() => {

    // リザルトフォーマットの初期化
    g_headerObj.resultFormat = g_headerObj.resultFormatOrg;

});
g_customJsObj.main.push(() => {

    const _changeStairs = (_rad) =>
        mainSprite.style.transform = `rotate(${_rad}deg)`;

    const _setStair = {
        'OFF': () => true,
        'Stairs': () => _changeStairs(-8),
        'R-Stairs': () => _changeStairs(8),
    };
    _setStair[g_stateObj.stepArea]();
});
```

## 結果画面

- 結果画面に今回追加した設定を反映します。
- `withOptions`関数は本体でも使われている関数で、デフォルト値以外の場合に設定したオプション名を返却する関数です。

```javascript
g_customJsObj.result.push(() => {

    const withOptions = (_flg, _defaultSet, _displayText = _flg) =>
        (_flg !== _defaultSet ? getStgDetailName(_displayText) : ``);

    const exSettingData = [
        withOptions(g_stateObj.stepArea, C_FLG_OFF),
    ].filter(value => value !== ``).join(`, `);

    // 結果画面へのEx-Settings設定の反映
    if (exSettingData !== ``) {
        lblStyleData.innerHTML += `, <span class="result_FullCombo">${exSettingData}</span>`;

        // 幅が足りなくなるためフォントサイズを350pxを上限に調整
        lblStyleData.style.fontSize = `${getFontSize(lblStyleData.textContent, 350, getBasicFont(), 14)}px`;

        // リザルトフォーマットへEx-Settings設定を追加
        g_headerObj.resultFormat = g_headerObj.resultFormatOrg.replace(`[exSettings]`, exSettingData);
    }
});
```

## ページ作成者
- ティックル

[^ Tips Indexに戻る](./tips-index)

| < [スコアドラムロールの作成](./tips-0023-score-drum-roll) | **カスタム画面の作成** | [設定追加の実装例](./tips-0025-freeze-return) > |
