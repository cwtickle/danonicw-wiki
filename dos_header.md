**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos_header) | Japanese** 

| [< Changelog](./Changelog-latest) || **譜面ヘッダー仕様** | [譜面本体仕様 >](./dos_score) |

# 譜面ヘッダー仕様 (初期設定)
ParaFla版をベースとしているため、ほとんどの項目はそのまま使えます。  
また区切り文字として"|"を使用していますが、"&"も一応使えます。  
注意点は[ParaFlaユーザ向け使い方](https://github.com/cwtickle/danoniplus/wiki/forParaFlaUser)もご覧ください。  

(🔧)[共通設定](dos_setting)あり / (🌎)多言語対応 / (◇)譜面別設定可 / (◆)譜面別分割書式可 /  
(★)ParaFlaソースとの共用項目  
また使用頻度が高かったり、要所で使う可能性のあるものは太字にしています。  

## 🎶 楽曲・譜面情報
- [**musicTitle**](dos-h0001-musicTitle) (🌎, ★) [:pencil:](dos-h0001-musicTitle/_edit) 楽曲／楽曲クレジット
- [**difData**](dos-h0002-difData) ([🔧](dos-s0003-initialGauge#デフォルトのゲージ設定-g_presetobjgauge), ◇, ★) [:pencil:](dos-h0002-difData/_edit) 譜面情報 
- [dosNo](dos-h0098-dosNo) (◇) [:pencil:](dos-h0098-dosNo/_edit) 譜面に割り当てる譜面ファイル番号及び譜面番号の設定
- [**musicUrl**](dos-h0011-musicUrl) [:pencil:](dos-h0011-musicUrl/_edit) 楽曲ファイル名
- [musicNo](dos-h0012-musicNo) (◇) [:pencil:](dos-h0012-musicNo/_edit) 楽曲ファイルと譜面の対応付け
- [musicFolder](dos-h0013-musicFolder) [:pencil:](dos-h0013-musicFolder/_edit) 楽曲ファイルの格納先
- [dummyId](dos-h0042-dummyId) (◇) [:pencil:](dos-h0042-dummyId/_edit) ダミー譜面に割り当てる譜面番号の指定

## ⏳ プレイ時間制御・譜面位置調整
- [startFrame](dos-h0005-startFrame) (◇, ★) [:pencil:](dos-h0005-startFrame/_edit) プレイ開始フレーム数
- [blankFrame](dos-h0006-blankFrame) (◇) [:pencil:](dos-h0006-blankFrame/_edit) 曲開始までの空白フレーム数
- [**endFrame**](dos-h0007-endFrame) (◇) [:pencil:](dos-h0007-endFrame/_edit) プレイ終了フレーム数
- [**fadeFrame**](dos-h0008-fadeFrame) (◇) [:pencil:](dos-h0008-fadeFrame/_edit) フェードアウト開始フレーム数
- [**adjustment**](dos-h0009-adjustment) (◇) [:pencil:](dos-h0009-adjustment/_edit) 譜面位置の初期調整
- [**playbackRate**](dos-h0010-playbackRate) [:pencil:](dos-h0010-playbackRate/_edit) 楽曲再生速度（主にテストプレイ用）
- [unStockCategory](dos-h0084-unStockCategory) ([🔧](dos-s0007-viewControl#フェードイン前のデータを保持しない種別の設定-g_presetobjunstockcategories)) [:pencil:](dos-h0084-unStockCategory/_edit) フェードイン前のデータを保持しない種別のリスト
- [word/back/maskStockForceDel](dos-h0085-stockForceDel) ([🔧](dos-s0007-viewControl#フェードイン前のデータを保持しないパターンの設定-g_presetobjstockforcedellist)) [:pencil:](dos-h0085-stockForceDel/_edit) フェードイン前のデータを保持しないパターンの設定

## 🛠 設定時の初期設定
- [minSpeed](dos-h0015-minSpeed) [:pencil:](dos-h0015-minSpeed/_edit) 設定できる最低速度
- [maxSpeed](dos-h0016-maxSpeed) [:pencil:](dos-h0016-maxSpeed/_edit) 設定できる最高速度
- [difSelectorUse](dos-h0051-difSelectorUse) [:pencil:](dos-h0051-difSelectorUse/_edit) 譜面選択セレクターの利用有無
- [scoreDetailUse (chartDetailUse)](dos-h0060-scoreDetailUse) (🌎) [:pencil:](dos-h0060-scoreDetailUse/_edit) 譜面明細表示の利用有無
- [**settingUse**](dos-h0035-settingUse) ([🔧](dos-s0006-settingUse#オプション有効化設定-g_presetobjsettinguse)) [:pencil:](dos-h0035-settingUse/_edit) 設定項目の利用有無
   - motionUse, scrollUse, shuffleUse, ... etc
- [displayUse](dos-h0057-displayUse) ([🔧](dos-s0006-settingUse#オプション有効化設定-g_presetobjsettinguse)) [:pencil:](dos-h0057-displayUse/_edit) Display項目の利用有無, 初期値設定
   - stepZoneUse, judgmentUse, fastSlowUse, ... etc
- [displayChainOFF](dos-h0064-displayChainOFF) [:pencil:](dos-h0064-displayChainOFF/_edit) Display項目の連動OFF設定
   - backgroundChainOFF, arrowEffectChainOFF, specialChainOFF, ... etc
- [transKeyUse](dos-h0024-transKeyUse) (◇) [:pencil:](dos-h0024-transKeyUse/_edit) 別キーモードの利用有無
- [keyGroupOrder](dos-h0092-keyGroupOrder) (◇) [:pencil:](dos-h0092-keyGroupOrder/_edit) キーコンフィグで設定可能な部分キーグループの設定
- [**customFont**](dos-h0020-customFont) [:pencil:](dos-h0020-customFont/_edit) 画面全般のフォント
- [**customGauge**](dos-h0053-customGauge) ([🔧](dos-s0003-initialGauge#カスタムゲージリスト-g_presetobjgaugelist), ◆) [:pencil:](dos-h0053-customGauge/_edit) カスタムゲージ設定
- [**gaugeX**](dos-h0022-gaugeX) ([🔧](dos-s0003-initialGauge#デフォルト以外のゲージ値の初期設定-g_presetobjgaugecustom), ◇, ◆) [:pencil:](dos-h0022-gaugeX/_edit) ゲージ設定の詳細
   - gaugeEasy, gaugeHard, gaugeLight, ... etc
- [colorDataType](dos-h0046-colorDataType) [:pencil:](dos-h0046-colorDataType/_edit) 色変化の過去互換設定
- [colorCdPaddingUse](dos-h0047-colorCdPaddingUse) [:pencil:](dos-h0047-colorCdPaddingUse/_edit) 初期矢印色/フリーズアロー色のゼロパディング有無設定

## ⏯ プレイ時の初期設定
### 矢印・フリーズアロー色の設定
- [**setColor**](dos-h0003-setColor) (◆, ★) [:pencil:](dos-h0003-setColor/_edit) 矢印色
- [**frzColor**](dos-h0004-frzColor) (◆, ★) [:pencil:](dos-h0004-frzColor/_edit) フリーズアロー色
- [setShadowColor](dos-h0041-setShadowColor) (◆) [:pencil:](dos-h0041-setShadowColor/_edit) 矢印の内側を塗りつぶす設定および色の設定
- [frzShadowColor](dos-h0062-frzShadowColor) (◆) [:pencil:](dos-h0062-frzShadowColor/_edit) フリーズアローの矢印内側を塗りつぶす設定および色の設定
- [**defaultColorgrd**](dos-h0061-defaultColorgrd) [:pencil:](dos-h0061-defaultColorgrd/_edit) 自動グラデーション設定
- [defaultFrzColorUse](dos-h0063-defaultFrzColorUse) ([🔧](dos-s0004-frzArrow#フリーズアローのデフォルト色セットの利用有無-g_presetobjfrzcolors)) [:pencil:](dos-h0063-defaultFrzColorUse/_edit) フリーズアロー初期色(frzColor)が未指定時の適用方法
- [frzScopeFromAC](dos-h0088-frzScopeFromAC) ([🔧](dos-s0004-frzArrow#矢印色変化に対応してフリーズアロー色を追随する範囲の設定-g_presetobjfrzscopefromac)) [:pencil:](dos-h0088-frzScopeFromAC/_edit) 矢印色変化をフリーズアロー色変化に自動適用する範囲の設定

### プレイ画面位置の設定
- [playingX / playingY](dos-h0070-playingX) ([🔧](dos-s0007-viewControl#ゲーム表示エリアのx-y座標-g_presetobjplayingx-g_presetobjplayingy)) [:pencil:](dos-h0070-playingX/_edit) ゲーム表示エリアのX, Y座標
- [playingWidth / playingHeight](dos-h0071-playingWidth) ([🔧](dos-s0007-viewControl#ゲーム表示エリアの横幅高さ-g_presetobjplayingwidth-g_presetobjplayingheight)) [:pencil:](dos-h0071-playingWidth/_edit) ゲーム表示エリアの横幅、高さ
- [customCreditWidth](dos-h0083-customCreditWidth) [:pencil:](dos-h0071-playingWidth/_edit) カスタムクレジットエリアの横幅
- [scArea](dos-h0096-scArea) ([🔧](dos-s0007-viewControl#ショートカットキーエリアの横幅拡張設定-g_presetobjscareawidth)[🔧](dos-s0007-viewControl#プレイ画面の表示レイアウト-g_presetobjplayinglayout)) [:pencil:](dos-h0096-scArea/_edit) ショートカットキー表示の横幅拡張設定
- [stepY](dos-h0014-stepY) [:pencil:](dos-h0014-stepY/_edit) ステップゾーンのY座標位置
- [stepYR](dos-h0049-stepYR) [:pencil:](dos-h0049-stepYR/_edit) ステップゾーン(下)のY座標現位置からの差分
- [arrowJdgY / frzJdgY](dos-h0058-jdgY) [:pencil:](dos-h0058-jdgY/_edit) 判定キャラクタのY座標位置
- [jdgPosReset](dos-h0065-jdgPosReset) [:pencil:](dos-h0065-jdgPosReset/_edit) 判定キャラクタ位置のリセット設定(Background:OFF時)
- [bottomWordSet](dos-h0059-bottomWordSet) [:pencil:](dos-h0059-bottomWordSet/_edit) 下側の歌詞表示位置をステップゾーン位置に連動させる設定
- [wordAutoReverse](dos-h0069-wordAutoReverse) ([🔧](dos-s0007-viewControl#reverse時の歌詞の自動反転制御設定-g_presetobjwordautoreverse)) [:pencil:](dos-h0069-wordAutoReverse/_edit) Reverse時に歌詞表示を条件付きで反転させる設定

### ライフゲージ・判定関連の設定
- [maxLifeVal](dos-h0045-maxLifeVal) [:pencil:](dos-h0045-maxLifeVal/_edit) ライフの上限値
- [frzStartjdgUse](dos-h0037-frzStartjdgUse) ([🔧](dos-s0004-frzArrow#フリーズアローの始点判定設定-g_presetobjfrzstartjdguse)) [:pencil:](dos-h0037-frzStartjdgUse/_edit) フリーズアロー開始判定の設定有無
- [frzAttempt](dos-h0038-frzAttempt) [:pencil:](dos-h0038-frzAttempt/_edit) フリーズアローヒット時の許容フレーム数
- [excessiveJdgUse](dos-h0093-excessiveJdgUse) ([🔧](dos-s0003-initialGauge#空押し判定のデフォルト設定-g_presetobjexcessivejdguse)) [:pencil:](dos-h0093-excessiveJdgUse/_edit) 空押し判定のデフォルト設定

### 画面表示・キーコントロール
- [readyDelayFrame](dos-h0052-readyDelayFrame) [:pencil:](dos-h0052-readyDelayFrame/_edit) Ready?が表示されるまでの遅延フレーム数
- [readyAnimationFrame](dos-h0073-readyAnimationFrame) [:pencil:](dos-h0073-readyAnimationFrame/_edit) Ready?のアニメーションフレーム数
- [readyAnimationName](dos-h0074-readyAnimationName) [:pencil:](dos-h0074-readyAnimationName/_edit) Ready?のアニメーション名
- [readyColor](dos-h0075-readyColor) [:pencil:](dos-h0075-readyColor/_edit) Ready?の先頭色設定
- [readyHtml](dos-h0080-readyHtml) [:pencil:](dos-h0080-readyHtml/_edit) Ready?の文字設定
- [finishView](dos-h0023-finishView) [:pencil:](dos-h0023-finishView/_edit) フルコンボ演出の有無
- [keyRetry](dos-h0039-keyRetry) [:pencil:](dos-h0039-keyRetry/_edit) リトライを行うショートカットキーの設定
- [keyTitleBack](dos-h0040-keyTitleBack) [:pencil:](dos-h0040-keyTitleBack/_edit) タイトルバックを行うショートカットキーの設定

## ⚓️ タイトル・結果画面の初期設定
- [**commentVal**](dos-h0066-commentVal) (🌎) [:pencil:](dos-h0066-commentVal/_edit) コメント表示
- [commentAutoBr](dos-h0067-commentAutoBr) [:pencil:](dos-h0067-commentAutoBr/_edit) コメント表示時に改行タグを自動挿入する設定
- [commentExternal](dos-h0068-commentExternal) [:pencil:](dos-h0068-commentExternal/_edit) コメントを本体の外部に置く設定
- [masktitleButton](dos-h0043-masktitleButton) [:pencil:](dos-h0043-masktitleButton/_edit) タイトル画面上のボタン群の有効/無効設定
- [resultDelayFrame](dos-h0076-resultDelayFrame) [:pencil:](dos-h0076-readyDelayFrame/_edit) CLEARED/FAILEDが表示されるまでの遅延フレーム数
- [maskresultButton](dos-h0044-maskresultButton) [:pencil:](dos-h0044-maskresultButton/_edit) リザルト画面上のボタン群の有効/無効設定
- [resultMotionSet](dos-h0048-resultMotionSet) [:pencil:](dos-h0048-resultMotionSet/_edit) リザルトモーションのON/OFF設定
- [**resultFormat**](dos-h0072-resultFormat) ([🔧](dos-s0008-resultVals#リザルトデータのフォーマット設定-g_presetobjresultformat)) [:pencil:](dos-h0072-resultFormat/_edit) リザルトデータのフォーマット設定
- [resultValsView](dos-h0095-resultValsView) ([🔧](dos-s0008-resultVals#リザルトデータ用のカスタム変数の画像表示設定-g_presetobjresultvalsview)) [:pencil:](dos-h0095-resultValsView/_edit) リザルトデータ用のカスタム変数の画像表示設定

## 🏙 カスタムデータの取込
- [**customJs**](dos-h0019-customjs) ([🔧](dos-s0002-customFile#デフォルトカスタムjs-g_presetobjcustomjs), 🌎) [:pencil:](dos-h0019-customjs/_edit) カスタムjsファイルの指定
- [customCss](dos-h0086-customcss) ([🔧](dos-s0002-customFile#デフォルトカスタムcss-g_presetobjcustomcss), 🌎) [:pencil:](dos-h0086-customcss/_edit) カスタムcssファイルの指定
- [**preloadImages**](dos-h0021-preloadImages) [:pencil:](dos-h0021-preloadImages/_edit) 画像ファイルの事前読み込み設定
- [**autoPreload**](dos-h0055-autoPreload) [:pencil:](dos-h0055-autoPreload/_edit) 画像ファイルの自動読み込み設定
- [**skinType**](dos-h0054-skinType) ([🔧](dos-s0002-customFile#デフォルトスキン-g_presetobjskintype)) [:pencil:](dos-h0054-skinType/_edit) スキン設定
- [bgCanvasUse](dos-h0094-bgCanvasUse) ([🔧](dos-s0002-customFile)) [:pencil:](dos-h0094-bgCanvasUse/_edit) デフォルトスキン時、背景としてCanvasデータを使用するかどうかの設定（ver9以前互換）
- [**settingType**](dos-h0056-settingType) [:pencil:](dos-h0056-settingType/_edit) 共通設定名
- [imgType](dos-h0082-imgType) ([🔧](dos-s0005-defaultDesign#デフォルト画像セットの設定-g_presetobjimagesets)) [:pencil:](dos-h0082-imgType/_edit) 矢印・フリーズアロー等の基本画像セット名及び設定
- [baseBright](dos-h0081-baseBright) [:pencil:](dos-h0081-baseBright/_edit) 背景の明暗状態
- [syncBackPath](dos-h0087-syncBackPath) ([🔧](dos-s0002-customFile#背景マスクモーションで使う画像パスの基準フォルダ設定-g_presetobjsyncbackpath)) [:pencil:](dos-h0087-syncBackPath/_edit) 背景・マスクモーションで使う画像パスの基準フォルダ設定

## 🍂 デフォルトデザインの利用有無
- [**customTitleUse**](dos-h0025-customTitleUse) ([🔧](dos-s0005-defaultDesign#デフォルトデザインの使用有無設定-g_presetobjcustomdesignuse)) [:pencil:](dos-h0025-customTitleUse/_edit) タイトルの曲名文字
- [customTitleArrowUse](dos-h0026-customTitleArrowUse) ([🔧](dos-s0005-defaultDesign#デフォルトデザインの使用有無設定-g_presetobjcustomdesignuse)) [:pencil:](dos-h0026-customTitleArrowUse/_edit) タイトルの背景矢印
- [customTitleAnimationUse](dos-h0078-customTitleAnimationUse) ([🔧](dos-s0005-defaultDesign#デフォルトデザインの使用有無設定-g_presetobjcustomdesignuse)) [:pencil:](dos-h0078-customTitleAnimationUse/_edit) タイトルのアニメーション設定
- [customBackUse](dos-h0027-customBackUse) ([🔧](dos-s0005-defaultDesign#デフォルトデザインの使用有無設定-g_presetobjcustomdesignuse)) [:pencil:](dos-h0027-customBackUse/_edit) 背景(プレイ画面以外)
- [customBackMainUse](dos-h0028-customBackMainUse) ([🔧](dos-s0005-defaultDesign#デフォルトデザインの使用有無設定-g_presetobjcustomdesignuse)) [:pencil:](dos-h0028-customBackMainUse/_edit) 背景(プレイ画面)
- [customReadyUse](dos-h0029-customReadyUse) ([🔧](dos-s0005-defaultDesign#デフォルトデザインの使用有無設定-g_presetobjcustomdesignuse)) [:pencil:](dos-h0029-customReadyUse/_edit) プレイ開始時演出

## 🌟 タイトル曲名文字(デフォルトデザイン)のエフェクト
- [**titleSize**](dos-h0030-titlesize) (🌎) [:pencil:](dos-h0030-titlesize/_edit) 文字サイズ
- [**titleFont**](dos-h0031-titlefont) (🌎) [:pencil:](dos-h0031-titlefont/_edit) フォント
- [titlegrd / titleArrowgrd](dos-h0032-titlegrd) (🌎) [:pencil:](dos-h0032-titlegrd/_edit) グラデーション
- [titlePos](dos-h0033-titlepos) (🌎) [:pencil:](dos-h0033-titlepos/_edit) X, Y座標位置
- [**titleLineHeight**](dos-h0034-titlelineheight) (🌎) [:pencil:](dos-h0034-titlelineheight/_edit) 複数行の際の行間
- [**titleAnimation**](dos-h0077-titleanimation) (🌎) [:pencil:](dos-h0077-titleanimation/_edit) アニメーション設定
- [titleAnimationClass](dos-h0079-titleanimationclass) (🌎) [:pencil:](dos-h0079-titleanimationclass/_edit) アニメーションクラス設定
- [titleArrowName](dos-h0099-titleArrowName) [:pencil:](dos-h0099-titleArrowName/_edit) 矢印種類

## 📋 クレジット関連、共通設定
- [**autoSpread**](dos-h0089-autoSpread) ([🔧](dos-s0001-makerInfo#自動横幅拡張設定-g_presetobjautospread)) [:pencil:](dos-h0089-autoSpread/_edit) 自動横幅拡張設定
- [**windowWidth**](dos-h0090-windowWidth) ([🔧](dos-s0001-makerInfo#最小横幅設定-g_presetobjautominwidth)) [:pencil:](dos-h0090-windowWidth/_edit) 画面ウィンドウの横幅
- [windowHeight](dos-h0097-windowHeight) ([🔧](dos-s0001-makerInfo#最小高さ設定-g_presetobjautominheight)) [:pencil:](dos-h0097-windowHeight/_edit) 画面ウィンドウの高さ
- [heightVariable](dos-h0100-heightVariable) ([🔧](dos-s0001-makerInfo)) [:pencil:](dos-h0100-heightVariable/_edit) 画面ウィンドウの高さ可変設定
- [**windowAlign**](dos-h0091-windowAlign) ([🔧](dos-s0001-makerInfo#ウィンドウ位置-g_presetobjwindowalign)) [:pencil:](dos-h0091-windowAlign/_edit) 画面ウィンドウの位置
- [**tuning**](dos-h0017-tuning) ([🔧](dos-s0001-makerInfo#譜面製作者名-g_presetobjtuning)[🔧](dos-s0001-makerInfo#譜面製作者url-g_presetobjtuningurl), 🌎, ★) [:pencil:](dos-h0017-tuning/_edit) 製作者クレジット
- [**makerView**](dos-h0050-makerView) [:pencil:](dos-h0050-makerView/_edit) 譜面別の制作者名表示設定 
- [**hashTag**](dos-h0018-hashTag) [:pencil:](dos-h0018-hashTag/_edit) ハッシュタグ
- [releaseDate](dos-h0036-releaseDate) [:pencil:](dos-h0036-releaseDate/_edit) 作品公開日

| [< Changelog](./Changelog-latest) || **譜面ヘッダー仕様** | [譜面本体仕様 >](./dos_score) |
