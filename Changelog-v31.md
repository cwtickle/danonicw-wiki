**[English](https://github.com/cwtickle/danoniplus-docs/wiki/Changelog-v31) | Japanese** 

[^ Back to All Version List (Security Policy)](https://github.com/cwtickle/danoniplus/security/policy)  
[^ Back to Update Info](./UpdateInfo#-v31-changelog)

[**<- v32**](Changelog-v32) | **v31** | [**v30 ->**](Changelog-v30)  
(**🔖 [17 Releases](https://github.com/cwtickle/danoniplus/releases?q=tag%3Av31)** )

## 🔃 Files changed (v31)

|Directory|FileName||Last Updated|
|----|----|----|----|
|/js|danoni_main.js|[📥](https://github.com/cwtickle/danoniplus/releases/download/v31.7.7/danoni_main.js)|**v31.7.7**|
|/js/lib|danoni_constants.js|[📥](https://github.com/cwtickle/danoniplus/releases/download/v31.7.0/danoni_constants.js)|[v31.7.0](./Changelog-v31#v3170-2023-05-03)|
|/skin|danoni_skin_default.css<br>danoni_skin_light.css<br>danoni_skin_skyblue.css|[📥](https://github.com/cwtickle/danoniplus/releases/download/v31.0.0/skin_css.zip)|[v31.0.0](./Changelog-v31#v3100-2023-03-20)|

<details>
<summary>Changed file list before v30</summary>

|Directory|FileName||Last Updated|
|----|----|----|----|
|/js/lib|danoni_localbinary.js|[📥](https://github.com/cwtickle/danoniplus/releases/download/v15.1.0/danoni_localbinary.js)|[v15.1.0](./Changelog-v15#v1510-2020-05-21)|
|/css|danoni_main.css|[📥](https://github.com/cwtickle/danoniplus/releases/download/v28.3.1/danoni_main.css)|[v28.3.1](Changelog-v28#v2831-2022-10-16)|
|/img|aaShadow.svg<br>arrow.svg<br>arrowShadow.svg<br>borderline.svg<br>c.svg<br>cursor.svg<br>giko.svg<br>iyo.svg<br>monar.svg<br>morara.svg<br>onigiri.svg|[📥](https://github.com/cwtickle/danoniplus/releases/download/v15.1.0/img.zip)|[v15.1.0](./Changelog-v15#v1510-2020-05-21)|

</details>

⭐ New Features / 🛠️ Improvements / 🐞 Bug Fixes  
( Files changed: ⚪ danoni_main.js / 🔵 danoni_constants.js / 🔴 css files )

## [Warning] 更新終了後の不具合情報
- [更新を終了したバージョンの不具合情報#v31](DeprecatedVersionBugs#v31) を参照

## v31.7.7 ([2023-09-02](https://github.com/cwtickle/danoniplus/releases/tag/v31.7.7))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v31.7.7/total)](https://github.com/cwtickle/danoniplus/archive/v31.7.7.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v31.7.7/support/v31?style=social)](https://github.com/cwtickle/danoniplus/compare/v31.7.7...support/v24#files_bucket) 
- 🐞 **C)** エラー時に表示される音楽ファイルの参照先URLが実際と異なる問題を修正 ( PR [#1540](https://github.com/cwtickle/danoniplus/pull/1540) ) <- :boom: [**v29.4.1**](./Changelog-v29#v2941-2023-01-28)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v31.7.7)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v31.7.6...v31.7.7#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v31.7.7)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v31.7.7.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v31.7.7.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v31.7.7/js/danoni_main.js)
/ 🎣 [**v33.3.0**](./Changelog-v33#v3330-2023-08-22)

## v31.7.6 ([2023-08-20](https://github.com/cwtickle/danoniplus/releases/tag/v31.7.6))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v31.7.6/total)](https://github.com/cwtickle/danoniplus/archive/v31.7.6.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v31.7.6/support/v31?style=social)](https://github.com/cwtickle/danoniplus/compare/v31.7.6...support/v24#files_bucket) 
- 🐞 **C)** stockForceDel使用中に除外する検索対象が未定義の場合、エラーになる問題を修正 ( PR [#1536](https://github.com/cwtickle/danoniplus/pull/1536) ) <- :boom: [**v25.0.0**](./Changelog-v25#v2500-2022-01-04)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v31.7.6)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v31.7.5...v31.7.6#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v31.7.6)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v31.7.6.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v31.7.6.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v31.7.6/js/danoni_main.js)
/ 🎣 [**v33.2.0**](./Changelog-v33#v3320-2023-08-20)

## v31.7.5 ([2023-08-19](https://github.com/cwtickle/danoniplus/releases/tag/v31.7.5))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v31.7.5/total)](https://github.com/cwtickle/danoniplus/archive/v31.7.5.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v31.7.5/support/v31?style=social)](https://github.com/cwtickle/danoniplus/compare/v31.7.5...support/v24#files_bucket) 
- 🐞 **B)** キー変化でadjustmentやplaybackRateが反映されない問題を修正 ( PR [#1534](https://github.com/cwtickle/danoniplus/pull/1534) ) <- :boom: [**v30.0.1**](./Changelog-v30#v3001-2023-02-11)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v31.7.5)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v31.7.4...v31.7.5#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v31.7.5)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v31.7.5.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v31.7.5.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v31.7.5/js/danoni_main.js)
/ 🎣 [**v33.1.4**](./Changelog-v33#v3314-2023-08-19)

## v31.7.4 ([2023-08-14](https://github.com/cwtickle/danoniplus/releases/tag/v31.7.4))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v31.7.4/total)](https://github.com/cwtickle/danoniplus/archive/v31.7.4.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v31.7.4/support/v31?style=social)](https://github.com/cwtickle/danoniplus/compare/v31.7.4...support/v24#files_bucket) 
- 🐞 **B)** フリーズアローの終点と次のフリーズアローの始点が近い場合の判定不具合を修正 ( PR [#1530](https://github.com/cwtickle/danoniplus/pull/1530), Issue [#1529](https://github.com/cwtickle/danoniplus/pull/1529) ) <- :boom: [**v29.0.1**](./Changelog-v29#v2901-2022-11-05)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v31.7.4)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v31.7.3...v31.7.4#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v31.7.4)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v31.7.4.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v31.7.4.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v31.7.4/js/danoni_main.js)
/ 🎣 [**v33.1.2**](./Changelog-v33#v3312-2023-08-14)

## v31.7.3 ([2023-06-02](https://github.com/cwtickle/danoniplus/releases/tag/v31.7.3))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v31.7.3/total)](https://github.com/cwtickle/danoniplus/archive/v31.7.3.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v31.7.3/support/v31?style=social)](https://github.com/cwtickle/danoniplus/compare/v31.7.3...support/v24#files_bucket) 
- 🐞 **C)** 矢印数がゼロの場合にツール値がNaNになる問題を修正 ( PR [#1498](https://github.com/cwtickle/danoniplus/pull/1498) ) <- :boom: [**v31.7.0**](./Changelog-v31#v3170-2023-05-03)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v31.7.3)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v31.7.2...v31.7.3#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v31.7.3)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v31.7.3.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v31.7.3.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v31.7.3/js/danoni_main.js)
/ 🎣 [**v32.3.0**](./Changelog-v32#v3230-2023-06-02)

## v31.7.2 ([2023-05-31](https://github.com/cwtickle/danoniplus/releases/tag/v31.7.2))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v31.7.2/total)](https://github.com/cwtickle/danoniplus/archive/v31.7.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v31.7.2/support/v31?style=social)](https://github.com/cwtickle/danoniplus/compare/v31.7.2...support/v24#files_bucket) 
- 🐞 **C)** customCreditWidthが使用できない問題を修正 ( PR [#1496](https://github.com/cwtickle/danoniplus/pull/1496) ) <- :boom: [**v23.4.0**](./Changelog-v23#v2340-2021-10-02)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v31.7.2)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v31.7.1...v31.7.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v31.7.2)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v31.7.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v31.7.2.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v31.7.2/js/danoni_main.js)
/ 🎣 [**v32.3.0**](./Changelog-v32#v3230-2023-06-02)

----

## v31.7.1 ([2023-05-04](https://github.com/cwtickle/danoniplus/releases/tag/v31.7.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v31.7.1/total)](https://github.com/cwtickle/danoniplus/archive/v31.7.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v31.7.1/support/v31?style=social)](https://github.com/cwtickle/danoniplus/compare/v31.7.1...support/v31#files_bucket)
- 🐞 **C)** 同じキー数の譜面が複数存在するときに譜面リストのキー別フィルタが重複表示される問題を修正 ( PR [#1477](https://github.com/cwtickle/danoniplus/pull/1477) ) <- :boom: [**v31.7.0**](./Changelog-v31#v3170-2023-05-03)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v31.7.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v31.7.0...v31.7.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v31.7.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v31.7.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v31.7.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v31.7.1/js/danoni_main.js)

## v31.7.0 ([2023-05-03](https://github.com/cwtickle/danoniplus/releases/tag/v31.7.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v31.7.0/total)](https://github.com/cwtickle/danoniplus/archive/v31.7.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v31.7.0/support/v31?style=social)](https://github.com/cwtickle/danoniplus/compare/v31.7.0...support/v31#files_bucket)
- ⭐ フリーズアロー始点判定成功時の割込み処理を実装 ( PR [#1473](https://github.com/cwtickle/danoniplus/pull/1473) )
- 🛠️ 譜面選択部分について、表示範囲を制限できる配列を追加 ( PR [#1475](https://github.com/cwtickle/danoniplus/pull/1475) )
- 🛠️ コードの整理 ( PR [#1474](https://github.com/cwtickle/danoniplus/pull/1474) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v31.7.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v31.6.0...v31.7.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v31.7.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v31.7.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v31.7.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v31.7.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v31.7.0/js/lib/danoni_constants.js)

## v31.6.0 ([2023-04-26](https://github.com/cwtickle/danoniplus/releases/tag/v31.6.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v31.6.0/total)](https://github.com/cwtickle/danoniplus/archive/v31.6.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v31.6.0/support/v31?style=social)](https://github.com/cwtickle/danoniplus/compare/v31.6.0...support/v31#files_bucket)
- ⭐ 7ikeyに7key, 12keyモードを追加 ( PR [#1467](https://github.com/cwtickle/danoniplus/pull/1467) )
- ⭐ 23keyにシャッフルグループを追加 ( PR [#1467](https://github.com/cwtickle/danoniplus/pull/1467) )
- 🛠️ 設定系のサブ関数をグローバル関数に変更 ( PR [#1466](https://github.com/cwtickle/danoniplus/pull/1466), [#1471](https://github.com/cwtickle/danoniplus/pull/1471) )
- 🛠️ キー数名称（key）を変更した場合に、その名前が譜面リストのキー数フィルタ名にも反映されるよう変更 ( PR [#1468](https://github.com/cwtickle/danoniplus/pull/1468) )
- 🐞 **C)** 譜面リスト選択時にキーコンフィグ画面で止まることがある問題を修正 ( PR [#1470](https://github.com/cwtickle/danoniplus/pull/1470) ) <- :boom: [**v24.5.1**](./Changelog-v24#v2451-2021-12-13)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v31.6.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v31.5.0...v31.6.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v31.6.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v31.6.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v31.6.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v31.6.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v31.6.0/js/lib/danoni_constants.js)

## v31.5.0 ([2023-04-16](https://github.com/cwtickle/danoniplus/releases/tag/v31.5.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v31.5.0/total)](https://github.com/cwtickle/danoniplus/archive/v31.5.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v31.5.0/support/v31?style=social)](https://github.com/cwtickle/danoniplus/compare/v31.5.0...support/v31#files_bucket)
- ⭐ Shuffle設定に「Turning」を実装 ( PR [#1464](https://github.com/cwtickle/danoniplus/pull/1464) )
- ⭐ 別キーモード、ミラー（Mirror, Asym-Mirror）時のハイスコア保存に対応 ( PR [#1464](https://github.com/cwtickle/danoniplus/pull/1464) )
- 🛠️ 別キーモードのときローカル保存が有効になっていれば、Appearance, Opacity, HitPosition, Adjustment, Volume を保存するように挙動を変更 ( PR [#1464](https://github.com/cwtickle/danoniplus/pull/1464) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v31.5.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v31.4.1...v31.5.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v31.5.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v31.5.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v31.5.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v31.5.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v31.5.0/js/lib/danoni_constants.js)<br>❤️ ★ぞろり★

## v31.4.1 ([2023-04-15](https://github.com/cwtickle/danoniplus/releases/tag/v31.4.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v31.4.1/total)](https://github.com/cwtickle/danoniplus/archive/v31.4.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v31.4.1/support/v31?style=social)](https://github.com/cwtickle/danoniplus/compare/v31.4.1...support/v31#files_bucket)
- 🛠️ カスタムキーの **posX** に空が指定されたときに初期化されるよう処理を改善 ( PR [#1462](https://github.com/cwtickle/danoniplus/pull/1462) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v31.4.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v31.4.0...v31.4.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v31.4.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v31.4.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v31.4.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v31.4.1/js/danoni_main.js)

## v31.4.0 ([2023-04-08](https://github.com/cwtickle/danoniplus/releases/tag/v31.4.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v31.4.0/total)](https://github.com/cwtickle/danoniplus/archive/v31.4.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v31.4.0/support/v31?style=social)](https://github.com/cwtickle/danoniplus/compare/v31.4.0...support/v31#files_bucket)
- ⭐ キー割当設定について KeyboardEvent.code の値及び略名が使えるよう変更 ( PR [#1460](https://github.com/cwtickle/danoniplus/pull/1460) )
- ⭐ リトライ、タイトルバックのショートカットキー設定について KeyboardEvent.code の値及び略名が使えるよう変更 ( PR [#1460](https://github.com/cwtickle/danoniplus/pull/1460) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v31.4.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v31.3.1...v31.4.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v31.4.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v31.4.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v31.4.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v31.4.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v31.4.0/js/lib/danoni_constants.js)

## v31.3.1 ([2023-04-05](https://github.com/cwtickle/danoniplus/releases/tag/v31.3.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v31.3.1/total)](https://github.com/cwtickle/danoniplus/archive/v31.3.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v31.3.1/support/v31?style=social)](https://github.com/cwtickle/danoniplus/compare/v31.3.1...support/v31#files_bucket)
- 🛠️ カスタムキー定義で **keyExtraList** の指定を原則不要化 ( PR [#1455](https://github.com/cwtickle/danoniplus/pull/1455) )
- 🛠️ カスタムキー定義で **charaX** の指定を任意に変更 ( PR [#1456](https://github.com/cwtickle/danoniplus/pull/1456), [#1458](https://github.com/cwtickle/danoniplus/pull/1458) )
- 🛠️ 設定画面の位置調整用の定数を g_limitObj, g_graphColor, g_settingPosへ移行 ( PR [#1454](https://github.com/cwtickle/danoniplus/pull/1454), Issue [#1453](https://github.com/cwtickle/danoniplus/pull/1453) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v31.3.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v31.2.0...v31.3.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v31.3.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v31.3.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v31.3.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v31.3.1/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v31.3.1/js/lib/danoni_constants.js)

## v31.2.0 ([2023-04-01](https://github.com/cwtickle/danoniplus/releases/tag/v31.2.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v31.2.0/total)](https://github.com/cwtickle/danoniplus/archive/v31.2.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v31.2.0/support/v31?style=social)](https://github.com/cwtickle/danoniplus/compare/v31.2.0...support/v31#files_bucket)
- ⭐ 譜面エフェクトデータ及び初期矢印色データに別譜面の変数名が入っていた場合、別譜面のデータを参照するよう変更 ( PR [#1451](https://github.com/cwtickle/danoniplus/pull/1451) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v31.2.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v31.1.0...v31.2.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v31.2.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v31.2.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v31.2.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v31.2.0/js/danoni_main.js)

## v31.1.0 ([2023-03-25](https://github.com/cwtickle/danoniplus/releases/tag/v31.1.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v31.1.0/total)](https://github.com/cwtickle/danoniplus/archive/v31.1.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v31.1.0/support/v31?style=social)](https://github.com/cwtickle/danoniplus/compare/v31.1.0...support/v31#files_bucket)
- ⭐ 譜面密度グラフについて、上位3番目まで色付けするよう変更 ( PR [#1449](https://github.com/cwtickle/danoniplus/pull/1449) )
- 🛠️ キーコンフィグ画面のShapeGroup及びColorGroupのオンマウス説明文を変更 ( PR [#1448](https://github.com/cwtickle/danoniplus/pull/1448) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v31.1.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v31.0.1...v31.1.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v31.1.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v31.1.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v31.1.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v31.1.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v31.1.0/js/lib/danoni_constants.js)

## v31.0.1 ([2023-03-22](https://github.com/cwtickle/danoniplus/releases/tag/v31.0.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v31.0.1/total)](https://github.com/cwtickle/danoniplus/archive/v31.0.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v31.0.1/support/v31?style=social)](https://github.com/cwtickle/danoniplus/compare/v31.0.1...support/v31#files_bucket)
- 🐞 **C)** 譜面リストで、上下キーを使って選択した譜面がキー数違いのときにキーコンフィグ画面へ移動すると画面が止まる問題を修正 ( PR [#1446](https://github.com/cwtickle/danoniplus/pull/1446) ) <- :boom: [**v24.5.1**](./Changelog-v24#v2451-2021-12-13)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v31.0.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v31.0.0...v31.0.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v31.0.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v31.0.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v31.0.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v31.0.1/js/danoni_main.js)

## v31.0.0 ([2023-03-20](https://github.com/cwtickle/danoniplus/releases/tag/v31.0.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v31.0.0/total)](https://github.com/cwtickle/danoniplus/archive/v31.0.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v31.0.0/support/v31?style=social)](https://github.com/cwtickle/danoniplus/compare/v31.0.0...support/v31#files_bucket)
- ⭐ 矢印・フリーズアローのヒット位置をステップゾーンからずらす機能を実装 ( PR [#1438](https://github.com/cwtickle/danoniplus/pull/1438) )
- ⭐ 矢印の回転、キャラクタに関するグループ（ShapeGroup）を実装 ( PR [#1440](https://github.com/cwtickle/danoniplus/pull/1440) )
- ⭐ キーコンフィグ画面においてConfigType設定を廃止 ( PR [#1440](https://github.com/cwtickle/danoniplus/pull/1440), Issue [#1430](https://github.com/cwtickle/danoniplus/pull/1430) )
- ⭐ 17keyにおいてシャッフル、カラー、シェイプグループを追加 ( PR [#1441](https://github.com/cwtickle/danoniplus/pull/1441), [#1442](https://github.com/cwtickle/danoniplus/pull/1442), [#1443](https://github.com/cwtickle/danoniplus/pull/1443), [#1444](https://github.com/cwtickle/danoniplus/pull/1444) )
- 🛠️ 9A, 11ikeyの一部キーパターンをシェイプグループへ移動 ( PR [#1440](https://github.com/cwtickle/danoniplus/pull/1440) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v31.0.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v30.6.0...v31.0.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v31.0.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v31.0.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v31.0.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v31.0.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v31.0.0/js/lib/danoni_constants.js)🔴<br>❤️ apoi, izkdic, すずめ (@suzme)

<img src="https://user-images.githubusercontent.com/44026291/226108006-0fe47d5c-8d20-4394-b1f6-7e0801309a05.png" width="70%">
<img src="https://user-images.githubusercontent.com/44026291/227749927-b1757ba9-dd28-4eb7-a235-0ac676a6cec3.png" width="70%">
<img src="https://user-images.githubusercontent.com/44026291/227749942-4b905cbe-6e10-4cd0-966d-8b0f4e9460a3.png" width="70%">

[**<- v32**](Changelog-v32) | **v31** | [**v30 ->**](Changelog-v30)  
