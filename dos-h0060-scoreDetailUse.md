**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-h0060-scoreDetailUse) | Japanese** 

[Home](./) &gt; [譜面ヘッダー仕様](dos_header) &gt; [設定時の初期設定](dos_header#-設定時の初期設定)

| [<- difSelectorUse](dos-h0051-difSelectorUse) | **scoreDetailUse** | [settingUse ->](dos-h0035-settingUse) |

## scoreDetailUse (chartDetailUse)
- 譜面明細表示の利用有無の設定

### 使い方
```
|scoreDetailUse=false|             // 全部非表示
|scoreDetailUse=Density,DifLevel|  // 譜面密度、レベル表示のみ表示
```
### 説明
設定画面において譜面の速度変化の遷移グラフ、譜面密度分布といった詳細表示を行うかどうかを設定します。  
指定しない場合は全て表示します。  
「Density」「Velocity」「DifLevel」から選択し、複数表示する場合はカンマで区切ります。  
※従来の名前 (Speed, ToolDif) も利用可能です。

|値|既定|内容|
|----|----|----|
|false||譜面明細表示を使用しない|
|Density||譜面密度グラフを表示|
|Velocity (Speed)||速度変化グラフを表示|
|DifLevel (ToolDif)||譜面情報を表示|
|HighScore||ハイスコアデータを表示|

<img src="https://github.com/cwtickle/danoniplus/assets/44026291/d2cd2d6b-a031-4590-bbb3-d055ddefa8b9" width="60%">

### 関連項目
- [startFrame](dos-h0005-startFrame) [:pencil:](dos-h0005-startFrame/_edit) プレイ開始フレーム数 

### 更新履歴

|Version|変更内容|
|----|----|
|[v36.2.0](https://github.com/cwtickle/danoniplus/releases/tag/v36.2.0)|・ハイスコア表示(HighScore)を追加|
|[v32.7.0](https://github.com/cwtickle/danoniplus/releases/tag/v32.7.0)|・"chartDetailUse"での指定に対応（利用方法は同じ）|
|[v32.5.0](https://github.com/cwtickle/danoniplus/releases/tag/v32.5.0)|・速度変化グラフ、譜面情報の設定名(Velocity, DifLevel)を追加|
|[v32.2.0](https://github.com/cwtickle/danoniplus/releases/tag/v32.2.0)|・譜面明細子画面別の表示／非表示設定を追加|
|[v11.3.0](https://github.com/cwtickle/danoniplus/releases/tag/v11.3.0)|・初回実装|

| [<- difSelectorUse](dos-h0051-difSelectorUse) | **scoreDetailUse** | [settingUse ->](dos-h0035-settingUse) |