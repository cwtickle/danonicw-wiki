**[English](https://github.com/cwtickle/danoniplus-docs/wiki/tips-0004-extraKeys) | Japanese** 

[^ Tips Indexに戻る](./tips-index)

|| **カスタムキーテンプレート** | [カスタムキーの作成方法](./tips-0015-make-custom-key-types) > |

# カスタムキーテンプレート
- カスタムキー定義については [キーの仕様について](./keys) もご覧ください。
- Dancing☆Onigiriエディター (CW Edition対応)における定義方法については[こちらのページ](https://github.com/superkuppabros/danoni-editor/wiki/%E9%9D%9E%E6%A8%99%E6%BA%96%E3%82%AD%E3%83%BC%E3%81%AE%E3%82%A8%E3%83%87%E3%82%A3%E3%82%BF%E5%AE%9A%E7%BE%A9%E3%81%BE%E3%81%A8%E3%82%81)をご覧ください。
- ver31以前の記法については[こちら](./tips-0004-extrakeys-v30)をご覧ください。

## 1key
<img src="https://user-images.githubusercontent.com/44026291/221399529-b7f00fb5-d53f-45a5-a8f1-857b07ed1086.png" width="50%">

### 譜面データ本体
```
|keyCtrl1=Space|
|chara1=space|
|color1=2|
|stepRtn1=onigiri|
```

## 5gkey
<img src="https://user-images.githubusercontent.com/44026291/221360877-393dee96-9b2a-44d8-9b4f-6930a4a34998.png" width="50%">

### 譜面データ本体
```
|keyCtrl5g=F1,F2,F3,F4,Enter/ShiftRight$D1,D2,D3,D4,Enter/ShiftRight$5_0$5_1$5_2|
|chara5g=5_0$5g_0$5_0$5_1$5_2|
|color5g=5_0$5g_0$5_0$5_1$5_2|
|stepRtn5g=0,45,135,180,giko$5g_0$5_0$5_1$5_2|
|blank5g=57.5|
|shuffle5g=5_0$5g_0$5_0$5_1$5_2|
|scroll5g=5_0$5g_0$5_0$5_1$5_2|
|transKey5g=$$5$5$5|
```

## 5pkey
<img src="https://user-images.githubusercontent.com/44026291/221357788-4b92a28a-f7a2-4352-9f92-a6329749b0d2.png" width="50%">

### 譜面データ本体
```
|keyCtrl5p=X/Z,E/W,D/F/S,T/R,V/C$B/N,Y/U,H/J/K,I/O,M/Comma$5_0$5_1$5_2|
|chara5p=5_2$5p_0$5_0$5_1$5_2|
|color5p=0,1,2,1,0$5p_0$5_0$5_1$5_2|
|stepRtn5p=-45,45,onigiri,135,-135$5p_0$5_0$5_1$5_2|
|blank5p=57.5|
|shuffle5p=0,0,0,0,0$5p_0$5_0$5_1$5_2|
|scroll5p=5_2$5p_0$5_0$5_1$5_2|
|transKey5p=$$5$5$5|
```

## 6key
<img src="https://user-images.githubusercontent.com/44026291/221349590-689c9f8b-f0e5-4ee9-8430-460d91e09418.png" width="55%">

### 譜面データ本体
```
|keyCtrl6=K,O,L,P,Semicolon,Space$Space,K,O,L,P,Semicolon|
|chara6=left,leftdia,down,rightdia,right,space$space,left,leftdia,down,rightdia,right|
|color6=0,1,0,1,0,2$2,0,1,0,1,0|
|stepRtn6=0,45,-90,135,180,onigiri$onigiri,0,45,-90,135,180|
|shuffle6=1,1,1,1,1,0$0,1,1,1,1,1|
```

## 7ekey
<img src="https://user-images.githubusercontent.com/44026291/221357898-f16a7765-d4d6-4c8f-82d9-4ff8fa382d52.png" width="70%">

### 譜面データ本体
```
|keyCtrl7e=Tab,S,E,D,R,F,N,D6,D7$D7,D8,N,L,P,Semicolon,Ja-@,Ja-Colon,Enter|
|chara7e=oni,left,down,gor,up,right,space,sleft,sright$sright,sleft,space,right,up,gor,down,left,oni|
|color7e=2,0,1,0,1,0,2,3,3$3,3,2,0,1,0,1,0,2|
|stepRtn7e=onigiri,0,45,-90,135,180,onigiri,iyo,giko$giko,iyo,onigiri,0,45,-90,135,180,onigiri|
|blank7e=52.5|
|shuffle7e=2,0,0,0,0,0,1,0,0$0,0,1,0,0,0,0,0,2|
|scroll7e=Cross::1,-,-,-,-,-,-,1,1$Cross::1,1,-,-,-,-,-,-,1|
```

## 8ikey
<img src="https://user-images.githubusercontent.com/44026291/221349785-4c17952a-35e0-44dd-934e-876658c9a3b4.png" width="60%">

### 譜面データ本体
```
|keyCtrl8i=A/Z,S/X,D/C,5_1$8_0$8_1$8_2$8_3$8_4$8_5|
|chara8i=8_0$left,leftdia,down,up,rightdia,right,sleft,space$space,left,leftdia,down,up,rightdia,right,sleft$8i_3$8i_3$8i_3$8i_3|
|color8i=1,1,1,2,0,0,0,0$8_0$8_1$8_2$8_3$8_4$8_5|
|stepRtn8i=0,-90,180,5_1/giko,morara,iyo,5_1$8_0$8_1$8_2$8_3$8_4$8_5|
|shuffle8i=0,0,0,2,1,1,1,1$8_0$8_1$8_2$8_3$8_4$8_5|
|transKey8i=$8$8$12$12$12$12|
```

## 9dkey
<img src="https://github.com/cwtickle/danoniplus/assets/44026291/a8c6349b-47c9-4876-91c6-7970ebcf5246" width="70%">

### 譜面データ本体
```
|minWidth9d=650|
|keyCtrl9d=S,D,F,V,B,N,J,K,L|
|chara9d=9B_0|
|color9d=0,1,0,2,2,2,0,1,0|
|stepRtn9d=0,-45,-90,giko,onigiri,iyo,90,135,180|
|shuffle9d=0,0,0,1,1,1,2,2,2|
|scroll9d=9B_0/Cross::1,1,1,-,-,-,1,1,1|
|assist9d=AA::0,0,0,1,1,1,0,0,0|
```

## 9gkey
<img src="https://user-images.githubusercontent.com/44026291/221360972-d8a1e505-4c46-444c-a237-e83ef4ae58ff.png" width="70%">

### 譜面データ本体
```
|keyCtrl9g=F1,F2,F3,F4,F5,F6,F7,F8,Enter/ShiftRight$D1,D2,D3,D4,D5,D6,D7,D8,Enter/ShiftRight|
|chara9g=left,down,up,right,sleft,sdown,sup,sright,space$9g_0|
|color9g=0,0,0,0,1,1,1,1,2$9g_0|
|stepRtn9g=0,45,135,180,0,45,135,180,giko$9g_0|
|blank9g=52.5|
|shuffle9g=0,0,0,0,1,1,1,1,2$9g_0|
|scroll9g=Cross::1,1,-,-,-,-,1,1,1/Split::1,1,1,1,-,-,-,-,-/Alternate::1,-,1,-,1,-,1,-,1$9g_0|
```

## 9jkey
<img src="https://user-images.githubusercontent.com/44026291/221361969-48acb80f-b78d-4f90-9f86-8159caba3bed.png" width="70%">

### 譜面データ本体
```
|keyCtrl9j=Tab,7_0,Enter$9A_0$9A_1$9A_2|
|chara9j=9A_0$9A_0$9A_1$9A_2|
|color9j=2,7_0_0,2$9A_0$9A_1$9A_2|
|stepRtn9j=giko,7_0_0,iyo$9A_0$9A_1$9A_2|
|shuffle9j=1,7_0_0,1$9A_0$9A_1$9A_2|
|scroll9j=9A_0/Cross::1,1,1,-,-,-,1,1,1/AA-Split::1,-,-,-,1,-,-,-,1$9A_0$9A_1$9A_2|
|transKey9j=$9A$9A$9B|
```

## 9tkey
- 9tkeyはカスタムキー定義だけで完結しません。下記のページをご覧ください。
https://github.com/suzme/danoni-9t

## 9vkey
<img src="https://user-images.githubusercontent.com/44026291/221349811-b692ccbf-9581-4332-9f52-10b016029a24.png" width="70%">

### 譜面データ本体
```
|keyCtrl9v=D4,R,F,V,Space,N,J,I,D9$9B_0|
|chara9v=9B_0$9B_0|
|color9v=9B_0$9B_0|
|stepRtn9v=90,120,150,180,onigiri,0,30,60,90$9B_0|
|shuffle9v=9B_0$9B_0|
|scroll9v=---::1,1,-,-,-,-,-,1,1/flat::1,1,1,1,1,1,1,1,1$9B_0|
|transKey9v=$9B|
```

## 10key
<img src="https://user-images.githubusercontent.com/44026291/221349843-63ec207f-6f2f-4d8d-b747-bef4f711b0af.png" width="70%">

### 譜面データ本体
```
|minWidth10=650|
|keyCtrl10=9A_0,Enter|
|chara10=9A_0,sspace|
|color10=9A_0_0,2|
|stepRtn10=9A_0_0,onigiri|
|blank10=52.5|
|shuffle10=9A_0_0,3/9A_0_1,1|
|scroll10=Cross::9A_0,1/Split::9A_0,-/Alternate::9A_0,-/Twist::9A_0,-/Asymmetry::9A_0,1/AA-Split::-,-,-,-,1,-,-,-,-,1|
```

## 10Akey
<img src="https://github.com/user-attachments/assets/bf61fe5e-89e2-4e0a-9186-ba19b325f12a" width="60%">

### 譜面データ本体
```
|minWidth10A=700|
|keyCtrl10A=E/W/R/Q/T/D3/D4/D2/D5,I/U/O/Y/P/D8/D9/D7/D0,Up/Down/Left/Right,7_0$11_0$11L_0$11W_0$12_0|
|chara10A=sleft,sdown,sright,7_0$11_0$11L_0$11W_0$12_0|
|color10A=3,3,3,7_0_0$11_0$11L_0$11W_0$12_0|
|stepRtn10A=giko,c,iyo,7_0_0$11_0$11L_0$11W_0$12_0|
|pos10A=1.5,4.5,8,9,10,11,12,13,14,15$11_0$11L_0$11W_0$12_0|
|div10A=9$11_0$11L_0$11W_0$12_0|
|shuffle10A=2,2,2,7_0_0$11_0$11L_0$11W_0$12_0|
|scroll10A=Flat::1,1,1,-,-,-,-,-,-,-$11_0$11L_0$11W_0$12_0|
|transKey10A=$11$11L$11W$12|
|keyGroup10A=0/1,0/1,0/1,0/2,0/2,0/2,0/2,0/2,0/2,0/2|
|keyGroupOrder10A=1,2|
```

## 10pkey
<img src="https://user-images.githubusercontent.com/44026291/221349870-272e0ede-c691-4eb5-8508-bd97bff397c3.png" width="70%">

### 譜面データ本体
```
|minWidth10p=650|
|keyCtrl10p=X/Z,E/W,D/F/S,T/R,V/C,B/N,Y/U,H/J/K,I/O,M/Comma$X/Z,W/Q,D/S/F,R/T,C/V,M/N,U/Y,K/J/L,O/P,Comma/Period$11i_0|
|chara10p=left,down,gor,up,right,sleft,sdown,siyo,sup,sright$10p_0$11i_0|
|color10p=0,1,2,1,0,0,1,2,1,0$10p_0$11i_0|
|stepRtn10p=-45,45,onigiri,135,-135,-45,45,onigiri,135,-135$10p_0$11i_0|
|blank10p=50|
|shuffle10p=0,0,0,0,0,0,0,0,0,0$10p_0$11i_0|
|scroll10p=Cross::1,1,1,-,-,-,-,1,1,1/Split::1,1,1,1,1,-,-,-,-,-/Alternate::1,-,1,-,1,1,-,1,-,1$10p_0$11i_0|
|transKey10p=$$11i|
```

## 11jkey
<img src="https://user-images.githubusercontent.com/44026291/221349884-3f1497f6-8fa8-470b-8ff8-c51e41a7b884.png" width="70%">

### 譜面データ本体
```
|minWidth11j=650|
|keyCtrl11j=Tab,9A_0,Enter|
|chara11j=gor,9A_0,siyo|
|color11j=2,9A_0_0,2|
|stepRtn11j=giko,9A_0_0,iyo|
|blank11j=50|
|shuffle11j=3,9A_0_0,4/1,9A_0_1,1|
|scroll11j=Cross::1,9A_0,1/Split::1,9A_0,-/Alternate::1,-,1,-,1,-,1,-,1,-,1/AA-Split::1,-,-,-,-,1,-,-,-,-,1|
```

## 11fkey
<img src="https://user-images.githubusercontent.com/44026291/221349907-d8388d66-09b7-4dc1-8d4d-ec6462b7b0b0.png" width="70%">

### 譜面データ本体
```
|minWidth11f=650|
|keyCtrl11f=S,E,D,R,F,Space,J,I,K,O,L$X,D,C,F,V,Space,N,J,M,K,Comma$E,R,I,O,7_0$11i_0|
|chara11f=7_0,sleft,sdown,sup,sright$11f_0$leftdia,space,sleft,sup,left,down,up,rightdia,right,sdown,sright$11f_0|
|color11f=0,1,0,1,0,2,0,1,0,1,0$11f_0$11_0$11i_0|
|stepRtn11f=0,45,-90,135,180,onigiri,0,45,-90,135,180$11f_0$11_0$11i_0|
|pos11f=$$0,1,4,5,6,7,8,9,10,11,12$|
|div11f=$$6$|
|blank11f=50$11f_0$55$11f_0|
|shuffle11f=0,0,0,0,0,1,2,2,2,2,2$11f_0$11_0$11i_0|
|scroll11f=11i_0$11f_0$Split::1,1,-,-,1,1,1,-,-,-,-$11f_0|
|assist11f=Left::1,1,1,1,1,0,0,0,0,0,0/Right::0,0,0,0,0,0,1,1,1,1,1$11f_0$$11f_0|
|transKey11f=$$11F$11i|
```

## 11gkey
<img src="https://user-images.githubusercontent.com/44026291/221349937-86fe3ce9-5ac3-40d0-9e5f-a40209ff9c1b.png" width="70%">

### 譜面データ本体
```
|minWidth11g=650|
|keyCtrl11g=Z,S,X,D/F,C/V,Space,M/N,K/J,Comma,L,Period$S,E,D,R,F,Space,J,I,K,O,L$E,R,I,O,7_0$11i_0|
|chara11g=left,down,up,right,space,tspace,sleft,sdown,sup,sright,sspace$11g_0$down,right,sdown,sright,left,up,space,tspace,sleft,sup,sspace$11g_0|
|color11g=0,1,0,1,0,2,0,1,0,1,0$11g_0$11_0$11i_0|
|stepRtn11g=0,-30,-60,-90,-120,onigiri,60,90,120,150,180$0,-45,-90,-135,180,onigiri,0,45,90,135,180$11_0$11i_0|
|pos11g=$$0,1,4,5,6,7,8,9,10,11,12$|
|div11g=$$6$|
|blank11g=50$11g_0$55$11g_0|
|shuffle11g=0,0,0,0,0,1,2,2,2,2,2$11g_0$11_0$11i_0|
|scroll11g=11i_0$11g_0$Split::1,1,-,-,1,1,1,-,-,-,-$11g_0|
|assist11g=Left::1,1,1,1,1,0,0,0,0,0,0/Right::0,0,0,0,0,0,1,1,1,1,1$11g_0$$11g_0|
|transKey11g=$11f$11F$11i|
```

## 12ikey
<img src="https://user-images.githubusercontent.com/44026291/221349966-82a28036-ecb6-405d-82c2-4be089a501b5.png" width="80%">

### 譜面データ本体
```
|minWidth12i=675|
|keyCtrl12i=F1,F2,F3,F4,F5,F6,F7,F8,F9,F10,F11,F12$Q,W,E,R,T,Y,U,I,O,P,Ja-@,Ja-[|
|chara12i=oni,left,leftdia,down,sleft,sdown,sup,sright,space,up,rightdia,right$12i_0|
|color12i=1,0,1,0,3,3,3,3,0,1,0,1$12i_0|
|stepRtn12i=45,0,-45,-90,giko,onigiri,iyo,c,90,135,180,225$12i_0|
|blank12i=50|
|shuffle12i=0,0,0,0,1,1,1,1,2,2,2,2$12i_0|
|scroll12i=Cross::1,1,1,1,-,-,-,-,1,1,1,1/Split::1,1,1,1,1,1,-,-,-,-,-,-$12i_0|
```

### 譜面データ本体(12keyの別キーとして定義)
- 下記の方法では、既存キーの設定に追記する方法を取っています。 
```
|minWidth12=675|
|append12=true|
|keyCtrl12=F1,F2,F3,F4,F5,F6,F7,F8,F9,F10,F11,F12$Q,W,E,R,T,Y,U,I,O,P,Ja-@,Ja-[|
|chara12=oni,left,leftdia,down,sleft,sdown,sup,sright,space,up,rightdia,right$12_(0)|
|color12=1,0,1,0,3,3,3,3,0,1,0,1$12_(0)|
|stepRtn12=45,0,-45,-90,giko,onigiri,iyo,c,90,135,180,225$12_(0)|
|blank12=50$12_(0)|
|shuffle12=0,0,0,0,1,1,1,1,2,2,2,2$12_(0)|
|scroll12=Cross::1,1,1,1,-,-,-,-,1,1,1,1/Split::1,1,1,1,1,1,-,-,-,-,-,-$12_(0)|
|transKey12=12i$12i|
```

## 14ekey
<img src="https://user-images.githubusercontent.com/44026291/221357975-acf6d090-52d8-4693-90d0-b940d4e340d3.png" width="90%">

### 譜面データ本体
```
|minWidth14e=950|
|keyCtrl14e=Tab,X,D,C,F,V,T,Y,U,I,Comma,L,Period,Semicolon,Slash,Enter|
|chara14e=aleft,bleft,adown,bdown,aup,bup,aright,bright,cleft,dleft,cdown,ddown,cup,dup,cright,dright|
|color14e=2,0,1,0,1,0,3,3,3,3,0,1,0,1,0,2|
|stepRtn14e=onigiri,0,45,-90,135,180,giko,onigiri,iyo,c,0,45,-90,135,180,onigiri|
|pos14e=0,1,2,3,4,5,6.5,7.5,8.5,9.5,11,12,13,14,15,16|
|blank14e=50|
|shuffle14e=1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1|
|scroll14e=Cross::1,-,-,-,-,-,1,1,1,1,-,-,-,-,-,1|
```

## 16key
<img src="https://github.com/user-attachments/assets/8a982905-fceb-4a26-ba09-35b930468749" width="70%">

### 譜面データ本体
```
|minWidth16=700|
|keyCtrl16=D1/D2,T,Space,Y,D0/Minus,S,D,F,V,G,B,H,N,J,K,L|
|chara16=sleft,sdown,space,sup,sright,aleft,adown,aup,gor,aright,oni,left,iyo,down,up,right|
|color16=0,1,2,1,0,3,2,3,4,3,4,3,4,3,2,3|
|stepRtn16=30,150,onigiri,30,150,0,-45,-90,giko,180,onigiri,0,iyo,90,135,180/30,150,onigiri,30,150,0,-45,-90,giko,180,monar,0,iyo,90,135,180/c,135,onigiri,45,morara,0,-45,-90,giko,180,monar,0,iyo,90,135,180|
|pos16=0.5,4,5,6,9.5,11,12,13,14,15,16,17,18,19,20,21|
|div16=11|
|shuffle16=0,0,3,0,0,1,1,1,2,1,2,1,2,1,1,1|
```

## 18key
<img src="https://github.com/cwtickle/danoniplus/assets/44026291/cd5820b1-5ae7-4089-969f-e6fa9b2c3cf8" width="70%">

### 譜面データ本体
```
|minWidth18=650|
|keyCtrl18=
W,E,D3/D4,R,Space,Left,Down,Up,Right,S,D,F,Z,X,C,J,K,L
W,E,D3/D4,R,Space,Left,Down,Up,Right,S,D,F,V,B,N,J,K,L
W,E,D3/D4,R,Space,U,I,D8/D9,O,S,D,F,Z,X,C,J,K,L
W,E,D3/D4,R,Space,U,I,D8/D9,O,S,D,F,V,B,N,J,K,L|
|chara18=sleft,sdown,sup,sright,space,aleft,adown,aup,aright,left,leftdia,down,gor,oni,iyo,up,rightdia,right$18_0$18_0$18_0|
|color18=3,3,3,3,2,4,4,4,4,0,1,0,2,2,2,0,1,0$18_0$18_0$18_0|
|div18=9$18_0$18_0$18_0|
|stepRtn18=9A_0,0,-45,-90,giko,onigiri,iyo,90,135,180$18_0$18_0$18_0|
|shuffle18=0,0,0,0,1,2,2,2,2,3,3,3,4,4,4,3,3,3$18_0$18_0$18_0|
|scroll18=Flat::1,1,1,1,1,1,1,1,1,-,-,-,-,-,-,-,-,-$18_0$18_0$18_0|
```

## 20key
<img src="https://user-images.githubusercontent.com/44026291/221358365-0cc27707-f5f5-40f2-976a-3b59d0cefa71.png" width="75%">

### 譜面データ本体
```
|minWidth20=850|
|keyCtrl20=W,E,D3/D4,R,U,I,D8/D9,O,Left,Down,Up,Right,8_2$20_0|
|chara20=aleft,adown,aup,aright,sleft,sdown,sup,sright,bleft,bdown,bup,bright,oni,left,leftdia,down,space,up,rightdia,right$20_0|
|color20=4,4,4,4,3,3,3,3,4,4,4,4,8_2_0$20_0|
|stepRtn20=0,-90,90,180,0,-90,90,180,0,-90,90,180,8_2_0$20_0|
|pos20=0,1,2,3,5,6,7,8,10,11,12,13,14,15,16,17,18,19,20,21$|
|div20=14,23$12,21|
|blank20=50|
|shuffle20=3,3,3,3,0,0,0,0,4,4,4,4,1,2,2,2,2,2,2,2$20_0|
```

### 「気分転換にRPG」のエディター(Nkey版)

<details>
<summary>ここをクリックして表示</summary>

```
map=11110121212133331111

//ここから出力内容
[header]

|aleft[i]_data=[a00]
|adown[i]_data=[a01]
|aup[i]_data=[a02]
|aright[i]_data=[a03]

|oni[i]_data=[a04]

|left[i]_data=[a05]
|leftdia[i]_data=[a06]
|down[i]_data=[a07]
|space[i]_data=[a08]
|up[i]_data=[a09]
|rightdia[i]_data=[a10]
|right[i]_data=[a11]

|sleft[i]_data=[a12]
|sdown[i]_data=[a13]
|sup[i]_data=[a14]
|sright[i]_data=[a15]

|bleft[i]_data=[a16]
|bdown[i]_data=[a17]
|bup[i]_data=[a18]
|bright[i]_data=[a19]

|afrzLeft[i]_data=[f00]
|afrzDown[i]_data=[f01]
|afrzUp[i]_data=[f02]
|afrzRight[i]_data=[f03]

|foni[i]_data=[f04]

|frzLeft[i]_data=[f05]
|frzLdia[i]_data=[f06]
|frzDown[i]_data=[f07]
|frzSpace[i]_data=[f08]
|frzUp[i]_data=[f09]
|frzRdia[i]_data=[f10]
|frzRight[i]_data=[f11]

|sfrzLeft[i]_data=[f12]
|sfrzDown[i]_data=[f13]
|sfrzUp[i]_data=[f14]
|sfrzRight[i]_data=[f15]

|bfrzLeft[i]_data=[f16]
|bfrzDown[i]_data=[f17]
|bfrzUp[i]_data=[f18]
|bfrzRight[i]_data=[f19]

|speed[i]_change=[speed]
|boost[i]_data=[boost]
[footer]

[intext]
|[creator]

//ここまで出力内容
```
</details>

## 21ikey
<img src="https://user-images.githubusercontent.com/44026291/221351254-50ac979d-345f-4bc5-b15e-9c313bef2617.png" width="80%">

### 譜面データ本体
```
|minWidth21i=750|
|keyCtrl21i=F1,F2,F3,F4,F5,F6,F7,F8,F9,F10,F11,F12,9B_0$21i_0|
|chara21i=f1key,f2key,f3key,f4key,f5key,f6key,f7key,f8key,f9key,f10key,f11key,f12key,9B_0$21i_0|
|color21i=1,0,1,0,3,3,3,3,0,1,0,1,4,0,4,0,2,0,4,0,4$21i_0|
|stepRtn21i=45,0,-45,-90,giko,onigiri,iyo,c,90,135,180,225,9B_0_0$21i_0|
|div21i=12$12,23|
|shuffle21i=0,0,0,0,1,1,1,1,2,2,2,2,3,3,3,3,4,3,3,3,3$21i_0|
```

## himsiyauz key
<img src="https://github.com/user-attachments/assets/7fba5d50-bf3d-4a6e-a30d-83dc37404e48" width="50%">

### 譜面データ本体
```
|keyCtrlhimsiyauz=H,I,M,S,D1,Y,A,U,Z|
|charahimsiyauz=hx,ix,mx,sx,1x,yx,ax,ux,zx|
|stepRtnhimsiyauz=-45,180,-90,135,giko,0,45,-90,-90|
|poshimsiyauz=11.5,6,13,10,0,4,8,5,9|
|colorhimsiyauz=1,3,0,1,2,3,1,3,0|
|divhimsiyauz=7,15|
|shufflehimsiyauz=0,0,0,1,1,0,1,0,1|
```

## 動作確認バージョン
- ver32.0.0以降

## ページ作成者
- ティックル

## 関連項目
- [カスタムキーの作成方法](./tips-0015-make-custom-key-types)
- [キーの仕様について](./keys)
- [既存キーのキーパターン上書き](./tips-0006-keypattern-update)
- [共通設定ファイル仕様](./dos_setting) &gt; [カスタムキー定義](./dos-s0010-customKeys)
- [KeyCtrl属性で使用するキーコード](./KeyCtrlCodeList)

[^ Tips Indexに戻る](./tips-index)

|| **カスタムキーテンプレート** | [カスタムキーの作成方法](./tips-0015-make-custom-key-types) > |
