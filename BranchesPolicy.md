**[English](https://github.com/cwtickle/danoniplus-docs/wiki/BranchesPolicy) | Japanese** 

| [< 機能実装方針](./FunctionalPolicy) | **ブランチポリシー** | [バージョン管理ポリシー >](./VersioningPolicy) |

# ブランチポリシー / Branches Policy
- Dancing☆Onigiri (CW Edition)では、複数ブランチにより管理しています。  

<img src="https://github.com/cwtickle/danoniplus/assets/44026291/b1ed0a5a-5cb9-4699-9083-5294bdd05762" width="100%">

## master
- リリース専用ブランチ。  
コミット間違いを除き、このブランチからリリースされます。  
developブランチ以外からmasterブランチへマージすることはありません。  

## develop
- 他ブランチからのマージを受けるブランチ。  
Contributor及び管理者はこのブランチへPull Requestを行います。  
原則としてこのブランチからの直接コミットはバージョン番号を上げる場合のみです。  
- マージ間違いを防ぐため、デフォルトブランチに設定しています。  
リリース直前のブランチですが、修正が完了していないことがあります。  
このブランチのリソースを利用する場合は、リリース前であることを理解した上で利用をお願いします。

## support
- 過去バージョンのメンテナンスブランチ。  
過去バージョンでは機能追加は行われず、最新版で修正した不具合の関連修正のみ行われます。  
このブランチから他のブランチへマージすることはありません。  

## feature/hotfix/chore
- 開発用ブランチ。特に規則は設けていませんが、管理者がPRを出すときは下記のルールでブランチを切ります。
- ブランチの生成元はいずれも**develop**ブランチで、特に区別はしていません。

|種類|概要|
|----|----|
|**feature/\***|機能追加や改善時に作成するブランチ|
|**hotfix/\***|不具合修正を目的とするときに作成するブランチ|
|**chore/\***|Readme、各種テンプレートの修正など機能に影響しないものを修正する場合に作成するブランチ|

<!--
```mermaid
gitGraph
   commit id:"000" tag:"v19.5.0"
   branch develop
   checkout develop
   commit id:"001"
   branch featureA
   commit id:"002-1"
   commit id:"002-2"
   checkout develop
   merge featureA
   branch featureB
   commit id:"003-1"
   commit id:"003-2"
   checkout develop
   merge featureB
   commit id:"004"
   checkout main
   merge develop tag:"v19.6.0"
   checkout main
   branch featureC
   commit id:"004-1"
   commit id:"004-2"
   checkout main
   branch support/v19
   checkout develop
   merge featureC
   checkout support/v19
   commit id:"005-1" tag:"v19.6.1"
   commit id:"005-2" tag:"v19.6.2"
   checkout develop
   commit id:"006"
   checkout main
   merge develop tag:"v20.0.0"
```
-->

| [< 機能実装方針](./FunctionalPolicy) | **ブランチポリシー** | [バージョン管理ポリシー >](./VersioningPolicy) |
