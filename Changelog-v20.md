**[English](https://github.com/cwtickle/danoniplus-docs/wiki/Changelog-v20) | Japanese** 

[^ Back to All Version List (Security Policy)](https://github.com/cwtickle/danoniplus/security/policy)  
[^ Back to Update Info](./UpdateInfo#-v20-changelog)

[**<- v21**](Changelog-v21) | **v20** | [**v19 ->**](Changelog-v19)  
(**🔖 [10 Releases](https://github.com/cwtickle/danoniplus/releases?q=tag%3Av20)** )

## 🔃 Files changed (v20)

|Directory|FileName||Last Updated|
|----|----|----|----|
|/js|danoni_main.js|[📥](https://github.com/cwtickle/danoniplus/releases/download/v20.5.4/danoni_main.js)|**v20.5.4**|
|/js/lib|danoni_constants.js|[📥](https://github.com/cwtickle/danoniplus/releases/download/v20.5.4/danoni_constants.js)|**v20.5.4**|

<details>
<summary>Changed file list before v19</summary>

|Directory|FileName||Last Updated|
|----|----|----|----|
|/js/lib|danoni_localbinary.js|[📥](https://github.com/cwtickle/danoniplus/releases/download/v15.1.0/danoni_localbinary.js)|[v15.1.0](Changelog-v15#v1510-2020-05-21)|
|/css|danoni_main.css|[📥](https://github.com/cwtickle/danoniplus/releases/download/v18.5.0/danoni_main.css)|[v18.5.0](Changelog-v18#v1850-2020-12-20)|
|/img|aaShadow.svg<br>arrow.svg<br>arrowShadow.svg<br>borderline.svg<br>c.svg<br>cursor.svg<br>giko.svg<br>iyo.svg<br>monar.svg<br>morara.svg<br>onigiri.svg|[📥](https://github.com/cwtickle/danoniplus/releases/download/v15.1.0/img.zip)|[v15.1.0](Changelog-v15#v1510-2020-05-21)|
|/skin|danoni_skin_default.css<br>danoni_skin_light.css<br>danoni_skin_skyblue.css|[📥](https://github.com/cwtickle/danoniplus/releases/download/v14.1.0/danoni_skin_default.css)<br>[📥](https://github.com/cwtickle/danoniplus/releases/download/v14.1.0/danoni_skin_light.css)<br>[📥](https://github.com/cwtickle/danoniplus/releases/download/v14.1.0/danoni_skin_skyblue.css)|[v14.1.0](Changelog-v14#v1410-2020-05-01)|

</details>

⭐ New Features / 🛠️ Improvements / 🐞 Bug Fixes  
( Files changed: ⚪ danoni_main.js / 🔵 danoni_constants.js / 🔴 css files )

## [Warning] 更新終了後の不具合情報
- [更新を終了したバージョンの不具合情報#v20](DeprecatedVersionBugs#v20) を参照

## v20.5.4 ([2021-05-16](https://github.com/cwtickle/danoniplus/releases/tag/v20.5.4))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v20.5.4/total)](https://github.com/cwtickle/danoniplus/archive/v20.5.4.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v20.5.4/support/v20?style=social)](https://github.com/cwtickle/danoniplus/compare/v20.5.4...support/v20#files_bucket) 
- 🐞 17key(パターン2)のシャッフルグループ間違いを修正 ( PR [#1084](https://github.com/cwtickle/danoniplus/pull/1084) ) <- :boom: [**v3.1.0**](Changelog-v3#v310-2019-02-26)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v20.5.4)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v20.5.3...v20.5.4#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v20.5.4)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v20.5.4.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v20.5.4.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v20.5.4/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v20.5.4/js/lib/danoni_constants.js)
/ 🎣 [**v22.4.1**](./Changelog-v22#v2241-2021-05-16)

## v20.5.3 ([2021-04-07](https://github.com/cwtickle/danoniplus/releases/tag/v20.5.3))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v20.5.3/total)](https://github.com/cwtickle/danoniplus/archive/v20.5.3.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v20.5.3/support/v20?style=social)](https://github.com/cwtickle/danoniplus/compare/v20.5.3...support/v20#files_bucket) 
- 🐞 空の矢印データを含む譜面にS-Randomをかけるとフルコン演出が早まる問題を修正
 ( PR [#1044](https://github.com/cwtickle/danoniplus/pull/1044), Gitter [2021-04-06](https://gitter.im/danonicw/community?at=606c808592a3431fd67b1640) ) <- :boom: [**v3.1.0**](Changelog-v3#v310-2019-02-26)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v20.5.3)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v20.5.2...v20.5.3#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v20.5.3)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v20.5.3.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v20.5.3.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v20.5.3/js/danoni_main.js)
/ 🎣 [**v21.4.2**](./Changelog-v21#v2142-2021-04-07)

----

## v20.5.2 ([2021-03-09](https://github.com/cwtickle/danoniplus/releases/tag/v20.5.2))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v20.5.2/total)](https://github.com/cwtickle/danoniplus/archive/v20.5.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v20.5.2/support/v20?style=social)](https://github.com/cwtickle/danoniplus/compare/v20.5.2...support/v20#files_bucket) 
- 🐞 グラデーション文字列にdeg/radなどが入っているときに文字が表示されない問題を修正 ( PR [#1014](https://github.com/cwtickle/danoniplus/pull/1014) ) <- :boom: [**v20.5.1**](Changelog-v20#v2051-2021-03-06)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v20.5.2)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v20.5.1...v20.5.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v20.5.2)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v20.5.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v20.5.2.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v20.5.2/js/danoni_main.js)

## v20.5.1 ([2021-03-06](https://github.com/cwtickle/danoniplus/releases/tag/v20.5.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v20.5.1/total)](https://github.com/cwtickle/danoniplus/archive/v20.5.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v20.5.1/support/v20?style=social)](https://github.com/cwtickle/danoniplus/compare/v20.5.1...support/v20#files_bucket) 
- 🛠️ Base64エンコードした曲データを直接デコードするよう変更 ( PR [#1009](https://github.com/cwtickle/danoniplus/pull/1009) )
- 🛠️ キーコンフィグ、グラデーション周りのコード整理 ( PR [#1006](https://github.com/cwtickle/danoniplus/pull/1006), [#1007](https://github.com/cwtickle/danoniplus/pull/1007), [#1008](https://github.com/cwtickle/danoniplus/pull/1008) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v20.5.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v20.4.0...v20.5.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v20.5.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v20.5.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v20.5.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v20.5.1/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v20.5.1/js/lib/danoni_constants.js)<br>❤️ すずめ (@suzme), MFV2 (@MFV2)

## v20.4.0 ([2021-02-27](https://github.com/cwtickle/danoniplus/releases/tag/v20.4.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v20.4.0/total)](https://github.com/cwtickle/danoniplus/archive/v20.4.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v20.4.0/support/v20?style=social)](https://github.com/cwtickle/danoniplus/compare/v20.4.0...support/v20#files_bucket) 
- ⭐ タイトル画面、ロード画面(iOSのみ)のショートカットキーを追加 ( PR [#1002](https://github.com/cwtickle/danoniplus/pull/1002) )
- 🛠️ ショートカットキーでキーコンフィグ画面へ移動した場合、0.5秒の待ち時間を設けるよう変更 ( PR [#1002](https://github.com/cwtickle/danoniplus/pull/1002) )
- 🛠️ clearWindow関数にキーを押した状態の初期化処理、背景再描画処理を統合 ( PR [#1002](https://github.com/cwtickle/danoniplus/pull/1002), [#1003](https://github.com/cwtickle/danoniplus/pull/1003) ) 
- 🛠️ setShortcutEvent関数にcreateScTextCommon関数を包含するよう変更 ( PR [#1002](https://github.com/cwtickle/danoniplus/pull/1002) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v20.4.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v20.3.1...v20.4.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v20.4.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v20.4.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v20.4.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v20.4.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v20.4.0/js/lib/danoni_constants.js)

<img src="./wiki/changelog/v20_0001.png" alt="v20_0001" width="45%">
<img src="./wiki/changelog/v20_0002.png" alt="v20_0002" width="45%">

## v20.3.1 ([2021-02-24](https://github.com/cwtickle/danoniplus/releases/tag/v20.3.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v20.3.1/total)](https://github.com/cwtickle/danoniplus/archive/v20.3.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v20.3.1/support/v20?style=social)](https://github.com/cwtickle/danoniplus/compare/v20.3.1...support/v20#files_bucket) 
- ⭐ 設定名、結果画面用の省略名を置き換える機能を実装 ( PR [#995](https://github.com/cwtickle/danoniplus/pull/995) )
- 🛠️ 譜面選択リストにあるキー別フィルターボタンのID重複を解消 ( PR [#994](https://github.com/cwtickle/danoniplus/pull/994) )
- 🛠️ MacOSのキー表示関係、ランク表示部分、その他コード重複部分を見直し ( PR [#994](https://github.com/cwtickle/danoniplus/pull/994), [#996](https://github.com/cwtickle/danoniplus/pull/996), [#998](https://github.com/cwtickle/danoniplus/pull/998), [#1000](https://github.com/cwtickle/danoniplus/pull/1000) )
- 🛠️ フォントリスト取得関数にて優先フォントが指定できるよう変更 ( PR [#994](https://github.com/cwtickle/danoniplus/pull/994) )
- 🐞 タイトル文字、譜面名のフォントサイズに関する不具合の修正 ( PR [#997](https://github.com/cwtickle/danoniplus/pull/997) ) <- :boom: [**v20.1.2**](Changelog-v20#v2012-2021-02-14) 

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v20.3.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v20.2.1...v20.3.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v20.3.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v20.3.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v20.3.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v20.3.1/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v20.3.1/js/lib/danoni_constants.js)

<img src="./wiki/changelog/v20_0003.png" alt="v20_0003" width="75%">

<img src="./wiki/changelog/v20_0004.png" alt="v20_0004" width="75%">

## v20.2.1 ([2021-02-20](https://github.com/cwtickle/danoniplus/releases/tag/v20.2.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v20.2.1/total)](https://github.com/cwtickle/danoniplus/archive/v20.2.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v20.2.1/support/v20?style=social)](https://github.com/cwtickle/danoniplus/compare/v20.2.1...support/v20#files_bucket) 
- 🛠️ KeyConfig画面のTo Settingsボタンの表示部にg_lblNameObjを適用 ( PR [#986](https://github.com/cwtickle/danoniplus/pull/986) )
- 🛠️ ショートカット表示部分のフォーマット方法を拡張 ( PR [#987](https://github.com/cwtickle/danoniplus/pull/987) )
- 🛠️ imgタグ生成時においてcrossOrigin属性を付与するよう変更 ( PR [#988](https://github.com/cwtickle/danoniplus/pull/988) )
- 🐞 preloadFile関数において画像ファイル以外が指定された場合の問題を修正 ( PR [#989](https://github.com/cwtickle/danoniplus/pull/989) ) <- :boom: [**v18.5.0**](Changelog-v18#v1850-2020-12-20) 

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v20.2.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v20.2.0...v20.2.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v20.2.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v20.2.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v20.2.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v20.2.1/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v20.2.1/js/lib/danoni_constants.js)<br>❤️ ショウタ

## v20.2.0 ([2021-02-16](https://github.com/cwtickle/danoniplus/releases/tag/v20.2.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v20.2.0/total)](https://github.com/cwtickle/danoniplus/archive/v20.2.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v20.2.0/support/v20?style=social)](https://github.com/cwtickle/danoniplus/compare/v20.2.0...support/v20#files_bucket) 
- ⭐ ボタン・ショートカットキー有効化までのフレーム数設定を実装 ( PR [#982](https://github.com/cwtickle/danoniplus/pull/982) )
- ⭐ 11ikeyに方向表示が異なるキーパターンを追加 ( PR [#936](https://github.com/cwtickle/danoniplus/pull/936) )
- 🛠️ キーコンフィグ画面以外の戻るボタンのショートカットキーにShift+Tabを追加割り当て ( PR [#981](https://github.com/cwtickle/danoniplus/pull/981) )
- 🛠️ MacOS/iPadOSにおいてメイン画面、キーコンフィグ画面の割り当てキーを一部変更 ( PR [#981](https://github.com/cwtickle/danoniplus/pull/981) )
- 🐞 MacOS/iPadOSにおいて結果画面のCopyResultのショートカットキーを押した後、
他のキーを押すとCopyResultが動いてしまう問題を修正 ( PR [#981](https://github.com/cwtickle/danoniplus/pull/981) ) <- :boom: [**v19.5.0**](Changelog-v19#v1950-2021-02-09) 
- 🐞 対象が無いときにショートカットキーを使うとエラーになることがある問題を修正 ( PR [#983](https://github.com/cwtickle/danoniplus/pull/983) ) <- :boom: [**v19.5.0**](Changelog-v19#v1950-2021-02-09)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v20.2.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v20.1.2...v20.2.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v20.2.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v20.2.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v20.2.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v20.2.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v20.2.0/js/lib/danoni_constants.js)

<img src="./wiki/changelog/v20_0005.png" alt="v20_0005" width="75%">

## v20.1.2 ([2021-02-14](https://github.com/cwtickle/danoniplus/releases/tag/v20.1.2))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v20.1.2/total)](https://github.com/cwtickle/danoniplus/archive/v20.1.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v20.1.2/support/v20?style=social)](https://github.com/cwtickle/danoniplus/compare/v20.1.2...support/v20#files_bucket) 
- ⭐ タイトル文字、譜面名のフォントサイズの自動設定方法を改善 ( PR [#975](https://github.com/cwtickle/danoniplus/pull/975) )
- 🛠️ Adjustment設定のショートカットにテンキーの「+」「-」を追加で割り当て ( PR [#973](https://github.com/cwtickle/danoniplus/pull/973) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v20.1.2)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v20.0.0...v20.1.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v20.1.2)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v20.1.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v20.1.2.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v20.1.2/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v20.1.2/js/lib/danoni_constants.js)<br>❤️ MFV2 (@MFV2)

<img src="./wiki/changelog/v20_0006.png" alt="v20_0006" width="75%">

## v20.0.0 ([2021-02-12](https://github.com/cwtickle/danoniplus/releases/tag/v20.0.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v20.0.0/total)](https://github.com/cwtickle/danoniplus/archive/v20.0.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v20.0.0/support/v20?style=social)](https://github.com/cwtickle/danoniplus/compare/v20.0.0...support/v20#files_bucket) 
- ⭐ ボタン処理を画面移動系とそれ以外に分離 ( PR [#970](https://github.com/cwtickle/danoniplus/pull/970) )
- ⭐ ボタンの割り込み処理を本体処理より前に挿入できるよう変更 ( PR [#970](https://github.com/cwtickle/danoniplus/pull/970) )
- ⭐ 右クリック操作の無いボタンに対して処理を追加できるように変更 ( PR [#970](https://github.com/cwtickle/danoniplus/pull/970) )
- 🛠️ 古い関数・変数の整理、単独変数のオブジェクト集約 ( PR [#971](https://github.com/cwtickle/danoniplus/pull/971) )
- 🛠️ 判定周りのコード整理 ( PR [#969](https://github.com/cwtickle/danoniplus/pull/969) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v20.0.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v19.5.2..v20.0.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v20.0.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v20.0.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v20.0.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v20.0.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v20.0.0/js/lib/danoni_constants.js)

[**<- v21**](Changelog-v21) | **v20** | [**v19 ->**](Changelog-v19)
