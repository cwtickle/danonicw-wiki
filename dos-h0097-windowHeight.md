**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-h0097-windowHeight) | Japanese** 

[Home](./) &gt; [譜面ヘッダー仕様](dos_header) &gt; [クレジット関連、共通設定](dos_header#-クレジット関連共通設定)

| [<- windowWidth](dos-h0090-windowWidth) | **windowHeight** | [heightVariable ->](dos-h0100-heightVariable) |

## windowHeight
- 画面ウィンドウの高さ設定
- 共通設定 ⇒ [g_presetObj.autoMinHeight](dos-s0001-makerInfo#最小高さ設定-g_presetobjautominheight)

### 使い方
```
|windowHeight=570|
```
### 説明
この作品の画面ウィンドウの高さをpx単位で設定します。デフォルトは500pxです。 

<img src="https://github.com/cwtickle/danoniplus/assets/44026291/3ba6a858-cb7d-4bc5-aeea-a426094c1eaf" width="50%"><img src="https://github.com/cwtickle/danoniplus/assets/44026291/2c9eb38a-2f9d-46b4-937a-9522eb6b30d6" width="50%">

### 関連項目
- [**windowWidth**](dos-h0090-windowWidth) [:pencil:](dos-h0090-windowWidth/_edit) 画面ウィンドウの横幅

### 更新履歴

|Version|変更内容|
|----|----|
|[v33.6.0](https://github.com/cwtickle/danoniplus/releases/tag/v33.6.0)|・初回実装|

| [<- windowWidth](dos-h0090-windowWidth) | **windowHeight** | [heightVariable ->](dos-h0100-heightVariable) |