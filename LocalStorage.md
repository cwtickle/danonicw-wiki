**[English](https://github.com/cwtickle/danoniplus-docs/wiki/LocalStorage) | Japanese**

| [< 色付きオブジェクト仕様](./AboutColorObject) | **ローカルストレージ仕様** | [オーディオ仕様 >](./Audio) |

# ローカルストレージ仕様 / LocalStorage
- Dancing☆Onigiri (CW Edition)で使用しているローカルストレージのデータ格納仕様です。  
データはドメイン別に格納され、ドメインを跨いで共有されることはありません。  

## ロケール/共通
- キー：`danoni-locale`

|キー|設定値の例|補足|
|----|----|----|
|locale|Ja|言語(Ja: 日本語, En: 英語)|
|safeMode|OFF|セーフモード(ONで読込時にローカルストレージ情報を使用しない)|

## 作品別
- キー：作品URL (http://...)  
    - URLパラメーターの`scoreId`は無視される。
    - それ以外のURLパラメーターはURLの一部と見なされ、区別される。
- 設定値
    - ここでいうカスタムキーは、譜面側で定義した「keyExtraList」に掲載のあるキーを指します。  
共通設定ファイルの[共通カスタムキー定義 (g_presetObj.keysData)](./dos-s0010-customKeys)に掲載のキーは対象外です。

|キー|設定値の例|補足|
|----|----|----|
|adjustment|0|作品別のAdjustmentの初期値|
|volume|75|作品別のVolume(音量)の初期値|
|highscores|7k-Normal:<br>{ii:700, shakin:20, matari:3, ...}<br>11k-Hard:<br>{ii:1100, shakin:15, matari:2, ...}|譜面別のハイスコア情報を判定数別とスコアに分けて格納。<br>キー数＋譜面名(＋制作者名※)で区別しているため、<br>この組み合わせで重複が起こらないよう譜面名を決める必要がある<br>※譜面ヘッダー：[makerView](./dos-h0050-makerView)=trueを指定した場合のみ|
|reverseX|ON|カスタムキー(Xkey)におけるReverseの初期値|
|keyCtrlX|37, 40, (38,0), 39, 83, 68, 70, 32, 74, 75, 76|カスタムキー(Xkey)におけるキーコンフィグの初期値|
|keyCtrlPtnX|0|カスタムキー(Xkey)におけるベースとするキーコンフィグパターン|
|colorX|[3, 3, 3, 3, 0, 1, 0, 2, 0, 1, 0]|カスタムキー(Xkey)におけるレーンごとの色番号指定用配列|
|shuffleX|[0, 0, 0, 0, 1, 1, 1, 2, 3, 3, 3]|カスタムキー(Xkey)におけるレーンごとのシャッフル番号指定用配列|
|setColorX|["#ffffff", "#ccccff", "#99ccff", "#ffccff", "#ffcccc"]|カスタムキー(Xkey)における色番号毎の矢印色|
|setShadowColorX|["#66666660", "#6666ff60", "#3366cc60", "#99339960", "#99333360"]|カスタムキー(Xkey)における色番号毎の矢印色（影矢印）|
|frzColorX|[<br>　["#cccccc","#999999","#cccc33","#999933"],<br>　["#ff66e3","#0011ff","#cccc33","#999933"],<br>　["#66ffff","#6600ff","#cccc33","#999933"],<br>　["#cc99cc","#ff99ff","#cccc33","#999933"],<br>　["#ff6666","#ff9999","#cccc33","#999933"]<br>]|カスタムキー(Xkey)における色番号毎のフリーズアロー色（通常時の本体・帯、ヒット時の本体・帯）|
|frzShadowColorX|ー|カスタムキー(Xkey)における色番号毎のフリーズアロー色（影矢印）※枠だけあるが現状未使用|
|appearance|Visible|Appearanceの初期値|
|opacity|100|Opacityの初期値|
|hitPosition|0|hitPositionの初期値|
|colorType|Default|ColorTypeの初期値|

### ハイスコア(highscores)詳細
- ハイスコアの管理キー: "キー数(別キーモード時のキー数)-譜面名-アシスト情報-Mirror利用可否-制作者名"  
※制作者名は譜面ヘッダー: makerView の指定があった場合のみ。

|キー|設定値の例|補足|
|----|----|----|
|ii|700|ハイスコアのイイの判定数|
|shakin|20|ハイスコアのシャキンの判定数|
|matari|3|ハイスコアのマターリの判定数|
|shobon|0|ハイスコアのショボーンの判定数|
|uwan|0|ハイスコアのウワァンの判定数|
|kita|70|ハイスコアのキターの判定数|
|iknai|3|ハイスコアのイクナイの判定数|
|maxCombo|200|ハイスコアのMaxCombo数|
|fmaxCombo|50|ハイスコアのフリーズアローのMaxCombo数|
|score|947000|ハイスコア|
|rankMark|AAA|ハイスコア時のランク|
|rankColor|#ff6666|ハイスコア時のランク色|
|fast|30|ハイスコア時のFast数|
|slow|22|ハイスコア時のSlow数|
|adj|0.7|ハイスコア時の推定Adj（平均値ではない）|
|excessive|`---`|ハイスコア時のExcessive数（設定時のみ。未設定時は`---`に設定）|
|dateTime|2024/5/1 14:30:00|ハイスコア更新日時|
|playStyle|3.5x, Brake|ハイスコア時の設定オプション（CopyResult用）|
|fullCombo|true|フルコンボランプ (trueでランプ有効)|
|perfect||パーフェクトランプ (trueでランプ有効)|
|allPerfect||APランプ (trueでランプ有効)|
|clearLamps|Normal, Hard|クリアランプリスト (ゲージごと、配列で管理)|
|（カスタム変数）||ハイスコア時のカスタム変数値 (g_presetObj.resultVals で設定した変数)<br>※画面に表示できるのは `resultValsView` で指定した変数のみ|

#### 作品別 (廃止項目)

|キー|設定値の例|補足|
|----|----|----|
|d_stepzone|ON|Display:StepZoneの初期値|
|d_judgment|ON|Display:Judgmentの初期値|
|d_fastslow|ON|Display:FastSlowの初期値|
|d_lifegauge|ON|Display:LifeGaugeの初期値|
|d_score|ON|Display:Scoreの初期値|
|d_musicinfo|ON|Display:MusicInfoの初期値|
|d_filterline|ON|Display:FilterLineの初期値|


## キー別 (譜面側で定義したkeyExtraList指定キーを除く)
- キー：danonicw-Nk (Nにはキー数が入る)
- 設定値

|キー|設定値の例|補足|
|----|----|----|
|reverse|ON|Reverseの初期値|
|keyCtrl|37, 40, (38,0), 39, 83, 68, 70, 32, 74, 75, 76|キーコンフィグの初期値|
|keyCtrlPtn|0|ベースとするキーコンフィグパターン|
|color|[3, 3, 3, 3, 0, 1, 0, 2, 0, 1, 0]|レーンごとの色番号指定用配列|
|shuffle|[0, 0, 0, 0, 1, 1, 1, 2, 3, 3, 3]|レーンごとのシャッフル番号指定用配列|
|setColor|["#ffffff", "#ccccff", "#99ccff", "#ffccff", "#ffcccc"]|色番号毎の矢印色|
|setShadowColor|["#66666660", "#6666ff60", "#3366cc60", "#99339960", "#99333360"]|色番号毎の矢印色（影矢印）|
|frzColor|[<br>　["#cccccc","#999999","#cccc33","#999933"],<br>　["#ff66e3","#0011ff","#cccc33","#999933"],<br>　["#66ffff","#6600ff","#cccc33","#999933"],<br>　["#cc99cc","#ff99ff","#cccc33","#999933"],<br>　["#ff6666","#ff9999","#cccc33","#999933"]<br>]|色番号毎のフリーズアロー色（通常時の本体・帯、ヒット時の本体・帯）|
|frzShadowColor|ー|色番号毎のフリーズアロー色（影矢印）※枠だけあるが現状未使用|

### 更新履歴

|Version|変更内容|
|----|----|
|[v39.6.0](https://github.com/cwtickle/danoniplus/releases/tag/v39.6.0)|・ロケール(共通)設定にセーフモードを追加|
|[v36.2.0](https://github.com/cwtickle/danoniplus/releases/tag/v36.2.0)|・highscoresにFast/Slow、ランク等の情報を追加|
|[v31.5.0](https://github.com/cwtickle/danoniplus/releases/tag/v31.5.0)|・highscoresの管理キーに別キーモード時のキーとMirror可否の設定を追加|
|[v31.0.0](https://github.com/cwtickle/danoniplus/releases/tag/v31.0.0)|・作品別にHitPositionを自動保存する機能を実装|
|[v28.2.1](https://github.com/cwtickle/danoniplus/releases/tag/v28.2.1)|・作品別からDisplay設定の保存を取り止め|
|[v28.1.0](https://github.com/cwtickle/danoniplus/releases/tag/v28.1.0)|・キー別に矢印色、フリーズアロー色のセットを保存する機能を追加|
|[v24.0.0](https://github.com/cwtickle/danoniplus/releases/tag/v24.0.0)|・ロケール設定を追加|
|[v18.3.0](https://github.com/cwtickle/danoniplus/releases/tag/v18.3.0)|・作品別にColorTypeを自動保存する機能を実装|
|[v18.1.0](https://github.com/cwtickle/danoniplus/releases/tag/v18.1.0)|・作品別にDisplay設定、Appearance、Opacityを自動保存する機能を実装|
|[v8.7.0](https://github.com/cwtickle/danoniplus/releases/tag/v8.7.0)|・作品別のキーからscoreIdを除去するよう変更|
|[v6.6.0](https://github.com/cwtickle/danoniplus/releases/tag/v6.6.0)|・特殊キー(keyExtraList指定キー)に対してリバース、キーコンフィグ保存機能を実装|
|[v4.6.0](https://github.com/cwtickle/danoniplus/releases/tag/v4.6.0)|・キー毎にリバース、キーコンフィグを自動保存する機能を実装|
|[v4.3.0](https://github.com/cwtickle/danoniplus/releases/tag/v4.3.0)|・作品別にハイスコア時のスコア・判定情報を自動保存する機能を実装|
|[v4.1.0](https://github.com/cwtickle/danoniplus/releases/tag/v4.1.0)|・作品別にAdjustment, Volumeを自動保存する機能を実装（初回実装）|

| [< 色付きオブジェクト仕様](./AboutColorObject) | **ローカルストレージ仕様** | [オーディオ仕様 >](./Audio) |
