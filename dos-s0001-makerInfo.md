**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-s0001-makerInfo) | Japanese** 

[↑ 共通設定ファイル仕様に戻る](dos_setting)

[ <- カスタムキー定義 ](dos-s0010-customKeys) || **制作者クレジット・基本設定** | [カスタムファイル設定 -> ](dos-s0002-customFile)

## 制作者クレジット・基本設定
### 譜面製作者名 (g_presetObj.tuning)
⇒ 指定があった場合に優先される譜面ヘッダー：[tuning](dos-h0017-tuning)
- デフォルトの制作者名に指定する値を設定します。  
下記の譜面製作者URLと合わせて設定しておくと、譜面ヘッダー：[tuning](dos-h0017-tuning)を省略できます。
```javascript
g_presetObj.tuning = `name`;

g_presetObj.tuningEn = `name`; // 言語が英語のときに表示
g_presetObj.tuningJa = `名前`; // 言語が日本語のときに表示
```

### 譜面製作者URL (g_presetObj.tuningUrl)
⇒ 指定があった場合に優先される譜面ヘッダー：[tuning](dos-h0017-tuning)
- デフォルトの制作者名URLを設定します。 
```javascript
g_presetObj.tuningUrl = `https://www.google.co.jp/`;
```

### 自動横幅拡張設定 (g_presetObj.autoSpread)
⇒ 指定があった場合に優先される譜面ヘッダー：[autoSpread](dos-h0089-autoSpread)
- キー別の最小横幅を考慮した横幅になるよう調整するかどうかを設定します。  
デフォルトは`true`(有効)です。 
```javascript
g_presetObj.autoSpread = false;
```

### 最小横幅設定 (g_presetObj.autoMinWidth)
⇒ 指定があった場合に優先される譜面ヘッダー：[windowWidth](dos-h0090-windowWidth)
- 作品共通の最小の横幅を設定します。デフォルト値は`500px`です。  
この設定は自動横幅拡張設定が無効のときには機能しません。
```javascript
g_presetObj.autoMinWidth = 600;
```

### 最小高さ設定 (g_presetObj.autoMinHeight)
⇒ 指定があった場合に優先される譜面ヘッダー：[windowHeight](dos-h0097-windowHeight)
- 作品共通の最小の高さを設定します。デフォルト値は`500px`です。  
```javascript
g_presetObj.autoMinHeight = 500;
```

### 高さ可変設定 (g_presetObj.heightVariable)
⇒ 指定があった場合に優先される譜面ヘッダー：[heightVariable](dos-h0100-heightVariable)
- この作品の画面ウィンドウの高さをパラメータ`h`にて制御できるようにするかを設定します。デフォルトは`false`(制御しない)です。  
パラメータ`h`の制御範囲は400px～1000pxです。  
500px以下を指定した場合は高さは500pxとなり、プレイ画面のサイズが指定された値に縮小されます。 
```javascript
g_presetObj.heightVariable = true;
```

### ウィンドウ位置 (g_presetObj.windowAlign)
⇒ 指定があった場合に優先される譜面ヘッダー：[windowAlign](dos-h0091-windowAlign)
- 作品部分のウィンドウ位置を左寄せ(left)/中央(center)/右寄せ(right)から選択します。  
デフォルトは`center`(中央)です。
```javascript
g_presetObj.windowAlign = `left`;
```

## 更新履歴

|Version|変更内容|
|----|----|
|[v35.0.0](https://github.com/cwtickle/danoniplus/releases/tag/v35.0.0)|・高さ可変設定 (g_presetObj.heightVariable)を追加|
|[v33.7.0](https://github.com/cwtickle/danoniplus/releases/tag/v33.7.0)|・最小高さ設定 (g_presetObj.autoMinHeight)を追加|
|[v29.2.0](https://github.com/cwtickle/danoniplus/releases/tag/v29.2.0)|・譜面製作者名 (g_presetObj.tuning)に対して言語別の設定を追加|
|[v27.1.0](https://github.com/cwtickle/danoniplus/releases/tag/v27.1.0)|・自動横幅拡張設定 (g_presetObj.autoSpread)、最小横幅設定 (g_presetObj.autoMinWidth)、<br>　ウィンドウ位置 (g_presetObj.windowAlign)を追加|
|[v26.3.1](https://github.com/cwtickle/danoniplus/releases/tag/v26.3.1)|・変数名変更<br>　・g_presetTuning -> g_presetObj.tuning<br>　・g_presetTuningUrl -> g_presetObj.tuningUrl|
|[v3.0.0](https://github.com/cwtickle/danoniplus/releases/tag/v3.0.0)|・初期実装|

[ <- カスタムキー定義 ](dos-s0010-customKeys) || **制作者クレジット・基本設定** | [カスタムファイル設定 -> ](dos-s0002-customFile)