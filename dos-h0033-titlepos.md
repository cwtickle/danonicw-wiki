**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-h0033-titlepos) | Japanese** 

[Home](./) &gt; [譜面ヘッダー仕様](dos_header) &gt; [タイトル曲名文字(デフォルトデザイン)のエフェクト](dos_header#-タイトル曲名文字デフォルトデザインのエフェクト)

| [<- titlegrd / titleArrowgrd](dos-h0032-titlegrd) | **titlePos** | [titleLineHeight ->](dos-h0034-titlelineheight) |

## titlePos (titlepos)
- タイトル文字のX, Y座標位置の設定

### 使い方
```
|titlePos=-5,10|
|titlePos=5,10$30,0|
```
### 説明
デフォルトの曲名表示を使用した場合に、  
タイトル文字の位置をX座標, Y座標の順で調整できます。  
単位はpxです。  
customTitleUseがfalseに設定されているとき、この値は無視されます。  

ver17.3.0より、$区切りで2行目の位置を調整できるようになりました。  
1行目の中央位置を基準にx, y座標の順に指定します。  
y座標についてはtitleLineHeightとの同時適用が可能です。

#### 言語別設定 (ver29.3.0以降)

|通常設定|言語別設定(Ja)|言語別設定(En)|
|----|----|----|
|titlePos|titlePos**Ja**|titlePos**En**|
|titlepos|titlepos**Ja**|titlepos**En**|

### タイトル曲名文字(デフォルトデザイン)の設定箇所について
![dos-h0030-01.png](./wiki/dos-h0030-01.png)

### 関連項目
- [**titleSize**](dos-h0030-titlesize) [:pencil:](dos-h0030-titlesize/_edit) 文字サイズ
- [**titleFont**](dos-h0031-titlefont) [:pencil:](dos-h0031-titlefont/_edit) フォント
- [**titleLineHeight**](dos-h0034-titlelineheight) [:pencil:](dos-h0034-titlelineheight/_edit) 複数行の際の行間

### 更新履歴

|Version|変更内容|
|----|----|
|[v32.7.0](https://github.com/cwtickle/danoniplus/releases/tag/v32.7.0)|・キャメル表記での指定に対応|
|[v29.3.0](https://github.com/cwtickle/danoniplus/releases/tag/v29.3.0)|・言語別の設定に対応|
|[v17.3.0](https://github.com/cwtickle/danoniplus/releases/tag/v17.3.0)|・2行目の位置設定を追加|
|[v3.2.0](https://github.com/cwtickle/danoniplus/releases/tag/v3.2.0)|・初回実装|

| [<- titlegrd / titleArrowgrd](dos-h0032-titlegrd) | **titlePos** | [titleLineHeight ->](dos-h0034-titlelineheight) |