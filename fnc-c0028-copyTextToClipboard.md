[← オブジェクトリファレンスに戻る](ObjectReferenceIndex)  

# copyTextToClipboard
### 概要
- 引数に渡された文字列をクリップボードへコピーする関数。
- コピー完了後、指定されたメッセージを画面上に表示する。

### 引数（括弧内はデフォルト値）

|引数|型|必須|指定内容|
|----|----|----|----|
|_textVal|string|*|任意の文字列|
|_msg|string|*|コピー後に表示するメッセージ文字列|

### 返却値
- コピーコマンドを実行した結果

### 使用例
```javascript
// リザルトデータをクリップボードへコピー
createCss2Button(`btnCopy`, g_lblNameObj.b_copy, _ => {
	copyTextToClipboard(resultText);
	//makeInfoWindow(g_msgInfoObj.I_0001, `leftToRightFade`); <-- この部分はcopyTextToClipboardに内包
}, {
	x: g_sWidth / 4,
	w: g_sWidth / 2,
	h: C_BTN_HEIGHT * 5 / 8, siz: 24,
	animationName: `smallToNormalY`,
}, g_cssObj.button_Setting),
```

### フローチャート
```mermaid
flowchart
    A([start])-->B{クリップボードAPIが使用できる};
    B-->|Yes| C1["クリップボードAPIにより<br>引数に渡された値を<br>クリップボードへコピー"];
    B-->|No| C2["従来の方法(execCommand)で<br>引数に渡された値を<br>クリップボードへコピー"];
    C1-->D["コピー完了後メッセージを表示"];
    C2-->D;
    D-->E([end]);
```

### 更新履歴

|Version|変更内容|
|----|----|
|[v25.4.1](https://github.com/cwtickle/danoniplus/releases/tag/v25.4.1)|・http環境ではクリップボードAPIが使用できないため、従来の方法を再実装|
|[v25.4.0](https://github.com/cwtickle/danoniplus/releases/tag/v25.4.0)|・クリップボードAPIを使用する方法に変更<br>・引数にコピー後のメッセージを追加|
|[v12.0.0](https://github.com/cwtickle/danoniplus/releases/tag/v17.0.0)|・初回実装|
