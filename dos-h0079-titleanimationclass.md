**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-h0079-titleanimationclass) | Japanese** 

[Home](./) &gt; [譜面ヘッダー仕様](dos_header) &gt; [タイトル曲名文字(デフォルトデザイン)のエフェクト](dos_header#-タイトル曲名文字デフォルトデザインのエフェクト)

| [<- titleAnimation](dos-h0077-titleanimation) | **titleAnimationClass** | [titleArrowName ->](dos-h0099-titleArrowName) |

## titleAnimationClass (titleanimationclass)
- タイトル文字のアニメーションクラス設定

### 使い方
```
|titleAnimationClass=title|
```
### 説明
タイトル文字のアニメーション等で指定するクラスを指定します。  
実体はclass属性で指定しているだけのため、複数指定する場合はスペースで区切ってください。  
$区切りで2行目のクラス名を指定できます。1つ目のみを指定した場合は、1つ目で指定したクラスが2行目にも適用されます。

#### 言語別設定 (ver32.7.0以降)

|通常設定|言語別設定(Ja)|言語別設定(En)|
|----|----|----|
|titleAnimationClass|titleAnimationClass**Ja**|titleAnimationClass**En**|
|titleanimationclass|titleanimationclass**Ja**|titleanimationclass**En**|

### 関連項目
- [customTitleAnimationUse](dos-h0078-customTitleAnimationUse) [:pencil:](dos-h0078-customTitleAnimationUse/_edit) タイトル文字のアニメーション有無設定
- [titleAnimation](dos-h0077-titleanimation) [:pencil:](dos-h0077-titleanimation/_edit) タイトル文字のアニメーション設定

### 更新履歴

|Version|変更内容|
|----|----|
|[v32.7.0](https://github.com/cwtickle/danoniplus/releases/tag/v32.7.0)|・キャメル表記での指定に対応<br>・言語別の設定に対応|
|[v18.9.0](https://github.com/cwtickle/danoniplus/releases/tag/v18.9.0)|・初回実装|

| [<- titleAnimation](dos-h0077-titleanimation) | **titleAnimationClass** | [titleArrowName ->](dos-h0099-titleArrowName) |