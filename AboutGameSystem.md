**[English](https://github.com/cwtickle/danoniplus-docs/wiki/AboutGameSystem) | Japanese** 

| [< 用途別アクセスマップ](./AccessMap) | **ゲーム画面の説明** | [ゲーム内ショートカット >](./Shortcut) |

# ゲーム画面の説明 (About Game System)
- [ゲーム内で使用できるショートカット](Shortcut)も合わせてご覧ください。
- 多鍵などキー数については [多鍵の種類](./AboutKeys-fingering) のページをご覧ください。
- v39から使用できる拡張設定については [ゲーム画面の説明(拡張設定)](./AboutGameSystem-ex) をご覧ください。

## GameTitle / ゲームタイトル
- Click Here!! を押してゲームをスタートします。  
※作品により、画面の見た目が異なる場合があります。 

<img src="./wiki/01game-title-1_v28.png" width="70%">

## Settings / 設定画面
- 譜面(Difficulty)や矢印の流れる速度などを変更できます。  
- PLAYボタンを押すとゲームがスタートします。  
譜面のタイミングがずれるときは、Adjustmentの設定を上下させてみてください。  

<img src="https://github.com/cwtickle/danoniplus/assets/44026291/cd1ad221-dfef-42d8-bbfb-11f22572af27" width="90%">


### Difficulty (譜面)
譜面が複数あれば、変更することができます。  
譜面数が多い場合、譜面名をクリックすると下記のように譜面を選択するリストが出ます。  
上下キーで譜面を切り替え、キー数を選択して譜面の絞り込み、「RANDOM」ボタンで譜面をランダムに選択できます。

<img src="https://github.com/cwtickle/danoniplus/assets/44026291/890922e7-6bcb-4299-b487-db8fb5bf52dc" width="90%">


### Speed (速度)
1x から 10x の間で矢印の流れる速度を変更できます。  
※譜面により、これ以上(以下)の速度で矢印が流れてくる場合があります。

### Motion (矢印の流れるモーションの変化)
矢印の速度を一定ではなく、変動させるモーションをつけるか設定します。  

|種類<br>Paramater|内容<br>Description|
|----|----|
|OFF|変化なし|
|Boost|初めは初速、ステップゾーンに近づくにつれて速くなります。|
|Hi-Boost|基本的にはBoostと同じですが、終端速度が初速に比例して速くなります。|
|Brake|初めは速く、ステップゾーンに近づくにつれて初速になります。|
|Compress|初めは初速、ステップゾーンに近づくにつれて徐々に遅くなります。|
|Fountain|矢印・フリーズアローが流れる反対側から出現し、画面中央で折り返してステップゾーンへ向かう動きをします。|

### Reverse (矢印の流れる向き)
矢印の流れる向きを設定します。  

|種類<br>Paramater|内容<br>Description|
|----|----|
|OFF|下から上へ流れます。<br>両方にステップゾーンがある場合は一部上から下へも流れます。|
|ON|上から下へ流れます。<br>両方にステップゾーンがある場合は、OFFの状態に対してそれぞれ逆になります。|

<img src="https://user-images.githubusercontent.com/44026291/193052561-235aa8b9-43a3-4045-a417-26fbced5beb0.png" width="45%"><img src="https://user-images.githubusercontent.com/44026291/193052877-fc0b5dc6-307c-4311-befa-22ba29580413.png" width="45%">


### Scroll (各レーンのスクロール方向の設定)
各レーンのスクロール方向をパターンに沿って設定します。  

|種類<br>Paramater|内容<br>Description|
|----|----|
|---|すべてのレーンが同じ方向に流れます。|
|Cross|正、逆、正の順に流れるよう設定します。|
|Split|中央を境界として、スクロール方向を反転します。|
|Alternate|交互にスクロール方向が反転するよう設定します。|
|Twist|Crossの亜種で、もう少し細かくスクロール方向が反転するよう設定します。|
|Asymmetry|Alternateの亜種で、基本的には交互にスクロール方向が反転するよう設定しますが、左右非対称になるよう設定します。|
|Flat|すべてのレーンが矢印の重なりに関係なく同じ方向に流れます。<br>一部のキーのみ使用できます。|

#### キー別のスクロール機能利用可否

|種類<br>Parameter|5, 17*key|7, 7i, 8, 9A, 9B, 9i*, 11ikey|12key|13key|11, 11L, 11W, 14, 14i, 15A, 15B, 16ikey|23key|
|----|----|----|----|----|----|----|
|Reverse|:heavy_check_mark:|:heavy_check_mark:|:heavy_check_mark:|:heavy_check_mark:|:heavy_check_mark:|:heavy_check_mark:|
|Cross|:heavy_check_mark:|:heavy_check_mark:||:heavy_check_mark:||:heavy_check_mark:|
|Split|:heavy_check_mark:|:heavy_check_mark:|||||
|Alternate|:heavy_check_mark:|:heavy_check_mark:|||||
|Twist||:heavy_check_mark:|:heavy_check_mark:|||:heavy_check_mark:|
|Asymmetry||:heavy_check_mark:||||:heavy_check_mark:|
|Flat|||:heavy_check_mark:|:heavy_check_mark:|:heavy_check_mark:|:heavy_check_mark:|

- 17keyは**KeyPattern: 1**のみCross/Split/Alternateが使用可能です。  
- 9ikeyは**KeyPattern: 2**のみCross/Split/Alternate/Twist/Asymmetryが使用可能です。  

### Shuffle (ミラー、ランダム)
譜面を左右反転したり、ランダムにします。  
ランダムにした場合は別譜面扱いとなり、**ハイスコアは保存されません。**  
※ver31.5.0以降はOFF/Mirror/X-Mirrorのみハイスコアを保存するようになりました。

|種類<br>Parameter|内容<br>Description|
|----|----|
|OFF|変化なし|
|Mirror|譜面をグループレーンごとに左右反転します。|
|X-Mirror|Mirrorに対して、グループ内のレーン数が4, 5, 6, 7の倍数のとき中央の2, 3個（上下左右なら、上下矢印）のみさらに反転します。該当キーが無い場合は、Mirrorと同じです。|
|Turning|譜面をグループレーンごとに出現レーンを左右にスライドします。その上で反転する場合もあります。|
|Random|譜面をグループレーンごとにランダムに再配置します。|
|Random+|譜面をグループに関係なくレーンごとにランダムに再配置します。|
|S-Random|譜面をグループごとに矢印個別にランダムに再配置します。|
|S-Random+|譜面をグループに関係なく矢印個別にランダムに再配置します。|
|Scatter|譜面をグループごと、矢印個別に矢印・フリーズアローが散らばるように再配置します。|
|Scatter+|譜面をグループに関係なく矢印個別に矢印・フリーズアローが散らばるように再配置します。|

### AutoPlay (オートプレイ)
一部もしくは全てのキーをアシストするかどうかを設定します。  
完全オートプレイ時は**ハイスコアを保存しません。**  

#### キー別のキーアシスト機能利用可否

|種類<br>Parameter|5key|9A, 11i, 17, 23key|Others|
|----|----|----|----|
|ALL|:heavy_check_mark:|:heavy_check_mark:|:heavy_check_mark:|
|Onigiri|:heavy_check_mark:|||
|Left||:heavy_check_mark:||
|Right||:heavy_check_mark:||

### Gauge (ライフゲージ設定)
クリア条件を設定します。  
Dancing☆Onigiri (CW Edition)の場合、  
クリア条件設定は譜面により大きくライフ制ゲージ・ノルマ制ゲージに分かれます。  
詳細は下記の通りです。  

#### ライフ制ゲージ（回復・ダメージ固定） / Life-Left Mode
- プレイ終了までにライフゲージが1でも残っていたらクリアです。  
ライフゲージが0になった場合、途中終了します。  
回復・ダメージ量は矢印数によらず固定です。  

|種類<br>Paramater|内容<br>Description|
|----|----|
|Original|回復・ダメージ量固定のデフォルトゲージです。|
|Light|Originalと同じですが、Originalよりも回復量が大きくなっています。|
|Heavy|Originalよりも回復量が小さく、ダメージが大きい設定です。|
|NoRecovery|ゲージが最初からMAXの状態で開始しますが、回復しません。|
|SuddenDeath|1ミス(ウワァン/イクナイ)で終了するモードです。|
|Practice|ゲージの変動が無い練習用モードです。|

#### ノルマ制ゲージ（回復・ダメージ変動） / Norm Mode
- プレイ終了時、ライフゲージがノルマに達している必要があります。  
ノルマが0の場合は、ライフ制ゲージと同じです。  
回復・ダメージ量は矢印数に応じて変動します。（SuddenDeathを除く）  
矢印数が多くなると、回復・ダメージ量の幅は小さくなります。  

|種類<br>Paramater|内容<br>Description|
|----|----|
|Normal|ノルマ制ゲージが指定された場合のデフォルトゲージです。<br>目標ノルマ到達でクリアです。|
|Easy|Normalと同じですが、Normalよりも回復量が大きくなっています。|
|Hard|ゲージが最初からMAXの状態で開始しますが、回復量が微小でダメージ量が大きいモードです。<br>最後までゲージが残っていればクリアです。|
|SuddenDeath|1ミス(ウワァン/イクナイ)で終了するモードです。|

### Adjustment (譜面のタイミング調整)
タイミングにズレを感じる場合、数値を変えることでズレを直すことができます。  
0.5フレーム(1/120秒)単位で調整できます。  
どの程度ずれているかは、プレイ後に結果画面で「推定Adj」という項目を確認することでも確認が可能です。

<img src="https://github.com/cwtickle/danoniplus/assets/44026291/b9b1a235-d8de-42c8-99bf-e3fafa5458e8" width="80%">


### Fadein (フェードイン)
譜面を途中から再生します。  
途中から開始した場合は**ハイスコアを保存しません。**  

### Volume (音量)
ゲーム内の音量を設定します。  

## Chart Details / 譜面明細表示
- 譜面情報の詳細を表示します。

### Density Graph / 譜面密度グラフ
- 譜面を時間で分割した場合に、密度にどのくらいのばらつきがあるかを表示します。  
この棒グラフのバーが高いほど、ノーツの密度が高くなることを表します。
- Single: 単押し、Chord: 同時押し、Triad+: 3つ以上の同時押し　を表します。

<img src="https://github.com/cwtickle/danoniplus/assets/44026291/a0c757b0-8a49-46fc-aeed-bbdc9c2017f7" width="45%">

### Velocity Graph / 速度変化グラフ
- 時間経過に合わせた速度変化傾向を表示します。  
赤色が「あるフレームでの全体的な加速量」、黄色が「矢印個別の加速量」を表します。

<img src="https://github.com/cwtickle/danoniplus/assets/44026291/a16421c8-74d7-4b59-a782-a039fb4ffafc" width="45%">

### Difficulty Level / 譜面難易度表示
- 譜面情報から譜面の難しさ（レベル）を数字で表示します。  
「*」は3つ以上の同時押しがあることを表します。
- 「データ表示」より、全譜面の難易度状況をクリップボードへコピーすることができます。

<img src="https://github.com/cwtickle/danoniplus/assets/44026291/d496d9d4-b6c4-475f-8f54-5a3fc7fbd5b5" width="45%">

### Highscore view / ハイスコア表示
- ハイスコアの詳細を表示します。
- シャッフル(Mirror/X-Mirrorのみ)別、アシスト(AutoPlay:OFF/ALL以外)、別キーモードのハイスコアは「Shuffle」「AutoPlay」の設定やキーコンパターンを切り替えることで表示を切り替えられます。
- 「CopyResult」にてハイスコア時の結果をクリップボードにコピーします。
- 「Reset」にて現在表示しているハイスコアをクリアします。

<img src="https://github.com/cwtickle/danoniplus/assets/44026291/708c1cad-63f1-46d9-a1e6-ffb6ebc6a75e" width="50%">

## Display Settings / 画面の見え方
<img src="https://github.com/cwtickle/danoniplus/assets/44026291/baf1e9ad-dea6-4fb8-a4d6-87616606b74b" width="70%">

### Display (オブジェクト等の表示／非表示)
プレイ画面上に存在するものの表示／非表示を切り替えます。  

|種類<br>Paramater|内容<br>Description|
|----|----|
|StepZone|ステップゾーンの表示<br>(拡張ボタンでステップゾーンをFlat化)|
|Judgment|判定キャラクタ・コンボの表示|
|FastSlow|Fast/Slow表示|
|LifeGauge|ライフゲージの表示|
|Score|現時点の判定数を表示|
|MusicInfo|楽曲情報、時間表記の表示|
|FilterLine|Hidden+, Sudden+使用時のフィルターの境界線表示|
|Velocity|途中変速、個別加速の有効化|
|Color|色変化の有効化|
|Lyrics|歌詞表示の有効化|
|Background|背景・マスクモーションの有効化|
|ArrowEffect|矢印・フリーズアローモーションの有効化|
|Special|作品固有の特殊演出の有効化|

### Appearance (流れる矢印の見え方)
流れる矢印の見え方を制御します。  
「Hidden+」と「Sudden+」についてはプレイ中に「pageUp」「pageDown」を押すことで可視範囲を調整できます。

|種類<br>Paramater|内容<br>Description|
|----|----|
|Visible|すべてが見える状態|
|Hidden|初めの50%までは見えますが、その後は見えなくなります。|
|Sudden|初めの40%までは見えませんが、その後は見えるようになります。|
|Hidden+|基本的にHiddenと同じですが、Hiddenの可視範囲をプレイ中に調整できます。<br>初期値を10%～100%の間で設定できます。「Lock」ボタンで可視範囲の調整を不可にします。|
|Sudden+|基本的にSuddenと同じですが、Suddenの可視範囲をプレイ中に調整できます。<br>初期値を0%～90%の間で設定できます。「Lock」ボタンで可視範囲の調整を不可にします。|
|Hid&Sud+|Hidden+とSudden+を組み合わせて表示します。<br>初期値を0%～50%の間で設定できます。「Lock」ボタンで可視範囲の調整を不可にします。|

<img src="https://user-images.githubusercontent.com/44026291/193053461-47c77dc3-4509-4a6e-923f-1f8e1a440b16.png" width="45%"><img src="https://user-images.githubusercontent.com/44026291/193053721-93c59089-daf0-47ed-9192-3c0ab0b21ff0.png" width="45%">

### Opacity (判定キャラクタ、Hidden+/Sudden+の境界線表示の透明度)
判定キャラクタ、コンボ数、Fast/Slow、Hidden+/Sudden+の境界線表示の透明度を設定します。

### HitPosition (矢印・フリーズアローのヒット位置調整)
矢印・フリーズアローを押すとき、早押し・遅押し傾向にある場合にこの数値を変更すると判定位置を変更することができます。  
プラスで手前側、マイナスで奥側になるよう変更します。

<img src="https://user-images.githubusercontent.com/44026291/226108006-0fe47d5c-8d20-4394-b1f6-7e0801309a05.png" width="60%">

## KeyConfig / キーコンフィグ（矢印に対応するキーの割り当て）
- 画面が表示されている間に対応するキーを押すことで、割り当てるキーを変えることができます。変更されたキーは「黄色」で表示されます。  
- また、設定画面で「Data Save」を「ON」にしていれば、  
次回以降、設定したキーコンフィグをパターンとして丸ごと保存します。  
保存したキーは青色で表示されます。

<img src="https://user-images.githubusercontent.com/44026291/227969646-aa6ed068-914e-4b4d-b2bd-1adc67d82ff4.png" width="70%">

### ColorType (矢印・フリーズアローの配色設定)
あらかじめ定義された矢印・フリーズアローの配色セットを変更できます。  
Type1～4では、左側のカラーパレットを使用することでグループ別に好きな色を一時的に設定可能です。  
またこのとき、[↓]ボタンを押すことで矢印色とフリーズアロー色を同色にすることが可能です。

<img src="https://user-images.githubusercontent.com/44026291/228386969-a38056fd-f92c-40dc-b22f-0cbb3acb71e2.png" width="95%">

<img src="https://user-images.githubusercontent.com/44026291/227938765-ec9f658e-f8ab-4148-80f6-6aaae4ab0fa6.png" width="95%">

### ImgType (ノーツスキン設定)
矢印、おにぎりなどのノーツスキンを変更できます。  
サイトによっては、設定できない場合があります。

<img src="https://user-images.githubusercontent.com/44026291/226272111-6329f0fa-e2e5-491d-907d-36e73db95a58.png" width="45%"><img src="https://github.com/cwtickle/danoniplus-docs/assets/44026291/ef5fead6-bf6b-4204-a1cf-8f422c98ef99" width="45%">
<img src="https://github.com/cwtickle/danoniplus-docs/assets/44026291/5f364b81-ac23-43e9-a451-4a392d1b189d" width="45%"><img src="https://user-images.githubusercontent.com/44026291/226272230-32cd000d-5874-4ce3-87b7-158d0773e3e7.png" width="45%">

### ShuffleGr. / ColorGr. / ShapeGr.
一部のキーでシャッフル、色、形状グループのパターンを変更できます。  
この変更は、それぞれShuffle設定や色変化に影響します。

<img src="https://user-images.githubusercontent.com/44026291/227833459-6df12e1d-7f67-4969-9843-4867c4fc96ab.png" width="95%">

<img src="https://user-images.githubusercontent.com/44026291/227833515-bd9f7d70-02bd-4f31-86bf-88521170a0a3.png" width="95%">

<img src="https://user-images.githubusercontent.com/44026291/227973719-2ede6220-08fb-48db-ba74-6edbf6d29f14.png" width="95%">

#### キー別のShuffleGr., ColorGr., ShapeGr. 利用可否

|種類<br>Parameter|11, 11L, 15A, 15Bkey|9Akey|9B, 13key|11ikey|17key|Others|
|----|----|----|----|----|----|----|
|ShuffleGr.|:heavy_check_mark:|:heavy_check_mark:||:heavy_check_mark:|:heavy_check_mark:||
|ColorGr.||:heavy_check_mark:|:heavy_check_mark:||:heavy_check_mark:||
|ShapeGr.||||:heavy_check_mark:|:heavy_check_mark:||

### KeyConfigパターン変更 (< > ボタン)
キーコンフィグの配列パターンを変更します。  
「前回のキーコンフィグパターン」→「パターン1」(→「パターン2」→・・・)  
の順に切り替わります。

<img src="https://user-images.githubusercontent.com/44026291/226273343-f774a3e8-2f17-4f4f-b08d-96c834800121.png" width="45%"><img src="https://user-images.githubusercontent.com/44026291/226273432-e1ccd84a-54f2-4522-bcfc-ba80dd7b3bd6.png" width="45%">

#### 別キーモード (Another Key Mode)
キーによっては、形状の似ている別のキーで遊ぶことができます。  
ただし、正規の譜面では無い遊び方のため、キーコンフィグやColorTypeは保存されません。  
詳細は[こちらのページ](AnotherKeysMode)をご覧ください。

<img src="https://user-images.githubusercontent.com/44026291/226273587-7804ca76-c1a5-490b-9f29-d8f370217d08.png" width="45%"><img src="https://user-images.githubusercontent.com/44026291/226273658-7afdc349-4e0c-4ae8-9d70-2f30eb80ab5f.png" width="45%">

|キー数<br>Keys|対応している別キー<br>Another Keys|
|----|----|
|7key|7ikey, 12key(下段)|
|8key|12key(下段)|
|9Akey|9Bkey, 11ikey|
|9Bkey|9Akey|
|11key|11Lkey, 12key|
|11Lkey|11key, 12key|
|11Wkey|12key|

### KeySwitch (コンフィグキーの切り替え)
主にキー変化する作品や一部のキー限定で、右側のボタンでそれぞれのキーの切り替えが可能です。

<img src="https://user-images.githubusercontent.com/44026291/226274137-89f87f01-f1e3-48db-a862-b20adc92df9f.png" width="45%"><img src="https://user-images.githubusercontent.com/44026291/226274215-ff43be88-0529-44a8-b953-242ea3165f93.png" width="45%">

### Reset (KeyConfigのリセット)
キーコンフィグのキー配列をデフォルトのキーパターンに戻します。

## Main / プレイ画面
- Ready? が出た後にゲームが始まります。  
※ゲームによって、表示の仕方が異なる場合があります。

<img src="./wiki/31game-main1.png" width="70%">

タイミング良くキーボードを押すと(・∀・)ｲｲ!!や(ﾟ∀ﾟ)ｷﾀｰ!!となり、ライフが上がります。  
一方、タイミングを外すと(´・ω・\`)ｼｮﾎﾞｰﾝ、(\`Д´)ｳﾜｧﾝ、(・A・)ｲｸﾅｲとなり、ライフが下がります。  
ゲーム終了までにライフゲージのライフが残っているか、ノルマ以上であればゲームクリアです。

<img src="./wiki/howtoplay2.png" width="70%">

<img src="./wiki/32game-main2.png" width="70%">

## Result / 結果画面
- ゲームが終わると、結果画面へ移行します。  
最終的な判定やランク、ハイスコア差分やFast/Slowが画面に表示されます。  
「Tweet」ボタンを押すことでX(Twitter)へ投稿が可能です。

<img src="https://github.com/cwtickle/danoniplus/assets/44026291/236c1e8f-7a1b-4677-a96a-0ad873e8a87f" width="70%">

<img src="https://github.com/cwtickle/danoniplus/assets/44026291/07e6cf4c-bbf0-43f3-9c38-5e03efc4b7ea" width="60%">

### Rank / ランク
- ランクは下記の基準で決定します。  
スコアについては、作品により独自の仕様が入っている場合があります。

|Rank|ランク決定条件 / Rank Determination Conditions|
|----|----|
|AP|スコアが100万点でクリア|
|PF|獲得した判定がイイ、シャキン、キターのみでクリア|
|SS|スコアが97万点以上でクリア|
|S|スコアが90万点以上でクリア|
|SA|スコアが85万点以上でクリア|
|AAA|スコアが80万点以上でクリア|
|AA|スコアが75万点以上でクリア|
|A|スコアが70万点以上でクリア|
|B|スコアが65万点以上でクリア|
|C|スコアが60万点以上でクリア|
|D|スコアが60万点未満でクリア|
|F|クリア失敗|

## Screen Transition Diagram / 画面遷移図

```mermaid
 graph LR;
   GameTitle--ClickHere-->Settings/Display/ExSettings;
   GameTitle--DataManagement-->DataManagement;
   DataManagement--Back-->GameTitle;
   Settings/Display/ExSettings--Back-->GameTitle;
   Settings/Display/ExSettings--KeyConfig-->KeyConfig;
   Settings/Display/ExSettings--Play-->Main;
   Settings/Display/ExSettings--DataManagement-->DataManagement;
   DataManagement--Back-->Settings/Display/ExSettings;
   KeyConfig--Play-->Main;
   KeyConfig--ToSettings-->Settings/Display/ExSettings;
   Main-->Result;
   Result--Retry-->Main;
   Result--Back-->GameTitle;
   Result--CopyResult-->Result;
```

| [< 用途別アクセスマップ](./AccessMap) | **ゲーム画面の説明** | [ゲーム内ショートカット >](./Shortcut) |