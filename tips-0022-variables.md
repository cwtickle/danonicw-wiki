**[English](https://github.com/cwtickle/danoniplus-docs/wiki/tips-0022-variables) | Japanese**

[^ Tips Indexに戻る](./tips-index)

| < [オブジェクトの階層とラベル・ボタンの挿入](./tips-0021-object-layer) | **利便性の高い変数群** | [ボタン処理の拡張・上書き](./tips-0005-button-expansion) > |

# 利便性の高い変数群

- カスタムJSを扱う上で利便性の高い変数群をまとめます。

## 長さ

- Dancing☆Onigiri (CW Edition)では、さまざまなキー種が登場します。
- 特にキー種により横の長さが変わってしまうため、位置を指定する際には固定値を使わない方が良いことが多いです。

<img src="https://github.com/user-attachments/assets/e55c7760-1edf-4723-835f-9dbbf91d87dc" width="80%">

<img src="https://github.com/user-attachments/assets/6ff7732d-fe20-4343-96c9-5e465185b9e2" width="60%">

|変数/関数|値|
|----|----|
|g_sWidth|画面の横幅|
|g_sHeight|画面の高さ|
|g_btnX(dx)|画面の横幅が900pxを超えた場合に、<br>900pxの横幅のオブジェクトを中央に配置する場合の左端からの距離。<br>引数には調整幅をpx単位で指定できる。<br>引数のデフォルトはゼロ。|
|g_btnWidth(multi)|画面の横幅（900pxを上限）。<br>引数は割合を指定でき、`1/2`とすれば画面の横幅の半分になる。<br>引数のデフォルトは1。|
|g_headerObj.playingWidth|プレイ時の横幅<br>(譜面ヘッダー: playingWidth 指定時に有効)|
|g_headerObj.playingX|プレイ時のX軸補正値<br>(譜面ヘッダー: playingX 指定時に有効)|
|g_headerObj.playingHeight|プレイ時の高さ<br>(譜面ヘッダー: playingHeight 指定時に有効)|
|g_headerObj.playingY|プレイ時のY軸補正値<br>(譜面ヘッダー: playingY 指定時に有効)|
|g_headerObj.scAreaWidth|ショートカットキーが通常と異なる場合のショートカットエリアの横幅<br>(譜面ヘッダー: scArea 指定時に有効)|

## 時間経過

|変数|値|
|----|----|
|g_scoreObj.baseFrame|経過フレーム数。<br>この数値はAdjustmentの影響を受けない。|

## 譜面情報

- scoreIdは譜面番号で、1譜面目=0, 2譜面目=1, ... を表します。

|変数|値|
|----|----|
|g_detailObj.toolDif[scoreId]|譜面の難易度(ツール値)|
|g_detailObj.playingFrameWithBlank[scoreId]|プレイ時間|
|g_detailObj.playingFrame[scoreId]|プレイ時間(最初の矢印出現時までを除く)|

## 判定範囲

- 詳細は [g_judgObj](./obj-v0028-g_judgObj) のページをご覧ください。

|変数|値|
|----|----|
|g_judgObj.arrow|矢印の判定範囲|
|g_judgObj.frz|フリーズアローの判定範囲|


## ページ作成者
- ティックル

## 関連項目
- [playingX / playingY](dos-h0070-playingX) ([🔧](dos-s0007-viewControl#ゲーム表示エリアのx-y座標-g_presetobjplayingx-g_presetobjplayingy)) ゲーム表示エリアのX, Y座標
- [playingWidth / playingHeight](dos-h0071-playingWidth) ([🔧](dos-s0007-viewControl#ゲーム表示エリアの横幅高さ-g_presetobjplayingwidth-g_presetobjplayingheight)) ゲーム表示エリアの横幅、高さ
- [scArea](./dos-h0096-scArea) ([🔧](./dos-s0007-viewControl#ショートカットキーエリアの横幅拡張設定-g_presetobjscareawidth)[🔧](./dos-s0007-viewControl#プレイ画面の表示レイアウト-g_presetobjplayinglayout)) ショートカットキー表示の横幅拡張設定

[^ Tips Indexに戻る](./tips-index)

| < [オブジェクトの階層とラベル・ボタンの挿入](./tips-0021-object-layer) | **利便性の高い変数群** | [ボタン処理の拡張・上書き](./tips-0005-button-expansion) > |
