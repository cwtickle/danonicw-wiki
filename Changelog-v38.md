**[English](https://github.com/cwtickle/danoniplus-docs/wiki/Changelog-v38) | Japanese**

[^ Back to All Version List (Security Policy)](https://github.com/cwtickle/danoniplus/security/policy)  
[^ Back to Update Info](./UpdateInfo#-v38-changelog)

[**<- v39**](./Changelog-v39) | **v38** | [**v37 ->**](./Changelog-v37)  
(**🔖 [13 Releases](https://github.com/cwtickle/danoniplus/releases?q=tag%3Av38)** )

## 🔃 Files changed (v38)

| Directory | FileName            |                                                                                            | Last Updated |
| --------- | ------------------- | ------------------------------------------------------------------------------------------ | ------------ |
| /js       | danoni_main.js      | [📥](https://github.com/cwtickle/danoniplus/releases/download/v38.3.4/danoni_main.js)      | **v38.3.4**  |
| /js/lib   | danoni_constants.js | [📥](https://github.com/cwtickle/danoniplus/releases/download/v38.3.0/danoni_constants.js) | [v38.3.0](./Changelog-v38#v3830-2025-01-08)  |

<details>
<summary>Changed file list before v37</summary>

| Directory | FileName                                                                                                                                               |                                                                                              | Last Updated                                   |
| --------- | ------------------------------------------------------------------------------------------------------------------------------------------------------ | -------------------------------------------------------------------------------------------- | ---------------------------------------------- |
| /js/lib   | danoni_localbinary.js                                                                                                                                  | [📥](https://github.com/cwtickle/danoniplus/releases/download/v15.1.0/danoni_localbinary.js) | [v15.1.0](./Changelog-v15#v1510-2020-05-21)    |
| /skin     | danoni_skin_default.css<br>danoni_skin_light.css<br>danoni_skin_skyblue.css                                                                            | [📥](https://github.com/cwtickle/danoniplus/releases/download/v36.4.1/skin_css.zip)          | [v36.4.1](./Changelog-v36#v3641-2024-05-18) |
| /css      | danoni_main.css                                                                                                                                        | [📥](https://github.com/cwtickle/danoniplus/releases/download/v37.2.1/danoni_main.css)       | [v37.2.1](./Changelog-v37#v3721-2024-06-30) |
| /img      | aaShadow.svg<br>arrow.svg<br>arrowShadow.svg<br>borderline.svg<br>c.svg<br>cursor.svg<br>giko.svg<br>iyo.svg<br>monar.svg<br>morara.svg<br>onigiri.svg | [📥](https://github.com/cwtickle/danoniplus/releases/download/v15.1.0/img.zip)               | [v15.1.0](./Changelog-v15#v1510-2020-05-21)    |

</details>

⭐ New Features / 🛠️ Improvements / 🐞 Bug Fixes  
( Files changed: ⚪ danoni_main.js / 🔵 danoni_constants.js / 🔴 css files )

## v38.3.4 ([2025-02-24](https://github.com/cwtickle/danoniplus/releases/tag/v38.3.4))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v38.3.4/total)](https://github.com/cwtickle/danoniplus/archive/v38.3.4.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v38.3.4/support/v38?style=social)](https://github.com/cwtickle/danoniplus/compare/v38.3.4...support/v38#files_bucket)

- 🐞 **C)** キーパターン違いで対象のキーアシストがいない場合、キーパターン変更時に止まることがある問題を修正 ( PR [#1786](https://github.com/cwtickle/danoniplus/pull/1786) ) <- :boom: [**v15.0.0**](./Changelog-v15#v1500-2020-05-13)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v38.3.4)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v38.3.3...v38.3.4#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v38.3.4)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v38.3.4.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v38.3.4.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v38.3.4/js/danoni_main.js)
> / 🎣 [**v39.8.2**](./Changelog-v39#v3982-2025-02-24)

## v38.3.3 ([2025-02-17](https://github.com/cwtickle/danoniplus/releases/tag/v38.3.3))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v38.3.3/total)](https://github.com/cwtickle/danoniplus/archive/v38.3.3.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v38.3.3/support/v38?style=social)](https://github.com/cwtickle/danoniplus/compare/v38.3.3...support/v38#files_bucket)

- 🐞 **C)** キーパターンが別キーモードで、デフォルトの曲中ショートカットが異なる場合に曲中ショートカットキーが変わらない問題を修正 ( PR [#1773](https://github.com/cwtickle/danoniplus/pull/1773) ) <- :boom: [**v4.8.0**](./Changelog-v4#v480-2019-05-10)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v38.3.3)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v38.3.2...v38.3.3#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v38.3.3)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v38.3.3.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v38.3.3.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v38.3.3/js/danoni_main.js)
> / 🎣 [**v39.6.1**](./Changelog-v39#v3961-2025-02-17)

## v38.3.2 ([2025-02-01](https://github.com/cwtickle/danoniplus/releases/tag/v38.3.2))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v38.3.2/total)](https://github.com/cwtickle/danoniplus/archive/v38.3.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v38.3.2/support/v38?style=social)](https://github.com/cwtickle/danoniplus/compare/v38.3.2...support/v38#files_bucket)

- 🐞 **C)** HitPosition有効時にHidden+/Sudden+のフィルター位置がずれる問題を修正 ( PR [#1746](https://github.com/cwtickle/danoniplus/pull/1746) ) <- :boom: [**v31.0.0**](./Changelog-v31#v3100-2023-03-20)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v38.3.2)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v38.3.1...v38.3.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v38.3.2)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v38.3.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v38.3.2.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v38.3.2/js/danoni_main.js)
> / 🎣 [**v39.0.0**](./Changelog-v39#v3900-2025-02-01)

----

## v38.3.1 ([2025-01-28](https://github.com/cwtickle/danoniplus/releases/tag/v38.3.1))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v38.3.1/total)](https://github.com/cwtickle/danoniplus/archive/v38.3.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v38.3.1/support/v38?style=social)](https://github.com/cwtickle/danoniplus/compare/v38.3.1...support/v38#files_bucket)

- 🐞 **B)** フリーズアロー移動中のフリーズアロー長の計算誤りを修正 ( PR [#1744](https://github.com/cwtickle/danoniplus/pull/1744) ) <- :boom: **initial**
- 🐞 **B)** フリーズアローの末尾付近に矢印がいる場合の判定不具合を修正 ( PR [#1744](https://github.com/cwtickle/danoniplus/pull/1744) ) <- :boom: **initial**

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v38.3.1)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v38.3.0...v38.3.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v38.3.1)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v38.3.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v38.3.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v38.3.1/js/danoni_main.js)<br>❤️ ★ぞろり★

## v38.3.0 ([2025-01-08](https://github.com/cwtickle/danoniplus/releases/tag/v38.3.0))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v38.3.0/total)](https://github.com/cwtickle/danoniplus/archive/v38.3.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v38.3.0/support/v38?style=social)](https://github.com/cwtickle/danoniplus/compare/v38.3.0...support/v38#files_bucket)

- ⭐ プレイ中ショートカットの変更に対応 ( PR [#1742](https://github.com/cwtickle/danoniplus/pull/1742) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v38.3.0)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v38.2.2...v38.3.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v38.3.0)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v38.3.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v38.3.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v38.3.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v38.3.0/js/lib/danoni_constants.js)

## v38.2.2 ([2024-12-19](https://github.com/cwtickle/danoniplus/releases/tag/v38.2.2))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v38.2.2/total)](https://github.com/cwtickle/danoniplus/archive/v38.2.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v38.2.2/support/v38?style=social)](https://github.com/cwtickle/danoniplus/compare/v38.2.2...support/v38#files_bucket)

- 🐞 **C)** 色変化(ncolor_data)でキーパターンを変えると適用する矢印グループが想定と異なる問題を修正 ( PR [#1738](https://github.com/cwtickle/danoniplus/pull/1738), [#1740](https://github.com/cwtickle/danoniplus/pull/1740) ) <- :boom: [**v36.0.0**](./Changelog-v36#v3600-2024-04-15)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v38.2.2)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v38.2.0...v38.2.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v38.2.2)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v38.2.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v38.2.2.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v38.2.2/js/danoni_main.js)<br>❤️ SKB ( @superkuppabros )

## v38.2.0 ([2024-12-14](https://github.com/cwtickle/danoniplus/releases/tag/v38.2.0))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v38.2.0/total)](https://github.com/cwtickle/danoniplus/archive/v38.2.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v38.2.0/support/v38?style=social)](https://github.com/cwtickle/danoniplus/compare/v38.2.0...support/v38#files_bucket)

- 🛠️ Scatterオプションのシャッフル方法を一部見直し ( PR [#1736](https://github.com/cwtickle/danoniplus/pull/1736) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v38.2.0)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v38.1.2...v38.2.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v38.2.0)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v38.2.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v38.2.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v38.2.0/js/danoni_main.js)

## v38.1.2 ([2024-12-07](https://github.com/cwtickle/danoniplus/releases/tag/v38.1.2))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v38.1.2/total)](https://github.com/cwtickle/danoniplus/archive/v38.1.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v38.1.2/support/v38?style=social)](https://github.com/cwtickle/danoniplus/compare/v38.1.2...support/v38#files_bucket)

- 🐞 **C)** Fountainで全体変速が1倍未満、個別加速が1倍以上のときにモーションが見づらくなる問題を修正 ( PR [#1732](https://github.com/cwtickle/danoniplus/pull/1732), [#1734](https://github.com/cwtickle/danoniplus/pull/1734) ) <- :boom: [**v38.1.0**](./Changelog-v38#v3810-2024-12-02)
- 🐞 **B)** 加算するモーションフレーム数の計算方法の誤りを修正 ( PR [#1734](https://github.com/cwtickle/danoniplus/pull/1734) ) <- :boom: [**v28.4.0**](./Changelog-v28#v2840-2022-10-20)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v38.1.2)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v38.1.0...v38.1.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v38.1.2)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v38.1.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v38.1.2.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v38.1.2/js/danoni_main.js)

## v38.1.0 ([2024-12-02](https://github.com/cwtickle/danoniplus/releases/tag/v38.1.0))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v38.1.0/total)](https://github.com/cwtickle/danoniplus/archive/v38.1.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v38.1.0/support/v38?style=social)](https://github.com/cwtickle/danoniplus/compare/v38.1.0...support/v38#files_bucket)

- ⭐ MotionオプションにCompress, Fountainを追加 ( PR [#1729](https://github.com/cwtickle/danoniplus/pull/1729), [#1730](https://github.com/cwtickle/danoniplus/pull/1730) )
- ⭐ リモート参照時のみ、カレントパス指定なしで作品URLの相対パスとしてファイル参照するよう変更 ( PR [#1728](https://github.com/cwtickle/danoniplus/pull/1728) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v38.1.0)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v38.0.3...v38.1.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v38.1.0)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v38.1.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v38.1.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v38.1.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v38.1.0/js/lib/danoni_constants.js)

## v38.0.3 ([2024-11-11](https://github.com/cwtickle/danoniplus/releases/tag/v38.0.3))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v38.0.3/total)](https://github.com/cwtickle/danoniplus/archive/v38.0.3.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v38.0.3/support/v38?style=social)](https://github.com/cwtickle/danoniplus/compare/v38.0.3...support/v38#files_bucket)

- 🐞 **C)** 色変化（ncolor_data）で矢印種類を省略したときに色変化対象が正しく反映されない問題を修正 ( PR [#1726](https://github.com/cwtickle/danoniplus/pull/1726) ) <- :boom: [**v38.0.0**](./Changelog-v38#v3800-2024-11-04)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v38.0.3)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v38.0.2...v38.0.3#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v38.0.3)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v38.0.3.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v38.0.3.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v38.0.3/js/danoni_main.js)

## v38.0.2 ([2024-11-07](https://github.com/cwtickle/danoniplus/releases/tag/v38.0.2))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v38.0.2/total)](https://github.com/cwtickle/danoniplus/archive/v38.0.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v38.0.2/support/v38?style=social)](https://github.com/cwtickle/danoniplus/compare/v38.0.2...support/v38#files_bucket)

- 🐞 **C)** 旧関数のボタン作成処理の引数間違いを修正 ( PR [#1724](https://github.com/cwtickle/danoniplus/pull/1724) ) <- :boom: [**v37.8.0**](./Changelog-v37#v3780-2024-11-01)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v38.0.2)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v38.0.1...v38.0.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v38.0.2)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v38.0.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v38.0.2.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v38.0.2/js/danoni_main.js)

## v38.0.1 ([2024-11-05](https://github.com/cwtickle/danoniplus/releases/tag/v38.0.1))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v38.0.1/total)](https://github.com/cwtickle/danoniplus/archive/v38.0.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v38.0.1/support/v38?style=social)](https://github.com/cwtickle/danoniplus/compare/v38.0.1...support/v38#files_bucket)

- 🐞 **B)** 結果画面で定義前に使われている定数がいる問題を修正 ( PR [#1722](https://github.com/cwtickle/danoniplus/pull/1722) ) <- :boom: [**v38.0.0**](./Changelog-v38#v3800-2024-11-04)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v38.0.1)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v38.0.0...v38.0.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v38.0.1)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v38.0.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v38.0.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v38.0.1/js/danoni_main.js)

## v38.0.0 ([2024-11-04](https://github.com/cwtickle/danoniplus/releases/tag/v38.0.0))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v38.0.0/total)](https://github.com/cwtickle/danoniplus/archive/v38.0.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v38.0.0/support/v38?style=social)](https://github.com/cwtickle/danoniplus/compare/v38.0.0...support/v38#files_bucket)

- ⭐ ゲージ設定部分で数式が使えるように変更 ( PR [#1717](https://github.com/cwtickle/danoniplus/pull/1717), [#1720](https://github.com/cwtickle/danoniplus/pull/1720) )
- 🛠️ ハッシュタグの前後に半角スペースを追加 ( PR [#1718](https://github.com/cwtickle/danoniplus/pull/1718) )
- 🐞 **C)** フェードインで矢印が足りない場合でもクリアランプの点灯条件を通過してしまう問題を修正 ( PR [#1719](https://github.com/cwtickle/danoniplus/pull/1719) ) <- :boom: [**v36.2.0**](./Changelog-v36#v3620-2024-05-01)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v38.0.0)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v37.8.0..v38.0.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v38.0.0)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v38.0.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v38.0.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v38.0.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v38.0.0/js/lib/danoni_constants.js)<br>❤️ すずめ ( @suzme ), ショウタ

[**<- v39**](./Changelog-v39) | **v38** | [**v37 ->**](./Changelog-v37)

