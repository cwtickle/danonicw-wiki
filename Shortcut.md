**[English](https://github.com/cwtickle/danoniplus-docs/wiki/Shortcut) | Japanese** 

| [< ゲーム画面の説明](./AboutGameSystem) | **ゲーム内ショートカット** | [多鍵の種類について >](./AboutKeys-fingering) |

# Shortcuts / ゲーム内で使用できるショートカット

## GameTitle / ゲームタイトル

<img src="./wiki/shortcut_title.png" width="70%">

|対応キー|操作|
|----|----|
|<kbd>C</kbd>|コメントウィンドウの開閉 (<kbd>Ctrl</kbd>+<kbd>C</kbd>)時は無効)<br>Opening and closing the comment window (Invalid when <kbd>Ctrl</kbd>+<kbd>C</kbd>)|
|<kbd>/ ?</kbd> or <kbd>F1</kbd>|ゲーム画面の説明（外部リンク）<br>About game system (external link)|
|<kbd>Enter</kbd> or <kbd>NumEnter</kbd>|次の画面へ<br>Transition to the next screen|

## Data Management / データ管理画面

<img src="https://github.com/user-attachments/assets/99d5274e-c84e-4a1f-8a44-01f182cd4cf2" width="70%">

|対応キー|操作|
|----|----|
|<kbd>E</kbd>|Environment|
|<kbd>H</kbd>|Highscores|
|<kbd>K</kbd>|CustomKey|
|<kbd>O</kbd>|Others|
|<kbd>Escape</kbd>|前の画面へ(もしくは<kbd>Shift</kbd>+<kbd>Tab</kbd>)<br>Go to the previous screen (can also use <kbd>Shift</kbd>+<kbd>Tab</kbd>)|

## Settings / 設定画面
<img src="https://github.com/cwtickle/danoniplus/assets/44026291/6495b8cf-5c55-4e75-98ce-c3e5106ce6c5" width="70%">
<img src="https://github.com/cwtickle/danoniplus/assets/44026291/f685887a-e020-4783-8a09-2d7d586ccf3d" width="45%"><img src="https://github.com/cwtickle/danoniplus/assets/44026291/84f8551d-6968-4df1-8173-d05e7eaa4f1f" width="45%">

|対応キー|操作|
|----|----|
|<kbd>D</kbd>|Difficulty (右回り / Next item)|
|<kbd>Shift</kbd>+<kbd>D</kbd>|Difficulty (左回り / Previous item)|
|<kbd>L</kbd>|譜面セレクターを表示　※対応時のみ<br>View charts list|
|<kbd>→</kbd>|Speed (右回り、0.25x刻み / Next item, every 0.25x)|
|<kbd>←</kbd>|Speed (左回り、0.25x刻み / Previous item, every 0.25x)|
|<kbd>Shift</kbd>+<kbd>→</kbd>|Speed (右回り、1x刻み / Next item, every 1x)|
|<kbd>Shift</kbd>+<kbd>←</kbd>|Speed (左回り、1x刻み / Previous item, every 1x)|
|<kbd>Alt</kbd>+<kbd>→</kbd>|Speed (右回り、0.05x刻み / Next item, every 0.05x)|
|<kbd>Alt</kbd>+<kbd>←</kbd>|Speed (左回り、0.05x刻み / Previous item, every 0.05x)|
|<kbd>M</kbd>|Motion (右回り / Next item)|
|<kbd>Shift</kbd>+<kbd>M</kbd>|Motion (左回り / Previous item)|
|<kbd>R</kbd>|Reverse|
|<kbd>↓</kbd>|Scroll (右回り / Next item)|
|<kbd>↑</kbd>|Scroll (左回り / Previous item)|
|<kbd>S</kbd>|Shuffle (右回り / Next item)|
|<kbd>Shift</kbd>+<kbd>S</kbd>|Shuffle (左回り / Previous item)|
|<kbd>A</kbd>|AutoPlay (右回り / Next item)|
|<kbd>Shift</kbd>+<kbd>A</kbd>|AutoPlay (左回り / Previous item)|
|<kbd>G</kbd>|Gauge (右回り / Next item)|
|<kbd>Shift</kbd>+<kbd>G</kbd>|Gauge (左回り / Previous item)|
|<kbd>E</kbd>|Excessive|
|<kbd>+</kbd> or <kbd>Num+</kbd>|Adjustment (右回り、1刻み / Next item, every 1frame)|
|<kbd>-</kbd> or <kbd>Num-</kbd>|Adjustment (左回り、1刻み / Previous item, every 1frame)|
|<kbd>Shift</kbd>+<kbd>+</kbd>|Adjustment (右回り、5刻み / Next item, every 5frame)|
|<kbd>Shift</kbd>+<kbd>-</kbd>|Adjustment (左回り、5刻み / Previous item, every 5frame)|
|<kbd>Alt</kbd>+<kbd>+</kbd>|Adjustment (右回り、0.5刻み / Next item, every 0.5frame)|
|<kbd>Alt</kbd>+<kbd>-</kbd>|Adjustment (左回り、0.5刻み / Previous item, every 0.5frame)|
|<kbd>Alt</kbd>+<kbd>Shift</kbd>+<kbd>+</kbd>|Adjustment (右回り、0.1刻み / Next item, every 0.1frame)|
|<kbd>Alt</kbd>+<kbd>Shift</kbd>+<kbd>-</kbd>|Adjustment (左回り、0.1刻み / Previous item, every 0.1frame)|
|<kbd>V</kbd>|Volume (右回り / Next item)|
|<kbd>Shift</kbd>+<kbd>V</kbd>|Volume (左回り / Previous item)|
|<kbd>I</kbd>|譜面詳細データ開閉 (密度グラフ、速度グラフ、ツール値)<br>View charts detail data (chart density graph, velocity change graph and difficulty calculated value)|
|<kbd>1</kbd> or <kbd>Num1</kbd>|譜面密度グラフ(Density)を表示<br>View chart density graph|
|<kbd>2</kbd> or <kbd>Num2</kbd>|速度変化グラフ(Velocity)を表示<br>View velocity change graph|
|<kbd>3</kbd> or <kbd>Num3</kbd>|譜面レベル、矢印数情報(DifLevel)を表示<br>View difficulty calculated value|
|<kbd>4</kbd> or <kbd>Num4</kbd>|ハイスコア(HighScore)を表示<br>View highscore|
|<kbd>Q</kbd>|譜面詳細データ切替 (密度 -> 速度 -> ツール値 -> ハイスコア -> 画面非表示)<br>Next charts detail data (Chart density graph -> Velocity change graph -> Difficulty calculated value -> Highscore -> screen non-display)|
|<kbd>P</kbd>|譜面詳細データ出力<br>Output charts detail data|
|<kbd>C</kbd>|ハイスコアデータをクリップボードへコピー<br>Copy the highscore result to the clipboard|
|<kbd>Z</kbd>|セーブ有無切替<br>Switch with or without save|
|<kbd>Escape</kbd>|前の画面へ(もしくは<kbd>Shift</kbd>+<kbd>Tab</kbd>)<br>Go to the previous screen (can also use <kbd>Shift</kbd>+<kbd>Tab</kbd>)|
|<kbd>Space</kbd>|キーコンフィグ画面へ<br>Go to the key config screen|
|<kbd>Enter</kbd> or <kbd>NumEnter</kbd>|次の画面へ<br>Go to next screen|
|<kbd>Tab</kbd>|設定画面（Display）へ<br>Go to display setting screen|

#### 廃止されたショートカットキー 

|対応キー|操作|
|----|----|
|<kbd>Shift</kbd>+<kbd>Q</kbd>|譜面詳細データ切替 (ツール値 -> 密度 -> 速度)<br>Previous charts detail data (Difficulty calculated value -> Density graph -> Speed graph)|

### 設定 - 譜面セレクター
<img src="https://github.com/cwtickle/danoniplus-docs/assets/44026291/53386ab3-9582-450b-92c5-19b4c48cdb6a" width="70%">

|対応キー|操作|
|----|----|
|<kbd>D</kbd>|Difficulty (右回り / Next item)|
|<kbd>Shift</kbd>+<kbd>D</kbd>|Difficulty (左回り / Previous item)|
|<kbd>↑</kbd>|前の譜面へ<br>Previous chart|
|<kbd>↓</kbd>|次の譜面へ<br>Next chart|
|<kbd>R</kbd>|譜面ランダム選択<br>Select chart randomly|
|<kbd>Enter</kbd> or <kbd>NumEnter</kbd>|譜面セレクターを閉じる<br>Close the charts list|
|<kbd>I</kbd>|譜面詳細データ開閉 (密度グラフ、速度グラフ、ツール値)<br>View charts detail data (chart density graph, velocity change graph and difficulty calculated value)|
|<kbd>1</kbd> or <kbd>Num1</kbd>|譜面密度グラフ(Density)を表示<br>View chart density graph|
|<kbd>2</kbd> or <kbd>Num2</kbd>|速度変化グラフ(Velocity)を表示<br>View velocity change graph|
|<kbd>3</kbd> or <kbd>Num3</kbd>|譜面レベル、矢印数情報(DifLevel)を表示<br>View difficulty calculated value|
|<kbd>4</kbd> or <kbd>Num4</kbd>|ハイスコア(HighScore)を表示<br>View highscore|
|<kbd>Q</kbd>|譜面詳細データ切替 (密度 -> 速度 -> ツール値 -> ハイスコア -> 画面非表示)<br>Next charts detail data (Chart density graph -> Velocity change graph -> Difficulty calculated value -> Highscore -> screen non-display)|
|<kbd>P</kbd>|譜面詳細データ出力<br>Output charts detail data|
|<kbd>C</kbd>|ハイスコアデータをクリップボードへコピー<br>Copy the highscore result to the clipboard|
|<kbd>Escape</kbd>|前の画面へ(もしくは<kbd>Shift</kbd>+<kbd>Tab</kbd>)<br>Go to the previous screen (can also use <kbd>Shift</kbd>+<kbd>Tab</kbd>)|
|<kbd>Space</kbd>|キーコンフィグ画面へ<br>Go to the key config screen|
|<kbd>Tab</kbd>|設定画面（Display）へ<br>Go to display setting screen|

## Display Settings / 画面の見え方
<img src="https://github.com/cwtickle/danoniplus/assets/44026291/ba7e292a-f6c2-4326-b0a9-3af45ad585b3" width="70%">

|対応キー|操作|
|----|----|
|<kbd>1</kbd> or <kbd>Num1</kbd>|Display - StepZone|
|<kbd>Shift</kbd>+(<kbd>1</kbd> or <kbd>Num1</kbd>)|Display - StepZone (拡張設定用)|
|<kbd>2</kbd> or <kbd>Num2</kbd>|Display - Judgment|
|<kbd>Shift</kbd>+(<kbd>2</kbd> or <kbd>Num2</kbd>)|Display - Judgment (拡張設定用)|
|<kbd>3</kbd> or <kbd>Num3</kbd>|Display - FastSlow|
|<kbd>Shift</kbd>+(<kbd>3</kbd> or <kbd>Num3</kbd>)|Display - FastSlow (拡張設定用)|
|<kbd>4</kbd> or <kbd>Num4</kbd>|Display - LifeGauge|
|<kbd>Shift</kbd>+(<kbd>4</kbd> or <kbd>Num4</kbd>)|Display - LifeGauge (拡張設定用)|
|<kbd>5</kbd> or <kbd>Num5</kbd>|Display - Score|
|<kbd>Shift</kbd>+(<kbd>5</kbd> or <kbd>Num5</kbd>)|Display - Score (拡張設定用)|
|<kbd>6</kbd> or <kbd>Num6</kbd>|Display - MusicInfo|
|<kbd>Shift</kbd>+(<kbd>6</kbd> or <kbd>Num6</kbd>)|Display - MusicInfo (拡張設定用)|
|<kbd>7</kbd> or <kbd>Num7</kbd>|Display - FilterLine|
|<kbd>Shift</kbd>+(<kbd>7</kbd> or <kbd>Num7</kbd>)|Display - FilterLine (拡張設定用)|
|<kbd>8</kbd> or <kbd>Num8</kbd>|Display - Velocity|
|<kbd>Shift</kbd>+(<kbd>8</kbd> or <kbd>Num8</kbd>)|Display - Velocity (拡張設定用)|
|<kbd>9</kbd> or <kbd>Num9</kbd>|Display - Color|
|<kbd>Shift</kbd>+(<kbd>9</kbd> or <kbd>Num9</kbd>)|Display - Color (拡張設定用)|
|<kbd>0</kbd> or <kbd>Num0</kbd>|Display - Lyrics|
|<kbd>Shift</kbd>+(<kbd>0</kbd> or <kbd>Num0</kbd>)|Display - Lyrics (拡張設定用)|
|<kbd>+</kbd> or <kbd>Num+</kbd>|Display - Background|
|<kbd>Shift</kbd>+(<kbd>+</kbd> or <kbd>Num+</kbd>)|Display - Background (拡張設定用)|
|<kbd>-</kbd> or <kbd>Num-</kbd>|Display - ArrowEffect|
|<kbd>Shift</kbd>+(<kbd>-</kbd> or <kbd>Num-</kbd>)|Display - ArrowEffect (拡張設定用)|
|<kbd>/</kbd> or <kbd>Num/</kbd>|Display - Special|
|<kbd>Shift</kbd>+(<kbd>/</kbd> or <kbd>Num/</kbd>)|Display - Special (拡張設定用)|
|<kbd>A</kbd>|Appearance (右回り / Next item)|
|<kbd>Shift</kbd>+<kbd>A</kbd>|Appearance (左回り / Previous item)|
|<kbd>L</kbd>|Appearance (Hidden+/Sudden+/Hid&Sud+ のロックボタン / Lock button)|
|<kbd>O</kbd>|Opacity (右回り / Next item)|
|<kbd>Shift</kbd>+<kbd>O</kbd>|Opacity (左回り / Previous item)|
|<kbd>B</kbd>|HitPosition (右回り、1刻み / Next item, every 1px)|
|<kbd>T</kbd>|HitPosition (左回り、1刻み / Previous item, every 1px)|
|<kbd>Shift</kbd>+<kbd>B</kbd>|HitPosition (右回り、5刻み / Next item, every 5px)|
|<kbd>Shift</kbd>+<kbd>T</kbd>|HitPosition (左回り、5刻み / Previous item, every 5px)|
|<kbd>Z</kbd>|セーブ有無切替<br>Switch with or without save|
|<kbd>Escape</kbd>|前の画面へ(もしくは<kbd>Shift</kbd>+<kbd>Tab</kbd>)<br>Go to the previous screen (can also use <kbd>Shift</kbd>+<kbd>Tab</kbd>)|
|<kbd>Space</kbd>|キーコンフィグ画面へ<br>Go to the key config screen|
|<kbd>Enter</kbd> or <kbd>NumEnter</kbd>|次の画面へ<br>Go to next screen|
|<kbd>Tab</kbd>|拡張設定画面（Ex-Settings）へ<br>Go to expansion settings screen|

## Ex-Settings / 拡張設定

<img src="https://github.com/user-attachments/assets/d6692f8e-b080-41e2-9ff8-57c41cfbbcf3" width="70%">

|対応キー|操作|
|----|----|
|<kbd>P</kbd>|PlayWindow (右回り / Next item)|
|<kbd>Shift</kbd>+<kbd>P</kbd>|PlayWindow (左回り / Previous item)|
|<kbd>S</kbd>|StepArea (右回り / Next item)|
|<kbd>Shift</kbd>+<kbd>S</kbd>|StepArea (左回り / Previous item)|
|<kbd>F</kbd>|FrzReturn (右回り / Next item)|
|<kbd>Shift</kbd>+<kbd>F</kbd>|FrzReturn (左回り / Previous item)|
|<kbd>H</kbd>|Shaking (右回り / Next item)|
|<kbd>Shift</kbd>+<kbd>H</kbd>|Shaking (左回り / Previous item)|
|<kbd>E</kbd>|Effect (右回り / Next item)|
|<kbd>Shift</kbd>+<kbd>E</kbd>|Effect (左回り / Previous item)|
|<kbd>C</kbd>|Camoufrage (右回り / Next item)|
|<kbd>Shift</kbd>+<kbd>C</kbd>|Camoufrage (左回り / Previous item)|
|<kbd>W</kbd>|Swapping (右回り / Next item)|
|<kbd>Shift</kbd>+<kbd>W</kbd>|Swapping (左回り / Previous item)|
|<kbd>J</kbd>|JudgRange (右回り / Next item)|
|<kbd>Shift</kbd>+<kbd>H</kbd>|JudgRange (左回り / Previous item)|
|<kbd>A</kbd>|AutoRetry (右回り / Next item)|
|<kbd>Shift</kbd>+<kbd>A</kbd>|AutoRetry (左回り / Previous item)|
|<kbd>Z</kbd>|セーブ有無切替<br>Switch with or without save|
|<kbd>Escape</kbd>|前の画面へ(もしくは<kbd>Shift</kbd>+<kbd>Tab</kbd>)<br>Go to the previous screen (can also use <kbd>Shift</kbd>+<kbd>Tab</kbd>)|
|<kbd>Space</kbd>|キーコンフィグ画面へ<br>Go to the key config screen|
|<kbd>Enter</kbd> or <kbd>NumEnter</kbd>|次の画面へ<br>Go to next screen|
|<kbd>Tab</kbd>|設定画面（Settings）へ<br>Go to settings screen|

## KeyConfig / キーコンフィグ（矢印に対応するキーの割り当て）
<img src="https://github.com/cwtickle/danoniplus-docs/assets/44026291/5ca98f40-2872-4276-aa27-83d1a3110d88" width="70%">

|対応キー|操作|
|----|----|
|<kbd>Escape</kbd>|前の画面へ戻る<br>Return to the previous screen|
|<kbd>Backspace</kbd>+<kbd>Enter</kbd> or <br><kbd>Backspace</kbd>+<kbd>NumEnter</kbd>|次の画面へ<br>Go to next screen<br>(MacOS/iPadでは<kbd>Delete</kbd>)|

## Loading / ロード画面
- この画面は通常ボタンは無いが、iOSの仕様上一部にボタンがある。
<img src="https://user-images.githubusercontent.com/44026291/109269159-7247f500-784f-11eb-80ad-fad59c3a1200.png" width="70%">

|対応キー|操作|
|----|----|
|<kbd>Enter</kbd> or <kbd>NumEnter</kbd>|次の画面へ(iOS限定)<br>Go to the next screen (iOS only)|

## Main / プレイ画面
- プレイ画面におけるショートカットキーは一般的な場合を表示している。  

|対応キー|操作|
|----|----|
|<kbd>Delete</kbd>|タイトルバック(MacOS/iPadでは<kbd>Shift</kbd>+<kbd>Delete</kbd>)<br>Return to title (<kbd>Shift</kbd>+<kbd>Delete</kbd> in MacOS, iPad)|
|<kbd>Backspace</kbd>|リトライ(MacOS/iPadでは<kbd>Delete</kbd>)<br>Play again (<kbd>Delete</kbd> in MacOS, iPad)|
|<kbd>PageUp</kbd>|Hidden+, Sudden+時、レーンカバーを奥側へ移動<br>When "Hidden+" or "Sudden+" is selected, the lane cover is moved to the far side.|
|<kbd>PageDown</kbd>|Hidden+, Sudden+時、レーンカバーを手前側へ移動<br>When "Hidden+" or "Sudden+" is selected, the lane cover is moved to the front side.|
|<kbd>Shift</kbd>+<kbd>PageUp</kbd>/<kbd>PageDown</kbd>|Hidden+, Sudden+時、レーンカバーの境界線を切り替え（移動量は<kbd>PageUp</kbd>/<kbd>PageDown</kbd>と同じ）<br>Toggle lane cover boundaries when Hidden+, Sudden+. (Direction of movement is the same as PageUp/PageDown)|

### プレイ画面ショートカットの特殊割り当て
- リトライ・タイトルバックについては作品によっては割り当て先が異なる場合がある。
- キーコンフィグ画面で確認できるほか、ver33.4.0以降は通常と異なる場合のみ右下に対応するショートカットキーが表示される。  
<img src="https://github.com/cwtickle/danoniplus/assets/44026291/ef172c31-bf10-4b11-8e23-1cec6a5d1ee9" width="70%">

## Result / 結果画面
- 結果画面のみ、2秒の待ち時間を設けてから有効化する。  
<img src="https://github.com/cwtickle/danoniplus/assets/44026291/678de674-9276-419f-8323-7ec1168cf749" width="70%">


|対応キー|操作|
|----|----|
|<kbd>Escape</kbd>|前の画面へ(もしくは<kbd>Shift</kbd>+<kbd>Tab</kbd>)<br>Go to the previous screen (can also use <kbd>Shift</kbd>+<kbd>Tab</kbd>)|
|<kbd>C</kbd>|結果をクリップボードへコピー(<kbd>Ctrl</kbd>+<kbd>C</kbd>)時は無効)<br>Copy the result to the clipboard (Invalid when <kbd>Ctrl</kbd>+<kbd>C</kbd>)|
|<kbd>Shift</kbd>+<kbd>C</kbd>|結果(デフォルトフォーマット)をクリップボードへコピー<br>Copy the result (Default Format) to the clipboard|
|<kbd>X</kbd>|結果をX (Twitter)へ投稿<br>Post the results to X (Twitter)|
|<kbd>D</kbd>|結果をDiscordへ投稿（リンクのみ）<br>Post the results to Discord server (link only)|
|<kbd>P</kbd>|結果をSNS投稿用の画像として作成しクリップボードへコピー<br>Create the result as an image for SNS posting and copy it to the clipboard|
|<kbd>Shift</kbd>+<kbd>P</kbd>|SNS投稿用の画像を表示（コピー可能）<br>Display images for SNS posting (can be copied)|
|<kbd>Backspace</kbd>|リトライ(MacOS/iPadでは<kbd>Delete</kbd>)<br>Play again (<kbd>Delete</kbd> in MacOS, iPad)|

### 関連項目
- [オブジェクト一覧](./ObjectReferenceIndex) &gt; [g_shortcutObj](obj-v0017-g_shortcutObj)
- [譜面ヘッダー仕様](./dos_header) &gt; [scArea](./dos-h0096-scArea)

### 更新履歴

|Version|変更内容|
|----|----|
|[v39.5.0](https://github.com/cwtickle/danoniplus/releases/tag/v39.5.0)|・データ管理画面追加に伴い、各種ショートカットキーを追加|
|[v39.2.0](https://github.com/cwtickle/danoniplus/releases/tag/v39.2.0)|・階層が多い場合、アルファマスク用の境界線を切り替えるショートカットキーを追加|
|[v39.0.0](https://github.com/cwtickle/danoniplus/releases/tag/v39.0.0)|・Ex-Settings画面追加に伴い、各種ショートカットキーを追加|
|[v37.1.0](https://github.com/cwtickle/danoniplus/releases/tag/v37.1.0)|・AppearanceのLockボタンに対するショートカットキーを追加|
|[v36.4.1](https://github.com/cwtickle/danoniplus/releases/tag/v36.4.1)|・Twitter投稿のショートカットキーを「T」から「X」に変更<br>・GitterリンクをDiscordリンクへ変更<br>・Discordリンクのショートカットキーを「G」から「D」に変更|
|[v36.2.0](https://github.com/cwtickle/danoniplus/releases/tag/v36.2.0)|・ハイスコア表示及びハイスコアをクリップボードへコピーするショートカットを追加|
|[v35.4.0](https://github.com/cwtickle/danoniplus/releases/tag/v35.4.0)|・Display設定の拡張用ショートカットを追加|
|[v34.6.0](https://github.com/cwtickle/danoniplus/releases/tag/v34.6.0)|・デフォルトのリザルトデータコピー用のショートカットを追加|
|[v33.5.0](https://github.com/cwtickle/danoniplus/releases/tag/v33.5.0)|・SNS投稿用の画像を表示する機能を実装|
|[v33.4.0](https://github.com/cwtickle/danoniplus/releases/tag/v33.4.0)|・プレイ画面のショートカットキーが通常と異なる場合に、プレイ画面右下に表示するよう変更|
|[v33.1.0](https://github.com/cwtickle/danoniplus/releases/tag/v33.1.0)|・結果をSNS投稿用の画像として作成しクリップボードへコピーする機能を実装<br>(クリップボードコピーに対応しない場合はSNS投稿用の画像を表示)|
|[v32.6.1](https://github.com/cwtickle/danoniplus/releases/tag/v32.6.1)|・Excessive追加に伴い、ショートカットキーを割り当て|
|[v32.5.0](https://github.com/cwtickle/danoniplus/releases/tag/v32.5.0)|・譜面明細表示の順番変更に伴い、ショートカットキーを入れ替え|
|[v31.0.0](https://github.com/cwtickle/danoniplus/releases/tag/v31.0.0)|・HitPosition追加に伴い、ショートカットキーを割り当て|
|[v28.0.1](https://github.com/cwtickle/danoniplus/releases/tag/v28.0.1)|・譜面明細画面の速度変化／譜面密度グラフ、譜面レベル表示を切替するショートカットキーを復活（画面の非表示を兼ねる）|
|[v27.8.0](https://github.com/cwtickle/danoniplus/releases/tag/v27.8.0)|・譜面明細画面の速度変化グラフ、譜面密度グラフ、譜面レベル表示に移動するショートカットキーを割り当て<br>・譜面明細画面のページ切替に関するショートカットを廃止|
|[v24.4.0](https://github.com/cwtickle/danoniplus/releases/tag/v24.4.0)|・キーコンフィグ画面のPlayボタンにショートカットキーを割り当て|
|[v23.3.1](https://github.com/cwtickle/danoniplus/releases/tag/v23.3.1)|・ReverseをRキー、Scrollを上下キーに変更|
|[v23.2.0](https://github.com/cwtickle/danoniplus/releases/tag/v23.2.0)|・Adjustmentのショートカット割り当て仕様を変更<br>( <kbd>Shift</kbd>+<kbd>+ / -</kbd> ±5frame / <kbd>+ / -</kbd> ±1frame / <kbd>Alt</kbd>+<kbd>+ / -</kbd> ±0.5frame / <kbd>Alt</kbd>+<kbd>Shift</kbd>+<kbd>+ / -</kbd> ±0.1frame)|
|[v23.1.0](https://github.com/cwtickle/danoniplus/releases/tag/v23.1.0)|・Speedについて±0.05倍速刻みのショートカットキーを割り当て<br>・Enterキーに割り当てている操作に対してテンキーにも対応|
|[v23.0.0](https://github.com/cwtickle/danoniplus/releases/tag/v23.0.0)|・Adjustmentのショートカット割り当て仕様を変更<br>( <kbd>Shift</kbd>+<kbd>+ / -</kbd> ±3frame / <kbd>+ / -</kbd> ±0.5frame / <kbd>Alt</kbd>+<kbd>+ / -</kbd> ±0.1frame)|
|[v21.5.0](https://github.com/cwtickle/danoniplus/releases/tag/v21.5.0)|・譜面明細表示の逆回しに対するショートカットキーを割り当て<br>・タイトル画面、結果画面において「Ctrl+C」(コピー)が機能するように空のショートカットキーを割り当て|
|[v21.0.0](https://github.com/cwtickle/danoniplus/releases/tag/v21.0.0)|・譜面セレクターに関するショートカットキー割り当てを追加|
|[v20.4.0](https://github.com/cwtickle/danoniplus/releases/tag/v20.4.0)|・タイトル画面、ロード画面のショートカットキー割り当てを追加|
|[v20.2.0](https://github.com/cwtickle/danoniplus/releases/tag/v20.2.0)|・MacOS/iPadに合わせてショートカットキーを一部変更|
|[v20.1.2](https://github.com/cwtickle/danoniplus/releases/tag/v20.1.2)|・Adjustmentのショートカットキーにテンキーを割り当て|
|[v19.5.0](https://github.com/cwtickle/danoniplus/releases/tag/v19.5.0)|・ショートカットキーを拡充|

| [< ゲーム画面の説明](./AboutGameSystem) | **ゲーム内ショートカット** | [多鍵の種類について >](./AboutKeys-fingering) |