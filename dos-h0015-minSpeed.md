**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-h0015-minSpeed) | Japanese** 

[Home](./) &gt; [譜面ヘッダー仕様](dos_header) &gt; [設定時の初期設定](dos_header#-設定時の初期設定)

| [<- stockForceDel](dos-h0085-stockForceDel) || **minSpeed** | [maxSpeed ->](dos-h0016-maxSpeed) |

## minSpeed
- 設定できる最低速度の設定

### 使い方
```
|minSpeed=0.75|
```
### 説明
設定画面で設定できる最低速度を指定します。デフォルトは1。  

<img src="https://user-images.githubusercontent.com/44026291/173166545-69468ba8-13f1-48dc-ae5c-67a567a1ff2c.png" width="80%">

### 関連項目
- [maxSpeed](dos-h0016-maxSpeed) [:pencil:](dos-h0016-maxSpeed/_edit) 設定できる最高速度

### 更新履歴

|Version|変更内容|
|----|----|
|[v5.2.0](https://github.com/cwtickle/danoniplus/releases/tag/v5.2.0)|・初回実装|

| [<- stockForceDel](dos-h0085-stockForceDel) || **minSpeed** | [maxSpeed ->](dos-h0016-maxSpeed) |