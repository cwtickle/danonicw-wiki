**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-h0050-makerView) | Japanese** 

[Home](./) &gt; [譜面ヘッダー仕様](dos_header) &gt; [クレジット関連、共通設定](dos_header#-クレジット関連共通設定)

| [<- tuning](dos-h0017-tuning) | **makerView** | [hashTag ->](dos-h0018-hashTag) |

## makerView
- 譜面別の制作者名表示設定

### 使い方
```
|makerView=true|
```
### 説明
譜面別の制作者表示を設定画面・結果画面に表示するかどうかを設定できます。  
また有効化(true)することで、譜面別データ(LocalStorage)のハイスコアキー項目に制作者名を付加できるようになります。  

デフォルトは`false`(表示しない)です。

|値|既定|内容|
|----|----|----|
|false|*|譜面別の制作者表示はリザルトコピーのみ<br>LocalStorageのハイスコアキー項目：キー数＋譜面名|
|true||譜面別の制作者表示を設定・結果画面へ拡大する<br>LocalStorageのハイスコアキー項目：キー数＋譜面名＋制作者名|

### 関連項目
- [**difData**](dos-h0002-difData) [:pencil:](dos-h0002-difData/_edit) 譜面情報 
- [difSelectorUse](dos-h0051-difSelectorUse) [:pencil:](dos-h0051-difSelectorUse/_edit) 譜面選択セレクターの利用有無
- [**tuning**](dos-h0017-tuning) [:pencil:](dos-h0017-tuning/_edit) 製作者クレジット

### 更新履歴

|Version|変更内容|
|----|----|
|[v8.2.0](https://github.com/cwtickle/danoniplus/releases/tag/v8.2.0)|・初回実装|

| [<- tuning](dos-h0017-tuning) | **makerView** | [hashTag ->](dos-h0018-hashTag) |