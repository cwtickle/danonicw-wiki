**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-h0016-maxSpeed) | Japanese** 

[Home](./) &gt; [譜面ヘッダー仕様](dos_header) &gt; [設定時の初期設定](dos_header#-設定時の初期設定)

| [<- minSpeed](dos-h0015-minSpeed) | **maxSpeed** | [difSelectorUse ->](dos-h0051-difSelectorUse) |

## maxSpeed
- 設定できる最高速度の設定

### 使い方
```
|maxSpeed=20|
```
### 説明
設定画面で設定できる最高速度を指定します。デフォルトは10。  

<img src="https://user-images.githubusercontent.com/44026291/173166603-8c2c5053-b5ef-4cf4-a77f-d9f92711567b.png" width="80%">

### 関連項目
- [minSpeed](dos-h0015-minSpeed) [:pencil:](dos-h0015-minSpeed/_edit) 設定できる最低速度

### 更新履歴

|Version|変更内容|
|----|----|
|[v5.2.0](https://github.com/cwtickle/danoniplus/releases/tag/v5.2.0)|・初回実装|

| [<- minSpeed](dos-h0015-minSpeed) | **maxSpeed** | [difSelectorUse ->](dos-h0051-difSelectorUse) |