**[English](https://github.com/cwtickle/danoniplus-docs/wiki/Changelog-v29) | Japanese**

[^ Back to All Version List (Security Policy)](https://github.com/cwtickle/danoniplus/security/policy)  
[^ Back to Update Info](./UpdateInfo#-v29-changelog)

[**<- v30**](Changelog-v30) | **v29** | [**v28 ->**](Changelog-v28)  
(**🔖 [24 Releases](https://github.com/cwtickle/danoniplus/releases?q=tag%3Av29)** )

## 🔃 Files changed (v29)

| Directory | FileName            |                                                                                             | Last Updated                                  |
| --------- | ------------------- | ------------------------------------------------------------------------------------------- | --------------------------------------------- |
| /js       | danoni_main.js      | [📥](https://github.com/cwtickle/danoniplus/releases/download/v29.4.15/danoni_main.js)      | **v29.4.15**                                  |
| /js/lib   | danoni_constants.js | [📥](https://github.com/cwtickle/danoniplus/releases/download/v29.4.15/danoni_constants.js) | **v29.4.15**                                  |

<details>
<summary>Changed file list before v28</summary>

| Directory | FileName                                                                                                                                               |                                                                                                                                                                                                                                                                                                  | Last Updated                                |
| --------- | ------------------------------------------------------------------------------------------------------------------------------------------------------ | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ | ------------------------------------------- |
| /js/lib   | danoni_localbinary.js                                                                                                                                  | [📥](https://github.com/cwtickle/danoniplus/releases/download/v15.1.0/danoni_localbinary.js)                                                                                                                                                                                                     | [v15.1.0](./Changelog-v15#v1510-2020-05-21) |
| /css      | danoni_main.css                                                                                                                                        | [📥](https://github.com/cwtickle/danoniplus/releases/download/v28.3.1/danoni_main.css)                                                                                                                                                                                                           | [v28.3.1](Changelog-v28#v2831-2022-10-16)   |
| /img      | aaShadow.svg<br>arrow.svg<br>arrowShadow.svg<br>borderline.svg<br>c.svg<br>cursor.svg<br>giko.svg<br>iyo.svg<br>monar.svg<br>morara.svg<br>onigiri.svg | [📥](https://github.com/cwtickle/danoniplus/releases/download/v15.1.0/img.zip)                                                                                                                                                                                                                   | [v15.1.0](./Changelog-v15#v1510-2020-05-21) |
| /skin     | danoni_skin_default.css<br>danoni_skin_light.css<br>danoni_skin_skyblue.css                                                                            | [📥](https://github.com/cwtickle/danoniplus/releases/download/v21.4.2/danoni_skin_default.css)<br>[📥](https://github.com/cwtickle/danoniplus/releases/download/v21.4.2/danoni_skin_light.css)<br>[📥](https://github.com/cwtickle/danoniplus/releases/download/v21.4.2/danoni_skin_skyblue.css) | [v21.4.2](./Changelog-v21#v2142-2021-04-07) |

</details>

⭐ New Features / 🛠️ Improvements / 🐞 Bug Fixes  
( Files changed: ⚪ danoni_main.js / 🔵 danoni_constants.js / 🔴 css files )

## [Warning] 更新終了後の不具合情報
- [更新を終了したバージョンの不具合情報#v29](./DeprecatedVersionBugs#v29) を参照

## v29.4.15 ([2024-11-04](https://github.com/cwtickle/danoniplus/releases/tag/v29.4.15))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v29.4.15/total)](https://github.com/cwtickle/danoniplus/archive/v29.4.15.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v29.4.15/support/v29?style=social)](https://github.com/cwtickle/danoniplus/compare/v29.4.15...support/v29#files_bucket)

- 🛠️ ハッシュタグの前後に半角スペースを追加 ( PR [#1718](https://github.com/cwtickle/danoniplus/pull/1718) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v29.4.15)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v29.4.14...v29.4.15#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v29.4.15)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v29.4.15.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v29.4.15.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v29.4.15/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v29.4.15/js/lib/danoni_constants.js)
> / 🎣 [**v38.0.0**](./Changelog-v38#v3800-2024-11-04)

## v29.4.14 ([2024-09-25](https://github.com/cwtickle/danoniplus/releases/tag/v29.4.14))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v29.4.14/total)](https://github.com/cwtickle/danoniplus/archive/v29.4.14.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v29.4.14/support/v29?style=social)](https://github.com/cwtickle/danoniplus/compare/v29.4.14...support/v29#files_bucket)

- 🐞 **C)** ImgType で拡張子の指定がないときに、強制的に他の拡張子に変更する機能の問題を修正 ( PR [#1708](https://github.com/cwtickle/danoniplus/pull/1708) ) <- :boom: [**v23.1.0**](./Changelog-v23#v2310-2021-09-11)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v29.4.14)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v29.4.13...v29.4.14#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v29.4.14)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v29.4.14.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v29.4.14.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v29.4.14/js/danoni_main.js)
> / 🎣 [**v37.6.2**](./Changelog-v37#v3762-2024-09-25)

## v29.4.13 ([2024-08-14](https://github.com/cwtickle/danoniplus/releases/tag/v29.4.13))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v29.4.13/total)](https://github.com/cwtickle/danoniplus/archive/v29.4.13.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v29.4.13/support/v29?style=social)](https://github.com/cwtickle/danoniplus/compare/v29.4.13...support/v29#files_bucket)

- 🐞 **C)** 一部文字列の置換処理がおかしくなる問題を修正 ( PR [#1697](https://github.com/cwtickle/danoniplus/pull/1697) ) <- :boom: [**v27.0.0**](./Changelog-v27#v2700-2022-03-18)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v29.4.13)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v29.4.12...v29.4.13#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v29.4.13)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v29.4.13.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v29.4.13.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v29.4.13/js/danoni_main.js)
> / 🎣 [**v37.4.0**](./Changelog-v37#v3740-2024-08-14)

## v29.4.12 ([2024-05-18](https://github.com/cwtickle/danoniplus/releases/tag/v29.4.12))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v29.4.12/total)](https://github.com/cwtickle/danoniplus/archive/v29.4.12.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v29.4.12/support/v29?style=social)](https://github.com/cwtickle/danoniplus/compare/v29.4.12...support/v29#files_bucket)

- 🛠️ 結果画面の旧 Twitter、Gitter のリンクを X, Discord に変更 ( PR [#1661](https://github.com/cwtickle/danoniplus/pull/1661) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v29.4.12)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v29.4.11...v29.4.12#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v29.4.12)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v29.4.12.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v29.4.12.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v29.4.12/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v29.4.12/js/lib/danoni_constants.js)
> / 🎣 [**v36.4.1**](./Changelog-v36#v3641-2024-05-18)

## v29.4.11 ([2024-03-17](https://github.com/cwtickle/danoniplus/releases/tag/v29.4.11))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v29.4.11/total)](https://github.com/cwtickle/danoniplus/archive/v29.4.11.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v29.4.11/support/v29?style=social)](https://github.com/cwtickle/danoniplus/compare/v29.4.11...support/v29#files_bucket)

- 🐞 **C)** フリーズアローの終点と始点が近いとき、キーを押しても次のフリーズアローが反応しないことがある問題を修正 ( PR [#1627](https://github.com/cwtickle/danoniplus/pull/1627), [#1628](https://github.com/cwtickle/danoniplus/pull/1628) ) <- :boom: [**v29.0.1**](./Changelog-v29#v2901-2022-11-05)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v29.4.11)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v29.4.10...v29.4.11#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v29.4.11)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v29.4.11.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v29.4.11.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v29.4.11/js/danoni_main.js)
> / 🎣 [**v35.4.3**](./Changelog-v35#v3543-2024-03-17)

## v29.4.10 ([2023-11-05](https://github.com/cwtickle/danoniplus/releases/tag/v29.4.10))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v29.4.10/total)](https://github.com/cwtickle/danoniplus/archive/v29.4.10.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v29.4.10/support/v29?style=social)](https://github.com/cwtickle/danoniplus/compare/v29.4.10...support/v29#files_bucket)

- 🛠️ 右シフトキーが環境により動作しない問題を改善 ( PR [#1593](https://github.com/cwtickle/danoniplus/pull/1593) ) <- :boom: [**v16.0.0**](./Changelog-v16#v1600-2020-08-06)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v29.4.10)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v29.4.9...v29.4.10#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v29.4.10)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v29.4.10.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v29.4.10.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v29.4.10/js/danoni_main.js)
> / 🎣 [**v34.5.1**](./Changelog-v34#v3451-2023-11-05)

## v29.4.9 ([2023-09-02](https://github.com/cwtickle/danoniplus/releases/tag/v29.4.9))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v29.4.9/total)](https://github.com/cwtickle/danoniplus/archive/v29.4.9.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v29.4.9/support/v29?style=social)](https://github.com/cwtickle/danoniplus/compare/v29.4.9...support/v29#files_bucket)

- 🐞 **C)** エラー時に表示される音楽ファイルの参照先 URL が実際と異なる問題を修正 ( PR [#1540](https://github.com/cwtickle/danoniplus/pull/1540) ) <- :boom: [**v29.4.1**](./Changelog-v29#v2941-2023-01-28)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v29.4.9)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v29.4.8...v29.4.9#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v29.4.9)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v29.4.9.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v29.4.9.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v29.4.9/js/danoni_main.js)
> / 🎣 [**v33.3.0**](./Changelog-v33#v3330-2023-08-22)

## v29.4.8 ([2023-08-20](https://github.com/cwtickle/danoniplus/releases/tag/v29.4.8))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v29.4.8/total)](https://github.com/cwtickle/danoniplus/archive/v29.4.8.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v29.4.8/support/v29?style=social)](https://github.com/cwtickle/danoniplus/compare/v29.4.8...support/v29#files_bucket)

- 🐞 **C)** stockForceDel 使用中に除外する検索対象が未定義の場合、エラーになる問題を修正 ( PR [#1536](https://github.com/cwtickle/danoniplus/pull/1536) ) <- :boom: [**v25.0.0**](./Changelog-v25#v2500-2022-01-04)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v29.4.8)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v29.4.7...v29.4.8#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v29.4.8)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v29.4.8.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v29.4.8.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v29.4.8/js/danoni_main.js)
> / 🎣 [**v33.2.0**](./Changelog-v33#v3320-2023-08-20)

## v29.4.7 ([2023-08-14](https://github.com/cwtickle/danoniplus/releases/tag/v29.4.7))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v29.4.7/total)](https://github.com/cwtickle/danoniplus/archive/v29.4.7.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v29.4.7/support/v29?style=social)](https://github.com/cwtickle/danoniplus/compare/v29.4.7...support/v29#files_bucket)

- 🐞 **B)** フリーズアローの終点と次のフリーズアローの始点が近い場合の判定不具合を修正 ( PR [#1530](https://github.com/cwtickle/danoniplus/pull/1530), Issue [#1529](https://github.com/cwtickle/danoniplus/pull/1529) ) <- :boom: [**v29.0.1**](./Changelog-v29#v2901-2022-11-05)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v29.4.7)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v29.4.6...v29.4.7#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v29.4.7)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v29.4.7.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v29.4.7.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v29.4.7/js/danoni_main.js)
> / 🎣 [**v33.1.2**](./Changelog-v33#v3312-2023-08-14)

## v29.4.6 ([2023-05-31](https://github.com/cwtickle/danoniplus/releases/tag/v29.4.6))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v29.4.6/total)](https://github.com/cwtickle/danoniplus/archive/v29.4.6.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v29.4.6/support/v29?style=social)](https://github.com/cwtickle/danoniplus/compare/v29.4.6...support/v29#files_bucket)

- 🐞 **C)** customCreditWidth が使用できない問題を修正 ( PR [#1496](https://github.com/cwtickle/danoniplus/pull/1496) ) <- :boom: [**v23.4.0**](./Changelog-v23#v2340-2021-10-02)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v29.4.6)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v29.4.5...v29.4.6#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v29.4.6)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v29.4.6.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v29.4.6.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v29.4.6/js/danoni_main.js)
> / 🎣 [**v32.3.0**](./Changelog-v32#v3230-2023-06-02)

## v29.4.5 ([2023-04-26](https://github.com/cwtickle/danoniplus/releases/tag/v29.4.5))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v29.4.5/total)](https://github.com/cwtickle/danoniplus/archive/v29.4.5.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v29.4.5/support/v29?style=social)](https://github.com/cwtickle/danoniplus/compare/v29.4.5...support/v29#files_bucket)

- 🛠️ キー数名称（key）を変更した場合に、その名前が譜面リストのキー数フィルタ名にも反映されるよう変更 ( PR [#1468](https://github.com/cwtickle/danoniplus/pull/1468) )
- 🐞 **C)** 譜面リスト選択時にキーコンフィグ画面で止まることがある問題を修正 ( PR [#1470](https://github.com/cwtickle/danoniplus/pull/1470) ) <- :boom: [**v24.5.1**](./Changelog-v24#v2451-2021-12-13)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v29.4.5)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v29.4.4...v29.4.5#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v29.4.5)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v29.4.5.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v29.4.5.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v29.4.5/js/danoni_main.js)
> / 🎣 [**v31.6.0**](./Changelog-v31#v3160-2023-04-26)

## v29.4.4 ([2023-03-22](https://github.com/cwtickle/danoniplus/releases/tag/v29.4.4))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v29.4.4/total)](https://github.com/cwtickle/danoniplus/archive/v29.4.4.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v29.4.4/support/v29?style=social)](https://github.com/cwtickle/danoniplus/compare/v29.4.4...support/v29#files_bucket)

- 🐞 **C)** 譜面リストで、上下キーを使って選択した譜面がキー数違いのときにキーコンフィグ画面へ移動すると画面が止まる問題を修正 ( PR [#1446](https://github.com/cwtickle/danoniplus/pull/1446) ) <- :boom: [**v24.5.1**](./Changelog-v24#v2451-2021-12-13)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v29.4.4)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v29.4.3...v29.4.4#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v29.4.4)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v29.4.4.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v29.4.4.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v29.4.4/js/danoni_main.js)
> / 🎣 [**v31.0.1**](./Changelog-v31#v3101-2023-03-22)

## v29.4.3 ([2023-03-04](https://github.com/cwtickle/danoniplus/releases/tag/v29.4.3))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v29.4.3/total)](https://github.com/cwtickle/danoniplus/archive/v29.4.3.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v29.4.3/support/v29?style=social)](https://github.com/cwtickle/danoniplus/compare/v29.4.3...support/v29#files_bucket)

- 🐞 **C)** 11, 11Lkey の別キーモードで Flat にしたときにスクロールが想定と逆になる問題を修正 <- :boom: [**v10.2.1**](./Changelog-v10#v1021-2019-11-20)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v29.4.3)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v29.4.2...v29.4.3#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v29.4.3)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v29.4.3.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v29.4.3.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v29.4.3/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v29.4.3/js/lib/danoni_constants.js)

## v29.4.2 ([2023-02-26](https://github.com/cwtickle/danoniplus/releases/tag/v29.4.2))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v29.4.2/total)](https://github.com/cwtickle/danoniplus/archive/v29.4.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v29.4.2/support/v29?style=social)](https://github.com/cwtickle/danoniplus/compare/v29.4.2...support/v29#files_bucket)

- 🐞 **C)** カスタムキーの divX が未定義のとき、ステップゾーンが並ばない問題を修正 ( PR [#1414](https://github.com/cwtickle/danoniplus/pull/1414) ) <- :boom: **initial**
- 🐞 **C)** カスタムキーの shuffleX が未定義のとき、エラーで止まる問題を修正 ( PR [#1414](https://github.com/cwtickle/danoniplus/pull/1414) ) <- :boom: **initial**

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v29.4.2)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v29.4.1...v29.4.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v29.4.2)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v29.4.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v29.4.2.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v29.4.2/js/danoni_main.js)
> / 🎣 [**v30.2.3**](./Changelog-v30#v3023-2023-02-26)

---

## v29.4.1 ([2023-01-28](https://github.com/cwtickle/danoniplus/releases/tag/v29.4.1))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v29.4.1/total)](https://github.com/cwtickle/danoniplus/archive/v29.4.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v29.4.1/support/v29?style=social)](https://github.com/cwtickle/danoniplus/compare/v29.4.1...support/v29#files_bucket)

- ⭐ 拡張スクロール設定に「Reverse」を追加すると既存の Reverse 設定を無効化するよう変更 ( PR [#1388](https://github.com/cwtickle/danoniplus/pull/1388) )
- 🛠️ ファイル読込で 404 エラー時に実際の絶対パスを表示するよう変更 ( PR [#1387](https://github.com/cwtickle/danoniplus/pull/1387) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v29.4.1)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v29.3.5...v29.4.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v29.4.1)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v29.4.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v29.4.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v29.4.1/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v29.4.1/js/lib/danoni_constants.js)

## v29.3.5 ([2023-01-20](https://github.com/cwtickle/danoniplus/releases/tag/v29.3.5))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v29.3.5/total)](https://github.com/cwtickle/danoniplus/archive/v29.3.5.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v29.3.5/support/v29?style=social)](https://github.com/cwtickle/danoniplus/compare/v29.3.5...support/v29#files_bucket)

- 🐞 **C)** カスタムキー定義における divX の第 2 要素が posX 末尾要素の最大値で無い場合、キーコンフィグ保存後にステップゾーン（下段）がずれる問題を修正 ( PR [#1385](https://github.com/cwtickle/danoniplus/pull/1385) ) <- :boom: **initial**

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v29.3.5)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v29.3.4...v29.3.5#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v29.3.5)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v29.3.5.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v29.3.5.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v29.3.5/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v29.3.5/js/lib/danoni_constants.js)<br>❤️ izkdic

## v29.3.4 ([2023-01-07](https://github.com/cwtickle/danoniplus/releases/tag/v29.3.4))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v29.3.4/total)](https://github.com/cwtickle/danoniplus/archive/v29.3.4.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v29.3.4/support/v29?style=social)](https://github.com/cwtickle/danoniplus/compare/v29.3.4...support/v29#files_bucket)

- 🛠️ 同時押し関連の英訳を見直し ( PR [#1378](https://github.com/cwtickle/danoniplus/pull/1378) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v29.3.4)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v29.3.3...v29.3.4#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v29.3.4)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v29.3.4.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v29.3.4.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v29.3.4/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v29.3.4/js/lib/danoni_constants.js)

## v29.3.3 ([2022-12-30](https://github.com/cwtickle/danoniplus/releases/tag/v29.3.3))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v29.3.3/total)](https://github.com/cwtickle/danoniplus/archive/v29.3.3.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v29.3.3/support/v29?style=social)](https://github.com/cwtickle/danoniplus/compare/v29.3.3...support/v29#files_bucket)

- 🐞 **B)** フリーズアローの始点判定が有効で遅ショボーンを出したときに二重判定することがある問題を修正 ( PR [#1376](https://github.com/cwtickle/danoniplus/pull/1376), Discord [2022-12-30](https://discord.com/channels/698460971231870977/944491021918683196/1058056741142671441) ) <- :boom: [**v29.0.1**](Changelog-v29#v2901-2022-11-05)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v29.3.3)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v29.3.2...v29.3.3#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v29.3.3)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v29.3.3.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v29.3.3.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v29.3.3/js/danoni_main.js)<br>❤️ MFV2 (@MFV2), すずめ (@suzme), SKB (@superkuppabros)

## v29.3.2 ([2022-12-21](https://github.com/cwtickle/danoniplus/releases/tag/v29.3.2))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v29.3.2/total)](https://github.com/cwtickle/danoniplus/archive/v29.3.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v29.3.2/support/v29?style=social)](https://github.com/cwtickle/danoniplus/compare/v29.3.2...support/v29#files_bucket)

- 🐞 **B)** フリーズアローの始点判定が有効のとき、始点判定で NG を出すと矢印側判定が二重になる問題を修正 ( PR [#1374](https://github.com/cwtickle/danoniplus/pull/1374), Gitter [2022-12-21](https://gitter.im/danonicw/community?at=63a2fa730dba3574233bc3fa) ) <- :boom: [**v29.0.1**](Changelog-v29#v2901-2022-11-05)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v29.3.2)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v29.3.1...v29.3.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v29.3.2)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v29.3.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v29.3.2.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v29.3.2/js/danoni_main.js)<br>❤️ izkdic, eintritt (decresc.)

## v29.3.1 ([2022-12-10](https://github.com/cwtickle/danoniplus/releases/tag/v29.3.1))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v29.3.1/total)](https://github.com/cwtickle/danoniplus/archive/v29.3.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v29.3.1/support/v29?style=social)](https://github.com/cwtickle/danoniplus/compare/v29.3.1...support/v29#files_bucket)

- 🛠️ ヘルプのリンクを言語別に切替できるよう変更 ( PR [#1368](https://github.com/cwtickle/danoniplus/pull/1368) )
- 🛠️ 不要引数削除、セミコロン抜け修正 ( PR [#1371](https://github.com/cwtickle/danoniplus/pull/1371) )
- 🐞 **B)** 速度が激しく変化する場合にタイミングがステップゾーン位置からずれることがある問題を修正 ( PR [#1372](https://github.com/cwtickle/danoniplus/pull/1372) ) <- :boom: **initial**

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v29.3.1)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v29.3.0...v29.3.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v29.3.1)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v29.3.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v29.3.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v29.3.1/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v29.3.1/js/lib/danoni_constants.js)<br>❤️ goe (@goe0)

## v29.3.0 ([2022-11-15](https://github.com/cwtickle/danoniplus/releases/tag/v29.3.0))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v29.3.0/total)](https://github.com/cwtickle/danoniplus/archive/v29.3.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v29.3.0/support/v29?style=social)](https://github.com/cwtickle/danoniplus/compare/v29.3.0...support/v29#files_bucket)

- ⭐ タイトル文字関連の多言語表示対応 ( PR [#1365](https://github.com/cwtickle/danoniplus/pull/1365) )
- 🛠️ シャッフル設定が無効もしくは一部無効のとき、表示メッセージを変更 ( PR [#1364](https://github.com/cwtickle/danoniplus/pull/1364) )
- 🛠️ 矢印・フリーズアロー処理部分のコード整理 ( PR [#1366](https://github.com/cwtickle/danoniplus/pull/1366) )
- 🐞 **C)** Motion オプションを使用し、フリーズアローについてタイミングをずらして押したとき、そのズレの中にストップが含まれていると、処理が止まる可能性がある問題を修正 ( PR [#1366](https://github.com/cwtickle/danoniplus/pull/1366) ) <- :boom: [**v23.1.1**](Changelog-v23#v2311-2021-09-17)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v29.3.0)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v29.2.0...v29.3.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v29.3.0)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v29.3.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v29.3.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v29.3.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v29.3.0/js/lib/danoni_constants.js)

## v29.2.0 ([2022-11-09](https://github.com/cwtickle/danoniplus/releases/tag/v29.2.0))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v29.2.0/total)](https://github.com/cwtickle/danoniplus/archive/v29.2.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v29.2.0/support/v29?style=social)](https://github.com/cwtickle/danoniplus/compare/v29.2.0...support/v29#files_bucket)

- ⭐ ショボーンに対して Fast/Slow 表示するよう変更 ( PR [#1361](https://github.com/cwtickle/danoniplus/pull/1361) )
- ⭐ 制作者クレジットについて多言語表記に対応 ( PR [#1362](https://github.com/cwtickle/danoniplus/pull/1362) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v29.2.0)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v29.1.0...v29.2.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v29.2.0)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v29.2.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v29.2.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v29.2.0/js/danoni_main.js)

## v29.1.0 ([2022-11-05](https://github.com/cwtickle/danoniplus/releases/tag/v29.1.0))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v29.1.0/total)](https://github.com/cwtickle/danoniplus/archive/v29.1.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v29.1.0/support/v29?style=social)](https://github.com/cwtickle/danoniplus/compare/v29.1.0...support/v29#files_bucket)

- ⭐ 途中終了時でもクリア判定を行うよう変更 ( PR [#1357](https://github.com/cwtickle/danoniplus/pull/1357) )
- ⭐ 楽曲クレジット表示の多言語対応 ( PR [#1358](https://github.com/cwtickle/danoniplus/pull/1358) )
- 🛠️ 関数引数名の見直し ( PR [#1359](https://github.com/cwtickle/danoniplus/pull/1359) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v29.1.0)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v29.0.1...v29.1.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v29.1.0)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v29.1.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v29.1.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v29.1.0/js/danoni_main.js)<br>❤️ goe (@goe0)

## v29.0.1 ([2022-11-05](https://github.com/cwtickle/danoniplus/releases/tag/v29.0.1))

[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v29.0.1/total)](https://github.com/cwtickle/danoniplus/archive/v29.0.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v29.0.1/support/v29?style=social)](https://github.com/cwtickle/danoniplus/compare/v29.0.1...support/v29#files_bucket)

- ⭐ フリーズアロー（始点）に対して Fast/Slow を常時カウントするよう変更 ( PR [#1352](https://github.com/cwtickle/danoniplus/pull/1352) )
- ⭐ フリーズアロー（始点）の判定範囲をマターリ、ショボーンへ拡大 ( PR [#1355](https://github.com/cwtickle/danoniplus/pull/1355) )
- 🛠️ 共通部分のコード整理 ( PR [#1353](https://github.com/cwtickle/danoniplus/pull/1353) )
- 🐞 **C)** クレジットリンク表示の折り返しが利かない問題を修正 ( PR [#1350](https://github.com/cwtickle/danoniplus/pull/1350) ) <- :boom: [**v28.2.1**](Changelog-v28#v2821-2022-10-05)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v29.0.1)
> / ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v28.6.0...v29.0.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v29.0.1)
> / 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v29.0.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v29.0.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v29.0.1/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v29.0.1/js/lib/danoni_constants.js)

[**<- v30**](Changelog-v30) | **v29** | [**v28 ->**](Changelog-v28)
