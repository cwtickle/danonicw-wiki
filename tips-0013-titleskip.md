
[^ Tips Indexに戻る](./tips-index)

| < [イベントリスナーの追加・削除](./tips-0012-event-listener) || **スキップボタンの作成方法** | [背景の表示方法](./tips-0002-background) > |

# スキップボタンの作成方法(タイトル画面)
- タイトルのアニメーションが長い場合などに、スキップボタンがあると便利な場合があります。
- タイトル画面で背景・マスクモーションを使っている場合、下記の方法でスキップボタンを作成できます。

## 使い方
- customJsを利用します。
```javascript
g_customJsObj.titleEnterFrame.push(() => {

        // スキップ開始元
        const skipFrameFrom = 0;

        // スキップ先のフレーム数
        const skipFrameTo = 500;

        if (g_scoreObj.titleFrameNum === skipFrameFrom) {
                const maskTitleSprite = document.querySelector(`#maskTitleSprite`);

                // マスク上のボタン制御を一時的に許可する
                maskTitleSprite.style.pointerEvents = `auto`;

                // マスク上のボタン作成 (大きさなどを定義)
                const lnkSkip = createCss2Button(`lnkSkip`, `Click to skip`, {
                    // マスク上のフレーム数を強制的にskipFrameToに飛ばす
                    // 本来は maskTitleFrameNum だけでよいが、背景なども同期させるならそれも変えておく
                    // モーション途中の場合、おかしくなる可能性があるためジャンプ先で必ず画像消去などを入れておくこと。
                    g_scoreObj.backTitleFrameNum = skipFrameTo;
                    g_scoreObj.maskTitleFrameNum = skipFrameTo;
      
                    // なお、html側(譜面側)で以下のように定義している - 500フレームで画像消去
                    // |masktitle_data=
                    // 0,0,../img/frzbar.png,,0,0,500,250,1
                    // 500,0
                    // |

                    // マスク上のボタン制御を元に戻す（無効化）
                    maskTitleSprite.style.pointerEvents = `none`;

                    // Skipボタンを削除
                    maskTitleSprite.removeChild(document.querySelector(`#lnkSkip`));
                }, {
                    x: g_sWidth * 3 / 4, y: 20, w: g_sWidth / 4, h: C_BTN_HEIGHT, siz: C_LBL_BTNSIZE, 
                }, g_cssObj.button_Tweet);

                // マスクへボタンを登録
                maskTitleSprite.appendChild(lnkSkip);
		
        } else if (g_scoreObj.titleFrameNum === skipFrameTo){
                // マスク上のボタン制御を元に戻す（無効化）
                maskTitleSprite.style.pointerEvents = `none`;

                // Skipボタンを削除
                maskTitleSprite.removeChild(document.querySelector(`#lnkSkip`));
        }
});
```

## ページ作成者
- ティックル

[^ Tips Indexに戻る](./tips-index)

| < [イベントリスナーの追加・削除](./tips-0012-event-listener) || **スキップボタンの作成方法** | [背景の表示方法](./tips-0002-background) > |
