**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-h0006-blankFrame) | Japanese** 


[Home](./) &gt; [譜面ヘッダー仕様](dos_header) &gt; [プレイ時間制御・譜面位置調整](dos_header#-プレイ時間制御譜面位置調整)

| [<- startFrame](dos-h0005-startFrame) | **blankFrame** | [endFrame ->](dos-h0007-endFrame) |

## blankFrame
- 曲開始までの空白フレーム数の設定

### 使い方
```
|blankFrame=200|
|blankFrame=200$300|
```
### 説明
曲が始まるまでのフレーム数を指定します。デフォルトは「200」。  
曲開始までが長い場合、この値を小さくすることで少しだけ調整が可能です。  

### 楽曲開始位置、blankFrame、adjustmentの関係
![dos-h0006-01.png](./wiki/dos-h0006-01.png)

### 関連項目
- [startFrame](dos-h0005-startFrame) [:pencil:](dos-h0005-startFrame/_edit) プレイ開始フレーム数
- [**adjustment**](dos-h0009-adjustment) [:pencil:](dos-h0009-adjustment/_edit) 譜面位置の初期調整

### 更新履歴

|Version|変更内容|
|----|----|
|[v32.1.0](https://github.com/cwtickle/danoniplus/releases/tag/v32.1.0)|・"$"区切りで譜面別に指定できるように変更（従来の使い方も可能）|
|[v1.0.0<br>(v0.42.0)](https://github.com/cwtickle/danoniplus/releases/tag/v1.0.1)|・初回実装|

| [<- startFrame](dos-h0005-startFrame) | **blankFrame** | [endFrame ->](dos-h0007-endFrame) |