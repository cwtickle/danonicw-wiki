**[English](https://github.com/cwtickle/danoniplus-docs/wiki/tips-0016-custom-key-abbreviation) | Japanese**

[^ Tips Indexに戻る](./tips-index)

| < [カスタムキーで独自のReverseを設定](./tips-0011-original-reverse) | **カスタムキーの省略形記法** || [作品ページを任意の場所に配置](./tips-0007-works-folder) > |

# カスタムキーの省略形記法
- カスタムキー定義では正確に定義すれば正しく反映されますが、何度も同じ定義を書かなくて済むように省略形記法があります。

## 1. デフォルトを採用
- 最も簡単な省略方法は記載しないことです。デフォルトの値が自動適用されます。
- ただし、**keyCtrlX**だけは指定が必要です。

#### 指定が無い場合のデフォルト値

|設定名|意味|デフォルト値|
|----|----|----|
|keyNameX|キー名, キー単位|X,key|
|minWidthX|対象キーの最小横幅|600(px)|
|charaX|読込変数の接頭辞|1a,2a,3a, ...|
|colorX|矢印色グループ|0,0,0,... (全て同一グループ)|
|stepRtnX|角度/AAの種類|0,0,0,... (全て同じ向き)|
|posX|ステップゾーン位置|0,1,2,...|
|divX|折り返し位置|**posX**の最後の数字+1,**posX**の最後の数字+1|
|blankX|ステップゾーン間隔(X座標)|55(px)|
|scaleX|矢印の描画エリアの拡大・縮小|1(1倍)|
|shuffleX|シャッフルグループ|0,0,0,... (全て同一グループ)|
|scrollX|拡張スクロール設定|(拡張スクロール設定なし)|
|assistX|アシストするキーの設定|(追加のアシスト設定なし)|
|keyGroupX|部分キーの割当先指定|0,0,0,... (全て同一グループ)|
|keyGroupOrderX|部分キーグループの表示順制御|(全てのグループを表示)|
|transKeyX|別キーモードの設定|(別キーモードの設定なし)|
|flatModeX|ステップゾーンを常時Flat化する設定|false (Flat化しない)|
|keyRetryX|リトライキー|BackSpace|
|keyTitleBackX|タイトルバックキー|Delete|
|appendX|既存キーのパターン追記設定|false (追記しない)|

### 1-1. 部分無指定
#### 使用できるカスタム定義変数
- **keyCtrlX を除く全て**

#### 利用例
- パターン2だけデフォルト値といった指定を行う場合に使用します。
<pre>
|pos7n=0,1,2,4,5,6,7$<b>0,1,2,3,4,5,6</b>|
-> |pos7n=0,1,2,4,5,6,7$|
</pre>

## 2. 他のパターンを引用
### 2-1. `(キー数)_(パターン番号-1)`
#### 使用できるカスタム定義変数
- **keyNameX, minWidthX, appendX を除く全て**

#### 置換パターン
- 全パターンは多すぎるため、7keyのパターン1の一部に絞って記述します。
    - keyCtrl: `7_0` -> `S, D, F, Space, J, K, L`
    - chara: `7_0` -> `left, leftdia, down, up, rightdia, right`

#### 利用例1: 同じキーのパターンを丸ごと引用
- キーコンフィグのみが変更されているパターン違いでは、ほとんどの設定が同じことが多いです。
- パターン1の設定を流用する設定として`(対象キー)_0`という設定が多くの場所で使えます。
<pre>
|chara9g=left,down,up,right,sleft,sdown,sup,sright,space$<b>left,down,up,right,sleft,sdown,sup,sright,space</b>|
-> |chara9g=left,down,up,right,sleft,sdown,sup,sright,space$<b>9g_0</b>|
</pre>

#### 利用例2: 異なるキーのパターンを引用して新たな設定を追加
- 10keyは9Akeyにスペースが増えたキーのため、9Akeyの設定をそのまま引用している例です。
- 他のキーの定義を引用するときは、定義より先にそのキーが定義済みである必要があります。
<pre>
|chara10=<b>left,down,up,right,space,sleft,sdown,sup,sright</b>,sspace|
-> |chara10=<b>9A_0</b>,sspace|

|scroll10=Cross::<b>1,1,-1,-1,-1,-1,-1,1,1</b>,1/Split::<b>1,1,1,1,-1,-1,-1,-1,-1</b>,-1/Alternate::<b>1,-1,1,-1,1,-1,1,-1,1</b>,-1|
-> |scroll10=Cross::<b>9A_0</b>,1/Split::<b>9A_0</b>,-1/Alternate::<b>9A_0</b>,-1|
</pre>

#### 利用例3: キーパターン追記時の相対パターン参照
- カスタムキー指定で`|appendX=true|`（既存キー上書き）にした場合、
カスタム部分の番号参照で`12_(0)`のように記載すると、カスタムパターンの1番目のデータを流用できます。
<pre>
|append12=true|
|keyCtrl12=F1,F2,F3,F4,F5,F6,F7,F8,F9,F10,F11,F12$Q,W,E,R,T,Y,U,I,O,P,Ja-@,Ja-[|
|chara12=oni,left,leftdia,down,sleft,sdown,sup,sright,space,up,rightdia,right$<b>12_(0)</b>|
|color12=1,0,1,0,3,3,3,3,0,1,0,1$<b>12_(0)</b>|
  ----> |color12=1,0,1,0,3,3,3,3,0,1,0,1$1,0,1,0,3,3,3,3,0,1,0,1| と同じ

|stepRtn12=45,0,-45,-90,giko,onigiri,iyo,c,90,135,180,225$<b>12_(0)</b>|
  ----> |stepRtn12=45,0,-45,-90,giko,onigiri,iyo,c,90,135,180,225$45,0,-45,-90,giko,onigiri,iyo,c,90,135,180,225| と同じ

|blank12=50$<b>12_(0)</b>|
  ----> |blank12=50$50| と同じ

|shuffle12=0,0,0,0,1,1,1,1,2,2,2,2$<b>12_(0)</b>|
  ----> |shuffle12=0,0,0,0,1,1,1,1,2,2,2,2$0,0,0,0,1,1,1,1,2,2,2,2| と同じ

|scroll12=Cross::1,1,1,1,-,-,-,-,1,1,1,1/Split::1,1,1,1,1,1,-,-,-,-,-,-$<b>12_(0)</b>|
  ----> |scroll12=Cross::1,1,1,1,-,-,-,-,1,1,1,1/Split::1,1,1,1,1,1,-,-,-,-,-,-$Cross::1,1,1,1,-,-,-,-,1,1,1,1/Split::1,1,1,1,1,1,-,-,-,-,-,-| と同じ
</pre>

### 2-2. `(キー数)_(パターン番号-1)_(グループ番号-1)`
#### 使用できるカスタム定義変数
- colorX, shuffleX, stepRtnX

#### 利用例
- **colorX, shuffleX, stepRtnX**は複数のグループを保持しているため、明示的にグループ1の設定を引用したい場合にこの指定をします。
<pre>
|stepRtn10=<b>0,-90,90,180,onigiri,0,-90,90,180</b>,onigiri|
-> |stepRtn10=<b>9A_0_0</b>,onigiri|
</pre>

### 2-3. `(接頭辞)>(キー数 or 部分キー名)`
#### 使用できるカスタム定義変数
- charaX

#### 置換パターン

|略名|実際の展開名|
|----|----|
|7_0|left,leftdia,down,up,rightdia,right|
|a>7_0|<b>a</b>left,<b>a</b>leftdia,<b>a</b>down,<b>a</b>up,<b>a</b>rightdia,<b>a</b>right|
|4A|left,down,up,right|
|bt3>7_0|<b>bt3</b>left,<b>bt3</b>down,<b>bt3</b>up,<b>bt3</b>right|

#### 利用例1
<pre>
|charaXX=<b>left,down,up,right,space</b>,<b>sleft,sdown,sup,sright</b>|
-> |chara9a=<b>5_0</b>,<b>s>4A</b>|
</pre>

### 2-4. `(部分キー名)`
#### 使用できるカスタム定義変数
- keyCtrlX, charaX, stepRtnX

#### 置換パターン

|設定名|略名|実際の展開名|
|----|----|----|
|keyCtrlX|4A|Left,Down,Up,Right|
|keyCtrlX|4S|S,D,E/R,F|
|keyCtrlX|4J|J,K,I/O,L|
|keyCtrlX|4W|W,E,Digit3/Digit4,R|
|keyCtrlX|4U|U,I,Digit8/Digit9,O|
|keyCtrlX|4W|W,E,Digit3/Digit4,R|
|keyCtrlX|3S|S,D,F|
|keyCtrlX|3J|J,K,L|
|keyCtrlX|3W|W,E,R|
|keyCtrlX|3Z|Z,X,C|
|charaX|4A|left,down,up,right|
|charaX|3S|left,leftdia,down|
|charaX|3J|up,rightdia,right|
|charaX|7|left,leftdia,down,space,up,rightdia,right|
|stepRtnX|4A|0,-90,90,180|
|stepRtnX|3S|0,-45,-90|
|stepRtnX|3J|90,135,180|
|stepRtnX|3Z|giko,onigiri,iyo|

#### 利用例1
<pre>
|chara9a=<b>left,down,up,right</b>,space,<b>sleft,sdown,sup,sright</b>|
-> |chara9a=<b>4A</b>,space,<b>s>4A</b>|
</pre>

#### 利用例2
- 18keyで利用する例です。
<pre>
|keyCtrl18=<b>W,E,D3/D4,R</b>,Space,<b>Left,Down,Up,Right</b>,<b>S,D,F</b>,<b>Z,X,C</b>,<b>J,K,L</b>|
-> |keyCtrl18=<b>4W</b>,Space,<b>4A</b>,<b>3S</b>,<b>3Z</b>,<b>3J</b>|

|chara18=<b>sleft,sdown,sup,sright</b>,space,<b>aleft,adown,aup,aright</b>,<b>left,leftdia,down</b>,gor,oni,iyo,<b>up,rightdia,right</b>|
->|chara18=<b>s>4A</b>,space,<b>a>4A</b>,<b>3S</b>,gor,oni,iyo,<b>3J</b>|

|stepRtn18=<b>0,-90,90,180</b>,onigiri,<b>0,-90,90,180</b>,<b>0,-45,-90</b>,<b>giko,onigiri,iyo</b>,<b>90,135,180</b>|
-> |stepRtn18=<b>4A</b>,onigiri,<b>4A</b>,<b>3S</b>,<b>3Z</b>,<b>3J</b>|
</pre>

## 3. `-1`から`-` への省略
#### 使用できるカスタム定義変数
- scrollX

#### 利用例
<pre>
|scroll18=Flat::1,1,1,1,1,1,1,1,1,<b>-1,-1,-1,-1,-1,-1,-1,-1,-1</b>|
-> |scroll18=Flat::1,1,1,1,1,1,1,1,1,<b>-,-,-,-,-,-,-,-,-</b>|
</pre>

## 4. `(開始数字)...(終了数字)`
- 開始の数字と最後の数字を元に数字列を作成します。開始の数字には小数も使用できます。
- ほぼposX用の機能ですが、他の一部設定でも一応使うことができます。

### 4-1. `(開始数字)...(終了数字)`
#### 使用できるカスタム定義変数
- keyCtrlX, colorX, posX, shuffleX, keyGroupOrderX

#### 利用例
<pre>
|pos20=0,1,2,3,5,6,7,8,10,11,12,13,14,15,16,17,18,19,20,21|
-> |pos20=<b>0...3,5...8,10...21</b>|
</pre>

### 4-2. `(開始数字)...(+加算数)`
#### 使用できるカスタム定義変数
- keyCtrlX, colorX, posX, shuffleX, keyGroupOrderX

#### 利用例
<pre>
|pos20=0,1,2,3,5,6,7,8,10,11,12,13,14,15,16,17,18,19,20,21|
-> |pos20=0...3,5...8,10...13,14...21</b>|
-> |pos20=0...</b>+3</b>,5...<b>+3<b>,10...<b>+3</b>,b0...<b>+11</b>|
</pre>

## 5. `(対象)@:(繰り返し数)`
- カンマ区切りで同じ指定を繰り返す場合に使用できます。
キーパターンを跨いだ繰り返し($区切り)には使えません。

### 5-1. `(対象)@:(繰り返し数)`
#### 使用できるカスタム定義変数
- keyCtrlX, charaX, colorX, stepRtnX, posX, shuffleX, keyGroupX, keyGroupOrderX, scrollX, assistX

#### 利用例1
- 同じ設定を9回ずつ繰り返す例です。
<pre>
|scroll18=Flat::1,1,1,1,1,1,1,1,1,-,-,-,-,-,-,-,-,-|
-> |scroll18=Flat::1<b>@:9</b>,-<b>@:9</b>|
</pre>

#### 利用例2
- 同じ設定を6回もしくは3回繰り返す例です。keyGroupXとkeyCtrlXは設定上少し特殊で、スラッシュを含む部分が1セットとなります。
<pre>
|keyGroup25=0/1,0/1,0/1,0/1,0/1,0/1,0/2,0/2,0/2,0/2,0/2,0/2,0/3,0/3,0/3,0/1/2/3/4,0/3,0/3,0/3,0/4,0/4,0/4,0/4,0/4,0/4|
-> |keyGroup25=0/1<b>@:6</b>,0/2<b>@:6</b>,0/3<b>@:3</b>,0/1/2/3/4,0/3<b>@:3</b>,0/4<b>@:6</b>|
|
</pre>

### 5-2. `(対象1!対象2!...!対象N)@:(繰り返し数)`
#### 使用できるカスタム定義変数
- keyCtrlX, charaX, colorX, stepRtnX, posX, shuffleX, keyGroupX, keyGroupOrderX, scrollX, assistX

#### 利用例1
- "0,-90,90,180"の組を3回繰り返すような場合、下記のように書けます。
- 繰り返しパターン内ではカンマ`,`では無く`!`を使うのがポイントです。
<pre>
|stepRtnXX=0,-90,90,180,0,-90,90,180,0,-90,90,180|
-> |stepRtnXX=<b>0!-90!90!180@:3</b>|
</pre>

## 関連項目
- [キー数仕様](./keys)
- [カスタムキーの作成方法](./tips-0015-make-custom-key-types)

[^ Tips Indexに戻る](./tips-index)

| < [カスタムキーで独自のReverseを設定](./tips-0011-original-reverse) | **カスタムキーの省略形記法** || [作品ページを任意の場所に配置](./tips-0007-works-folder) > |
