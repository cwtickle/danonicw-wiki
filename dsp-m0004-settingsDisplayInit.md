[← オブジェクトリファレンスに戻る](ObjectReferenceIndex)  

# 設定画面（Display）表示
### 概要
- 設定画面（Display）の各項目を表示する。

### フローチャート
- カスタムJs/スキンJsによる差し込み処理は水色で表記。
- 関連ページがあるボックスは青枠で表記。

```mermaid
flowchart TD
    A([start])-->settingsDisplayInit-->Z([end]);

subgraph settingsDisplayInit
    B["画面クリア・背景再描画<br>clearWindow"]-->C["タイトル文字描画<br>getTitleDivLabel"]
    C-->D-->E["ユーザカスタムイベント<br>g_customObj.settingsDisplay"]
    E-->F["ボタン作成処理<br>commonSettingBtn"]-->G["キー操作イベントの作成<br>setShortcutEvent"]
    G-->H["ユーザスキン設定<br>g_skinJsObj.settingsDisplay"]
end;

subgraph D["設定用ウィンドウの作成 / createSettingsDisplayWindow"]
    x1([start])-->a1-->b1["設定用ラベル・ボタンの作成<br>createGeneralSetting"]-->y1([end])
end;

subgraph a1["Displayボタンの作成 / makeDisplayButton"]
    x2([start])-->a2{"対象のDisplayボタンは有効？"}
    a2-->|Yes| b2["ボタンの表示<br>makeSettingLblCssButton"]-->c2["ショートカット表記を表示<br>createScText"]-->y2([end])
    a2-->|No| d2["無効化用ラベルを表示<br>makeDisabledDisplayLabel"]-->y2
end;

click B "https://github.com/cwtickle/danoniplus/wiki/fnc-c0021-clearWindow" _blank;
style B stroke:#0000ff,stroke-width:4px;
style E fill:#00ffff;
click G "https://github.com/cwtickle/danoniplus/wiki/fnc-c0035-setShortcutEvent" _blank;
style G stroke:#0000ff,stroke-width:4px;
style H fill:#00ffff;
```

### 関連項目
- [ローカルストレージ仕様](LocalStorage)
- [カスタムJs(スキンJs)による処理割り込み](AboutCustomJs)