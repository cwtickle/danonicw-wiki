**Japanese only** 

| [< ゲージ設定適用順仕様](./gaugeSettingOrder) | **Gitクライアントを使ったFork、Pull Requestの方法** | [Pull Requestに上がっているソースの取得方法 >](./fetch-source) |

# Gitクライアントを使ったFork、Pull Requestの方法
- ここではGitクライアント「[fork](https://git-fork.com/)」を使った場合の例です。

## 1. Forkを作成
- GitHubの「cwtickle/danoniplus」のページに移動し、「fork」ボタンを押します。  
※これを書いた人がリポジトリのオーナーのため、この例ではボタンが押せませんが実際は押せます。  

<img src="https://github.com/cwtickle/danoniplus/assets/44026291/e5069b36-6f02-46cf-acb5-e304c040663b" width="80%">

## 2. Forkされたリポジトリが作成されたことを確認
- このような画面になればFork作成完了です。（下記はgoeさんのリポジトリの例）

<img src="https://github.com/cwtickle/danoniplus/assets/44026291/9261ff25-b540-4dbc-9ba1-8e923befb383" width="80%">

## 3. GitクライアントよりForkのCloneを作成
- GitクライアントのForkを使う場合は、「File」->「Clone」からクローン（リモートのファイルをローカルへコピー）できます。

<img src="https://github.com/cwtickle/danoniplus/assets/44026291/b3da8874-716b-4344-8406-2854d63e3b24" width="80%">

<img src="https://github.com/cwtickle/danoniplus/assets/44026291/49d40f08-18b7-4390-a342-0b7c42ba2a63" width="60%">

<img src="https://github.com/cwtickle/danoniplus/assets/44026291/04f3638d-3e60-4c54-bfcc-a83e033101e2" width="100%">

## 4. Fork元のリポジトリを取得
- この時点ではForkされたリポジトリしかトレースできないため、Fork元のリポジトリを追加します。

<img src="https://github.com/cwtickle/danoniplus/assets/44026291/9bf837b7-0752-4c74-bba2-ef4623698486" width="50%">

- 名前を「upstream」に変更し、URLに「cwtickle/danoniplus」のトップページを追加します。
- 「Test Connection」を押し、「Connection succeeded」となれば接続はOKです。「Add Remote」を押します。
<img src="https://github.com/cwtickle/danoniplus/assets/44026291/cec9d721-36c4-496a-b92f-a24e771c3553" width="60%">

- Remotesに「upstream」が追加されたことを確認したら、右クリックして「Fetch」します。
<img src="https://github.com/cwtickle/danoniplus/assets/44026291/db51b7d5-29b7-45eb-86a8-e57acf52b4f7" width="60%">

<img src="https://github.com/cwtickle/danoniplus/assets/44026291/1367363f-1f90-45ca-8383-603cfc87a9fe" width="50%">

- 「upstream」にブランチが増え、右半分の画面に「upstream/master」などが表示されれば、リポジトリ情報が取れています。
<img src="https://github.com/cwtickle/danoniplus/assets/44026291/ea091638-394e-48be-94d1-7902b7635caa" width="100%">

## 5. ブランチを切り替える
- 他のブランチに切り替える場合は、「Remotes」配下にあるブランチを右クリックして「checkout」します。

<img src="https://github.com/cwtickle/danoniplus/assets/44026291/d227dbe9-e30a-495d-a266-e0f315c963b9" width="60%">

- 「Track」を押せばブランチを切り替え、参照先のソースを変更することができます。
<img src="https://github.com/cwtickle/danoniplus/assets/44026291/48fa767e-0ffd-48f6-9049-74997c21ffba" width="50%">

- 左側の「Branches」に「develop」が追加され、チェックされた状態になっていれば切替完了です。
- 次回以降は「Branches」にあるブランチのリストを右クリックして「checkout」することでブランチの切り替えが可能です。
<img src="https://github.com/cwtickle/danoniplus/assets/44026291/209c47ab-e0ba-4f98-bb19-ccb124cdc2ca" width="80%">

- upstream配下にあるブランチも同様に切り替えが可能ですが、同じ名前のブランチ名は定義できないため、
「upstream/develop」のように名前を変える必要があります。
<img src="https://github.com/cwtickle/danoniplus/assets/44026291/f95ca7d5-1eab-42c3-85d5-79480d76b875" width="50%">

## 6. 自身のブランチをFork元のブランチと同期（追従）
- Fork元のブランチの更新が進んでしまい、自身のブランチが古くなることがあります。
- この場合は、GitHubからブランチを同期させることができます。

<img src="https://github.com/cwtickle/danoniplus/assets/44026291/8f4bea10-1569-4b8b-b88f-180bd37c8070" width="60%">

## 7. 最新の情報を取得（Fetch, Pull）
- ブランチの最新の情報を取得するには、「Fetch」を押します。
- 現在選択中のブランチの最新をローカルのフォルダに反映したい場合は、「Pull」を押します。
<img src="https://github.com/cwtickle/danoniplus/assets/44026291/767ca72a-2433-48e2-8def-432798740640" width="50%">

## 8. ブランチを作成し、機能を実装してcommit
<img src="https://github.com/cwtickle/danoniplus/assets/44026291/af435494-6b69-4705-bdea-b7d47bef7a31" width="70%">

<img src="https://github.com/cwtickle/danoniplus/assets/44026291/03052a40-bad9-42ea-905b-43e26bd58297" width="100%">

## 9. commitが完了したら、GitHubへ移動し「Pull Request」を押して「cwtickle/danoniplus」のdevelopへ変更内容をリクエスト
<img src="https://github.com/cwtickle/danoniplus/assets/44026291/6e60f333-e152-4a3c-a4eb-925e7507c1c8" width="100%">

- 下記は別リポジトリの例ですが、今回の場合でも同じように設定します。
<img src="https://github.com/cwtickle/danoniplus/assets/44026291/033bbf2d-8275-4169-8f33-9e3974c2c5b2" width="100%">

- Pull Requestのテンプレートに沿って記入し、「Create pull request」を押します。
- 未完成の場合は、「Create draft pull request」を押すと作成中としてリクエストされます。
<img src="https://github.com/cwtickle/danoniplus/assets/44026291/7933f3ef-80fa-4662-a5f3-804a50dbe85c" width="80%">

<img src="https://github.com/cwtickle/danoniplus/assets/44026291/d071a145-b110-4833-aba2-445a20d2a676" width="80%">

| [< ゲージ設定適用順仕様](./gaugeSettingOrder) | **Gitクライアントを使ったFork、Pull Requestの方法** | [Pull Requestに上がっているソースの取得方法 >](./fetch-source) |
