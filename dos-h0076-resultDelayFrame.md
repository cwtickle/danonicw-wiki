**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-h0076-resultDelayFrame) | Japanese** 

[Home](./) &gt; [譜面ヘッダー仕様](dos_header) &gt; [タイトル・結果画面の初期設定](dos_header#%EF%B8%8F-タイトル結果画面の初期設定)

| [<- masktitleButton](dos-h0043-masktitleButton) | **resultDelayFrame** | [maskresultButton ->](dos-h0044-maskresultButton) |

## resultDelayFrame
- CLEARED/FAILEDが表示されるまでの遅延フレーム数の設定

### 使い方
```
|resultDelayFrame=1000|
```
### 説明
結果表示の「CLEARED」「FAILED」の表示を遅延させるフレーム数を指定します。  
デフォルトは0フレームです。  
readyDelayFrameと同様、CSSの`animationDelay`属性を使用していますが、  
こちらは指定が無い場合、属性の付与を行いません。  
また、途中終了した場合もこの設定の有無によらず、同様に0フレーム扱いとして処理します。

### 関連項目
- [readyDelayFrame](dos-h0052-readyDelayFrame) [:pencil:](dos-h0052-readyDelayFrame/_edit) Ready?が表示されるまでの遅延フレーム数
- [**customJs**](dos-h0019-customjs) [:pencil:](dos-h0019-customjs/_edit) カスタムjsファイルの指定

### 更新履歴

|Version|変更内容|
|----|----|
|[v16.4.0](https://github.com/cwtickle/danoniplus/releases/tag/v16.4.0)|・初回実装|

| [<- masktitleButton](dos-h0043-masktitleButton) | **resultDelayFrame** | [maskresultButton ->](dos-h0044-maskresultButton) |