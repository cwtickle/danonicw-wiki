[← オブジェクトリファレンスに戻る](ObjectReferenceIndex)  

## g_workObj

### 概要
- 主にプレイ画面中に使われるオブジェクト。

### 生成タイミング
- 主にロード画面中 (loadingScoreInit) に定義される。  
ここで設定される値は設定画面や譜面ヘッダーで定義される値そのままではなく、  
プレイ画面で扱いやすいように加工している変数が多い。

## プロパティ

### Motionオプション適用時の矢印別の速度
- motionOnFrames (array)

### ライフ増減値、ノルマ値、初期値
- lifeRcv (float)
- lifeDmg (float)
- lifeBorder (float)
- lifeInit (float)

### フレーム毎の矢印・フリーズアロー情報、色変化、矢印モーション
- mkArrow (array2)
```javascript
// 対象フレーム数のときに1, 2, 3番目の矢印を出現させる
g_workObj.mkArrow[frame] = [0, 1, 2];
```
- mkFrzArrow (array2)
- mkFrzLength (array2)
- mkColor (array2)
- mkColorCd (array2)
```javascript
// 対象フレーム数のときに1, 2, 3番目の矢印の色を黄色に変更
g_workObj.mkColor[frame] = [0, 1, 2];  // 対象矢印
g_workObj.mkColorCd[frame] = [`#ffff99`, `#ffff99`, `#ffff99`]; // 変更するカラーコード

// mkColorの値が1000以上の場合は全体色変化を表す（1000を引いた値が実際に変更する矢印の対象。今回の場合は1, 2, 3番目）
g_workObj.mkColor[frame2] = [1000, 1001, 1002];  // 対象矢印(全体色変化モード)
g_workObj.mkColorCd[frame2] = [`#ffff99`, `#ffff99`, `#ffff99`]; // 変更するカラーコード
```
- mkFColorNormal (array2)
- mkFColorNormalCd (array2)
- mkFColorNormalBar (array2)
- mkFColorNormalBarCd (array2)
- mkFColorHit (array2)
- mkFColorHitCd (array2)
- mkFColorHitBar (array2)
- mkFColorHitBarCd (array2)
- mkArrowCssMotion (array2)
- mkArrowCssMotionName (array2)
- mkFrzCssMotion (array2)
- mkFrzCssMotionName (array2)

### フレーム毎の全体色変化タイミング
- mkArrowColorChangeAll (array)
- mkFrzColorChangeAll (array)

### フレーム毎の矢印の移動距離
- initY (array)
- initBoostY (array)

### フレーム毎の矢印がステップゾーンに到達するまでのフレーム数
- arrivalFrame (array)

### フレーム毎のMotionの適用フレーム数
- motionFrame (array)

### フレーム毎の速度変化（変更フレームのみ値が入る）
- speedData (array)
- boostData (array)

### キーパターン(デフォルト)に対応する矢印番号
- replaceNums (array)

### プレイ上の初期設定
- stepX (array)
- scrollDir (array)
- devidePos (array)
- stepRtn (array)
- stepHitRtn (array)
- arrowRtn (array)
- keyCtrl (array2)
- keyCtrlN (array2)
- keyHitFlg (array2)

### 現在設定中の色情報（個別、全体色変化双方）
- arrowColors (array)
- arrowColorsAll (array)
- frzNormalColors (array)
- frzNormalColorsAll (array)
- frzNormalBarColors (array)
- frzNormalBarColorsAll (array)
- frzHitColors (array)
- frzHitColorsAll (array)
- frzHitBarColors (array)
- frzHitBarColorsAll (array)

### レーン毎のモーション保持情報
- arrowCssMotions (array)
- frzCssMotions (array)

### Displayの表示有無
※ステップゾーンなど表示系で使用。speedDisp、colorDispなどもあるが実際には使用していない。
- stepzoneDisp (string)
- judgmentDisp (string)
- fastslowDisp (string)
- lifegaugeDisp (string)
- scoreDisp (string)
- filterlineDisp (string)

### ライフゲージ値
- lifeVal (number)

### 歌詞表示のフェードイン、アウト
- fadeInNo (array)
- fadeOutNo (array)
- lastFadeFrame (array)
- wordFadeFrame (array)

### 現在の速度（全体、矢印個別）
- currentSpeed (float)
- boostSpd (float)

### 現在の判定済レーン別矢印数
- judgArrowCnt (array)
- judgDummyArrowCnt (array)
- judgFrzCnt (array)
- judgDummyFrzCnt (array)
- judgFrzHitCnt (array) - フリーズアロー始点の判定完了数

### 現在表示している歌詞内容
※間の数字は歌詞表示の階層を表し、階層の数だけ存在
- word0Data (string)
- word1Data (string)

### プレイ画面の最終時間（ミリ秒）
- mainEndFrame
