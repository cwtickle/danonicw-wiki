**[English](https://github.com/cwtickle/danoniplus-docs/wiki/AboutJudgment) | Japanese** 

| [< 速度変化・Motionオプション仕様](./SpeedChange) | **判定仕様** | [ゲージ設定適用順仕様 >](./gaugeSettingOrder) |

# 判定仕様 / Judgment
- Dancing☆Onigiri (CW Edition)における判定は（疑似）フレーム単位での判定です。
- 疑似フレームについては「[疑似フレーム処理仕様](AboutFrameProcessing)」をご覧ください。

## 各判定条件（基本）
- 基本的には [g_judgObj](obj-v0028-g_judgObj) で定義した内容が反映されます。
- 対象の矢印・フリーズアロー(始点)がステップゾーン上に来たタイミングを±0フレームとした場合に、キーを押したタイミングがどれだけずれているか(早いか遅いか)によって、判定が決まります。
- 押したタイミングが遅すぎた場合や、押さなかった場合は枠外判定になり、判定対象が次の矢印・フリーズアロー(始点)へ移ります。

### 矢印

|判定名(Ja)|判定名(En)|判定条件(許容されるズレ)|Fast/Slowカウント条件|
|----|----|----|----|
|(・∀・)ｲｲ!!|:D Perfect!!|0～±2 Frames|±2 Frames|
|(`・ω・)ｼｬｷﾝ|:) Great!|±3～±4 Frames|±3～±4 Frames|
|( ´∀`)ﾏﾀｰﾘ|:\| Good|±5～±6 Frames|±5～±6 Frames|
|(´・ω・`)ｼｮﾎﾞｰﾝ|:( Bad|±7～±8 Frames|±7～±8 Frames|
|( `Д´)ｳﾜｧﾝ!!|:_( Miss...|枠外<br>(+9 Frames 以上)|(なし)|

### フリーズアロー

|判定名(Ja)|判定名(En)|判定条件(許容されるズレ)|Fast/Slowカウント条件|
|----|----|----|----|
|(ﾟ∀ﾟ)ｷﾀ-!!|:) O.K.|0～±4 Frames(始点)<br>かつ フリーズアローを最後まで押す|±2～±4 Frames(始点)|
|(・A・)ｲｸﾅｲ|:( N.G.|・±5～±8 Frames(始点)<br>・+9 Frames 以上(枠外)<br>・フリーズアローを途中で離す<br>・判定前に次のフリーズアロー(始点)が到達<br>のいずれかが合致|±5～±8 Frames(始点)|

### 空押し判定
- 空押し判定は、矢印を早押しした場合に行われます。「Excessive」設定をONにしている場合のみ適用されます。
- 空押しは矢印の判定では無いため、判定が入っても矢印が消去されることはありません。

|判定名|判定条件|
|----|----|
|Excessive|-16～-9 Frames|

## 判定の流れ
- レーンごとに判定対象の矢印・フリーズアロー名を定義することで重複判定を防いでいます。
- 判定完了後、次の矢印・フリーズアロー名を判定対象として指定する仕組みです。

### キーを押したとき
- 矢印とフリーズアロー(始点)の判定が被らないように、それぞれのステップゾーンからみた距離が近い方(残余時間が短い方)を対象として判定します。
- どちらも判定領域にかかっていないときは、ステップゾーンを光らせるだけで、判定はしません。

```mermaid
flowchart TD
A([start])-->judgeArrow-->Z([end])

subgraph judgeArrow
    B1["次に判定する対象の矢印名を取得"]-->B2["次に判定する対象のフリーズアロー名を取得"];
    B2-->B3{"次の矢印の残余時間と<br>次のフリーズアローの残余時間を比較"};
    B3-->|"矢印の<br>残余時間の方が少ない"| B4["judgeTargetArrow<br>矢印を判定"];
    B3-->|"フリーズアローの<br>残余時間の方が少ない"| B5["judgeTargetFrzArrow<br>フリーズアロー(始点)を判定"];
end;
```

### キーを途中で離したとき(フリーズアローのみ)
- フリーズアロー(始点)を押した後、途中で離した場合は離した時間が[frzAttempt](dos-h0038-frzAttempt)で示されたフレーム数に収まっていれば処理は継続されます。超過した場合は失敗とします。

### キーを押さずに画面枠外へ流れたとき
- 矢印やフリーズアローが押されずに画面枠外へ行った場合、枠外用の判定を出します。
- 一つ先の矢印／フリーズアロー（始点）が判定領域（回復判定内）に入った場合に前の矢印／フリーズアローを強制的に削除する処理を入れています。
前の矢印／フリーズアローの判定が完了していないために、次の矢印／フリーズアローの判定ができない問題を解消するためです。  
ただし、押したタイミングが前の矢印／フリーズアロー（始点）の±2フレーム以内の場合は、前の矢印／フリーズアロー（始点）の判定を優先します。

<img src="https://user-images.githubusercontent.com/44026291/198320878-116119e4-d78a-4848-86a2-c0456f51820f.png" width="70%">

## 関連項目
- [g_judgObj](obj-v0028-g_judgObj)
- [frzAttempt](dos-h0038-frzAttempt)
- [オブジェクト構成・ボタン](DOM)
- [疑似フレーム処理仕様](AboutFrameProcessing)

## 更新履歴

|Version|変更内容|
|----|----|
|[v35.4.3](https://github.com/cwtickle/danoniplus/releases/tag/v35.4.3)|・フリーズアローの終点と次のフリーズアローの始点が近い場合の判定不具合を修正 [#1627](https://github.com/cwtickle/danoniplus/pull/1627)|
|[v33.1.2](https://github.com/cwtickle/danoniplus/releases/tag/v33.1.2)|・フリーズアローの終点と次のフリーズアローの始点が近い場合の判定不具合を修正 [#1530](https://github.com/cwtickle/danoniplus/pull/1530)|
|[v32.5.0](https://github.com/cwtickle/danoniplus/releases/tag/v32.5.0)|・空押し判定実装|
|[v29.3.3](https://github.com/cwtickle/danoniplus/releases/tag/v29.3.3)|・フリーズアローの始点判定が有効で遅ショボーンを出したときに二重判定することがある問題を修正 [#1376](https://github.com/cwtickle/danoniplus/pull/1376)|
|[v29.3.2](https://github.com/cwtickle/danoniplus/releases/tag/v29.3.2)|・フリーズアローの始点判定が有効のとき、始点判定でNGを出すと矢印側判定が二重になる問題を修正 [#1374](https://github.com/cwtickle/danoniplus/pull/1374)|
|[v29.0.1](https://github.com/cwtickle/danoniplus/releases/tag/v29.0.1)|・フリーズアロー(始点)に対して始点判定によらず、Fast/Slowをカウントするよう変更<br>・フリーズアロー(始点)の判定範囲を枠外手前まで拡大|
|[v28.5.0](https://github.com/cwtickle/danoniplus/releases/tag/v28.5.0)|・前の矢印の判定が終了する前に後の矢印がステップゾーンに到達した場合、例外的に前の矢印から判定を移す処理を追加 [#1342](https://github.com/cwtickle/danoniplus/pull/1342)|
|[v18.9.1](https://github.com/cwtickle/danoniplus/releases/tag/v18.9.1)|・同一レーン上に通常矢印とフリーズアローが短い間隔で存在するとき、通常矢印の判定が優先されてしまう事象を修正 [#930](https://github.com/cwtickle/danoniplus/pull/930)|
|[v5.12.1](https://github.com/cwtickle/danoniplus/releases/tag/v5.12.1)|・前のフリーズアローの判定が終了する前に後のフリーズアローがステップゾーンに到達した場合、例外的に前のフリーズアローから判定を移す処理を追加<br>　（この処理が無かったことで、フリーズアローが止まる問題として解消）[#341](https://github.com/cwtickle/danoniplus/pull/341)|
|[v1.0.0](https://github.com/cwtickle/danoniplus/releases/tag/v1.0.1)|・初期実装|

| [< 速度変化・Motionオプション仕様](./SpeedChange) | **判定仕様** | [ゲージ設定適用順仕様 >](./gaugeSettingOrder) |
