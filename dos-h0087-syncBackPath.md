**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-h0087-syncBackPath) | Japanese** 

[Home](./) &gt; [譜面ヘッダー仕様](dos_header) &gt; [カスタムデータの取込](dos_header#-カスタムデータの取込)

| [<- baseBright](dos-h0081-baseBright) | **syncBackPath** || [customTitleUse ->](dos-h0025-customTitleUse) |

## syncBackPath
- 背景・マスクモーションで使う画像パスの基準フォルダ設定
- 共通設定 ⇒ [g_presetObj.syncBackPath](dos-s0002-customFile#背景マスクモーションで使う画像パスの基準フォルダ設定-g_presetobjsyncbackpath)

### 使い方
```
|syncBackPath=true|
```

### 説明
背景・マスクモーションで指定する画像パスを、customJsやskinTypeで指定しているようなカレントディレクトリ指定にするかどうかを指定します。  
指定すると、カレントディレクトリの基準が常に/danoniplus/js/になります。画像フォルダを/danoniplus/img/のように固定した場所に置いている場合、作品側のフォルダを移動してもdanoni_main.jsのパスを正しく設定しておけば背景・マスクモーションで指定する画像パスを見直す必要が無くなります。  
自身の作品のパスを指定したい場合、カレント指定`(..)`が使えます。

|値|既定|内容|
|----|----|----|
|false|*|カレントディレクトリ指定を使用しない（基準フォルダは作品ページがあるフォルダ）|
|true||カレントディレクトリ指定を使用する（基準フォルダは /danoniplus/js/）|

#### `syncBackPath`の指定による実際の画像パスの違い
- ここでは、作品のルートパスを `/danoniplus/works/sub/`とします。
- `|syncBackPath=false|`の場合、カレント指定`(..)`は適用されません。文字列として扱われます。

|画像パス|実際の画像パス<br>(falseのとき)|実際の画像パス<br>(trueのとき)|
|----|----|----|
|image001.png|/danoniplus/works/sub/image001.png|/danoniplus/js/image001.png|
|../img/image002.png|/danoniplus/works/img/image002.png|/danoniplus/img/image002.png|
|(..)image003.png|/danoniplus/works/sub/(..)image003.png|/danoniplus/works/sub/image003.png|

### 関連項目
- [共通設定ファイル仕様](dos_setting)
- [**customJs**](dos-h0019-customjs) [:pencil:](dos-h0019-customjs/_edit) カスタムjsファイルの指定
- [**skinType**](dos-h0054-skinType) [:pencil:](dos-h0054-skinType/_edit) スキン設定
- [基準ディレクトリ仕様](AboutBaseDirectory)

### 更新履歴

|Version|変更内容|
|----|----|
|[v25.4.0](https://github.com/cwtickle/danoniplus/releases/tag/v25.0.0)|・初回実装|

| [<- baseBright](dos-h0081-baseBright) | **syncBackPath** || [customTitleUse ->](dos-h0025-customTitleUse) |