**[English](https://github.com/cwtickle/danoniplus-docs/wiki/MigrationGuide) | Japanese** 

| [< 本体のバージョンアップ](./HowToUpdate) | **アップグレードガイド** | [更新情報概要 >](./UpdateInfo) |

# アップグレードガイド / Migration Guide
- 最新の情報は[Release](https://github.com/cwtickle/danoniplus/releases)をご覧ください。
- 更新情報の概要については[更新情報(UpdateInfo)](./UpdateInfo)もご覧ください。

## ⭐ v39 -> v40
ReleasesのFiles Changedの項目を参照してください。

### 1. `g_keyObj`のプロパティの仕様変更

- ver40.0.0より、譜面中で使っていないキーのプロパティ情報はタイトル画面表示後は自動削除されるようになりました。
- 削除する過程で、`_`区切りを行っているため、`_`を含むキー種が使えなくなります。

#### 🛠 修正事項

- 基本的に影響はないと思いますが、譜面ヘッダーのdifDataのキー名を外部から直接変更している場合、  
遅くともタイトル画面表示までに変更するようにしてください。  
- g_customJsObj.preTitle での変更を推奨。g_customJsObj.title (旧関数: customTitleInit)でも問題ありません。
- g_headerObj.keyLists に対象がいるかどうかで判断しています。この変数に必要なキー種を入れるようにしてください。
- キー種で`_`が含まれている場合、`_`を使わないように変更してください。  
キー種の名前に`_`を含ませたい場合は、keyNameXを使えば可能です。

## ⭐ v38 -> v39
ReleasesのFiles Changedの項目を参照してください。

### 1. フリーズアローの階層構造変更

- ver39.0.0より、フリーズアローの階層構造が下記のように変更になりました。
- 個々のidに対してカスタムJS等で座標変更を行っている場合、影響が出る可能性があります。

#### 変更前

- X = 矢印種類 (0～N)、 Y = その矢印のカウント番号 (_arrowCnt) を表します。
- 5keyの左矢印の20番目のフリーズアローの先頭矢印であれば、`frzTop0_20` となります。

```
frzRoot
  - frzBarX_Y (フリーズアローバー)
  - frzTopShadowX_Y (フリーズアローの先頭矢印の影部分)
  - frzTopX_Y (フリーズアローの先頭矢印)
  - frzBtmShadowX_Y (フリーズアローの後矢印の影部分)
  - frzBtmX_Y (フリーズアローの後矢印)
```

#### 変更後

- frzTopRootX_Y, frzBtmRootX_Y がグループに入り、個々の位置調整がしやすくなりました。
- frzBtmShadowX_Y と frzBtmX_Y は frzBtmRootX_Y に対する相対距離に変わるので、デフォルトのY座標が 0px になります。

```
frzRoot
  - frzBarX_Y (フリーズアローバー)
  - frzTopRootX_Y
    - frzTopShadowX_Y (フリーズアローの先頭矢印の影部分)
    - frzTopX_Y (フリーズアローの先頭矢印)
  - frzBtmRootX_Y
    - frzBtmShadowX_Y (フリーズアローの後矢印の影部分)　※座標影響を受けるid
    - frzBtmX_Y (フリーズアローの後矢印)　※座標影響を受けるid
```

#### 🛠 修正事項

- 個々のidに対してカスタムJS等で座標変更を行っている場合、上記を参考に書き換えてください。  
できれば、グループ化された frzTopRootX_Y, frzBtmRootX_Y を使ってコントロールすることをお勧めします。
- また、ver39.0.0より個々の矢印・フリーズアローに対してカスタムJSを挟めるようになったため、その方法も検討してください。
- それぞれ、`g_customJsObj.makeArrow`, `g_customJsObj.makeFrzArrow`で、引数を持っています。  
詳細は [v39.0.0 のリリースノート](https://github.com/cwtickle/danoniplus/releases/tag/v39.0.0) を参照してください。


### 2. プレイ画面の階層構造の変更

- ver39.0.0より、プレイ画面の階層構造が下記のように変更になりました。
- 個々のidに対して指定している場合は影響はありませんが、カスタムJSにて親子関係の相対参照を行っている場合は影響があります。

#### 変更前

```
mainSprite
  - stepRoot0 ～ stepRootN
  - arrowSprite0 / arrowSprite1
  - frzHit0 ～ frzHitN
```

#### 変更後

- ステップゾーン、フリーズアローヒット部分についてグループ化したスプライトができました。  
(stepSprite*N*, frzHitSprite*N*)
- また、ステップゾーン・矢印／フリーズアロー・フリーズアローヒット部分をまとめて扱える mainSprite*N* ができました。
- *N* の部分は現状スクロール方向によって変わり、通常側が「0」、リバース側が「1」になっています。  
ステップゾーン上段に位置するステップゾーン・矢印が「0」に入り、下段が「1」に入るイメージです。

```
mainSprite
  - mainSpriteN
    - stepSpriteN
      - stepRoot0 ～ stepRootN　※グループNに属するもののみ
    - arrowSpriteN
    - frzHitSpriteN
      - frzHit0 ～ frzHitN　※グループNに属するもののみ
```

#### 🛠 修正事項

- カスタムJSにて親子関係の相対参照を行っている場合はdivの親子関係が変わるため、上記を参考に見直してください。
- また、個々の矢印・フリーズアローに対しての処理であればver39.0.0よりカスタムJSを挟めるようになったため、その方法も検討してください。
- それぞれ、`g_customJsObj.makeArrow`, `g_customJsObj.makeFrzArrow`で、引数を持っています。  
詳細は [v39.0.0 のリリースノート](https://github.com/cwtickle/danoniplus/releases/tag/v39.0.0) を参照してください。


### 3. 廃止された変数とその代替

- g_baseDisp
  - g_settings.settingWindowsへ移行しました。ページの追加に対応しています。
他の設定系と同じ使い方になりました。

```javascript
g_settings.settingWindows = [optionInit, settingsDisplayInit, exSettingInit];
g_settings.settingWindowNum = 0;
```

- g_jumpSettingWindow
  - g_moveSettingWindowへ移行しました。下記のように使います。
```javascript
g_moveSettingWindow();  // １つ先の設定画面へ（設定画面の「＞」ボタンでの利用を想定）
g_moveSettingWindow(true, -1);  // １つ前の設定画面へ（設定画面の「＞」ボタンでの利用を想定）
g_moveSettingWindow(false);  // 同じ画面にとどまる（KeyConfig画面からの戻り先で指定）
```

#### 🛠 修正事項

- カスタムの設定画面を作成している場合、上記を参考に見直してください。


### 4. 判定（g_judgObj）を直接書き換えている場合の扱い

- ver39.0.0にて JudgRange 設定が追加され、設定画面側で判定範囲を変更できるようになりました。
- この画面では現在設定されている判定範囲も表示します。
- カスタムJSでの定義方法によっては、うまく反映されない場合があります。

#### 🛠 修正事項

- g_judgObj を直接書き換える場合は、設定画面より前で行ってください。
  - g_customJsObj.preTitle (初期設定、一度しか通らないため推奨)
  - g_customJsObj.title (タイトル画面表示後)
- 他の判定種類を設定させたくない場合は、共通設定ファイル（`danoni_setting.js`）にて  
`g_presetObj.settingUse` の項目にて `judgRange : false, `を追加してください。
- 他の判定種類も含めて変更したい場合は、`g_judgRanges`ごと書き換えれば対処可能です。

```javascript
g_judgRanges.Normal = [[2, 4, 6, 8, 16], [2, 4, 8]];
g_judgRanges.Narrow = [[2, 3, 4, 8, 16], [2, 4, 8]];
g_judgRanges.Hard = [[1, 3, 5, 8, 16], [1, 3, 8]];
g_judgRanges.ExHard = [[1, 2, 3, 8, 16], [1, 2, 8]]; 
```

### 5. PlayWindow, StepArea設定の扱い

- 動作上は問題ありませんが、作品演出によってはPlayWindowやStepAreaのような画面を崩す設定の場合、意図しない動作となることがあります。

#### 🛠 修正事項

- 意図しない動作となる場合は個別に`|playWindowUse=false|`や`|stepAreaUse=false|`などを指定してください。  
（もしくは、作品ページにその旨を明記）

## ⭐ v37 -> v38
ReleasesのFiles Changedの項目を参照してください。

### 1. `resetColorAndGauge`関数から`resetColorSetting` / `resetGaugeSetting`関数への分離
- ver38.0.0より、ゲージ設定で数式を使えるようにするためにsetGauge関数呼び出しまで計算させないように変更しました。
- この関係で`resetColorAndGauge`関数を廃止し、`resetColorSetting` / `resetGaugeSetting`関数の2つに分離しました。
- 実際の使用箇所は下記になります。
```js
for (let j = 0; j < g_headerObj.keyLabels.length; j++) {

	// 譜面ファイルが分割されている場合、譜面詳細情報取得のために譜面をロード
	if (g_stateObj.dosDivideFlg) {
		await loadChartFile(j);
		resetColorSetting(j);
	}
	getScoreDetailData(j);
}
const loopCount = g_stateObj.dosDivideFlg ? g_headerObj.keyLabels.length : 1;
for (let j = 0; j < loopCount; j++) {
	resetGaugeSetting(j);
}
```

#### 🛠 修正事項
- `resetColorAndGauge`関数をカスタムJSを利用している場合、上記を参考に書き換えてください。

## ⭐ v36 -> v37
ReleasesのFiles Changedの項目を参照してください。

### 1. danoni_legacy_function.jsの廃止、legacy_functions.jsの導入
- ver37.0.0より、過去互換用の関数群である `danoni_legacy_function.js` を廃止し、  
代わりにlegacy_functions.jsを導入しました。
- v16以前の関数（多くはv9以前）なので、それ以後から使い始めていた場合は影響しません。

#### 🛠 修正事項
- `danoni_legacy_function.js`にいる関数を使っている場合、新しい関数への移行を検討するか、
`legacy_functions.js`に差し替えてください。
  - [v10で追加された後継関数](./UpdateInfo-v10#5-%E9%96%A2%E6%95%B0%E5%90%8D%E3%81%AE%E5%A4%89%E6%9B%B4-customjs%E5%88%A9%E7%94%A8%E8%80%85%E5%90%91%E3%81%91) / 
[v17で追加された後継関数](./MigrationGuide#2-%E3%83%A9%E3%83%99%E3%83%AB%E3%83%9C%E3%82%BF%E3%83%B3%E8%89%B2%E4%BB%98%E3%81%8D%E3%82%AA%E3%83%96%E3%82%B8%E3%82%A7%E3%82%AF%E3%83%88%E4%BD%9C%E6%88%90%E7%94%A8%E9%96%A2%E6%95%B0%E3%81%AE%E5%A4%89%E6%9B%B4)
- ただし、`legacy_functions.js`には以下の関数が存在しません。必要な場合はカスタムJSへ移行してください。
  - createLabel, getStrLength, loadScript, importCssFile, loadMultipleFiles

### 2. 未使用定数の廃止
- ver37.0.0より、v31.3.0以降未使用となっていた定数群を削除しました。
- 移行先は `g_limitObj`, `g_graphColorObj`となっています。  
※ver37.8.0以降は `C_CLR_DEFAULTA～E, C_CLR_SFSF`を除いて`legacy_functions.js`へ移行しました。

#### 🛠 修正事項
- 必要に応じて、定数からオブジェクト変数への移行を行ってください。
- もしくは、customjs側に元の定数を再定義することでも対応可能です（当面の対応としてはこちらがお手軽）。

|旧定数|新オブジェクト変数|
|----|----|
|C_BTN_HEIGHT|g_limitObj.btnHeight|
|C_LBL_BTNSIZE|g_limitObj.btnSiz|
|C_LNK_HEIGHT|g_limitObj.lnkHeight|
|C_LBL_LNKSIZE|g_limitObj.lnkSiz|
|C_LEN_SETLBL_LEFT|g_limitObj.setLblLeft|
|C_LEN_SETLBL_WIDTH|g_limitObj.setLblWidth|
|C_LEN_SETLBL_HEIGHT|g_limitObj.setLblHeight|
|C_SIZ_SETLBL|g_limitObj.setLblSiz|
|C_LEN_SETMINI_WIDTH|g_limitObj.setMiniWidth|
|C_SIZ_SETMINI|g_limitObj.setMiniSiz|
|C_LEN_DIFSELECTOR_WIDTH|g_limitObj.difSelectorWidth|
|C_SIZ_DIFSELECTOR|g_limitObj.difSelectorSiz|
|C_LEN_DIFCOVER_WIDTH|g_limitObj.difCoverWidth|
|C_LEN_JDGCHARA_WIDTH|g_limitObj.jdgCharaWidth|
|C_LEN_JDGCHARA_HEIGHT|g_limitObj.jdgCharaHeight|
|C_SIZ_JDGCHARA|g_limitObj.jdgCharaSiz|
|C_LEN_JDGCNTS_WIDTH|g_limitObj.jdgCntsWidth|
|C_LEN_JDGCNTS_HEIGHT|g_limitObj.jdgCntsHeight|
|C_SIZ_JDGCNTS|g_limitObj.jdgCntsSiz|
|C_LEN_GRAPH_WIDTH|g_limitObj.graphWidth|
|C_LEN_GRAPH_HEIGHT|g_limitObj.graphHeight|
|C_LBL_TITLESIZE|g_limitObj.titleSiz|
|C_SIZ_MAIN|g_limitObj.mainSiz|
|C_SIZ_MUSIC_TITLE|g_limitObj.musicTitleSiz|
|C_CLR_SPEEDGRAPH_SPEED|g_graphColorObj.speed|
|C_CLR_SPEEDGRAPH_BOOST|g_graphColorObj.boost|
|C_CLR_DENSITY_MAX|g_graphColorObj.max|
|C_CLR_DENSITY_DEFAULT|g_graphColorObj.default|
|C_LEN_DENSITY_DIVISION|g_limitObj.densityDivision|

## ⭐ v35 -> v36
ReleasesのFiles Changedの項目を参照してください。

### 1. 色変化新仕様（ncolor_data）導入
- ver36.0.0より、色変化の記述仕様が新しくなりました。
- 従来の色変化（acolor_data, color_data）もそのまま使えますが、新機能はncolor_dataの方にのみ実装されています。

#### 🛠 修正事項
- 新仕様（ncolor_data）と旧仕様（acolor_data, color_data）は併用が可能です。
- 置き換える場合は[色変化 (ncolor_data)](./dos-e0002-ncolorData)のページを参考に置き換えてみてください。

### 2. 譜面ヘッダー：colorDataTypeの廃止 (ver36.1.0以降)
- 動作上の問題から、譜面ヘッダーのcolorDataTypeを廃止しています。  
使用している場合、フリーズアローのヒット時の個別色変化についてフレーム数を変える必要がある場合があります。

#### 🛠 修正事項
- |colorDataType=v6|は使えないため、フリーズアローのヒット時の個別色変化で指定するフレーム数を必要に応じて見直してください。

## ⭐ v34 -> v35
ReleasesのFiles Changedの項目を参照してください。

### 1. 高さ変更パラメータ`h`導入による変更
- ver35.0.0より、ウィンドウの高さを動的に変更するパラメータ`h`が実装されています。
- これはデフォルトで無効になっていますが、有効にする場合は背景・マスクで指定している座標に留意してください。

#### 🛠 修正事項
- 背景・マスクモーションにて、ステップゾーン(下)の位置に合わせた背景やマスクを設定している場合、  
座標指定を変数化する必要があります。
- 背景・マスクモーションでは、座標指定に変数`{}`が使用できるため、その機能を使用します。  
`g_headerObj.playingY + g_headerObj.playingHeight`が画面サイズの一番下になるように、位置を調整します。
- この設定は高さ変更パラメータ`h`を有効にした場合の対応です。無効にしている場合は対処不要です。
```
|back_data=
400,2,文字のフローテスト,,100,{g_headerObj.playingY + g_headerObj.playingHeight - 50},36,0,0,fromBig,100,forwards
|
```

## ⭐ v33 -> v34
ReleasesのFiles Changedの項目を参照してください。

## ⭐ v32 -> v33
ReleasesのFiles Changedの項目を参照してください。

### 1. スキンファイル仕様変更
- ver33.0.0よりスキンファイルの位置づけが変わり、カスタムプロパティを設定するファイルに変わりました。
- 独自のスキンファイルを作っている場合、新しい方法に変更するか  
従来のものを利用するかのどちらかを選択し変更する必要があります。

#### 🛠 修正事項
- 新しい方法に変更する場合は、スキンファイルの作り変えが必要です。
- 背景画像を利用する場合は、基準パスがCSS直指定時(./css)と譜面ヘッダー時(./js)で異なるため注意が必要です。
- 参考：[スキンファイル仕様](./AboutSkin), [スキン変更](./dos-e0008-styleData)

## ⭐ v31 -> v32
ReleasesのFiles Changedの項目を参照してください。

### 1. Shift, Ctrl, Altキーの左右キーの割り当て分離によるカスタムキー定義の変更
- ver32.0.0より、Shift, Ctrl, Altキーの左右キーが区別されるようになりました。  
これにより、従来のShift, Ctrl, Altキーは左側のみに割り当てされます。  
右側のキーに割り当てたい場合、カスタムキー定義の`keyCtrlX`の値の見直しが必要です。

|KeyCode|KeyboardEvent.code|
|----|----|
|16|ShiftLeft|
|17|ControlLeft|
|18|AltLeft|
|256|ShiftRight|
|257|ControlRight|
|258|AltRight|

#### 🛠 修正事項
- カスタムキー定義の`keyCtrlX`について、右側のキーに割り当てる場合は上記の表に従って見直してください。

## ⭐ v30 -> v31
ReleasesのFiles Changedの項目を参照してください。

### 1. 設定項目追加によるスキンファイルの変更項目について
- ver31.0.0より、設定項目「HitPosition」が追加になりました。  
この変更で、スキンファイルを独自定義している場合に設定の追加が必要となります。

#### 🛠 修正事項
- 以下を参考に、「HitPosition」用の色を定義し「danoni_skin_XXX.css」に追記してください。  
※`XXX`はスキン名
```css
.settings_HitPosition::first-letter {
	color: #ff99ff;
}
```
- また「Opacity」設定についても色変更しているため、必要であれば色を変更してください。
```css
.settings_Opacity::first-letter {
	color: #ee99ff;
}
```

### 2. 設定画面（Display設定含む）の設定用ボタンの縦サイズ変更
- 設定用ボタンの縦サイズが「23px」から「22px」に変更になりました。  
独自デザインを採用している場合、設定項目間の縦間隔が狭くなるため調整が必要になる場合があります。
- 変更前が左側、変更後が右側です。

<img src="https://user-images.githubusercontent.com/44026291/226182004-8c0abcbf-9ad9-4e3d-9327-8045a91fb93d.png" width="50%"><img src="https://user-images.githubusercontent.com/44026291/226181963-30faddfd-36a8-4d19-8861-c6a01bbd99e1.png" width="50%">

#### 🛠 修正事項
- 独自デザインの場合、設定項目の縦サイズが`22px`に収まるように調整してください。

### 3. ShapeGroup追加による11ikeyのパターン2廃止、ConfigType廃止
- ver31よりShapeGroupが追加になり、それにより11ikeyのパターン2が廃止されました。
- カスタムキー定義で11ikeyのパターン2(`11i_1`)を略記指定している場合、今後は使用できません。
- 今後は11ikeyのパターン1の中でShapeGroupを変更することでパターン2が再現できます。
<img src="https://user-images.githubusercontent.com/44026291/226166916-ff173fbc-b1a4-43ad-960a-b11d1fef179f.png" width="70%">

- また、これに合わせてConfigTypeの関連ボタンの廃止、ColorType, ColorGr., ShuffleGr.のボタンの配置変更を行っています。

#### 🛠 修正事項
- カスタムキー定義で11ikeyのパターン2(`11i_1`)を略記指定している場合、指定を削除してください。
- ConfigTypeの関連ボタンを座標指定している場合、参照しないよう修正してください。

### 4. キーパターンのスキップボタン追加 (ver30.5.0以降)
- ver30.5.0より、キーコンフィグ画面においてキーパターンのスキップボタンを実装しました。  
これによりボタンが増えているため、デザインをカスタムしている場合、追加ボタンへの対応が必要です。
- スキップボタンに対応するIDは「btnPtnChangeRR」「btnPtnChangeLL」です。

#### 🛠 修正事項
- デザインを個別に座標指定している場合、上記も合わせて修正してください。

## ⭐ v29 -> v30
ReleasesのFiles Changedの項目を参照してください。

### 1. キーコンフィグ画面の初期表示方法変更
- ver30.0.1より、キーコンフィグ画面の初期表示方法が常時キーグループを参照する方法に変わりました。  
カスタムキー定義用の設定「keyGroupX」を使用しない場合、強制的に全てのキーを表示する挙動に変わっているため、
キー変化作品で一部のステップゾーンを消している場合、念のため挙動を確認してください。

#### 🛠 修正事項
- 上記で全てのキーが表示される場合、「keyGroupX」と「keyGroupOrder」を利用することで回避が可能です。  
初期表示したいキーコンフィグについて、最初の5つの矢印のみ対象として、それ以外を非表示にしたいとします。
この場合、初期表示したいキーコンフィグのキーグループを「5」とし、全体表示用を「A」として下記のように記述します。
```
|keyExtraList=Tr|
|keyGroupTr=5/A,5/A,5/A,5/A,5/A,A,A,A,A,A,A,A,A,A,A,A,A,A,A,A|
|keyGroupOrder=5| <- キーコンフィグ上はキーグループ「5」の矢印だけ表示するよう制御
```
- キーの表示制御はcustomjsで行っている場合、次の設定を譜面に入れてください。  
こうすれば、キーコンフィグは一部の矢印のみ表示され、プレイ画面では全ての矢印が見えた状態になります。
```
|keych_data=0,A| <- 最初から全ての矢印が見えるようにする（表示・非表示の制御は従来通りcustomjsで行う）
```
- なお、根本的な対応としては **keyGroupX** と **keych_data** を使って適切に表示されるように制御することをお勧めします。
（customjsの記述量が大きく減らせるため）

## ⭐ v28 -> v29
ReleasesのFiles Changedの項目を参照してください。

### 1. g_workObj.judgFrzHitCnt[] の利用必須化
- ver29.0.0より、フリーズアロー(始点)のFast/Slowの取得開始に伴い、  
フリーズアローの始点判定の有無によらず、g_workObj.judgFrzHitCnt[] の初期化が必須となりました。
- プレイ画面前の getArrowSettings にて必要なキーレーン分の初期化は行っていますが、  
途中で（キー変更などで）レーンを強制的に後で増やす場合、その分についても初期化が必要です。  
（該当するケースは限定的と思われます）

#### 🛠 修正事項
- 実際のソースでは下記のように指定しています。  
必要があれば、レーン追加分について1で初期化してください。
```javascript
g_workObj.judgFrzHitCnt = [...Array(keyNum)].fill(1);
```

### 2. id: diffFJ の利用開始
- フリーズアロー始点のFast/Slowを実装したことで、これまで未使用になっていた diffFJ を使うようになりました。  
矢印用のFast/Slow表記：diffJの表示・非表示を切り替えている場合、フリーズアロー始点用のFast/Slow表記である diffFJ も制御が必要になります。
- なお、単にFast/Slowを初めから表示しない場合は、[Displayの譜面ヘッダー](./dos-h0057-displayUse)から可能です。

#### 🛠 修正事項
- 作品中で動的にFast/Slow表記を切り替えたい場合、`diffFJ`(フリーズアロー始点のFast/Slow)についても変更対象にするようにしてください。


## ⭐ v27 -> v28
ReleasesのFiles Changedの項目を参照してください。

### 1. 譜面明細画面のボタン変更 (ver27.7.0以前からの移行の場合のみ)
- ver27.8.0より、設定画面中にある譜面明細画面のボタンレイアウトが変わりました。  
デザインを外部から変更している場合、id名の見直しが必要です。

#### 🛠 修正事項
- 下記及び[ID一覧](./IdReferenceIndex)を参考に、id名を見直してください。
    - 削除: lnkScoreDetail, lnkScoreDetailB (共通ボタン廃止のため)
    - 追加: lnkSpeedG, lnkDensityG, lnkToolDifG (個別ボタン追加のため) 


## ⭐ v26 -> v27
ReleasesのFiles Changedの項目を参照してください。

### 1. プレイ画面から結果画面の移行フレーム廃止
- プレイ画面から結果画面の移行フレームの100ミリ秒を廃止しました。

#### 🛠 修正事項
- プレイ画面から結果画面へ移行するようなリザルトモーションを作成している場合、  
リザルトモーションのフレーム数を見直してください。  
(backresult_data, maskresult_data)

### 2. danoni_legacy_function.js へ移動した関数について
- 現行ソースで未使用の以下の関数を「danoni_legacy_function.js」へ移動しました。  
対象：loadScript, importCssFile, loadMultipleFiles  

#### 🛠 修正事項
- 今後も利用する場合は、lib/danoni_legacy_function.js をサーバーへアップロードしてください。  
※未使用の関数はコメントアウトすることをおススメします。

## ⭐ v25 -> v26
ReleasesのFiles Changedの項目を参照してください。

### 1. 一部内部関数の引数変更
- コード整理により、一部の関数の引数に変更があります。
（内部用関数のため、直接参照しているケースはほぼ無いと思われます）

|関数名|従来|変更後|
|----|----|----|
|mergeColorData|_header<br>(引数デフォルト: `color`)|_header<br>(引数デフォルト: ``)|
|changeColors|_mkColor, _mkColorCd, _objType|_mkColor, _mkColorCd, _header, _name|
|changeCssMotions|_mkCssMotion, _mkCssMotionName, _name|_header, _name, _frameNum|

#### 🛠 修正事項
- もしカスタム側でこれらの関数を使用している場合は、引数を見直してください。

## ⭐ v24 -> v25
ReleasesのFiles Changedの項目を参照してください。

### 1. フェードイン前のデータの保持方法変更
- v25.0.0より、歌詞表示及び背景・マスク表示についてはフェードイン前のデータを原則保持するようになりました。  
ただし同一階層のものは原則不要のため、除外しています。  
- この変更のほとんどは影響を受けませんが、背景・マスク表示でCSSフェードアウトを多用している場合、
フェードインでプレイ時、表示がおかしくなる可能性があります。  
※フェードアウト後、階層:ALLや階層指定で空指定している場合はこの影響を受けません。

#### 影響を受けない例
```
300,0,歌詞表示～～～
500,0   // 表示を消す一文が入っている
```

#### 🛠 修正事項 (方法1)
- CSSフェードアウトしているアニメーション名を拾い出し、danoni_setting.jsの下記変数名にリストアップしてください。  
なお下記リストは部分一致で検索を行うため、文字列の一部を共通化できる場合、まとめた方が記述を少なくできます。
```javascript
const g_presetStockForceDelList = {
	word: [],
	back: [`fade`, `out`, `disappear`],
	mask: [`fade`, `out`],
};
```

- 作品別の指定も可能です。[こちら](./dos-h0085-stockForceDel)を参考にしてください。

#### 🛠 修正事項 (方法2)
- danoni_setting.jsに以下の記述を行うことで、従来の設定を維持できます。  
まとめて無効化する方法で、取り急ぎ対策を取る場合に有用です。
```javascript
const g_presetUnStockCategories = [`word`, `back`, `mask`];
```

### 2. 内部変数名の扱い変更
- ver25.1.0より、個別・全体色変化をまとめて扱うように変更したことで、  
内部変数の扱いが変わっています。

#### 変数名が統合されたもの

|従来変数|変更後変数|用途|
|----|----|----|
|g_workObj.mkColor<br>g_workObj.mkAColor|g_workObj.mkColor|個別・全体色変化（矢印の種別）|
|g_workObj.mkColorCd<br>g_workObj.mkAColorCd|g_workObj.mkColorCd|個別・全体色変化（矢印用カラーコード）|
|g_workObj.mkFColor<br>g_workObj.mkFAColor|g_workObj.mkFColorNormal<br>g_workObj.mkFColorNormalBar<br>g_workObj.mkFColorHit<br>g_workObj.mkFColorHitBar|個別・全体色変化（フリーズアローの種別）|
|g_workObj.mkFColorCd<br>g_workObj.mkFAColorCd|g_workObj.mkFColorNormalCd<br>g_workObj.mkFColorNormalBarCd<br>g_workObj.mkFColorHitCd<br>g_workObj.mkFColorHitBarCd|個別・全体色変化（フリーズアロー用カラーコード）|

#### 変数名は同じだが配列の扱いが変わったもの

|対象変数|従来の扱い|今後の扱い|
|----|----|----|
|g_scoreObj.colorData|(フレーム数, 色番号, カラーコード)の3つ1セット|(フレーム数, 色番号, カラーコード, 全体色変化フラグ)の4つ1セット|

#### 🛠 修正事項
- これらの変数群を使用している場合、上記のように変数名を見直してください。

## ⭐ v23 -> v24
ReleasesのFiles Changedの項目を参照してください。

### 1. ラベル・メッセージ定義上書き仕様の一部変更(danoni_setting.js)
- v24.0.0で導入した複数言語対応により、g_local_msgObj, g_local_lblNameObjの定義に変更があります。

|変更前|変更後|
|----|----|
|g_local_msgObj.(ID名)|g_local_msgObj.**Ja**.(ID名)|
|g_local_lblNameObj.(ID名)|g_local_lblNameObj.**Ja**.(ID名)|

#### 🛠 修正事項
- これらの設定を使用している場合、上記のように変数名を見直してください。

### 2. メイン画面のラベル上書き設定
- danoni_setting.js にて定義している`g_lblRenames`について、
1.で定義したラベル上書きをメイン画面にも適用する場合、一部追記が必要です。

#### 🛠 修正事項
- 下記設定を danoni_setting.js に追加してください。
```javascript
/**
 * 設定名の上書き可否設定
 */
const g_lblRenames = {
	option: true,
	settingsDisplay: true,
	main: true, // この行を追加する
	keyConfig: true,
	result: true,
};
```

### 3. g_msgObj, g_lblNameObj を直接変更する場合の留意点
- v24.0.0の言語対応により、`g_msgObj`, `g_lblNameObj`は
一度`g_lang_msgObj.(lang)`, `g_lang_lblNameObj.(lang)`からのデータで上書きされるようになりました。
- これにより、直接`g_msgObj`, `g_lblNameObj`を変更している場合、  
表示名が変わらない事象が発生する可能性があります。

#### 🛠 修正事項
- `g_msgObj`, `g_lblNameObj`の直接修正を止め、  
別の`danoni_setting.js`を作成することを検討してください。
- [settingType](./dos-h0056-settingType)を使えば、別グループの共通設定を作成することができます。  
|settingType=works|と指定した場合、`danoni_setting_works.js` をその作品の共通設定にできます。
- その上で、`g_local_msgObj.(lang)`, `g_local_lblNameObj.(lang)`を`g_msgObj`, `g_lblNameObj`の代わりに使用すれば、
メッセージの上書きが可能になります。

```javascript
// danoni_setting_works.js (別設定ファイルで定義)
g_local_msgObj.Ja = {
    difficulty: `譜面を選択します。この作品では使用しません`,
    shuffle: `この作品では使用できません`,
};
```

## ⭐ v22 -> v23
ReleasesのFiles Changedの項目を参照してください。

### 1. デフォルト画像セット(danoni_setting.js)の一部変数変更
- v22.5.1で導入したデフォルト画像セット用の変数名が変わっています。

```javascript
// 旧変数
const g_presetImageSet = `classic,png`;

// 新変数 (旧変数と同じ内容にする場合)
const g_presetImageSets = [`classic,png`];
```
#### 🛠 修正事項
- 旧変数を使用している場合、新変数を使うよう変更してください。

## ⭐ v21 -> v22
ReleasesのFiles Changedの項目を参照してください。 

### 1. キーコンフィグ画面の一部デザイン変更
- シャッフルグループ変更機能実装、初期矢印色一時変更機能実装に伴い、ボタンが追加されています。

|ID名|変更内容|
|----|----|
|lblshuffleGroup|ShuffleGroupラベル|
|lnkShuffleGroup|ShuffleGroupボタン|
|sArrow*N*|シャッフルグループ番号表示ラベル ※*N*は番号が入ります|

#### 🛠 修正事項
- 全体的なデザイン変更が入る場合、上記ID名の座標を修正してください。

### 2. danoni_legacy_function.js へ移動した関数について
- 現行ソースで未使用の以下の関数を「danoni_legacy_function.js」へ移動しました。  
対象：createSprite 

#### 🛠 修正事項
- 今後も利用する場合は、lib/danoni_legacy_function.js をサーバーへアップロードしてください。

## ⭐ v20 -> v21
ReleasesのFiles Changedの項目を参照してください。 

### 1. 一部関数の見直し
- 一部関数について廃止もしくは引数の見直しを行っています。

|関数|変更内容|
|----|----|
|resetCursor|引数カット|
|resetCursorMain|廃止|
|resetCursorReplaced|廃止|
|resetCursorALL|廃止|
|setKeyConfigCursor|引数カット|

#### 🛠 修正事項
- カスタムJs等で利用している場合は注意してください。

### 2. danoni_legacy_function.js へ移動した関数について
- 現行ソースで未使用の以下の関数を「danoni_legacy_function.js」へ移動しました。  
対象：checkArrayVal, getStrLength, paddingLeft, createDivCustomLabel  

#### 🛠 修正事項
- 今後も利用する場合は、lib/danoni_legacy_function.js をサーバーへアップロードしてください。

## ⭐ v19 -> v20
ReleasesのFiles Changedの項目を参照してください。 

### 1. 変数名の変更
- 現行ソースで未使用、もしくは今後の機能拡張上支障が出る変数群に対して  
変数名の見直しを行いました。

<details>
<summary>変更対象の変数一覧（クリックして開閉）</summary>

#### 判定キャラクタ

|旧変数|新変数|デフォルト値|
|----|----|----|
|C_JCR_II|g_lblNameObj.j_ii|"(・∀・)ｲｲ!!"|
|C_JCR_SHAKIN|g_lblNameObj.j_shakin|"(`・ω・)ｼｬｷﾝ"|
|C_JCR_MATARI|g_lblNameObj.j_matari|"( ´∀`)ﾏﾀｰﾘ"|
|C_JCR_SHOBON|g_lblNameObj.j_shobon|"(´・ω・`)ｼｮﾎﾞｰﾝ"|
|C_JCR_UWAN|g_lblNameObj.j_uwan|"( `Д´)ｳﾜｧﾝ!!"|
|C_JCR_KITA|g_lblNameObj.j_kita|"(ﾟ∀ﾟ)ｷﾀ-!!"|
|C_JCR_IKNAI|g_lblNameObj.j_iknai|"(・A・)ｲｸﾅｲ"|

#### 判定領域 g_judgObj の位置

|旧変数|新変数|デフォルト値|
|----|----|----|
|C_JDG_II|g_judgPosObj.ii|0|
|C_JDG_SHAKIN|g_judgPosObj.shakin|1|
|C_JDG_MATARI|g_judgPosObj.matari|2|
|C_JDG_SHOBON|g_judgPosObj.shobon|3|
|C_JDG_UWAN|g_judgPosObj.uwan|4|
|C_JDG_KITA|g_judgPosObj.kita|0|
|C_JDG_SFSF|g_judgPosObj.sfsf|1|
|C_JDG_IKNAI|g_judgPosObj.iknai|2|

#### 設定系変数群

|旧変数|新変数|
|----|----|
|g_speeds|g_settings.speeds|
|g_speedNum|g_settings.speedNum|
|g_motions|g_settings.motions|
|g_motionNum|g_settings.motionNum|
|g_scrolls|g_settings.scrolls|
|g_scrollNum|g_settings.scrollNum|
|g_reverses|g_settings.reverses|
|g_reverseNum|g_settings.reverseNum|
|g_shuffles|g_settings.shuffles|
|g_shuffleNum|g_settings.shuffleNum|
|g_autoPlays|g_settings.autoPlays|
|g_autoPlayNum|g_settings.autoPlayNum|
|g_gauges|g_settings.gauges|
|g_gaugeNum|g_settings.gaugeNum|
|g_adjustments|g_settings.adjustments|
|g_adjustmentNum|g_settings.adjustmentNum|
|g_volumes|g_settings.volumes|
|g_volumeNum|g_settings.volumeNum|
|g_appearances|g_settings.appearances|
|g_appearanceNum|g_settings.appearanceNum|
|g_opacitys|g_settings.opacitys|
|g_opacityNum|g_settings.opacityNum|
|g_scoreDetails|g_settings.scoreDetails|
|g_scoreDetailNum|g_settings.scoreDetailNum|

#### 前/次の設定に進めるボタンの表示位置

|旧変数|新変数|
|----|----|
|C_LBL_SETMINIL|g_settingBtnObj.chara.L|
|C_LBL_SETMINILL|g_settingBtnObj.chara.LL|
|C_LBL_SETMINIR|g_settingBtnObj.chara.R|
|C_LBL_SETMINIRR|g_settingBtnObj.chara.RR|
|C_LBL_SETMINIL_LEFT|g_settingBtnObj.pos.L|
|C_LBL_SETMINILL_LEFT|g_settingBtnObj.pos.LL|
|C_LBL_SETMINIR_LEFT|g_settingBtnObj.pos.R|
|C_LBL_SETMINIRR_LEFT|g_settingBtnObj.pos.RR|

</details>

#### 🛠 修正事項
- 該当する変数群をカスタムしている場合は、新変数に変更してください。

### 2. danoni_legacy_function.js へ移動した関数について
- 現行ソースで未使用の以下の関数を「danoni_legacy_function.js」へ移動しました。  
対象：createLabel  

#### 🛠 修正事項
- 今後も利用する場合は、lib/danoni_legacy_function.js をサーバーへアップロードしてください。

### 3. g_resultMsgObj の廃止 (ver19.5.1以降)
- FULL COMBOやPerfectの表示部分のhtml部分を管理する`g_resultMsgObj`が廃止になっています。

#### 🛠 修正事項
- 利用している場合は、代替関数 `resultViewText` の利用を検討してください。

### 4. Display画面からSettings画面へ戻るボタンの位置変更
- Settings画面からDisplay画面へ移動するボタンと統一しました。  
ID名は従来のままのため、個別の設定は可能です。

#### 🛠 修正事項
- 当該ボタンをカスタムしている場合、念のため見た目に影響が無いか確認してください。

### 5. ステップゾーン位置を表す旧変数の削除
- 従来、ステップゾーン位置を表していた `g_stepY`や`g_distY`などについて、  
正式に g_posObj へ移行しました。

|旧変数|新変数|
|----|----|
|g_stepY|g_posObj.stepY|
|g_stepYR|g_posObj.stepYR|
|g_distY|g_posObj.distY|
|g_reverseStepY|g_posObj.reverseStepY|

#### 🛠 修正事項
- v20以降、旧変数は使えなくなります。  
カスタム側で使用している場合は変数を置き換えてください。

## ⭐ v18 -> v19
ReleasesのFiles Changedの項目を参照してください。 

### 1. danoni_legacy_function.js へ移動した関数について
- 現行ソースで未使用の以下の関数を「danoni_legacy_function.js」へ移動しました。  
対象：createDivCssLabel, createCssButton, createColorObject  

#### 🛠 修正事項
- 今後も利用する場合は、lib/danoni_legacy_function.js をサーバーへアップロードしてください。

### 2. gaugeXの初期値補完について
- gaugeX (gaugeEasy, gaugeHardなど)について、2譜面目以降の設定を省略した場合、  
1譜面目のgaugeXの設定が引き継がれるようになりました。

#### 🛠 修正事項
- 基本的に影響は無いはずですが、gaugeXを中途半端に設定していた場合、  
設定の引継ぎにより設定値が変わる可能性があります。  
必要に応じて、gaugeXの設定値を見直してください。  

### 3. 初期表示時のclearWindow関数内包
- titleInit / optionInit / settingsDisplayInit / loadMusic / MainInit / resultInit の処理の先頭に  
clearWindow() 処理を追加しています。

#### 🛠 修正事項
- 上記関数呼び出し時のclearWindow関数の呼び出しが不要になります。  
基本的に影響は無いと思いますが、カスタムで処理をさせている場合はご注意ください。  
- clearWindow関数を上記関数前に指定している場合、2回呼び出されることになりますが影響はありません。

## ⭐ v17 -> v18
ReleasesのFiles Changedの項目を参照してください。  

### 1. 矢印・フリーズアローの個別属性の変更について
- 直接使用する機会は無いかもしれませんが、矢印・フリーズアローの個別属性の仕様が変わりました。
カスタムjsで使用している場合、修正が必要です。

#### 🛠 修正事項
- [主要オブジェクトのプロパティ](./objectMainRef)に記載の変数を使用している場合、修正が必要です。

変更前
```javascript
const boostSpd = parseFloat(document.querySelector(`#arrow${_j}_${_k}`).getAttribute(`boostSpd`));
```

変更後
```javascript
const boostSpd = g_attrObj[`arrow${_j}_${_k}`].boostSpd;
```

### 2. タイトルアニメーションの仕様変更 (ver18.5.0以降)  
- タイトル文字のアニメーションを譜面ヘッダー：[titleanimation](./dos-h0077-titleanimation) にて設定できるようになりました。

#### 🛠 修正事項
- 今後、`#lblmusicTitle` にてCSSを直接変更した場合、両方の設定が反映されます。ご注意ください。
- 従来の設定を維持したい場合は、`danoni_main.css`を**変更せず**、
`danoni_setting.js`にてg_presetCustomDesignUse の titleAnimation を `true`に変更してください。

#### 従来の指定方法
```css
#lblmusicTitle {
    animation-name: upToDown;
    animation-duration: 2.0s;
}
```

#### 今後の指定方法
```
|titleanimation=upToDown,120,0|
```

### 3. Gitterボタン、操作方法リンクの追加 (ver18.8.0以降)
- Gitter（結果画面）、操作方法（タイトル画面）へリンクするボタンが追加されました。  
ID名はそれぞれ「btnGitter」「btnHelp」です。  

#### 🛠 修正事項
- 結果画面、タイトル画面のデザインを変更している場合は修正が必要となります。

## ⭐ v16 -> v17
ReleasesのFiles Changedの項目を参照してください。  

### 1. strictモードの再有効化 (ver17.4.0より)
- v2～v17.3まで、strictモードが有効になっていませんでした。  
ver17.4.0より再度有効化しています。  
カスタムjsにて予約語を利用していたり、変数を初期化していない場合動作しないことがあります。

#### 🛠 修正事項
- カスタムjsにて変数を初期化していること、予約語を使用していないことを確認してください。  
以下は予約語のため、変数に使用できません。
  - implements、interface、let、package、private、protected、public、static、yield
- 参考：https://developer.mozilla.org/ja/docs/Web/JavaScript/Reference/Strict_mode

### 2. ラベル・ボタン・色付きオブジェクト作成用関数の変更
- それぞれ次の関数に移行しています。

|変更前関数|変更後関数|概要|
|----|----|----|
|createDivCssLabel|createDivCss2Label|ラベル作成用関数|
|createCssButton|createCss2Button|ボタン作成用関数|
|createColorObject|createColorObject2|色付きオブジェクト作成用関数|

#### 🛠 修正事項
- 変更前関数は残しているのでそのままでも問題はありませんが、  
今後`danoni_legacy_function.js`へ移動となる可能性があります。  
customjsで指定している場合はご注意ください。

## ⭐ v15 -> v16
ReleasesのFiles Changedの項目を参照してください。  

### 1. CopyResultボタンの追加 (ver16.1.0より)
- リザルトデータをクリップボードへコピーするボタンが追加されました。  
ID名は「btnCopy」です。  

#### 🛠 修正事項
- 結果画面デザインを変更している場合は修正が必要となります。

## ⭐ v14 -> v15
ReleasesのFiles Changedの項目を参照してください。

### 1. 設定画面内の設定別スプライト変数名変更
- 設定別のスプライト変数名を変更しています。
なお、ID名は変更していません。

|変数変更前|変数変更後|
|----|----|
|difficultySprite|spriteList.difficulty|
|speedSprite|spriteList.speed|
|motionSprite|spriteList.motion|
|shuffleSprite|spriteList.shuffle|
|scrollSprite|spriteList.scroll|
|reverseSprite|spriteList.reverse|
|autoPlaySprite|spriteList.autoPlay|
|gaugeSprite|spriteList.gauge|
|adjustmentSprite|spriteList.adjustment|
|fadeinSprite|spriteList.fadein|
|volumeSprite|spriteList.volume|
|appearanceSprite|spriteList.appearance|
|opacitySprite|spriteList.opacity|

#### 🛠 修正事項
- 設定別スプライト変数名をそのまま使用している場合は、変更が必要です。  
ID名参照（例：`querySelector('#difficultySprite')`など）の場合は変更不要です。

## ⭐️ v13 -> v14
ReleasesのFiles Changedの項目を参照してください。

### 1. スキンファイル(css)の更新
- Opacityオプションの追加、Displayオプションの無効化ボタン背景色追加による変更です。 

#### 🛠 修正事項（必須）
- デフォルトスキンを利用している方は、Files Changedの項目を参考に入れ替えてください。
- 独自スキンを利用している方は、以下の項目を足してください。
```css
/* opacityオプション用：ver13.5.1以降。ver13.4.0以前からの変更のみ追加 */
.settings_Opacity::first-letter {
	color:#999999;
}

/* ステップゾーン用の影矢印色：ver14.1.0以降 */
.main_objStepShadow {
	background-color: #000000;
}

/* ボタン：ON/OFF 無効化ボタン用 */
.button_DisabledOFF {
	color: #999999;
	background-color: #333333;
}
.button_DisabledON {
	color: #ffffff;
	background-color: #009999;
}
```

## ⭐️ v12 -> v13
ReleasesのFiles Changedの項目を参照してください。

### 1. スキンファイル(css)の更新
- Opacityオプションの追加、Displayオプションの無効化ボタン背景色追加による変更です。 

#### 🛠 修正事項（必須）
- デフォルトスキンを利用している方は、Files Changedの項目を参考に入れ替えてください。
- 独自スキンを利用している方は、以下の項目を足してください。
```css
/* opacityオプション用：ver13.5.1以降 */
.settings_Opacity::first-letter {
	color:#999999;
}
```

## ⭐️ v11 -> v12
### 1. danoni_contents.jsの更新
- 譜面詳細画面の追加に伴う変更です。  

#### 🛠 修正事項（必須）
- danoni_contents.js を入れ替えてください。（./js/lib にあります）

## ⭐️ v10 -> v11
### 1. danoni_contents.jsの更新
- キーコンフィグの見直しを行っています。

#### 🛠 修正事項（必須）
- danoni_contents.js を入れ替えてください。（./js/lib にあります）

### 2. danoni_skin_default(light, skyblue).css の更新（v10.1.1以前からの更新のみ）
- v10.2.1より、Reverse / Scroll の色設定を追加しています。  

#### 🛠 修正事項
- v10.1.1以前からの更新の場合、skinフォルダにある以下のファイルを入れ替えてください。
  - danoni_skin_default.css
  - danoni_skin_light.css
  - danoni_skin_skyblue.css

### 3. word/back/mask_dataのCross/Split対応（v10.1.1以前からの更新のみ）
- v10.2.1より、Cross/Split等のスクロール拡張オプションが追加されています。  
スクロール方向により、演出系に影響がある場合は独自に対応するか、  
Cross/Split等の実行時は演出OFFにするといった対応が必要です。

#### 🛠 修正事項
- Cross/Split等のword/back/mask_dataとして、  
`wordAlt_data`, `backAlt_data`, `maskAlt_data`が使用できます。  
取り急ぎCross/Split等の実行時は演出OFFにする場合、  
下記を譜面に追加することで対応可能です。  
```
|wordAlt_data=|backAlt_data=|maskAlt_data=|
```

## ⭐️ v9 -> v10
### 1. スキンファイル追加・画像ファイル名変更
- [更新情報(UpdateInfo-v10)](./UpdateInfo-v10)にも追記していますが、  
スキン実装によりファイルが追加・変更されています。

#### 🛠 修正事項 (必須)
- cssフォルダ、imgフォルダを v10 のものに入れ替えてください。  
また、skinフォルダをcss, imgフォルダと同じ階層に置いてください。  
※danoni_custom.js, danoni_setting.jsの入れ替えは不要です。

### 2. 設定画面のoptionsprite位置変更
- 設定画面の各項目を覆うoptionspriteについて、背景色を塗ったときに欠けが起こらないよう、位置を調整しました。

#### 🛠 修正事項
- optionsprite上に別オブジェクトを配置している場合、X方向に25px, Y方向に20pxを足してください。

### 3. ステップゾーンのグループ化
- ステップゾーン関係のオブジェクトが`stepRootX`で一律管理されるようになりました。

#### 🛠 修正事項
1. 座標変更している作品は`stepX` -> `stepRootX` へ変更が必要です。
```javascript
    document.querySelector(`#step0`).style.left = `50px`;  // 変更前
    document.querySelector(`#stepRoot0`).style.left = `50px`;  // 変更後
```

2. またステップゾーンに回転を加えている場合、
指定するオブジェクトにより指定する回転数が異なります。
（既存作品については、手間でも構成オブジェクト個々に回転数を指定した方が無難です。）
    - `stepRootX` (ステップゾーンルート)：
生成時点からの**相対値** (最初が60度、移動後が90度なら指定する値は30度)
    - `stepX`, `stepDivX`, `stepHitX` (ステップゾーン構成オブジェクト)：
指定した**絶対値**そのまま (移動後が90度なら、指定する値は最初によらず90度固定)
```javascript
    document.querySelector(`#step0`).style.transform = `rotate(180deg)`; 
    document.querySelector(`#stepHit0`).style.transform = `rotate(180deg)`; 
    document.querySelector(`#stepDiv0`).style.transform = `rotate(180deg)`;   // 新たに追加
```

## ⭐️ v8 -> v9
### 1. 矢印・フリーズアローのオブジェクトルート変更
- 以下のように変更しました。arrowSprite0/1は、mainSpriteの子要素です。

|オブジェクト|v8まで|v9以降|
|----|----|----|
|ステップゾーン|mainSprite|mainSprite|
|矢印・フリーズアロー|mainSprite|arrowSprite0 (スクロール方向が上向き)<br>arrowSprite1 (スクロール方向が下向き)|

#### 🛠 修正事項
- 通常は変更不要です。  
矢印・フリーズアローを作成・削除する処理を外部から行っている場合、変更が必要です。  

### 2. danoni_main.cssの更新（v9.4.3にて更新あり）
- Appearance用のクリッピングマスクとして、CSSクラスを追加しています。
- 設定画面のGaugeオプションの詳細表示で、新たにCSSクラスを追加しています。

#### 🛠 修正事項（必須）
- danoni_main.css を入れ替えてください。

### 3. danoni_setting.jsの更新（v9.4.2にて更新あり）
- カスタムゲージ実装によりデフォルト設定以外のゲージの再設定が必要なため、一部追加が必要です。

#### 🛠 修正事項（カスタムゲージ利用時必須）
- danoni_setting.js のg_presetGaugeCustom を、以下のように変更してください。  
SuddenDeath以外の各数値は必要に応じて変更して問題ありません。

```javascript
// ゲージ設定（デフォルト以外）
const g_presetGaugeCustom = {
	Easy: {
		Border: 70,
		Recovery: 4,
		Damage: 7,
		Init: 25,
	},
	Hard: {
		Border: `x`,
		Recovery: 1,
		Damage: 50,
		Init: 100,
	},
	NoRecovery: {
		Border: `x`,
		Recovery: 0,
		Damage: 50,
		Init: 100,
	},
	SuddenDeath: {
		Border: `x`,
		Recovery: 0,
		Damage: setVal(g_rootObj.maxLifeVal, C_VAL_MAXLIFE, C_TYP_FLOAT),
		Init: 100,
	},
	Practice: {
		Border: `x`,
		Recovery: 0,
		Damage: 0,
		Init: 50,
	}
};
```

## ⭐️ v7 -> v8
### 1. リザルトモーションの音楽同期化
- リザルトモーション（backresult_data 他）について、  
音楽に同期したフレーム制御になりました。（メイン画面と同様）

#### 🛠 修正事項
- 通常は変更不要です。  
リザルトモーションと音楽を同期させる演出を行っている場合、フレーム数の修正が必要になる場合があります。  

### 2. 速度変化 (speed_data/change) の統一
- 途中変速で使用する速度変化 (speed_data, speed_change) がキーの種類に関係なくどちらでも使えるようになりました。  
どちらも指定した場合は、speed_change側が優先されます。

|B/A|概要|
|----|----|
|変更前|5key: speed_data / 5key以外: speed_change|
|変更後|speed_data もしくは speed_change (キーの種類によらず)|

#### 🛠 修正事項
- 通常は変更不要です。  
同一譜面でどちらも指定していた場合のみ、どちらかに合わせることを推奨します。  

## ⭐️ v6 -> v7
### 1. 個別色変化(フリーズアローヒット時)のタイミング変更
- [個別色変化(color_data)](./dos-e0002-colorData)においてフリーズアローヒット時の色のタイミングが変わりました。  

|B/A|概要|
|----|----|
|変更前|対象のフリーズアローが出現したタイミングで色変更<br>※逆算されるため、実際に使用するタイミングより値を大きくする必要がある|
|変更後|対象のフリーズアローがヒット時に変わる(ステップゾーン到達)タイミングで色変更<br>※実際に使用するタイミングと同一|

#### 🛠 修正事項
- フリーズアローヒット時の色変更を`color_data`で行っていた場合、  
値の見直しを行うか、譜面ヘッダーで以下を指定してください。
```
|colorDataType=v6|
```
### 2. 楽曲の終了判定タイミングの変更（秒単位からフレーム単位へ）
- 楽曲の終了判定タイミングがフレーム単位に変わりました。

#### 🛠 修正事項
- ほとんど問題はありませんが、譜面ヘッダー：endFrameなどで終了タイミングを指定していない場合、
楽曲の終了位置が若干変わる可能性があります。  
必要に応じて、譜面ヘッダー：endFrameを指定して終了位置を調整してください。

### 3. danoni_main.cssの更新
- 誤爆抑止用のボタンアニメの実装のために、CSSモーションを追加しています。

#### 🛠 修正事項（必須）
- danoni_main.css を入れ替えてください。

## ⭐️ v5 -> v6
### 1. ライフゲージ（初期）の値変更
- ライフ上限値を可変にしたため、ライフゲージ（初期）の値(lifeInit)の取り方が変わりました。

|B/A|概要|
|----|----|
|変更前|ライフ上限：1000に対して実際の初期ライフ値を設定（デフォルト：250）|
|変更後|上限値に関係なく、全体の何％を初期ライフ値にするかを設定（デフォルト：25）|

#### 🛠 修正事項
- 通常はありませんが、カスタム側で強制的に`lifeInit`を変更する処理を入れていた場合、  
値の見直し（数値を10で割る）が必要です。

## ⭐️ v4 -> v5
### 1. 変数名の変更
- コード集約により、変数名の一部が変わりました。

|変更前|変更後|
|----|----|
|g_stateObj.auto|g_stateObj.autoPlay|
|g_stateObj.lifeId|g_gaugeNum|
|g_stateObj.lifeSetName|g_stateObj.gauge|

#### 🛠 修正事項
- カスタム側でこれらの変数（変更前）を使用していた場合、  
変更後の変数へ置き換えが必要です。

### 2. danoni_main.cssの更新
- 歌詞表示のフェードイン・アウト用のCSSモーションを追加しています。

#### 🛠 修正事項（必須）
- danoni_main.css を入れ替えてください。

## ⭐️ v3 -> v4
- 特にありません。
- v4.x以降、[ローカルストレージ保存](./LocalStorage)機能が有効となり、  
ハイスコア等のデータが保存されるようになります。

## ⭐️ v2 -> v3
### 1. 設定ファイル (danoni_setting.js)の追加
- 作品群全体の設定ファイル(danoni_setting.js)が追加されました。

#### 🛠 修正事項（必須）
- jsフォルダ配下に、`danoni_setting.js`を追加する必要があります。

### 2. danoni_main.cssの更新
- タイトル文字用のCSSモーションを追加しています。

#### 🛠 修正事項（必須）
- danoni_main.css を入れ替えてください。

### 3. 歌詞表示(word_data)のセット毎改行区切りに対応
- 1セット1行としている場合に限り、行末のカンマが不要になりました。
※色変化についてもセット毎改行区切りに対応しました。
```
|word_data=
300,0,[fadein]
350,0,歌詞テスト
400,0,[fadeout]
|
```
#### 🛠 修正事項
- 特になし。  
ただし今後、改行を使わない歌詞表示(word_data)については非推奨となる可能性があります。

### 4. 設定画面のレイアウト変更
- Shuffle機能追加に伴い、画面レイアウトが変わりました。

#### 🛠 修正事項
- 設定画面の上からカスタムでデザインを変えている場合、位置調整が必要です。  

## ⭐️ v1 -> v2
### 1. danoni_custom.jsの見直し
- 従来のcustomファイルより、共通部分がdanoni_main.jsへ移動しました。

#### 🛠 修正事項（必須）
- [ver2.1.0への移行について](./for_ver2.1.0)をご覧ください。

| [< 本体のバージョンアップ](./HowToUpdate) | **アップグレードガイド** | [更新情報概要 >](./UpdateInfo) |
