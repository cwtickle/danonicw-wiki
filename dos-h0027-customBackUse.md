**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-h0027-customBackUse) | Japanese** 

[Home](./) &gt; [譜面ヘッダー仕様](dos_header) &gt; [デフォルトデザインの利用有無
](dos_header#-デフォルトデザインの利用有無)

| [<- customTitleAnimationUse](dos-h0078-customTitleAnimationUse) | **customBackUse** | [customBackMainUse ->](dos-h0028-customBackMainUse) |

## customBackUse
- デフォルト背景(プレイ画面以外)の利用有無設定
- 共通設定 ⇒ [g_presetObj.customDesignUse](dos-s0005-defaultDesign#デフォルトデザインの使用有無設定-g_presetobjcustomdesignuse)

### 使い方
```
|customBackUse=false|
```
### 説明
タイトル画面の背景を個別に設定するか、デフォルトのものを使用するかを指定します。  
v10以降はskin側で設定できるため、Canvas未使用時は無効となっています。

|値|既定|内容|
|----|----|----|
|false|*|デフォルトの形式を採用|
|true||個別設定|

### 関連項目
- [**customTitleUse**](dos-h0025-customTitleUse) [:pencil:](dos-h0025-customTitleUse/_edit) タイトルの曲名文字
- [customBackMainUse](dos-h0028-customBackMainUse) [:pencil:](dos-h0028-customBackMainUse/_edit) 背景(プレイ画面)
- [背景・マスクモーション (back_data, mask_data)](dos-e0004-animationData) [:pencil:](dos-e0004-animationData/_edit) 

### 更新履歴

|Version|変更内容|
|----|----|
|[v3.6.0](https://github.com/cwtickle/danoniplus/releases/tag/v3.6.0)|・デフォルト値変更 (true -> false)|
|[v2.1.0](https://github.com/cwtickle/danoniplus/releases/tag/v2.1.0)|・初回実装|

| [<- customTitleAnimationUse](dos-h0078-customTitleAnimationUse) | **customBackUse** | [customBackMainUse ->](dos-h0028-customBackMainUse) |