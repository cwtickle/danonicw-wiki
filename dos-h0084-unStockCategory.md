**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-h0084-unStockCategory) | Japanese** 

[Home](./) &gt; [譜面ヘッダー仕様](dos_header) &gt; [プレイ時間制御・譜面位置調整](dos_header#-プレイ時間制御譜面位置調整)

| [<- playbackRate](dos-h0010-playbackRate) | **unStockCategory** | [stockForceDel ->](dos-h0085-stockForceDel) |

## unStockCategory
- フェードイン前のデータを保持しない種別のリスト設定
- 共通設定 ⇒ [g_presetObj.unStockCategories](dos-s0007-viewControl#フェードイン前のデータを保持しない種別の設定-g_presetobjunstockcategories)

### 使い方
```
|unStockCategory=back,mask|
```
### 説明
ver25.0.0より、フェードイン時にフェードイン前のデータを原則保持し、  
プレイ開始時にそのデータを同時に表示する形式に変わりました。  
ただ、作品によっては不都合が出ることが想定されるため、この設定を使って、従来通り保持しない設定をまとめて行うことができます。

指定可能な値は、「word」(歌詞表示)、「back」（背景表示・モーション）、「mask」（マスク表示・モーション）の3種類です。  
カンマ区切りで複数指定が可能です。

共通設定ファイルとの重ね掛けが可能です。

### 関連項目
- [word/back/maskStockForceDel](dos-h0085-stockForceDel) [:pencil:](dos-h0085-stockForceDel/_edit) フェードイン前のデータを保持しないパターンの設定
- [共通設定ファイル仕様](dos_setting) &gt; [プレイ画面制御](dos-s0007-viewControl)

### 更新履歴

|Version|変更内容|
|----|----|
|[v25.0.0](https://github.com/cwtickle/danoniplus/releases/tag/v25.0.0)|・初回実装|

| [<- playbackRate](dos-h0010-playbackRate) | **unStockCategory** | [stockForceDel ->](dos-h0085-stockForceDel) |