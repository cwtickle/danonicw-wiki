⭐ New Features / 🛠️ Improvements / 🐞 Bug Fixes  
( Files changed: ⚪ danoni_main.js / 🔵 danoni_constants.js / 🔴 css files )

[**<- v15**](Changelog-v15) | **v14** | [**v13 ->**](Changelog-v13)  
(**🔖 [26 Releases](https://github.com/cwtickle/danoniplus/releases?q=tag%3Av14)** )

## [Warning] 更新終了後の不具合情報
- [更新を終了したバージョンの不具合情報#v14](DeprecatedVersionBugs#v14) を参照

## v14.5.21 ([2021-05-16](https://github.com/cwtickle/danoniplus/releases/tag/v14.5.21))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v14.5.21/total)](https://github.com/cwtickle/danoniplus/archive/v14.5.21.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v14.5.21/support/v14?style=social)](https://github.com/cwtickle/danoniplus/compare/v14.5.21...support/v14#files_bucket) 
- 🐞 17key(パターン2)のシャッフルグループ間違いを修正 ( PR [#1084](https://github.com/cwtickle/danoniplus/pull/1084) ) <- :boom: [**v3.1.0**](Changelog-v3#v310-2019-02-26)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v14.5.21)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v14.5.20...v14.5.21#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v14.5.21)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v14.5.21.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v14.5.21.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v14.5.21/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v14.5.21/js/lib/danoni_constants.js)
/ 🎣 [**v22.4.1**](./Changelog-v22#v2241-2021-05-16)

## v14.5.20 ([2021-04-07](https://github.com/cwtickle/danoniplus/releases/tag/v14.5.20))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v14.5.20/total)](https://github.com/cwtickle/danoniplus/archive/v14.5.20.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v14.5.20/support/v14?style=social)](https://github.com/cwtickle/danoniplus/compare/v14.5.20...support/v14#files_bucket) 
- 🐞 空の矢印データを含む譜面にS-Randomをかけるとフルコン演出が早まる問題を修正
 ( PR [#1044](https://github.com/cwtickle/danoniplus/pull/1044), Gitter [2021-04-06](https://gitter.im/danonicw/community?at=606c808592a3431fd67b1640) ) <- :boom: [**v3.1.0**](Changelog-v3#v310-2019-02-26)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v14.5.20)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v14.5.19...v14.5.20#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v14.5.20)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v14.5.20.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v14.5.20.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v14.5.20/js/danoni_main.js)
/ 🎣 [**v21.4.2**](./Changelog-v21#v2142-2021-04-07)

## v14.5.19 ([2021-03-06](https://github.com/cwtickle/danoniplus/releases/tag/v14.5.19))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v14.5.19/total)](https://github.com/cwtickle/danoniplus/archive/v14.5.19.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v14.5.19/support/v14?style=social)](https://github.com/cwtickle/danoniplus/compare/v14.5.19...support/v14#files_bucket) 
- 🛠️ Base64エンコードした曲データを直接デコードするよう変更 ( PR [#1009](https://github.com/cwtickle/danoniplus/pull/1009) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v14.5.19)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v14.5.18...v14.5.19#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v14.5.19)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v14.5.19.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v14.5.19.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v14.5.19/js/danoni_main.js)
/ 🎣 [**v20.5.1**](./Changelog-v20#v2051-2021-03-06)

## v14.5.18 ([2021-02-20](https://github.com/cwtickle/danoniplus/releases/tag/v14.5.18))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v14.5.18/total)](https://github.com/cwtickle/danoniplus/archive/v14.5.18.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v14.5.18/support/v14?style=social)](https://github.com/cwtickle/danoniplus/compare/v14.5.18...support/v14#files_bucket) 
- 🛠️ imgタグ生成時においてcrossOrigin属性を付与するよう変更 ( PR [#988](https://github.com/cwtickle/danoniplus/pull/988) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v14.5.18)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v14.5.17...v14.5.18#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v14.5.18)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v14.5.18.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v14.5.18.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v14.5.18/js/danoni_main.js)
/ 🎣 [**v20.2.1**](./Changelog-v20#v2021-2021-02-20)

## v14.5.17 ([2021-01-05](https://github.com/cwtickle/danoniplus/releases/tag/v14.5.17))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v14.5.17/total)](https://github.com/cwtickle/danoniplus/archive/v14.5.17.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v14.5.17/support/v14?style=social)](https://github.com/cwtickle/danoniplus/compare/v14.5.17...support/v14#files_bucket) 
- 🐞 同一レーン上に通常矢印とフリーズアローが短い間隔で存在するとき、通常矢印の判定が優先されてしまう事象を修正 ( PR [#929](https://github.com/cwtickle/danoniplus/pull/929), [#930](https://github.com/cwtickle/danoniplus/pull/930), [#932](https://github.com/cwtickle/danoniplus/pull/932) ) <- :boom: **initial**

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v14.5.17)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v14.5.16...v14.5.17#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v14.5.17)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v14.5.17.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v14.5.17.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v14.5.17/js/danoni_main.js)
/ 🎣 [**v18.9.2**](./Changelog-v18#v1892-2021-01-05), [**v18.9.1**](./Changelog-v18#v1891-2021-01-03)

## v14.5.16 ([2020-12-25](https://github.com/cwtickle/danoniplus/releases/tag/v14.5.16))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v14.5.16/total)](https://github.com/cwtickle/danoniplus/archive/v14.5.16.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v14.5.16/support/v14?style=social)](https://github.com/cwtickle/danoniplus/compare/v14.5.16...support/v14#files_bucket) 
- 🐞 カスタムjs, スキンjsを二重読込している問題を修正 ( PR [#917](https://github.com/cwtickle/danoniplus/pull/917) ) <- :boom: [**v7.7.0**](Changelog-v7#v770-2019-08-25)
- 🐞 楽曲・画像読込チェックの待ち時間誤りを修正 ( PR [#915](https://github.com/cwtickle/danoniplus/pull/915) ) <- :boom: [**v10.1.1**](Changelog-v10#v1011-2019-11-11)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v14.5.16)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v14.5.15...v14.5.16#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v14.5.16)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v14.5.16.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v14.5.16.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v14.5.16/js/danoni_main.js)
/ 🎣 [**v18.7.0**](./Changelog-v18#v1870-2020-12-25)

## v14.5.15 ([2020-12-21](https://github.com/cwtickle/danoniplus/releases/tag/v14.5.15))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v14.5.15/total)](https://github.com/cwtickle/danoniplus/archive/v14.5.15.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v14.5.15/support/v14?style=social)](https://github.com/cwtickle/danoniplus/compare/v14.5.15...support/v14#files_bucket) 
- 🐞 曲終了直後にリトライキーを押すとALL0のリザルトが表示される問題を修正 ( PR [#908](https://github.com/cwtickle/danoniplus/pull/908) ) <- :boom: [**v0.67.x**](Changelog-v0#v067x-2018-11-17) 

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v14.5.15)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v14.5.14...v14.5.15#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v14.5.15)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v14.5.15.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v14.5.15.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v14.5.15/js/danoni_main.js)
/ 🎣 [**v18.6.0**](./Changelog-v18#v1860-2020-12-21)

## v14.5.14 ([2020-11-23](https://github.com/cwtickle/danoniplus/releases/tag/v14.5.14))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v14.5.14/total)](https://github.com/cwtickle/danoniplus/archive/v14.5.14.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v14.5.14/support/v14?style=social)](https://github.com/cwtickle/danoniplus/compare/v14.5.14...support/v14#files_bucket) 
- 🐞 Display:Colorのボタンが無効化されていた場合にColorTypeの挙動により、Display:Colorの切り替えができてしまう問題を修正 ( PR [#892](https://github.com/cwtickle/danoniplus/pull/892) ) <- :boom: [**v14.0.2**](Changelog-v14#v1402-2020-04-29) 

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v14.5.14)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v14.5.13...v14.5.14#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v14.5.14)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v14.5.14.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v14.5.14.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v14.5.14/js/danoni_main.js)
/ 🎣 [**v18.3.0**](./Changelog-v18#v1830-2020-11-18)

## v14.5.13 ([2020-11-02](https://github.com/cwtickle/danoniplus/releases/tag/v14.5.13))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v14.5.13/total)](https://github.com/cwtickle/danoniplus/archive/v14.5.13.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v14.5.13/support/v14?style=social)](https://github.com/cwtickle/danoniplus/compare/v14.5.13...support/v14#files_bucket) 
- 🐞 速度データ(speed_change)が空の場合、データがあると判断されてしまいspeed_changeが優先されてしまう問題を修正 ( PR [#884](https://github.com/cwtickle/danoniplus/pull/884) ) <- :boom: [**v8.0.0**](Changelog-v8#v800-2019-09-08) 

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v14.5.13)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v14.5.12...v14.5.13#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v14.5.13)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v14.5.13.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v14.5.13.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v14.5.13/js/danoni_main.js)
/ 🎣 [**v18.1.0**](./Changelog-v18#v1810-2020-11-02)

## v14.5.12 ([2020-10-16](https://github.com/cwtickle/danoniplus/releases/tag/v14.5.12))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v14.5.12/total)](https://github.com/cwtickle/danoniplus/archive/v14.5.12.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v14.5.12/support/v14?style=social)](https://github.com/cwtickle/danoniplus/compare/v14.5.12...support/v14#files_bucket) 
- 🐞 矢印塗りつぶし有、ステップゾーンをOFFにした場合に矢印塗りつぶし部分が残る問題を修正 ( PR [#866](https://github.com/cwtickle/danoniplus/pull/866) ) <- :boom: [**v5.12.0**](Changelog-v5#v5120-2019-06-14)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v14.5.12)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v14.5.11...v14.5.12#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v14.5.12)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v14.5.12.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v14.5.12.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v14.5.12/js/danoni_main.js)
/ 🎣 [**v17.4.3**](./Changelog-v17#v1743-2020-10-12)

## v14.5.11 ([2020-10-09](https://github.com/cwtickle/danoniplus/releases/tag/v14.5.11))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v14.5.11/total)](https://github.com/cwtickle/danoniplus/archive/v14.5.11.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v14.5.11/support/v14?style=social)](https://github.com/cwtickle/danoniplus/compare/v14.5.11...support/v14#files_bucket) 
- 🛠️ danoni_main.jsに対してstrictモードを解除
- 🐞 tuningのみ指定があった場合に制作者リンクが出ない問題を修正 ( PR [#850](https://github.com/cwtickle/danoniplus/pull/850) ) <- :boom: [**v3.0.0**](Changelog-v3#v300-2019-02-25)
- 🐞 ツール値出力時、時間表示がおかしくなることがある問題を修正 ( PR [#858](https://github.com/cwtickle/danoniplus/pull/858) ) <- :boom: [**v12.0.0**](Changelog-v12#v1200-2020-02-09) <- [**v2.0.0**](Changelog-v2#v200-2019-01-18)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v14.5.11)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v14.5.9...v14.5.11#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v14.5.11)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v14.5.11.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v14.5.11.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v14.5.11/js/danoni_main.js)
/ 🎣 [**v17.4.0**](./Changelog-v17#v1740-2020-10-09)

## v14.5.9 ([2020-08-24](https://github.com/cwtickle/danoniplus/releases/tag/v14.5.9))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v14.5.9/total)](https://github.com/cwtickle/danoniplus/archive/v14.5.9.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v14.5.9/support/v14?style=social)](https://github.com/cwtickle/danoniplus/compare/v14.5.9...support/v14#files_bucket) 
- 🐞 リトライ時に稀に早回しになる不具合を修正、キーリピートを無効化 ( PR [#810](https://github.com/cwtickle/danoniplus/pull/810) ) <- :boom: [**v4.0.0**](Changelog-v4#v400-2019-04-25)
- 🐞 キー変換処理が抜けていた部分を修正 ( PR [#811](https://github.com/cwtickle/danoniplus/pull/811) ) <- :boom: [**v11.2.0**](Changelog-v11#v1120-2020-01-04)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v14.5.9)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v14.5.8...v14.5.9#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v14.5.9)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v14.5.9.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v14.5.9.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v14.5.9/js/danoni_main.js)
/ 🎣 [**v16.2.1**](./Changelog-v16#v1621-2020-08-25)

## v14.5.8 ([2020-08-22](https://github.com/cwtickle/danoniplus/releases/tag/v14.5.8))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v14.5.8/total)](https://github.com/cwtickle/danoniplus/archive/v14.5.8.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v14.5.8/support/v14?style=social)](https://github.com/cwtickle/danoniplus/compare/v14.5.8...support/v14#files_bucket) 
- 🛠️ Applicationキーがキーコンフィグ画面、メイン画面で反応しないよう変更 ( PR [#806](https://github.com/cwtickle/danoniplus/pull/806) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v14.5.8)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v14.5.7...v14.5.8#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v14.5.8)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v14.5.8.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v14.5.8.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v14.5.8/js/danoni_main.js)
/ 🎣 [**v16.1.0**](./Changelog-v16#v1610-2020-08-22)

## v14.5.7 ([2020-07-02](https://github.com/cwtickle/danoniplus/releases/tag/v14.5.7))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v14.5.7/total)](https://github.com/cwtickle/danoniplus/archive/v14.5.7.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v14.5.7/support/v14?style=social)](https://github.com/cwtickle/danoniplus/compare/v14.5.7...support/v14#files_bucket) 
- 🛠️ バージョン比較用のリンクをSecurity Policyに変更 ( Issue [#759](https://github.com/cwtickle/danoniplus/pull/759), PR [#760](https://github.com/cwtickle/danoniplus/pull/760) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v14.5.7)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v14.5.6...v14.5.7#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v14.5.7)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v14.5.7.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v14.5.7.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v14.5.7/js/danoni_main.js)
/ 🎣 [**v15.5.0**](./Changelog-v15#v1530-2020-07-02)

## v14.5.6 ([2020-06-27](https://github.com/cwtickle/danoniplus/releases/tag/v14.5.6))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v14.5.6/total)](https://github.com/cwtickle/danoniplus/archive/v14.5.6.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v14.5.6/support/v14?style=social)](https://github.com/cwtickle/danoniplus/compare/v14.5.6...support/v14#files_bucket) 
- 🐞 カスタムゲージ使用時にクリア失敗時のリザルトモーションが使用できない問題を修正 ( Issue [#749](https://github.com/cwtickle/danoniplus/pull/749), PR [#751](https://github.com/cwtickle/danoniplus/pull/751) ) <- :boom: [**v9.4.0**](Changelog-v9#v940-2019-10-20)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v14.5.6)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v14.5.5...v14.5.6#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v14.5.6)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v14.5.6.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v14.5.6.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v14.5.6/js/danoni_main.js)
/ 🎣 [**v15.3.0**](./Changelog-v15#v1530-2020-06-27)

## v14.5.5 ([2020-06-21](https://github.com/cwtickle/danoniplus/releases/tag/v14.5.5))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v14.5.5/total)](https://github.com/cwtickle/danoniplus/archive/v14.5.5.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v14.5.5/support/v14?style=social)](https://github.com/cwtickle/danoniplus/compare/v14.5.5...support/v14#files_bucket) 
- 🛠️ プレイ中に他のキーイベントが呼ばれないようにするよう変更 ( PR [#741](https://github.com/cwtickle/danoniplus/pull/741) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v14.5.5)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v14.5.4...v14.5.5#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v14.5.5)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v14.5.5.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v14.5.5.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v14.5.5/js/danoni_main.js)
/ 🎣 [**v15.2.2**](./Changelog-v15#v1522-2020-06-21)

## v14.5.4 ([2020-06-18](https://github.com/cwtickle/danoniplus/releases/tag/v14.5.4))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v14.5.4/total)](https://github.com/cwtickle/danoniplus/archive/v14.5.4.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v14.5.4/support/v14?style=social)](https://github.com/cwtickle/danoniplus/compare/v14.5.4...support/v14#files_bucket) 
- 🐞 通常キーであらかじめデフォルトセットされているキーコンフィグパターンと異なるキーを割り当てた際、初回保存時のみそのキーがリセットされる問題を修正 ( PR [#738](https://github.com/cwtickle/danoniplus/pull/738) ) <- :boom: [**v6.6.0**](Changelog-v6#v660-2019-07-08)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v14.5.4)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v14.5.3...v14.5.4#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v14.5.4)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v14.5.4.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v14.5.4.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v14.5.4/js/danoni_main.js)
/ 🎣 [**v15.2.1**](./Changelog-v15#v1522-2020-06-18)

## v14.5.3 ([2020-05-24](https://github.com/cwtickle/danoniplus/releases/tag/v14.5.3))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v14.5.3/total)](https://github.com/cwtickle/danoniplus/archive/v14.5.3.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v14.5.3/support/v14?style=social)](https://github.com/cwtickle/danoniplus/compare/v14.5.3...support/v14#files_bucket) 
- 🐞 特殊キーの後に通常キーの譜面があると譜面選択できない問題を修正 ( PR [#729](https://github.com/cwtickle/danoniplus/pull/729) ) <- :boom: [**v4.6.2**](Changelog-v4#v462-2019-05-04)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v14.5.3)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v14.5.2...v14.5.3#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v14.5.3)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v14.5.3.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v14.5.3.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v14.5.3/js/danoni_main.js)
/ 🎣 [**v15.1.2**](./Changelog-v15#v1512-2020-05-24)

----

## v14.5.2 ([2020-05-13](https://github.com/cwtickle/danoniplus/releases/tag/v14.5.2))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v14.5.2/total)](https://github.com/cwtickle/danoniplus/archive/v14.5.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v14.5.2/support/v14?style=social)](https://github.com/cwtickle/danoniplus/compare/v14.5.2...support/v14#files_bucket) 
- 🛠️ バージョン比較用リンクを作成 ( PR [#711](https://github.com/cwtickle/danoniplus/pull/711) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v14.5.2)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v14.5.1...v14.5.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v14.5.2)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v14.5.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v14.5.2.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v14.5.2/js/danoni_main.js)

## v14.5.1 ([2020-05-09](https://github.com/cwtickle/danoniplus/releases/tag/v14.5.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v14.5.1/total)](https://github.com/cwtickle/danoniplus/archive/v14.5.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v14.5.1/support/v14?style=social)](https://github.com/cwtickle/danoniplus/compare/v14.5.1...support/v14#files_bucket) 
- 🛠️ 譜面データ用の特殊文字対象にバッククォート、シングルクォートを追加 ( PR [#706](https://github.com/cwtickle/danoniplus/pull/706) )
- 🐞 歌詞表示の一部で置き換え文字の処理が利いていない問題を修正 ( PR [#706](https://github.com/cwtickle/danoniplus/pull/706) ) <- :boom: [**v14.4.0**](Changelog-v14#v1440-2020-05-05)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v14.5.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v14.5.0...v14.5.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v14.5.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v14.5.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v14.5.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v14.5.1/js/danoni_main.js)

## v14.5.0 ([2020-05-07](https://github.com/cwtickle/danoniplus/releases/tag/v14.5.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v14.5.0/total)](https://github.com/cwtickle/danoniplus/archive/v14.5.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v14.5.0/support/v14?style=social)](https://github.com/cwtickle/danoniplus/compare/v14.5.0...support/v14#files_bucket) 
- ⭐ 譜面数が5を超える場合、自動で譜面セレクターを使用するよう変更 ( PR [#703](https://github.com/cwtickle/danoniplus/pull/703) )
- 🛠️ グラフON/OFFボタン、Data Saveボタン等の説明文を追加 ( PR [#701](https://github.com/cwtickle/danoniplus/pull/701) )
- 🛠️ Twitter投稿リザルトで曲名・制作者名の後に半角空白を追加 ( PR [#702](https://github.com/cwtickle/danoniplus/pull/702) )
- 🐞 danoni_setting.jsでDisplay設定を明示的に有効にした場合、ボタンの初期値がOFFになってしまう問題を修正 ( PR [#700](https://github.com/cwtickle/danoniplus/pull/700) ) <- :boom: [**v14.0.2**](Changelog-v14#v1402-2020-04-29)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v14.5.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v14.4.0...v14.5.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v14.5.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v14.5.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v14.5.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v14.5.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v14.5.0/js/lib/danoni_constants.js)<br>❤️ izkdic

## v14.4.0 ([2020-05-05](https://github.com/cwtickle/danoniplus/releases/tag/v14.4.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v14.4.0/total)](https://github.com/cwtickle/danoniplus/archive/v14.4.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v14.4.0/support/v14?style=social)](https://github.com/cwtickle/danoniplus/compare/v14.4.0...support/v14#files_bucket) 
- ⭐ 譜面データでカンマ、ドル等の特殊文字を利用するための文字列を作成 ( Issue [#697](https://github.com/cwtickle/danoniplus/pull/697), PR [#696](https://github.com/cwtickle/danoniplus/pull/696) )
- 🐞 結果画面のTweetで<, >の文字が置換されて表示されてしまう問題を修正 ( PR [#696](https://github.com/cwtickle/danoniplus/pull/696) ) <- :boom:   **initial**

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v14.4.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v14.3.4...v14.4.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v14.4.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v14.4.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v14.4.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v14.4.0/js/danoni_main.js)

## v14.3.4 ([2020-05-04](https://github.com/cwtickle/danoniplus/releases/tag/v14.3.4))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v14.3.4/total)](https://github.com/cwtickle/danoniplus/archive/v14.3.4.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v14.3.4/support/v14?style=social)](https://github.com/cwtickle/danoniplus/compare/v14.3.4...support/v14#files_bucket) 
- ⭐ ローカル時に限り、誤差1フレームでもFast/Slowを表示するように変更 ( PR [#685](https://github.com/cwtickle/danoniplus/pull/685) )
- ⭐ 結果画面にFast/Slowのカウント数を表示するよう変更 ( PR [#685](https://github.com/cwtickle/danoniplus/pull/685), [#693](https://github.com/cwtickle/danoniplus/pull/693), [#695](https://github.com/cwtickle/danoniplus/pull/695) )
- ⭐ ±1フレーム以内で表示するJust表記を廃止 ( PR [#685](https://github.com/cwtickle/danoniplus/pull/685) )
- ⭐ Twitter投稿用リザルトのURL表示に譜面番号を付加するよう変更 ( PR [#686](https://github.com/cwtickle/danoniplus/pull/686) )
- ⭐ 譜面データでパイプ、アンパサンド等の特殊文字を利用するための文字列を作成 ( PR [#688](https://github.com/cwtickle/danoniplus/pull/688) )
- 🛠️ フリーズアローヒット時のコード整理 ( PR [#684](https://github.com/cwtickle/danoniplus/pull/684) )
- 🐞 フリーズ始点判定有効時、シャキン以下の判定関数に差分フレーム数としてマイナス値が渡せていない問題を修正 ( PR [#684](https://github.com/cwtickle/danoniplus/pull/684) ) <- :boom: [**v5.7.0**](Changelog-v5#v570-2019-06-01)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v14.3.4)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v14.2.0...v14.3.4#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v14.3.4)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v14.3.4.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v14.3.4.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v14.3.4/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v14.3.4/js/lib/danoni_constants.js)<br>❤️ izkdic, ショウタ, MFV2 (@MFV2)

## v14.2.0 ([2020-05-03](https://github.com/cwtickle/danoniplus/releases/tag/v14.2.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v14.2.0/total)](https://github.com/cwtickle/danoniplus/archive/v14.2.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v14.2.0/support/v14?style=social)](https://github.com/cwtickle/danoniplus/compare/v14.2.0...support/v14#files_bucket) 
- 🛠️ APMの計算式をレベル計算ツール++の形式に寄せるよう修正 ( PR [#682](https://github.com/cwtickle/danoniplus/pull/682) )
- 🛠️ Appearanceの「Slit」を削除 ( PR [#681](https://github.com/cwtickle/danoniplus/pull/681) )
- 🐞 高速(5x以上)で矢印を見逃したとき、画面上に矢印が残ることがある事象を修正 ( PR [#681](https://github.com/cwtickle/danoniplus/pull/681) ) <- :boom: [**v14.1.0**](Changelog-v14#v1410-2020-05-01)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v14.2.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v14.1.0...v14.2.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v14.2.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v14.2.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v14.2.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v14.2.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v14.2.0/js/lib/danoni_constants.js)🔴<br>❤️ ぷろろーぐ (@prlg25), izkdic

## v14.1.0 ([2020-05-01](https://github.com/cwtickle/danoniplus/releases/tag/v14.1.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v14.1.0/total)](https://github.com/cwtickle/danoniplus/archive/v14.1.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v14.1.0/support/v14?style=social)](https://github.com/cwtickle/danoniplus/compare/v14.1.0...support/v14#files_bucket) 
- ⭐ 矢印描画について、ステップゾーン位置に応じて描画領域が変わるように変更  ( PR [#673](https://github.com/cwtickle/danoniplus/pull/673) )
- ⭐ Display: MusicInfo適用時、楽曲のクレジットを最初の数秒のみ表示する形式に変更 ( PR [#674](https://github.com/cwtickle/danoniplus/pull/674) )
- ⭐ グラデーション表記で色名 (blueやredなど) が使えるよう変更 ( PR [#675](https://github.com/cwtickle/danoniplus/pull/675) )
- 🛠️ 影矢印のデザインを見直し ( PR [#672](https://github.com/cwtickle/danoniplus/pull/672) )
- 🛠️ setShadowColor有効時、ステップゾーンとフリーズアローの塗りつぶし色の
適用スタイルをcssとして分けるように変更 ( PR [#672](https://github.com/cwtickle/danoniplus/pull/672) )
- 🛠️ ステップゾーン位置関連の変数をオブジェクトへ集約 ( PR [#673](https://github.com/cwtickle/danoniplus/pull/673) )
- 🛠️ 譜面ヘッダー：playbackRate, maxLifeValに対して入力チェックを強化 ( PR [#675](https://github.com/cwtickle/danoniplus/pull/675) )
- 🛠️ 結果画面で表示するHidden+, Sudden+について、パーセント表記を廃止 ( PR [#673](https://github.com/cwtickle/danoniplus/pull/673) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v14.1.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v14.0.2...v14.1.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v14.1.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v14.1.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v14.1.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v14.1.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v14.1.0/js/lib/danoni_constants.js)🔴+(img)

## v14.0.2 ([2020-04-29](https://github.com/cwtickle/danoniplus/releases/tag/v14.0.2))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v14.0.2/total)](https://github.com/cwtickle/danoniplus/archive/v14.0.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v14.0.2/support/v14?style=social)](https://github.com/cwtickle/danoniplus/compare/v14.0.2...support/v14#files_bucket) 
- ⭐ DisplayオプションのデフォルトOFF設定及び有効/無効化設定の実装 ( PR [#665](https://github.com/cwtickle/danoniplus/pull/665) )
- ⭐ DisplayオプションのボタンがONの状態のとき、
他のボタンを連動してOFFに自動変更する機能を実装 ( Issue [#664](https://github.com/cwtickle/danoniplus/pull/664), PR [#665](https://github.com/cwtickle/danoniplus/pull/665) )
- ⭐ オンマウスでオプション設定の説明文を表示するよう変更 ( PR [#663](https://github.com/cwtickle/danoniplus/pull/663) )
- 🛠️ 警告メッセージを常に再作成し手前に来るよう変更 ( PR [#665](https://github.com/cwtickle/danoniplus/pull/665) )
- 🛠️ 警告メッセージエリアの最大長を設定し、超過した場合はスクロールするよう変更 ( PR [#670](https://github.com/cwtickle/danoniplus/pull/670) )
- 🐞 キーコンフィグ画面で、割り当てキーがundefinedとなる場合がある問題を修正 ( PR [#668](https://github.com/cwtickle/danoniplus/pull/668) ) <- :boom: [**v13.3.1**](Changelog-v13#v1331-2020-04-12)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v14.0.2)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v13.6.0...v14.0.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v14.0.2)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v14.0.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v14.0.2.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v14.0.2/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v14.0.2/js/lib/danoni_constants.js)🔴

[**<- v15**](Changelog-v15) | **v14** | [**v13 ->**](Changelog-v13)
