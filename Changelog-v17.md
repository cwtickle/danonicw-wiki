⭐ New Features / 🛠️ Improvements / 🐞 Bug Fixes  
( Files changed: ⚪ danoni_main.js / 🔵 danoni_constants.js / 🔴 css files )

[**<- v18**](Changelog-v18) | **v17** | [**v16 ->**](Changelog-v16)  
(**🔖 [17 Releases](https://github.com/cwtickle/danoniplus/releases?q=tag%3Av17)** )

## [Warning] 更新終了後の不具合情報
- [更新を終了したバージョンの不具合情報#v17](DeprecatedVersionBugs#v17) を参照

## v17.5.9 ([2021-02-12](https://github.com/cwtickle/danoniplus/releases/tag/v17.5.9))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v17.5.9/total)](https://github.com/cwtickle/danoniplus/archive/v17.5.9.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v17.5.9/support/v17?style=social)](https://github.com/cwtickle/danoniplus/compare/v17.5.9...support/v17#files_bucket) 
- 🐞 createCss2Button関数にて右クリック拡張ができない場合がある問題を修正 ( PR [#970](https://github.com/cwtickle/danoniplus/pull/970) ) <- :boom: [**v17.5.0**](Changelog-v17#v1750-2020-10-17)
- 🐞 changeStyle関数のコードミスを修正 ( PR [#958](https://github.com/cwtickle/danoniplus/pull/958) ) <- :boom: [**v17.1.0**](Changelog-v17#v1710-2020-09-28)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v17.5.9)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v17.5.8...v17.5.9#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v17.5.9)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v17.5.9.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v17.5.9.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v17.5.9/js/danoni_main.js)
/ 🎣 [**v19.5.2**](./Changelog-v19#v1952-2021-02-12), [**v19.4.1**](./Changelog-v19#v1941-2021-02-06)

## v17.5.8 ([2021-01-05](https://github.com/cwtickle/danoniplus/releases/tag/v17.5.8))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v17.5.8/total)](https://github.com/cwtickle/danoniplus/archive/v17.5.8.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v17.5.8/support/v17?style=social)](https://github.com/cwtickle/danoniplus/compare/v17.5.8...support/v17#files_bucket) 
- 🐞 同一レーン上に通常矢印とフリーズアローが短い間隔で存在するとき、通常矢印の判定が優先されてしまう事象を修正 ( PR [#929](https://github.com/cwtickle/danoniplus/pull/929), [#930](https://github.com/cwtickle/danoniplus/pull/930), [#932](https://github.com/cwtickle/danoniplus/pull/932) ) <- :boom: **initial**

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v17.5.8)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v17.5.7...v17.5.8#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v17.5.8)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v17.5.8.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v17.5.8.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v17.5.8/js/danoni_main.js)
/ 🎣 [**v18.9.2**](./Changelog-v18#v1892-2021-01-05), [**v18.9.1**](./Changelog-v18#v1891-2021-01-03)

## v17.5.7 ([2020-12-30](https://github.com/cwtickle/danoniplus/releases/tag/v17.5.7))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v17.5.7/total)](https://github.com/cwtickle/danoniplus/archive/v17.5.7.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v17.5.7/support/v17?style=social)](https://github.com/cwtickle/danoniplus/compare/v17.5.7...support/v17#files_bucket) 
- 🐞 一部のキーを押すと全押し状態になる問題を修正 ( PR [#924](https://github.com/cwtickle/danoniplus/pull/924) ) <- :boom: [**v16.0.0**](Changelog-v16#v1600-2020-08-06) 

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v17.5.7)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v17.5.6...v17.5.7#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v17.5.7)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v17.5.7.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v17.5.7.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v17.5.7/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v17.5.7/js/lib/danoni_constants.js)
/ 🎣 [**v18.8.2**](./Changelog-v18#v1892-2020-12-30)

## v17.5.6 ([2020-12-25](https://github.com/cwtickle/danoniplus/releases/tag/v17.5.6))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v17.5.6/total)](https://github.com/cwtickle/danoniplus/archive/v17.5.6.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v17.5.6/support/v17?style=social)](https://github.com/cwtickle/danoniplus/compare/v17.5.6...support/v17#files_bucket) 
- 🐞 カスタムjs, スキンjsを二重読込している問題を修正 ( PR [#917](https://github.com/cwtickle/danoniplus/pull/917) ) <- :boom: [**v7.7.0**](Changelog-v7#v770-2019-08-25)
- 🐞 楽曲・画像読込チェックの待ち時間誤りを修正 ( PR [#915](https://github.com/cwtickle/danoniplus/pull/915) ) <- :boom: [**v10.1.1**](Changelog-v10#v1011-2019-11-11)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v17.5.6)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v17.5.5...v17.5.6#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v17.5.6)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v17.5.6.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v17.5.6.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v17.5.6/js/danoni_main.js)
/ 🎣 [**v18.7.0**](./Changelog-v18#v1870-2020-12-25)

## v17.5.5 ([2020-12-21](https://github.com/cwtickle/danoniplus/releases/tag/v17.5.5))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v17.5.5/total)](https://github.com/cwtickle/danoniplus/archive/v17.5.5.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v17.5.5/support/v17?style=social)](https://github.com/cwtickle/danoniplus/compare/v17.5.5...support/v17#files_bucket) 
- 🐞 曲終了直後にリトライキーを押すとALL0のリザルトが表示される問題を修正 ( PR [#908](https://github.com/cwtickle/danoniplus/pull/908) ) <- :boom: [**v0.67.x**](Changelog-v0#v067x-2018-11-17) 

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v17.5.5)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v17.5.4...v17.5.5#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v17.5.5)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v17.5.5.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v17.5.5.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v17.5.5/js/danoni_main.js)
/ 🎣 [**v18.6.0**](./Changelog-v18#v1860-2020-12-21)

## v17.5.4 ([2020-11-23](https://github.com/cwtickle/danoniplus/releases/tag/v17.5.4))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v17.5.4/total)](https://github.com/cwtickle/danoniplus/archive/v17.5.4.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v17.5.4/support/v17?style=social)](https://github.com/cwtickle/danoniplus/compare/v17.5.4...support/v17#files_bucket) 
- 🐞 Display:Colorのボタンが無効化されていた場合にColorTypeの挙動により、Display:Colorの切り替えができてしまう問題を修正 ( PR [#892](https://github.com/cwtickle/danoniplus/pull/892) ) <- :boom: [**v14.0.2**](Changelog-v14#v1402-2020-04-29)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v17.5.4)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v17.5.3...v17.5.4#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v17.5.4)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v17.5.4.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v17.5.4.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v17.5.4/js/danoni_main.js)
/ 🎣 [**v18.3.0**](./Changelog-v18#v1830-2020-11-18)

## v17.5.3 ([2020-11-02](https://github.com/cwtickle/danoniplus/releases/tag/v17.5.3))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v17.5.3/total)](https://github.com/cwtickle/danoniplus/archive/v17.5.3.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v17.5.3/support/v17?style=social)](https://github.com/cwtickle/danoniplus/compare/v17.5.3...support/v17#files_bucket) 
- 🐞 速度データ(speed_change)が空の場合、データがあると判断されてしまいspeed_changeが優先されてしまう問題を修正 ( PR [#884](https://github.com/cwtickle/danoniplus/pull/884) ) <- :boom: [**v8.0.0**](Changelog-v8#v800-2019-09-08) 

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v17.5.3)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v17.5.2...v17.5.3#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v17.5.3)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v17.5.3.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v17.5.3.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v17.5.3/js/danoni_main.js)
/ 🎣 [**v18.3.0**](./Changelog-v18#v1830-2020-11-18)

## v17.5.2 ([2020-10-31](https://github.com/cwtickle/danoniplus/releases/tag/v17.5.2))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v17.5.2/total)](https://github.com/cwtickle/danoniplus/archive/v17.5.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v17.5.2/support/v17?style=social)](https://github.com/cwtickle/danoniplus/compare/v17.5.2...support/v17#files_bucket) 
- 🐞 複数曲搭載時に曲名の2段目がundefinedになる問題を修正 ( PR [#882](https://github.com/cwtickle/danoniplus/pull/882) ) <- :boom: [**v17.3.0**](Changelog-v17#v1730-2020-10-08)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v17.5.2)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v17.5.1...v17.5.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v17.5.2)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v17.5.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v17.5.2.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v17.5.2/js/danoni_main.js)
/ 🎣 [**v18.0.1**](./Changelog-v18#v1801-2020-10-31)

----

## v17.5.1 ([2020-10-24](https://github.com/cwtickle/danoniplus/releases/tag/v17.5.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v17.5.1/total)](https://github.com/cwtickle/danoniplus/archive/v17.5.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v17.5.1/support/v17?style=social)](https://github.com/cwtickle/danoniplus/compare/v17.5.1...support/v17#files_bucket) 
- 🐞 カンマのエスケープ文字の変換間違いを修正 ( Gitter [2020-10-21](https://gitter.im/danonicw/community?at=5f903fc0631a250ab2823b73), PR [#877](https://github.com/cwtickle/danoniplus/pull/877) ) <- :boom: [**v17.4.2**](Changelog-v17#v1742-2020-10-11)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v17.5.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v17.5.0...v17.5.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v17.5.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v17.5.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v17.5.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v17.5.1/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v17.5.1/js/lib/danoni_constants.js)<br>❤️ izkdic

## v17.5.0 ([2020-10-17](https://github.com/cwtickle/danoniplus/releases/tag/v17.5.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v17.5.0/total)](https://github.com/cwtickle/danoniplus/archive/v17.5.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v17.5.0/support/v17?style=social)](https://github.com/cwtickle/danoniplus/compare/v17.5.0...support/v17#files_bucket) 
- ⭐ ボタン作成処理に右クリック処理、イベント引数を追加 ( PR [#871](https://github.com/cwtickle/danoniplus/pull/871) )
- 🛠️ 複数子要素を親要素へ追加する処理を集約 ( PR [#871](https://github.com/cwtickle/danoniplus/pull/871) )
- 🛠️ 長い名前の関数を全体的に見直し、その他コード整理 ( PR [#872](https://github.com/cwtickle/danoniplus/pull/872), [#873](https://github.com/cwtickle/danoniplus/pull/873) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v17.5.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v17.4.3...v17.5.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v17.5.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v17.5.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v17.5.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v17.5.0/js/danoni_main.js)

```javascript
g_cxtDeleteFlg.btnStart = true;  // g_cxtDeleteFlg.(ボタンID) = true でボタンIDの右クリック処理をやめる
g_cxtAddFunc.btnStart = _ => {
    commentInit(); // g_cxtAddFunc.(ボタンID) に関数を定義することでその処理を後から挿入
};
```

## v17.4.3 ([2020-10-12](https://github.com/cwtickle/danoniplus/releases/tag/v17.4.3))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v17.4.3/total)](https://github.com/cwtickle/danoniplus/archive/v17.4.3.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v17.4.3/support/v17?style=social)](https://github.com/cwtickle/danoniplus/compare/v17.4.3...support/v17#files_bucket) 
- 🐞 矢印塗りつぶし有、ステップゾーンをOFFにした場合に矢印塗りつぶし部分が残る問題を修正 ( PR [#866](https://github.com/cwtickle/danoniplus/pull/866), [#868](https://github.com/cwtickle/danoniplus/pull/868) ) <- :boom: [**v5.12.0**](Changelog-v5#v5120-2019-06-14)
- 🐞 DataSaveをOFFにしたとき、Reverseのstyleが反映されない問題を修正 ( PR [#867](https://github.com/cwtickle/danoniplus/pull/867) ) <- :boom: [**v17.0.1**](Changelog-v17#v1701-2020-09-27)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v17.4.3)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v17.4.2...v17.4.3#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v17.4.3)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v17.4.3.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v17.4.3.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v17.4.3/js/danoni_main.js)

## v17.4.2 ([2020-10-11](https://github.com/cwtickle/danoniplus/releases/tag/v17.4.2))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v17.4.2/total)](https://github.com/cwtickle/danoniplus/archive/v17.4.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v17.4.2/support/v17?style=social)](https://github.com/cwtickle/danoniplus/compare/v17.4.2...support/v17#files_bucket) 
- 🛠️ コードの見直し ( PR [#861](https://github.com/cwtickle/danoniplus/pull/861) )
- 🐞 結果画面のショボーンのラベルID誤りを修正 ( PR [#860](https://github.com/cwtickle/danoniplus/pull/860) ) <- :boom: [**v17.2.0**](Changelog-v17#v1720-2020-10-03)
- 🐞 最後の矢印を取り忘れてクリアするとundefinedになる問題を修正 ( PR [#862](https://github.com/cwtickle/danoniplus/pull/862) ) <- :boom: [**v17.0.1**](Changelog-v17#v1701-2020-09-27)
- 🐞 strictモード有効化に伴い、初期化されていない変数を修正 ( PR [#864](https://github.com/cwtickle/danoniplus/pull/864) ) <- :boom: [**v17.4.0**](Changelog-v17#v1740-2020-10-09)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v17.4.2)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v17.4.0...v17.4.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v17.4.2)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v17.4.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v17.4.2.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v17.4.2/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v17.4.2/js/lib/danoni_constants.js)<br>❤️ apoi, izkdic

## v17.4.0 ([2020-10-09](https://github.com/cwtickle/danoniplus/releases/tag/v17.4.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v17.4.0/total)](https://github.com/cwtickle/danoniplus/archive/v17.4.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v17.4.0/support/v17?style=social)](https://github.com/cwtickle/danoniplus/compare/v17.4.0...support/v17#files_bucket) 
- 🛠️ strictモードを有効化 ( PR [#855](https://github.com/cwtickle/danoniplus/pull/855) )
- 🐞 strictモード有効化に伴い、初期化されていない変数を見直し ( PR [#856](https://github.com/cwtickle/danoniplus/pull/856) ) <- :boom: [**v17.4.0**](Changelog-v17#v1740-2020-10-09) <- [**v12.0.0**](Changelog-v12#v1200-2020-02-09)
- 🐞 カスタムゲージで一部未指定がある場合にボーダー値が反映されない問題を修正 ( PR [#857](https://github.com/cwtickle/danoniplus/pull/857) ) <- :boom: [**v17.3.0**](Changelog-v17#v1730-2020-10-08)
- 🐞 ツール値出力時、時間表示がおかしくなることがある問題を修正 ( PR [#858](https://github.com/cwtickle/danoniplus/pull/858) ) <- :boom: [**v12.0.0**](Changelog-v12#v1200-2020-02-09) <- [**v2.0.0**](Changelog-v2#v200-2019-01-18)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v17.4.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v17.3.0...v17.4.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v17.4.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v17.4.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v17.4.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v17.4.0/js/danoni_main.js)<br>❤️ goe (@goe0)

## v17.3.0 ([2020-10-08](https://github.com/cwtickle/danoniplus/releases/tag/v17.3.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v17.3.0/total)](https://github.com/cwtickle/danoniplus/archive/v17.3.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v17.3.0/support/v17?style=social)](https://github.com/cwtickle/danoniplus/compare/v17.3.0...support/v17#files_bucket) 
- ⭐ タイトル文字のフォントと位置について2行目指定ができるように変更 ( PR [#851](https://github.com/cwtickle/danoniplus/pull/851) )
- 🛠️ レベル計算、階層スプライト、ゲージ設定周りのコード整理 ( PR [#852](https://github.com/cwtickle/danoniplus/pull/852), [#853](https://github.com/cwtickle/danoniplus/pull/853) )
- 🐞 tuningのみ指定があった場合に制作者リンクが出ない問題を修正 ( PR [#850](https://github.com/cwtickle/danoniplus/pull/850) ) <- :boom: [**v3.0.0**](Changelog-v3#v300-2019-02-25)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v17.3.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v17.2.0...v17.3.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v17.3.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v17.3.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v17.3.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v17.3.0/js/danoni_main.js)

## v17.2.0 ([2020-10-03](https://github.com/cwtickle/danoniplus/releases/tag/v17.2.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v17.2.0/total)](https://github.com/cwtickle/danoniplus/archive/v17.2.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v17.2.0/support/v17?style=social)](https://github.com/cwtickle/danoniplus/compare/v17.2.0...support/v17#files_bucket) 
- 🛠️ 制作者、アーティスト名リンクが未指定の場合、リンクを実行させないよう修正 ( PR [#846](https://github.com/cwtickle/danoniplus/pull/846) )
- 🛠️ HTMLを埋め込まない箇所を`Node.textContent`の記述に置き換え ( PR [#847](https://github.com/cwtickle/danoniplus/pull/847) )
- 🛠️ その他コード整理、CodeQLの適用 ( PR [#844](https://github.com/cwtickle/danoniplus/pull/844), [#848](https://github.com/cwtickle/danoniplus/pull/848) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v17.2.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v17.1.0...v17.2.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v17.2.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v17.2.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v17.2.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v17.2.0/js/danoni_main.js)

## v17.1.0 ([2020-09-28](https://github.com/cwtickle/danoniplus/releases/tag/v17.1.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v17.1.0/total)](https://github.com/cwtickle/danoniplus/archive/v17.1.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v17.1.0/support/v17?style=social)](https://github.com/cwtickle/danoniplus/compare/v17.1.0...support/v17#files_bucket) 
- ⭐ ボタンクリック後の処理に対して処理を追加できる機能を実装 ( PR [#841](https://github.com/cwtickle/danoniplus/pull/841) )
- ⭐ ボタン及びラベルのスタイル一括変更関数を実装 ( PR [#841](https://github.com/cwtickle/danoniplus/pull/841) )
- 🐞 フェードイン時、フリーズアローの計算処理で止まる問題を修正 ( Gitter [2020-09-27](https://gitter.im/danonicw/community?at=5f7096740b5f3873c9f74f7e), PR [#842](https://github.com/cwtickle/danoniplus/pull/842) ) <- :boom: [**v17.0.1**](Changelog-v17#v1701-2020-09-27)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v17.1.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v17.0.1...v17.1.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v17.1.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v17.1.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v17.1.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v17.1.0/js/danoni_main.js)<br>❤️ goe (@goe0), ショウタ

```javascript
g_btnDeleteFlg.btnStart = true;  // g_btnDeleteFlg.(ボタンID) = true でボタンIDが行っている処理をやめる
g_btnAddFunc.btnStart = _ => {
    commentInit(); // g_btnAddFunc.(ボタンID) に関数を定義することでその処理を後から挿入
};
```

## v17.0.1 ([2020-09-27](https://github.com/cwtickle/danoniplus/releases/tag/v17.0.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v17.0.1/total)](https://github.com/cwtickle/danoniplus/archive/v17.0.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v17.0.1/support/v17?style=social)](https://github.com/cwtickle/danoniplus/compare/v17.0.1...support/v17#files_bucket) 
- 🛠️ ラベル・ボタン・色付きオブジェクト作成用関数を作り直し ( Issue [#833](https://github.com/cwtickle/danoniplus/pull/833), PR [#834](https://github.com/cwtickle/danoniplus/pull/834) )
- 🛠️ コードの整理 ( PR [#835](https://github.com/cwtickle/danoniplus/pull/835), [#837](https://github.com/cwtickle/danoniplus/pull/837) )
- 🐞 user-selectがSafariブラウザのみ動作しない問題を修正 ( PR [#832](https://github.com/cwtickle/danoniplus/pull/832) ) <- :boom: [**v16.4.0**](Changelog-v16#v1640-2020-09-22)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v17.0.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v16.4.0...v17.0.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v17.0.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v17.0.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v17.0.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v17.0.1/js/danoni_main.js)

[**<- v18**](Changelog-v18) | **v17** | [**v16 ->**](Changelog-v16)
