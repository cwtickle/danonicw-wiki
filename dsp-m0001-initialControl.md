[← オブジェクトリファレンスに戻る](ObjectReferenceIndex)  

# タイトル前処理
### 概要
- 外部jsファイルの読込やローカルストレージの読込、レベル計算用のデータ取得などを行う。
- 原則、この処理は1度のみ行われる。

### 処理の流れ
1. danoni_main.jsが配置されているパスを取得 ( **current** 関数 )
2. 下記のファイルを読込 ( [**loadScript2**](fnc-c0010-loadScript) 関数 )
    - 1.で取得したパスをルートパスとして読み込む。

|フォルダ|ファイル名|必須|備考|
|----|----|----|----|
|js/lib|danoni_localbinary.js|(*)|ローカル時のみ必須、svgデータ|
|js/lib|danoni_constants.js|*|定数管理ファイル|
|js/lib|danoni_legacy_function.js||過去の関数群。必須ではないがカスタムJsで利用している場合は必要|

3. ルートdiv要素(divRoot), 背景の描画(divBack)、Now Loading表示を行う  
 ( **initialControl** 関数 )
4. 譜面の読込オプションの設定を行う。パラメータの詳細は[こちら](HowtoMake#参考その他の設定) ( **initialControl** 関数 )

|設定名|必須|備考|
|----|----|----|
|enableAmpersandSplit||譜面データ内の主要区切り文字。デフォルトは`&`と`\|`両方を区切り文字として扱う。|
|enableDecodeURI||譜面データをURIデコードするかどうかを設定します。|

5. 作品別ローカルストレージの読み込みを行う ( **loadLocalStorage** 関数 )
    - Adjustment, Volume, Appearance, Opacityの設定があれば適用、無ければ初期化を行う。
    - ハイスコア情報があればそのまま、無ければ初期化を行う。

6. HTML埋め込み、もしくは外部ファイルより譜面データを読み込む ( [**loadChartFile**](fnc-c0011-loadChartFile) 関数 )  
読み込んだ譜面データを分解し、`&key=value`を`g_rootObj.key=value;`の形式に変換する ( **dosConvert** 関数 )  
7. 共通設定ファイル (デフォルト：danoni_setting.js) があれば、取り込みする ( **initialControl** 関数 )
8. URLのクエリに`scoreId`が含まれていれば、譜面番号を取得する ( **initialControl** 関数 )
9. CSSファイル読込前に必要な譜面ヘッダーを読み込んだ後、CSSファイル、customJsファイルを読み込む ( **preheaderConvert**, [**loadMultipleFiles2**](fnc-c0044-loadMultipleFiles) 関数 )
10. 譜面ヘッダー、特殊キー情報の取り込み処理を行う ( **headerConvert**, **keysConvert** 関数 )  
このタイミングで g_headerObj 及び g_keyObj が初期化される。
11. CSSファイル内の背景情報を取得するために再描画を行う ( **initialControl** 関数 )
12. スキンCSSファイル、プリロード指定したファイルを読み込む ( **initialControl** 関数 )
13. customJsファイル、譜面情報取得のため譜面全データの読込 ( [**loadChartFile**](fnc-c0011-loadChartFile), **getScoreDetailData** 関数 )  
このタイミングで g_detailObj が初期化される。

以上の処理が完了後、タイトル画面描画を行う ( **titleinit** 関数 )

### フローチャート
- カスタムJs/スキンJsによる差し込み処理は水色で表記。
- 関連ページがあるボックスは青枠で表記。

※この時点では譜面明細表示（速度変化、譜面密度、レベル計算）に必要なデータのみを取り込む。  
　色変化・歌詞表示、タイトル以外の背景・マスクモーションはデータ格納しない。
```mermaid
flowchart TD
    a0["danoni_main.jsが配置されているパスを取得<br>current"]-->a1["danoni_main.css の読込<br>importCssFile2"]-->b1["danoni_constants.js の読込<br>loadScript2"]-->initialControl;
    initialControl-->titleInit;

subgraph initialControl
    A([start])-->B["作品別ローカルストレージの読込<br>loadLocalStorage"]-->C["譜面データの読み込み(1ファイル目)<br>loadChartFile"]-->D["共通設定ファイルパスの取得<br>getFilePath"]-->E["共通設定ファイルの読込<br>loadScript2"]-->F["譜面ヘッダーの読込<br>(先行読込分)<br>preheaderConvert"]-->G["カスタムjs/cssファイルの読込<br>loadMultipleFiles2"]-->H["譜面ヘッダーの読込<br>(先行読込分以外全て)<br>headerConvert"]-->I["カスタムキー設定の読込<br>keysConvert"]-->J["可変ウィンドウサイズを更新<br>updateWindowSiz"]-->Q["画像ファイルの読込<br>preloadFile"]-->K["譜面詳細情報取得(1ファイル目)<br>getScoreDetailData"]
    K.->getScoreDetailData;
    K-->L{他の外部譜面ファイル};
    L------>|なし| M["カスタム関数の実行<br>g_customJs.preTitle"]-->Z([end]);
    L-->|"あり"| N["譜面ファイル読込<br>(2ファイル目以降)<br>loadChartFile"]-->O["初期色やゲージ情報を取得<br>(2ファイル目以降)<br>resetColorAndGauge"]-->P["譜面詳細情報取得<br>(2ファイル目以降)<br>getScoreDetailData"];
    P-->L;
    P.->getScoreDetailData;
end

subgraph getScoreDetailData
    a2["譜面データ分割<br>scoreConvert ※"]-->storeBaseData
end;

subgraph storeBaseData
    a3["最終フレームの取得<br>getLastFrame"]-->b3["先頭フレームの取得<br>getStartFrame"]-->c3["最初の矢印のフレーム取得<br>getFirstArrowFrame"];
    c3-->d3["譜面密度グラフ用に譜面データを時間で16分割し、<br>分割単位ごとの矢印・フリーズアロー数をカウント"]-->e3["譜面難易度の計算<br>calcLevel"];
    e3-->f3["g_detailObjに譜面密度データ、矢印数、プレイ時間などを設定"];
end;

style M fill:#00ffff;
click a1 "https://github.com/cwtickle/danoniplus/wiki/fnc-c0020-importCssFile" _blank;
style a1 stroke:#0000ff;
style a1 stroke-width:4px;
click b1 "https://github.com/cwtickle/danoniplus/wiki/fnc-c0010-loadScript" _blank;
style b1 stroke:#0000ff;
style b1 stroke-width:4px;
click C "https://github.com/cwtickle/danoniplus/wiki/fnc-c0011-loadChartFile" _blank;
style C stroke:#0000ff;
style C stroke-width:4px;
click D "https://github.com/cwtickle/danoniplus/wiki/fnc-c0037-getFilePath" _blank;
style D stroke:#0000ff;
style D stroke-width:4px;
click E "https://github.com/cwtickle/danoniplus/wiki/fnc-c0010-loadScript" _blank;
style E stroke:#0000ff;
style E stroke-width:4px;
click G "https://github.com/cwtickle/danoniplus/wiki/fnc-c0044-loadMultipleFiles" _blank;
style G stroke:#0000ff;
style G stroke-width:4px;
click I "https://github.com/cwtickle/danoniplus/wiki/keys" _blank;
style I stroke:#0000ff;
style I stroke-width:4px;
click N "https://github.com/cwtickle/danoniplus/wiki/fnc-c0011-loadChartFile" _blank;
style N stroke:#0000ff;
style N stroke-width:4px;
click O "https://github.com/cwtickle/danoniplus/wiki/fnc-c0011-loadChartFile" _blank;
style O stroke:#0000ff;
style O stroke-width:4px;
click Q "https://github.com/cwtickle/danoniplus/wiki/fnc-c0019-preloadFile" _blank;
style Q stroke:#0000ff;
style Q stroke-width:4px;
```

### 関連項目
- [ソースの構成](AboutSource)
- [ローカルストレージ仕様](LocalStorage)
- [譜面ヘッダー仕様](dos_header)
- [カスタムJs(スキンJs)による処理割り込み](AboutCustomJs)