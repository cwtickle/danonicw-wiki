⭐ New Features / 🛠️ Improvements / 🐞 Bug Fixes

[**<- v9**](Changelog-v9) | **v8** | [**v7 ->**](Changelog-v7)  
(**🔖 [25 Releases](https://github.com/cwtickle/danoniplus/releases?q=tag%3Av8)** )

## [Warning] 更新終了後の不具合情報
- [更新を終了したバージョンの不具合情報#v8](DeprecatedVersionBugs#v8) を参照

## v8.7.10 ([2019-12-14](https://github.com/cwtickle/danoniplus/releases/tag/v8.7.10))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v8.7.10/total)](https://github.com/cwtickle/danoniplus/archive/v8.7.10.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v8.7.9/support/v8?style=social)](https://github.com/cwtickle/danoniplus/compare/v8.7.9...support/v8#files_bucket) 
- 🛠️ Localhost時にフレーム数を表示するように変更 ( PR [#561](https://github.com/cwtickle/danoniplus/pull/561) )
- 🛠️ ラベルのID重複を解消 ( PR [#545](https://github.com/cwtickle/danoniplus/pull/545), [#546](https://github.com/cwtickle/danoniplus/pull/546), [#562](https://github.com/cwtickle/danoniplus/pull/562) )
- 🛠️ fileでも起動可能なようにcrossorigin属性の付加条件を変更 ( PR [#564](https://github.com/cwtickle/danoniplus/pull/564) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v8.7.10)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v8.7.9...v8.7.10#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v8.7.10)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v8.7.10.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v8.7.10.tar.gz) )
/ 🎣 [**v11.0.0**](./Changelog-v11#v1100-2019-12-14)

## v8.7.9 ([2019-11-11](https://github.com/cwtickle/danoniplus/releases/tag/v8.7.9))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v8.7.9/total)](https://github.com/cwtickle/danoniplus/archive/v8.7.9.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v8.7.9/support/v8?style=social)](https://github.com/cwtickle/danoniplus/compare/v8.7.9...support/v8#files_bucket) 
- 🐞 MacOS, iOS (Safari)においてWeb Audio APIが動作しない問題を修正 ( PR [#523](https://github.com/cwtickle/danoniplus/pull/523), [#525](https://github.com/cwtickle/danoniplus/pull/525), [#527](https://github.com/cwtickle/danoniplus/pull/527) ) <- :boom: **initial**

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v8.7.9)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v8.7.7...v8.7.9#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v8.7.9)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v8.7.9.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v8.7.9.tar.gz) )
/ 🎣 [**v10.1.1**](./Changelog-v10#v1011-2019-11-11)

## v8.7.7 ([2019-10-14](https://github.com/cwtickle/danoniplus/releases/tag/v8.7.7))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v8.7.7/total)](https://github.com/cwtickle/danoniplus/archive/v8.7.7.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v8.7.7/support/v8?style=social)](https://github.com/cwtickle/danoniplus/compare/v8.7.7...support/v8#files_bucket) 
- 🐞 背景・マスクモーションでオールクリアが使用できない問題を修正 ( PR [#488](https://github.com/cwtickle/danoniplus/pull/488) ) <- :boom: [**v7.7.0**](Changelog-v7#v770-2019-08-25)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v8.7.7)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v8.7.6...v8.7.7#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v8.7.7)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v8.7.7.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v8.7.7.tar.gz) )
/ 🎣 [**v9.1.0**](./Changelog-v9#v910-2019-10-15)

## v8.7.6 ([2019-10-13](https://github.com/cwtickle/danoniplus/releases/tag/v8.7.6))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v8.7.6/total)](https://github.com/cwtickle/danoniplus/archive/v8.7.6.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v8.7.6/support/v8?style=social)](https://github.com/cwtickle/danoniplus/compare/v8.7.6...support/v8#files_bucket) 
- 🐞 ダミーIDの譜面ヘッダー誤りを修正 ( PR [#483](https://github.com/cwtickle/danoniplus/pull/483) ) <- :boom: [**v6.0.0**](Changelog-v6#v600-2019-06-22)
- 🐞 ダミー矢印/フリーズアロー用カスタム関数が定義できない問題を修正 ( PR [#485](https://github.com/cwtickle/danoniplus/pull/485) ) <- :boom: [**v6.1.0**](Changelog-v6#v610-2019-06-22)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v8.7.6)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v8.7.4...v8.7.6#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v8.7.6)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v8.7.6.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v8.7.6.tar.gz) )
/ 🎣 [**v9.0.3**](./Changelog-v9#v903-2019-10-13)

## v8.7.4 ([2019-10-12](https://github.com/cwtickle/danoniplus/releases/tag/v8.7.4))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v8.7.4/total)](https://github.com/cwtickle/danoniplus/archive/v8.7.4.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v8.7.4/support/v8?style=social)](https://github.com/cwtickle/danoniplus/compare/v8.7.4...support/v8#files_bucket) 
- 🐞 低速時かつフリーズアロー開始判定時にPF判定が早まる問題を修正 ( PR [#481](https://github.com/cwtickle/danoniplus/pull/481) ) <- :boom: [**v5.7.0**](Changelog-v5#v570-2019-06-01)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v8.7.4)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v8.7.3...v8.7.4#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v8.7.4)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v8.7.4.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v8.7.4.tar.gz) )
/ 🎣 [**v9.0.1**](./Changelog-v9#v901-2019-10-12)

----

## v8.7.3 ([2019-10-08](https://github.com/cwtickle/danoniplus/releases/tag/v8.7.3))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v8.7.3/total)](https://github.com/cwtickle/danoniplus/archive/v8.7.3.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v8.7.3/support/v8?style=social)](https://github.com/cwtickle/danoniplus/compare/v8.7.3...support/v8#files_bucket) 
- 🐞 速度変化が同一フレームにあると正常に動作しなくなる不具合を修正 ( PR [#477](https://github.com/cwtickle/danoniplus/pull/477) ) <- :boom: **initial**

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v8.7.3)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v8.7.2...v8.7.3#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v8.7.3)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v8.7.3.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v8.7.3.tar.gz) )<br>❤️ さつき (@satsukizzz), すずめ (@suzme)

## v8.7.2 ([2019-10-06](https://github.com/cwtickle/danoniplus/releases/tag/v8.7.2))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v8.7.2/total)](https://github.com/cwtickle/danoniplus/archive/v8.7.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v8.7.2/support/v8?style=social)](https://github.com/cwtickle/danoniplus/compare/v8.7.2...support/v8#files_bucket) 
- 🐞 エスケープ文字の適用順序誤りを修正 ( PR [#472](https://github.com/cwtickle/danoniplus/pull/472), [#473](https://github.com/cwtickle/danoniplus/pull/473) ) <- :boom: [**v0.66.x**](Changelog-v0#v066x-2018-11-16)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v8.7.2)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v8.7.1...v8.7.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v8.7.2)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v8.7.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v8.7.2.tar.gz) )<br>❤️ ショウタ

## v8.7.1 ([2019-10-05](https://github.com/cwtickle/danoniplus/releases/tag/v8.7.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v8.7.1/total)](https://github.com/cwtickle/danoniplus/archive/v8.7.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v8.7.1/support/v8?style=social)](https://github.com/cwtickle/danoniplus/compare/v8.7.1...support/v8#files_bucket) 
- 🐞 FPSが小数の場合にタイマーが作動しない問題を修正 ( PR [#470](https://github.com/cwtickle/danoniplus/pull/470) ) <- :boom: [**v8.6.3**](Changelog-v8#v863-2019-10-02)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v8.7.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v8.7.0...v8.7.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v8.7.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v8.7.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v8.7.1.tar.gz) )<br>❤️ すずめ (@suzme)

## v8.7.0 ([2019-10-03](https://github.com/cwtickle/danoniplus/releases/tag/v8.7.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v8.7.0/total)](https://github.com/cwtickle/danoniplus/archive/v8.7.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v8.7.0/support/v8?style=social)](https://github.com/cwtickle/danoniplus/compare/v8.7.0...support/v8#files_bucket) 
- ⭐️ ローカルストレージ（作品別）のキーからscoreIdを除去 ( PR [#467](https://github.com/cwtickle/danoniplus/pull/467) )
- 🛠️ リザルトコピーのURLからscoreIdを除去 ( PR [#468](https://github.com/cwtickle/danoniplus/pull/468) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v8.7.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v8.6.3...v8.7.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v8.7.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v8.7.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v8.7.0.tar.gz) )<br>❤️ すずめ (@suzme)

## v8.6.3 ([2019-10-02](https://github.com/cwtickle/danoniplus/releases/tag/v8.6.3))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v8.6.3/total)](https://github.com/cwtickle/danoniplus/archive/v8.6.3.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v8.6.3/support/v8?style=social)](https://github.com/cwtickle/danoniplus/compare/v8.6.3...support/v8#files_bucket) 
- 🛠️ 最初の読込時のオブジェクト削除をカット、ローディング文字のみ削除するように変更 ( PR [#464](https://github.com/cwtickle/danoniplus/pull/464) )
- 🛠️ ローカルmp3の読込失敗時のエラーメッセージを追加 ( PR [#464](https://github.com/cwtickle/danoniplus/pull/464) )
- 🛠️ FPS値の変数化に対応 ( PR [#465](https://github.com/cwtickle/danoniplus/pull/465) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v8.6.3)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v8.6.2...v8.6.3#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v8.6.3)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v8.6.3.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v8.6.3.tar.gz) )

## v8.6.2 ([2019-10-01](https://github.com/cwtickle/danoniplus/releases/tag/v8.6.2))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v8.6.2/total)](https://github.com/cwtickle/danoniplus/archive/v8.6.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v8.6.2/support/v8?style=social)](https://github.com/cwtickle/danoniplus/compare/v8.6.2...support/v8#files_bucket) 
- 🐞 譜面変更時にゲージ設定が初期値に設定されてしまう問題を修正 ( PR [#462](https://github.com/cwtickle/danoniplus/pull/462) ) <- :boom: [**v8.5.2**](Changelog-v8#v852-2019-09-29)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v8.6.2)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v8.6.1...v8.6.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v8.6.2)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v8.6.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v8.6.2.tar.gz) )<br>❤️ goe (@goe0)

## v8.6.1 ([2019-09-30](https://github.com/cwtickle/danoniplus/releases/tag/v8.6.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v8.6.1/total)](https://github.com/cwtickle/danoniplus/archive/v8.6.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v8.6.1/support/v8?style=social)](https://github.com/cwtickle/danoniplus/compare/v8.6.1...support/v8#files_bucket) 
- 🛠️ ファイル読込に失敗した場合のメッセージを追加 ( Issue [#458](https://github.com/cwtickle/danoniplus/pull/458), PR [#459](https://github.com/cwtickle/danoniplus/pull/459), [#460](https://github.com/cwtickle/danoniplus/pull/460) )
- 🐞 エラーウィンドウが表示されない問題を修正 ( PR [#459](https://github.com/cwtickle/danoniplus/pull/459) ) <- :boom: [**v2.6.0**](Changelog-v2#v260-2019-02-10)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v8.6.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v8.6.0...v8.6.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v8.6.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v8.6.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v8.6.1.tar.gz) )

## v8.6.0 ([2019-09-29](https://github.com/cwtickle/danoniplus/releases/tag/v8.6.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v8.6.0/total)](https://github.com/cwtickle/danoniplus/archive/v8.6.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v8.6.0/support/v8?style=social)](https://github.com/cwtickle/danoniplus/compare/v8.6.0...support/v8#files_bucket) 
- 🛠️ コード整理（本来booleanで定義すべき変数の型変更、定数化）( PR [#455](https://github.com/cwtickle/danoniplus/pull/455) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v8.6.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v8.5.2...v8.6.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v8.6.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v8.6.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v8.6.0.tar.gz) )

## v8.5.2 ([2019-09-29](https://github.com/cwtickle/danoniplus/releases/tag/v8.5.2))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v8.5.2/total)](https://github.com/cwtickle/danoniplus/archive/v8.5.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v8.5.2/support/v8?style=social)](https://github.com/cwtickle/danoniplus/compare/v8.5.2...support/v8#files_bucket) 
- 🛠️ ゲージ設定周りのコード整理 ( PR [#453](https://github.com/cwtickle/danoniplus/pull/453) )
- 🛠️ サンプルHTMLの見直し（HTML5の記述に対応していないブラウザに警告表示）( PR [#452](https://github.com/cwtickle/danoniplus/pull/452) )
- 🐞 クエリで譜面番号が指定されていた場合に、初期速度が1譜面目の設定になる問題を修正 ( PR [#453](https://github.com/cwtickle/danoniplus/pull/453) ) <- :boom: [**v7.3.0**](Changelog-v7#v730-2019-07-20)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v8.5.2)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v8.5.1...v8.5.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v8.5.2)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v8.5.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v8.5.2.tar.gz) )<br>❤️ MFV2 (@MFV2), izkdic

## v8.5.1 ([2019-09-28](https://github.com/cwtickle/danoniplus/releases/tag/v8.5.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v8.5.1/total)](https://github.com/cwtickle/danoniplus/archive/v8.5.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v8.5.1/support/v8?style=social)](https://github.com/cwtickle/danoniplus/compare/v8.5.1...support/v8#files_bucket) 
- 🛠️ 外部の譜面ファイル内の譜面データを一時クリアする条件を「譜面ファイル分割時」「譜面番号固定時」に限定するよう修正 ( PR [#450](https://github.com/cwtickle/danoniplus/pull/450) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v8.5.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v8.5.0...v8.5.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v8.5.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v8.5.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v8.5.1.tar.gz) )

## v8.5.0 ([2019-09-28](https://github.com/cwtickle/danoniplus/releases/tag/v8.5.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v8.5.0/total)](https://github.com/cwtickle/danoniplus/archive/v8.5.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v8.5.0/support/v8?style=social)](https://github.com/cwtickle/danoniplus/compare/v8.5.0...support/v8#files_bucket) 
- ⭐️ 譜面毎のファイル分割に対応 ( Issue [#446](https://github.com/cwtickle/danoniplus/pull/446), PR [#447](https://github.com/cwtickle/danoniplus/pull/447), [#448](https://github.com/cwtickle/danoniplus/pull/448) )
- ⭐️ 譜面をファイル分割した場合に、譜面番号を常時固定するかどうかの設定を追加 ( PR [#447](https://github.com/cwtickle/danoniplus/pull/447) )
- 🛠️ 譜面読込時に前回読込を行ったデータをクリアするよう変更 ( PR [#447](https://github.com/cwtickle/danoniplus/pull/447) )
- 🛠️ 画像のプリロードを二重に行わないよう変更 ( PR [#447](https://github.com/cwtickle/danoniplus/pull/447) )
- 🐞 クエリで譜面番号が指定されていた場合に、キー数が異なることがある問題を修正 ( PR [#447](https://github.com/cwtickle/danoniplus/pull/447) ) <- :boom: [**v7.7.0**](Changelog-v7#v770-2019-08-25)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v8.5.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v8.4.0...v8.5.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v8.5.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v8.5.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v8.5.0.tar.gz) )

## v8.4.0 ([2019-09-26](https://github.com/cwtickle/danoniplus/releases/tag/v8.4.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v8.4.0/total)](https://github.com/cwtickle/danoniplus/archive/v8.4.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v8.4.0/support/v8?style=social)](https://github.com/cwtickle/danoniplus/compare/v8.4.0...support/v8#files_bucket) 
- ⭐️ 譜面変更用のセレクターについて、キー別フィルターを追加 ( PR [#444](https://github.com/cwtickle/danoniplus/pull/444) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v8.4.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v8.3.0...v8.4.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v8.4.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v8.4.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v8.4.0.tar.gz) )

## v8.3.0 ([2019-09-26](https://github.com/cwtickle/danoniplus/releases/tag/v8.3.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v8.3.0/total)](https://github.com/cwtickle/danoniplus/archive/v8.3.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v8.3.0/support/v8?style=social)](https://github.com/cwtickle/danoniplus/compare/v8.3.0...support/v8#files_bucket) 
- ⭐️ 譜面変更用のセレクターを実装 ( PR [#441](https://github.com/cwtickle/danoniplus/pull/441) )
- 🛠️ 曲中ショートカット移動後のkeyUpイベントを無効化 ( PR [#442](https://github.com/cwtickle/danoniplus/pull/442) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v8.3.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v8.2.0...v8.3.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v8.3.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v8.3.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v8.3.0.tar.gz) )

## v8.2.0 ([2019-09-24](https://github.com/cwtickle/danoniplus/releases/tag/v8.2.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v8.2.0/total)](https://github.com/cwtickle/danoniplus/archive/v8.2.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v8.2.0/support/v8?style=social)](https://github.com/cwtickle/danoniplus/compare/v8.2.0...support/v8#files_bucket) 
- ⭐️ 曲名（複数行）を1行で表示する場合に間を空白で埋めない（全角で埋める）設定を追加 ( PR [#437](https://github.com/cwtickle/danoniplus/pull/437) )
- ⭐️ 制作者表示の複数化に対応（リザルトコピー）( PR [#438](https://github.com/cwtickle/danoniplus/pull/438) )
- ⭐️ 設定・結果画面への制作者名表示有無設定を追加 ( PR [#438](https://github.com/cwtickle/danoniplus/pull/438) )
- ⭐️ 譜面別データ(LocalStorage)に制作者名を付加できるように変更 ( PR [#439](https://github.com/cwtickle/danoniplus/pull/439) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v8.2.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v8.1.0...v8.2.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v8.2.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v8.2.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v8.2.0.tar.gz) )<br>❤️ goe (@goe0)

## v8.1.0 ([2019-09-23](https://github.com/cwtickle/danoniplus/releases/tag/v8.1.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v8.1.0/total)](https://github.com/cwtickle/danoniplus/archive/v8.1.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v8.1.0/support/v8?style=social)](https://github.com/cwtickle/danoniplus/compare/v8.1.0...support/v8#files_bucket) 
- 🛠️ タイトル画面の「Dancing☆Onigiri」の文字をbacktitle_dataの前面に来るように変更 ( PR [#435](https://github.com/cwtickle/danoniplus/pull/435) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v8.1.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v8.0.4...v8.1.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v8.1.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v8.1.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v8.1.0.tar.gz) )<br>❤️ ショウタ

## v8.0.4 ([2019-09-23](https://github.com/cwtickle/danoniplus/releases/tag/v8.0.4))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v8.0.4/total)](https://github.com/cwtickle/danoniplus/archive/v8.0.4.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v8.0.4/support/v8?style=social)](https://github.com/cwtickle/danoniplus/compare/v8.0.4...support/v8#files_bucket) 
- 🐞 ロード画面でEnterを押すと複数回ロードが発生する問題を修正 ( PR [#432](https://github.com/cwtickle/danoniplus/pull/432) ) <- :boom: **initial**
- 🐞 メイン画面で曲中リトライキーを連打した場合に譜面がずれることがある問題を修正 ( PR [#433](https://github.com/cwtickle/danoniplus/pull/433) ) <- :boom: **initial**

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v8.0.4)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v8.0.3...v8.0.4#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v8.0.4)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v8.0.4.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v8.0.4.tar.gz) )<br>❤️ すずめ (@suzme)

## v8.0.3 ([2019-09-22](https://github.com/cwtickle/danoniplus/releases/tag/v8.0.3))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v8.0.3/total)](https://github.com/cwtickle/danoniplus/archive/v8.0.3.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v8.0.3/support/v8?style=social)](https://github.com/cwtickle/danoniplus/compare/v8.0.3...support/v8#files_bucket) 
- 🐞 fadeFrameが譜面数に到達しない場合にプレイが止まる問題を修正 ( PR [#430](https://github.com/cwtickle/danoniplus/pull/430) ) <- :boom: [**v8.0.0**](Changelog-v8#v800-2019-09-08)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v8.0.3)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v8.0.2...v8.0.3#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v8.0.3)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v8.0.3.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v8.0.3.tar.gz) )<br>❤️ izkdic, すずめ (@suzme)

## v8.0.2 ([2019-09-16](https://github.com/cwtickle/danoniplus/releases/tag/v8.0.2))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v8.0.2/total)](https://github.com/cwtickle/danoniplus/archive/v8.0.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v8.0.2/support/v8?style=social)](https://github.com/cwtickle/danoniplus/compare/v8.0.2...support/v8#files_bucket) 
- 🐞 ゲームオーバー時の200ミリ秒遅延により、ハイスコア表示がおかしくなることがある問題を修正 ( PR [#428](https://github.com/cwtickle/danoniplus/pull/428) ) <- :boom: **initial**

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v8.0.2)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v8.0.1...v8.0.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v8.0.2)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v8.0.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v8.0.2.tar.gz) )<br>❤️ MFV2 (@MFV2), izkdic

## v8.0.1 ([2019-09-15](https://github.com/cwtickle/danoniplus/releases/tag/v8.0.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v8.0.1/total)](https://github.com/cwtickle/danoniplus/archive/v8.0.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v8.0.1/support/v8?style=social)](https://github.com/cwtickle/danoniplus/compare/v8.0.1...support/v8#files_bucket) 
- 🐞 速度変化が補正込みで負のフレームにあるときの不具合を修正 ( PR [#426](https://github.com/cwtickle/danoniplus/pull/426) ) <- :boom: **initial**

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v8.0.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v8.0.0...v8.0.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v8.0.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v8.0.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v8.0.1.tar.gz) )<br>❤️ すずめ (@suzme)

## v8.0.0 ([2019-09-08](https://github.com/cwtickle/danoniplus/releases/tag/v8.0.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v8.0.0/total)](https://github.com/cwtickle/danoniplus/archive/v8.0.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v8.0.0/support/v8?style=social)](https://github.com/cwtickle/danoniplus/compare/v8.0.0...support/v8#files_bucket) 
- ⭐️ タイトル／リザルトモーションの音楽同期対応 ( PR [#419](https://github.com/cwtickle/danoniplus/pull/419), [#423](https://github.com/cwtickle/danoniplus/pull/423) )
- ⭐️ リザルトモーションを譜面別に作成できるように対応 ( PR [#419](https://github.com/cwtickle/danoniplus/pull/419) )
- ⭐️ 楽曲のフェードアウト長を指定できるように変更 ( Issue [#413](https://github.com/cwtickle/danoniplus/pull/413), PR [#420](https://github.com/cwtickle/danoniplus/pull/420) )
- ⭐️ 速度変化表記 (speed_data/change)の統一 ( Issue [#416](https://github.com/cwtickle/danoniplus/pull/416), PR [#421](https://github.com/cwtickle/danoniplus/pull/421) )
- ⭐️ ステップゾーン(下)位置を変更できる設定を追加 ( PR [#424](https://github.com/cwtickle/danoniplus/pull/424) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v8.0.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v7.9.1...v8.0.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v8.0.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v8.0.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v8.0.0.tar.gz) )<br>❤️ ショウタ

[**<- v9**](Changelog-v9) | **v8** | [**v7 ->**](Changelog-v7)
