
[^ Tips Indexに戻る](./tips-index)

| < [カスタム画面の作成](./tips-0024-custom-window) | **設定追加の実装例** || [タイトルや歌詞表示に半角コンマ(,)を使う](./tips-0001-comma) > |

# 設定追加の実装例

- [カスタム画面の作成](./tips-0024-custom-window) で画面ができていることを前提とします。
- フリーズアローヒット時に画面回転する設定 (FrzReturn) をカスタムJSで実装した場合の例です。
- この機能に関係のない箇所は省略しています。

## v39以降の補足

- v39で本機能が実装されたため、あくまで参考として残します。
- transform属性の上書き・更新は、v39時点では`addTransform`という関数が用意されており、こちらを使った方が安全です。
- 下記は本体での実装例です。（プレイ画面での処理部分に相当）

```javascript
const changeReturn = (_rad, _axis) => {
	g_workObj.frzReturnFlg = true;
	let _transform = `rotate${_axis[0]}(${_rad}deg)`;
	if (_axis[1] !== undefined) {
		_transform += ` rotate${_axis[1]}(${_rad}deg)`;
	}
	if (document.getElementById(`mainSprite`) !== null) {
		addTransform(`mainSprite`, `frzReturn`, _transform);

		if (_rad < 360 && g_workObj.frzReturnFlg) {
			setTimeout(() => changeReturn(_rad + 4, _axis), 20);
		} else {
			addTransform(`mainSprite`, `frzReturn`, ``);
			g_workObj.frzReturnFlg = false;
		}
	}
}
```

## タイトル画面前の読み込み処理

```javascript
g_customJsObj.preTitle.push(() => {

    // FreezeReturn設定の初期化
    g_stateObj.freezeReturn = C_FLG_OFF;
    g_settings.freezeReturns = [C_FLG_OFF, `X-Axis`, `Y-Axis`, `Z-Axis`, `Random`, `XY-Axis`, `XZ-Axis`, `YZ-Axis`, `Random+`];
    g_settings.freezeReturnNum = 0;
    g_lblNameObj.FreezeReturn = `FrzReturn`;
    g_lang_msgObj.Ja.freezeReturn = `フリーズアローヒット時(一部矢印ヒット時)、X/Y/Z軸のいずれかに回転します`;
    g_lang_msgObj.En.freezeReturn = `When the freeze arrow is hit (or some arrows are hit), it rotates on the X/Y/Z axis.`;

    // ショートカット設定
    g_shortcutObj._exSetting = {
        // ・・・省略
        ShiftLeft_KeyF: { id: `lnkFreezeReturnL` },
        KeyF: { id: `lnkFreezeReturnR` },
        // ・・・省略
    };
};
```

## 独自の設定画面（Ex-Settings）

```javascript
const _exSettingInit = () => {
    // ・・・省略

    g_settingPos.exSetting = {
        // ・・・省略
        freezeReturn: { heightPos: 2.5, y: 0, dw: 0, dh: 0 },
        // ・・・省略
    }
    // ・・・省略
    createGeneralSetting(spriteList.freezeReturn, `freezeReturn`);
    // ・・・省略
};
```

## ロード画面／プレイ画面（初期）

```javascript
g_customJsObj.loading.push(() => {

    // FreezeReturnの初期化
    g_workObj._freezeReturnFlg = false;
};

g_customJsObj.main.push(() => {

    // メイン初期でのtransform設定を更新
    g_workObj._transform = mainSprite.style.transform;
};

```

## 判定別の追加処理

### FreezeReturnのメイン処理

- 360度画面を回転する処理。最大2軸設定できます。
- `setTimeout`により後続に自身の関数に1度足した状態で呼び出すことで連続した回転を実現しています。
- フリーズアローヒット中に他のフリーズアローがヒットすると二重に動いてしまうため、`g_workObj._freezeReturnFlg` でロックを掛けています。

```javascript
/**
 * FreezeReturnの追加処理
 * @param {number} _rad 回転角度
 * @param {number[]} _axis 回転軸
 */
const _changeReturn = (_rad, _axis) => {
    g_workObj._freezeReturnFlg = true;
    let _transform = g_workObj._transform;
    _transform += ` rotate${_axis[0]}(${_rad}deg)`;
    if (_axis[1] !== undefined) {
        _transform += ` rotate${_axis[1]}(${_rad}deg)`;
    }
    if (document.getElementById(`mainSprite`) !== null) {
        document.getElementById(`mainSprite`).style.transform = _transform;

        if (_rad < 360 && g_workObj._freezeReturnFlg) {
            setTimeout(() => _changeReturn(_rad + 1, _axis), 5);
        } else {
            g_workObj._freezeReturnFlg = false;
        }
    }
}
```

### 判定別処理

- `g_stateObj.freezeReturn`の値により、`_setFreezeReturn`にて対象の軸を設定します。
- `g_workObj._freezeReturnFlg = false` (ロックが掛かっていない)のときだけ、処理を開始するようにします。
- フリーズアローがない譜面の場合は効果がなくなってしまうため、矢印の場合でもイイ判定が100の倍数にかかるたびに起動するようにします。

```javascript
const _setFreezeReturn = {
    'OFF': () => true,
    'X-Axis': () => [`X`],
    'Y-Axis': () => [`Y`],
    'Z-Axis': () => [`Z`],
    'Random': () => [`X`, `Y`, `Z`][Math.floor(Math.random() * 3)],
    'XY-Axis': () => [`X`, `Y`],
    'XZ-Axis': () => [`X`, `Z`],
    'YZ-Axis': () => [`Y`, `Z`],
    'Random+': () => {
        const axis1 = [`X`, `Y`, `Z`][Math.floor(Math.random() * 3)];
        const axis2 = [`X`, `Y`, `Z`, undefined].filter(val => val !== axis1)[Math.floor(Math.random() * 3)];
        return [axis1, axis2];
    },
};
g_customJsObj.judg_ii.push(() => {
    if (g_stateObj.freezeReturn !== C_FLG_OFF) {
        if (g_resultObj.ii % 100 === 0 && !g_workObj._freezeReturnFlg) {
            _changeReturn(1, _setFreezeReturn[g_stateObj.freezeReturn]());
        }
    }
});
g_customJsObj.judg_frzHit.push(() => {
    if (g_stateObj.freezeReturn !== C_FLG_OFF) {
        if (!g_workObj._freezeReturnFlg) {
            _changeReturn(1, _setFreezeReturn[g_stateObj.freezeReturn]());
        }
    }
});
g_customJsObj.judg_iknai.push(() => {
    if (g_stateObj.freezeReturn !== C_FLG_OFF) {
        if (!g_workObj._freezeReturnFlg) {
            _changeReturn(1, _setFreezeReturn[g_stateObj.freezeReturn]());
        }
    }
});
```

## 結果画面

- 今回追加したFreezeReturnの設定を追加します。

```javascript
g_customJsObj.result.push(() => {

    const withOptions = (_flg, _defaultSet, _displayText = _flg) =>
        (_flg !== _defaultSet ? getStgDetailName(_displayText) : ``);

    const exSettingData = [
        withOptions(g_stateObj.stepArea, C_FLG_OFF),
        withOptions(g_stateObj.freezeReturn, C_FLG_OFF, `FR:${g_stateObj.freezeReturn}`), // 追加
    ].filter(value => value !== ``).join(`, `);

    // ・・・省略
};
```

## ページ作成者
- ティックル

[^ Tips Indexに戻る](./tips-index)

| < [カスタム画面の作成](./tips-0024-custom-window) | **設定追加の実装例** || [タイトルや歌詞表示に半角コンマ(,)を使う](./tips-0001-comma) > |
