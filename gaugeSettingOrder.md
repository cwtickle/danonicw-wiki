| [< 判定仕様](./AboutJudgment) | **ゲージ設定適用順仕様** | [Gitクライアントを使ったFork、Pull Requestの方法 >](./fork-pull-request) |

# ゲージ設定適用順仕様
- 仕様が複雑なため、備忘用として記載します。

### 1. ゲージ優先度
- カスタムゲージの場合
    - ゲージ個別設定(gaugeXXX) > 共通設定(g_presetObj.gaugeCustom)
- 汎用ゲージかつゲージ初期設定(Original or Normal)で無い場合
    - ゲージ個別設定(gaugeXXX) > 共通設定(g_presetObj.gaugeCustom) > 基本設定(g_gaugeOptionObj)
- 汎用ゲージかつゲージ初期設定(Original or Normal)の場合
    - ゲージ個別設定(gaugeXXX) > 共通設定(g_presetObj.gaugeCustom) > 譜面ヘッダー(difData) > 基本設定(g_gaugeOptionObj)

### 2. ゲージ設定を変更した場合の挙動
- Difficultyを変更した場合
    - 同一のゲージ設定名があれば、そのゲージ設定に合致するゲージ設定を1.から取得
    - ない場合は、その譜面のゲージ設定群の先頭をゲージ設定として、そのゲージ設定を1.から取得
- Gaugeを変更した場合
    - 対象譜面のゲージ設定群のカーソルを1つ右 or 左にずらして、そのゲージ設定を1.から取得

### 3. ゲージ設定関連の変数（ノルマ、回復、ダメージ、初期値）
複数譜面あるため、いずれも配列で保持。  
#### ゲージ個別設定(gaugeX)、共通設定(g_presetObj.gaugeCustom)  
※この部分の初期化処理は`getGaugeSetting`で実施
```javascript
g_gaugeOptionObj.gaugeXs = { lifeBorders: [], lifeRecoverys: [], lifeDamages: [], lifeInits: []  }
```

#### 譜面ヘッダー(difData)  
※g_headerObjは譜面ヘッダー定義の集合体のため、ゲージ設定のみ抜粋  
※この部分の初期化処理は`headerConvert`で実施
```javascript
g_headerObj = { lifeBorders: [], lifeRecoverys: [], lifeDamages: [], lifeInits: []  }
```

#### 基本設定(g_gaugeOptionObj)　※初期化済み  

- ライフ制ゲージの場合
```javascript
g_gaugeOptionObj = { clearSurvival: [], rcvSurvival: [], dmgSurvival: [], initSurvival: [] }
```

- ノルマ制ゲージの場合
```javascript
g_gaugeOptionObj = { clearBorder: [], rcvBorder: [], dmgBorder: [], initBorder: [] }
```

### 4. ノルマ制ゲージ決定条件
- ゲージ個別設定(gaugeX)、共通設定(g_presetObj.gaugeCustom)
    - `g_gaugeOptionObj.gaugeXXXs.lifeBorders[譜面番号]` が「x」以外であること。
- 譜面ヘッダー(difData)  
    - `g_headerObj.lifeBorders[譜面番号]`が「x」以外であること。
- 基本設定(g_gaugeOptionObj)
    - `g_gaugeOptionObj.typeSurvival[譜面番号]` もしくは `g_gaugeOptionObj.typeBorder[譜面番号]` の値が「border」であること。（「survival」の場合はライフ制）

### 5. 回復・ダメージ量の固定・変動条件
- カスタムゲージの場合
    - 譜面ヘッダー: `customGaugeX`の2要素目が「F」なら固定、「V」なら変動。  
内部変数としては `g_gaugeOptionObj.varCustom[譜面番号]` で定義され、「OFF」なら固定、「ON」なら変動。
- ライフ制ゲージ群の場合　※4. とは異なる。「Original, Light, Heavy, SuddenDeath」の1セット。
    - `g_gaugeOptionObj.varSurvival[譜面番号]` の値が「OFF」なら固定、「ON」なら変動。
- ノルマ制ゲージ群の場合　※4. とは異なる。「Normal, Easy, Hard, SuddenDeath」の1セット。
    - `g_gaugeOptionObj.varBorder[譜面番号]` の値が「OFF」なら固定、「ON」なら変動。

### 関連項目
- [譜面ヘッダー仕様](./dos_header) &gt; [difData](./dos-h0002-difData)　- 譜面情報
- [譜面ヘッダー仕様](./dos_header) &gt; [customGauge](./dos-h0053-customGauge)　- カスタムゲージ設定
- [譜面ヘッダー仕様](./dos_header) &gt; [gaugeX](./dos-h0022-gaugeX)　- ゲージ設定の詳細
- [共通設定ファイル仕様](./dos_setting) &gt; [ゲージ設定](./dos-s0003-initialGauge)
- [オブジェクト一覧](./ObjectReferenceIndex) &gt; [g_headerObj](./obj-v0002-g_headerObj)
- [オブジェクト一覧](./ObjectReferenceIndex) &gt; [g_gaugeOptionObj](./obj-v0007-g_gaugeOptionObj)

| [< 判定仕様](./AboutJudgment) | **ゲージ設定適用順仕様** | [Gitクライアントを使ったFork、Pull Requestの方法 >](./fork-pull-request) |