**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos_effect) | Japanese** 

| [< 譜面本体仕様](./dos_score) | **譜面エフェクト仕様 (速度/ 色/ 背景)** | [グラデーション仕様 >](./dos-c0001-gradation) |

# 譜面エフェクト仕様

速度変化・色変化など、譜面データには画面内にさまざまな効果を与える機能があります。  
ParaFla版の仕様と互換性があります。  

ParaFlaソースとの共用項目には(★)をつけています。  

- [速度変化 (speed_data/change, boost_data)](dos-e0001-speedData)(★) [:pencil:](dos-e0001-speedData/_edit) 
- [色変化 (ncolor_data)](dos-e0002-ncolorData) [:pencil:](dos-e0002-ncolorData/_edit) 
    - [色変化[旧仕様] (acolor_data, color_data)](dos-e0002-colorData)(★) [:pencil:](dos-e0002-colorData/_edit) 
- [歌詞表示 (word_data)](dos-e0003-wordData)(★) [:pencil:](dos-e0003-wordData/_edit) 
- [背景・マスクモーション (back_data, mask_data)](dos-e0004-animationData) [:pencil:](dos-e0004-animationData/_edit) 
- [スキン変更 (style_data)](dos-e0008-styleData) [:pencil:](dos-e0008-styleData/_edit) 
- [矢印・フリーズアローモーション (arrowMotion_data, frzMotion_data)](dos-e0005-motionData) [:pencil:](dos-e0005-motionData/_edit) 
- [キー数変化 (keych_data)](dos-e0006-keychData) [:pencil:](dos-e0006-keychData/_edit)
- [スクロール反転・階層移動 (scrollch_data)](dos-e0007-scrollchData) [:pencil:](dos-e0007-scrollchData/_edit)

| [< 譜面本体仕様](./dos_score) | **譜面エフェクト仕様 (速度/ 色/ 背景)** | [グラデーション仕様 >](./dos-c0001-gradation) |
