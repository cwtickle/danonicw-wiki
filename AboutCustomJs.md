**[English](https://github.com/cwtickle/danoniplus-docs/wiki/AboutCustomJs) | Japanese**

| [< 疑似フレーム処理仕様](./AboutFrameProcessing) | **カスタムjs(スキンjs)による処理割込み** | [カスタム関数の定義 >](./AboutCustomFunction) |

# カスタムjs(スキンjs)による処理割込み
Dancing☆Onigiri(CW Edition)ソースでは、カスタムjsファイルやスキンjsファイルによる処理割込みを行うことで`danoni_main.js`を直接変更することなく、画面内のオブジェクトの情報を変更することができます。    
アクセスできるオブジェクトについては[DOM](DOM)、[ID一覧](idReferenceIndex)、[オブジェクト一覧](objectReferenceIndex)のページもご覧ください。  

なお、スキン用関数についてはフォントサイズや色変更などがCSSで制御できない場合に使用することを想定していますが、カスタムjsと同じような使い方をすることも可能です。  

ver25.3.0より、カスタム関数の定義に一部変更がありました。[カスタム関数の定義](AboutCustomFunction)もご覧ください。  
下記の図は、次の設定がされていることが前提になっています。  
スキンJs名は便宜上の名前です。実際のスキンJs名は「danoni_skin_(スキン名).js」の形式にする必要があります。

### カスタムJs側の関数の指定例
```js
function preTitle1 () {
    // 処理
}
g_customJsObj.pretitle.push(preTitle1);
```
### スキンJs側の関数の指定例
```js
function skinTitle1 () {
    // 処理
}
g_skinJsObj.title.push(skinTitle1);
```

## 画面別の割込み処理の流れ
- 四角の中に記載されている名前は関数名を表します。
- custom1, custom2, skin1, skin2に記載の関数名は最初から存在しているわけではなく、上記の設定例に沿って定義する必要があります。

### タイトル画面 (titleInit)
<img src="https://github.com/cwtickle/danoniplus/assets/44026291/ddaeb8a0-6725-46ea-80e9-8f57b4006ed5" width="80%">

### 設定画面 (optionInit)
<img src="https://github.com/cwtickle/danoniplus/assets/44026291/4ab981cb-727b-4268-bf73-9bb3dc02ab9c" width="80%">

### 設定(Display)画面 (settingsDisplayInit)
<img src="https://github.com/cwtickle/danoniplus/assets/44026291/f731affa-9731-4236-a5ff-fae14aa4e3bf" width="80%">

### キーコンフィグ画面 (keyConfigInit)
<img src="https://github.com/cwtickle/danoniplus/assets/44026291/7a7f2ad3-72b4-4f21-8f81-3bf20329cf75" width="80%">

### 読込画面 (loadingScoreInit)
<img src="https://github.com/cwtickle/danoniplus/assets/44026291/cb573630-0799-4b23-9e67-7572a2a4375d" width="80%">

### メイン画面 (mainInit)
<img src="https://github.com/cwtickle/danoniplus/assets/44026291/e88b4481-ac65-4bbe-9e2a-03f1e76ed9c1" width="80%">

### 結果画面 (resultInit)
<img src="https://github.com/cwtickle/danoniplus/assets/44026291/99fd3d83-7e8e-43d6-9e8a-46c5c2e160d8" width="80%">

## 割込み関数の制限
- 同じ画面内であれば、基本的に[ID一覧](./IdReferenceIndex)で定義されているIDが使用できます。  
ただし、他の画面へ移動するボタンにはアクセスできません。
- [オブジェクト一覧](./ObjectReferenceIndex)で定義されている関数のうち、外側にある関数（関数内関数ではない）が使用できます。

## 関連項目
- [**customJs**](dos-h0019-customjs) カスタムjsファイルの指定
- [**skinType**](dos-h0054-skinType) スキン設定
- [ボタン処理の拡張・上書き](tips-0005-button-expansion)
- [ボタン処理のコピー](tips-0014-copy-button-process)

| [< 疑似フレーム処理仕様](./AboutFrameProcessing) | **カスタムjs(スキンjs)による処理割込み** | [カスタム関数の定義 >](./AboutCustomFunction) |
