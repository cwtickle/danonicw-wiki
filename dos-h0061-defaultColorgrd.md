**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-h0061-defaultColorgrd) | Japanese** 

[Home](./) &gt; [譜面ヘッダー仕様](dos_header) &gt; [プレイ時の初期設定](dos_header#-プレイ時の初期設定) &gt; [矢印・フリーズアロー色の設定](dos_header#矢印フリーズアロー色の設定)

| [<- frzShadowColor](dos-h0062-frzShadowColor) | **defaultColorgrd** | [defaultFrzColorUse ->](dos-h0063-defaultFrzColorUse) |

## defaultColorgrd
- 矢印・フリーズアロー色の自動グラデーション設定

### 使い方
```
|defaultColorgrd=true|
|defaultColorgrd=true,#000066|
```
### 説明
譜面ヘッダー：setColor, frzColor 及び color_data, acolor_dataで指定するカラーコードについて、  
単独のカラーコードを指定した場合に自動で線形グラデーションにするかどうかを設定します。  
※titlegrd, titleArrowgrdは対象外です。

ver18.2.0より、自動グラデーションの中継色を設定できるようになりました。  
ver21.2.0より、自動グラデーションの中継色の既定値が [backBright](dos-h0081-backBright) や 背景状況により変わるようになりました。  
明色用と判断した場合は、既定値が #111111 に変わります。

|番号|設定例|内容|
|----|----|----|
|1|true|自動で線形グラデーションにするかどうかの設定 (既定：false)|
|2|#000066|自動グラデーションにする際の中継色の設定（既定：#eeeeee [暗色], #111111 [明色]）|

#### 変更イメージ
```
|setColor=#ffff99| // 単独のカラーコード
|setColor=to right:#ffff99:#eeeeee:#ffff99@linear-gradient| // trueにするとこの効果と同じになる
```

### 関連項目
- [**setColor**](dos-h0003-setColor) [:pencil:](dos-h0003-setColor/_edit) 矢印色
- [**frzColor**](dos-h0004-frzColor) [:pencil:](dos-h0004-frzColor/_edit) フリーズアロー色
- [backBright](dos-h0081-backBright) [:pencil:](dos-h0081-backBright/_edit) 背景の明暗状態
- [色変化 (ncolor_data)](dos-e0002-ncolorData) [:pencil:](dos-e0002-ncolorData/_edit) 
- [グラデーション仕様](dos-c0001-gradation) [:pencil:](dos-c0001-gradation/_edit)

### 更新履歴

|Version|変更内容|
|----|----|
|[v21.2.0](https://github.com/cwtickle/danoniplus/releases/tag/v21.2.0)|・自動グラデーションにする際の中継色のデフォルト値適用条件を変更|
|[v18.2.0](https://github.com/cwtickle/danoniplus/releases/tag/v18.2.0)|・自動グラデーションにする際の中継色を実装|
|[v12.0.0](https://github.com/cwtickle/danoniplus/releases/tag/v12.0.0)|・初回実装|

| [<- frzShadowColor](dos-h0062-frzShadowColor) | **defaultColorgrd** | [defaultFrzColorUse ->](dos-h0063-defaultFrzColorUse) |