⭐ New Features / 🛠️ Improvements / 🐞 Bug Fixes  
( Files changed: ⚪ danoni_main.js / 🔵 danoni_constants.js / 🔴 css files )

[**<- v12**](Changelog-v12) | **v11** | [**v10 ->**](Changelog-v10)  
(**🔖 [14 Releases](https://github.com/cwtickle/danoniplus/releases?q=tag%3Av11)** )

## [Warning] 更新終了後の不具合情報
- [更新を終了したバージョンの不具合情報#v11](DeprecatedVersionBugs#v11) を参照

## v11.4.5 ([2020-04-16](https://github.com/cwtickle/danoniplus/releases/tag/v11.4.5))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v11.4.5/total)](https://github.com/cwtickle/danoniplus/archive/v11.4.5.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v11.4.5/support/v11?style=social)](https://github.com/cwtickle/danoniplus/compare/v11.4.5...support/v11#files_bucket) 
- 🛠️ 厳密等価演算子が使用されていない箇所を修正 ( PR [#654](https://github.com/cwtickle/danoniplus/pull/654) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v11.4.5)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v11.4.4...v11.4.5#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v11.4.5)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v11.4.5.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v11.4.5.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v11.4.5/js/danoni_main.js)
/ 🎣 [**v13.5.1**](./Changelog-v13#v1351-2020-04-16)

## v11.4.4 ([2020-04-12](https://github.com/cwtickle/danoniplus/releases/tag/v11.4.4))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v11.4.4/total)](https://github.com/cwtickle/danoniplus/archive/v11.4.4.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v11.4.4/support/v11?style=social)](https://github.com/cwtickle/danoniplus/compare/v11.4.4...support/v11#files_bucket) 
- 🐞 キーコンフィグ画面で代替キーの無効化ができない問題を修正 ( PR [#647](https://github.com/cwtickle/danoniplus/pull/647) ) <- :boom: [**v10.3.0**](Changelog-v10#v1030-2019-12-01)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v11.4.4)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v11.4.3...v11.4.4#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v11.4.4)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v11.4.4.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v11.4.4.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v11.4.4/js/danoni_main.js)
/ 🎣 [**v13.3.1**](./Changelog-v13#v1321-2020-04-12)

## v11.4.3 ([2020-04-11](https://github.com/cwtickle/danoniplus/releases/tag/v11.4.3))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v11.4.3/total)](https://github.com/cwtickle/danoniplus/archive/v11.4.3.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v11.4.3/support/v11?style=social)](https://github.com/cwtickle/danoniplus/compare/v11.4.3...support/v11#files_bucket) 
- 🛠️ ローカルで直接htmlファイルを開いても画像が表示されるように変更 ( PR [#627](https://github.com/cwtickle/danoniplus/pull/627), [#628](https://github.com/cwtickle/danoniplus/pull/628) )
- 🐞 設定画面（フェードイン表示）、キーコンフィグ画面（カラータイプ）のid重複を修正 ( PR [#642](https://github.com/cwtickle/danoniplus/pull/642) ) <- :boom: [**v3.12.0**](Changelog-v3#v3120-2019-04-21), [**v0.76.x**](Changelog-v0#v076x-2018-11-24)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v11.4.3)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v11.4.2...v11.4.3#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v11.4.3)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v11.4.3.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v11.4.3.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v11.4.3/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v11.4.3/js/lib/danoni_constants.js)
/ 🎣 [**v13.2.1**](./Changelog-v13#v1321-2020-04-11)

## v11.4.2 ([2020-03-08](https://github.com/cwtickle/danoniplus/releases/tag/v11.4.2))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v11.4.2/total)](https://github.com/cwtickle/danoniplus/archive/v11.4.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v11.4.2/support/v11?style=social)](https://github.com/cwtickle/danoniplus/compare/v11.4.2...support/v11#files_bucket) 
- 🐞 ダミー矢印のヒット位置が前の通常矢印のヒット結果に引きずられる問題を修正 ( PR [#623](https://github.com/cwtickle/danoniplus/pull/623) ) <- :boom: [**v10.4.0**](Changelog-v10#v1040-2019-12-07)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v11.4.2)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v11.4.1...v11.4.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v11.4.2)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v11.4.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v11.4.2.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v11.4.2/js/danoni_main.js)
/ 🎣 [**v12.2.1**](./Changelog-v12#v1221-2020-03-08)

## v11.4.1 ([2020-02-25](https://github.com/cwtickle/danoniplus/releases/tag/v11.4.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v11.4.1/total)](https://github.com/cwtickle/danoniplus/archive/v11.4.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v11.4.1/support/v11?style=social)](https://github.com/cwtickle/danoniplus/compare/v11.4.1...support/v11#files_bucket) 
- 🐞 スコア上書き時ハイスコア差分が計算されない問題を修正 ( PR [#616](https://github.com/cwtickle/danoniplus/pull/616) ) <- :boom: [**v10.5.0**](Changelog-v10#v1050-2019-12-13)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v11.4.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v11.4.0...v11.4.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v11.4.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v11.4.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v11.4.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v11.4.1/js/danoni_main.js)
/ 🎣 [**v12.1.1**](./Changelog-v12#v1211-2020-02-25)

----

## v11.4.0 ([2020-02-05](https://github.com/cwtickle/danoniplus/releases/tag/v11.4.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v11.4.0/total)](https://github.com/cwtickle/danoniplus/archive/v11.4.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v11.4.0/support/v11?style=social)](https://github.com/cwtickle/danoniplus/compare/v11.4.0...support/v11#files_bucket) 
- ⭐️ 譜面明細画面に速度変化数・プレイ時間などの補足情報を追加 ( Issue [#417](https://github.com/cwtickle/danoniplus/pull/417), PR [#598](https://github.com/cwtickle/danoniplus/pull/598) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v11.4.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v11.3.1...v11.4.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v11.4.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v11.4.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v11.4.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v11.4.0/js/danoni_main.js)

## v11.3.1 ([2020-01-30](https://github.com/cwtickle/danoniplus/releases/tag/v11.3.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v11.3.1/total)](https://github.com/cwtickle/danoniplus/archive/v11.3.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v11.3.1/support/v11?style=social)](https://github.com/cwtickle/danoniplus/compare/v11.3.1...support/v11#files_bucket) 
- 🛠️ startFrameの値に応じて速度変化・譜面密度グラフの位置が変わるように修正 ( PR [#595](https://github.com/cwtickle/danoniplus/pull/595) )
- 🐞 譜面密度グラフにおいて、最大密度部分に色が付かない問題を修正 ( PR [#595](https://github.com/cwtickle/danoniplus/pull/595) ) <- :boom: [**v11.3.0**](Changelog-v11#v1130-2020-01-29)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v11.3.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v11.3.0...v11.3.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v11.3.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v11.3.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v11.3.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v11.3.1/js/danoni_main.js)

## v11.3.0 ([2020-01-29](https://github.com/cwtickle/danoniplus/releases/tag/v11.3.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v11.3.0/total)](https://github.com/cwtickle/danoniplus/archive/v11.3.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v11.3.0/support/v11?style=social)](https://github.com/cwtickle/danoniplus/compare/v11.3.0...support/v11#files_bucket) 
- ⭐️ 速度変化の遷移グラフを表示する機能の実装 ( Issue [#417](https://github.com/cwtickle/danoniplus/pull/417), PR [#589](https://github.com/cwtickle/danoniplus/pull/589), [#590](https://github.com/cwtickle/danoniplus/pull/590), [#591](https://github.com/cwtickle/danoniplus/pull/591) )
- ⭐️ 譜面密度グラフの実装 ( Issue [#417](https://github.com/cwtickle/danoniplus/pull/417), PR [#592](https://github.com/cwtickle/danoniplus/pull/592) )
- 🛠️ リザルト画面部分のコード整理 ( PR [#588](https://github.com/cwtickle/danoniplus/pull/588) )
- 🛠️ 譜面ロード部分のコード整理 ( PR [#590](https://github.com/cwtickle/danoniplus/pull/590), [#594](https://github.com/cwtickle/danoniplus/pull/594) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v11.3.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v11.2.1...v11.3.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v11.3.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v11.3.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v11.3.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v11.3.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v11.3.0/js/lib/danoni_constants.js)<br>❤️ goe (@goe0), MFV2 (@MFV2)

## v11.2.1 ([2020-01-12](https://github.com/cwtickle/danoniplus/releases/tag/v11.2.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v11.2.1/total)](https://github.com/cwtickle/danoniplus/archive/v11.2.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v11.2.1/support/v11?style=social)](https://github.com/cwtickle/danoniplus/compare/v11.2.1...support/v11#files_bucket) 
- 🛠️ メイン画面初期化部分のコード見直し ( PR [#586](https://github.com/cwtickle/danoniplus/pull/586) )
- 🐞 Display: Judgement をOFFにしたときに、判定キャラクタが消えない問題を修正 ( PR [#586](https://github.com/cwtickle/danoniplus/pull/586) ) <- :boom: [**v11.0.0**](Changelog-v11#v1100-2019-12-14)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v11.2.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v11.2.0...v11.2.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v11.2.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v11.2.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v11.2.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v11.2.1/js/danoni_main.js)

## v11.2.0 ([2020-01-04](https://github.com/cwtickle/danoniplus/releases/tag/v11.2.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v11.2.0/total)](https://github.com/cwtickle/danoniplus/archive/v11.2.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v11.2.0/support/v11?style=social)](https://github.com/cwtickle/danoniplus/compare/v11.2.0...support/v11#files_bucket) 
- 🛠️ キーを押す操作の処理の見直し ( PR [#582](https://github.com/cwtickle/danoniplus/pull/582) )
- 🐞 ロード画面でブロックすべきキーコントロールがブロックされない問題を修正 ( PR [#581](https://github.com/cwtickle/danoniplus/pull/581) ) <- :boom: [**v8.0.4**](Changelog-v8#v804-2019-09-23)
- 🐞 譜面名未指定のときにリザルトコピー時のクレジットがundefinedになる問題を修正 ( PR [#583](https://github.com/cwtickle/danoniplus/pull/583) ) <- :boom: [**v8.2.0**](Changelog-v8#v820-2019-09-24)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v11.2.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v11.1.2...v11.2.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v11.2.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v11.2.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v11.2.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v11.2.0/js/danoni_main.js)<br>❤️ すずめ (@suzme)

## v11.1.2 ([2019-12-28](https://github.com/cwtickle/danoniplus/releases/tag/v11.1.2))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v11.1.2/total)](https://github.com/cwtickle/danoniplus/archive/v11.1.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v11.1.2/support/v11?style=social)](https://github.com/cwtickle/danoniplus/compare/v11.1.2...support/v11#files_bucket) 
- ⭐️ 下側の歌詞表示位置をステップゾーン位置に追随させる設定を追加 ( PR [#574](https://github.com/cwtickle/danoniplus/pull/574) )
- ⭐️ 判定キャラクタ、ReadyのY座標を下側のステップゾーン位置(stepYR)に合わせて
自動調整するように変更 ( PR [#574](https://github.com/cwtickle/danoniplus/pull/574) )
- 🐞 ライフが0のとき、ライフ値が反映されない問題を修正 ( PR [#573](https://github.com/cwtickle/danoniplus/pull/573), [#576](https://github.com/cwtickle/danoniplus/pull/576) ) <- :boom: [**v10.5.0**](Changelog-v10#v1050-2019-12-13)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v11.1.2)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v11.0.2...v11.1.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v11.1.2)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v11.1.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v11.1.2.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v11.1.2/js/danoni_main.js)<br>❤️ goe (@goe0)

## v11.0.2 ([2019-12-22](https://github.com/cwtickle/danoniplus/releases/tag/v11.0.2))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v11.0.2/total)](https://github.com/cwtickle/danoniplus/archive/v11.0.2.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v11.0.2/support/v11?style=social)](https://github.com/cwtickle/danoniplus/compare/v11.0.2...support/v11#files_bucket) 
- 🐞 キーコンフィグがundefinedになる場合がある問題を修正 ( PR [#569](https://github.com/cwtickle/danoniplus/pull/569) ) <- :boom: **initial**
- 🐞 実装途中の未定義関数により、低速時エラーで止まる問題を修正 ( PR [#570](https://github.com/cwtickle/danoniplus/pull/570) ) <- :boom: [**v10.3.0**](Changelog-v10#v1030-2019-12-01)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v11.0.2)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v11.0.1...v11.0.2#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v11.0.2)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v11.0.2.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v11.0.2.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v11.0.2/js/danoni_main.js)

## v11.0.1 ([2019-12-16](https://github.com/cwtickle/danoniplus/releases/tag/v11.0.1))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v11.0.1/total)](https://github.com/cwtickle/danoniplus/archive/v11.0.1.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v11.0.1/support/v11?style=social)](https://github.com/cwtickle/danoniplus/compare/v11.0.1...support/v11#files_bucket) 
- 🐞 Fast表示の誤字を修正 ( PR [#566](https://github.com/cwtickle/danoniplus/pull/566) ) <- :boom: [**v11.0.0**](Changelog-v11#v1100-2019-12-14)
- 🐞 結果画面（曲名）のid重複を修正 ( PR [#567](https://github.com/cwtickle/danoniplus/pull/567) ) <- :boom: [**v3.5.0**](Changelog-v3#v350-2019-03-23)

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v11.0.1)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v11.0.0...v11.0.1#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v11.0.1)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v11.0.1.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v11.0.1.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v11.0.1/js/danoni_main.js)

## v11.0.0 ([2019-12-14](https://github.com/cwtickle/danoniplus/releases/tag/v11.0.0))
[![GitHub Releases (by Release)](https://img.shields.io/github/downloads/cwtickle/danoniplus/v11.0.0/total)](https://github.com/cwtickle/danoniplus/archive/v11.0.0.zip) [![GitHub commits since tagged version (branch)](https://img.shields.io/github/commits-since/cwtickle/danoniplus/v11.0.0/support/v11?style=social)](https://github.com/cwtickle/danoniplus/compare/v11.0.0...support/v11#files_bucket) 
- ⭐️ Displayオプションのデフォルト設定を追加 ( Issue [#415](https://github.com/cwtickle/danoniplus/pull/415), PR [#556](https://github.com/cwtickle/danoniplus/pull/556) )
- ⭐️ カスタム用のDisplayオプション「Special」を追加 ( PR [#556](https://github.com/cwtickle/danoniplus/pull/556) )
- ⭐️ 判定キャラクタの下にFast/Slow/Just表記を追加 ( PR [#556](https://github.com/cwtickle/danoniplus/pull/556) )
- ⭐️ 判定キャラクタの位置調整機能を追加 ( Issue [#415](https://github.com/cwtickle/danoniplus/pull/415), PR [#556](https://github.com/cwtickle/danoniplus/pull/556) )
- ⭐️ 5keyのKeyPattern: 3(おにぎり中央)の既定のキーコンフィグを変更 ( PR [#555](https://github.com/cwtickle/danoniplus/pull/555) )
- ⭐️ 7keyの既定のキーコンフィグを追加 ( PR [#555](https://github.com/cwtickle/danoniplus/pull/555) )
- 🛠️ 別キーモードで、指定の必要が無い項目を削除 ( PR [#555](https://github.com/cwtickle/danoniplus/pull/555) )
- 🛠️ fileでも起動可能なようにcrossorigin属性の付加条件を変更 ( PR [#564](https://github.com/cwtickle/danoniplus/pull/564) )

> 📁 [**Tree**](https://github.com/cwtickle/danoniplus/tree/v11.0.0)
/ ↕️ [**Diffs**](https://github.com/cwtickle/danoniplus/compare/v10.5.0...v11.0.0#files_bucket) / ▶️ [**Preview**](https://danonicw.skr.jp/?v=v11.0.0)
/ 📥 **Download** ( [zip](https://github.com/cwtickle/danoniplus/archive/refs/tags/v11.0.0.zip) / [tar.gz](https://github.com/cwtickle/danoniplus/archive/refs/tags/v11.0.0.tar.gz) ) [⚪](https://github.com/cwtickle/danoniplus/blob/v11.0.0/js/danoni_main.js)[🔵](https://github.com/cwtickle/danoniplus/blob/v11.0.0/js/lib/danoni_constants.js)

[**<- v12**](Changelog-v12) | **v11** | [**v10 ->**](Changelog-v10)
