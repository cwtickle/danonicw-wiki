**[English](https://github.com/cwtickle/danoniplus-docs/wiki/dos-h0075-readyColor) | Japanese** 

[Home](./) &gt; [譜面ヘッダー仕様](dos_header) &gt; [プレイ時の初期設定](dos_header#-プレイ時の初期設定) &gt; [画面表示・キーコントロール](dos_header#画面表示キーコントロール)

| [<- readyAnimationName](dos-h0074-readyAnimationName) | **readyColor** | [readyHtml ->](dos-h0080-readyHtml) |

## readyColor
- Ready?の先頭色設定

### 使い方
```
|readyColor=#ffffcc|
```
### 説明
Ready表示の先頭文字「R」の色を指定します。デフォルトは1番目の矢印色が指定されます。  
※この項目はグラデーションに対応していません。

またreadyHtmlが指定された場合、この設定は無視されます。

### 関連項目
- [readyDelayFrame](dos-h0052-readyDelayFrame) [:pencil:](dos-h0052-readyDelayFrame/_edit) Ready?が表示されるまでの遅延フレーム数
- [readyAnimationFrame](dos-h0073-readyAnimationFrame) [:pencil:](dos-h0073-readyAnimationFrame/_edit) Ready?のアニメーションフレーム数
- [readyAnimationName](dos-h0074-readyAnimationName) [:pencil:](dos-h0074-readyAnimationName/_edit) Ready?のアニメーション名
- [readyHtml](dos-h0080-readyHtml) [:pencil:](dos-h0080-readyHtml/_edit) Ready?の文字設定

### 更新履歴

|Version|変更内容|
|----|----|
|[v16.3.0](https://github.com/cwtickle/danoniplus/releases/tag/v16.3.0)|・初回実装|

| [<- readyAnimationName](dos-h0074-readyAnimationName) | **readyColor** | [readyHtml ->](dos-h0080-readyHtml) |