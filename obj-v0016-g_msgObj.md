[← オブジェクトリファレンスに戻る](ObjectReferenceIndex)  

## g_msgObj / g_lang_msgObj

### 概要
- オンマウステキスト、確認メッセージを管理するオブジェクト。
- `g_lang_msgObj`は`g_msgObj`の言語別版で、  
`g_lang_msgObj.Ja/En`側のプロパティが優先される設定になっている。

### 生成タイミング
- 初回起動時  
(言語別は headerConvert関数呼び出し時)

### プロパティ
```javascript
const g_msgObj = {
    reload: `説明文の言語を変更します。\nChange the language of the description.`,
};

const g_lang_msgObj = {

    Ja: {
        howto: `ゲーム画面の見方や設定の詳細についてのページへ移動します（GitHub Wiki）。`,
        dataReset: `この作品で保存されているハイスコアや\nAdjustment情報等をリセットします。`,
        github: `Dancing☆Onigiri (CW Edition)のGitHubページへ移動します。`,
        security: `Dancing☆Onigiri (CW Edition)のサポート情報ページへ移動します。`,

        dataResetConfirm: `この作品のローカル設定をクリアします。よろしいですか？\n(ハイスコアやAdjustment等のデータがクリアされます)`,
        keyResetConfirm: `キーを初期配置に戻します。よろしいですか？`,
        colorCopyConfirm: `フリーズアローの配色を矢印色に置き換えます\n(通常・ヒット時双方を置き換えます)。よろしいですか？`,

        difficulty: `譜面を選択します。`,
        speed: `矢印の流れる速度を設定します。\n外側のボタンは1x単位、内側は0.25x単位で変更できます。`,
        motion: `矢印の速度を一定ではなく、\n変動させるモーションをつけるか設定します。`,
        reverse: `矢印の流れる向きを設定します。`,
        scroll: `各レーンのスクロール方向をパターンに沿って設定します。\nReverse:ONでスクロール方向を反転します。`,
        shuffle: `譜面を左右反転したり、ランダムにします。\nランダムにした場合は別譜面扱いとなり、ハイスコアは保存されません。`,
        autoPlay: `オートプレイや一部キーを自動で打たせる設定を行います。\nオートプレイ時はハイスコアを保存しません。`,
        gauge: `クリア条件を設定します。\n[Start] ゲージ初期値, [Border] クリア条件(ハイフン時は0),\n[Recovery] 回復量, [Damage] ダメージ量, [Accuracy] クリアに必要な正確率(オンマウスで許容ミス数表示)`,
        excessive: `空押し判定を行うか設定します。`,
        adjustment: `タイミングにズレを感じる場合、\n数値を変えることでフレーム単位のズレを直すことができます。\n外側のボタンは5f刻み、真ん中は1f刻み、内側は0.5f刻みで調整できます。`,
        fadein: `譜面を途中から再生します。\n途中から開始した場合はハイスコアを保存しません。`,
        volume: `ゲーム内の音量を設定します。`,

        graph: `速度変化や譜面密度状況、\n譜面の難易度など譜面の詳細情報を表示します。`,
        dataSave: `ハイスコア、リバース設定、\nキーコンフィグの保存の有無を設定します。`,
        toDisplay: `プレイ画面上のオブジェクトの\n表示・非表示（一部透明度）を設定します。`,
        toSettings: `SETTINGS画面へ戻ります。`,

        d_stepzone: `ステップゾーンの表示`,
        d_judgment: `判定キャラクタ・コンボの表示`,
        d_fastslow: `Fast/Slow表示`,
        d_lifegauge: `ライフゲージの表示`,
        d_score: `現時点の判定数を表示`,
        d_musicinfo: `音楽情報（時間表示含む）`,
        d_filterline: `Hidden+, Sudden+使用時のフィルターの境界線表示`,
        d_speed: `途中変速、個別加速の有効化設定`,
        d_color: `色変化の有効化設定`,
        d_lyrics: `歌詞表示の有効化設定`,
        d_background: `背景・マスクモーションの有効化設定`,
        d_arroweffect: `矢印・フリーズアローモーションの有効化設定`,
        d_special: `作品固有の特殊演出の有効化設定`,

        appearance: `流れる矢印の見え方を制御します。`,
        opacity: `判定キャラクタ、コンボ数、Fast/Slow、Hidden+/Sudden+の\n境界線表示の透明度を設定します。`,
        hitPosition: `判定位置にズレを感じる場合、\n数値を変えることで判定の中央位置を1px単位(プラス:手前, マイナス:奥側)で調整することができます。\n早押し・遅押し傾向にある場合に使用します。`,

        configType: `キーコンフィグ対象を切り替えます。\n[Main] メインキーのみ, [Replaced] 代替キーのみ, [ALL] 全て`,
        colorType: `矢印色の配色パターンを変更します。\nType1～4選択時は色変化が自動でOFFになります。\n[Type0] グラデーション切替, [Type1～4] デフォルトパターン`,
        imgType: `矢印・フリーズアローなどのオブジェクトの見た目を変更します。`,
        colorGroup: `矢印・フリーズアロー色グループの割り当てパターンを変更します。`,
        shuffleGroup: `Mirror/X-Mirror/Turning/Random/S-Random選択時、シャッフルするグループを変更します。\n矢印の上にある同じ数字同士でシャッフルします。`,
        stepRtnGroup: `矢印などノーツの種類、回転に関するパターンを切り替えます。\nあらかじめ設定されている場合のみ変更可能です。`,

        pickArrow: `色番号ごとの矢印色（枠、塗りつぶし）、通常時のフリーズアロー色（枠、帯）を\nカラーピッカーから選んで変更できます。`,
        pickColorCopy: `このボタンを押すと、フリーズアローの配色を矢印（枠）の色で上書きします。\nヒット時のフリーズアローの色も上書きします。`,
    },

    En: {
        howto: `Go to the page about the game screen and settings at GitHub Wiki.`,
        dataReset: `Resets the high score, adjustment information, etc. saved in this game.`,
        github: `Go to the GitHub page of Dancing Onigiri "CW Edition".`,
        security: `Go to the support information page for Dancing Onigiri "CW Edition".`,

        dataResetConfirm: `Delete the local settings in this game. Is it OK?\n(High score, adjustment, volume and some settings will be initialized)`,
        keyResetConfirm: `Resets the assigned key to the initial state. Is it OK?`,
        colorCopyConfirm: `Replace freeze arrow color scheme with arrow color\n(replace both normal and hit). Is this OK?`,

        difficulty: `Select a chart.`,
        speed: `Set the speed of the sequences.\nThe outer button can be changed in 1x increments and the inner button in 0.25x increments.`,
        motion: `Set whether to turn on acceleration or deceleration\nin the middle of the sequence.`,
        reverse: `Set the flow direction of the sequences.`,
        scroll: `Set the scroll direction for each lane according to the pattern.\nIf "Reverse:ON" sets, reverse the scroll direction.`,
        shuffle: `Flip the chart left and right or make it random.\nIf you make it random, it will be treated as other charts and the high score will not be saved.`,
        autoPlay: `Set to auto play and to hit some keys automatically.\nHigh score is not saved during auto play.`,
        gauge: `Set the clear condition.\n[Start] initial value, [Border] borderline value (hyphen means zero),\n[Recovery] recovery amount, [Damage] damage amount,\n[Accuracy] accuracy required to clear (mouseover to see the number of allowed mistakes)`,
        excessive: `Set whether to use excessive miss judgment.`,
        adjustment: `If you feel a shift in timing, \nyou can correct the shift in frame units by changing the value.\nThe outer button can be adjusted in 5 frame increments, the middle in 1 frame increments, \nand the inner button in 0.5 frame increments.`,
        fadein: `Plays the chart from the middle.\nIf you start in the middle, the high score will not be saved.`,
        volume: `Set the in-game volume.`,

        graph: `Displays detailed information about the chart, such as sequences' speed changes, chart's density status, and chart's difficulty.`,
        dataSave: `Set whether to save the high score, reverse setting, and key config.`,
        toDisplay: `Set the display or non-display (partial transparency) of objects on the play screen.`,
        toSettings: `Return to the SETTINGS screen.`,

        d_stepzone: `Display step zone`,
        d_judgment: `Display judgment and combo counts`,
        d_fastslow: `Display fast and slow `,
        d_lifegauge: `Display lifegauge`,
        d_score: `Display the current number of judgments`,
        d_musicinfo: `Display the music credits and current time`,
        d_filterline: `Filter border display when using "Hidden+" or "Sudden+"`,
        d_speed: `Enable speed change settings`,
        d_color: `Enable color change settings`,
        d_lyrics: `Enable lyrics display`,
        d_background: `Enable background images and animations`,
        d_arroweffect: `Enable sequences' animations`,
        d_special: `Enable setting of special effects to the work`,

        appearance: `Controls how the flowing sequences look.`,
        opacity: `Set the transparency of some objects such as judgment, combo counts, fast and slow`,
        hitPosition: `If you feel a discrepancy in the judgment position, \nyou can adjust the center position of the judgment in 1px increments \n (plus: in front, minus: at the back) by changing the numerical value. \nUse this function when there is a tendency to push too fast or too slow.`,

        configType: `Switch the key config target.\n[Main] main keys only, [Replaced] alternate keys only, [ALL] all keys`,
        colorType: `Change the color scheme of the sequences color.\nWhen Type 1 to 4 are selected, the color change is automatically turned off.\n[Type0] Switch the sequences color gradations, [Type1～4] default color scheme`,
        imgType: `Change the appearance of sequences.`,
        colorGroup: `Change the sequences color group assignment pattern.`,
        shuffleGroup: `Change the shuffle group when Mirror, X-Mirror, Turning, Random or S-Random are selected.\nShuffle with the same numbers listed above.`,
        stepRtnGroup: `Switches the type of notes, such as arrows, and the pattern regarding rotation.\nThis can only be changed if it has been set in advance.`,

        pickArrow: `Change the frame or fill of arrow color and the frame or bar of normal freeze-arrow color\nfor each color number from the color picker.`,
        pickColorCopy: `Pressing this button will override the color scheme of the freeze arrow with the frame color of the arrow. \nIt also overrides the color of the freeze arrow on hit.`,
    },

};
```