# 5key
- 難易度表 :link: : [片手通常](http://dodl4.g3.xrea.com/normal5s/) / [両手通常](http://dodl4.g3.xrea.com/normal5d/) / [片手発狂](http://dodl4.g3.xrea.com/insane5s/) / [両手発狂](http://dodl4.g3.xrea.com/insane5d/) 
- メジャー多鍵データベース :link: : [5key](https://apoi108.sakura.ne.jp/danoni/danoni_all_list/?key=5)

<img src="https://raw.githubusercontent.com/wiki/cwtickle/danoniplus/wiki/keyimg/key5_02.png" width="40%">
<img src="https://raw.githubusercontent.com/wiki/cwtickle/danoniplus/wiki/keyimg/key5_03.png" width="40%">

<img src="https://raw.githubusercontent.com/wiki/cwtickle/danoniplus/wiki/keyimg/key5_01.jpg" width="40%">

## 概要
- Dancing☆Onigiri登場時から存在するキー。初出は**2003年**。
- 矢印キー4種とおにぎり(スペースキー)の組み合わせが特徴。
- 最初はおにぎりが右のパターンのみだったが、プレイの多様化に伴って左、中央でもプレイできるようになっている。

## キーパターン
- おにぎり位置は右、左、中央から選択できる。
- Pattern: 3では、両手プレイ用に初期のキー割り当てが変わるようになっている。

|Pattern|Default Key Config (Ja/En)|
|----|----|
|1|<img src="https://github.com/cwtickle/danoniplus/assets/44026291/d8b21bbd-7143-4c56-aa53-c8c8c8bc5783" width="40%">|
|2|<img src="https://github.com/cwtickle/danoniplus/assets/44026291/aea89a2d-b2eb-4ed4-b5b3-7fd143f76c30" width="40%">|
|3|<img src="https://github.com/cwtickle/danoniplus/assets/44026291/4d783255-566d-400c-a822-6e52bb2b8230" width="40%">|

## キー特有のオプション
### Scroll

|Pattern|Image|
|----|----|
|Cross|<img src="https://github.com/cwtickle/danoniplus/assets/44026291/904d73a1-baac-4fcf-9a02-51bd59462195" width="50%">|
|Split|<img src="https://github.com/cwtickle/danoniplus/assets/44026291/41d84684-7b07-45d6-be09-ac6007d5bd1d" width="50%">|
|Alternate|<img src="https://github.com/cwtickle/danoniplus/assets/44026291/d38a2d47-61ad-4926-a50d-2fdf269a807e" width="50%">|

### AutoPlay

|Pattern|Image|
|----|----|
|Onigiri|<img src="https://github.com/cwtickle/danoniplus/assets/44026291/d57da3f1-6ca4-4303-b6c0-9cd9dedce580" width="40%">|
